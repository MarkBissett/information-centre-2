namespace WoodPlan5
{
    partial class frm_UT_Tree_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Tree_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07056UTTreeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.DeadTreeCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.GrowthRateIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07406UTGrowthRatesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.GrowthRateIDGridLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.G55CategoryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07186UTG55CategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07059UTTreeObjectTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditSpeciesID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07062UTSpeciesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScientificName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOfTrees = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditNumberOfTrees = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TreeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoleIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LengthTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AreaTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatLongPairsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.XYPairsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.MapIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.XTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.YTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SizeBandIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07058UTSizeBandWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07006UTCircuitStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.CircuitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.PoleNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ReferenceNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForTreeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPoleName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMapID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatLongPairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArea = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForXYPairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupSpecies = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSizeBandID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForG55CategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGrowthRateID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeadTree = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07006_UT_Circuit_StatusTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter();
            this.sp07056_UT_Tree_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07056_UT_Tree_ItemTableAdapter();
            this.sp07058_UT_Size_Band_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07058_UT_Size_Band_With_BlankTableAdapter();
            this.sp07059_UT_Tree_Object_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07059_UT_Tree_Object_Types_With_BlankTableAdapter();
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter();
            this.sp07062_UT_Species_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07062_UT_Species_List_With_BlankTableAdapter();
            this.sp07186_UT_G55_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07186_UT_G55_Categories_With_BlankTableAdapter();
            this.sp07406_UT_Growth_Rates_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07406_UT_Growth_Rates_With_BlankTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07056UTTreeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeadTreeCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthRateIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07406UTGrowthRatesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthRateIDGridLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.G55CategoryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07186UTG55CategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07059UTTreeObjectTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSpeciesID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07062UTSpeciesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditNumberOfTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongPairsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XYPairsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SizeBandIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07058UTSizeBandWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongPairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXYPairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSizeBandID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForG55CategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrowthRateID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeadTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(908, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Size = new System.Drawing.Size(908, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(908, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 627);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "ID";
            this.gridColumn8.FieldName = "Value";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Species ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // colID2
            // 
            this.colID2.Caption = "Size Band ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(908, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 653);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(908, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(908, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 627);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DeadTreeCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.GrowthRateIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.G55CategoryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.TPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.TreeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LengthTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AreaTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LatLongPairsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.XYPairsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.MapIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.XTextEdit);
            this.dataLayoutControl1.Controls.Add(this.YTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SizeBandIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.CircuitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07056UTTreeItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTreeID,
            this.ItemForPoleID,
            this.ItemForClientName,
            this.ItemForClientID,
            this.ItemForCircuitNumber,
            this.ItemForCircuitID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(205, 77, 250, 364);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(908, 627);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(114, 252);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(782, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 51;
            // 
            // sp07056UTTreeItemBindingSource
            // 
            this.sp07056UTTreeItemBindingSource.DataMember = "sp07056_UT_Tree_Item";
            this.sp07056UTTreeItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DeadTreeCheckEdit
            // 
            this.DeadTreeCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "DeadTree", true));
            this.DeadTreeCheckEdit.Location = new System.Drawing.Point(109, 229);
            this.DeadTreeCheckEdit.MenuManager = this.barManager1;
            this.DeadTreeCheckEdit.Name = "DeadTreeCheckEdit";
            this.DeadTreeCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DeadTreeCheckEdit.Properties.ValueChecked = 1;
            this.DeadTreeCheckEdit.Properties.ValueUnchecked = 0;
            this.DeadTreeCheckEdit.Size = new System.Drawing.Size(787, 19);
            this.DeadTreeCheckEdit.StyleController = this.dataLayoutControl1;
            this.DeadTreeCheckEdit.TabIndex = 8;
            // 
            // GrowthRateIDGridLookUpEdit
            // 
            this.GrowthRateIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "GrowthRateID", true));
            this.GrowthRateIDGridLookUpEdit.EditValue = "";
            this.GrowthRateIDGridLookUpEdit.Location = new System.Drawing.Point(109, 157);
            this.GrowthRateIDGridLookUpEdit.MenuManager = this.barManager1;
            this.GrowthRateIDGridLookUpEdit.Name = "GrowthRateIDGridLookUpEdit";
            this.GrowthRateIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GrowthRateIDGridLookUpEdit.Properties.DataSource = this.sp07406UTGrowthRatesWithBlankBindingSource;
            this.GrowthRateIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.GrowthRateIDGridLookUpEdit.Properties.NullText = "";
            this.GrowthRateIDGridLookUpEdit.Properties.PopupView = this.GrowthRateIDGridLookUpEditView;
            this.GrowthRateIDGridLookUpEdit.Properties.ValueMember = "Value";
            this.GrowthRateIDGridLookUpEdit.Size = new System.Drawing.Size(787, 20);
            this.GrowthRateIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.GrowthRateIDGridLookUpEdit.TabIndex = 5;
            this.GrowthRateIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.GrowthRateIDGridLookUpEdit_Validating);
            // 
            // sp07406UTGrowthRatesWithBlankBindingSource
            // 
            this.sp07406UTGrowthRatesWithBlankBindingSource.DataMember = "sp07406_UT_Growth_Rates_With_Blank";
            this.sp07406UTGrowthRatesWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // GrowthRateIDGridLookUpEditView
            // 
            this.GrowthRateIDGridLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.colDescription,
            this.colOrder});
            this.GrowthRateIDGridLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn8;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.GrowthRateIDGridLookUpEditView.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.GrowthRateIDGridLookUpEditView.Name = "GrowthRateIDGridLookUpEditView";
            this.GrowthRateIDGridLookUpEditView.OptionsLayout.Columns.StoreAllOptions = true;
            this.GrowthRateIDGridLookUpEditView.OptionsLayout.StoreAppearance = true;
            this.GrowthRateIDGridLookUpEditView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.GrowthRateIDGridLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GrowthRateIDGridLookUpEditView.OptionsView.AllowHtmlDrawHeaders = true;
            this.GrowthRateIDGridLookUpEditView.OptionsView.ColumnAutoWidth = false;
            this.GrowthRateIDGridLookUpEditView.OptionsView.EnableAppearanceEvenRow = true;
            this.GrowthRateIDGridLookUpEditView.OptionsView.ShowGroupPanel = false;
            this.GrowthRateIDGridLookUpEditView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.GrowthRateIDGridLookUpEditView.OptionsView.ShowIndicator = false;
            this.GrowthRateIDGridLookUpEditView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Growth Rate";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // G55CategoryIDGridLookUpEdit
            // 
            this.G55CategoryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "G55CategoryID", true));
            this.G55CategoryIDGridLookUpEdit.EditValue = "";
            this.G55CategoryIDGridLookUpEdit.Location = new System.Drawing.Point(109, 205);
            this.G55CategoryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.G55CategoryIDGridLookUpEdit.Name = "G55CategoryIDGridLookUpEdit";
            this.G55CategoryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.G55CategoryIDGridLookUpEdit.Properties.DataSource = this.sp07186UTG55CategoriesWithBlankBindingSource;
            this.G55CategoryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.G55CategoryIDGridLookUpEdit.Properties.NullText = "";
            this.G55CategoryIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.G55CategoryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.G55CategoryIDGridLookUpEdit.Size = new System.Drawing.Size(787, 20);
            this.G55CategoryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.G55CategoryIDGridLookUpEdit.TabIndex = 7;
            this.G55CategoryIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.G55CategoryIDGridLookUpEdit_Validating);
            // 
            // sp07186UTG55CategoriesWithBlankBindingSource
            // 
            this.sp07186UTG55CategoriesWithBlankBindingSource.DataMember = "sp07186_UT_G55_Categories_With_Blank";
            this.sp07186UTG55CategoriesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.gridColumn7,
            this.colRecordOrder2});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn7;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "G55 Category";
            this.colDescription3.FieldName = "Description";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 147;
            // 
            // colRecordOrder2
            // 
            this.colRecordOrder2.Caption = "Order";
            this.colRecordOrder2.FieldName = "RecordOrder";
            this.colRecordOrder2.Name = "colRecordOrder2";
            this.colRecordOrder2.OptionsColumn.AllowEdit = false;
            this.colRecordOrder2.OptionsColumn.AllowFocus = false;
            this.colRecordOrder2.OptionsColumn.ReadOnly = true;
            // 
            // TPONumberTextEdit
            // 
            this.TPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "TPONumber", true));
            this.TPONumberTextEdit.Location = new System.Drawing.Point(109, 181);
            this.TPONumberTextEdit.MenuManager = this.barManager1;
            this.TPONumberTextEdit.Name = "TPONumberTextEdit";
            this.TPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TPONumberTextEdit, true);
            this.TPONumberTextEdit.Size = new System.Drawing.Size(787, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TPONumberTextEdit, optionsSpelling2);
            this.TPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.TPONumberTextEdit.TabIndex = 6;
            // 
            // TypeIDGridLookUpEdit
            // 
            this.TypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "TypeID", true));
            this.TypeIDGridLookUpEdit.Location = new System.Drawing.Point(109, 83);
            this.TypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.TypeIDGridLookUpEdit.Name = "TypeIDGridLookUpEdit";
            this.TypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TypeIDGridLookUpEdit.Properties.DataSource = this.sp07059UTTreeObjectTypesWithBlankBindingSource;
            this.TypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TypeIDGridLookUpEdit.Properties.NullText = "";
            this.TypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.TypeIDGridLookUpEdit.Properties.ReadOnly = true;
            this.TypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.TypeIDGridLookUpEdit.Size = new System.Drawing.Size(787, 20);
            this.TypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TypeIDGridLookUpEdit.TabIndex = 2;
            // 
            // sp07059UTTreeObjectTypesWithBlankBindingSource
            // 
            this.sp07059UTTreeObjectTypesWithBlankBindingSource.DataMember = "sp07059_UT_Tree_Object_Types_With_Blank";
            this.sp07059UTTreeObjectTypesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tree Type ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tree Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 201;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07060UTTreeSpeciesLinkedToTreeBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(20, 333);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemGridLookUpEditSpeciesID,
            this.repositoryItemSpinEditNumberOfTrees,
            this.repositoryItemSpinEditPercentage});
            this.gridControl1.Size = new System.Drawing.Size(868, 274);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07060UTTreeSpeciesLinkedToTreeBindingSource
            // 
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataMember = "sp07060_UT_Tree_Species_Linked_To_Tree";
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeSpeciesID,
            this.colTreeID,
            this.colSpeciesID,
            this.colNumberOfTrees,
            this.colPercentage,
            this.colRemarks,
            this.colGUID,
            this.colstrMode,
            this.colstrRecordIDs});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colTreeSpeciesID
            // 
            this.colTreeSpeciesID.Caption = "Tree Species ID";
            this.colTreeSpeciesID.FieldName = "TreeSpeciesID";
            this.colTreeSpeciesID.Name = "colTreeSpeciesID";
            this.colTreeSpeciesID.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesID.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesID.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesID.Width = 105;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.Caption = "Species";
            this.colSpeciesID.ColumnEdit = this.repositoryItemGridLookUpEditSpeciesID;
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.Visible = true;
            this.colSpeciesID.VisibleIndex = 0;
            this.colSpeciesID.Width = 208;
            // 
            // repositoryItemGridLookUpEditSpeciesID
            // 
            this.repositoryItemGridLookUpEditSpeciesID.AutoHeight = false;
            this.repositoryItemGridLookUpEditSpeciesID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditSpeciesID.DataSource = this.sp07062UTSpeciesListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditSpeciesID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditSpeciesID.Name = "repositoryItemGridLookUpEditSpeciesID";
            this.repositoryItemGridLookUpEditSpeciesID.NullText = "";
            this.repositoryItemGridLookUpEditSpeciesID.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditSpeciesID.ValueMember = "ID";
            // 
            // sp07062UTSpeciesListWithBlankBindingSource
            // 
            this.sp07062UTSpeciesListWithBlankBindingSource.DataMember = "sp07062_UT_Species_List_With_Blank";
            this.sp07062UTSpeciesListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.colScientificName,
            this.gridColumn6});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn4;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Species Name";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 143;
            // 
            // colScientificName
            // 
            this.colScientificName.Caption = "Scientific Name";
            this.colScientificName.FieldName = "ScientificName";
            this.colScientificName.Name = "colScientificName";
            this.colScientificName.OptionsColumn.AllowEdit = false;
            this.colScientificName.OptionsColumn.AllowFocus = false;
            this.colScientificName.OptionsColumn.ReadOnly = true;
            this.colScientificName.Visible = true;
            this.colScientificName.VisibleIndex = 1;
            this.colScientificName.Width = 153;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // colNumberOfTrees
            // 
            this.colNumberOfTrees.Caption = "Number of Trees";
            this.colNumberOfTrees.ColumnEdit = this.repositoryItemSpinEditNumberOfTrees;
            this.colNumberOfTrees.FieldName = "NumberOfTrees";
            this.colNumberOfTrees.Name = "colNumberOfTrees";
            this.colNumberOfTrees.Visible = true;
            this.colNumberOfTrees.VisibleIndex = 1;
            this.colNumberOfTrees.Width = 101;
            // 
            // repositoryItemSpinEditNumberOfTrees
            // 
            this.repositoryItemSpinEditNumberOfTrees.AutoHeight = false;
            this.repositoryItemSpinEditNumberOfTrees.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditNumberOfTrees.IsFloatValue = false;
            this.repositoryItemSpinEditNumberOfTrees.Mask.EditMask = "N00";
            this.repositoryItemSpinEditNumberOfTrees.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.repositoryItemSpinEditNumberOfTrees.Name = "repositoryItemSpinEditNumberOfTrees";
            // 
            // colPercentage
            // 
            this.colPercentage.Caption = "Percentage";
            this.colPercentage.ColumnEdit = this.repositoryItemSpinEditPercentage;
            this.colPercentage.FieldName = "Percentage";
            this.colPercentage.Name = "colPercentage";
            this.colPercentage.Visible = true;
            this.colPercentage.VisibleIndex = 2;
            this.colPercentage.Width = 76;
            // 
            // repositoryItemSpinEditPercentage
            // 
            this.repositoryItemSpinEditPercentage.AutoHeight = false;
            this.repositoryItemSpinEditPercentage.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditPercentage.Mask.EditMask = "P";
            this.repositoryItemSpinEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditPercentage.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.repositoryItemSpinEditPercentage.Name = "repositoryItemSpinEditPercentage";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 177;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode
            // 
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // TreeIDTextEdit
            // 
            this.TreeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "TreeID", true));
            this.TreeIDTextEdit.Location = new System.Drawing.Point(118, -9);
            this.TreeIDTextEdit.MenuManager = this.barManager1;
            this.TreeIDTextEdit.Name = "TreeIDTextEdit";
            this.TreeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TreeIDTextEdit, true);
            this.TreeIDTextEdit.Size = new System.Drawing.Size(481, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TreeIDTextEdit, optionsSpelling3);
            this.TreeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TreeIDTextEdit.TabIndex = 46;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(118, 165);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(778, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling4);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 37;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(118, 141);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(778, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling5);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 36;
            // 
            // PoleIDTextEdit
            // 
            this.PoleIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "PoleID", true));
            this.PoleIDTextEdit.Location = new System.Drawing.Point(118, 189);
            this.PoleIDTextEdit.MenuManager = this.barManager1;
            this.PoleIDTextEdit.Name = "PoleIDTextEdit";
            this.PoleIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PoleIDTextEdit, true);
            this.PoleIDTextEdit.Size = new System.Drawing.Size(778, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PoleIDTextEdit, optionsSpelling6);
            this.PoleIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PoleIDTextEdit.TabIndex = 31;
            // 
            // LengthTextEdit
            // 
            this.LengthTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Length", true));
            this.LengthTextEdit.Location = new System.Drawing.Point(546, 427);
            this.LengthTextEdit.MenuManager = this.barManager1;
            this.LengthTextEdit.Name = "LengthTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LengthTextEdit, true);
            this.LengthTextEdit.Size = new System.Drawing.Size(322, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LengthTextEdit, optionsSpelling7);
            this.LengthTextEdit.StyleController = this.dataLayoutControl1;
            this.LengthTextEdit.TabIndex = 50;
            // 
            // AreaTextEdit
            // 
            this.AreaTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Area", true));
            this.AreaTextEdit.Location = new System.Drawing.Point(121, 427);
            this.AreaTextEdit.MenuManager = this.barManager1;
            this.AreaTextEdit.Name = "AreaTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AreaTextEdit, true);
            this.AreaTextEdit.Size = new System.Drawing.Size(324, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AreaTextEdit, optionsSpelling8);
            this.AreaTextEdit.StyleController = this.dataLayoutControl1;
            this.AreaTextEdit.TabIndex = 49;
            // 
            // LatLongPairsMemoEdit
            // 
            this.LatLongPairsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "LatLongPairs", true));
            this.LatLongPairsMemoEdit.Location = new System.Drawing.Point(546, 370);
            this.LatLongPairsMemoEdit.MenuManager = this.barManager1;
            this.LatLongPairsMemoEdit.Name = "LatLongPairsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatLongPairsMemoEdit, true);
            this.LatLongPairsMemoEdit.Size = new System.Drawing.Size(322, 53);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatLongPairsMemoEdit, optionsSpelling9);
            this.LatLongPairsMemoEdit.StyleController = this.dataLayoutControl1;
            this.LatLongPairsMemoEdit.TabIndex = 48;
            // 
            // XYPairsMemoEdit
            // 
            this.XYPairsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "XYPairs", true));
            this.XYPairsMemoEdit.Location = new System.Drawing.Point(121, 370);
            this.XYPairsMemoEdit.MenuManager = this.barManager1;
            this.XYPairsMemoEdit.Name = "XYPairsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.XYPairsMemoEdit, true);
            this.XYPairsMemoEdit.Size = new System.Drawing.Size(324, 53);
            this.scSpellChecker.SetSpellCheckerOptions(this.XYPairsMemoEdit, optionsSpelling10);
            this.XYPairsMemoEdit.StyleController = this.dataLayoutControl1;
            this.XYPairsMemoEdit.TabIndex = 47;
            // 
            // MapIDTextEdit
            // 
            this.MapIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "MapID", true));
            this.MapIDTextEdit.Location = new System.Drawing.Point(121, 298);
            this.MapIDTextEdit.MenuManager = this.barManager1;
            this.MapIDTextEdit.Name = "MapIDTextEdit";
            this.MapIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MapIDTextEdit, true);
            this.MapIDTextEdit.Size = new System.Drawing.Size(747, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MapIDTextEdit, optionsSpelling11);
            this.MapIDTextEdit.StyleController = this.dataLayoutControl1;
            this.MapIDTextEdit.TabIndex = 45;
            // 
            // LongitudeTextEdit
            // 
            this.LongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Longitude", true));
            this.LongitudeTextEdit.Location = new System.Drawing.Point(546, 346);
            this.LongitudeTextEdit.MenuManager = this.barManager1;
            this.LongitudeTextEdit.Name = "LongitudeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LongitudeTextEdit, true);
            this.LongitudeTextEdit.Size = new System.Drawing.Size(322, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LongitudeTextEdit, optionsSpelling12);
            this.LongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeTextEdit.TabIndex = 42;
            // 
            // LatitudeTextEdit
            // 
            this.LatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Latitude", true));
            this.LatitudeTextEdit.Location = new System.Drawing.Point(546, 322);
            this.LatitudeTextEdit.MenuManager = this.barManager1;
            this.LatitudeTextEdit.Name = "LatitudeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatitudeTextEdit, true);
            this.LatitudeTextEdit.Size = new System.Drawing.Size(322, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatitudeTextEdit, optionsSpelling13);
            this.LatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeTextEdit.TabIndex = 41;
            // 
            // XTextEdit
            // 
            this.XTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "X", true));
            this.XTextEdit.Location = new System.Drawing.Point(121, 322);
            this.XTextEdit.MenuManager = this.barManager1;
            this.XTextEdit.Name = "XTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.XTextEdit, true);
            this.XTextEdit.Size = new System.Drawing.Size(324, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.XTextEdit, optionsSpelling14);
            this.XTextEdit.StyleController = this.dataLayoutControl1;
            this.XTextEdit.TabIndex = 40;
            // 
            // YTextEdit
            // 
            this.YTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Y", true));
            this.YTextEdit.Location = new System.Drawing.Point(121, 346);
            this.YTextEdit.MenuManager = this.barManager1;
            this.YTextEdit.Name = "YTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.YTextEdit, true);
            this.YTextEdit.Size = new System.Drawing.Size(324, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.YTextEdit, optionsSpelling15);
            this.YTextEdit.StyleController = this.dataLayoutControl1;
            this.YTextEdit.TabIndex = 39;
            // 
            // SizeBandIDGridLookUpEdit
            // 
            this.SizeBandIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "SizeBandID", true));
            this.SizeBandIDGridLookUpEdit.Location = new System.Drawing.Point(109, 131);
            this.SizeBandIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SizeBandIDGridLookUpEdit.Name = "SizeBandIDGridLookUpEdit";
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.SizeBandIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SizeBandIDGridLookUpEdit.Properties.DataSource = this.sp07058UTSizeBandWithBlankBindingSource;
            this.SizeBandIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SizeBandIDGridLookUpEdit.Properties.NullText = "";
            this.SizeBandIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.SizeBandIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SizeBandIDGridLookUpEdit.Size = new System.Drawing.Size(787, 22);
            this.SizeBandIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SizeBandIDGridLookUpEdit.TabIndex = 4;
            this.SizeBandIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SizeBandIDGridLookUpEdit_ButtonClick);
            this.SizeBandIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SizeBandIDGridLookUpEdit_Validating);
            // 
            // sp07058UTSizeBandWithBlankBindingSource
            // 
            this.sp07058UTSizeBandWithBlankBindingSource.DataMember = "sp07058_UT_Size_Band_With_Blank";
            this.sp07058UTSizeBandWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID2;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Size Band";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.EditValue = "";
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(109, 107);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp07006UTCircuitStatusBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(787, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 3;
            // 
            // sp07006UTCircuitStatusBindingSource
            // 
            this.sp07006UTCircuitStatusBindingSource.DataMember = "sp07006_UT_Circuit_Status";
            this.sp07006UTCircuitStatusBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription1,
            this.colItemOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID1
            // 
            this.colID1.Caption = "Status ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 174;
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07056UTTreeItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(109, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(241, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // CircuitIDTextEdit
            // 
            this.CircuitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "CircuitID", true));
            this.CircuitIDTextEdit.Location = new System.Drawing.Point(118, 93);
            this.CircuitIDTextEdit.MenuManager = this.barManager1;
            this.CircuitIDTextEdit.Name = "CircuitIDTextEdit";
            this.CircuitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitIDTextEdit, true);
            this.CircuitIDTextEdit.Size = new System.Drawing.Size(778, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitIDTextEdit, optionsSpelling16);
            this.CircuitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitIDTextEdit.TabIndex = 4;
            // 
            // CircuitNumberTextEdit
            // 
            this.CircuitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "CircuitNumber", true));
            this.CircuitNumberTextEdit.Location = new System.Drawing.Point(118, 117);
            this.CircuitNumberTextEdit.MenuManager = this.barManager1;
            this.CircuitNumberTextEdit.Name = "CircuitNumberTextEdit";
            this.CircuitNumberTextEdit.Properties.MaxLength = 50;
            this.CircuitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNumberTextEdit, true);
            this.CircuitNumberTextEdit.Size = new System.Drawing.Size(778, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNumberTextEdit, optionsSpelling17);
            this.CircuitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNumberTextEdit.TabIndex = 6;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(24, 298);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(844, 149);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling18);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // PoleNameButtonEdit
            // 
            this.PoleNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "PoleName", true));
            this.PoleNameButtonEdit.EditValue = "";
            this.PoleNameButtonEdit.Location = new System.Drawing.Point(109, 35);
            this.PoleNameButtonEdit.MenuManager = this.barManager1;
            this.PoleNameButtonEdit.Name = "PoleNameButtonEdit";
            this.PoleNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PoleNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PoleNameButtonEdit.Size = new System.Drawing.Size(787, 20);
            this.PoleNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.PoleNameButtonEdit.TabIndex = 0;
            this.PoleNameButtonEdit.TabStop = false;
            this.PoleNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PoleNameButtonEdit_ButtonClick);
            this.PoleNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PoleNameButtonEdit_Validating);
            // 
            // ReferenceNumberButtonEdit
            // 
            this.ReferenceNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07056UTTreeItemBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberButtonEdit.Location = new System.Drawing.Point(109, 59);
            this.ReferenceNumberButtonEdit.MenuManager = this.barManager1;
            this.ReferenceNumberButtonEdit.Name = "ReferenceNumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem4.Text = "Sequence button - information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem5.Text = "Number button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.ReferenceNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "Sequence", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "Number", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ReferenceNumberButtonEdit.Properties.MaxLength = 50;
            this.ReferenceNumberButtonEdit.Size = new System.Drawing.Size(787, 20);
            this.ReferenceNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberButtonEdit.TabIndex = 1;
            this.ReferenceNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ReferenceNumberButtonEdit_ButtonClick);
            this.ReferenceNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ReferenceNumberButtonEdit_Validating);
            // 
            // ItemForTreeID
            // 
            this.ItemForTreeID.Control = this.TreeIDTextEdit;
            this.ItemForTreeID.CustomizationFormText = "Tree ID:";
            this.ItemForTreeID.Location = new System.Drawing.Point(0, 81);
            this.ItemForTreeID.Name = "ItemForTreeID";
            this.ItemForTreeID.Size = new System.Drawing.Size(591, 24);
            this.ItemForTreeID.Text = "Tree ID:";
            this.ItemForTreeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPoleID
            // 
            this.ItemForPoleID.Control = this.PoleIDTextEdit;
            this.ItemForPoleID.CustomizationFormText = "Pole ID:";
            this.ItemForPoleID.Location = new System.Drawing.Point(0, 177);
            this.ItemForPoleID.Name = "ItemForPoleID";
            this.ItemForPoleID.Size = new System.Drawing.Size(888, 24);
            this.ItemForPoleID.Text = "Pole ID:";
            this.ItemForPoleID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 153);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(888, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 129);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(888, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCircuitNumber
            // 
            this.ItemForCircuitNumber.Control = this.CircuitNumberTextEdit;
            this.ItemForCircuitNumber.CustomizationFormText = "Circuit Number:";
            this.ItemForCircuitNumber.Location = new System.Drawing.Point(0, 105);
            this.ItemForCircuitNumber.Name = "ItemForCircuitNumber";
            this.ItemForCircuitNumber.Size = new System.Drawing.Size(888, 24);
            this.ItemForCircuitNumber.Text = "Circuit Number:";
            this.ItemForCircuitNumber.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForCircuitID
            // 
            this.ItemForCircuitID.Control = this.CircuitIDTextEdit;
            this.ItemForCircuitID.CustomizationFormText = "Circuit ID:";
            this.ItemForCircuitID.Location = new System.Drawing.Point(0, 81);
            this.ItemForCircuitID.Name = "ItemForCircuitID";
            this.ItemForCircuitID.Size = new System.Drawing.Size(888, 24);
            this.ItemForCircuitID.Text = "Circuit ID:";
            this.ItemForCircuitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 240);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(888, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(908, 627);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPoleName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForReferenceNumber,
            this.emptySpaceItem3,
            this.layoutControlGroupSpecies,
            this.ItemForStatusID,
            this.ItemForSizeBandID,
            this.ItemForTypeID,
            this.ItemForTPONumber,
            this.ItemForG55CategoryID,
            this.ItemForGrowthRateID,
            this.ItemForDeadTree});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(888, 607);
            // 
            // ItemForPoleName
            // 
            this.ItemForPoleName.AllowHide = false;
            this.ItemForPoleName.Control = this.PoleNameButtonEdit;
            this.ItemForPoleName.CustomizationFormText = "Linked to Circuit:";
            this.ItemForPoleName.Location = new System.Drawing.Point(0, 23);
            this.ItemForPoleName.Name = "ItemForPoleName";
            this.ItemForPoleName.Size = new System.Drawing.Size(888, 24);
            this.ItemForPoleName.Text = "Linked to Pole:";
            this.ItemForPoleName.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(97, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(97, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(97, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(342, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(546, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(97, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 240);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(888, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.Expanded = false;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 250);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(888, 31);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(872, 201);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Mapping";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForX,
            this.ItemForY,
            this.ItemForMapID,
            this.ItemForLatitude,
            this.ItemForLongitude,
            this.ItemForLatLongPairs,
            this.ItemForArea,
            this.ItemForLength,
            this.ItemForXYPairs});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(848, 153);
            this.layoutControlGroup7.Text = "Mapping";
            // 
            // ItemForX
            // 
            this.ItemForX.Control = this.XTextEdit;
            this.ItemForX.CustomizationFormText = "X Coordinate:";
            this.ItemForX.Location = new System.Drawing.Point(0, 24);
            this.ItemForX.Name = "ItemForX";
            this.ItemForX.Size = new System.Drawing.Size(425, 24);
            this.ItemForX.Text = "X Coordinate:";
            this.ItemForX.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForY
            // 
            this.ItemForY.Control = this.YTextEdit;
            this.ItemForY.CustomizationFormText = "Y Coordinate:";
            this.ItemForY.Location = new System.Drawing.Point(0, 48);
            this.ItemForY.Name = "ItemForY";
            this.ItemForY.Size = new System.Drawing.Size(425, 24);
            this.ItemForY.Text = "Y Coordinate:";
            this.ItemForY.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForMapID
            // 
            this.ItemForMapID.Control = this.MapIDTextEdit;
            this.ItemForMapID.CustomizationFormText = "Map ID:";
            this.ItemForMapID.Location = new System.Drawing.Point(0, 0);
            this.ItemForMapID.Name = "ItemForMapID";
            this.ItemForMapID.Size = new System.Drawing.Size(848, 24);
            this.ItemForMapID.Text = "Map ID:";
            this.ItemForMapID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeTextEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(425, 24);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(423, 24);
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeTextEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(425, 48);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(423, 24);
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForLatLongPairs
            // 
            this.ItemForLatLongPairs.Control = this.LatLongPairsMemoEdit;
            this.ItemForLatLongPairs.CustomizationFormText = "Lat \\ Long Pairs:";
            this.ItemForLatLongPairs.Location = new System.Drawing.Point(425, 72);
            this.ItemForLatLongPairs.MaxSize = new System.Drawing.Size(0, 57);
            this.ItemForLatLongPairs.MinSize = new System.Drawing.Size(111, 57);
            this.ItemForLatLongPairs.Name = "ItemForLatLongPairs";
            this.ItemForLatLongPairs.Size = new System.Drawing.Size(423, 57);
            this.ItemForLatLongPairs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLatLongPairs.Text = "Lat \\ Long Pairs:";
            this.ItemForLatLongPairs.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForArea
            // 
            this.ItemForArea.Control = this.AreaTextEdit;
            this.ItemForArea.CustomizationFormText = "Area:";
            this.ItemForArea.Location = new System.Drawing.Point(0, 129);
            this.ItemForArea.Name = "ItemForArea";
            this.ItemForArea.Size = new System.Drawing.Size(425, 24);
            this.ItemForArea.Text = "Area:";
            this.ItemForArea.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForLength
            // 
            this.ItemForLength.Control = this.LengthTextEdit;
            this.ItemForLength.CustomizationFormText = "Length:";
            this.ItemForLength.Location = new System.Drawing.Point(425, 129);
            this.ItemForLength.Name = "ItemForLength";
            this.ItemForLength.Size = new System.Drawing.Size(423, 24);
            this.ItemForLength.Text = "Length:";
            this.ItemForLength.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForXYPairs
            // 
            this.ItemForXYPairs.Control = this.XYPairsMemoEdit;
            this.ItemForXYPairs.CustomizationFormText = "X \\ Y Pairs:";
            this.ItemForXYPairs.Location = new System.Drawing.Point(0, 72);
            this.ItemForXYPairs.MaxSize = new System.Drawing.Size(0, 57);
            this.ItemForXYPairs.MinSize = new System.Drawing.Size(111, 57);
            this.ItemForXYPairs.Name = "ItemForXYPairs";
            this.ItemForXYPairs.Size = new System.Drawing.Size(425, 57);
            this.ItemForXYPairs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForXYPairs.Text = "X \\ Y Pairs:";
            this.ItemForXYPairs.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(848, 153);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(848, 153);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.AllowHide = false;
            this.ItemForReferenceNumber.Control = this.ReferenceNumberButtonEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(0, 47);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(888, 24);
            this.ItemForReferenceNumber.Text = "Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 281);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(888, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupSpecies
            // 
            this.layoutControlGroupSpecies.CustomizationFormText = "Linked Species";
            this.layoutControlGroupSpecies.ExpandButtonVisible = true;
            this.layoutControlGroupSpecies.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupSpecies.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroupSpecies.Location = new System.Drawing.Point(0, 291);
            this.layoutControlGroupSpecies.Name = "layoutControlGroupSpecies";
            this.layoutControlGroupSpecies.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroupSpecies.Size = new System.Drawing.Size(888, 316);
            this.layoutControlGroupSpecies.Text = "Linked Species";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Species:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(872, 278);
            this.layoutControlItem2.Text = "Linked Species:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 95);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(888, 24);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForSizeBandID
            // 
            this.ItemForSizeBandID.AllowHide = false;
            this.ItemForSizeBandID.Control = this.SizeBandIDGridLookUpEdit;
            this.ItemForSizeBandID.CustomizationFormText = "Size Band:";
            this.ItemForSizeBandID.Location = new System.Drawing.Point(0, 119);
            this.ItemForSizeBandID.Name = "ItemForSizeBandID";
            this.ItemForSizeBandID.Size = new System.Drawing.Size(888, 26);
            this.ItemForSizeBandID.Text = "Size Band:";
            this.ItemForSizeBandID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForTypeID
            // 
            this.ItemForTypeID.Control = this.TypeIDGridLookUpEdit;
            this.ItemForTypeID.CustomizationFormText = "Tree Type:";
            this.ItemForTypeID.Location = new System.Drawing.Point(0, 71);
            this.ItemForTypeID.Name = "ItemForTypeID";
            this.ItemForTypeID.Size = new System.Drawing.Size(888, 24);
            this.ItemForTypeID.Text = "Tree Type:";
            this.ItemForTypeID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForTPONumber
            // 
            this.ItemForTPONumber.Control = this.TPONumberTextEdit;
            this.ItemForTPONumber.CustomizationFormText = "TPO Number:";
            this.ItemForTPONumber.Location = new System.Drawing.Point(0, 169);
            this.ItemForTPONumber.Name = "ItemForTPONumber";
            this.ItemForTPONumber.Size = new System.Drawing.Size(888, 24);
            this.ItemForTPONumber.Text = "TPO Number:";
            this.ItemForTPONumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForG55CategoryID
            // 
            this.ItemForG55CategoryID.Control = this.G55CategoryIDGridLookUpEdit;
            this.ItemForG55CategoryID.CustomizationFormText = "G55/2 Category:";
            this.ItemForG55CategoryID.Location = new System.Drawing.Point(0, 193);
            this.ItemForG55CategoryID.Name = "ItemForG55CategoryID";
            this.ItemForG55CategoryID.Size = new System.Drawing.Size(888, 24);
            this.ItemForG55CategoryID.Text = "G55/2 Category:";
            this.ItemForG55CategoryID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForGrowthRateID
            // 
            this.ItemForGrowthRateID.AllowHide = false;
            this.ItemForGrowthRateID.Control = this.GrowthRateIDGridLookUpEdit;
            this.ItemForGrowthRateID.CustomizationFormText = "Growth Rate:";
            this.ItemForGrowthRateID.Location = new System.Drawing.Point(0, 145);
            this.ItemForGrowthRateID.Name = "ItemForGrowthRateID";
            this.ItemForGrowthRateID.Size = new System.Drawing.Size(888, 24);
            this.ItemForGrowthRateID.Text = "Growth Rate:";
            this.ItemForGrowthRateID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForDeadTree
            // 
            this.ItemForDeadTree.Control = this.DeadTreeCheckEdit;
            this.ItemForDeadTree.CustomizationFormText = "Dead Tree:";
            this.ItemForDeadTree.Location = new System.Drawing.Point(0, 217);
            this.ItemForDeadTree.Name = "ItemForDeadTree";
            this.ItemForDeadTree.Size = new System.Drawing.Size(888, 23);
            this.ItemForDeadTree.Text = "Dead Tree:";
            this.ItemForDeadTree.TextSize = new System.Drawing.Size(94, 13);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07006_UT_Circuit_StatusTableAdapter
            // 
            this.sp07006_UT_Circuit_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp07056_UT_Tree_ItemTableAdapter
            // 
            this.sp07056_UT_Tree_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07058_UT_Size_Band_With_BlankTableAdapter
            // 
            this.sp07058_UT_Size_Band_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07059_UT_Tree_Object_Types_With_BlankTableAdapter
            // 
            this.sp07059_UT_Tree_Object_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp07062_UT_Species_List_With_BlankTableAdapter
            // 
            this.sp07062_UT_Species_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07186_UT_G55_Categories_With_BlankTableAdapter
            // 
            this.sp07186_UT_G55_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07406_UT_Growth_Rates_With_BlankTableAdapter
            // 
            this.sp07406_UT_Growth_Rates_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Tree_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(908, 683);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Tree_Edit";
            this.Text = "Edit Tree";
            this.Activated += new System.EventHandler(this.frm_UT_Tree_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Tree_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Tree_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07056UTTreeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeadTreeCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthRateIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07406UTGrowthRatesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrowthRateIDGridLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.G55CategoryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07186UTG55CategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07059UTTreeObjectTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSpeciesID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07062UTSpeciesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditNumberOfTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LengthTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AreaTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongPairsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XYPairsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SizeBandIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07058UTSizeBandWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongPairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForXYPairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSpecies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSizeBandID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForG55CategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGrowthRateID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeadTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit CircuitIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit CircuitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit PoleNameButtonEdit;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07006UTCircuitStatusBindingSource;
        private DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter sp07006_UT_Circuit_StatusTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraEditors.GridLookUpEdit SizeBandIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSizeBandID;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.TextEdit PoleIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleID;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit YTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForY;
        private DevExpress.XtraEditors.TextEdit XTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForX;
        private DevExpress.XtraEditors.TextEdit LatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraEditors.TextEdit LongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraEditors.TextEdit MapIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMapID;
        private System.Windows.Forms.BindingSource sp07056UTTreeItemBindingSource;
        private DataSet_UT_EditTableAdapters.sp07056_UT_Tree_ItemTableAdapter sp07056_UT_Tree_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit TreeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeID;
        private DevExpress.XtraEditors.MemoEdit LatLongPairsMemoEdit;
        private DevExpress.XtraEditors.MemoEdit XYPairsMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForXYPairs;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatLongPairs;
        private DevExpress.XtraEditors.TextEdit AreaTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArea;
        private DevExpress.XtraEditors.TextEdit LengthTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLength;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSpecies;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.BindingSource sp07058UTSizeBandWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07058_UT_Size_Band_With_BlankTableAdapter sp07058_UT_Size_Band_With_BlankTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit TypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTypeID;
        private System.Windows.Forms.BindingSource sp07059UTTreeObjectTypesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07059_UT_Tree_Object_Types_With_BlankTableAdapter sp07059_UT_Tree_Object_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private System.Windows.Forms.BindingSource sp07060UTTreeSpeciesLinkedToTreeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditSpeciesID;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfTrees;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditNumberOfTrees;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private System.Windows.Forms.BindingSource sp07062UTSpeciesListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07062_UT_Species_List_With_BlankTableAdapter sp07062_UT_Species_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colScientificName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.ButtonEdit ReferenceNumberButtonEdit;
        private DevExpress.XtraEditors.TextEdit TPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTPONumber;
        private DevExpress.XtraEditors.GridLookUpEdit G55CategoryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForG55CategoryID;
        private System.Windows.Forms.BindingSource sp07186UTG55CategoriesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07186_UT_G55_Categories_With_BlankTableAdapter sp07186_UT_G55_Categories_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder2;
        private DevExpress.XtraEditors.GridLookUpEdit GrowthRateIDGridLookUpEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGrowthRateID;
        private DevExpress.XtraGrid.Views.Grid.GridView GrowthRateIDGridLookUpEditView;
        private DevExpress.XtraEditors.CheckEdit DeadTreeCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeadTree;
        private System.Windows.Forms.BindingSource sp07406UTGrowthRatesWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07406_UT_Growth_Rates_With_BlankTableAdapter sp07406_UT_Growth_Rates_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

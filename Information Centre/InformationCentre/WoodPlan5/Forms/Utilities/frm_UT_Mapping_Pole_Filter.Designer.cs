﻿namespace WoodPlan5
{
    partial class frm_UT_Mapping_Pole_Filter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Mapping_Pole_Filter));
            this.popupContainerEdit1 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlClients = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07001UTClientFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit2 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlRegions = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07036UTRegionPopupFilteredByClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRegionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRegionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit3 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlSubAreas = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSubAreaFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit4 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlPrimarys = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPrimaryFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerEdit5 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlFeeders = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnFeederFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.popupContainerEdit6 = new DevExpress.XtraEditors.PopupContainerEdit();
            this.popupContainerControlCircuits = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCircuitFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefreshCircuits = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.sp07001_UT_Client_FilterTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter();
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter();
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter();
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter();
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter();
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).BeginInit();
            this.popupContainerControlClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).BeginInit();
            this.popupContainerControlRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).BeginInit();
            this.popupContainerControlSubAreas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).BeginInit();
            this.popupContainerControlPrimarys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).BeginInit();
            this.popupContainerControlFeeders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).BeginInit();
            this.popupContainerControlCircuits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(821, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 529);
            this.barDockControlBottom.Size = new System.Drawing.Size(821, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 503);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(821, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 503);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.EditValue = "No Client Filter";
            this.popupContainerEdit1.Location = new System.Drawing.Point(95, 33);
            this.popupContainerEdit1.MenuManager = this.barManager1;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            this.popupContainerEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit1.Properties.PopupControl = this.popupContainerControlClients;
            this.popupContainerEdit1.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit1.TabIndex = 4;
            this.popupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // popupContainerControlClients
            // 
            this.popupContainerControlClients.Controls.Add(this.gridControl2);
            this.popupContainerControlClients.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControlClients.Location = new System.Drawing.Point(12, 364);
            this.popupContainerControlClients.Name = "popupContainerControlClients";
            this.popupContainerControlClients.Size = new System.Drawing.Size(127, 154);
            this.popupContainerControlClients.TabIndex = 29;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07001UTClientFilterBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 1);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(121, 123);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07001UTClientFilterBindingSource
            // 
            this.sp07001UTClientFilterBindingSource.DataMember = "sp07001_UT_Client_Filter";
            this.sp07001UTClientFilterBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 209;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 1;
            this.colClientTypeDescription.Width = 111;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 102;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 128);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 2;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.EditValue = "No Region  Filter";
            this.popupContainerEdit2.Location = new System.Drawing.Point(95, 75);
            this.popupContainerEdit2.MenuManager = this.barManager1;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            this.popupContainerEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit2.Properties.PopupControl = this.popupContainerControlRegions;
            this.popupContainerEdit2.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit2.TabIndex = 5;
            this.popupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControlRegions
            // 
            this.popupContainerControlRegions.Controls.Add(this.gridControl4);
            this.popupContainerControlRegions.Controls.Add(this.btnRegionFilterOK);
            this.popupContainerControlRegions.Location = new System.Drawing.Point(145, 367);
            this.popupContainerControlRegions.Name = "popupContainerControlRegions";
            this.popupContainerControlRegions.Size = new System.Drawing.Size(120, 152);
            this.popupContainerControlRegions.TabIndex = 30;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp07036UTRegionPopupFilteredByClientBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 1);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(114, 121);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07036UTRegionPopupFilteredByClientBindingSource
            // 
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataMember = "sp07036_UT_Region_Popup_Filtered_By_Client";
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colRegionID1,
            this.colRegionName1,
            this.colRegionNumber});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView4_CustomDrawEmptyForeground);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Name";
            this.gridColumn2.FieldName = "ClientName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 209;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Client Code";
            this.gridColumn3.FieldName = "ClientCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Client Type ID";
            this.gridColumn4.FieldName = "ClientTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client Type";
            this.gridColumn5.FieldName = "ClientTypeDescription";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Width = 111;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Remarks";
            this.gridColumn6.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn6.FieldName = "Remarks";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 102;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRegionID1
            // 
            this.colRegionID1.Caption = "Region ID";
            this.colRegionID1.FieldName = "RegionID";
            this.colRegionID1.Name = "colRegionID1";
            this.colRegionID1.OptionsColumn.AllowEdit = false;
            this.colRegionID1.OptionsColumn.AllowFocus = false;
            this.colRegionID1.OptionsColumn.ReadOnly = true;
            this.colRegionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region Name";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 0;
            this.colRegionName1.Width = 206;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region Number";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 1;
            this.colRegionNumber.Width = 97;
            // 
            // btnRegionFilterOK
            // 
            this.btnRegionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRegionFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnRegionFilterOK.Name = "btnRegionFilterOK";
            this.btnRegionFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnRegionFilterOK.TabIndex = 2;
            this.btnRegionFilterOK.Text = "OK";
            this.btnRegionFilterOK.Click += new System.EventHandler(this.btnRegionFilterOK_Click);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.EditValue = "No Sub-Area";
            this.popupContainerEdit3.Location = new System.Drawing.Point(95, 117);
            this.popupContainerEdit3.MenuManager = this.barManager1;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            this.popupContainerEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit3.Properties.PopupControl = this.popupContainerControlSubAreas;
            this.popupContainerEdit3.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit3.TabIndex = 6;
            this.popupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit3_QueryResultValue);
            // 
            // popupContainerControlSubAreas
            // 
            this.popupContainerControlSubAreas.Controls.Add(this.gridControl5);
            this.popupContainerControlSubAreas.Controls.Add(this.btnSubAreaFilterOK);
            this.popupContainerControlSubAreas.Location = new System.Drawing.Point(282, 366);
            this.popupContainerControlSubAreas.Name = "popupContainerControlSubAreas";
            this.popupContainerControlSubAreas.Size = new System.Drawing.Size(119, 151);
            this.popupContainerControlSubAreas.TabIndex = 31;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp07037UTSubAreaPopupFilteredByRegionBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 1);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5});
            this.gridControl5.Size = new System.Drawing.Size(113, 120);
            this.gridControl5.TabIndex = 3;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07037UTSubAreaPopupFilteredByRegionBindingSource
            // 
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataMember = "sp07037_UT_SubArea_Popup_Filtered_By_Region";
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colSubAreaID1,
            this.colSubAreaName1,
            this.colSubAreaNumber});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView5_CustomDrawEmptyForeground);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client ID";
            this.gridColumn7.FieldName = "ClientID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Client Name";
            this.gridColumn8.FieldName = "ClientName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 209;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Client Code";
            this.gridColumn9.FieldName = "ClientCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Type ID";
            this.gridColumn10.FieldName = "ClientTypeID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Client Type";
            this.gridColumn11.FieldName = "ClientTypeDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Width = 111;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Remarks";
            this.gridColumn12.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn12.FieldName = "Remarks";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 102;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Region ID";
            this.gridColumn13.FieldName = "RegionID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Region Name";
            this.gridColumn14.FieldName = "RegionName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 206;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Region Number";
            this.gridColumn15.FieldName = "RegionNumber";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn15.Width = 161;
            // 
            // colSubAreaID1
            // 
            this.colSubAreaID1.Caption = "Sub-Area ID";
            this.colSubAreaID1.FieldName = "SubAreaID";
            this.colSubAreaID1.Name = "colSubAreaID1";
            this.colSubAreaID1.OptionsColumn.AllowEdit = false;
            this.colSubAreaID1.OptionsColumn.AllowFocus = false;
            this.colSubAreaID1.OptionsColumn.ReadOnly = true;
            this.colSubAreaID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaID1.Width = 80;
            // 
            // colSubAreaName1
            // 
            this.colSubAreaName1.Caption = "Sub-Area Name";
            this.colSubAreaName1.FieldName = "SubAreaName";
            this.colSubAreaName1.Name = "colSubAreaName1";
            this.colSubAreaName1.OptionsColumn.AllowEdit = false;
            this.colSubAreaName1.OptionsColumn.AllowFocus = false;
            this.colSubAreaName1.OptionsColumn.ReadOnly = true;
            this.colSubAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName1.Visible = true;
            this.colSubAreaName1.VisibleIndex = 0;
            this.colSubAreaName1.Width = 199;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area Number ";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 1;
            this.colSubAreaNumber.Width = 121;
            // 
            // btnSubAreaFilterOK
            // 
            this.btnSubAreaFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSubAreaFilterOK.Location = new System.Drawing.Point(3, 125);
            this.btnSubAreaFilterOK.Name = "btnSubAreaFilterOK";
            this.btnSubAreaFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnSubAreaFilterOK.TabIndex = 2;
            this.btnSubAreaFilterOK.Text = "OK";
            this.btnSubAreaFilterOK.Click += new System.EventHandler(this.btnSubAreaFilterOK_Click);
            // 
            // popupContainerEdit4
            // 
            this.popupContainerEdit4.EditValue = "No Primary";
            this.popupContainerEdit4.Location = new System.Drawing.Point(95, 159);
            this.popupContainerEdit4.MenuManager = this.barManager1;
            this.popupContainerEdit4.Name = "popupContainerEdit4";
            this.popupContainerEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit4.Properties.PopupControl = this.popupContainerControlPrimarys;
            this.popupContainerEdit4.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit4.TabIndex = 7;
            this.popupContainerEdit4.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit4_QueryResultValue);
            // 
            // popupContainerControlPrimarys
            // 
            this.popupContainerControlPrimarys.Controls.Add(this.gridControl6);
            this.popupContainerControlPrimarys.Controls.Add(this.btnPrimaryFilterOK);
            this.popupContainerControlPrimarys.Location = new System.Drawing.Point(422, 364);
            this.popupContainerControlPrimarys.Name = "popupContainerControlPrimarys";
            this.popupContainerControlPrimarys.Size = new System.Drawing.Size(119, 150);
            this.popupContainerControlPrimarys.TabIndex = 32;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
            this.gridControl6.Location = new System.Drawing.Point(3, 1);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControl6.Size = new System.Drawing.Size(113, 119);
            this.gridControl6.TabIndex = 3;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07038UTPrimaryPopupFilteredBySubAreaBindingSource
            // 
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataMember = "sp07038_UT_Primary_Popup_Filtered_By_SubArea";
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.colPrimaryID1,
            this.colPrimaryName1,
            this.colPrimaryNumber});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView6_CustomDrawEmptyForeground);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Client ID";
            this.gridColumn16.FieldName = "ClientID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Client Name";
            this.gridColumn17.FieldName = "ClientName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 209;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Client Code";
            this.gridColumn18.FieldName = "ClientCode";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Type ID";
            this.gridColumn19.FieldName = "ClientTypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Client Type";
            this.gridColumn20.FieldName = "ClientTypeDescription";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn20.Width = 111;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Remarks";
            this.gridColumn21.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn21.FieldName = "Remarks";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 2;
            this.gridColumn21.Width = 102;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Region ID";
            this.gridColumn22.FieldName = "RegionID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Region Name";
            this.gridColumn23.FieldName = "RegionName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            this.gridColumn23.Width = 206;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Region Number";
            this.gridColumn24.FieldName = "RegionNumber";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn24.Width = 161;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Sub-Area ID";
            this.gridColumn25.FieldName = "SubAreaID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn25.Width = 80;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Sub-Area Name";
            this.gridColumn26.FieldName = "SubAreaName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 199;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Sub-Area Number ";
            this.gridColumn27.FieldName = "SubAreaNumber";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn27.Width = 121;
            // 
            // colPrimaryID1
            // 
            this.colPrimaryID1.Caption = "Primary ID";
            this.colPrimaryID1.FieldName = "PrimaryID";
            this.colPrimaryID1.Name = "colPrimaryID1";
            this.colPrimaryID1.OptionsColumn.AllowEdit = false;
            this.colPrimaryID1.OptionsColumn.AllowFocus = false;
            this.colPrimaryID1.OptionsColumn.ReadOnly = true;
            this.colPrimaryID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPrimaryName1
            // 
            this.colPrimaryName1.Caption = "Primary Name";
            this.colPrimaryName1.FieldName = "PrimaryName";
            this.colPrimaryName1.Name = "colPrimaryName1";
            this.colPrimaryName1.OptionsColumn.AllowEdit = false;
            this.colPrimaryName1.OptionsColumn.AllowFocus = false;
            this.colPrimaryName1.OptionsColumn.ReadOnly = true;
            this.colPrimaryName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName1.Visible = true;
            this.colPrimaryName1.VisibleIndex = 0;
            this.colPrimaryName1.Width = 196;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary Number";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 1;
            this.colPrimaryNumber.Width = 117;
            // 
            // btnPrimaryFilterOK
            // 
            this.btnPrimaryFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrimaryFilterOK.Location = new System.Drawing.Point(3, 124);
            this.btnPrimaryFilterOK.Name = "btnPrimaryFilterOK";
            this.btnPrimaryFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnPrimaryFilterOK.TabIndex = 2;
            this.btnPrimaryFilterOK.Text = "OK";
            this.btnPrimaryFilterOK.Click += new System.EventHandler(this.btnPrimaryFilterOK_Click);
            // 
            // popupContainerEdit5
            // 
            this.popupContainerEdit5.EditValue = "No Feeder";
            this.popupContainerEdit5.Location = new System.Drawing.Point(95, 201);
            this.popupContainerEdit5.MenuManager = this.barManager1;
            this.popupContainerEdit5.Name = "popupContainerEdit5";
            this.popupContainerEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit5.Properties.PopupControl = this.popupContainerControlFeeders;
            this.popupContainerEdit5.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit5.TabIndex = 8;
            this.popupContainerEdit5.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit5_QueryResultValue);
            // 
            // popupContainerControlFeeders
            // 
            this.popupContainerControlFeeders.Controls.Add(this.gridControl7);
            this.popupContainerControlFeeders.Controls.Add(this.btnFeederFilterOK);
            this.popupContainerControlFeeders.Location = new System.Drawing.Point(556, 364);
            this.popupContainerControlFeeders.Name = "popupContainerControlFeeders";
            this.popupContainerControlFeeders.Size = new System.Drawing.Size(117, 149);
            this.popupContainerControlFeeders.TabIndex = 33;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp07039UTFeederPopupFilteredByPrimaryBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 1);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7});
            this.gridControl7.Size = new System.Drawing.Size(111, 118);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07039UTFeederPopupFilteredByPrimaryBindingSource
            // 
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataMember = "sp07039_UT_Feeder_Popup_Filtered_By_Primary";
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.colFeederID1,
            this.colFeederName1,
            this.colFeederNumber});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 4;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn29, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn38, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn41, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView7_CustomDrawEmptyForeground);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client ID";
            this.gridColumn28.FieldName = "ClientID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Client Name";
            this.gridColumn29.FieldName = "ClientName";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 0;
            this.gridColumn29.Width = 209;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Client Code";
            this.gridColumn30.FieldName = "ClientCode";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client Type ID";
            this.gridColumn31.FieldName = "ClientTypeID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Client Type";
            this.gridColumn32.FieldName = "ClientTypeDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn32.Width = 111;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Remarks";
            this.gridColumn33.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn33.FieldName = "Remarks";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 2;
            this.gridColumn33.Width = 102;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Region ID";
            this.gridColumn34.FieldName = "RegionID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Region Name";
            this.gridColumn35.FieldName = "RegionName";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            this.gridColumn35.Width = 206;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Region Number";
            this.gridColumn36.FieldName = "RegionNumber";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn36.Width = 161;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Sub-Area ID";
            this.gridColumn37.FieldName = "SubAreaID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn37.Width = 80;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Sub-Area Name";
            this.gridColumn38.FieldName = "SubAreaName";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            this.gridColumn38.Width = 199;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Sub-Area Number ";
            this.gridColumn39.FieldName = "SubAreaNumber";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn39.Width = 121;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Primary ID";
            this.gridColumn40.FieldName = "PrimaryID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Primary Name";
            this.gridColumn41.FieldName = "PrimaryName";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 0;
            this.gridColumn41.Width = 196;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Primary Number";
            this.gridColumn42.FieldName = "PrimaryNumber";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn42.Width = 117;
            // 
            // colFeederID1
            // 
            this.colFeederID1.Caption = "Feeder ID";
            this.colFeederID1.FieldName = "FeederID";
            this.colFeederID1.Name = "colFeederID1";
            this.colFeederID1.OptionsColumn.AllowEdit = false;
            this.colFeederID1.OptionsColumn.AllowFocus = false;
            this.colFeederID1.OptionsColumn.ReadOnly = true;
            this.colFeederID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFeederName1
            // 
            this.colFeederName1.Caption = "Feeder Name";
            this.colFeederName1.FieldName = "FeederName";
            this.colFeederName1.Name = "colFeederName1";
            this.colFeederName1.OptionsColumn.AllowEdit = false;
            this.colFeederName1.OptionsColumn.AllowFocus = false;
            this.colFeederName1.OptionsColumn.ReadOnly = true;
            this.colFeederName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName1.Visible = true;
            this.colFeederName1.VisibleIndex = 0;
            this.colFeederName1.Width = 205;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder Number";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 1;
            this.colFeederNumber.Width = 123;
            // 
            // btnFeederFilterOK
            // 
            this.btnFeederFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFeederFilterOK.Location = new System.Drawing.Point(3, 123);
            this.btnFeederFilterOK.Name = "btnFeederFilterOK";
            this.btnFeederFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnFeederFilterOK.TabIndex = 2;
            this.btnFeederFilterOK.Text = "OK";
            this.btnFeederFilterOK.Click += new System.EventHandler(this.btnFeederFilterOK_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 36);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(58, 13);
            this.labelControl1.TabIndex = 9;
            this.labelControl1.Text = "Client Filter:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 78);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 10;
            this.labelControl2.Text = "Region Filter:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 120);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 13);
            this.labelControl3.TabIndex = 11;
            this.labelControl3.Text = "Sub-Area Filter:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(13, 162);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(67, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Primary Filter:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(13, 204);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(65, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Feeder Filter:";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(230, 54);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(23, 20);
            this.pictureEdit1.TabIndex = 14;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(230, 96);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Size = new System.Drawing.Size(23, 20);
            this.pictureEdit2.TabIndex = 15;
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(230, 138);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(23, 20);
            this.pictureEdit3.TabIndex = 16;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(230, 180);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(23, 20);
            this.pictureEdit4.TabIndex = 17;
            // 
            // popupContainerEdit6
            // 
            this.popupContainerEdit6.EditValue = "No Circuit Filter";
            this.popupContainerEdit6.Location = new System.Drawing.Point(516, 229);
            this.popupContainerEdit6.MenuManager = this.barManager1;
            this.popupContainerEdit6.Name = "popupContainerEdit6";
            this.popupContainerEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit6.Properties.PopupControl = this.popupContainerControlCircuits;
            this.popupContainerEdit6.Size = new System.Drawing.Size(294, 20);
            this.popupContainerEdit6.TabIndex = 18;
            this.popupContainerEdit6.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit6_QueryResultValue);
            // 
            // popupContainerControlCircuits
            // 
            this.popupContainerControlCircuits.Controls.Add(this.gridControl8);
            this.popupContainerControlCircuits.Controls.Add(this.btnCircuitFilterOK);
            this.popupContainerControlCircuits.Location = new System.Drawing.Point(687, 359);
            this.popupContainerControlCircuits.Name = "popupContainerControlCircuits";
            this.popupContainerControlCircuits.Size = new System.Drawing.Size(118, 152);
            this.popupContainerControlCircuits.TabIndex = 34;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp07040UTCircuitPopupFilteredByVariousBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(3, 1);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.Size = new System.Drawing.Size(112, 121);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp07040UTCircuitPopupFilteredByVariousBindingSource
            // 
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataMember = "sp07040_UT_Circuit_Popup_Filtered_By_Various";
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsFind.AlwaysVisible = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn45, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn48, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn49, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView8_CustomDrawEmptyForeground);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Circuit ID";
            this.gridColumn43.FieldName = "CircuitID";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Client ID";
            this.gridColumn44.FieldName = "ClientID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Client Name";
            this.gridColumn45.FieldName = "ClientName";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 0;
            this.gridColumn45.Width = 240;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Client Code";
            this.gridColumn46.FieldName = "ClientCode";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn46.Width = 76;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Status ID";
            this.gridColumn47.FieldName = "StatusID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Circuit Name";
            this.gridColumn48.FieldName = "CircuitName";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 0;
            this.gridColumn48.Width = 212;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Circuit Number";
            this.gridColumn49.FieldName = "CircuitNumber";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 1;
            this.gridColumn49.Width = 144;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Voltage ID";
            this.gridColumn50.FieldName = "VoltageID";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Voltage Type";
            this.gridColumn51.FieldName = "VoltageType";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 2;
            this.gridColumn51.Width = 127;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Feeder";
            this.gridColumn52.FieldName = "FeederName";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 6;
            this.gridColumn52.Width = 136;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Coordinate Pairs";
            this.gridColumn53.FieldName = "CoordinatePairs";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 203;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Lat\\Long Pairs";
            this.gridColumn54.FieldName = "LatLongPairs";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 200;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Remarks";
            this.gridColumn55.FieldName = "Remarks";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 8;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Status";
            this.gridColumn57.FieldName = "Status";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 7;
            this.gridColumn57.Width = 62;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Primary";
            this.gridColumn58.FieldName = "PrimaryName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 5;
            this.gridColumn58.Width = 129;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Region";
            this.gridColumn59.FieldName = "RegionName";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 3;
            this.gridColumn59.Width = 122;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Sub Area";
            this.gridColumn60.FieldName = "SubAreaName";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 4;
            this.gridColumn60.Width = 124;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Feeder ID";
            this.gridColumn61.FieldName = "FeederID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Primary ID";
            this.gridColumn62.FieldName = "PrimaryID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Region ID";
            this.gridColumn63.FieldName = "RegionID";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Sub-Area ID";
            this.gridColumn64.FieldName = "SubAreaID";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // btnCircuitFilterOK
            // 
            this.btnCircuitFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCircuitFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnCircuitFilterOK.Name = "btnCircuitFilterOK";
            this.btnCircuitFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnCircuitFilterOK.TabIndex = 2;
            this.btnCircuitFilterOK.Text = "OK";
            this.btnCircuitFilterOK.Click += new System.EventHandler(this.btnCircuitFilterOK_Click);
            // 
            // btnRefreshCircuits
            // 
            this.btnRefreshCircuits.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshCircuits.Image")));
            this.btnRefreshCircuits.Location = new System.Drawing.Point(516, 205);
            this.btnRefreshCircuits.Name = "btnRefreshCircuits";
            this.btnRefreshCircuits.Size = new System.Drawing.Size(106, 23);
            this.btnRefreshCircuits.TabIndex = 19;
            this.btnRefreshCircuits.Text = "Refresh Circuits";
            this.btnRefreshCircuits.Click += new System.EventHandler(this.btnRefreshCircuits_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(654, 324);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 20;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(735, 324);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.EditValue = ((object)(resources.GetObject("pictureEdit5.EditValue")));
            this.pictureEdit5.Location = new System.Drawing.Point(422, 229);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(23, 20);
            this.pictureEdit5.TabIndex = 22;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.popupContainerEdit1);
            this.groupControl1.Controls.Add(this.popupContainerEdit2);
            this.groupControl1.Controls.Add(this.popupContainerEdit3);
            this.groupControl1.Controls.Add(this.popupContainerEdit4);
            this.groupControl1.Controls.Add(this.popupContainerEdit5);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.pictureEdit4);
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Controls.Add(this.pictureEdit3);
            this.groupControl1.Controls.Add(this.pictureEdit2);
            this.groupControl1.Location = new System.Drawing.Point(12, 112);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(404, 235);
            this.groupControl1.TabIndex = 27;
            this.groupControl1.Text = "Filter Circuit List";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl6);
            this.groupControl2.Location = new System.Drawing.Point(12, 37);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(798, 62);
            this.groupControl2.TabIndex = 28;
            this.groupControl2.Text = "Information";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Location = new System.Drawing.Point(8, 23);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(785, 35);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = resources.GetString("labelControl6.Text");
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(449, 232);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(61, 13);
            this.labelControl7.TabIndex = 18;
            this.labelControl7.Text = "Circuit Filter:";
            // 
            // sp07001_UT_Client_FilterTableAdapter
            // 
            this.sp07001_UT_Client_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter
            // 
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter
            // 
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.ClearBeforeFill = true;
            // 
            // sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter
            // 
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.ClearBeforeFill = true;
            // 
            // sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter
            // 
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.ClearBeforeFill = true;
            // 
            // sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter
            // 
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Mapping_Pole_Filter
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(821, 529);
            this.ControlBox = false;
            this.Controls.Add(this.popupContainerControlCircuits);
            this.Controls.Add(this.popupContainerControlFeeders);
            this.Controls.Add(this.popupContainerControlPrimarys);
            this.Controls.Add(this.popupContainerControlSubAreas);
            this.Controls.Add(this.popupContainerControlRegions);
            this.Controls.Add(this.popupContainerControlClients);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.pictureEdit5);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnRefreshCircuits);
            this.Controls.Add(this.popupContainerEdit6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Mapping_Pole_Filter";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pole Filter - Select Circuits to Filter By";
            this.Load += new System.EventHandler(this.frm_UT_Mapping_Pole_Filter_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.popupContainerEdit6, 0);
            this.Controls.SetChildIndex(this.btnRefreshCircuits, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.pictureEdit5, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.popupContainerControlClients, 0);
            this.Controls.SetChildIndex(this.popupContainerControlRegions, 0);
            this.Controls.SetChildIndex(this.popupContainerControlSubAreas, 0);
            this.Controls.SetChildIndex(this.popupContainerControlPrimarys, 0);
            this.Controls.SetChildIndex(this.popupContainerControlFeeders, 0);
            this.Controls.SetChildIndex(this.popupContainerControlCircuits, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).EndInit();
            this.popupContainerControlClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).EndInit();
            this.popupContainerControlRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).EndInit();
            this.popupContainerControlSubAreas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).EndInit();
            this.popupContainerControlPrimarys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).EndInit();
            this.popupContainerControlFeeders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).EndInit();
            this.popupContainerControlCircuits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit2;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit3;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit4;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit6;
        private DevExpress.XtraEditors.SimpleButton btnRefreshCircuits;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlClients;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRegions;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DevExpress.XtraEditors.SimpleButton btnRegionFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSubAreas;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DevExpress.XtraEditors.SimpleButton btnSubAreaFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlPrimarys;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DevExpress.XtraEditors.SimpleButton btnPrimaryFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlFeeders;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraEditors.SimpleButton btnFeederFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCircuits;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraEditors.SimpleButton btnCircuitFilterOK;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07001UTClientFilterBindingSource;
        private DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter sp07001_UT_Client_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp07036UTRegionPopupFilteredByClientBindingSource;
        private System.Windows.Forms.BindingSource sp07037UTSubAreaPopupFilteredByRegionBindingSource;
        private System.Windows.Forms.BindingSource sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
        private System.Windows.Forms.BindingSource sp07039UTFeederPopupFilteredByPrimaryBindingSource;
        private System.Windows.Forms.BindingSource sp07040UTCircuitPopupFilteredByVariousBindingSource;
        private DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter;
        private DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter;
        private DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter;
        private DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter;
        private DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter;
    }
}

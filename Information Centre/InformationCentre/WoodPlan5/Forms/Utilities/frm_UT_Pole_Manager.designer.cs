namespace WoodPlan5
{
    partial class frm_UT_Pole_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Pole_Manager));
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.popupContainerControlClients = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07001UTClientFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07041UTPoleManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionElapsedDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.popupContainerControlCircuits = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCircuitFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlFeeders = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnFeederFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlPrimarys = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPrimaryFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlSubAreas = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSubAreaFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlRegions = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07036UTRegionPopupFilteredByClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRegionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRegionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp07064UTPoleManagerLinkedTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBandID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXYPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatLongPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DPMetres = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrowthRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeadTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedByStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberOfTrees = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer8 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessIssue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn66 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn67 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn68 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn69 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn70 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn71 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn72 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn73 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn74 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl12 = new DevExpress.XtraGrid.GridControl();
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn77 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn78 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn79 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn80 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn81 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn82 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn83 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn84 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn85 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn86 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn87 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn88 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn89 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn90 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn91 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn92 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn93 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn94 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn95 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn96 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn97 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn98 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn99 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp07001_UT_Client_FilterTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerEdit1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit5 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.popupContainerEdit6 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefreshPoles = new DevExpress.XtraBars.BarButtonItem();
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter();
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter();
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter();
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter();
            this.sp07041_UT_Pole_ManagerTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07041_UT_Pole_ManagerTableAdapter();
            this.sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter();
            this.sp07064_UT_Pole_Manager_Linked_TreesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07064_UT_Pole_Manager_Linked_TreesTableAdapter();
            this.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter();
            this.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter();
            this.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter();
            this.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending9 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending10 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending14 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending11 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending12 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending13 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.bbiAddPolesToNewSurvey = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddPolesToExistingSurvey = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).BeginInit();
            this.popupContainerControlClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07041UTPoleManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).BeginInit();
            this.popupContainerControlCircuits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).BeginInit();
            this.popupContainerControlFeeders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).BeginInit();
            this.popupContainerControlPrimarys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).BeginInit();
            this.popupContainerControlSubAreas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).BeginInit();
            this.popupContainerControlRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07064UTPoleManagerLinkedTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).BeginInit();
            this.gridSplitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            this.bsiAdd.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolesToNewSurvey, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolesToExistingSurvey)});
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1210, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1210, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1210, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.popupContainerEdit1,
            this.popupContainerEdit2,
            this.popupContainerEdit3,
            this.popupContainerEdit4,
            this.popupContainerEdit5,
            this.popupContainerEdit6,
            this.bbiRefreshPoles,
            this.bbiAddPolesToNewSurvey,
            this.bbiAddPolesToExistingSurvey});
            this.barManager1.MaxItemId = 38;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemPopupContainerEdit3,
            this.repositoryItemPopupContainerEdit4,
            this.repositoryItemPopupContainerEdit5,
            this.repositoryItemTextEdit1,
            this.repositoryItemPopupContainerEdit6});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // popupContainerControlClients
            // 
            this.popupContainerControlClients.Controls.Add(this.gridControl2);
            this.popupContainerControlClients.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControlClients.Location = new System.Drawing.Point(12, 123);
            this.popupContainerControlClients.Name = "popupContainerControlClients";
            this.popupContainerControlClients.Size = new System.Drawing.Size(119, 152);
            this.popupContainerControlClients.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07001UTClientFilterBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 1);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(113, 121);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07001UTClientFilterBindingSource
            // 
            this.sp07001UTClientFilterBindingSource.DataMember = "sp07001_UT_Client_Filter";
            this.sp07001UTClientFilterBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 209;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 1;
            this.colClientTypeDescription.Width = 111;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 102;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 2;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07041UTPoleManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(1210, 315);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07041UTPoleManagerBindingSource
            // 
            this.sp07041UTPoleManagerBindingSource.DataMember = "sp07041_UT_Pole_Manager";
            this.sp07041UTPoleManagerBindingSource.DataSource = this.dataSet_UT;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleID,
            this.colCircuitID,
            this.colPoleNumber,
            this.colPoleTypeID,
            this.colStatusID,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colX,
            this.colY,
            this.colLatitude,
            this.colLongitude,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.colRemarks1,
            this.colGUID,
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colCircuitStatus,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colRegionName,
            this.colStatus,
            this.colPrimaryName,
            this.colSubAreaName,
            this.colRegionID,
            this.colSubAreaID,
            this.colPrimaryID,
            this.colFeederID,
            this.colPoleType,
            this.colLastInspectionElapsedDays,
            this.colMapID,
            this.colCreatedByStaffID,
            this.colPoleSubAreaID,
            this.colPoleSubAreaName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitID
            // 
            this.colCircuitID.Caption = "Circuit ID";
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 6;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleTypeID
            // 
            this.colPoleTypeID.Caption = "Pole Type ID";
            this.colPoleTypeID.FieldName = "PoleTypeID";
            this.colPoleTypeID.Name = "colPoleTypeID";
            this.colPoleTypeID.OptionsColumn.AllowEdit = false;
            this.colPoleTypeID.OptionsColumn.AllowFocus = false;
            this.colPoleTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 8;
            this.colLastInspectionDate.Width = 94;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 10;
            this.colInspectionCycle.Width = 55;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Cycle Unit ID";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.Width = 136;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Cycle Unit";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.Visible = true;
            this.colInspectionUnitDesc.VisibleIndex = 11;
            this.colInspectionUnitDesc.Width = 69;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 12;
            this.colNextInspectionDate.Width = 97;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.Visible = true;
            this.colX.VisibleIndex = 19;
            this.colX.Width = 59;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.Visible = true;
            this.colY.VisibleIndex = 20;
            this.colY.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 21;
            this.colLatitude.Width = 66;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 22;
            this.colLongitude.Width = 68;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 14;
            this.colIsTransformer.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.Visible = true;
            this.colTransformerNumber.VisibleIndex = 15;
            this.colTransformerNumber.Width = 120;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 130;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.Visible = true;
            this.colClientCode1.VisibleIndex = 24;
            this.colClientCode1.Width = 76;
            // 
            // colCircuitStatus
            // 
            this.colCircuitStatus.Caption = "Circuit Status";
            this.colCircuitStatus.FieldName = "CircuitStatus";
            this.colCircuitStatus.Name = "colCircuitStatus";
            this.colCircuitStatus.OptionsColumn.AllowEdit = false;
            this.colCircuitStatus.OptionsColumn.AllowFocus = false;
            this.colCircuitStatus.OptionsColumn.ReadOnly = true;
            this.colCircuitStatus.Visible = true;
            this.colCircuitStatus.VisibleIndex = 23;
            this.colCircuitStatus.Width = 82;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 3;
            this.colCircuitName.Width = 130;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 25;
            this.colCircuitNumber.Width = 91;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 16;
            this.colVoltageType.Width = 84;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 5;
            this.colFeederName.Width = 130;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 130;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 17;
            this.colStatus.Width = 63;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 4;
            this.colPrimaryName.Width = 130;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 2;
            this.colSubAreaName.Width = 130;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            // 
            // colSubAreaID
            // 
            this.colSubAreaID.Caption = "Sub-Area ID";
            this.colSubAreaID.FieldName = "SubAreaID";
            this.colSubAreaID.Name = "colSubAreaID";
            this.colSubAreaID.OptionsColumn.AllowEdit = false;
            this.colSubAreaID.OptionsColumn.AllowFocus = false;
            this.colSubAreaID.OptionsColumn.ReadOnly = true;
            this.colSubAreaID.Width = 80;
            // 
            // colPrimaryID
            // 
            this.colPrimaryID.Caption = "Primary ID";
            this.colPrimaryID.FieldName = "PrimaryID";
            this.colPrimaryID.Name = "colPrimaryID";
            this.colPrimaryID.OptionsColumn.AllowEdit = false;
            this.colPrimaryID.OptionsColumn.AllowFocus = false;
            this.colPrimaryID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederID
            // 
            this.colFeederID.Caption = "Feeder ID";
            this.colFeederID.FieldName = "FeederID";
            this.colFeederID.Name = "colFeederID";
            this.colFeederID.OptionsColumn.AllowEdit = false;
            this.colFeederID.OptionsColumn.AllowFocus = false;
            this.colFeederID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.Visible = true;
            this.colPoleType.VisibleIndex = 13;
            // 
            // colLastInspectionElapsedDays
            // 
            this.colLastInspectionElapsedDays.Caption = "Elapsed Days";
            this.colLastInspectionElapsedDays.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.Name = "colLastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays.Visible = true;
            this.colLastInspectionElapsedDays.VisibleIndex = 9;
            this.colLastInspectionElapsedDays.Width = 85;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Width = 240;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colPoleSubAreaID
            // 
            this.colPoleSubAreaID.Caption = "Pole Sub-Area ID";
            this.colPoleSubAreaID.FieldName = "PoleSubAreaID";
            this.colPoleSubAreaID.Name = "colPoleSubAreaID";
            this.colPoleSubAreaID.OptionsColumn.AllowEdit = false;
            this.colPoleSubAreaID.OptionsColumn.AllowFocus = false;
            this.colPoleSubAreaID.OptionsColumn.ReadOnly = true;
            this.colPoleSubAreaID.Width = 101;
            // 
            // colPoleSubAreaName
            // 
            this.colPoleSubAreaName.Caption = "Pole Sub-Area Name";
            this.colPoleSubAreaName.FieldName = "PoleSubAreaName";
            this.colPoleSubAreaName.Name = "colPoleSubAreaName";
            this.colPoleSubAreaName.OptionsColumn.AllowEdit = false;
            this.colPoleSubAreaName.OptionsColumn.AllowFocus = false;
            this.colPoleSubAreaName.OptionsColumn.ReadOnly = true;
            this.colPoleSubAreaName.Visible = true;
            this.colPoleSubAreaName.VisibleIndex = 7;
            this.colPoleSubAreaName.Width = 121;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 33);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl2.Panel2.Text = "Linked Documents";
            this.splitContainerControl2.Size = new System.Drawing.Size(1210, 508);
            this.splitContainerControl2.SplitterPosition = 187;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlCircuits);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlFeeders);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlPrimarys);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlSubAreas);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlRegions);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlClients);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1210, 315);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // popupContainerControlCircuits
            // 
            this.popupContainerControlCircuits.Controls.Add(this.gridControl8);
            this.popupContainerControlCircuits.Controls.Add(this.btnCircuitFilterOK);
            this.popupContainerControlCircuits.Location = new System.Drawing.Point(636, 123);
            this.popupContainerControlCircuits.Name = "popupContainerControlCircuits";
            this.popupContainerControlCircuits.Size = new System.Drawing.Size(118, 152);
            this.popupContainerControlCircuits.TabIndex = 6;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp07040UTCircuitPopupFilteredByVariousBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(3, 1);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.Size = new System.Drawing.Size(112, 121);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp07040UTCircuitPopupFilteredByVariousBindingSource
            // 
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataMember = "sp07040_UT_Circuit_Popup_Filtered_By_Various";
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsFind.AlwaysVisible = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn45, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn49, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView8_CustomDrawEmptyForeground);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Circuit ID";
            this.gridColumn43.FieldName = "CircuitID";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Client ID";
            this.gridColumn44.FieldName = "ClientID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Client Name";
            this.gridColumn45.FieldName = "ClientName";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 0;
            this.gridColumn45.Width = 240;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Client Code";
            this.gridColumn46.FieldName = "ClientCode";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Width = 76;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Status ID";
            this.gridColumn47.FieldName = "StatusID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Circuit Name";
            this.gridColumn48.FieldName = "CircuitName";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 0;
            this.gridColumn48.Width = 212;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Circuit Number";
            this.gridColumn49.FieldName = "CircuitNumber";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 1;
            this.gridColumn49.Width = 144;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Voltage ID";
            this.gridColumn50.FieldName = "VoltageID";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Voltage Type";
            this.gridColumn51.FieldName = "VoltageType";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 2;
            this.gridColumn51.Width = 127;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Feeder";
            this.gridColumn52.FieldName = "FeederName";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 6;
            this.gridColumn52.Width = 136;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Coordinate Pairs";
            this.gridColumn53.FieldName = "CoordinatePairs";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 203;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Lat\\Long Pairs";
            this.gridColumn54.FieldName = "LatLongPairs";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 200;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Remarks";
            this.gridColumn55.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn55.FieldName = "Remarks";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 8;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Status";
            this.gridColumn57.FieldName = "Status";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 7;
            this.gridColumn57.Width = 62;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Primary";
            this.gridColumn58.FieldName = "PrimaryName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 5;
            this.gridColumn58.Width = 129;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Region";
            this.gridColumn59.FieldName = "RegionName";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 3;
            this.gridColumn59.Width = 122;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Sub Area";
            this.gridColumn60.FieldName = "SubAreaName";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 4;
            this.gridColumn60.Width = 124;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Feeder ID";
            this.gridColumn61.FieldName = "FeederID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Primary ID";
            this.gridColumn62.FieldName = "PrimaryID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Region ID";
            this.gridColumn63.FieldName = "RegionID";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Sub-Area ID";
            this.gridColumn64.FieldName = "SubAreaID";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            // 
            // btnCircuitFilterOK
            // 
            this.btnCircuitFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCircuitFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnCircuitFilterOK.Name = "btnCircuitFilterOK";
            this.btnCircuitFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnCircuitFilterOK.TabIndex = 2;
            this.btnCircuitFilterOK.Text = "OK";
            this.btnCircuitFilterOK.Click += new System.EventHandler(this.btnCircuitFilterOK_Click);
            // 
            // popupContainerControlFeeders
            // 
            this.popupContainerControlFeeders.Controls.Add(this.gridControl7);
            this.popupContainerControlFeeders.Controls.Add(this.btnFeederFilterOK);
            this.popupContainerControlFeeders.Location = new System.Drawing.Point(513, 126);
            this.popupContainerControlFeeders.Name = "popupContainerControlFeeders";
            this.popupContainerControlFeeders.Size = new System.Drawing.Size(117, 149);
            this.popupContainerControlFeeders.TabIndex = 5;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp07039UTFeederPopupFilteredByPrimaryBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 1);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7});
            this.gridControl7.Size = new System.Drawing.Size(111, 118);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07039UTFeederPopupFilteredByPrimaryBindingSource
            // 
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataMember = "sp07039_UT_Feeder_Popup_Filtered_By_Primary";
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.colFeederID1,
            this.colFeederName1,
            this.colFeederNumber});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 4;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn29, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn38, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn41, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView7_CustomDrawEmptyForeground);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client ID";
            this.gridColumn28.FieldName = "ClientID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Client Name";
            this.gridColumn29.FieldName = "ClientName";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 0;
            this.gridColumn29.Width = 209;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Client Code";
            this.gridColumn30.FieldName = "ClientCode";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client Type ID";
            this.gridColumn31.FieldName = "ClientTypeID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Client Type";
            this.gridColumn32.FieldName = "ClientTypeDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 111;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Remarks";
            this.gridColumn33.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn33.FieldName = "Remarks";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 2;
            this.gridColumn33.Width = 102;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Region ID";
            this.gridColumn34.FieldName = "RegionID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Region Name";
            this.gridColumn35.FieldName = "RegionName";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            this.gridColumn35.Width = 206;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Region Number";
            this.gridColumn36.FieldName = "RegionNumber";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 161;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Sub-Area ID";
            this.gridColumn37.FieldName = "SubAreaID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Width = 80;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Sub-Area Name";
            this.gridColumn38.FieldName = "SubAreaName";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            this.gridColumn38.Width = 199;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Sub-Area Number ";
            this.gridColumn39.FieldName = "SubAreaNumber";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 121;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Primary ID";
            this.gridColumn40.FieldName = "PrimaryID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Primary Name";
            this.gridColumn41.FieldName = "PrimaryName";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 0;
            this.gridColumn41.Width = 196;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Primary Number";
            this.gridColumn42.FieldName = "PrimaryNumber";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 117;
            // 
            // colFeederID1
            // 
            this.colFeederID1.Caption = "Feeder ID";
            this.colFeederID1.FieldName = "FeederID";
            this.colFeederID1.Name = "colFeederID1";
            this.colFeederID1.OptionsColumn.AllowEdit = false;
            this.colFeederID1.OptionsColumn.AllowFocus = false;
            this.colFeederID1.OptionsColumn.ReadOnly = true;
            // 
            // colFeederName1
            // 
            this.colFeederName1.Caption = "Feeder Name";
            this.colFeederName1.FieldName = "FeederName";
            this.colFeederName1.Name = "colFeederName1";
            this.colFeederName1.OptionsColumn.AllowEdit = false;
            this.colFeederName1.OptionsColumn.AllowFocus = false;
            this.colFeederName1.OptionsColumn.ReadOnly = true;
            this.colFeederName1.Visible = true;
            this.colFeederName1.VisibleIndex = 0;
            this.colFeederName1.Width = 205;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder Number";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 1;
            this.colFeederNumber.Width = 123;
            // 
            // btnFeederFilterOK
            // 
            this.btnFeederFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFeederFilterOK.Location = new System.Drawing.Point(3, 123);
            this.btnFeederFilterOK.Name = "btnFeederFilterOK";
            this.btnFeederFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnFeederFilterOK.TabIndex = 2;
            this.btnFeederFilterOK.Text = "OK";
            this.btnFeederFilterOK.Click += new System.EventHandler(this.btnFeederFilterOK_Click);
            // 
            // popupContainerControlPrimarys
            // 
            this.popupContainerControlPrimarys.Controls.Add(this.gridControl6);
            this.popupContainerControlPrimarys.Controls.Add(this.btnPrimaryFilterOK);
            this.popupContainerControlPrimarys.Location = new System.Drawing.Point(388, 125);
            this.popupContainerControlPrimarys.Name = "popupContainerControlPrimarys";
            this.popupContainerControlPrimarys.Size = new System.Drawing.Size(119, 150);
            this.popupContainerControlPrimarys.TabIndex = 4;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
            this.gridControl6.Location = new System.Drawing.Point(3, 1);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControl6.Size = new System.Drawing.Size(113, 119);
            this.gridControl6.TabIndex = 3;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07038UTPrimaryPopupFilteredBySubAreaBindingSource
            // 
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataMember = "sp07038_UT_Primary_Popup_Filtered_By_SubArea";
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.colPrimaryID1,
            this.colPrimaryName1,
            this.colPrimaryNumber});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView6_CustomDrawEmptyForeground);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Client ID";
            this.gridColumn16.FieldName = "ClientID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Client Name";
            this.gridColumn17.FieldName = "ClientName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 209;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Client Code";
            this.gridColumn18.FieldName = "ClientCode";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Type ID";
            this.gridColumn19.FieldName = "ClientTypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Client Type";
            this.gridColumn20.FieldName = "ClientTypeDescription";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 111;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Remarks";
            this.gridColumn21.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn21.FieldName = "Remarks";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 2;
            this.gridColumn21.Width = 102;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Region ID";
            this.gridColumn22.FieldName = "RegionID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Region Name";
            this.gridColumn23.FieldName = "RegionName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            this.gridColumn23.Width = 206;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Region Number";
            this.gridColumn24.FieldName = "RegionNumber";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 161;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Sub-Area ID";
            this.gridColumn25.FieldName = "SubAreaID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 80;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Sub-Area Name";
            this.gridColumn26.FieldName = "SubAreaName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 199;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Sub-Area Number ";
            this.gridColumn27.FieldName = "SubAreaNumber";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Width = 121;
            // 
            // colPrimaryID1
            // 
            this.colPrimaryID1.Caption = "Primary ID";
            this.colPrimaryID1.FieldName = "PrimaryID";
            this.colPrimaryID1.Name = "colPrimaryID1";
            this.colPrimaryID1.OptionsColumn.AllowEdit = false;
            this.colPrimaryID1.OptionsColumn.AllowFocus = false;
            this.colPrimaryID1.OptionsColumn.ReadOnly = true;
            // 
            // colPrimaryName1
            // 
            this.colPrimaryName1.Caption = "Primary Name";
            this.colPrimaryName1.FieldName = "PrimaryName";
            this.colPrimaryName1.Name = "colPrimaryName1";
            this.colPrimaryName1.OptionsColumn.AllowEdit = false;
            this.colPrimaryName1.OptionsColumn.AllowFocus = false;
            this.colPrimaryName1.OptionsColumn.ReadOnly = true;
            this.colPrimaryName1.Visible = true;
            this.colPrimaryName1.VisibleIndex = 0;
            this.colPrimaryName1.Width = 196;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary Number";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 1;
            this.colPrimaryNumber.Width = 117;
            // 
            // btnPrimaryFilterOK
            // 
            this.btnPrimaryFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrimaryFilterOK.Location = new System.Drawing.Point(3, 124);
            this.btnPrimaryFilterOK.Name = "btnPrimaryFilterOK";
            this.btnPrimaryFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnPrimaryFilterOK.TabIndex = 2;
            this.btnPrimaryFilterOK.Text = "OK";
            this.btnPrimaryFilterOK.Click += new System.EventHandler(this.btnPrimaryFilterOK_Click);
            // 
            // popupContainerControlSubAreas
            // 
            this.popupContainerControlSubAreas.Controls.Add(this.gridControl5);
            this.popupContainerControlSubAreas.Controls.Add(this.btnSubAreaFilterOK);
            this.popupContainerControlSubAreas.Location = new System.Drawing.Point(263, 124);
            this.popupContainerControlSubAreas.Name = "popupContainerControlSubAreas";
            this.popupContainerControlSubAreas.Size = new System.Drawing.Size(119, 151);
            this.popupContainerControlSubAreas.TabIndex = 3;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp07037UTSubAreaPopupFilteredByRegionBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 1);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5});
            this.gridControl5.Size = new System.Drawing.Size(113, 120);
            this.gridControl5.TabIndex = 3;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07037UTSubAreaPopupFilteredByRegionBindingSource
            // 
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataMember = "sp07037_UT_SubArea_Popup_Filtered_By_Region";
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colSubAreaID1,
            this.colSubAreaName1,
            this.colSubAreaNumber});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView5_CustomDrawEmptyForeground);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client ID";
            this.gridColumn7.FieldName = "ClientID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Client Name";
            this.gridColumn8.FieldName = "ClientName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 209;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Client Code";
            this.gridColumn9.FieldName = "ClientCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Type ID";
            this.gridColumn10.FieldName = "ClientTypeID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Client Type";
            this.gridColumn11.FieldName = "ClientTypeDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 111;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Remarks";
            this.gridColumn12.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn12.FieldName = "Remarks";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 102;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Region ID";
            this.gridColumn13.FieldName = "RegionID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Region Name";
            this.gridColumn14.FieldName = "RegionName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 206;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Region Number";
            this.gridColumn15.FieldName = "RegionNumber";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 161;
            // 
            // colSubAreaID1
            // 
            this.colSubAreaID1.Caption = "Sub-Area ID";
            this.colSubAreaID1.FieldName = "SubAreaID";
            this.colSubAreaID1.Name = "colSubAreaID1";
            this.colSubAreaID1.OptionsColumn.AllowEdit = false;
            this.colSubAreaID1.OptionsColumn.AllowFocus = false;
            this.colSubAreaID1.OptionsColumn.ReadOnly = true;
            this.colSubAreaID1.Width = 80;
            // 
            // colSubAreaName1
            // 
            this.colSubAreaName1.Caption = "Sub-Area Name";
            this.colSubAreaName1.FieldName = "SubAreaName";
            this.colSubAreaName1.Name = "colSubAreaName1";
            this.colSubAreaName1.OptionsColumn.AllowEdit = false;
            this.colSubAreaName1.OptionsColumn.AllowFocus = false;
            this.colSubAreaName1.OptionsColumn.ReadOnly = true;
            this.colSubAreaName1.Visible = true;
            this.colSubAreaName1.VisibleIndex = 0;
            this.colSubAreaName1.Width = 199;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area Number ";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 1;
            this.colSubAreaNumber.Width = 121;
            // 
            // btnSubAreaFilterOK
            // 
            this.btnSubAreaFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSubAreaFilterOK.Location = new System.Drawing.Point(3, 125);
            this.btnSubAreaFilterOK.Name = "btnSubAreaFilterOK";
            this.btnSubAreaFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnSubAreaFilterOK.TabIndex = 2;
            this.btnSubAreaFilterOK.Text = "OK";
            this.btnSubAreaFilterOK.Click += new System.EventHandler(this.btnSubAreaFilterOK_Click);
            // 
            // popupContainerControlRegions
            // 
            this.popupContainerControlRegions.Controls.Add(this.gridControl4);
            this.popupContainerControlRegions.Controls.Add(this.btnRegionFilterOK);
            this.popupContainerControlRegions.Location = new System.Drawing.Point(137, 123);
            this.popupContainerControlRegions.Name = "popupContainerControlRegions";
            this.popupContainerControlRegions.Size = new System.Drawing.Size(120, 152);
            this.popupContainerControlRegions.TabIndex = 2;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp07036UTRegionPopupFilteredByClientBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 1);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(114, 121);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07036UTRegionPopupFilteredByClientBindingSource
            // 
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataMember = "sp07036_UT_Region_Popup_Filtered_By_Client";
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colRegionID1,
            this.colRegionName1,
            this.colRegionNumber});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView4_CustomDrawEmptyForeground);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Name";
            this.gridColumn2.FieldName = "ClientName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 209;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Client Code";
            this.gridColumn3.FieldName = "ClientCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Client Type ID";
            this.gridColumn4.FieldName = "ClientTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client Type";
            this.gridColumn5.FieldName = "ClientTypeDescription";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 111;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Remarks";
            this.gridColumn6.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn6.FieldName = "Remarks";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 102;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRegionID1
            // 
            this.colRegionID1.Caption = "Region ID";
            this.colRegionID1.FieldName = "RegionID";
            this.colRegionID1.Name = "colRegionID1";
            this.colRegionID1.OptionsColumn.AllowEdit = false;
            this.colRegionID1.OptionsColumn.AllowFocus = false;
            this.colRegionID1.OptionsColumn.ReadOnly = true;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region Name";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 0;
            this.colRegionName1.Width = 206;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region Number";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 1;
            this.colRegionNumber.Width = 94;
            // 
            // btnRegionFilterOK
            // 
            this.btnRegionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRegionFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnRegionFilterOK.Name = "btnRegionFilterOK";
            this.btnRegionFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnRegionFilterOK.TabIndex = 2;
            this.btnRegionFilterOK.Text = "OK";
            this.btnRegionFilterOK.Click += new System.EventHandler(this.btnRegionFilterOK_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(1206, 183);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2,
            this.xtraTabPage6,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage1});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage2.Text = "Linked Trees";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer4);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Tree Species";
            this.splitContainerControl1.Size = new System.Drawing.Size(1201, 154);
            this.splitContainerControl1.SplitterPosition = 572;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl9;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl9);
            this.gridSplitContainer3.Size = new System.Drawing.Size(572, 154);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp07064UTPoleManagerLinkedTreesBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl9_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEdit2DPMetres,
            this.repositoryItemCheckEdit2});
            this.gridControl9.Size = new System.Drawing.Size(572, 154);
            this.gridControl9.TabIndex = 1;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp07064UTPoleManagerLinkedTreesBindingSource
            // 
            this.sp07064UTPoleManagerLinkedTreesBindingSource.DataMember = "sp07064_UT_Pole_Manager_Linked_Trees";
            this.sp07064UTPoleManagerLinkedTreesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID1,
            this.colPoleID1,
            this.colTypeID,
            this.colReferenceNumber,
            this.colStatusID1,
            this.colSizeBandID,
            this.colX1,
            this.colY1,
            this.colXYPairs,
            this.colLatitude1,
            this.colLongitude1,
            this.colLatLongPairs,
            this.colArea,
            this.colLength,
            this.colRemarks3,
            this.colGUID2,
            this.colMapID1,
            this.colCircuitID1,
            this.colPoleNumber2,
            this.colClientID2,
            this.colCircuitNumber1,
            this.colPoleName,
            this.colClientName2,
            this.colSizeBand,
            this.colStatus1,
            this.colObjectType,
            this.colTPONumber,
            this.colGrowthRate,
            this.colDeadTree,
            this.colCreatedByStaffID1});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 1;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.DoubleClick += new System.EventHandler(this.gridView9_DoubleClick);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // colTreeID1
            // 
            this.colTreeID1.Caption = "Tree ID";
            this.colTreeID1.FieldName = "TreeID";
            this.colTreeID1.Name = "colTreeID1";
            this.colTreeID1.OptionsColumn.AllowEdit = false;
            this.colTreeID1.OptionsColumn.AllowFocus = false;
            this.colTreeID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID1
            // 
            this.colPoleID1.Caption = "Pole ID";
            this.colPoleID1.FieldName = "PoleID";
            this.colPoleID1.Name = "colPoleID1";
            this.colPoleID1.OptionsColumn.AllowEdit = false;
            this.colPoleID1.OptionsColumn.AllowFocus = false;
            this.colPoleID1.OptionsColumn.ReadOnly = true;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 0;
            this.colReferenceNumber.Width = 135;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // colSizeBandID
            // 
            this.colSizeBandID.Caption = "Size Band ID";
            this.colSizeBandID.FieldName = "SizeBandID";
            this.colSizeBandID.Name = "colSizeBandID";
            this.colSizeBandID.OptionsColumn.AllowEdit = false;
            this.colSizeBandID.OptionsColumn.AllowFocus = false;
            this.colSizeBandID.OptionsColumn.ReadOnly = true;
            this.colSizeBandID.Width = 81;
            // 
            // colX1
            // 
            this.colX1.Caption = "X Coordinate";
            this.colX1.FieldName = "X";
            this.colX1.Name = "colX1";
            this.colX1.OptionsColumn.AllowEdit = false;
            this.colX1.OptionsColumn.AllowFocus = false;
            this.colX1.OptionsColumn.ReadOnly = true;
            this.colX1.Width = 83;
            // 
            // colY1
            // 
            this.colY1.Caption = "Y Coordinate";
            this.colY1.FieldName = "Y";
            this.colY1.Name = "colY1";
            this.colY1.OptionsColumn.AllowEdit = false;
            this.colY1.OptionsColumn.AllowFocus = false;
            this.colY1.OptionsColumn.ReadOnly = true;
            this.colY1.Width = 83;
            // 
            // colXYPairs
            // 
            this.colXYPairs.Caption = "X \\ Y Pairs";
            this.colXYPairs.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colXYPairs.FieldName = "XYPairs";
            this.colXYPairs.Name = "colXYPairs";
            this.colXYPairs.OptionsColumn.AllowEdit = false;
            this.colXYPairs.OptionsColumn.AllowFocus = false;
            this.colXYPairs.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colLatitude1
            // 
            this.colLatitude1.Caption = "Latitude";
            this.colLatitude1.FieldName = "Latitude";
            this.colLatitude1.Name = "colLatitude1";
            this.colLatitude1.OptionsColumn.AllowEdit = false;
            this.colLatitude1.OptionsColumn.AllowFocus = false;
            this.colLatitude1.OptionsColumn.ReadOnly = true;
            // 
            // colLongitude1
            // 
            this.colLongitude1.Caption = "Longitude";
            this.colLongitude1.FieldName = "Longitude";
            this.colLongitude1.Name = "colLongitude1";
            this.colLongitude1.OptionsColumn.AllowEdit = false;
            this.colLongitude1.OptionsColumn.AllowFocus = false;
            this.colLongitude1.OptionsColumn.ReadOnly = true;
            // 
            // colLatLongPairs
            // 
            this.colLatLongPairs.Caption = "Lat \\ Long Pairs";
            this.colLatLongPairs.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colLatLongPairs.FieldName = "LatLongPairs";
            this.colLatLongPairs.Name = "colLatLongPairs";
            this.colLatLongPairs.OptionsColumn.AllowEdit = false;
            this.colLatLongPairs.OptionsColumn.AllowFocus = false;
            this.colLatLongPairs.OptionsColumn.ReadOnly = true;
            this.colLatLongPairs.Width = 95;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 6;
            this.colArea.Width = 60;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colLength
            // 
            this.colLength.Caption = "Length";
            this.colLength.ColumnEdit = this.repositoryItemTextEdit2DPMetres;
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 7;
            // 
            // repositoryItemTextEdit2DPMetres
            // 
            this.repositoryItemTextEdit2DPMetres.AutoHeight = false;
            this.repositoryItemTextEdit2DPMetres.Mask.EditMask = "#######0.00 Metres";
            this.repositoryItemTextEdit2DPMetres.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DPMetres.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DPMetres.Name = "repositoryItemTextEdit2DPMetres";
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 9;
            // 
            // colGUID2
            // 
            this.colGUID2.Caption = "GUID";
            this.colGUID2.FieldName = "GUID";
            this.colGUID2.Name = "colGUID2";
            this.colGUID2.OptionsColumn.AllowEdit = false;
            this.colGUID2.OptionsColumn.AllowFocus = false;
            this.colGUID2.OptionsColumn.ReadOnly = true;
            // 
            // colMapID1
            // 
            this.colMapID1.Caption = "Map ID";
            this.colMapID1.FieldName = "MapID";
            this.colMapID1.Name = "colMapID1";
            this.colMapID1.OptionsColumn.AllowEdit = false;
            this.colMapID1.OptionsColumn.AllowFocus = false;
            this.colMapID1.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitID1
            // 
            this.colCircuitID1.Caption = "Circuit ID";
            this.colCircuitID1.FieldName = "CircuitID";
            this.colCircuitID1.Name = "colCircuitID1";
            this.colCircuitID1.OptionsColumn.AllowEdit = false;
            this.colCircuitID1.OptionsColumn.AllowFocus = false;
            this.colCircuitID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber2
            // 
            this.colPoleNumber2.Caption = "Pole Number";
            this.colPoleNumber2.FieldName = "PoleNumber";
            this.colPoleNumber2.Name = "colPoleNumber2";
            this.colPoleNumber2.OptionsColumn.AllowEdit = false;
            this.colPoleNumber2.OptionsColumn.AllowFocus = false;
            this.colPoleNumber2.OptionsColumn.ReadOnly = true;
            this.colPoleNumber2.Width = 104;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitNumber1
            // 
            this.colCircuitNumber1.Caption = "Circuit Number";
            this.colCircuitNumber1.FieldName = "CircuitNumber";
            this.colCircuitNumber1.Name = "colCircuitNumber1";
            this.colCircuitNumber1.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber1.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber1.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber1.Width = 106;
            // 
            // colPoleName
            // 
            this.colPoleName.Caption = "Pole";
            this.colPoleName.FieldName = "PoleName";
            this.colPoleName.Name = "colPoleName";
            this.colPoleName.OptionsColumn.AllowEdit = false;
            this.colPoleName.OptionsColumn.AllowFocus = false;
            this.colPoleName.OptionsColumn.ReadOnly = true;
            this.colPoleName.Visible = true;
            this.colPoleName.VisibleIndex = 8;
            this.colPoleName.Width = 555;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 178;
            // 
            // colSizeBand
            // 
            this.colSizeBand.Caption = "Size Band";
            this.colSizeBand.FieldName = "SizeBand";
            this.colSizeBand.Name = "colSizeBand";
            this.colSizeBand.OptionsColumn.AllowEdit = false;
            this.colSizeBand.OptionsColumn.AllowFocus = false;
            this.colSizeBand.OptionsColumn.ReadOnly = true;
            this.colSizeBand.Visible = true;
            this.colSizeBand.VisibleIndex = 1;
            this.colSizeBand.Width = 86;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 3;
            this.colStatus1.Width = 87;
            // 
            // colObjectType
            // 
            this.colObjectType.Caption = "Object Type";
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.OptionsColumn.AllowEdit = false;
            this.colObjectType.OptionsColumn.AllowFocus = false;
            this.colObjectType.OptionsColumn.ReadOnly = true;
            this.colObjectType.Visible = true;
            this.colObjectType.VisibleIndex = 5;
            this.colObjectType.Width = 80;
            // 
            // colTPONumber
            // 
            this.colTPONumber.Caption = "TPO Number";
            this.colTPONumber.FieldName = "TPONumber";
            this.colTPONumber.Name = "colTPONumber";
            this.colTPONumber.OptionsColumn.AllowEdit = false;
            this.colTPONumber.OptionsColumn.AllowFocus = false;
            this.colTPONumber.OptionsColumn.ReadOnly = true;
            this.colTPONumber.Visible = true;
            this.colTPONumber.VisibleIndex = 8;
            this.colTPONumber.Width = 105;
            // 
            // colGrowthRate
            // 
            this.colGrowthRate.Caption = "Growth Rate";
            this.colGrowthRate.FieldName = "GrowthRate";
            this.colGrowthRate.Name = "colGrowthRate";
            this.colGrowthRate.OptionsColumn.AllowEdit = false;
            this.colGrowthRate.OptionsColumn.AllowFocus = false;
            this.colGrowthRate.OptionsColumn.ReadOnly = true;
            this.colGrowthRate.Visible = true;
            this.colGrowthRate.VisibleIndex = 2;
            this.colGrowthRate.Width = 82;
            // 
            // colDeadTree
            // 
            this.colDeadTree.Caption = "Dead Tree";
            this.colDeadTree.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDeadTree.FieldName = "DeadTree";
            this.colDeadTree.Name = "colDeadTree";
            this.colDeadTree.OptionsColumn.AllowEdit = false;
            this.colDeadTree.OptionsColumn.AllowFocus = false;
            this.colDeadTree.OptionsColumn.ReadOnly = true;
            this.colDeadTree.Visible = true;
            this.colDeadTree.VisibleIndex = 4;
            this.colDeadTree.Width = 71;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colCreatedByStaffID1
            // 
            this.colCreatedByStaffID1.Caption = "Created By Staff ID";
            this.colCreatedByStaffID1.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID1.Name = "colCreatedByStaffID1";
            this.colCreatedByStaffID1.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID1.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID1.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID1.Width = 114;
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl10;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl10);
            this.gridSplitContainer4.Size = new System.Drawing.Size(598, 151);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEditPercentage2});
            this.gridControl10.Size = new System.Drawing.Size(598, 151);
            this.gridControl10.TabIndex = 2;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource
            // 
            this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource.DataMember = "sp07065_UT_Pole_Manager_Species_Linked_To_Tree";
            this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeSpeciesID,
            this.colTreeID,
            this.colSpeciesID,
            this.colNumberOfTrees,
            this.colPercentage,
            this.colRemarks2,
            this.colGUID1,
            this.colSpecies,
            this.colTreeReferenceNumber,
            this.colPoleNumber1});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpecies, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.DoubleClick += new System.EventHandler(this.gridView10_DoubleClick);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colTreeSpeciesID
            // 
            this.colTreeSpeciesID.Caption = "Tree Species ID";
            this.colTreeSpeciesID.FieldName = "TreeSpeciesID";
            this.colTreeSpeciesID.Name = "colTreeSpeciesID";
            this.colTreeSpeciesID.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesID.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesID.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesID.Width = 96;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.Caption = "Species ID";
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.OptionsColumn.AllowEdit = false;
            this.colSpeciesID.OptionsColumn.AllowFocus = false;
            this.colSpeciesID.OptionsColumn.ReadOnly = true;
            // 
            // colNumberOfTrees
            // 
            this.colNumberOfTrees.Caption = "Number of Trees";
            this.colNumberOfTrees.FieldName = "NumberOfTrees";
            this.colNumberOfTrees.Name = "colNumberOfTrees";
            this.colNumberOfTrees.OptionsColumn.AllowEdit = false;
            this.colNumberOfTrees.OptionsColumn.AllowFocus = false;
            this.colNumberOfTrees.OptionsColumn.ReadOnly = true;
            this.colNumberOfTrees.Visible = true;
            this.colNumberOfTrees.VisibleIndex = 3;
            this.colNumberOfTrees.Width = 101;
            // 
            // colPercentage
            // 
            this.colPercentage.Caption = "Percentage";
            this.colPercentage.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colPercentage.FieldName = "Percentage";
            this.colPercentage.Name = "colPercentage";
            this.colPercentage.OptionsColumn.AllowEdit = false;
            this.colPercentage.OptionsColumn.AllowFocus = false;
            this.colPercentage.OptionsColumn.ReadOnly = true;
            this.colPercentage.Visible = true;
            this.colPercentage.VisibleIndex = 4;
            this.colPercentage.Width = 76;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.AllowEdit = false;
            this.colRemarks2.OptionsColumn.AllowFocus = false;
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSpecies
            // 
            this.colSpecies.Caption = "Species";
            this.colSpecies.FieldName = "Species";
            this.colSpecies.Name = "colSpecies";
            this.colSpecies.OptionsColumn.AllowEdit = false;
            this.colSpecies.OptionsColumn.AllowFocus = false;
            this.colSpecies.OptionsColumn.ReadOnly = true;
            this.colSpecies.Visible = true;
            this.colSpecies.VisibleIndex = 2;
            this.colSpecies.Width = 130;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 1;
            this.colTreeReferenceNumber.Width = 132;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.Visible = true;
            this.colPoleNumber1.VisibleIndex = 0;
            this.colPoleNumber1.Width = 118;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gridSplitContainer8);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage6.Text = "Linked Access Issues";
            // 
            // gridSplitContainer8
            // 
            this.gridSplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer8.Grid = this.gridControl14;
            this.gridSplitContainer8.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer8.Name = "gridSplitContainer8";
            this.gridSplitContainer8.Panel1.Controls.Add(this.gridControl14);
            this.gridSplitContainer8.Size = new System.Drawing.Size(1201, 154);
            this.gridSplitContainer8.TabIndex = 0;
            // 
            // gridControl14
            // 
            this.gridControl14.DataSource = this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource;
            this.gridControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl14.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl14.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl14_EmbeddedNavigator_ButtonClick);
            this.gridControl14.Location = new System.Drawing.Point(0, 0);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit14});
            this.gridControl14.Size = new System.Drawing.Size(1201, 154);
            this.gridControl14.TabIndex = 4;
            this.gridControl14.UseEmbeddedNavigator = true;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp07070UTPoleManagerLinkedAccessIssuesBindingSource
            // 
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource.DataMember = "sp07070_UT_Pole_Manager_Linked_Access_Issues";
            this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleAccessIssueID,
            this.colPoleID2,
            this.colAccessIssueID,
            this.colGUID3,
            this.colCircuitID2,
            this.colPoleNumber3,
            this.colClientID3,
            this.colCircuitNumber2,
            this.colPoleName1,
            this.colClientName3,
            this.colAccessIssue,
            this.colRemarks4});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.GroupCount = 1;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.MultiSelect = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAccessIssue, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView14.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView14.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView14.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView14.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView14.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView14_MouseUp);
            this.gridView14.DoubleClick += new System.EventHandler(this.gridView14_DoubleClick);
            this.gridView14.GotFocus += new System.EventHandler(this.gridView14_GotFocus);
            // 
            // colPoleAccessIssueID
            // 
            this.colPoleAccessIssueID.Caption = "Pole Access Issue ID";
            this.colPoleAccessIssueID.FieldName = "PoleAccessIssueID";
            this.colPoleAccessIssueID.Name = "colPoleAccessIssueID";
            this.colPoleAccessIssueID.OptionsColumn.AllowEdit = false;
            this.colPoleAccessIssueID.OptionsColumn.AllowFocus = false;
            this.colPoleAccessIssueID.OptionsColumn.ReadOnly = true;
            this.colPoleAccessIssueID.Width = 120;
            // 
            // colPoleID2
            // 
            this.colPoleID2.Caption = "Pole ID";
            this.colPoleID2.FieldName = "PoleID";
            this.colPoleID2.Name = "colPoleID2";
            this.colPoleID2.OptionsColumn.AllowEdit = false;
            this.colPoleID2.OptionsColumn.AllowFocus = false;
            this.colPoleID2.OptionsColumn.ReadOnly = true;
            // 
            // colAccessIssueID
            // 
            this.colAccessIssueID.Caption = "Access Issue ID";
            this.colAccessIssueID.FieldName = "AccessIssueID";
            this.colAccessIssueID.Name = "colAccessIssueID";
            this.colAccessIssueID.OptionsColumn.AllowEdit = false;
            this.colAccessIssueID.OptionsColumn.AllowFocus = false;
            this.colAccessIssueID.OptionsColumn.ReadOnly = true;
            this.colAccessIssueID.Width = 97;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitID2
            // 
            this.colCircuitID2.Caption = "Circuit ID";
            this.colCircuitID2.FieldName = "CircuitID";
            this.colCircuitID2.Name = "colCircuitID2";
            this.colCircuitID2.OptionsColumn.AllowEdit = false;
            this.colCircuitID2.OptionsColumn.AllowFocus = false;
            this.colCircuitID2.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber3
            // 
            this.colPoleNumber3.Caption = "Pole Number";
            this.colPoleNumber3.FieldName = "PoleNumber";
            this.colPoleNumber3.Name = "colPoleNumber3";
            this.colPoleNumber3.OptionsColumn.AllowEdit = false;
            this.colPoleNumber3.OptionsColumn.AllowFocus = false;
            this.colPoleNumber3.OptionsColumn.ReadOnly = true;
            this.colPoleNumber3.Width = 81;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitNumber2
            // 
            this.colCircuitNumber2.Caption = "Circuit Number";
            this.colCircuitNumber2.FieldName = "CircuitNumber";
            this.colCircuitNumber2.Name = "colCircuitNumber2";
            this.colCircuitNumber2.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber2.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber2.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber2.Width = 91;
            // 
            // colPoleName1
            // 
            this.colPoleName1.Caption = "Pole Name";
            this.colPoleName1.FieldName = "PoleName";
            this.colPoleName1.Name = "colPoleName1";
            this.colPoleName1.OptionsColumn.AllowEdit = false;
            this.colPoleName1.OptionsColumn.AllowFocus = false;
            this.colPoleName1.OptionsColumn.ReadOnly = true;
            this.colPoleName1.Visible = true;
            this.colPoleName1.VisibleIndex = 0;
            this.colPoleName1.Width = 438;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 78;
            // 
            // colAccessIssue
            // 
            this.colAccessIssue.Caption = "Access Issue";
            this.colAccessIssue.FieldName = "AccessIssue";
            this.colAccessIssue.Name = "colAccessIssue";
            this.colAccessIssue.OptionsColumn.AllowEdit = false;
            this.colAccessIssue.OptionsColumn.AllowFocus = false;
            this.colAccessIssue.OptionsColumn.ReadOnly = true;
            this.colAccessIssue.Visible = true;
            this.colAccessIssue.VisibleIndex = 0;
            this.colAccessIssue.Width = 210;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 1;
            this.colRemarks4.Width = 250;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridSplitContainer5);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage3.Text = "Linked Environmental Issues";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControl11;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl11);
            this.gridSplitContainer5.Size = new System.Drawing.Size(1201, 154);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl11.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl11_EmbeddedNavigator_ButtonClick);
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11});
            this.gridControl11.Size = new System.Drawing.Size(1201, 154);
            this.gridControl11.TabIndex = 3;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource
            // 
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource.DataMember = "sp07075_UT_Pole_Manager_Linked_Environmental_Issues";
            this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn56,
            this.gridColumn65,
            this.gridColumn66,
            this.gridColumn67,
            this.gridColumn68,
            this.gridColumn69,
            this.gridColumn70,
            this.gridColumn71,
            this.gridColumn72,
            this.gridColumn73,
            this.gridColumn74,
            this.gridColumn75});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 1;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn72, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn74, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView11.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.DoubleClick += new System.EventHandler(this.gridView11_DoubleClick);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Pole Environmental Issue ID";
            this.gridColumn56.FieldName = "PoleEnvironmentalIssueID";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.AllowEdit = false;
            this.gridColumn56.OptionsColumn.AllowFocus = false;
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.Width = 120;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Pole ID";
            this.gridColumn65.FieldName = "PoleID";
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.OptionsColumn.AllowEdit = false;
            this.gridColumn65.OptionsColumn.AllowFocus = false;
            this.gridColumn65.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn66
            // 
            this.gridColumn66.Caption = "Environmental Issue ID";
            this.gridColumn66.FieldName = "EnvironmentalIssueID";
            this.gridColumn66.Name = "gridColumn66";
            this.gridColumn66.OptionsColumn.AllowEdit = false;
            this.gridColumn66.OptionsColumn.AllowFocus = false;
            this.gridColumn66.OptionsColumn.ReadOnly = true;
            this.gridColumn66.Width = 97;
            // 
            // gridColumn67
            // 
            this.gridColumn67.Caption = "GUID";
            this.gridColumn67.FieldName = "GUID";
            this.gridColumn67.Name = "gridColumn67";
            this.gridColumn67.OptionsColumn.AllowEdit = false;
            this.gridColumn67.OptionsColumn.AllowFocus = false;
            this.gridColumn67.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn68
            // 
            this.gridColumn68.Caption = "Circuit ID";
            this.gridColumn68.FieldName = "CircuitID";
            this.gridColumn68.Name = "gridColumn68";
            this.gridColumn68.OptionsColumn.AllowEdit = false;
            this.gridColumn68.OptionsColumn.AllowFocus = false;
            this.gridColumn68.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn69
            // 
            this.gridColumn69.Caption = "Pole Number";
            this.gridColumn69.FieldName = "PoleNumber";
            this.gridColumn69.Name = "gridColumn69";
            this.gridColumn69.OptionsColumn.AllowEdit = false;
            this.gridColumn69.OptionsColumn.AllowFocus = false;
            this.gridColumn69.OptionsColumn.ReadOnly = true;
            this.gridColumn69.Width = 81;
            // 
            // gridColumn70
            // 
            this.gridColumn70.Caption = "Client ID";
            this.gridColumn70.FieldName = "ClientID";
            this.gridColumn70.Name = "gridColumn70";
            this.gridColumn70.OptionsColumn.AllowEdit = false;
            this.gridColumn70.OptionsColumn.AllowFocus = false;
            this.gridColumn70.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn71
            // 
            this.gridColumn71.Caption = "Circuit Number";
            this.gridColumn71.FieldName = "CircuitNumber";
            this.gridColumn71.Name = "gridColumn71";
            this.gridColumn71.OptionsColumn.AllowEdit = false;
            this.gridColumn71.OptionsColumn.AllowFocus = false;
            this.gridColumn71.OptionsColumn.ReadOnly = true;
            this.gridColumn71.Width = 91;
            // 
            // gridColumn72
            // 
            this.gridColumn72.Caption = "Pole Name";
            this.gridColumn72.FieldName = "PoleName";
            this.gridColumn72.Name = "gridColumn72";
            this.gridColumn72.OptionsColumn.AllowEdit = false;
            this.gridColumn72.OptionsColumn.AllowFocus = false;
            this.gridColumn72.OptionsColumn.ReadOnly = true;
            this.gridColumn72.Visible = true;
            this.gridColumn72.VisibleIndex = 0;
            this.gridColumn72.Width = 438;
            // 
            // gridColumn73
            // 
            this.gridColumn73.Caption = "Client Name";
            this.gridColumn73.FieldName = "ClientName";
            this.gridColumn73.Name = "gridColumn73";
            this.gridColumn73.OptionsColumn.AllowEdit = false;
            this.gridColumn73.OptionsColumn.AllowFocus = false;
            this.gridColumn73.OptionsColumn.ReadOnly = true;
            this.gridColumn73.Width = 78;
            // 
            // gridColumn74
            // 
            this.gridColumn74.Caption = "Environmental Issue";
            this.gridColumn74.FieldName = "EnvironmentalIssue";
            this.gridColumn74.Name = "gridColumn74";
            this.gridColumn74.OptionsColumn.AllowEdit = false;
            this.gridColumn74.OptionsColumn.AllowFocus = false;
            this.gridColumn74.OptionsColumn.ReadOnly = true;
            this.gridColumn74.Visible = true;
            this.gridColumn74.VisibleIndex = 0;
            this.gridColumn74.Width = 210;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Remarks";
            this.gridColumn75.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.gridColumn75.FieldName = "Remarks";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.ReadOnly = true;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 1;
            this.gridColumn75.Width = 250;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridSplitContainer6);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage4.Text = "Linked Site Hazards";
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControl12;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl12);
            this.gridSplitContainer6.Size = new System.Drawing.Size(1201, 154);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControl12
            // 
            this.gridControl12.DataSource = this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource;
            this.gridControl12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl12.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl12.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl12.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl12.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl12_EmbeddedNavigator_ButtonClick);
            this.gridControl12.Location = new System.Drawing.Point(0, 0);
            this.gridControl12.MainView = this.gridView12;
            this.gridControl12.MenuManager = this.barManager1;
            this.gridControl12.Name = "gridControl12";
            this.gridControl12.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit12});
            this.gridControl12.Size = new System.Drawing.Size(1201, 154);
            this.gridControl12.TabIndex = 4;
            this.gridControl12.UseEmbeddedNavigator = true;
            this.gridControl12.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView12});
            // 
            // sp07080UTPoleManagerLinkedSiteHazardsBindingSource
            // 
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource.DataMember = "sp07080_UT_Pole_Manager_Linked_Site_Hazards";
            this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView12
            // 
            this.gridView12.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn76,
            this.gridColumn77,
            this.gridColumn78,
            this.gridColumn79,
            this.gridColumn80,
            this.gridColumn81,
            this.gridColumn82,
            this.gridColumn83,
            this.gridColumn84,
            this.gridColumn85,
            this.gridColumn86,
            this.gridColumn87});
            this.gridView12.GridControl = this.gridControl12;
            this.gridView12.GroupCount = 1;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView12.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView12.OptionsLayout.StoreAppearance = true;
            this.gridView12.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView12.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView12.OptionsSelection.MultiSelect = true;
            this.gridView12.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView12.OptionsView.ColumnAutoWidth = false;
            this.gridView12.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            this.gridView12.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn84, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn86, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView12.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView12.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView12.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView12.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView12.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView12.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView12_MouseUp);
            this.gridView12.DoubleClick += new System.EventHandler(this.gridView12_DoubleClick);
            this.gridView12.GotFocus += new System.EventHandler(this.gridView12_GotFocus);
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "Pole Site Hazard ID";
            this.gridColumn76.FieldName = "PoleSiteHazardID";
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.OptionsColumn.AllowEdit = false;
            this.gridColumn76.OptionsColumn.AllowFocus = false;
            this.gridColumn76.OptionsColumn.ReadOnly = true;
            this.gridColumn76.Width = 120;
            // 
            // gridColumn77
            // 
            this.gridColumn77.Caption = "Pole ID";
            this.gridColumn77.FieldName = "PoleID";
            this.gridColumn77.Name = "gridColumn77";
            this.gridColumn77.OptionsColumn.AllowEdit = false;
            this.gridColumn77.OptionsColumn.AllowFocus = false;
            this.gridColumn77.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn78
            // 
            this.gridColumn78.Caption = "Site Hazard ID";
            this.gridColumn78.FieldName = "SiteHazardID";
            this.gridColumn78.Name = "gridColumn78";
            this.gridColumn78.OptionsColumn.AllowEdit = false;
            this.gridColumn78.OptionsColumn.AllowFocus = false;
            this.gridColumn78.OptionsColumn.ReadOnly = true;
            this.gridColumn78.Width = 97;
            // 
            // gridColumn79
            // 
            this.gridColumn79.Caption = "GUID";
            this.gridColumn79.FieldName = "GUID";
            this.gridColumn79.Name = "gridColumn79";
            this.gridColumn79.OptionsColumn.AllowEdit = false;
            this.gridColumn79.OptionsColumn.AllowFocus = false;
            this.gridColumn79.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn80
            // 
            this.gridColumn80.Caption = "Circuit ID";
            this.gridColumn80.FieldName = "CircuitID";
            this.gridColumn80.Name = "gridColumn80";
            this.gridColumn80.OptionsColumn.AllowEdit = false;
            this.gridColumn80.OptionsColumn.AllowFocus = false;
            this.gridColumn80.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn81
            // 
            this.gridColumn81.Caption = "Pole Number";
            this.gridColumn81.FieldName = "PoleNumber";
            this.gridColumn81.Name = "gridColumn81";
            this.gridColumn81.OptionsColumn.AllowEdit = false;
            this.gridColumn81.OptionsColumn.AllowFocus = false;
            this.gridColumn81.OptionsColumn.ReadOnly = true;
            this.gridColumn81.Width = 81;
            // 
            // gridColumn82
            // 
            this.gridColumn82.Caption = "Client ID";
            this.gridColumn82.FieldName = "ClientID";
            this.gridColumn82.Name = "gridColumn82";
            this.gridColumn82.OptionsColumn.AllowEdit = false;
            this.gridColumn82.OptionsColumn.AllowFocus = false;
            this.gridColumn82.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn83
            // 
            this.gridColumn83.Caption = "Circuit Number";
            this.gridColumn83.FieldName = "CircuitNumber";
            this.gridColumn83.Name = "gridColumn83";
            this.gridColumn83.OptionsColumn.AllowEdit = false;
            this.gridColumn83.OptionsColumn.AllowFocus = false;
            this.gridColumn83.OptionsColumn.ReadOnly = true;
            this.gridColumn83.Width = 91;
            // 
            // gridColumn84
            // 
            this.gridColumn84.Caption = "Pole Name";
            this.gridColumn84.FieldName = "PoleName";
            this.gridColumn84.Name = "gridColumn84";
            this.gridColumn84.OptionsColumn.AllowEdit = false;
            this.gridColumn84.OptionsColumn.AllowFocus = false;
            this.gridColumn84.OptionsColumn.ReadOnly = true;
            this.gridColumn84.Visible = true;
            this.gridColumn84.VisibleIndex = 0;
            this.gridColumn84.Width = 438;
            // 
            // gridColumn85
            // 
            this.gridColumn85.Caption = "Client Name";
            this.gridColumn85.FieldName = "ClientName";
            this.gridColumn85.Name = "gridColumn85";
            this.gridColumn85.OptionsColumn.AllowEdit = false;
            this.gridColumn85.OptionsColumn.AllowFocus = false;
            this.gridColumn85.OptionsColumn.ReadOnly = true;
            this.gridColumn85.Width = 78;
            // 
            // gridColumn86
            // 
            this.gridColumn86.Caption = "Site Hazard";
            this.gridColumn86.FieldName = "SiteHazard";
            this.gridColumn86.Name = "gridColumn86";
            this.gridColumn86.OptionsColumn.AllowEdit = false;
            this.gridColumn86.OptionsColumn.AllowFocus = false;
            this.gridColumn86.OptionsColumn.ReadOnly = true;
            this.gridColumn86.Visible = true;
            this.gridColumn86.VisibleIndex = 0;
            this.gridColumn86.Width = 210;
            // 
            // gridColumn87
            // 
            this.gridColumn87.Caption = "Remarks";
            this.gridColumn87.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.gridColumn87.FieldName = "Remarks";
            this.gridColumn87.Name = "gridColumn87";
            this.gridColumn87.OptionsColumn.ReadOnly = true;
            this.gridColumn87.Visible = true;
            this.gridColumn87.VisibleIndex = 1;
            this.gridColumn87.Width = 250;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.gridSplitContainer7);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage5.Text = "Linked Electrical Hazards";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControl13;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl13);
            this.gridSplitContainer7.Size = new System.Drawing.Size(1201, 154);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl13
            // 
            this.gridControl13.DataSource = this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource;
            this.gridControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl13.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl13.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl13_EmbeddedNavigator_ButtonClick);
            this.gridControl13.Location = new System.Drawing.Point(0, 0);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13});
            this.gridControl13.Size = new System.Drawing.Size(1201, 154);
            this.gridControl13.TabIndex = 4;
            this.gridControl13.UseEmbeddedNavigator = true;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // sp07085UTPoleManagerLinkedElectricalHazardsBindingSource
            // 
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource.DataMember = "sp07085_UT_Pole_Manager_Linked_Electrical_Hazards";
            this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn88,
            this.gridColumn89,
            this.gridColumn90,
            this.gridColumn91,
            this.gridColumn92,
            this.gridColumn93,
            this.gridColumn94,
            this.gridColumn95,
            this.gridColumn96,
            this.gridColumn97,
            this.gridColumn98,
            this.gridColumn99});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.GroupCount = 1;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.MultiSelect = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn96, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn98, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView13.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView13.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView13.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView13.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView13.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView13_MouseUp);
            this.gridView13.DoubleClick += new System.EventHandler(this.gridView13_DoubleClick);
            this.gridView13.GotFocus += new System.EventHandler(this.gridView13_GotFocus);
            // 
            // gridColumn88
            // 
            this.gridColumn88.Caption = "Pole Electrical Hazard ID";
            this.gridColumn88.FieldName = "PoleElectricalHazardID";
            this.gridColumn88.Name = "gridColumn88";
            this.gridColumn88.OptionsColumn.AllowEdit = false;
            this.gridColumn88.OptionsColumn.AllowFocus = false;
            this.gridColumn88.OptionsColumn.ReadOnly = true;
            this.gridColumn88.Width = 120;
            // 
            // gridColumn89
            // 
            this.gridColumn89.Caption = "Pole ID";
            this.gridColumn89.FieldName = "PoleID";
            this.gridColumn89.Name = "gridColumn89";
            this.gridColumn89.OptionsColumn.AllowEdit = false;
            this.gridColumn89.OptionsColumn.AllowFocus = false;
            this.gridColumn89.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn90
            // 
            this.gridColumn90.Caption = "Electrical Hazard ID";
            this.gridColumn90.FieldName = "ElectricalHazardID";
            this.gridColumn90.Name = "gridColumn90";
            this.gridColumn90.OptionsColumn.AllowEdit = false;
            this.gridColumn90.OptionsColumn.AllowFocus = false;
            this.gridColumn90.OptionsColumn.ReadOnly = true;
            this.gridColumn90.Width = 97;
            // 
            // gridColumn91
            // 
            this.gridColumn91.Caption = "GUID";
            this.gridColumn91.FieldName = "GUID";
            this.gridColumn91.Name = "gridColumn91";
            this.gridColumn91.OptionsColumn.AllowEdit = false;
            this.gridColumn91.OptionsColumn.AllowFocus = false;
            this.gridColumn91.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn92
            // 
            this.gridColumn92.Caption = "Circuit ID";
            this.gridColumn92.FieldName = "CircuitID";
            this.gridColumn92.Name = "gridColumn92";
            this.gridColumn92.OptionsColumn.AllowEdit = false;
            this.gridColumn92.OptionsColumn.AllowFocus = false;
            this.gridColumn92.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn93
            // 
            this.gridColumn93.Caption = "Pole Number";
            this.gridColumn93.FieldName = "PoleNumber";
            this.gridColumn93.Name = "gridColumn93";
            this.gridColumn93.OptionsColumn.AllowEdit = false;
            this.gridColumn93.OptionsColumn.AllowFocus = false;
            this.gridColumn93.OptionsColumn.ReadOnly = true;
            this.gridColumn93.Width = 81;
            // 
            // gridColumn94
            // 
            this.gridColumn94.Caption = "Client ID";
            this.gridColumn94.FieldName = "ClientID";
            this.gridColumn94.Name = "gridColumn94";
            this.gridColumn94.OptionsColumn.AllowEdit = false;
            this.gridColumn94.OptionsColumn.AllowFocus = false;
            this.gridColumn94.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn95
            // 
            this.gridColumn95.Caption = "Circuit Number";
            this.gridColumn95.FieldName = "CircuitNumber";
            this.gridColumn95.Name = "gridColumn95";
            this.gridColumn95.OptionsColumn.AllowEdit = false;
            this.gridColumn95.OptionsColumn.AllowFocus = false;
            this.gridColumn95.OptionsColumn.ReadOnly = true;
            this.gridColumn95.Width = 91;
            // 
            // gridColumn96
            // 
            this.gridColumn96.Caption = "Pole Name";
            this.gridColumn96.FieldName = "PoleName";
            this.gridColumn96.Name = "gridColumn96";
            this.gridColumn96.OptionsColumn.AllowEdit = false;
            this.gridColumn96.OptionsColumn.AllowFocus = false;
            this.gridColumn96.OptionsColumn.ReadOnly = true;
            this.gridColumn96.Visible = true;
            this.gridColumn96.VisibleIndex = 0;
            this.gridColumn96.Width = 438;
            // 
            // gridColumn97
            // 
            this.gridColumn97.Caption = "Client Name";
            this.gridColumn97.FieldName = "ClientName";
            this.gridColumn97.Name = "gridColumn97";
            this.gridColumn97.OptionsColumn.AllowEdit = false;
            this.gridColumn97.OptionsColumn.AllowFocus = false;
            this.gridColumn97.OptionsColumn.ReadOnly = true;
            this.gridColumn97.Width = 78;
            // 
            // gridColumn98
            // 
            this.gridColumn98.Caption = "Electrical Hazard";
            this.gridColumn98.FieldName = "ElectricalHazard";
            this.gridColumn98.Name = "gridColumn98";
            this.gridColumn98.OptionsColumn.AllowEdit = false;
            this.gridColumn98.OptionsColumn.AllowFocus = false;
            this.gridColumn98.OptionsColumn.ReadOnly = true;
            this.gridColumn98.Visible = true;
            this.gridColumn98.VisibleIndex = 0;
            this.gridColumn98.Width = 210;
            // 
            // gridColumn99
            // 
            this.gridColumn99.Caption = "Remarks";
            this.gridColumn99.ColumnEdit = this.repositoryItemMemoExEdit13;
            this.gridColumn99.FieldName = "Remarks";
            this.gridColumn99.Name = "gridColumn99";
            this.gridColumn99.OptionsColumn.ReadOnly = true;
            this.gridColumn99.Visible = true;
            this.gridColumn99.VisibleIndex = 1;
            this.gridColumn99.Width = 250;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.ImageOptions.Image")));
            this.xtraTabPage1.ImageOptions.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1201, 154);
            this.xtraTabPage1.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl3;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1201, 154);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl3.Size = new System.Drawing.Size(1201, 154);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07001_UT_Client_FilterTableAdapter
            // 
            this.sp07001_UT_Client_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(559, 183);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit2),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit3),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit4),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit5),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit6, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshPoles)});
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.Caption = "Client Filter";
            this.popupContainerEdit1.Edit = this.repositoryItemPopupContainerEdit1;
            this.popupContainerEdit1.EditValue = "No Client Filter";
            this.popupContainerEdit1.EditWidth = 110;
            this.popupContainerEdit1.Id = 28;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlClients;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.Caption = "Region Filter";
            this.popupContainerEdit2.Edit = this.repositoryItemPopupContainerEdit2;
            this.popupContainerEdit2.EditValue = "No Region Filter";
            this.popupContainerEdit2.EditWidth = 110;
            this.popupContainerEdit2.Id = 29;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControlRegions;
            this.repositoryItemPopupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit2_QueryResultValue);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.Caption = "Sub-Area Filter";
            this.popupContainerEdit3.Edit = this.repositoryItemPopupContainerEdit3;
            this.popupContainerEdit3.EditValue = "No Sub-Area Filter";
            this.popupContainerEdit3.EditWidth = 110;
            this.popupContainerEdit3.Id = 30;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControlSubAreas;
            this.repositoryItemPopupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit3_QueryResultValue);
            // 
            // popupContainerEdit4
            // 
            this.popupContainerEdit4.Caption = "Primary Filter";
            this.popupContainerEdit4.Edit = this.repositoryItemPopupContainerEdit4;
            this.popupContainerEdit4.EditValue = "No Primary Filter";
            this.popupContainerEdit4.EditWidth = 110;
            this.popupContainerEdit4.Id = 31;
            this.popupContainerEdit4.Name = "popupContainerEdit4";
            // 
            // repositoryItemPopupContainerEdit4
            // 
            this.repositoryItemPopupContainerEdit4.AutoHeight = false;
            this.repositoryItemPopupContainerEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit4.Name = "repositoryItemPopupContainerEdit4";
            this.repositoryItemPopupContainerEdit4.PopupControl = this.popupContainerControlPrimarys;
            this.repositoryItemPopupContainerEdit4.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit4_QueryResultValue);
            // 
            // popupContainerEdit5
            // 
            this.popupContainerEdit5.Caption = "Feeder Filter";
            this.popupContainerEdit5.Edit = this.repositoryItemPopupContainerEdit5;
            this.popupContainerEdit5.EditValue = "No Feeder Filter";
            this.popupContainerEdit5.EditWidth = 110;
            this.popupContainerEdit5.Id = 32;
            this.popupContainerEdit5.Name = "popupContainerEdit5";
            // 
            // repositoryItemPopupContainerEdit5
            // 
            this.repositoryItemPopupContainerEdit5.AutoHeight = false;
            this.repositoryItemPopupContainerEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit5.Name = "repositoryItemPopupContainerEdit5";
            this.repositoryItemPopupContainerEdit5.PopupControl = this.popupContainerControlFeeders;
            this.repositoryItemPopupContainerEdit5.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit5_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Circuits";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // popupContainerEdit6
            // 
            this.popupContainerEdit6.Caption = "Circuit Filter";
            this.popupContainerEdit6.Edit = this.repositoryItemPopupContainerEdit6;
            this.popupContainerEdit6.EditValue = "No Circuit Filter";
            this.popupContainerEdit6.EditWidth = 110;
            this.popupContainerEdit6.Id = 34;
            this.popupContainerEdit6.Name = "popupContainerEdit6";
            // 
            // repositoryItemPopupContainerEdit6
            // 
            this.repositoryItemPopupContainerEdit6.AutoHeight = false;
            this.repositoryItemPopupContainerEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit6.Name = "repositoryItemPopupContainerEdit6";
            this.repositoryItemPopupContainerEdit6.PopupControl = this.popupContainerControlCircuits;
            this.repositoryItemPopupContainerEdit6.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit6_QueryResultValue);
            // 
            // bbiRefreshPoles
            // 
            this.bbiRefreshPoles.Caption = "Refresh Poles";
            this.bbiRefreshPoles.Id = 35;
            this.bbiRefreshPoles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshPoles.ImageOptions.Image")));
            this.bbiRefreshPoles.Name = "bbiRefreshPoles";
            this.bbiRefreshPoles.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshPoles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshPoles_ItemClick);
            // 
            // sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter
            // 
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter
            // 
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.ClearBeforeFill = true;
            // 
            // sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter
            // 
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.ClearBeforeFill = true;
            // 
            // sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter
            // 
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter
            // 
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.ClearBeforeFill = true;
            // 
            // sp07041_UT_Pole_ManagerTableAdapter
            // 
            this.sp07041_UT_Pole_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp07064_UT_Pole_Manager_Linked_TreesTableAdapter
            // 
            this.sp07064_UT_Pole_Manager_Linked_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter
            // 
            this.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter
            // 
            this.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter
            // 
            this.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter
            // 
            this.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending9
            // 
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending9.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending9.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending9.GridControl = this.gridControl9;
            // 
            // xtraGridBlending10
            // 
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending10.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending10.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending10.GridControl = this.gridControl10;
            // 
            // xtraGridBlending14
            // 
            this.xtraGridBlending14.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending14.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending14.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending14.GridControl = this.gridControl14;
            // 
            // xtraGridBlending11
            // 
            this.xtraGridBlending11.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending11.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending11.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending11.GridControl = this.gridControl11;
            // 
            // xtraGridBlending12
            // 
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending12.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending12.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending12.GridControl = this.gridControl12;
            // 
            // xtraGridBlending13
            // 
            this.xtraGridBlending13.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending13.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending13.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending13.GridControl = this.gridControl13;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // bbiAddPolesToNewSurvey
            // 
            this.bbiAddPolesToNewSurvey.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddPolesToNewSurvey.Caption = "Add <b>Selected Poles</b> to <b>New</b> Survey";
            this.bbiAddPolesToNewSurvey.Id = 36;
            this.bbiAddPolesToNewSurvey.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddPolesToNewSurvey.ImageOptions.Image")));
            this.bbiAddPolesToNewSurvey.Name = "bbiAddPolesToNewSurvey";
            this.bbiAddPolesToNewSurvey.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolesToNewSurvey_ItemClick);
            // 
            // bbiAddPolesToExistingSurvey
            // 
            this.bbiAddPolesToExistingSurvey.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddPolesToExistingSurvey.Caption = "Add <b>Selected Poles</b> to <b>Existing</b> Survey";
            this.bbiAddPolesToExistingSurvey.Id = 37;
            this.bbiAddPolesToExistingSurvey.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddPolesToExistingSurvey.ImageOptions.Image")));
            this.bbiAddPolesToExistingSurvey.Name = "bbiAddPolesToExistingSurvey";
            this.bbiAddPolesToExistingSurvey.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolesToExistingSurvey_ItemClick);
            // 
            // frm_UT_Pole_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1210, 542);
            this.Controls.Add(this.splitContainerControl2);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Pole_Manager";
            this.Text = "Pole Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Pole_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Pole_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Pole_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).EndInit();
            this.popupContainerControlClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07041UTPoleManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).EndInit();
            this.popupContainerControlCircuits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).EndInit();
            this.popupContainerControlFeeders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).EndInit();
            this.popupContainerControlPrimarys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).EndInit();
            this.popupContainerControlSubAreas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).EndInit();
            this.popupContainerControlRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07064UTPoleManagerLinkedTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).EndInit();
            this.gridSplitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07070UTPoleManagerLinkedAccessIssuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07080UTPoleManagerLinkedSiteHazardsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07085UTPoleManagerLinkedElectricalHazardsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlClients;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07001UTClientFilterBindingSource;
        private DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter sp07001_UT_Client_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit4;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit5;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRegions;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnRegionFilterOK;
        private System.Windows.Forms.BindingSource sp07036UTRegionPopupFilteredByClientBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSubAreas;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.SimpleButton btnSubAreaFilterOK;
        private System.Windows.Forms.BindingSource sp07037UTSubAreaPopupFilteredByRegionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlPrimarys;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.SimpleButton btnPrimaryFilterOK;
        private System.Windows.Forms.BindingSource sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlFeeders;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private System.Windows.Forms.BindingSource sp07039UTFeederPopupFilteredByPrimaryBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraEditors.SimpleButton btnFeederFilterOK;
        private DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshPoles;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCircuits;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.SimpleButton btnCircuitFilterOK;
        private System.Windows.Forms.BindingSource sp07040UTCircuitPopupFilteredByVariousBindingSource;
        private DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private System.Windows.Forms.BindingSource sp07041UTPoleManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID;
        private DataSet_UTTableAdapters.sp07041_UT_Pole_ManagerTableAdapter sp07041_UT_Pole_ManagerTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridControl gridControl12;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer8;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private System.Windows.Forms.BindingSource sp07065UTPoleManagerSpeciesLinkedToTreeBindingSource;
        private DataSet_UTTableAdapters.sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter;
        private System.Windows.Forms.BindingSource sp07064UTPoleManagerLinkedTreesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBandID;
        private DevExpress.XtraGrid.Columns.GridColumn colX1;
        private DevExpress.XtraGrid.Columns.GridColumn colY1;
        private DevExpress.XtraGrid.Columns.GridColumn colXYPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBand;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DataSet_UTTableAdapters.sp07064_UT_Pole_Manager_Linked_TreesTableAdapter sp07064_UT_Pole_Manager_Linked_TreesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DPMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberOfTrees;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private System.Windows.Forms.BindingSource sp07070UTPoleManagerLinkedAccessIssuesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleAccessIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID2;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessIssue;
        private System.Windows.Forms.BindingSource sp07075UTPoleManagerLinkedEnvironmentalIssuesBindingSource;
        private System.Windows.Forms.BindingSource sp07080UTPoleManagerLinkedSiteHazardsBindingSource;
        private System.Windows.Forms.BindingSource sp07085UTPoleManagerLinkedElectricalHazardsBindingSource;
        private DataSet_UTTableAdapters.sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter;
        private DataSet_UTTableAdapters.sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter;
        private DataSet_UTTableAdapters.sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter;
        private DataSet_UTTableAdapters.sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn66;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn67;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn68;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn69;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn70;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn71;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn72;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn73;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn74;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn77;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn78;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn79;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn80;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn81;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn82;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn83;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn84;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn85;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn86;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn87;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn88;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn89;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn90;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn91;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn92;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn93;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn94;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn95;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn96;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn97;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn98;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn99;
        private DevExpress.XtraGrid.Columns.GridColumn colTPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colGrowthRate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeadTree;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending9;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending10;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending14;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending11;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending12;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending13;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraBars.BarButtonItem bbiAddPolesToNewSurvey;
        private DevExpress.XtraBars.BarButtonItem bbiAddPolesToExistingSurvey;
    }
}

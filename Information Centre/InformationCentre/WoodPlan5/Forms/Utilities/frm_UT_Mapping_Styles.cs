using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Styles : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        private DataSet _dsDefaultStyle;
        private DataSet _dsThematicSets;
        private DataSet _dsThematicItems;
        private int _intThematicStylingFieldHeaderID;
        private int _intThematicStylingFieldHeaderType;
        private int _intTransparency;
        public frm_UT_Mapping frmCallingForm = null;
        private bool boolFirstLoad = true;  // Used to prevent Lookup Grid resetting the thematic styles on initial value set when loading //
        
        public int intLoadedStyleSetID = 0;
        public int intLoadedStyleSetOwnerID = -1;
        public int intUseThematicStyling = 0;
        public string strPointSize = "";
        public int intTransparency = 0;
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //
        public string strThematicStylingFieldName = "";

        private int intBasePickListID = 0;
        #endregion

        public frm_UT_Mapping_Styles()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Styles_Load(object sender, EventArgs e)
        {
            this.FormID = 500014;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            WaitDialogForm loading = new WaitDialogForm("Loading Styles...", "Utilities Mapping");
            loading.Show();

            gridControl5.DataSource = _dsDefaultStyle.Tables[0];
            glueThematicField.Properties.DataSource = _dsThematicSets.Tables[0];
            glueThematicField.EditValue = _intThematicStylingFieldHeaderID;
            gridControl6.DataSource = _dsThematicItems.Tables[0];
            trackBarControl1.Value = intTransparency;
            RefreshGridViewState = new RefreshGridState(glueThematicField.Properties.View, "HeaderID");
           
            DataRow[] tempDS = _dsThematicSets.Tables[0].Select("HeaderID = '" + glueThematicField.EditValue.ToString() + "'");
            if (tempDS.Length == 1)
            {
                DataRow currentRow = tempDS[0];
                intLoadedStyleSetOwnerID = Convert.ToInt32(currentRow["OwnerID"]);
                intLoadedStyleSetID = Convert.ToInt32(currentRow["HeaderID"]);
                strThematicStylingFieldName = currentRow["SetName"].ToString();
                intBasePickListID = Convert.ToInt32(currentRow["BasePicklistID"]);            
            }          
            gridControl6.MainView = (intBasePickListID >= 90000 ? gridView1 : gridView6);
            gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = (intBasePickListID >= 90000 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = (intBasePickListID >= 90000 ? true : false);
            layoutControlItem8.Visibility = (intBasePickListID >= 90000 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);



            ceUseThematicStyling.Checked = (intUseThematicStyling == 1 ? true : false);
            if (strPointSize.StartsWith("Fix"))
            {
                checkEdit2.Checked = true;
            }
            else
            {
                checkEdit3.Checked = true;
            }

            // Get default value for ShowMap checkbox //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strDefaultBandWidth = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Thematic_Band_Width").ToString();
            decimal decDefaultBandWidth = (string.IsNullOrEmpty(strDefaultBandWidth) ? (decimal)5 : Convert.ToDecimal(strDefaultBandWidth));
            spinEditDefaultBandIncrement.EditValue = decDefaultBandWidth;           

            loading.Close();

        }


        public DataSet dsDefaultStyle
        {
            get
            {
                return _dsDefaultStyle;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsDefaults  //
                _dsDefaultStyle = value;
            }
        }

        public DataSet dsThematicSets
        {
            get
            {
                return _dsThematicSets;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsThematicSets  //
                _dsThematicSets = value;
            }
        }

        public DataSet dsThematicItems
        {
            get
            {
                return _dsThematicItems;
            }
            set
            {
                // does not create a new dataset, but gives the variable a reference to the dataset assigned to dsThematicItems  //
                _dsThematicItems = value;
            }
        }

        public int intThematicStylingFieldHeaderID
        {
            get
            {
                return _intThematicStylingFieldHeaderID;
            }
            set
            {
                // does not create a new int, but gives the variable a reference to the int assigned to intThematicStylingFieldHeaderID  //
                _intThematicStylingFieldHeaderID = value;
            }
        }

        public int intThematicStylingFieldHeaderType
        {
            get
            {
                return _intThematicStylingFieldHeaderType;
            }
            set
            {
                // does not create a new int, but gives the variable a reference to the int assigned to intThematicStylingFieldHeaderType  //
                _intThematicStylingFieldHeaderType = value;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView6":
                case "gridView1":
                    message = "No thematics defined - select a value from the Thematic Field to proceed";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        #endregion


        #region Styling Defaults Grid

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            //if (e.Column.Caption.Contains("Point Colour") || e.Column.Caption.Contains("Polygon Line Colour"))
            //{
            //    e.Graphics.DrawLine(new Pen(Color.LightGray, 2), e.Bounds.Right, e.Bounds.Top - 2, e.Bounds.Right, e.Bounds.Bottom + 2);
            //}
            //if (e.Column.Caption.Contains("Point Size"))
            //{
            //    e.Graphics.FillRectangle(new SolidBrush(Color.Gray), e.Bounds);
            //}
        }
       
        #endregion


        #region Styling Items Grid

        private void glueThematicField_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to CLEAR all Thematic Styles!\n\nAre you sure you wish to proceed?", "Clear Thematic Styles", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    glueThematicField.EditValue = null;
                    GridView view = (GridView)gridControl6.MainView;
                    view.BeginUpdate();
                    view.BeginDataUpdate();
                    _dsThematicItems.Clear();
                    view.EndDataUpdate();
                    view.EndUpdate();
                }
            }
            else if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {

                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to DELETE the currently selected Thematic Style Set!\n\nAre you sure you wish to proceed?", "Delete Thematic Style Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {

                    // Physically delete the Style Set Header and Style Set Items from the Database //
                    DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter DeleteStyleSet = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                    DeleteStyleSet.ChangeConnectionString(strConnectionString);
                    try
                    {
                        DeleteStyleSet.sp01262_AT_Tree_Picker_Style_Set_Delete_Style_Set(intLoadedStyleSetID);
                        
                        // Clear Grid Contents //
                        glueThematicField.EditValue = null;
                        GridView view = (GridView)gridControl6.MainView;
                        view.BeginUpdate();
                        view.BeginDataUpdate();
                        _dsThematicItems.Clear();
                        view.EndDataUpdate();
                        view.EndUpdate();
                        
                        // Reload Style Set Header List //
                        this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
                        frmCallingForm.ChildForm_RefreshListOfAvailableStyleSets(0, 0);  // Refresh GridLookUpEdit's data on Calling Form //
                        this.RefreshGridViewState.LoadViewInfo();  // Restore Grid View State //

                        glueThematicField.Properties.Buttons[2].Enabled = false;  // Disable Delete Button

                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show("Style Set Deleted Successfully.", "Delete Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete style set - an error occurred while deleting the style set!\n\nTry deleting again - if the problem persists, contact Technical Support.", "Delete Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }
       }

        private void glueThematicField_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            GridView view = (GridView)glue.Properties.View;
            if (boolFirstLoad)
            {
                boolFirstLoad = false;
                intLoadedStyleSetOwnerID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "OwnerID"));

                int intRowHandle = view.LocateByValue(0, view.Columns["HeaderID"], Convert.ToInt32(glue.EditValue));
                if (intRowHandle != GridControl.InvalidRowHandle) intLoadedStyleSetOwnerID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "OwnerID"));
                intLoadedStyleSetID = (intLoadedStyleSetOwnerID >= 0 ? Convert.ToInt32(view.GetRowCellValue(intRowHandle, "HeaderID")) : 0);  // Store Saved Style Set ID if user has selected one or 0 if system picklist //
                if (intRowHandle != GridControl.InvalidRowHandle) strThematicStylingFieldName = view.GetRowCellValue(intRowHandle, "SetName").ToString();
                intBasePickListID = (intRowHandle >= 0 ? Convert.ToInt32(view.GetRowCellValue(intRowHandle, "BasePicklistID")) : 0);
            }
            else
            {
                intLoadedStyleSetOwnerID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "OwnerID"));
                intLoadedStyleSetID = (intLoadedStyleSetOwnerID >= 0 ? Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "HeaderID")) : 0);  // Store Saved Style Set ID if user has selected one or 0 if system picklist //
                int intHeaderType = (intLoadedStyleSetOwnerID == -1 ? 0 : 1);
                LoadThematicGrid(Convert.ToInt32(glue.EditValue), intHeaderType);
                strThematicStylingFieldName = (view.FocusedRowHandle >= 0 ? view.GetRowCellValue(view.FocusedRowHandle, "SetName").ToString() : "");
                intBasePickListID = (view.FocusedRowHandle >= 0 ? Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BasePicklistID")) : 0);
            }

            if (intLoadedStyleSetID > 0 && intLoadedStyleSetOwnerID == this.GlobalSettings.UserID)
            {
                glueThematicField.Properties.Buttons[2].Enabled = true;
            }
            else
            {
                glueThematicField.Properties.Buttons[2].Enabled = false;
            }

            gridControl6.MainView = (intBasePickListID >= 90000 ? gridView1 :gridView6);
            gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = (intBasePickListID >= 90000 ? true : false);
            gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = (intBasePickListID >= 90000 ? true : false);
            layoutControlItem8.Visibility = (intBasePickListID >= 90000 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
        }

        private void LoadThematicGrid(int intHeaderID, int intHeaderType)
        {
            // Get Defaults to pass through to SP //
            int intDefaultSymbol = 0;
            int intDefaultSymbolColour = 0;
            int intDefaultSymbolSize = 0;
            int intDefaultPolygonBoundaryColour = 0;
            int intDefaultPolygonBoundaryWidth = 0;
            int intDefaultPolygonFillColour = 0;
            int intDefaultPolygonFillPattern = 0;
            int intDefaultPolygonFillPatternColour = 0;
            int intDefaultLineStyle = 0;
            int intDefaultLineColour = 0;
            int intDefaultLineWidth = 0;

            GridView viewDefault = (GridView)gridControl5.MainView;
            if (viewDefault.DataRowCount > 0)
            {
                intDefaultSymbol = Convert.ToInt32(viewDefault.GetRowCellValue(0, "Symbol"));
                intDefaultSymbolColour = Convert.ToInt32(viewDefault.GetRowCellValue(0, "SymbolColour"));
                intDefaultSymbolSize = Convert.ToInt32(viewDefault.GetRowCellValue(0, "Size"));
                intDefaultPolygonBoundaryColour = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolygonLineColour"));
                intDefaultPolygonBoundaryWidth = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolygonLineWidth"));
                intDefaultPolygonFillColour = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolygonFillColour"));
                intDefaultPolygonFillPattern = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolygonFillPattern"));
                intDefaultPolygonFillPatternColour = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolygonFillPatternColour"));
                intDefaultLineStyle = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolylineStyle"));
                intDefaultLineColour = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolylineColour"));
                intDefaultLineWidth = Convert.ToInt32(viewDefault.GetRowCellValue(0, "PolylineWidth"));
            }

            GridView view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            view.BeginDataUpdate();
            frmCallingForm.ChildForm_RefreshThematicItemDataSet(intHeaderID, intHeaderType, intDefaultSymbol, intDefaultSymbolColour, intDefaultSymbolSize, intDefaultPolygonBoundaryColour, intDefaultPolygonBoundaryWidth, intDefaultPolygonFillColour, intDefaultPolygonFillPattern, intDefaultPolygonFillPatternColour, intDefaultLineStyle, intDefaultLineColour, intDefaultLineWidth);
            view.EndDataUpdate();
            view.EndUpdate();
        }

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            //if (e.Column.Caption.Contains("Point Colour") || e.Column.Caption.Contains("Polygon Line Colour"))
            //{
            //    e.Graphics.DrawLine(new Pen(Color.LightGray, 2), e.Bounds.Right, e.Bounds.Top - 2, e.Bounds.Right, e.Bounds.Bottom + 2);
            //}
        }

        private void gridView6_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.DataRowCount <= 0)
                {
                    bbiSaveThematicSet.Enabled = false;
                    bbiSaveThematicSetAs.Enabled = false;
                    bbiBlockEditThematicStyles.Enabled = false;
                }
                else  // At least one style item present //
                {
                    if (intLoadedStyleSetID > 0)  // Current Set has already been saved in the past //
                    {
                        if (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID)  // Owner allowed to save //
                        {
                            bbiSaveThematicSet.Enabled = true;
                        }
                        else  // Not owner so no save //
                        {
                            bbiSaveThematicSet.Enabled = false;
                        }
                        bbiSaveThematicSetAs.Enabled = true;  // Save As allowed regardless of owner //
                    }
                    else  // Current styles have not been saved so save allowed but not save as //
                    {
                        bbiSaveThematicSet.Enabled = true;
                        bbiSaveThematicSetAs.Enabled = false;
                    }
                    int[] intRowHandles = view.GetSelectedRows();
                    if (intRowHandles.Length > 0) // at least one stylke selected for editing so allow block edit //
                    {
                        bbiBlockEditThematicStyles.Enabled = true;

                    }
                    else  // No styles selected so no block edit //
                    {
                        bbiBlockEditThematicStyles.Enabled = false;
                    }
                }
                bbiAddBand.Enabled = false;
                bbiDeleteBand.Enabled = false;
                bbiCreateDescriptionsFromBands.Enabled = false;
                popupMenu_ThematicGrid.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void btnStylingOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void bbiBlockEditThematicStyles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more thematic styles before proceeding.", "Block Edit Thematic Styles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strRecordIDs = "";
            int intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ItemID")) + ",";
                intCount++;
            }
            frm_AT_Mapping_Block_Edit_Thematic_Styles frm_styles = new frm_AT_Mapping_Block_Edit_Thematic_Styles();
            frm_styles.intCount = intCount;
            if (frm_styles.ShowDialog() == DialogResult.OK)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    if (frm_styles.intPointSymbol != null) view.SetRowCellValue(intRowHandle, "Symbol", frm_styles.intPointSymbol);
                    if (frm_styles.decPointSize != null) view.SetRowCellValue(intRowHandle, "Size", frm_styles.decPointSize);
                    if (frm_styles.intPointColour != null) view.SetRowCellValue(intRowHandle, "SymbolColour", frm_styles.intPointColour);
                    if (frm_styles.intPolygonFillColour != null) view.SetRowCellValue(intRowHandle, "PolygonFillColour", frm_styles.intPolygonFillColour);
                    if (frm_styles.intPolygonLineColour != null) view.SetRowCellValue(intRowHandle, "PolygonLineColour", frm_styles.intPolygonLineColour);
                    if (frm_styles.intPolygonFillPattern != null) view.SetRowCellValue(intRowHandle, "PolygonFillPattern", frm_styles.intPolygonFillPattern);
                    if (frm_styles.intPolygonFillPatternColour != null) view.SetRowCellValue(intRowHandle, "PolygonFillPatternColour", frm_styles.intPolygonFillPatternColour);
                    if (frm_styles.intPolygonLineWidth != null) view.SetRowCellValue(intRowHandle, "PolygonLineWidth", frm_styles.intPolygonLineWidth);

                    if (frm_styles.intPolylineStyle != null) view.SetRowCellValue(intRowHandle, "PolylineStyle", frm_styles.intPolylineStyle);
                    if (frm_styles.intPolylineColour != null) view.SetRowCellValue(intRowHandle, "PolylineColour", frm_styles.intPolylineColour);
                    if (frm_styles.intPolylineWidth != null) view.SetRowCellValue(intRowHandle, "PolylineWidth", frm_styles.intPolylineWidth);

                    if (frm_styles.intShowInLegend != null) view.SetRowCellValue(intRowHandle, "ShowInLegend", frm_styles.intShowInLegend);
                }
                view.EndUpdate();
            }
        }

        private void bbiSaveThematicSet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (intLoadedStyleSetID == 0)  // Nothing saved so open screen to allow user to input a description etc //
            {
                SaveStyleSetHeaderInfo();
            }
            else if (intLoadedStyleSetOwnerID != GlobalSettings.UserID)  // Saved Style Set open but user didn't create it so abort //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You can only save changes to Style Sets you own.\n\nYou did not create the currently loaded style set so you cannot change it!\n\nSave Changes Cancelled.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else  // Ok to save changes so update associated info //
            {
                this.fProgress = new frmProgress(10);  // Show Progress Window //

                // Clear associated info for this header first [they will be recreated in SaveStyleSetItemInfo method call] //
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter ClearStyleItems = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                ClearStyleItems.ChangeConnectionString(strConnectionString);
                try
                {
                    ClearStyleItems.sp01261_AT_Tree_Picker_Style_Set_Delete_Style_Items(intLoadedStyleSetID);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save style set - an error occurred while clearing the previous version [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Thematic style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                this.fProgress = new frmProgress(10);  // Show Progress Window //
                this.AddOwnedForm(fProgress);
                fProgress.UpdateCaption("Saving Style Set, Please Wait...");
                fProgress.Show();
                Application.DoEvents();

                SaveStyleSetItemInfo(intLoadedStyleSetID);
            }
        }

        private void bbiSaveThematicSetAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveStyleSetHeaderInfo();
        }

        private void SaveStyleSetHeaderInfo()
        {
            frm_AT_Mapping_Styles_Save fChildForm = new frm_AT_Mapping_Styles_Save();
            this.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;

            if (fChildForm.ShowDialog() == DialogResult.OK)
            {

                this.fProgress = new frmProgress(10);  // Show Progress Window //
                this.AddOwnedForm(fProgress);
                fProgress.UpdateCaption("Saving Style Set, Please Wait...");
                fProgress.Show();
                Application.DoEvents();

                string strDescription = fChildForm.strStyleSetDescription;
                string strRemarks = fChildForm.strStyleSetRemarks;
                int intTransparency = trackBarControl1.Value;

                //int intBasePicklistID = 0;
                DataRow[] tempDS = _dsThematicSets.Tables[0].Select("HeaderID = '" + glueThematicField.EditValue.ToString() + "'");
                if (tempDS.Length == 1)
                {
                    DataRow currentRow = tempDS[0];
                    intBasePickListID = Convert.ToInt32(currentRow["BasePicklistID"]);
                }          
                if (intBasePickListID == 0)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save style set [" + strDescription + "] - an error occurred while saving the style set header!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }


                // Save Header... //
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SaveHeader = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                SaveHeader.ChangeConnectionString(strConnectionString);
                try
                {
                    intLoadedStyleSetID = Convert.ToInt32(SaveHeader.sp01259_AT_Tree_Picker_Style_Set_Save_Header("insert", intLoadedStyleSetID, strDescription, strRemarks, intBasePickListID, this.GlobalSettings.UserID, intTransparency));
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save style set [" + strDescription + "] - an error occurred while saving the style set header!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                intLoadedStyleSetOwnerID = this.GlobalSettings.UserID;
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                SaveStyleSetItemInfo(intLoadedStyleSetID);

                // Reload Style Set Header List //
                this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
                frmCallingForm.ChildForm_RefreshListOfAvailableStyleSets(intLoadedStyleSetID, 1);  // Refresh GridLookUpEdit's data on Calling Form //
                this.RefreshGridViewState.LoadViewInfo();  // Restore Grid View State //
                
                boolFirstLoad = true;  // Prevent List of actual style items reloading when the GridLookupEdit of Style Sets has it's value changed on line below //
                glueThematicField.EditValue = intLoadedStyleSetID;
                intLoadedStyleSetOwnerID = this.GlobalSettings.UserID;


            }
        }

        private void SaveStyleSetItemInfo(int intTemplateID)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Save Style Set Items...// 
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SaveItem = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            SaveItem.ChangeConnectionString(strConnectionString);

            GridView view = (GridView)gridControl6.MainView;
            if (ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //

            int intThematicItemID = 0;
            int intThematicSetID = 0;
            string strItemDescription = "";
            int intPicklistItemID = 0;
            int intPointSymbol = 0;
            int intPointSize = 0;
            int intPointColour = 0;
            int intPolygonFillColour = 0;
            int intPolygonFillPattern = 0;
            int intPolygonFillPatternColour = 0;
            int intPolygonLineWidth = 0;
            int intPolygonLineColour = 0;
            int intPolylineStyle = 0;
            int intPolylineColour = 0;
            int intPolylineWidth = 0;
            int intShowInLegend = 0;
            int intItemOrder = 0;
            decimal decBandStart = (decimal)0.00;
            decimal decBandEnd = (decimal)0.00;
            int intTransparency = trackBarControl1.Value;

            for (int i = 0; i < view.DataRowCount; i++)
            {
                intThematicItemID = Convert.ToInt32(view.GetRowCellValue(i, "ItemID"));
                intThematicSetID = intLoadedStyleSetID;
                strItemDescription = Convert.ToString(view.GetRowCellValue(i, "ItemDescription"));
                intPicklistItemID = Convert.ToInt32(view.GetRowCellValue(i, "PicklistItemID"));
                intPointSymbol = Convert.ToInt32(view.GetRowCellValue(i, "Symbol"));
                intPointSize = Convert.ToInt32(view.GetRowCellValue(i, "Size"));
                intPointColour = Convert.ToInt32(view.GetRowCellValue(i, "SymbolColour"));
                intPolygonFillColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillColour"));
                intPolygonFillPattern = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillPattern"));
                intPolygonFillPatternColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonFillPatternColour"));
                intPolygonLineWidth = Convert.ToInt32(view.GetRowCellValue(i, "PolygonLineWidth"));
                intPolygonLineColour = Convert.ToInt32(view.GetRowCellValue(i, "PolygonLineColour"));
                intPolylineStyle = Convert.ToInt32(view.GetRowCellValue(i, "PolylineStyle"));
                intPolylineColour = Convert.ToInt32(view.GetRowCellValue(i, "PolylineColour"));
                intPolylineWidth = Convert.ToInt32(view.GetRowCellValue(i, "PolylineWidth"));
                intShowInLegend = Convert.ToInt32(view.GetRowCellValue(i, "ShowInLegend"));
                intItemOrder = Convert.ToInt32(view.GetRowCellValue(i, "Order"));
                decBandStart = Convert.ToDecimal(view.GetRowCellValue(i, "StartBand"));
                decBandEnd = Convert.ToDecimal(view.GetRowCellValue(i, "EndBand"));
                try
                {
                    SaveItem.sp01260_AT_Tree_Picker_Style_Set_Save_Item("insert", 0, intThematicSetID, strItemDescription, intPicklistItemID, intPointSymbol, intPointSize, intPointColour, intPolygonFillColour, intPolygonFillPattern, intPolygonFillPatternColour, intPolygonLineWidth, intPolygonLineColour, intPolylineStyle, intPolylineColour, intPolylineWidth, intShowInLegend, intItemOrder, decBandStart, decBandEnd, intTransparency);
                }
                catch
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save style set - an error occurred while saving the style items!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();  // close Progress Window //
                fProgress = null;
            }
            if (this.GlobalSettings.ShowConfirmations == 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Thematic Style Set Saved.", "Save Thematic Style Set", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.DataRowCount <= 0)
                {
                    bbiSaveThematicSet.Enabled = false;
                    bbiSaveThematicSetAs.Enabled = false;
                    bbiBlockEditThematicStyles.Enabled = false;
                    bbiDeleteBand.Enabled = false;
                    bbiCreateDescriptionsFromBands.Enabled = false;
                }
                else  // At least one style item present //
                {
                    if (intLoadedStyleSetID > 0)  // Current Set has already been saved in the past //
                    {
                        bbiSaveThematicSet.Enabled = (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID ? true : false);
                        bbiSaveThematicSetAs.Enabled = true;  // Save As allowed regardless of owner //
                    }
                    else  // Current styles have not been saved so save allowed but not save as //
                    {
                        bbiSaveThematicSet.Enabled = true;
                        bbiSaveThematicSetAs.Enabled = false;
                    }
                    int[] intRowHandles = view.GetSelectedRows();
                    bbiBlockEditThematicStyles.Enabled = (intRowHandles.Length > 0 ? true : false);
                    bbiAddBand.Enabled = true;
                    bbiDeleteBand.Enabled = (intRowHandles.Length > 0 ? true : false);
                    bbiCreateDescriptionsFromBands.Enabled = true;
                }
                popupMenu_ThematicGrid.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    AddRecord();
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    DeleteRecord();
                    e.Handled = true;
                    break;
            }
        }

        private void bbiAddBand_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AddRecord();
        }

        private void bbiDeleteBand_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord()
        {
            int intMaxOrder = 0;
            decimal decLastEndBand = (decimal)0.00;
            foreach (DataRow dr in dsThematicItems.Tables[0].Rows)
            {
                if (Convert.ToInt32(dr["Order"]) > intMaxOrder) intMaxOrder = Convert.ToInt32(dr["Order"]);
                if (Convert.ToDecimal(dr["EndBand"]) > decLastEndBand) decLastEndBand = Convert.ToDecimal(dr["EndBand"]);
            }
            DataRow drStyle = dsDefaultStyle.Tables[0].Rows[0];
            int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
            int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
            int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
            int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
            int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
            int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
            int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
            int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
            int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
            int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
            int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);

            DataRow drNewRow = dsThematicItems.Tables[0].NewRow();
            drNewRow["ItemDescription"] = "";
            drNewRow["ItemCode"] = "";
            drNewRow["ItemID"] = 0;
            drNewRow["PicklistItemID"] = 0;
            drNewRow["HeaderID"] = intBasePickListID;
            drNewRow["HeaderDescription"] = "";
            drNewRow["Order"] = intMaxOrder + 1;
            drNewRow["Symbol"] = intDefaultSymbol;
            drNewRow["Size"] = intDefaultSymbolSize;
            drNewRow["SymbolColour"] = intDefaultSymbolColour;
            drNewRow["PolygonFillColour"] = intDefaultPolygonFillColour;
            drNewRow["PolygonLineColour"] = intDefaultPolygonBoundaryColour;
            drNewRow["PolygonFillPattern"] = intDefaultPolygonFillPattern;
            drNewRow["PolygonFillPatternColour"] = intDefaultPolygonFillPatternColour;
            drNewRow["PolygonLineWidth"] = intDefaultPolygonBoundaryWidth;
            drNewRow["PolylineColour"] = intDefaultLineColour;
            drNewRow["PolylineWidth"] = intDefaultLineWidth;
            drNewRow["PolylineStyle"] = intDefaultLineStyle;
            drNewRow["ShowInLegend"] = 1;
            drNewRow["StartBand"] = decLastEndBand + (decimal)0.01;
            drNewRow["EndBand"] = decLastEndBand + (decimal)spinEditDefaultBandIncrement.Value;
            dsThematicItems.Tables[0].Rows.Add(drNewRow);        
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more rows to be deleted before proceeding.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + (intRowHandles.Length == 1 ? "1 Thematic Style Item" : intRowHandles.Length.ToString() + " Thematic Style Items") + " selected for delete!\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                view.BeginUpdate();
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    dsThematicItems.Tables[0].Rows.Remove(view.GetDataRow(intRowHandles[i]));                    
                }
                view.EndUpdate();
            }
        }

        private void bbiCreatDescriptionsFromBands_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            foreach (DataRow dr in dsThematicItems.Tables[0].Rows)
            {
                dr["ItemDescription"] = dr["StartBand"].ToString() + " - " + dr["EndBand"].ToString();
            }

        }

        #endregion


        private void ceUseThematicStyling_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            gridControl6.Enabled = ce.Checked;
            glueThematicField.Enabled = ce.Checked;
            spinEditDefaultBandIncrement.Enabled = ce.Checked;
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;

            view = (GridView)gridControl6.MainView;
            view.CloseEditor();
            if (!view.UpdateCurrentRow()) return;

            intUseThematicStyling = (ceUseThematicStyling.Checked ? 1 : 0);
            strPointSize = (checkEdit2.Checked ? "Fixed" : "Dynamic");
            intTransparency = trackBarControl1.Value;

            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

 
    }
}


namespace WoodPlan5
{
    partial class frm_UT_Risk_Assessment_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Risk_Assessment_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling32 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling33 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling34 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling35 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.pmPrint = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiEditReportLayout = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.FeederNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07256UTRiskAssessmentQuestionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.FeederNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrimaryNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrimaryNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NearestLandLineMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.NearestAandEMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.MobilePhoneSignalBarsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EmergencyServicesMeetPointMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.BanksManMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.AirAmbulanceLongTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AirAmbulanceLatTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmergencyKitLocationMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.MEWPArialRescuersMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.SiteAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubContractorSignatureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colSignatureFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditSignature = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnSignatureCapture = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditSignatureCapture = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.VoltageTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoleNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubAreaNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubAreaNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.RiskAssessmentTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RiskAssessmentTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PersonCompletingIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PersonCompletingTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SurveyedPoleIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PersonCompletingNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PersonCompletingTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastUpdatedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NRSWAOpeningNoticeNoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PermitHolderNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TypeOfWorkingIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07263UTTypeOfWorkWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ElectricalRiskCategoryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ElectricalAssessmentVerifiedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PermissionCheckedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DateTimeRecordedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.RiskAssessmentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colQuestionNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditAnswer = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasterQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.ItemForRiskAssessmentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyedPoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonCompletingTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonCompletingID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRiskAssessmentTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrimaryNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrimaryName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroupLocation = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForVoltage = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoleNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupOther = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPersonCompletingName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPersonCompletingType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForElectricalRiskCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermissionChecked = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateTimeRecorded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastUpdated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForElectricalAssessmentVerified = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermitHolderName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTypeOfWorkingID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNRSWAOpeningNoticeNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupSiteSpecific = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSiteAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMEWPArialRescuers = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmergencyKitLocation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAirAmbulanceLat = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBanksMan = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAirAmbulanceLong = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmergencyServicesMeetPoint = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobilePhoneSignalBars = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForNearestAandE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNearestLandLine = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForRiskAssessmentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroupQuestions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupTeamSignatures = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroupNearMisses = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter();
            this.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter();
            this.sp07263_UT_Type_Of_Work_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07263_UT_Type_Of_Work_With_BlankTableAdapter();
            this.sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter();
            this.sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07256UTRiskAssessmentQuestionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestLandLineMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandEMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobilePhoneSignalBarsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyServicesMeetPointMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BanksManMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirAmbulanceLongTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirAmbulanceLatTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyKitLocationMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEWPArialRescuersMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditSignatureCapture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoltageTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedPoleIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastUpdatedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NRSWAOpeningNoticeNoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermitHolderNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeOfWorkingIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07263UTTypeOfWorkWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectricalRiskCategoryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectricalAssessmentVerifiedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionCheckedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeRecordedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAnswer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedPoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVoltage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectricalRiskCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionChecked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeRecorded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastUpdated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectricalAssessmentVerified)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermitHolderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeOfWorkingID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNRSWAOpeningNoticeNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSiteSpecific)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMEWPArialRescuers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyKitLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAirAmbulanceLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBanksMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAirAmbulanceLong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyServicesMeetPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobilePhoneSignalBars)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestLandLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTeamSignatures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNearMisses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 720);
            this.barDockControlBottom.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 694);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1143, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 694);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiEditReportLayout});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.barButtonItemPrint});
            this.barManager2.MaxItemId = 28;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint)});
            this.bar2.Text = "Discounts";
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemPrint.Caption = "View Risk Assessment Sheet";
            this.barButtonItemPrint.DropDownControl = this.pmPrint;
            this.barButtonItemPrint.Id = 27;
            this.barButtonItemPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.ImageOptions.Image")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // pmPrint
            // 
            this.pmPrint.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditReportLayout, true)});
            this.pmPrint.Manager = this.barManager1;
            this.pmPrint.MenuCaption = "Print Menu";
            this.pmPrint.Name = "pmPrint";
            this.pmPrint.ShowCaption = true;
            // 
            // bbiEditReportLayout
            // 
            this.bbiEditReportLayout.Caption = "Edit Risk Assessment Layout";
            this.bbiEditReportLayout.Id = 28;
            this.bbiEditReportLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditReportLayout.ImageOptions.Image")));
            this.bbiEditReportLayout.Name = "bbiEditReportLayout";
            this.bbiEditReportLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditReportLayout_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1143, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 720);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1143, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 694);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1143, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 694);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.FeederNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimaryNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimaryNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NearestLandLineMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.NearestAandEMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.MobilePhoneSignalBarsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EmergencyServicesMeetPointMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.BanksManMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.AirAmbulanceLongTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AirAmbulanceLatTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmergencyKitLocationMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.MEWPArialRescuersMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl1);
            this.dataLayoutControl1.Controls.Add(this.gridControl3);
            this.dataLayoutControl1.Controls.Add(this.VoltageTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubAreaNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubAreaNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.RiskAssessmentTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RiskAssessmentTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonCompletingIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonCompletingTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyedPoleIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonCompletingNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PersonCompletingTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LastUpdatedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NRSWAOpeningNoticeNoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PermitHolderNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TypeOfWorkingIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ElectricalRiskCategoryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ElectricalAssessmentVerifiedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PermissionCheckedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DateTimeRecordedTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.RiskAssessmentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.DataSource = this.sp07256UTRiskAssessmentQuestionItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRiskAssessmentID,
            this.ItemForSurveyedPoleID,
            this.ItemForPersonCompletingTypeID,
            this.ItemForPersonCompletingID,
            this.ItemForRiskAssessmentTypeID,
            this.ItemForPrimaryNumber,
            this.ItemForFeederName,
            this.ItemForFeederNumber,
            this.ItemForPrimaryName,
            this.ItemForRegionName,
            this.ItemForClientName,
            this.ItemForRegionNumber});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(143, 196, 250, 542);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1143, 694);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // FeederNumberTextEdit
            // 
            this.FeederNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "FeederNumber", true));
            this.FeederNumberTextEdit.Location = new System.Drawing.Point(184, 250);
            this.FeederNumberTextEdit.MenuManager = this.barManager1;
            this.FeederNumberTextEdit.Name = "FeederNumberTextEdit";
            this.FeederNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederNumberTextEdit, true);
            this.FeederNumberTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederNumberTextEdit, optionsSpelling1);
            this.FeederNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederNumberTextEdit.TabIndex = 71;
            // 
            // sp07256UTRiskAssessmentQuestionItemBindingSource
            // 
            this.sp07256UTRiskAssessmentQuestionItemBindingSource.DataMember = "sp07256_UT_Risk_Assessment_Question_Item";
            this.sp07256UTRiskAssessmentQuestionItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // FeederNameTextEdit
            // 
            this.FeederNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "FeederName", true));
            this.FeederNameTextEdit.Location = new System.Drawing.Point(184, 260);
            this.FeederNameTextEdit.MenuManager = this.barManager1;
            this.FeederNameTextEdit.Name = "FeederNameTextEdit";
            this.FeederNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederNameTextEdit, true);
            this.FeederNameTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederNameTextEdit, optionsSpelling2);
            this.FeederNameTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederNameTextEdit.TabIndex = 70;
            // 
            // PrimaryNumberTextEdit
            // 
            this.PrimaryNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PrimaryNumber", true));
            this.PrimaryNumberTextEdit.Location = new System.Drawing.Point(184, 250);
            this.PrimaryNumberTextEdit.MenuManager = this.barManager1;
            this.PrimaryNumberTextEdit.Name = "PrimaryNumberTextEdit";
            this.PrimaryNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PrimaryNumberTextEdit, true);
            this.PrimaryNumberTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PrimaryNumberTextEdit, optionsSpelling3);
            this.PrimaryNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PrimaryNumberTextEdit.TabIndex = 69;
            // 
            // PrimaryNameTextEdit
            // 
            this.PrimaryNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PrimaryName", true));
            this.PrimaryNameTextEdit.Location = new System.Drawing.Point(184, 226);
            this.PrimaryNameTextEdit.MenuManager = this.barManager1;
            this.PrimaryNameTextEdit.Name = "PrimaryNameTextEdit";
            this.PrimaryNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PrimaryNameTextEdit, true);
            this.PrimaryNameTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PrimaryNameTextEdit, optionsSpelling4);
            this.PrimaryNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PrimaryNameTextEdit.TabIndex = 68;
            // 
            // RegionNumberTextEdit
            // 
            this.RegionNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "RegionNumber", true));
            this.RegionNumberTextEdit.Location = new System.Drawing.Point(184, 131);
            this.RegionNumberTextEdit.MenuManager = this.barManager1;
            this.RegionNumberTextEdit.Name = "RegionNumberTextEdit";
            this.RegionNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNumberTextEdit, true);
            this.RegionNumberTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNumberTextEdit, optionsSpelling5);
            this.RegionNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNumberTextEdit.TabIndex = 65;
            // 
            // RegionNameTextEdit
            // 
            this.RegionNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "RegionName", true));
            this.RegionNameTextEdit.Location = new System.Drawing.Point(184, 130);
            this.RegionNameTextEdit.MenuManager = this.barManager1;
            this.RegionNameTextEdit.Name = "RegionNameTextEdit";
            this.RegionNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNameTextEdit, true);
            this.RegionNameTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNameTextEdit, optionsSpelling6);
            this.RegionNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameTextEdit.TabIndex = 64;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(184, 130);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(254, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling7);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 63;
            // 
            // NearestLandLineMemoEdit
            // 
            this.NearestLandLineMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "NearestLandLine", true));
            this.NearestLandLineMemoEdit.Location = new System.Drawing.Point(712, 281);
            this.NearestLandLineMemoEdit.MenuManager = this.barManager1;
            this.NearestLandLineMemoEdit.Name = "NearestLandLineMemoEdit";
            this.NearestLandLineMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestLandLineMemoEdit, true);
            this.NearestLandLineMemoEdit.Size = new System.Drawing.Size(395, 33);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestLandLineMemoEdit, optionsSpelling8);
            this.NearestLandLineMemoEdit.StyleController = this.dataLayoutControl1;
            this.NearestLandLineMemoEdit.TabIndex = 86;
            // 
            // NearestAandEMemoEdit
            // 
            this.NearestAandEMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "NearestAandE", true));
            this.NearestAandEMemoEdit.Location = new System.Drawing.Point(193, 281);
            this.NearestAandEMemoEdit.MenuManager = this.barManager1;
            this.NearestAandEMemoEdit.Name = "NearestAandEMemoEdit";
            this.NearestAandEMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestAandEMemoEdit, true);
            this.NearestAandEMemoEdit.Size = new System.Drawing.Size(358, 33);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestAandEMemoEdit, optionsSpelling9);
            this.NearestAandEMemoEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Nearest A && E - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "This should be programed into the Sat. Nav. by the team.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.NearestAandEMemoEdit.SuperTip = superToolTip4;
            this.NearestAandEMemoEdit.TabIndex = 85;
            // 
            // MobilePhoneSignalBarsSpinEdit
            // 
            this.MobilePhoneSignalBarsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "MobilePhoneSignalBars", true));
            this.MobilePhoneSignalBarsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.MobilePhoneSignalBarsSpinEdit.Location = new System.Drawing.Point(712, 245);
            this.MobilePhoneSignalBarsSpinEdit.MenuManager = this.barManager1;
            this.MobilePhoneSignalBarsSpinEdit.Name = "MobilePhoneSignalBarsSpinEdit";
            this.MobilePhoneSignalBarsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MobilePhoneSignalBarsSpinEdit.Properties.Mask.EditMask = "#0.00 Bars";
            this.MobilePhoneSignalBarsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.MobilePhoneSignalBarsSpinEdit.Size = new System.Drawing.Size(395, 20);
            this.MobilePhoneSignalBarsSpinEdit.StyleController = this.dataLayoutControl1;
            this.MobilePhoneSignalBarsSpinEdit.TabIndex = 84;
            // 
            // EmergencyServicesMeetPointMemoEdit
            // 
            this.EmergencyServicesMeetPointMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "EmergencyServicesMeetPoint", true));
            this.EmergencyServicesMeetPointMemoEdit.Location = new System.Drawing.Point(193, 245);
            this.EmergencyServicesMeetPointMemoEdit.MenuManager = this.barManager1;
            this.EmergencyServicesMeetPointMemoEdit.Name = "EmergencyServicesMeetPointMemoEdit";
            this.EmergencyServicesMeetPointMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmergencyServicesMeetPointMemoEdit, true);
            this.EmergencyServicesMeetPointMemoEdit.Size = new System.Drawing.Size(358, 32);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmergencyServicesMeetPointMemoEdit, optionsSpelling10);
            this.EmergencyServicesMeetPointMemoEdit.StyleController = this.dataLayoutControl1;
            this.EmergencyServicesMeetPointMemoEdit.TabIndex = 83;
            // 
            // BanksManMemoEdit
            // 
            this.BanksManMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "BanksMan", true));
            this.BanksManMemoEdit.Location = new System.Drawing.Point(712, 197);
            this.BanksManMemoEdit.MenuManager = this.barManager1;
            this.BanksManMemoEdit.Name = "BanksManMemoEdit";
            this.BanksManMemoEdit.Properties.NullText = "32000";
            this.scSpellChecker.SetShowSpellCheckMenu(this.BanksManMemoEdit, true);
            this.BanksManMemoEdit.Size = new System.Drawing.Size(395, 44);
            this.scSpellChecker.SetSpellCheckerOptions(this.BanksManMemoEdit, optionsSpelling11);
            this.BanksManMemoEdit.StyleController = this.dataLayoutControl1;
            this.BanksManMemoEdit.TabIndex = 82;
            // 
            // AirAmbulanceLongTextEdit
            // 
            this.AirAmbulanceLongTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "AirAmbulanceLong", true));
            this.AirAmbulanceLongTextEdit.Location = new System.Drawing.Point(193, 221);
            this.AirAmbulanceLongTextEdit.MenuManager = this.barManager1;
            this.AirAmbulanceLongTextEdit.Name = "AirAmbulanceLongTextEdit";
            this.AirAmbulanceLongTextEdit.Properties.Mask.EditMask = "##.###############";
            this.AirAmbulanceLongTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AirAmbulanceLongTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AirAmbulanceLongTextEdit, true);
            this.AirAmbulanceLongTextEdit.Size = new System.Drawing.Size(358, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AirAmbulanceLongTextEdit, optionsSpelling12);
            this.AirAmbulanceLongTextEdit.StyleController = this.dataLayoutControl1;
            this.AirAmbulanceLongTextEdit.TabIndex = 81;
            // 
            // AirAmbulanceLatTextEdit
            // 
            this.AirAmbulanceLatTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "AirAmbulanceLat", true));
            this.AirAmbulanceLatTextEdit.Location = new System.Drawing.Point(193, 197);
            this.AirAmbulanceLatTextEdit.MenuManager = this.barManager1;
            this.AirAmbulanceLatTextEdit.Name = "AirAmbulanceLatTextEdit";
            this.AirAmbulanceLatTextEdit.Properties.Mask.EditMask = "##.###############";
            this.AirAmbulanceLatTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AirAmbulanceLatTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AirAmbulanceLatTextEdit, true);
            this.AirAmbulanceLatTextEdit.Size = new System.Drawing.Size(358, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AirAmbulanceLatTextEdit, optionsSpelling13);
            this.AirAmbulanceLatTextEdit.StyleController = this.dataLayoutControl1;
            this.AirAmbulanceLatTextEdit.TabIndex = 80;
            // 
            // EmergencyKitLocationMemoEdit
            // 
            this.EmergencyKitLocationMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "EmergencyKitLocation", true));
            this.EmergencyKitLocationMemoEdit.Location = new System.Drawing.Point(712, 154);
            this.EmergencyKitLocationMemoEdit.MenuManager = this.barManager1;
            this.EmergencyKitLocationMemoEdit.Name = "EmergencyKitLocationMemoEdit";
            this.EmergencyKitLocationMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmergencyKitLocationMemoEdit, true);
            this.EmergencyKitLocationMemoEdit.Size = new System.Drawing.Size(395, 39);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmergencyKitLocationMemoEdit, optionsSpelling14);
            this.EmergencyKitLocationMemoEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Emergency Kit Location - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Record any necessary kit and preperation.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.EmergencyKitLocationMemoEdit.SuperTip = superToolTip5;
            this.EmergencyKitLocationMemoEdit.TabIndex = 79;
            // 
            // MEWPArialRescuersMemoEdit
            // 
            this.MEWPArialRescuersMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "MEWPArialRescuers", true));
            this.MEWPArialRescuersMemoEdit.Location = new System.Drawing.Point(712, 108);
            this.MEWPArialRescuersMemoEdit.MenuManager = this.barManager1;
            this.MEWPArialRescuersMemoEdit.Name = "MEWPArialRescuersMemoEdit";
            this.MEWPArialRescuersMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MEWPArialRescuersMemoEdit, true);
            this.MEWPArialRescuersMemoEdit.Size = new System.Drawing.Size(395, 42);
            this.scSpellChecker.SetSpellCheckerOptions(this.MEWPArialRescuersMemoEdit, optionsSpelling15);
            this.MEWPArialRescuersMemoEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem6.Text = "MEWP \\ Aerial Rescuers - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Consider necessary number of persons and suitable capabilities of rescuers.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.MEWPArialRescuersMemoEdit.SuperTip = superToolTip6;
            this.MEWPArialRescuersMemoEdit.TabIndex = 78;
            // 
            // SiteAddressMemoEdit
            // 
            this.SiteAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "SiteAddress", true));
            this.SiteAddressMemoEdit.Location = new System.Drawing.Point(193, 108);
            this.SiteAddressMemoEdit.MenuManager = this.barManager1;
            this.SiteAddressMemoEdit.Name = "SiteAddressMemoEdit";
            this.SiteAddressMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteAddressMemoEdit, true);
            this.SiteAddressMemoEdit.Size = new System.Drawing.Size(358, 85);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteAddressMemoEdit, optionsSpelling16);
            this.SiteAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressMemoEdit.TabIndex = 77;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(36, 417);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(1071, 13);
            this.labelControl1.StyleController = this.dataLayoutControl1;
            this.labelControl1.TabIndex = 76;
            this.labelControl1.Text = "For Future Use";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record(s)", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(36, 417);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemHyperLinkEditSignature,
            this.repositoryItemButtonEditSignatureCapture});
            this.gridControl3.Size = new System.Drawing.Size(1071, 241);
            this.gridControl3.TabIndex = 40;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07265UTRiskAssessmentTeamSignaturesListBindingSource
            // 
            this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource.DataMember = "sp07265_UT_Risk_Assessment_Team_Signatures_List";
            this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubContractorSignatureID,
            this.colRiskAssessmentID1,
            this.colTeamMemberID,
            this.colDateTimeRecorded,
            this.colSignatureFile,
            this.colRemarks1,
            this.colGUID1,
            this.colRiskAssessmentDateTimeRecorded,
            this.colRiskAssessmentType,
            this.colRiskAssessmentTypeID1,
            this.colTeamMemberName,
            this.colTeamName,
            this.gridColumnSignatureCapture});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamMemberName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colSubContractorSignatureID
            // 
            this.colSubContractorSignatureID.Caption = "Sub-Contractor Signature ID";
            this.colSubContractorSignatureID.FieldName = "SubContractorSignatureID";
            this.colSubContractorSignatureID.Name = "colSubContractorSignatureID";
            this.colSubContractorSignatureID.OptionsColumn.AllowEdit = false;
            this.colSubContractorSignatureID.OptionsColumn.AllowFocus = false;
            this.colSubContractorSignatureID.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentID1
            // 
            this.colRiskAssessmentID1.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID1.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID1.Name = "colRiskAssessmentID1";
            this.colRiskAssessmentID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID1.Width = 114;
            // 
            // colTeamMemberID
            // 
            this.colTeamMemberID.Caption = "Team Member ID";
            this.colTeamMemberID.FieldName = "TeamMemberID";
            this.colTeamMemberID.Name = "colTeamMemberID";
            this.colTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID.Width = 102;
            // 
            // colDateTimeRecorded
            // 
            this.colDateTimeRecorded.Caption = "Date Recorded";
            this.colDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit1;
            this.colDateTimeRecorded.FieldName = "DateTimeRecorded";
            this.colDateTimeRecorded.Name = "colDateTimeRecorded";
            this.colDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeRecorded.Visible = true;
            this.colDateTimeRecorded.VisibleIndex = 1;
            this.colDateTimeRecorded.Width = 99;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colSignatureFile
            // 
            this.colSignatureFile.Caption = "Signature File";
            this.colSignatureFile.ColumnEdit = this.repositoryItemHyperLinkEditSignature;
            this.colSignatureFile.FieldName = "SignatureFile";
            this.colSignatureFile.Name = "colSignatureFile";
            this.colSignatureFile.OptionsColumn.ReadOnly = true;
            this.colSignatureFile.Visible = true;
            this.colSignatureFile.VisibleIndex = 2;
            this.colSignatureFile.Width = 308;
            // 
            // repositoryItemHyperLinkEditSignature
            // 
            this.repositoryItemHyperLinkEditSignature.AutoHeight = false;
            this.repositoryItemHyperLinkEditSignature.Name = "repositoryItemHyperLinkEditSignature";
            this.repositoryItemHyperLinkEditSignature.SingleClick = true;
            this.repositoryItemHyperLinkEditSignature.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditSignature_OpenLink);
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 4;
            this.colRemarks1.Width = 195;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentDateTimeRecorded
            // 
            this.colRiskAssessmentDateTimeRecorded.Caption = "Risk Assessment Date";
            this.colRiskAssessmentDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit1;
            this.colRiskAssessmentDateTimeRecorded.FieldName = "RiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.Name = "colRiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRiskAssessmentDateTimeRecorded.Width = 126;
            // 
            // colRiskAssessmentType
            // 
            this.colRiskAssessmentType.Caption = "Risk Assessment Type";
            this.colRiskAssessmentType.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType.Name = "colRiskAssessmentType";
            this.colRiskAssessmentType.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType.Width = 127;
            // 
            // colRiskAssessmentTypeID1
            // 
            this.colRiskAssessmentTypeID1.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID1.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID1.Name = "colRiskAssessmentTypeID1";
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID1.Width = 141;
            // 
            // colTeamMemberName
            // 
            this.colTeamMemberName.Caption = "Team Member";
            this.colTeamMemberName.FieldName = "TeamMemberName";
            this.colTeamMemberName.Name = "colTeamMemberName";
            this.colTeamMemberName.OptionsColumn.AllowEdit = false;
            this.colTeamMemberName.OptionsColumn.AllowFocus = false;
            this.colTeamMemberName.OptionsColumn.ReadOnly = true;
            this.colTeamMemberName.Visible = true;
            this.colTeamMemberName.VisibleIndex = 0;
            this.colTeamMemberName.Width = 212;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 4;
            this.colTeamName.Width = 294;
            // 
            // gridColumnSignatureCapture
            // 
            this.gridColumnSignatureCapture.Caption = "Signature Capture";
            this.gridColumnSignatureCapture.ColumnEdit = this.repositoryItemButtonEditSignatureCapture;
            this.gridColumnSignatureCapture.FieldName = "gridColumnSignatureCapture";
            this.gridColumnSignatureCapture.Name = "gridColumnSignatureCapture";
            this.gridColumnSignatureCapture.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnSignatureCapture.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnSignatureCapture.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnSignatureCapture.OptionsColumn.AllowShowHide = false;
            this.gridColumnSignatureCapture.OptionsColumn.AllowSize = false;
            this.gridColumnSignatureCapture.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnSignatureCapture.OptionsColumn.FixedWidth = true;
            this.gridColumnSignatureCapture.OptionsColumn.ReadOnly = true;
            this.gridColumnSignatureCapture.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnSignatureCapture.OptionsFilter.AllowFilter = false;
            this.gridColumnSignatureCapture.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnSignatureCapture.Visible = true;
            this.gridColumnSignatureCapture.VisibleIndex = 3;
            this.gridColumnSignatureCapture.Width = 100;
            // 
            // repositoryItemButtonEditSignatureCapture
            // 
            this.repositoryItemButtonEditSignatureCapture.AutoHeight = false;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem7.Text = "Capture Signature - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to open the Signature Capture Screen.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.repositoryItemButtonEditSignatureCapture.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Capture Signature", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "capture", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditSignatureCapture.Name = "repositoryItemButtonEditSignatureCapture";
            this.repositoryItemButtonEditSignatureCapture.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditSignatureCapture.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditSignatureCapture_ButtonClick);
            // 
            // VoltageTextEdit
            // 
            this.VoltageTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "Voltage", true));
            this.VoltageTextEdit.Location = new System.Drawing.Point(205, 140);
            this.VoltageTextEdit.MenuManager = this.barManager1;
            this.VoltageTextEdit.Name = "VoltageTextEdit";
            this.VoltageTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VoltageTextEdit, true);
            this.VoltageTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VoltageTextEdit, optionsSpelling17);
            this.VoltageTextEdit.StyleController = this.dataLayoutControl1;
            this.VoltageTextEdit.TabIndex = 75;
            // 
            // PoleNumberTextEdit
            // 
            this.PoleNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PoleNumber", true));
            this.PoleNumberTextEdit.Location = new System.Drawing.Point(205, 260);
            this.PoleNumberTextEdit.MenuManager = this.barManager1;
            this.PoleNumberTextEdit.Name = "PoleNumberTextEdit";
            this.PoleNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PoleNumberTextEdit, true);
            this.PoleNumberTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PoleNumberTextEdit, optionsSpelling18);
            this.PoleNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PoleNumberTextEdit.TabIndex = 74;
            // 
            // CircuitNumberTextEdit
            // 
            this.CircuitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "CircuitNumber", true));
            this.CircuitNumberTextEdit.Location = new System.Drawing.Point(205, 188);
            this.CircuitNumberTextEdit.MenuManager = this.barManager1;
            this.CircuitNumberTextEdit.Name = "CircuitNumberTextEdit";
            this.CircuitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNumberTextEdit, true);
            this.CircuitNumberTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNumberTextEdit, optionsSpelling19);
            this.CircuitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNumberTextEdit.TabIndex = 73;
            // 
            // CircuitNameTextEdit
            // 
            this.CircuitNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "CircuitName", true));
            this.CircuitNameTextEdit.Location = new System.Drawing.Point(205, 164);
            this.CircuitNameTextEdit.MenuManager = this.barManager1;
            this.CircuitNameTextEdit.Name = "CircuitNameTextEdit";
            this.CircuitNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNameTextEdit, true);
            this.CircuitNameTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNameTextEdit, optionsSpelling20);
            this.CircuitNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNameTextEdit.TabIndex = 72;
            // 
            // SubAreaNumberTextEdit
            // 
            this.SubAreaNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "SubAreaNumber", true));
            this.SubAreaNumberTextEdit.Location = new System.Drawing.Point(205, 236);
            this.SubAreaNumberTextEdit.MenuManager = this.barManager1;
            this.SubAreaNumberTextEdit.Name = "SubAreaNumberTextEdit";
            this.SubAreaNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubAreaNumberTextEdit, true);
            this.SubAreaNumberTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubAreaNumberTextEdit, optionsSpelling21);
            this.SubAreaNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaNumberTextEdit.TabIndex = 67;
            // 
            // SubAreaNameTextEdit
            // 
            this.SubAreaNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "SubAreaName", true));
            this.SubAreaNameTextEdit.Location = new System.Drawing.Point(205, 212);
            this.SubAreaNameTextEdit.MenuManager = this.barManager1;
            this.SubAreaNameTextEdit.Name = "SubAreaNameTextEdit";
            this.SubAreaNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubAreaNameTextEdit, true);
            this.SubAreaNameTextEdit.Size = new System.Drawing.Size(206, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubAreaNameTextEdit, optionsSpelling22);
            this.SubAreaNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaNameTextEdit.TabIndex = 66;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 108);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(1071, 206);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling23);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 62;
            // 
            // RiskAssessmentTypeTextEdit
            // 
            this.RiskAssessmentTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "RiskAssessmentType", true));
            this.RiskAssessmentTypeTextEdit.Location = new System.Drawing.Point(601, 12);
            this.RiskAssessmentTypeTextEdit.MenuManager = this.barManager1;
            this.RiskAssessmentTypeTextEdit.Name = "RiskAssessmentTypeTextEdit";
            this.RiskAssessmentTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RiskAssessmentTypeTextEdit, true);
            this.RiskAssessmentTypeTextEdit.Size = new System.Drawing.Size(530, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RiskAssessmentTypeTextEdit, optionsSpelling24);
            this.RiskAssessmentTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.RiskAssessmentTypeTextEdit.TabIndex = 61;
            // 
            // RiskAssessmentTypeIDTextEdit
            // 
            this.RiskAssessmentTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "RiskAssessmentTypeID", true));
            this.RiskAssessmentTypeIDTextEdit.Location = new System.Drawing.Point(172, 222);
            this.RiskAssessmentTypeIDTextEdit.MenuManager = this.barManager1;
            this.RiskAssessmentTypeIDTextEdit.Name = "RiskAssessmentTypeIDTextEdit";
            this.RiskAssessmentTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RiskAssessmentTypeIDTextEdit, true);
            this.RiskAssessmentTypeIDTextEdit.Size = new System.Drawing.Size(947, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RiskAssessmentTypeIDTextEdit, optionsSpelling25);
            this.RiskAssessmentTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RiskAssessmentTypeIDTextEdit.TabIndex = 57;
            // 
            // PersonCompletingIDTextEdit
            // 
            this.PersonCompletingIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PersonCompletingID", true));
            this.PersonCompletingIDTextEdit.Location = new System.Drawing.Point(159, 281);
            this.PersonCompletingIDTextEdit.MenuManager = this.barManager1;
            this.PersonCompletingIDTextEdit.Name = "PersonCompletingIDTextEdit";
            this.PersonCompletingIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonCompletingIDTextEdit, true);
            this.PersonCompletingIDTextEdit.Size = new System.Drawing.Size(948, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonCompletingIDTextEdit, optionsSpelling26);
            this.PersonCompletingIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonCompletingIDTextEdit.TabIndex = 50;
            // 
            // PersonCompletingTypeIDTextEdit
            // 
            this.PersonCompletingTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PersonCompletingTypeID", true));
            this.PersonCompletingTypeIDTextEdit.Location = new System.Drawing.Point(174, 293);
            this.PersonCompletingTypeIDTextEdit.MenuManager = this.barManager1;
            this.PersonCompletingTypeIDTextEdit.Name = "PersonCompletingTypeIDTextEdit";
            this.PersonCompletingTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonCompletingTypeIDTextEdit, true);
            this.PersonCompletingTypeIDTextEdit.Size = new System.Drawing.Size(945, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonCompletingTypeIDTextEdit, optionsSpelling27);
            this.PersonCompletingTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonCompletingTypeIDTextEdit.TabIndex = 47;
            // 
            // SurveyedPoleIDTextEdit
            // 
            this.SurveyedPoleIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "SurveyedPoleID", true));
            this.SurveyedPoleIDTextEdit.Location = new System.Drawing.Point(126, 293);
            this.SurveyedPoleIDTextEdit.MenuManager = this.barManager1;
            this.SurveyedPoleIDTextEdit.Name = "SurveyedPoleIDTextEdit";
            this.SurveyedPoleIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyedPoleIDTextEdit, true);
            this.SurveyedPoleIDTextEdit.Size = new System.Drawing.Size(993, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyedPoleIDTextEdit, optionsSpelling28);
            this.SurveyedPoleIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyedPoleIDTextEdit.TabIndex = 45;
            // 
            // PersonCompletingNameTextEdit
            // 
            this.PersonCompletingNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PersonCompletingName", true));
            this.PersonCompletingNameTextEdit.Location = new System.Drawing.Point(596, 140);
            this.PersonCompletingNameTextEdit.MenuManager = this.barManager1;
            this.PersonCompletingNameTextEdit.Name = "PersonCompletingNameTextEdit";
            this.PersonCompletingNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonCompletingNameTextEdit, true);
            this.PersonCompletingNameTextEdit.Size = new System.Drawing.Size(174, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonCompletingNameTextEdit, optionsSpelling29);
            this.PersonCompletingNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonCompletingNameTextEdit.TabIndex = 60;
            // 
            // PersonCompletingTypeTextEdit
            // 
            this.PersonCompletingTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PersonCompletingType", true));
            this.PersonCompletingTypeTextEdit.Location = new System.Drawing.Point(931, 140);
            this.PersonCompletingTypeTextEdit.MenuManager = this.barManager1;
            this.PersonCompletingTypeTextEdit.Name = "PersonCompletingTypeTextEdit";
            this.PersonCompletingTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PersonCompletingTypeTextEdit, true);
            this.PersonCompletingTypeTextEdit.Size = new System.Drawing.Size(164, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PersonCompletingTypeTextEdit, optionsSpelling30);
            this.PersonCompletingTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.PersonCompletingTypeTextEdit.TabIndex = 59;
            // 
            // LastUpdatedTextEdit
            // 
            this.LastUpdatedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "LastUpdated", true));
            this.LastUpdatedTextEdit.Location = new System.Drawing.Point(931, 164);
            this.LastUpdatedTextEdit.MenuManager = this.barManager1;
            this.LastUpdatedTextEdit.Name = "LastUpdatedTextEdit";
            this.LastUpdatedTextEdit.Properties.Mask.EditMask = "g";
            this.LastUpdatedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.LastUpdatedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LastUpdatedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LastUpdatedTextEdit, true);
            this.LastUpdatedTextEdit.Size = new System.Drawing.Size(164, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LastUpdatedTextEdit, optionsSpelling31);
            this.LastUpdatedTextEdit.StyleController = this.dataLayoutControl1;
            this.LastUpdatedTextEdit.TabIndex = 58;
            // 
            // NRSWAOpeningNoticeNoTextEdit
            // 
            this.NRSWAOpeningNoticeNoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "NRSWAOpeningNoticeNo", true));
            this.NRSWAOpeningNoticeNoTextEdit.Location = new System.Drawing.Point(931, 236);
            this.NRSWAOpeningNoticeNoTextEdit.MenuManager = this.barManager1;
            this.NRSWAOpeningNoticeNoTextEdit.Name = "NRSWAOpeningNoticeNoTextEdit";
            this.NRSWAOpeningNoticeNoTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NRSWAOpeningNoticeNoTextEdit, true);
            this.NRSWAOpeningNoticeNoTextEdit.Size = new System.Drawing.Size(164, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NRSWAOpeningNoticeNoTextEdit, optionsSpelling32);
            this.NRSWAOpeningNoticeNoTextEdit.StyleController = this.dataLayoutControl1;
            this.NRSWAOpeningNoticeNoTextEdit.TabIndex = 56;
            // 
            // PermitHolderNameTextEdit
            // 
            this.PermitHolderNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PermitHolderName", true));
            this.PermitHolderNameTextEdit.Location = new System.Drawing.Point(931, 188);
            this.PermitHolderNameTextEdit.MenuManager = this.barManager1;
            this.PermitHolderNameTextEdit.Name = "PermitHolderNameTextEdit";
            this.PermitHolderNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PermitHolderNameTextEdit, true);
            this.PermitHolderNameTextEdit.Size = new System.Drawing.Size(164, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PermitHolderNameTextEdit, optionsSpelling33);
            this.PermitHolderNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PermitHolderNameTextEdit.TabIndex = 55;
            // 
            // TypeOfWorkingIDGridLookUpEdit
            // 
            this.TypeOfWorkingIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "TypeOfWorkingID", true));
            this.TypeOfWorkingIDGridLookUpEdit.Location = new System.Drawing.Point(596, 236);
            this.TypeOfWorkingIDGridLookUpEdit.MenuManager = this.barManager1;
            this.TypeOfWorkingIDGridLookUpEdit.Name = "TypeOfWorkingIDGridLookUpEdit";
            this.TypeOfWorkingIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TypeOfWorkingIDGridLookUpEdit.Properties.DataSource = this.sp07263UTTypeOfWorkWithBlankBindingSource;
            this.TypeOfWorkingIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TypeOfWorkingIDGridLookUpEdit.Properties.NullText = "";
            this.TypeOfWorkingIDGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.TypeOfWorkingIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.TypeOfWorkingIDGridLookUpEdit.Size = new System.Drawing.Size(174, 20);
            this.TypeOfWorkingIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TypeOfWorkingIDGridLookUpEdit.TabIndex = 54;
            // 
            // sp07263UTTypeOfWorkWithBlankBindingSource
            // 
            this.sp07263UTTypeOfWorkWithBlankBindingSource.DataMember = "sp07263_UT_Type_Of_Work_With_Blank";
            this.sp07263UTTypeOfWorkWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Description";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 198;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Record Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 100;
            // 
            // ElectricalRiskCategoryIDGridLookUpEdit
            // 
            this.ElectricalRiskCategoryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "ElectricalRiskCategoryID", true));
            this.ElectricalRiskCategoryIDGridLookUpEdit.Location = new System.Drawing.Point(596, 212);
            this.ElectricalRiskCategoryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ElectricalRiskCategoryIDGridLookUpEdit.Name = "ElectricalRiskCategoryIDGridLookUpEdit";
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.DataSource = this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource;
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.NullText = "";
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.ElectricalRiskCategoryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ElectricalRiskCategoryIDGridLookUpEdit.Size = new System.Drawing.Size(174, 20);
            this.ElectricalRiskCategoryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ElectricalRiskCategoryIDGridLookUpEdit.TabIndex = 53;
            // 
            // sp07264UTElectricalRiskCategoriesWithBlankBindingSource
            // 
            this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource.DataMember = "sp07264_UT_Electrical_Risk_Categories_With_Blank";
            this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 198;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Record Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 100;
            // 
            // ElectricalAssessmentVerifiedCheckEdit
            // 
            this.ElectricalAssessmentVerifiedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "ElectricalAssessmentVerified", true));
            this.ElectricalAssessmentVerifiedCheckEdit.Location = new System.Drawing.Point(931, 212);
            this.ElectricalAssessmentVerifiedCheckEdit.MenuManager = this.barManager1;
            this.ElectricalAssessmentVerifiedCheckEdit.Name = "ElectricalAssessmentVerifiedCheckEdit";
            this.ElectricalAssessmentVerifiedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ElectricalAssessmentVerifiedCheckEdit.Properties.ValueChecked = 1;
            this.ElectricalAssessmentVerifiedCheckEdit.Properties.ValueUnchecked = 0;
            this.ElectricalAssessmentVerifiedCheckEdit.Size = new System.Drawing.Size(164, 19);
            this.ElectricalAssessmentVerifiedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ElectricalAssessmentVerifiedCheckEdit.TabIndex = 52;
            // 
            // PermissionCheckedCheckEdit
            // 
            this.PermissionCheckedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "PermissionChecked", true));
            this.PermissionCheckedCheckEdit.Location = new System.Drawing.Point(596, 188);
            this.PermissionCheckedCheckEdit.MenuManager = this.barManager1;
            this.PermissionCheckedCheckEdit.Name = "PermissionCheckedCheckEdit";
            this.PermissionCheckedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.PermissionCheckedCheckEdit.Properties.ValueChecked = 1;
            this.PermissionCheckedCheckEdit.Properties.ValueUnchecked = 0;
            this.PermissionCheckedCheckEdit.Size = new System.Drawing.Size(174, 19);
            this.PermissionCheckedCheckEdit.StyleController = this.dataLayoutControl1;
            this.PermissionCheckedCheckEdit.TabIndex = 51;
            // 
            // DateTimeRecordedTextEdit
            // 
            this.DateTimeRecordedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "DateTimeRecorded", true));
            this.DateTimeRecordedTextEdit.Location = new System.Drawing.Point(596, 164);
            this.DateTimeRecordedTextEdit.MenuManager = this.barManager1;
            this.DateTimeRecordedTextEdit.Name = "DateTimeRecordedTextEdit";
            this.DateTimeRecordedTextEdit.Properties.Mask.EditMask = "g";
            this.DateTimeRecordedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.DateTimeRecordedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateTimeRecordedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DateTimeRecordedTextEdit, true);
            this.DateTimeRecordedTextEdit.Size = new System.Drawing.Size(174, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DateTimeRecordedTextEdit, optionsSpelling34);
            this.DateTimeRecordedTextEdit.StyleController = this.dataLayoutControl1;
            this.DateTimeRecordedTextEdit.TabIndex = 49;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07256UTRiskAssessmentQuestionItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 40;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // RiskAssessmentIDTextEdit
            // 
            this.RiskAssessmentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07256UTRiskAssessmentQuestionItemBindingSource, "RiskAssessmentID", true));
            this.RiskAssessmentIDTextEdit.Location = new System.Drawing.Point(126, 317);
            this.RiskAssessmentIDTextEdit.MenuManager = this.barManager1;
            this.RiskAssessmentIDTextEdit.Name = "RiskAssessmentIDTextEdit";
            this.RiskAssessmentIDTextEdit.Properties.Mask.EditMask = "0000000";
            this.RiskAssessmentIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.RiskAssessmentIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RiskAssessmentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RiskAssessmentIDTextEdit, true);
            this.RiskAssessmentIDTextEdit.Size = new System.Drawing.Size(993, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RiskAssessmentIDTextEdit, optionsSpelling35);
            this.RiskAssessmentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RiskAssessmentIDTextEdit.TabIndex = 4;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 417);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1071, 241);
            this.gridSplitContainer1.TabIndex = 42;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemDateEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemGridLookUpEditAnswer});
            this.gridControl1.Size = new System.Drawing.Size(1071, 241);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource
            // 
            this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource.DataMember = "sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_Edit";
            this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskQuestionID,
            this.colRiskAssessmentID,
            this.colQuestionDescription,
            this.colQuestionNotes,
            this.colQuestionOrder,
            this.colAnswer,
            this.colRemarks,
            this.colMasterQuestionID,
            this.colGUID,
            this.colAnswerText1,
            this.colAnswerText2,
            this.colAnswerText3,
            this.colRiskAssessmentTypeID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuestionOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colRiskQuestionID
            // 
            this.colRiskQuestionID.Caption = "Risk Question ID";
            this.colRiskQuestionID.FieldName = "RiskQuestionID";
            this.colRiskQuestionID.Name = "colRiskQuestionID";
            this.colRiskQuestionID.OptionsColumn.AllowEdit = false;
            this.colRiskQuestionID.OptionsColumn.AllowFocus = false;
            this.colRiskQuestionID.OptionsColumn.ReadOnly = true;
            this.colRiskQuestionID.Width = 100;
            // 
            // colRiskAssessmentID
            // 
            this.colRiskAssessmentID.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID.Name = "colRiskAssessmentID";
            this.colRiskAssessmentID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID.Width = 114;
            // 
            // colQuestionDescription
            // 
            this.colQuestionDescription.Caption = "Question Description";
            this.colQuestionDescription.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colQuestionDescription.FieldName = "QuestionDescription";
            this.colQuestionDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionDescription.Name = "colQuestionDescription";
            this.colQuestionDescription.OptionsColumn.ReadOnly = true;
            this.colQuestionDescription.Visible = true;
            this.colQuestionDescription.VisibleIndex = 1;
            this.colQuestionDescription.Width = 515;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colQuestionNotes
            // 
            this.colQuestionNotes.Caption = "Question Notes";
            this.colQuestionNotes.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colQuestionNotes.FieldName = "QuestionNotes";
            this.colQuestionNotes.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionNotes.Name = "colQuestionNotes";
            this.colQuestionNotes.OptionsColumn.ReadOnly = true;
            this.colQuestionNotes.Visible = true;
            this.colQuestionNotes.VisibleIndex = 2;
            this.colQuestionNotes.Width = 140;
            // 
            // colQuestionOrder
            // 
            this.colQuestionOrder.Caption = "#";
            this.colQuestionOrder.FieldName = "QuestionOrder";
            this.colQuestionOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionOrder.Name = "colQuestionOrder";
            this.colQuestionOrder.OptionsColumn.AllowEdit = false;
            this.colQuestionOrder.OptionsColumn.AllowFocus = false;
            this.colQuestionOrder.OptionsColumn.ReadOnly = true;
            this.colQuestionOrder.Visible = true;
            this.colQuestionOrder.VisibleIndex = 0;
            this.colQuestionOrder.Width = 42;
            // 
            // colAnswer
            // 
            this.colAnswer.Caption = "Answer";
            this.colAnswer.ColumnEdit = this.repositoryItemGridLookUpEditAnswer;
            this.colAnswer.FieldName = "Answer";
            this.colAnswer.Name = "colAnswer";
            this.colAnswer.Visible = true;
            this.colAnswer.VisibleIndex = 3;
            // 
            // repositoryItemGridLookUpEditAnswer
            // 
            this.repositoryItemGridLookUpEditAnswer.AutoHeight = false;
            this.repositoryItemGridLookUpEditAnswer.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditAnswer.DataSource = this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource;
            this.repositoryItemGridLookUpEditAnswer.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditAnswer.Name = "repositoryItemGridLookUpEditAnswer";
            this.repositoryItemGridLookUpEditAnswer.NullText = "";
            this.repositoryItemGridLookUpEditAnswer.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditAnswer.ValueMember = "ID";
            this.repositoryItemGridLookUpEditAnswer.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemGridLookUpEditAnswer_EditValueChanging);
            // 
            // sp07417UTRiskAssessmentQuestionAnswersListBindingSource
            // 
            this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource.DataMember = "sp07417_UT_Risk_Assessment_Question_Answers_List";
            this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource.DataSource = this.dataSet_UT;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Answer";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 135;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            this.colRemarks.Width = 175;
            // 
            // colMasterQuestionID
            // 
            this.colMasterQuestionID.Caption = "Master Question ID";
            this.colMasterQuestionID.FieldName = "MasterQuestionID";
            this.colMasterQuestionID.Name = "colMasterQuestionID";
            this.colMasterQuestionID.OptionsColumn.AllowEdit = false;
            this.colMasterQuestionID.OptionsColumn.AllowFocus = false;
            this.colMasterQuestionID.OptionsColumn.ReadOnly = true;
            this.colMasterQuestionID.Width = 114;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colAnswerText1
            // 
            this.colAnswerText1.Caption = "Significant Hazards";
            this.colAnswerText1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAnswerText1.FieldName = "AnswerText1";
            this.colAnswerText1.Name = "colAnswerText1";
            this.colAnswerText1.Visible = true;
            this.colAnswerText1.VisibleIndex = 4;
            this.colAnswerText1.Width = 225;
            // 
            // colAnswerText2
            // 
            this.colAnswerText2.Caption = "People Affected";
            this.colAnswerText2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAnswerText2.FieldName = "AnswerText2";
            this.colAnswerText2.Name = "colAnswerText2";
            this.colAnswerText2.Visible = true;
            this.colAnswerText2.VisibleIndex = 5;
            this.colAnswerText2.Width = 225;
            // 
            // colAnswerText3
            // 
            this.colAnswerText3.Caption = "Controls";
            this.colAnswerText3.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colAnswerText3.FieldName = "AnswerText3";
            this.colAnswerText3.Name = "colAnswerText3";
            this.colAnswerText3.Visible = true;
            this.colAnswerText3.VisibleIndex = 6;
            this.colAnswerText3.Width = 225;
            // 
            // colRiskAssessmentTypeID
            // 
            this.colRiskAssessmentTypeID.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.Name = "colRiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID.Width = 141;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // ItemForRiskAssessmentID
            // 
            this.ItemForRiskAssessmentID.Control = this.RiskAssessmentIDTextEdit;
            this.ItemForRiskAssessmentID.CustomizationFormText = "Risk Assessment ID:";
            this.ItemForRiskAssessmentID.Location = new System.Drawing.Point(0, 247);
            this.ItemForRiskAssessmentID.Name = "ItemForRiskAssessmentID";
            this.ItemForRiskAssessmentID.Size = new System.Drawing.Size(1099, 24);
            this.ItemForRiskAssessmentID.Text = "Risk Assessment ID:";
            this.ItemForRiskAssessmentID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForSurveyedPoleID
            // 
            this.ItemForSurveyedPoleID.Control = this.SurveyedPoleIDTextEdit;
            this.ItemForSurveyedPoleID.CustomizationFormText = "Surveyed Pole ID:";
            this.ItemForSurveyedPoleID.Location = new System.Drawing.Point(0, 223);
            this.ItemForSurveyedPoleID.Name = "ItemForSurveyedPoleID";
            this.ItemForSurveyedPoleID.Size = new System.Drawing.Size(1099, 24);
            this.ItemForSurveyedPoleID.Text = "Surveyed Pole ID:";
            this.ItemForSurveyedPoleID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPersonCompletingTypeID
            // 
            this.ItemForPersonCompletingTypeID.Control = this.PersonCompletingTypeIDTextEdit;
            this.ItemForPersonCompletingTypeID.CustomizationFormText = "Completed By Person Type ID:";
            this.ItemForPersonCompletingTypeID.Location = new System.Drawing.Point(0, 223);
            this.ItemForPersonCompletingTypeID.Name = "ItemForPersonCompletingTypeID";
            this.ItemForPersonCompletingTypeID.Size = new System.Drawing.Size(1099, 24);
            this.ItemForPersonCompletingTypeID.Text = "Completed By Person Type ID:";
            this.ItemForPersonCompletingTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPersonCompletingID
            // 
            this.ItemForPersonCompletingID.Control = this.PersonCompletingIDTextEdit;
            this.ItemForPersonCompletingID.CustomizationFormText = "Completed By Person ID:";
            this.ItemForPersonCompletingID.Location = new System.Drawing.Point(0, 120);
            this.ItemForPersonCompletingID.Name = "ItemForPersonCompletingID";
            this.ItemForPersonCompletingID.Size = new System.Drawing.Size(1075, 24);
            this.ItemForPersonCompletingID.Text = "Completed By Person ID:";
            this.ItemForPersonCompletingID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRiskAssessmentTypeID
            // 
            this.ItemForRiskAssessmentTypeID.Control = this.RiskAssessmentTypeIDTextEdit;
            this.ItemForRiskAssessmentTypeID.CustomizationFormText = "Risk Assessment Type ID:";
            this.ItemForRiskAssessmentTypeID.Location = new System.Drawing.Point(0, 152);
            this.ItemForRiskAssessmentTypeID.Name = "ItemForRiskAssessmentTypeID";
            this.ItemForRiskAssessmentTypeID.Size = new System.Drawing.Size(1099, 24);
            this.ItemForRiskAssessmentTypeID.Text = "Risk Assessment Type ID:";
            this.ItemForRiskAssessmentTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPrimaryNumber
            // 
            this.ItemForPrimaryNumber.Control = this.PrimaryNumberTextEdit;
            this.ItemForPrimaryNumber.CustomizationFormText = "Primary Number:";
            this.ItemForPrimaryNumber.Location = new System.Drawing.Point(0, 144);
            this.ItemForPrimaryNumber.Name = "ItemForPrimaryNumber";
            this.ItemForPrimaryNumber.Size = new System.Drawing.Size(406, 24);
            this.ItemForPrimaryNumber.Text = "Primary Number:";
            this.ItemForPrimaryNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFeederName
            // 
            this.ItemForFeederName.Control = this.FeederNameTextEdit;
            this.ItemForFeederName.CustomizationFormText = "Feeder Name:";
            this.ItemForFeederName.Location = new System.Drawing.Point(0, 154);
            this.ItemForFeederName.Name = "ItemForFeederName";
            this.ItemForFeederName.Size = new System.Drawing.Size(406, 24);
            this.ItemForFeederName.Text = "Feeder Name:";
            this.ItemForFeederName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFeederNumber
            // 
            this.ItemForFeederNumber.Control = this.FeederNumberTextEdit;
            this.ItemForFeederNumber.CustomizationFormText = "Feeder Number:";
            this.ItemForFeederNumber.Location = new System.Drawing.Point(0, 144);
            this.ItemForFeederNumber.Name = "ItemForFeederNumber";
            this.ItemForFeederNumber.Size = new System.Drawing.Size(406, 24);
            this.ItemForFeederNumber.Text = "Feeder Number:";
            this.ItemForFeederNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPrimaryName
            // 
            this.ItemForPrimaryName.Control = this.PrimaryNameTextEdit;
            this.ItemForPrimaryName.CustomizationFormText = "Primary Name:";
            this.ItemForPrimaryName.Location = new System.Drawing.Point(0, 120);
            this.ItemForPrimaryName.Name = "ItemForPrimaryName";
            this.ItemForPrimaryName.Size = new System.Drawing.Size(406, 24);
            this.ItemForPrimaryName.Text = "Primary Name:";
            this.ItemForPrimaryName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameTextEdit;
            this.ItemForRegionName.CustomizationFormText = "Region Name:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 24);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(406, 24);
            this.ItemForRegionName.Text = "Region Name:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(406, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(145, 13);
            // 
            // ItemForRegionNumber
            // 
            this.ItemForRegionNumber.Control = this.RegionNumberTextEdit;
            this.ItemForRegionNumber.CustomizationFormText = "Region Number:";
            this.ItemForRegionNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForRegionNumber.Name = "ItemForRegionNumber";
            this.ItemForRegionNumber.Size = new System.Drawing.Size(406, 24);
            this.ItemForRegionNumber.Text = "Region Number:";
            this.ItemForRegionNumber.TextSize = new System.Drawing.Size(145, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1143, 694);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.simpleSeparator6,
            this.ItemForRiskAssessmentType,
            this.splitterItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1123, 336);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Risk Assessment Header";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup3});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 26);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1123, 304);
            this.layoutControlGroup13.Text = "Risk Assessment Header";
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.CustomizationFormText = "tabbedControlGroup3";
            this.tabbedControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup3.Name = "tabbedControlGroup3";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup3.SelectedTabPageIndex = 0;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1099, 258);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroupSiteSpecific,
            this.layoutControlGroup7});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.layoutControlGroupLocation,
            this.layoutControlGroupOther});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1075, 210);
            this.layoutControlGroup4.Text = "Details";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 188);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1075, 22);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroupLocation
            // 
            this.layoutControlGroupLocation.CustomizationFormText = "Location";
            this.layoutControlGroupLocation.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVoltage,
            this.ItemForCircuitName,
            this.ItemForCircuitNumber,
            this.ItemForSubAreaName,
            this.ItemForSubAreaNumber,
            this.ItemForPoleNumber});
            this.layoutControlGroupLocation.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupLocation.Name = "layoutControlGroupLocation";
            this.layoutControlGroupLocation.Size = new System.Drawing.Size(391, 188);
            this.layoutControlGroupLocation.Text = "Location";
            // 
            // ItemForVoltage
            // 
            this.ItemForVoltage.Control = this.VoltageTextEdit;
            this.ItemForVoltage.CustomizationFormText = "Voltage:";
            this.ItemForVoltage.Location = new System.Drawing.Point(0, 0);
            this.ItemForVoltage.Name = "ItemForVoltage";
            this.ItemForVoltage.Size = new System.Drawing.Size(367, 24);
            this.ItemForVoltage.Text = "Voltage:";
            this.ItemForVoltage.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForCircuitName
            // 
            this.ItemForCircuitName.Control = this.CircuitNameTextEdit;
            this.ItemForCircuitName.CustomizationFormText = "Circuit Name:";
            this.ItemForCircuitName.Location = new System.Drawing.Point(0, 24);
            this.ItemForCircuitName.Name = "ItemForCircuitName";
            this.ItemForCircuitName.Size = new System.Drawing.Size(367, 24);
            this.ItemForCircuitName.Text = "Circuit Name:";
            this.ItemForCircuitName.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForCircuitNumber
            // 
            this.ItemForCircuitNumber.Control = this.CircuitNumberTextEdit;
            this.ItemForCircuitNumber.CustomizationFormText = "Circuit Number:";
            this.ItemForCircuitNumber.Location = new System.Drawing.Point(0, 48);
            this.ItemForCircuitNumber.Name = "ItemForCircuitNumber";
            this.ItemForCircuitNumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForCircuitNumber.Text = "Circuit Number:";
            this.ItemForCircuitNumber.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForSubAreaName
            // 
            this.ItemForSubAreaName.Control = this.SubAreaNameTextEdit;
            this.ItemForSubAreaName.CustomizationFormText = "Sub-Area Name:";
            this.ItemForSubAreaName.Location = new System.Drawing.Point(0, 72);
            this.ItemForSubAreaName.Name = "ItemForSubAreaName";
            this.ItemForSubAreaName.Size = new System.Drawing.Size(367, 24);
            this.ItemForSubAreaName.Text = "Sub-Area Name:";
            this.ItemForSubAreaName.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForSubAreaNumber
            // 
            this.ItemForSubAreaNumber.Control = this.SubAreaNumberTextEdit;
            this.ItemForSubAreaNumber.CustomizationFormText = "Sub-Area Number:";
            this.ItemForSubAreaNumber.Location = new System.Drawing.Point(0, 96);
            this.ItemForSubAreaNumber.Name = "ItemForSubAreaNumber";
            this.ItemForSubAreaNumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForSubAreaNumber.Text = "Sub-Area Number:";
            this.ItemForSubAreaNumber.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForPoleNumber
            // 
            this.ItemForPoleNumber.Control = this.PoleNumberTextEdit;
            this.ItemForPoleNumber.CustomizationFormText = "Pole Number:";
            this.ItemForPoleNumber.Location = new System.Drawing.Point(0, 120);
            this.ItemForPoleNumber.Name = "ItemForPoleNumber";
            this.ItemForPoleNumber.Size = new System.Drawing.Size(367, 24);
            this.ItemForPoleNumber.Text = "Pole Number:";
            this.ItemForPoleNumber.TextSize = new System.Drawing.Size(154, 13);
            // 
            // layoutControlGroupOther
            // 
            this.layoutControlGroupOther.CustomizationFormText = "Other";
            this.layoutControlGroupOther.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPersonCompletingName,
            this.ItemForPersonCompletingType,
            this.ItemForElectricalRiskCategoryID,
            this.ItemForPermissionChecked,
            this.ItemForDateTimeRecorded,
            this.ItemForLastUpdated,
            this.ItemForElectricalAssessmentVerified,
            this.ItemForPermitHolderName,
            this.ItemForTypeOfWorkingID,
            this.ItemForNRSWAOpeningNoticeNo});
            this.layoutControlGroupOther.Location = new System.Drawing.Point(391, 0);
            this.layoutControlGroupOther.Name = "layoutControlGroupOther";
            this.layoutControlGroupOther.Size = new System.Drawing.Size(684, 188);
            this.layoutControlGroupOther.Text = "Other";
            // 
            // ItemForPersonCompletingName
            // 
            this.ItemForPersonCompletingName.Control = this.PersonCompletingNameTextEdit;
            this.ItemForPersonCompletingName.CustomizationFormText = "Person Completing Name:";
            this.ItemForPersonCompletingName.Location = new System.Drawing.Point(0, 0);
            this.ItemForPersonCompletingName.Name = "ItemForPersonCompletingName";
            this.ItemForPersonCompletingName.Size = new System.Drawing.Size(335, 24);
            this.ItemForPersonCompletingName.Text = "Person Completing Name:";
            this.ItemForPersonCompletingName.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForPersonCompletingType
            // 
            this.ItemForPersonCompletingType.Control = this.PersonCompletingTypeTextEdit;
            this.ItemForPersonCompletingType.CustomizationFormText = "Person Completing Type:";
            this.ItemForPersonCompletingType.Location = new System.Drawing.Point(335, 0);
            this.ItemForPersonCompletingType.Name = "ItemForPersonCompletingType";
            this.ItemForPersonCompletingType.Size = new System.Drawing.Size(325, 24);
            this.ItemForPersonCompletingType.Text = "Person Completing Type:";
            this.ItemForPersonCompletingType.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForElectricalRiskCategoryID
            // 
            this.ItemForElectricalRiskCategoryID.Control = this.ElectricalRiskCategoryIDGridLookUpEdit;
            this.ItemForElectricalRiskCategoryID.CustomizationFormText = "Electrical Risk Category:";
            this.ItemForElectricalRiskCategoryID.Location = new System.Drawing.Point(0, 72);
            this.ItemForElectricalRiskCategoryID.MaxSize = new System.Drawing.Size(0, 24);
            this.ItemForElectricalRiskCategoryID.MinSize = new System.Drawing.Size(202, 24);
            this.ItemForElectricalRiskCategoryID.Name = "ItemForElectricalRiskCategoryID";
            this.ItemForElectricalRiskCategoryID.Size = new System.Drawing.Size(335, 24);
            this.ItemForElectricalRiskCategoryID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForElectricalRiskCategoryID.Text = "Electrical Risk Category:";
            this.ItemForElectricalRiskCategoryID.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForPermissionChecked
            // 
            this.ItemForPermissionChecked.Control = this.PermissionCheckedCheckEdit;
            this.ItemForPermissionChecked.CustomizationFormText = "Permission Checked:";
            this.ItemForPermissionChecked.Location = new System.Drawing.Point(0, 48);
            this.ItemForPermissionChecked.Name = "ItemForPermissionChecked";
            this.ItemForPermissionChecked.Size = new System.Drawing.Size(335, 24);
            this.ItemForPermissionChecked.Text = "Permission Checked:";
            this.ItemForPermissionChecked.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForDateTimeRecorded
            // 
            this.ItemForDateTimeRecorded.Control = this.DateTimeRecordedTextEdit;
            this.ItemForDateTimeRecorded.CustomizationFormText = "Date Recorded:";
            this.ItemForDateTimeRecorded.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateTimeRecorded.Name = "ItemForDateTimeRecorded";
            this.ItemForDateTimeRecorded.Size = new System.Drawing.Size(335, 24);
            this.ItemForDateTimeRecorded.Text = "Date Recorded:";
            this.ItemForDateTimeRecorded.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForLastUpdated
            // 
            this.ItemForLastUpdated.Control = this.LastUpdatedTextEdit;
            this.ItemForLastUpdated.CustomizationFormText = "Last Updated:";
            this.ItemForLastUpdated.Location = new System.Drawing.Point(335, 24);
            this.ItemForLastUpdated.Name = "ItemForLastUpdated";
            this.ItemForLastUpdated.Size = new System.Drawing.Size(325, 24);
            this.ItemForLastUpdated.Text = "Last Updated:";
            this.ItemForLastUpdated.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForElectricalAssessmentVerified
            // 
            this.ItemForElectricalAssessmentVerified.Control = this.ElectricalAssessmentVerifiedCheckEdit;
            this.ItemForElectricalAssessmentVerified.CustomizationFormText = "Electrical Assessment Verified:";
            this.ItemForElectricalAssessmentVerified.Location = new System.Drawing.Point(335, 72);
            this.ItemForElectricalAssessmentVerified.Name = "ItemForElectricalAssessmentVerified";
            this.ItemForElectricalAssessmentVerified.Size = new System.Drawing.Size(325, 24);
            this.ItemForElectricalAssessmentVerified.Text = "Electrical Assessment Verified:";
            this.ItemForElectricalAssessmentVerified.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForPermitHolderName
            // 
            this.ItemForPermitHolderName.Control = this.PermitHolderNameTextEdit;
            this.ItemForPermitHolderName.CustomizationFormText = "Permit Holder Name:";
            this.ItemForPermitHolderName.Location = new System.Drawing.Point(335, 48);
            this.ItemForPermitHolderName.Name = "ItemForPermitHolderName";
            this.ItemForPermitHolderName.Size = new System.Drawing.Size(325, 24);
            this.ItemForPermitHolderName.Text = "Permit Holder Name:";
            this.ItemForPermitHolderName.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForTypeOfWorkingID
            // 
            this.ItemForTypeOfWorkingID.Control = this.TypeOfWorkingIDGridLookUpEdit;
            this.ItemForTypeOfWorkingID.CustomizationFormText = "Type Of Working:";
            this.ItemForTypeOfWorkingID.Location = new System.Drawing.Point(0, 96);
            this.ItemForTypeOfWorkingID.Name = "ItemForTypeOfWorkingID";
            this.ItemForTypeOfWorkingID.Size = new System.Drawing.Size(335, 48);
            this.ItemForTypeOfWorkingID.Text = "Type Of Working:";
            this.ItemForTypeOfWorkingID.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForNRSWAOpeningNoticeNo
            // 
            this.ItemForNRSWAOpeningNoticeNo.Control = this.NRSWAOpeningNoticeNoTextEdit;
            this.ItemForNRSWAOpeningNoticeNo.CustomizationFormText = "NRSWA Opening Notice #:";
            this.ItemForNRSWAOpeningNoticeNo.Location = new System.Drawing.Point(335, 96);
            this.ItemForNRSWAOpeningNoticeNo.Name = "ItemForNRSWAOpeningNoticeNo";
            this.ItemForNRSWAOpeningNoticeNo.Size = new System.Drawing.Size(325, 48);
            this.ItemForNRSWAOpeningNoticeNo.Text = "NRSWA Opening Notice #:";
            this.ItemForNRSWAOpeningNoticeNo.TextSize = new System.Drawing.Size(154, 13);
            // 
            // layoutControlGroupSiteSpecific
            // 
            this.layoutControlGroupSiteSpecific.CustomizationFormText = "Site Specific Emergency Procedures";
            this.layoutControlGroupSiteSpecific.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSiteAddress,
            this.ItemForMEWPArialRescuers,
            this.ItemForEmergencyKitLocation,
            this.ItemForAirAmbulanceLat,
            this.ItemForBanksMan,
            this.ItemForAirAmbulanceLong,
            this.ItemForEmergencyServicesMeetPoint,
            this.ItemForMobilePhoneSignalBars,
            this.emptySpaceItem1,
            this.ItemForNearestAandE,
            this.ItemForNearestLandLine});
            this.layoutControlGroupSiteSpecific.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupSiteSpecific.Name = "layoutControlGroupSiteSpecific";
            this.layoutControlGroupSiteSpecific.Size = new System.Drawing.Size(1075, 210);
            this.layoutControlGroupSiteSpecific.Text = "Site Specific Emergency Procedures";
            // 
            // ItemForSiteAddress
            // 
            this.ItemForSiteAddress.Control = this.SiteAddressMemoEdit;
            this.ItemForSiteAddress.CustomizationFormText = "Site Address and Access Route:";
            this.ItemForSiteAddress.Location = new System.Drawing.Point(0, 0);
            this.ItemForSiteAddress.Name = "ItemForSiteAddress";
            this.ItemForSiteAddress.Size = new System.Drawing.Size(519, 89);
            this.ItemForSiteAddress.Text = "Site Address and Access Route:";
            this.ItemForSiteAddress.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForMEWPArialRescuers
            // 
            this.ItemForMEWPArialRescuers.Control = this.MEWPArialRescuersMemoEdit;
            this.ItemForMEWPArialRescuers.CustomizationFormText = "MEWP \\ Aerial Rescuers:";
            this.ItemForMEWPArialRescuers.Location = new System.Drawing.Point(519, 0);
            this.ItemForMEWPArialRescuers.Name = "ItemForMEWPArialRescuers";
            this.ItemForMEWPArialRescuers.Size = new System.Drawing.Size(556, 46);
            this.ItemForMEWPArialRescuers.Text = "MEWP \\ Aerial Rescuers:";
            this.ItemForMEWPArialRescuers.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForEmergencyKitLocation
            // 
            this.ItemForEmergencyKitLocation.Control = this.EmergencyKitLocationMemoEdit;
            this.ItemForEmergencyKitLocation.CustomizationFormText = "Emergency Kit Location:";
            this.ItemForEmergencyKitLocation.Location = new System.Drawing.Point(519, 46);
            this.ItemForEmergencyKitLocation.Name = "ItemForEmergencyKitLocation";
            this.ItemForEmergencyKitLocation.Size = new System.Drawing.Size(556, 43);
            this.ItemForEmergencyKitLocation.Text = "Emergency Kit Location:";
            this.ItemForEmergencyKitLocation.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForAirAmbulanceLat
            // 
            this.ItemForAirAmbulanceLat.Control = this.AirAmbulanceLatTextEdit;
            this.ItemForAirAmbulanceLat.CustomizationFormText = "Air Ambulance Latitude:";
            this.ItemForAirAmbulanceLat.Location = new System.Drawing.Point(0, 89);
            this.ItemForAirAmbulanceLat.Name = "ItemForAirAmbulanceLat";
            this.ItemForAirAmbulanceLat.Size = new System.Drawing.Size(519, 24);
            this.ItemForAirAmbulanceLat.Text = "Air Ambulance Latitude:";
            this.ItemForAirAmbulanceLat.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForBanksMan
            // 
            this.ItemForBanksMan.Control = this.BanksManMemoEdit;
            this.ItemForBanksMan.CustomizationFormText = "Banksman:";
            this.ItemForBanksMan.Location = new System.Drawing.Point(519, 89);
            this.ItemForBanksMan.Name = "ItemForBanksMan";
            this.ItemForBanksMan.Size = new System.Drawing.Size(556, 48);
            this.ItemForBanksMan.Text = "Banksman:";
            this.ItemForBanksMan.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForAirAmbulanceLong
            // 
            this.ItemForAirAmbulanceLong.Control = this.AirAmbulanceLongTextEdit;
            this.ItemForAirAmbulanceLong.CustomizationFormText = "Air Ambulance Longitude:";
            this.ItemForAirAmbulanceLong.Location = new System.Drawing.Point(0, 113);
            this.ItemForAirAmbulanceLong.Name = "ItemForAirAmbulanceLong";
            this.ItemForAirAmbulanceLong.Size = new System.Drawing.Size(519, 24);
            this.ItemForAirAmbulanceLong.Text = "Air Ambulance Longitude:";
            this.ItemForAirAmbulanceLong.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForEmergencyServicesMeetPoint
            // 
            this.ItemForEmergencyServicesMeetPoint.Control = this.EmergencyServicesMeetPointMemoEdit;
            this.ItemForEmergencyServicesMeetPoint.CustomizationFormText = "Emergency Services Meet Point:";
            this.ItemForEmergencyServicesMeetPoint.Location = new System.Drawing.Point(0, 137);
            this.ItemForEmergencyServicesMeetPoint.Name = "ItemForEmergencyServicesMeetPoint";
            this.ItemForEmergencyServicesMeetPoint.Size = new System.Drawing.Size(519, 36);
            this.ItemForEmergencyServicesMeetPoint.Text = "Emergency Services Meet Point:";
            this.ItemForEmergencyServicesMeetPoint.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForMobilePhoneSignalBars
            // 
            this.ItemForMobilePhoneSignalBars.Control = this.MobilePhoneSignalBarsSpinEdit;
            this.ItemForMobilePhoneSignalBars.CustomizationFormText = "Mobile Phone Signal:";
            this.ItemForMobilePhoneSignalBars.Location = new System.Drawing.Point(519, 137);
            this.ItemForMobilePhoneSignalBars.Name = "ItemForMobilePhoneSignalBars";
            this.ItemForMobilePhoneSignalBars.Size = new System.Drawing.Size(556, 24);
            this.ItemForMobilePhoneSignalBars.Text = "Mobile Phone Signal:";
            this.ItemForMobilePhoneSignalBars.TextSize = new System.Drawing.Size(154, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(519, 161);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(556, 12);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForNearestAandE
            // 
            this.ItemForNearestAandE.Control = this.NearestAandEMemoEdit;
            this.ItemForNearestAandE.CustomizationFormText = "Nearest A && E:";
            this.ItemForNearestAandE.Location = new System.Drawing.Point(0, 173);
            this.ItemForNearestAandE.Name = "ItemForNearestAandE";
            this.ItemForNearestAandE.Size = new System.Drawing.Size(519, 37);
            this.ItemForNearestAandE.Text = "Nearest A && E:";
            this.ItemForNearestAandE.TextSize = new System.Drawing.Size(154, 13);
            // 
            // ItemForNearestLandLine
            // 
            this.ItemForNearestLandLine.Control = this.NearestLandLineMemoEdit;
            this.ItemForNearestLandLine.CustomizationFormText = "Nearest Working Land Line:";
            this.ItemForNearestLandLine.Location = new System.Drawing.Point(519, 173);
            this.ItemForNearestLandLine.Name = "ItemForNearestLandLine";
            this.ItemForNearestLandLine.Size = new System.Drawing.Size(556, 37);
            this.ItemForNearestLandLine.Text = "Nearest Working Land Line:";
            this.ItemForNearestLandLine.TextSize = new System.Drawing.Size(154, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup7.CaptionImageOptions.Image")));
            this.layoutControlGroup7.CustomizationFormText = "Remarks";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1075, 210);
            this.layoutControlGroup7.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(1075, 210);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(177, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(255, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 24);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(1123, 2);
            // 
            // ItemForRiskAssessmentType
            // 
            this.ItemForRiskAssessmentType.Control = this.RiskAssessmentTypeTextEdit;
            this.ItemForRiskAssessmentType.CustomizationFormText = "Risk Assessment Type:";
            this.ItemForRiskAssessmentType.Location = new System.Drawing.Point(432, 0);
            this.ItemForRiskAssessmentType.Name = "ItemForRiskAssessmentType";
            this.ItemForRiskAssessmentType.Size = new System.Drawing.Size(691, 24);
            this.ItemForRiskAssessmentType.Text = "Risk Assessment Type:";
            this.ItemForRiskAssessmentType.TextSize = new System.Drawing.Size(154, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 330);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1123, 6);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleSeparator1,
            this.layoutControlGroup9});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 336);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1123, 338);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1123, 2);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Linked Data";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1123, 336);
            this.layoutControlGroup9.Text = "Linked Data";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "Team Signatures";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroupQuestions;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1099, 290);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroupQuestions,
            this.layoutControlGroupTeamSignatures,
            this.layoutControlGroupNearMisses});
            this.tabbedControlGroup2.Text = "Team Signatures";
            // 
            // layoutControlGroupQuestions
            // 
            this.layoutControlGroupQuestions.CustomizationFormText = "Risk Assessment Questions";
            this.layoutControlGroupQuestions.ExpandButtonVisible = true;
            this.layoutControlGroupQuestions.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroupQuestions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroupQuestions.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupQuestions.Name = "layoutControlGroupQuestions";
            this.layoutControlGroupQuestions.Size = new System.Drawing.Size(1075, 245);
            this.layoutControlGroupQuestions.Text = "Risk Assessment Questions";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "Risk Questions:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1075, 245);
            this.layoutControlItem1.Text = "Risk Questions:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroupTeamSignatures
            // 
            this.layoutControlGroupTeamSignatures.CustomizationFormText = "Team Signatures";
            this.layoutControlGroupTeamSignatures.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroupTeamSignatures.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupTeamSignatures.Name = "layoutControlGroupTeamSignatures";
            this.layoutControlGroupTeamSignatures.Size = new System.Drawing.Size(1075, 245);
            this.layoutControlGroupTeamSignatures.Text = "Team Signatures";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl3;
            this.layoutControlItem3.CustomizationFormText = "Team Members Signatures:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1075, 245);
            this.layoutControlItem3.Text = "Team Members Signatures:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroupNearMisses
            // 
            this.layoutControlGroupNearMisses.CustomizationFormText = "Near Misses \\ Dangerous Occurrences";
            this.layoutControlGroupNearMisses.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.emptySpaceItem2});
            this.layoutControlGroupNearMisses.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroupNearMisses.Name = "layoutControlGroupNearMisses";
            this.layoutControlGroupNearMisses.Size = new System.Drawing.Size(1075, 245);
            this.layoutControlGroupNearMisses.Text = "Near Misses \\ Dangerous Occurrences";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl1;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1075, 17);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 17);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1075, 228);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1
            // 
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1.DataMember = "sp07249_UT_Work_Order_Manager_Linked_Actions_Edit";
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07256_UT_Risk_Assessment_Question_ItemTableAdapter
            // 
            this.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter
            // 
            this.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07263_UT_Type_Of_Work_With_BlankTableAdapter
            // 
            this.sp07263_UT_Type_Of_Work_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter
            // 
            this.sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter
            // 
            this.sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter
            // 
            this.sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_UT_Risk_Assessment_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1143, 750);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Risk_Assessment_Edit";
            this.Text = "Edit Risk Assessment";
            this.Activated += new System.EventHandler(this.frm_UT_Risk_Assessment_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Risk_Assessment_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Risk_Assessment_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FeederNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07256UTRiskAssessmentQuestionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestLandLineMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandEMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobilePhoneSignalBarsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyServicesMeetPointMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BanksManMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirAmbulanceLongTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AirAmbulanceLatTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyKitLocationMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MEWPArialRescuersMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07265UTRiskAssessmentTeamSignaturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditSignatureCapture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoltageTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedPoleIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PersonCompletingTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastUpdatedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NRSWAOpeningNoticeNoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermitHolderNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeOfWorkingIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07263UTTypeOfWorkWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectricalRiskCategoryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07264UTElectricalRiskCategoriesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ElectricalAssessmentVerifiedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionCheckedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateTimeRecordedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RiskAssessmentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAnswer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07417UTRiskAssessmentQuestionAnswersListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedPoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVoltage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPersonCompletingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectricalRiskCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionChecked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTimeRecorded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastUpdated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForElectricalAssessmentVerified)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermitHolderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeOfWorkingID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNRSWAOpeningNoticeNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupSiteSpecific)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMEWPArialRescuers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyKitLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAirAmbulanceLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBanksMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAirAmbulanceLong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyServicesMeetPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobilePhoneSignalBars)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestLandLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRiskAssessmentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupQuestions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupTeamSignatures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroupNearMisses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit RiskAssessmentIDTextEdit;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRiskAssessmentID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit SurveyedPoleIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyedPoleID;
        private DevExpress.XtraEditors.TextEdit PersonCompletingTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonCompletingTypeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupNearMisses;
        private System.Windows.Forms.BindingSource sp07256UTRiskAssessmentQuestionItemBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT_EditTableAdapters.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter sp07256_UT_Risk_Assessment_Question_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit DateTimeRecordedTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateTimeRecorded;
        private DevExpress.XtraEditors.TextEdit PersonCompletingIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonCompletingID;
        private DevExpress.XtraEditors.CheckEdit PermissionCheckedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermissionChecked;
        private DevExpress.XtraEditors.CheckEdit ElectricalAssessmentVerifiedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForElectricalAssessmentVerified;
        private DevExpress.XtraEditors.GridLookUpEdit ElectricalRiskCategoryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForElectricalRiskCategoryID;
        private DevExpress.XtraEditors.GridLookUpEdit TypeOfWorkingIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTypeOfWorkingID;
        private DevExpress.XtraEditors.TextEdit PermitHolderNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermitHolderName;
        private DevExpress.XtraEditors.TextEdit NRSWAOpeningNoticeNoTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNRSWAOpeningNoticeNo;
        private DevExpress.XtraEditors.TextEdit RiskAssessmentTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRiskAssessmentTypeID;
        private DevExpress.XtraEditors.TextEdit LastUpdatedTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastUpdated;
        private DevExpress.XtraEditors.TextEdit PersonCompletingNameTextEdit;
        private DevExpress.XtraEditors.TextEdit PersonCompletingTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonCompletingType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPersonCompletingName;
        private DevExpress.XtraEditors.TextEdit RiskAssessmentTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRiskAssessmentType;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit RegionNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.TextEdit RegionNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionNumber;
        private DevExpress.XtraEditors.TextEdit SubAreaNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaName;
        private DevExpress.XtraEditors.TextEdit SubAreaNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaNumber;
        private DevExpress.XtraEditors.TextEdit PrimaryNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimaryName;
        private DevExpress.XtraEditors.TextEdit PrimaryNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimaryNumber;
        private DevExpress.XtraEditors.TextEdit FeederNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit FeederNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederNumber;
        private DevExpress.XtraEditors.TextEdit CircuitNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitName;
        private DevExpress.XtraEditors.TextEdit CircuitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitNumber;
        private DevExpress.XtraEditors.TextEdit PoleNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleNumber;
        private System.Windows.Forms.BindingSource sp07249UTWorkOrderManagerLinkedActionsEditBindingSource1;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DevExpress.XtraEditors.TextEdit VoltageTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVoltage;
        private System.Windows.Forms.BindingSource sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText1;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText2;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText3;
        private DataSet_UT_EditTableAdapters.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditAnswer;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private System.Windows.Forms.BindingSource sp07263UTTypeOfWorkWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07263_UT_Type_Of_Work_With_BlankTableAdapter sp07263_UT_Type_Of_Work_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private System.Windows.Forms.BindingSource sp07264UTElectricalRiskCategoriesWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private System.Windows.Forms.BindingSource sp07265UTRiskAssessmentTeamSignaturesListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorSignatureID;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSignature;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentDateTimeRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberName;
        private DataSet_UT_EditTableAdapters.sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem bbiEditReportLayout;
        private DevExpress.XtraBars.PopupMenu pmPrint;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraEditors.MemoEdit BanksManMemoEdit;
        private DevExpress.XtraEditors.TextEdit AirAmbulanceLongTextEdit;
        private DevExpress.XtraEditors.TextEdit AirAmbulanceLatTextEdit;
        private DevExpress.XtraEditors.MemoEdit EmergencyKitLocationMemoEdit;
        private DevExpress.XtraEditors.MemoEdit MEWPArialRescuersMemoEdit;
        private DevExpress.XtraEditors.MemoEdit SiteAddressMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupSiteSpecific;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMEWPArialRescuers;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmergencyKitLocation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAirAmbulanceLat;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBanksMan;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAirAmbulanceLong;
        private DevExpress.XtraEditors.MemoEdit EmergencyServicesMeetPointMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmergencyServicesMeetPoint;
        private DevExpress.XtraEditors.SpinEdit MobilePhoneSignalBarsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobilePhoneSignalBars;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraEditors.MemoEdit NearestLandLineMemoEdit;
        private DevExpress.XtraEditors.MemoEdit NearestAandEMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestAandE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestLandLine;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupLocation;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupOther;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnSignatureCapture;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditSignatureCapture;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupQuestions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroupTeamSignatures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp07417UTRiskAssessmentQuestionAnswersListBindingSource;
        private DataSet_UTTableAdapters.sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
    }
}

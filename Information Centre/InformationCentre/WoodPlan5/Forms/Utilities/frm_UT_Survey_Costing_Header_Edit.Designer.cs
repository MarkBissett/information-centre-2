namespace WoodPlan5
{
    partial class frm_UT_Survey_Costing_Header_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Survey_Costing_Header_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedToSurveyTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07223UTSuveyCostingHeaderItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.CreatedByIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateCreatedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.LiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SurveyIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SurveyCostingHeaderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForSurveyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyCostingHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCreatedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToSurvey = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07223_UT_Suvey_Costing_Header_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07223_UT_Suvey_Costing_Header_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToSurveyTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07223UTSuveyCostingHeaderItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyCostingHeaderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyCostingHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToSurvey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedToSurveyTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCreatedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.LiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SurveyCostingHeaderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.sp07223UTSuveyCostingHeaderItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSurveyID,
            this.ItemForCreatedByID,
            this.ItemForSurveyCostingHeaderID,
            this.layoutControlGroup3});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(953, 404, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedToSurveyTextEdit
            // 
            this.LinkedToSurveyTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "LinkedToSurvey", true));
            this.LinkedToSurveyTextEdit.Location = new System.Drawing.Point(115, 35);
            this.LinkedToSurveyTextEdit.MenuManager = this.barManager1;
            this.LinkedToSurveyTextEdit.Name = "LinkedToSurveyTextEdit";
            this.LinkedToSurveyTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToSurveyTextEdit, true);
            this.LinkedToSurveyTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToSurveyTextEdit, optionsSpelling1);
            this.LinkedToSurveyTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToSurveyTextEdit.TabIndex = 33;
            // 
            // sp07223UTSuveyCostingHeaderItemBindingSource
            // 
            this.sp07223UTSuveyCostingHeaderItemBindingSource.DataMember = "sp07223_UT_Suvey_Costing_Header_Item";
            this.sp07223UTSuveyCostingHeaderItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CreatedByIDTextEdit
            // 
            this.CreatedByIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "CreatedByID", true));
            this.CreatedByIDTextEdit.Location = new System.Drawing.Point(125, 198);
            this.CreatedByIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByIDTextEdit.Name = "CreatedByIDTextEdit";
            this.CreatedByIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByIDTextEdit, true);
            this.CreatedByIDTextEdit.Size = new System.Drawing.Size(491, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByIDTextEdit, optionsSpelling2);
            this.CreatedByIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByIDTextEdit.TabIndex = 32;
            // 
            // DateCreatedDateEdit
            // 
            this.DateCreatedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "DateCreated", true));
            this.DateCreatedDateEdit.EditValue = "";
            this.DateCreatedDateEdit.Location = new System.Drawing.Point(115, 150);
            this.DateCreatedDateEdit.MenuManager = this.barManager1;
            this.DateCreatedDateEdit.Name = "DateCreatedDateEdit";
            this.DateCreatedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateCreatedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCreatedDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.DateCreatedDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateCreatedDateEdit.Properties.ReadOnly = true;
            this.DateCreatedDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCreatedDateEdit.Size = new System.Drawing.Size(501, 20);
            this.DateCreatedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCreatedDateEdit.TabIndex = 31;
            // 
            // LiveCheckEdit
            // 
            this.LiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "Live", true));
            this.LiveCheckEdit.Location = new System.Drawing.Point(115, 93);
            this.LiveCheckEdit.MenuManager = this.barManager1;
            this.LiveCheckEdit.Name = "LiveCheckEdit";
            this.LiveCheckEdit.Properties.Caption = "(Tick if Live - Only 1 per Survey)";
            this.LiveCheckEdit.Properties.ValueChecked = 1;
            this.LiveCheckEdit.Properties.ValueUnchecked = 0;
            this.LiveCheckEdit.Size = new System.Drawing.Size(501, 19);
            this.LiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.LiveCheckEdit.TabIndex = 30;
            // 
            // SurveyIDTextEdit
            // 
            this.SurveyIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "SurveyID", true));
            this.SurveyIDTextEdit.Location = new System.Drawing.Point(137, 346);
            this.SurveyIDTextEdit.MenuManager = this.barManager1;
            this.SurveyIDTextEdit.Name = "SurveyIDTextEdit";
            this.SurveyIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyIDTextEdit, true);
            this.SurveyIDTextEdit.Size = new System.Drawing.Size(467, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyIDTextEdit, optionsSpelling3);
            this.SurveyIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyIDTextEdit.TabIndex = 29;
            // 
            // TotalCostSpinEdit
            // 
            this.TotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "TotalCost", true));
            this.TotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostSpinEdit.Location = new System.Drawing.Point(115, 174);
            this.TotalCostSpinEdit.MenuManager = this.barManager1;
            this.TotalCostSpinEdit.Name = "TotalCostSpinEdit";
            this.TotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostSpinEdit.Properties.ReadOnly = true;
            this.TotalCostSpinEdit.Size = new System.Drawing.Size(501, 20);
            this.TotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostSpinEdit.TabIndex = 27;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07223UTSuveyCostingHeaderItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(115, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SurveyCostingHeaderIDTextEdit
            // 
            this.SurveyCostingHeaderIDTextEdit.Location = new System.Drawing.Point(115, 198);
            this.SurveyCostingHeaderIDTextEdit.MenuManager = this.barManager1;
            this.SurveyCostingHeaderIDTextEdit.Name = "SurveyCostingHeaderIDTextEdit";
            this.SurveyCostingHeaderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyCostingHeaderIDTextEdit, true);
            this.SurveyCostingHeaderIDTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyCostingHeaderIDTextEdit, optionsSpelling4);
            this.SurveyCostingHeaderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyCostingHeaderIDTextEdit.TabIndex = 4;
            // 
            // CreatedByNameTextEdit
            // 
            this.CreatedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "CreatedByName", true));
            this.CreatedByNameTextEdit.Location = new System.Drawing.Point(115, 126);
            this.CreatedByNameTextEdit.MenuManager = this.barManager1;
            this.CreatedByNameTextEdit.Name = "CreatedByNameTextEdit";
            this.CreatedByNameTextEdit.Properties.MaxLength = 50;
            this.CreatedByNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByNameTextEdit, true);
            this.CreatedByNameTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByNameTextEdit, optionsSpelling5);
            this.CreatedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByNameTextEdit.TabIndex = 6;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(115, 69);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling6);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 7;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07223UTSuveyCostingHeaderItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 277);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(556, 129);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 18;
            this.RemarksMemoEdit.TabStop = false;
            // 
            // ItemForSurveyID
            // 
            this.ItemForSurveyID.Control = this.SurveyIDTextEdit;
            this.ItemForSurveyID.CustomizationFormText = "Survey ID:";
            this.ItemForSurveyID.Location = new System.Drawing.Point(0, 152);
            this.ItemForSurveyID.Name = "ItemForSurveyID";
            this.ItemForSurveyID.Size = new System.Drawing.Size(584, 24);
            this.ItemForSurveyID.Text = "Survey ID:";
            this.ItemForSurveyID.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForSurveyID.TextToControlDistance = 5;
            // 
            // ItemForCreatedByID
            // 
            this.ItemForCreatedByID.Control = this.CreatedByIDTextEdit;
            this.ItemForCreatedByID.CustomizationFormText = "Created By ID:";
            this.ItemForCreatedByID.Location = new System.Drawing.Point(0, 186);
            this.ItemForCreatedByID.Name = "ItemForCreatedByID";
            this.ItemForCreatedByID.Size = new System.Drawing.Size(608, 24);
            this.ItemForCreatedByID.Text = "Created By ID:";
            this.ItemForCreatedByID.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForCreatedByID.TextToControlDistance = 5;
            // 
            // ItemForSurveyCostingHeaderID
            // 
            this.ItemForSurveyCostingHeaderID.Control = this.SurveyCostingHeaderIDTextEdit;
            this.ItemForSurveyCostingHeaderID.CustomizationFormText = "Costing Header ID:";
            this.ItemForSurveyCostingHeaderID.Location = new System.Drawing.Point(0, 186);
            this.ItemForSurveyCostingHeaderID.Name = "ItemForSurveyCostingHeaderID";
            this.ItemForSurveyCostingHeaderID.Size = new System.Drawing.Size(608, 24);
            this.ItemForSurveyCostingHeaderID.Text = "Costing Header ID:";
            this.ItemForSurveyCostingHeaderID.TextSize = new System.Drawing.Size(50, 20);
            this.ItemForSurveyCostingHeaderID.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 363);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 69);
            this.layoutControlGroup3.Text = "autoGeneratedGroup1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 452);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForDescription,
            this.ItemForLive,
            this.emptySpaceItem7,
            this.ItemForCreatedByName,
            this.ItemForDateCreated,
            this.ItemForTotalCost,
            this.ItemForLinkedToSurvey});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 432);
            this.layoutControlGroup2.Text = "autoGeneratedGroup0";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(103, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(103, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(103, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(280, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(328, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(103, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 186);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 196);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(608, 226);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 181);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 133);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.RemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 133);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextToControlDistance = 0;
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 422);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.AllowHide = false;
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Costing Description:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 57);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForDescription.Text = "Costing Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForLive
            // 
            this.ItemForLive.Control = this.LiveCheckEdit;
            this.ItemForLive.CustomizationFormText = "Live Costing Header:";
            this.ItemForLive.Location = new System.Drawing.Point(0, 81);
            this.ItemForLive.Name = "ItemForLive";
            this.ItemForLive.Size = new System.Drawing.Size(608, 23);
            this.ItemForLive.Text = "Live Costing Header:";
            this.ItemForLive.TextSize = new System.Drawing.Size(100, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 104);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCreatedByName
            // 
            this.ItemForCreatedByName.AllowHide = false;
            this.ItemForCreatedByName.Control = this.CreatedByNameTextEdit;
            this.ItemForCreatedByName.CustomizationFormText = "Created By:";
            this.ItemForCreatedByName.Location = new System.Drawing.Point(0, 114);
            this.ItemForCreatedByName.Name = "ItemForCreatedByName";
            this.ItemForCreatedByName.Size = new System.Drawing.Size(608, 24);
            this.ItemForCreatedByName.Text = "Created By:";
            this.ItemForCreatedByName.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForDateCreated
            // 
            this.ItemForDateCreated.Control = this.DateCreatedDateEdit;
            this.ItemForDateCreated.CustomizationFormText = "Date Created:";
            this.ItemForDateCreated.Location = new System.Drawing.Point(0, 138);
            this.ItemForDateCreated.Name = "ItemForDateCreated";
            this.ItemForDateCreated.Size = new System.Drawing.Size(608, 24);
            this.ItemForDateCreated.Text = "Date Created:";
            this.ItemForDateCreated.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostSpinEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 162);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(608, 24);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForLinkedToSurvey
            // 
            this.ItemForLinkedToSurvey.Control = this.LinkedToSurveyTextEdit;
            this.ItemForLinkedToSurvey.CustomizationFormText = "Linked To Survey:";
            this.ItemForLinkedToSurvey.Location = new System.Drawing.Point(0, 23);
            this.ItemForLinkedToSurvey.Name = "ItemForLinkedToSurvey";
            this.ItemForLinkedToSurvey.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedToSurvey.Text = "Linked To Survey:";
            this.ItemForLinkedToSurvey.TextSize = new System.Drawing.Size(100, 13);
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07223_UT_Suvey_Costing_Header_ItemTableAdapter
            // 
            this.sp07223_UT_Suvey_Costing_Header_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Survey_Costing_Header_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Survey_Costing_Header_Edit";
            this.Text = "Edit Survey Costings Header";
            this.Activated += new System.EventHandler(this.frm_UT_Survey_Costing_Header_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Survey_Costing_Header_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Survey_Costing_Header_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToSurveyTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07223UTSuveyCostingHeaderItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyCostingHeaderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyCostingHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToSurvey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit SurveyCostingHeaderIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit CreatedByNameTextEdit;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyCostingHeaderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SpinEdit TotalCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.CheckEdit LiveCheckEdit;
        private System.Windows.Forms.BindingSource sp07223UTSuveyCostingHeaderItemBindingSource;
        private DevExpress.XtraEditors.TextEdit SurveyIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLive;
        private DataSet_UT_EditTableAdapters.sp07223_UT_Suvey_Costing_Header_ItemTableAdapter sp07223_UT_Suvey_Costing_Header_ItemTableAdapter;
        private DevExpress.XtraEditors.DateEdit DateCreatedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCreated;
        private DevExpress.XtraEditors.TextEdit CreatedByIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByID;
        private DevExpress.XtraEditors.TextEdit LinkedToSurveyTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToSurvey;
    }
}

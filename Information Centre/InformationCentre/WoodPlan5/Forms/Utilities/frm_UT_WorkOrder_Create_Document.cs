using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // Used by File.Delete and FileSystemWatcher //
using System.Data.SqlClient;  // Used by Generate Map process //
using System.Reflection;

using DevExpress.LookAndFeel;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;  // Required by Hyperlink Repository Items //
using DevExpress.XtraPrinting;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_WorkOrder_Create_Document : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSignaturePath = "";
        public string strWorkOrderDocumentPath = "";
        public string strPicturePath = "";
        public string strMapPath = "";
        public string _ReportLayoutFolder = "";
        public string _strExistingPDFFileName = "";
 
        public int _intWorkOrderID = 0;
        public bool _boolViewOnly = false;
        rpt_UT_Report_Layout_WorkOrder rptReport;

        WaitDialogForm loadingForm = null;
         #endregion
       
        public frm_UT_WorkOrder_Create_Document()
        {
            InitializeComponent();
        }

        private void frm_UT_WorkOrder_Create_Document_Load(object sender, EventArgs e)
        {
            this.FormID = 500066;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            this.Text = (_boolViewOnly ? "View" : "Create") + " Work Order Document";

            if (_boolViewOnly)
            {
                bbiEditLayout.Enabled = false;
                ddbtnViewWorkOrderDocument.DropDownControl = null;

                btnSaveWorkOrderDocument.Enabled = false;
                btnSaveWorkOrderDocument.Visible = false;
            }

            printPreviewBarItem44.Enabled = false;  // Switch off Open button //
            textEditWorkOrderNumber.Text = _intWorkOrderID.ToString().PadLeft(7, '0');
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _ReportLayoutFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "UtilitiesSavedReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!_ReportLayoutFolder.EndsWith("\\")) _ReportLayoutFolder += "\\";  // Add Backslash to end //
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 249;
            
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_UT_WorkOrder_Create_Document_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_UT_WorkOrder_Create_Document_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (string.IsNullOrEmpty(printControl1.PrintingSystem.Document.Name)) return;
            /*if (printControl1.PrintingSystem.Document.Name != "Document")
            {
                printControl1.PrintingSystem.ClearContent(); // This should free up any map jpg if open //
                rptReport.Dispose();
                rptReport = null;
                GC.GetTotalMemory(true);
            }*/
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] {"iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void Update_Calling_Screen_With_Changes()
        {
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_WorkOrder_Edit")
                    {
                        var fParentForm = (frm_UT_WorkOrder_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(99, "");
                    }
                }
            }
        }

        private void ddbtnViewWorkOrderDocument_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }

            bool IncludeMaps = checkEditIncludeMaps.Checked;
            bool IncludePictures = checkEditIncludePictures.Checked;
            bool IncludeEquipment = checkEditIncludeEquipment.Checked;
            bool IncludeMaterials = checkEditIncludeMaterials.Checked;

            string strReportFileName = "Work_Order_Layout.repx";
            rpt_UT_Report_Layout_WorkOrder rptReport = new rpt_UT_Report_Layout_WorkOrder(this.GlobalSettings, _intWorkOrderID, IncludeMaps, IncludeEquipment, IncludeMaterials, IncludePictures, strMapPath, strPicturePath);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);

                AdjustEventHandlers(rptReport);  // Tie in custom Sorting //

                printControl1.PrintingSystem = rptReport.PrintingSystem;
                printingSystem1.Begin();
                rptReport.CreateDocument();
                printingSystem1.End();
                //loadingForm.Close();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiEditLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "Work_Order_Layout.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                rpt_UT_Report_Layout_WorkOrder rptReport = new rpt_UT_Report_Layout_WorkOrder(this.GlobalSettings, _intWorkOrderID, true, true, true, true, strMapPath, strPicturePath);
                try
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (!splashScreenManager.IsSplashFormVisible)
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    this.splashScreenManager = splashScreenManager1;
                    this.splashScreenManager.ShowWaitForm();
                }

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, DevExpress.XtraReports.UserDesigner.CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, _ReportLayoutFolder, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                form.ShowDialog();
                panel.CloseReport();
            }
        }


        private void btnSaveWorkOrderDocument_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }

            // Get Save Folder //
            string strPDFFolder = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPDFFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Work Order Documents Folder (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolder.EndsWith("\\")) strPDFFolder += "\\";

            // Load Report into memory //
            string strReportFileName = "Work_Order_Layout.repx";
            
            bool IncludeMaps = checkEditIncludeMaps.Checked;
            bool IncludePictures = checkEditIncludePictures.Checked;
            bool IncludeEquipment = checkEditIncludeEquipment.Checked;
            bool IncludeMaterials = checkEditIncludeMaterials.Checked;

            //rpt_UT_Report_Layout_WorkOrder rptReport = new rpt_UT_Report_Layout_WorkOrder(this.GlobalSettings, _intWorkOrderID, true, true, true, true, strMapPath, strPicturePath);
            rpt_UT_Report_Layout_WorkOrder rptReport = new rpt_UT_Report_Layout_WorkOrder(this.GlobalSettings, _intWorkOrderID, IncludeMaps, IncludeEquipment, IncludeMaterials, IncludePictures, strMapPath, strPicturePath);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Create Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Set security options of report so when it is exported, it can't be edited and is password protected //
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
            rptReport.ExportOptions.Pdf.Compressed = true;
            rptReport.ExportOptions.Pdf.ImageQuality = PdfJpegImageQuality.Low;
            //rptReport.ExportOptions.Pdf.NeverEmbeddedFonts = "";


            // Save physical PDF file //
            string strPDFName = "Work_Order_Doc_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") +"__" + _intWorkOrderID.ToString().PadLeft(7, '0') + ".pdf";;
            string strFolderAndPDFName = strPDFFolder + strPDFName;  // Put path onto start of filename //
            try
            {
                rptReport.ExportToPdf(strFolderAndPDFName);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                //Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Work Order Document - an error occurred while generating the PDF File [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Create Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Delete any prior version from file system //
            try
            {
                if (!string.IsNullOrEmpty(_strExistingPDFFileName))
                {
                    string OldFileName = strPDFFolder + _strExistingPDFFileName;
                    System.IO.File.Delete(OldFileName);
                }
            }
            catch (Exception Ex)
            {
            }

            // Update Permission Record with name of PDF file //
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter UpdateWorkOrderRecord = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            UpdateWorkOrderRecord.ChangeConnectionString(strConnectionString);
            try
            {
                UpdateWorkOrderRecord.sp07288_UT_Work_Order_Document_Update_Path(0, _intWorkOrderID, strPDFName, DateTime.Now);

                // Update Calling screen that signature has been saved so it will need to update it's data //
                Update_Calling_Screen_With_Changes();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Work Order Document - an error occurred while updating the Work Order Record [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Create Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            this.Close();
        }





        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            //if (loadingForm != null) loadingForm.Close();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }
       
        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }            
         }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Data...", "Report Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        /*
                public class SaveCommandHandler : ICommandHandler
                {
                    XRDesignPanel panel;

                    public string strFullPath = "";

                    public string strFileName = "";

                    public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                    {
                        this.panel = panel;
                        this.strFullPath = strFullPath;
                        this.strFileName = strFileName;
                    }

                    public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                    {
                        if (!CanHandleCommand(command)) return;
                        Save();  // Save report //
                        handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                    }

                    public virtual bool CanHandleCommand(ReportCommand command)
                    {
                        // This handler is used for SaveFile, SaveFileAs and Closing commands.
                        return command == ReportCommand.SaveFile ||
                            command == ReportCommand.SaveFileAs ||
                            command == ReportCommand.Closing;
                    }

                    void Save()
                    {
                        // Custom Saving Logic //
                        Boolean blSaved = false;
                        panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                        // Update existing file layout //
                        panel.Report.DataSource = null;
                        panel.Report.DataMember = null;
                        panel.Report.DataAdapter = null;
                        try
                        {
                            panel.Report.SaveLayout(strFullPath + strFileName);
                            blSaved = true;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            Console.WriteLine(ex.Message);
                            blSaved = false;
                        }
                        if (blSaved)
                        {
                            panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                        }
                    }
                }
        */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }







    }
}


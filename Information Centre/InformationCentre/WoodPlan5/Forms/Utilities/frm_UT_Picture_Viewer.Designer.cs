﻿namespace WoodPlan5
{
    partial class frm_UT_Picture_Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement3 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement4 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement5 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement6 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement7 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement8 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement9 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement10 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement11 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement12 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement13 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement14 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement15 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement16 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement17 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement18 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement19 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement20 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement21 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement22 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Picture_Viewer));
            this.colDescription1 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.dummyImage = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colPictureTypeDescription = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn8 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn9 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn5 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn10 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.dummyImage2 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn11 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07476UTPicturesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.tileView1 = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.colSurveyPictureID = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colTopLevelDescription = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.sp07476_UT_PicturesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07476_UT_PicturesTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07478UTPicturesTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tileView2 = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.tileViewColumn1 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn2 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn3 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn4 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn6 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn7 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.tileViewColumn12 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemPictureEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.sp07477UTPicturesGroupByBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiRowCount = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEditRowCount = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barEditItemGroupBy = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemGridLookUpEditGroupBy = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sp07477_UT_Pictures_Group_ByTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07477_UT_Pictures_Group_ByTableAdapter();
            this.sp07478_UT_Pictures_TreesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07478_UT_Pictures_TreesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07476UTPicturesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07478UTPicturesTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07477UTPicturesGroupByBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditRowCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGroupBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(944, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 657);
            this.barDockControlBottom.Size = new System.Drawing.Size(944, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 631);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(944, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 631);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiRowCount,
            this.bbiRefresh,
            this.barEditItemGroupBy});
            this.barManager1.MaxItemId = 34;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEditRowCount,
            this.repositoryItemGridLookUpEditGroupBy});
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description1";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 7;
            // 
            // colDescription2
            // 
            this.colDescription2.FieldName = "Description2";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 8;
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 4;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 9;
            // 
            // dummyImage
            // 
            this.dummyImage.Caption = "dummyImage";
            this.dummyImage.ColumnEdit = this.repositoryItemPictureEdit1;
            this.dummyImage.FieldName = "dummyImage";
            this.dummyImage.Name = "dummyImage";
            this.dummyImage.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.dummyImage.Visible = true;
            this.dummyImage.VisibleIndex = 11;
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            // 
            // colPictureTypeDescription
            // 
            this.colPictureTypeDescription.FieldName = "PictureTypeDescription";
            this.colPictureTypeDescription.Name = "colPictureTypeDescription";
            this.colPictureTypeDescription.Visible = true;
            this.colPictureTypeDescription.VisibleIndex = 10;
            // 
            // tileViewColumn8
            // 
            this.tileViewColumn8.FieldName = "Description1";
            this.tileViewColumn8.Name = "tileViewColumn8";
            this.tileViewColumn8.Visible = true;
            this.tileViewColumn8.VisibleIndex = 7;
            // 
            // tileViewColumn9
            // 
            this.tileViewColumn9.FieldName = "Description2";
            this.tileViewColumn9.Name = "tileViewColumn9";
            this.tileViewColumn9.Visible = true;
            this.tileViewColumn9.VisibleIndex = 8;
            // 
            // tileViewColumn5
            // 
            this.tileViewColumn5.FieldName = "DateTimeTaken";
            this.tileViewColumn5.Name = "tileViewColumn5";
            this.tileViewColumn5.Visible = true;
            this.tileViewColumn5.VisibleIndex = 4;
            // 
            // tileViewColumn10
            // 
            this.tileViewColumn10.FieldName = "AddedByStaffName";
            this.tileViewColumn10.Name = "tileViewColumn10";
            this.tileViewColumn10.Visible = true;
            this.tileViewColumn10.VisibleIndex = 9;
            // 
            // dummyImage2
            // 
            this.dummyImage2.Caption = "dummyImage2";
            this.dummyImage2.ColumnEdit = this.repositoryItemPictureEdit3;
            this.dummyImage2.FieldName = "dummyImage2";
            this.dummyImage2.Name = "dummyImage2";
            this.dummyImage2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.dummyImage2.Visible = true;
            this.dummyImage2.VisibleIndex = 11;
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            // 
            // colDescription3
            // 
            this.colDescription3.FieldName = "Description3";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 13;
            // 
            // tileViewColumn11
            // 
            this.tileViewColumn11.FieldName = "PictureTypeDescription";
            this.tileViewColumn11.Name = "tileViewColumn11";
            this.tileViewColumn11.Visible = true;
            this.tileViewColumn11.VisibleIndex = 10;
            // 
            // colPicturePath
            // 
            this.colPicturePath.ColumnEdit = this.repositoryItemPictureEdit1;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 3;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07476UTPicturesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.tileView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemPictureEdit2});
            this.gridControl1.Size = new System.Drawing.Size(939, 605);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tileView1});
            // 
            // sp07476UTPicturesBindingSource
            // 
            this.sp07476UTPicturesBindingSource.DataMember = "sp07476_UT_Pictures";
            this.sp07476UTPicturesBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tileView1
            // 
            this.tileView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyPictureID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colPicturePath,
            this.colDateTimeTaken,
            this.colRemarks,
            this.colTopLevelDescription,
            this.colDescription1,
            this.colDescription2,
            this.colAddedByStaffName,
            this.colPictureTypeDescription,
            this.colSurveyDate,
            this.dummyImage});
            this.tileView1.ColumnSet.GroupColumn = this.colSurveyDate;
            this.tileView1.GridControl = this.gridControl1;
            this.tileView1.Name = "tileView1";
            this.tileView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.tileView1.OptionsLayout.StoreAllOptions = true;
            this.tileView1.OptionsTiles.ItemPadding = new System.Windows.Forms.Padding(8);
            this.tileView1.OptionsTiles.ItemSize = new System.Drawing.Size(360, 170);
            this.tileView1.OptionsTiles.Padding = new System.Windows.Forms.Padding(5);
            this.tileView1.OptionsTiles.RowCount = 3;
            this.tileView1.OptionsTiles.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyDate, DevExpress.Data.ColumnSortOrder.Ascending)});
            tileViewItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement1.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement1.Column = null;
            tileViewItemElement1.Text = "Circuit:";
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement2.Column = this.colDescription1;
            tileViewItemElement2.Text = "colDescription1";
            tileViewItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement2.TextLocation = new System.Drawing.Point(0, 15);
            tileViewItemElement3.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement3.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement3.Column = null;
            tileViewItemElement3.Text = "Pole:";
            tileViewItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement3.TextLocation = new System.Drawing.Point(0, 40);
            tileViewItemElement4.Column = this.colDescription2;
            tileViewItemElement4.Text = "colDescription2";
            tileViewItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement4.TextLocation = new System.Drawing.Point(0, 55);
            tileViewItemElement5.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement5.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement5.Column = null;
            tileViewItemElement5.Text = "Taken On:";
            tileViewItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement5.TextLocation = new System.Drawing.Point(0, 80);
            tileViewItemElement6.Column = this.colDateTimeTaken;
            tileViewItemElement6.Text = "colDateTimeTaken";
            tileViewItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement6.TextLocation = new System.Drawing.Point(0, 95);
            tileViewItemElement7.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement7.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement7.Column = null;
            tileViewItemElement7.Text = "Taken By:";
            tileViewItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement7.TextLocation = new System.Drawing.Point(0, 120);
            tileViewItemElement8.Column = this.colAddedByStaffName;
            tileViewItemElement8.Text = "colAddedByStaffName";
            tileViewItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement8.TextLocation = new System.Drawing.Point(0, 135);
            tileViewItemElement9.Column = this.dummyImage;
            tileViewItemElement9.ImageLocation = new System.Drawing.Point(70, 20);
            tileViewItemElement9.ImageSize = new System.Drawing.Size(250, 150);
            tileViewItemElement9.Text = "dummyImage";
            tileViewItemElement10.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement10.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement10.Column = this.colPictureTypeDescription;
            tileViewItemElement10.StretchHorizontal = true;
            tileViewItemElement10.Text = "colPictureTypeDescription";
            tileViewItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement10.TextLocation = new System.Drawing.Point(118, -2);
            this.tileView1.TileTemplate.Add(tileViewItemElement1);
            this.tileView1.TileTemplate.Add(tileViewItemElement2);
            this.tileView1.TileTemplate.Add(tileViewItemElement3);
            this.tileView1.TileTemplate.Add(tileViewItemElement4);
            this.tileView1.TileTemplate.Add(tileViewItemElement5);
            this.tileView1.TileTemplate.Add(tileViewItemElement6);
            this.tileView1.TileTemplate.Add(tileViewItemElement7);
            this.tileView1.TileTemplate.Add(tileViewItemElement8);
            this.tileView1.TileTemplate.Add(tileViewItemElement9);
            this.tileView1.TileTemplate.Add(tileViewItemElement10);
            this.tileView1.ItemDoubleClick += new DevExpress.XtraGrid.Views.Tile.TileViewItemClickEventHandler(this.tileView1_ItemDoubleClick);
            this.tileView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.tileView1_CustomDrawEmptyForeground);
            this.tileView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.tileView1_CustomUnboundColumnData);
            // 
            // colSurveyPictureID
            // 
            this.colSurveyPictureID.FieldName = "SurveyPictureID";
            this.colSurveyPictureID.Name = "colSurveyPictureID";
            this.colSurveyPictureID.Visible = true;
            this.colSurveyPictureID.VisibleIndex = 0;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.Visible = true;
            this.colLinkedToRecordID.VisibleIndex = 1;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Visible = true;
            this.colLinkedToRecordTypeID.VisibleIndex = 2;
            // 
            // colRemarks
            // 
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 5;
            // 
            // colTopLevelDescription
            // 
            this.colTopLevelDescription.FieldName = "TopLevelDescription";
            this.colTopLevelDescription.Name = "colTopLevelDescription";
            this.colTopLevelDescription.Visible = true;
            this.colTopLevelDescription.VisibleIndex = 6;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.Visible = true;
            this.colSurveyDate.VisibleIndex = 12;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            // 
            // sp07476_UT_PicturesTableAdapter
            // 
            this.sp07476_UT_PicturesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(944, 631);
            this.xtraTabControl1.TabIndex = 5;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(939, 605);
            this.xtraTabPage1.Text = "Poles";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(939, 605);
            this.xtraTabPage2.Text = "Trees";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07478UTPicturesTreesBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.tileView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemPictureEdit4});
            this.gridControl2.Size = new System.Drawing.Size(939, 605);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tileView2});
            // 
            // sp07478UTPicturesTreesBindingSource
            // 
            this.sp07478UTPicturesTreesBindingSource.DataMember = "sp07478_UT_Pictures_Trees";
            this.sp07478UTPicturesTreesBindingSource.DataSource = this.dataSet_UT;
            // 
            // tileView2
            // 
            this.tileView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.tileViewColumn1,
            this.tileViewColumn2,
            this.tileViewColumn3,
            this.tileViewColumn4,
            this.tileViewColumn5,
            this.tileViewColumn6,
            this.tileViewColumn7,
            this.tileViewColumn8,
            this.tileViewColumn9,
            this.tileViewColumn10,
            this.tileViewColumn11,
            this.tileViewColumn12,
            this.dummyImage2,
            this.colDescription3});
            this.tileView2.ColumnSet.GroupColumn = this.tileViewColumn12;
            this.tileView2.GridControl = this.gridControl2;
            this.tileView2.Name = "tileView2";
            this.tileView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.tileView2.OptionsLayout.StoreAllOptions = true;
            this.tileView2.OptionsTiles.ItemPadding = new System.Windows.Forms.Padding(8);
            this.tileView2.OptionsTiles.ItemSize = new System.Drawing.Size(360, 170);
            this.tileView2.OptionsTiles.Padding = new System.Windows.Forms.Padding(5);
            this.tileView2.OptionsTiles.RowCount = 3;
            this.tileView2.OptionsTiles.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar;
            this.tileView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.tileViewColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            tileViewItemElement11.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement11.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement11.Column = null;
            tileViewItemElement11.Text = "Circuit:";
            tileViewItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement11.TextLocation = new System.Drawing.Point(0, -5);
            tileViewItemElement12.Column = this.tileViewColumn8;
            tileViewItemElement12.Text = "tileViewColumn8";
            tileViewItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement12.TextLocation = new System.Drawing.Point(0, 8);
            tileViewItemElement13.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement13.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement13.Column = null;
            tileViewItemElement13.Text = "Pole:";
            tileViewItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement13.TextLocation = new System.Drawing.Point(0, 29);
            tileViewItemElement14.Column = this.tileViewColumn9;
            tileViewItemElement14.Text = "tileViewColumn9";
            tileViewItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement14.TextLocation = new System.Drawing.Point(0, 42);
            tileViewItemElement15.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement15.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement15.Column = null;
            tileViewItemElement15.Text = "Taken On:";
            tileViewItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement15.TextLocation = new System.Drawing.Point(0, 98);
            tileViewItemElement16.Column = this.tileViewColumn5;
            tileViewItemElement16.Text = "tileViewColumn5";
            tileViewItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement16.TextLocation = new System.Drawing.Point(0, 111);
            tileViewItemElement17.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement17.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement17.Column = null;
            tileViewItemElement17.Text = "Taken By:";
            tileViewItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement17.TextLocation = new System.Drawing.Point(0, 131);
            tileViewItemElement18.Column = this.tileViewColumn10;
            tileViewItemElement18.Text = "tileViewColumn10";
            tileViewItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement18.TextLocation = new System.Drawing.Point(0, 144);
            tileViewItemElement19.Column = this.dummyImage2;
            tileViewItemElement19.ImageLocation = new System.Drawing.Point(70, 20);
            tileViewItemElement19.ImageSize = new System.Drawing.Size(250, 150);
            tileViewItemElement19.Text = "dummyImage2";
            tileViewItemElement20.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement20.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement20.Column = null;
            tileViewItemElement20.Text = "Tree:";
            tileViewItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement20.TextLocation = new System.Drawing.Point(0, 62);
            tileViewItemElement21.Column = this.colDescription3;
            tileViewItemElement21.Text = "colDescription3";
            tileViewItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement21.TextLocation = new System.Drawing.Point(0, 75);
            tileViewItemElement22.Appearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            tileViewItemElement22.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement22.Column = this.tileViewColumn11;
            tileViewItemElement22.Text = "tileViewColumn11";
            tileViewItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileViewItemElement22.TextLocation = new System.Drawing.Point(118, -2);
            this.tileView2.TileTemplate.Add(tileViewItemElement11);
            this.tileView2.TileTemplate.Add(tileViewItemElement12);
            this.tileView2.TileTemplate.Add(tileViewItemElement13);
            this.tileView2.TileTemplate.Add(tileViewItemElement14);
            this.tileView2.TileTemplate.Add(tileViewItemElement15);
            this.tileView2.TileTemplate.Add(tileViewItemElement16);
            this.tileView2.TileTemplate.Add(tileViewItemElement17);
            this.tileView2.TileTemplate.Add(tileViewItemElement18);
            this.tileView2.TileTemplate.Add(tileViewItemElement19);
            this.tileView2.TileTemplate.Add(tileViewItemElement20);
            this.tileView2.TileTemplate.Add(tileViewItemElement21);
            this.tileView2.TileTemplate.Add(tileViewItemElement22);
            this.tileView2.ItemDoubleClick += new DevExpress.XtraGrid.Views.Tile.TileViewItemClickEventHandler(this.tileView2_ItemDoubleClick);
            this.tileView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.tileView2_CustomDrawEmptyForeground);
            this.tileView2.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.tileView2_CustomUnboundColumnData);
            // 
            // tileViewColumn1
            // 
            this.tileViewColumn1.FieldName = "SurveyPictureID";
            this.tileViewColumn1.Name = "tileViewColumn1";
            this.tileViewColumn1.Visible = true;
            this.tileViewColumn1.VisibleIndex = 0;
            // 
            // tileViewColumn2
            // 
            this.tileViewColumn2.FieldName = "LinkedToRecordID";
            this.tileViewColumn2.Name = "tileViewColumn2";
            this.tileViewColumn2.Visible = true;
            this.tileViewColumn2.VisibleIndex = 1;
            // 
            // tileViewColumn3
            // 
            this.tileViewColumn3.FieldName = "LinkedToRecordTypeID";
            this.tileViewColumn3.Name = "tileViewColumn3";
            this.tileViewColumn3.Visible = true;
            this.tileViewColumn3.VisibleIndex = 2;
            // 
            // tileViewColumn4
            // 
            this.tileViewColumn4.ColumnEdit = this.repositoryItemPictureEdit3;
            this.tileViewColumn4.FieldName = "PicturePath";
            this.tileViewColumn4.Name = "tileViewColumn4";
            this.tileViewColumn4.Visible = true;
            this.tileViewColumn4.VisibleIndex = 3;
            // 
            // tileViewColumn6
            // 
            this.tileViewColumn6.FieldName = "Remarks";
            this.tileViewColumn6.Name = "tileViewColumn6";
            this.tileViewColumn6.Visible = true;
            this.tileViewColumn6.VisibleIndex = 5;
            // 
            // tileViewColumn7
            // 
            this.tileViewColumn7.FieldName = "TopLevelDescription";
            this.tileViewColumn7.Name = "tileViewColumn7";
            this.tileViewColumn7.Visible = true;
            this.tileViewColumn7.VisibleIndex = 6;
            // 
            // tileViewColumn12
            // 
            this.tileViewColumn12.FieldName = "SurveyDate";
            this.tileViewColumn12.Name = "tileViewColumn12";
            this.tileViewColumn12.Visible = true;
            this.tileViewColumn12.VisibleIndex = 12;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemPictureEdit4
            // 
            this.repositoryItemPictureEdit4.Name = "repositoryItemPictureEdit4";
            // 
            // sp07477UTPicturesGroupByBindingSource
            // 
            this.sp07477UTPicturesGroupByBindingSource.DataMember = "sp07477_UT_Pictures_Group_By";
            this.sp07477UTPicturesGroupByBindingSource.DataSource = this.dataSet_UT;
            // 
            // bar1
            // 
            this.bar1.BarName = "Grid Toolbar";
            this.bar1.DockCol = 1;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiRowCount),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemGroupBy),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Custom 3";
            // 
            // beiRowCount
            // 
            this.beiRowCount.Caption = "Number of Rows:";
            this.beiRowCount.Edit = this.repositoryItemSpinEditRowCount;
            this.beiRowCount.EditValue = 3;
            this.beiRowCount.Id = 30;
            this.beiRowCount.Name = "beiRowCount";
            this.beiRowCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.beiRowCount.Width = 36;
            // 
            // repositoryItemSpinEditRowCount
            // 
            this.repositoryItemSpinEditRowCount.AutoHeight = false;
            this.repositoryItemSpinEditRowCount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEditRowCount.IsFloatValue = false;
            this.repositoryItemSpinEditRowCount.Mask.EditMask = "N00";
            this.repositoryItemSpinEditRowCount.MaxValue = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.repositoryItemSpinEditRowCount.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.repositoryItemSpinEditRowCount.Name = "repositoryItemSpinEditRowCount";
            // 
            // barEditItemGroupBy
            // 
            this.barEditItemGroupBy.Caption = "Group By:";
            this.barEditItemGroupBy.Edit = this.repositoryItemGridLookUpEditGroupBy;
            this.barEditItemGroupBy.EditValue = "Survey Date";
            this.barEditItemGroupBy.Id = 33;
            this.barEditItemGroupBy.Name = "barEditItemGroupBy";
            this.barEditItemGroupBy.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barEditItemGroupBy.Width = 121;
            // 
            // repositoryItemGridLookUpEditGroupBy
            // 
            this.repositoryItemGridLookUpEditGroupBy.AutoHeight = false;
            this.repositoryItemGridLookUpEditGroupBy.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditGroupBy.DataSource = this.sp07477UTPicturesGroupByBindingSource;
            this.repositoryItemGridLookUpEditGroupBy.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditGroupBy.Name = "repositoryItemGridLookUpEditGroupBy";
            this.repositoryItemGridLookUpEditGroupBy.NullText = "";
            this.repositoryItemGridLookUpEditGroupBy.ValueMember = "Description";
            this.repositoryItemGridLookUpEditGroupBy.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Group By";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 148;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 31;
            this.bbiRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.LargeGlyph")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // sp07477_UT_Pictures_Group_ByTableAdapter
            // 
            this.sp07477_UT_Pictures_Group_ByTableAdapter.ClearBeforeFill = true;
            // 
            // sp07478_UT_Pictures_TreesTableAdapter
            // 
            this.sp07478_UT_Pictures_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Picture_Viewer
            // 
            this.ClientSize = new System.Drawing.Size(944, 657);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Picture_Viewer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilities Picture Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Picture_Viewer_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Picture_Viewer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07476UTPicturesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07478UTPicturesTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07477UTPicturesGroupByBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditRowCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditGroupBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Tile.TileView tileView1;
        private System.Windows.Forms.BindingSource sp07476UTPicturesBindingSource;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DataSet_UTTableAdapters.sp07476_UT_PicturesTableAdapter sp07476_UT_PicturesTableAdapter;
        private DevExpress.XtraGrid.Columns.TileViewColumn colSurveyPictureID;
        private DevExpress.XtraGrid.Columns.TileViewColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.TileViewColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.TileViewColumn colPicturePath;
        private DevExpress.XtraGrid.Columns.TileViewColumn colDateTimeTaken;
        private DevExpress.XtraGrid.Columns.TileViewColumn colRemarks;
        private DevExpress.XtraGrid.Columns.TileViewColumn colTopLevelDescription;
        private DevExpress.XtraGrid.Columns.TileViewColumn colDescription1;
        private DevExpress.XtraGrid.Columns.TileViewColumn colDescription2;
        private DevExpress.XtraGrid.Columns.TileViewColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.TileViewColumn colPictureTypeDescription;
        private DevExpress.XtraGrid.Columns.TileViewColumn dummyImage;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem beiRowCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditRowCount;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraGrid.Columns.TileViewColumn colSurveyDate;
        private System.Windows.Forms.BindingSource sp07477UTPicturesGroupByBindingSource;
        private DataSet_UTTableAdapters.sp07477_UT_Pictures_Group_ByTableAdapter sp07477_UT_Pictures_Group_ByTableAdapter;
        private DevExpress.XtraBars.BarEditItem barEditItemGroupBy;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditGroupBy;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Tile.TileView tileView2;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn1;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn2;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn3;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn5;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn6;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn7;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn8;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn9;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn10;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn11;
        private DevExpress.XtraGrid.Columns.TileViewColumn tileViewColumn12;
        private DevExpress.XtraGrid.Columns.TileViewColumn dummyImage2;
        private DevExpress.XtraGrid.Columns.TileViewColumn colDescription3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit4;
        private System.Windows.Forms.BindingSource sp07478UTPicturesTreesBindingSource;
        private DataSet_UTTableAdapters.sp07478_UT_Pictures_TreesTableAdapter sp07478_UT_Pictures_TreesTableAdapter;
    }
}

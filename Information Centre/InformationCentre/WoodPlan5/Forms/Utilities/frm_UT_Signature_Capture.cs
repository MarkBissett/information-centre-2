﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Signature_Capture : DevExpress.XtraEditors.XtraForm
    {
        #region Instance Variables

        public string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
 
        public Image _returnedImage = null;
        public string strLoadedSignatureName = "";
        private Point? _Previous = null;
        private Color _color = Color.Black;
        private Pen _Pen = new Pen(System.Drawing.Color.Black);

        #endregion

        public frm_UT_Signature_Capture()
        {
            InitializeComponent();
        }

        private void frm_UT_Signature_Capture_Load(object sender, EventArgs e)
        {
            textEditSignatureName.Text = strLoadedSignatureName;
            _Pen.Width = (float)3;
        }


        #region Picture Box

        private void pictureEdit1_MouseDown(object sender, MouseEventArgs e)
        {
            _Previous = e.Location;
            pictureEdit1_MouseMove(sender, e);
        }

        private void pictureEdit1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Previous != null)
            {
                if (pictureEdit1.Image == null)
                {
                    Bitmap bmp = new Bitmap(pictureEdit1.Width, pictureEdit1.Height);
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.Clear(Color.White);
                    }
                    pictureEdit1.Image = bmp;
                }
                using (Graphics g = Graphics.FromImage(pictureEdit1.Image))
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    g.DrawLine(_Pen, _Previous.Value, e.Location);
                }
                pictureEdit1.Invalidate();
                _Previous = e.Location;
            }
        }

        private void pictureEdit1_MouseUp(object sender, MouseEventArgs e)
        {
            _Previous = null;
        }

        private void pictureEdit1_ImageChanged(object sender, EventArgs e)
        {
            PictureEdit pe = (PictureEdit)sender;
            if (pe.Image != null)
            {
                bbiClearPicture.Enabled = true;
                bbiSavePicture.Enabled = true;
            }
            else
            {
                bbiClearPicture.Enabled = false;
                bbiSavePicture.Enabled = false;
            }
        }

        private void bbiClearPicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = null;
            pictureEdit1.Refresh();

            bbiClearPicture.Enabled = false;
            bbiSavePicture.Enabled = false;
        }

        #endregion


        private void bbiSavePicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (pictureEdit1.Image == null)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Save signature, no signature is loaded.", " Save Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            _returnedImage = pictureEdit1.Image;
            pictureEdit1.Image = null;
            this.DialogResult = DialogResult.OK;
            Close();
            return;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void frm_UT_Signature_Capture_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pictureEdit1.Image != null)
            {
                string strMessage = "There are one or more outstanding changes.\n\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        bbiSavePicture.PerformClick();
                        break;
                }
            }
        }


    }
}
﻿namespace WoodPlan5
{
    partial class frm_UT_Risk_Assessment_Add_Select_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Risk_Assessment_Add_Select_Type));
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnSiteSpecific = new DevExpress.XtraEditors.SimpleButton();
            this.btnG552 = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(8, 10);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(35, 34);
            this.pictureEdit1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.AutoEllipsis = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(51, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(370, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Please select the Type of Risk Assessment to Add by clicking a button below.";
            // 
            // btnSiteSpecific
            // 
            this.btnSiteSpecific.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnSiteSpecific.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSiteSpecific.Image = ((System.Drawing.Image)(resources.GetObject("btnSiteSpecific.Image")));
            this.btnSiteSpecific.Location = new System.Drawing.Point(12, 53);
            this.btnSiteSpecific.Name = "btnSiteSpecific";
            this.btnSiteSpecific.Size = new System.Drawing.Size(159, 23);
            this.btnSiteSpecific.TabIndex = 2;
            this.btnSiteSpecific.Text = "<b>Site Specific</b> Assessment";
            this.btnSiteSpecific.Click += new System.EventHandler(this.btnSiteSpecific_Click);
            // 
            // btnG552
            // 
            this.btnG552.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnG552.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnG552.Image = ((System.Drawing.Image)(resources.GetObject("btnG552.Image")));
            this.btnG552.Location = new System.Drawing.Point(179, 53);
            this.btnG552.Name = "btnG552";
            this.btnG552.Size = new System.Drawing.Size(127, 23);
            this.btnG552.TabIndex = 3;
            this.btnG552.Text = "<b>G55/2</b> Assessment";
            this.btnG552.Click += new System.EventHandler(this.btnG552_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.Location = new System.Drawing.Point(341, 53);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel      ";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frm_UT_Risk_Assessment_Add_Select_Type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 86);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnG552);
            this.Controls.Add(this.btnSiteSpecific);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.pictureEdit1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Risk_Assessment_Add_Select_Type";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Risk Assessment - Select Type";
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSiteSpecific;
        private DevExpress.XtraEditors.SimpleButton btnG552;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
    }
}
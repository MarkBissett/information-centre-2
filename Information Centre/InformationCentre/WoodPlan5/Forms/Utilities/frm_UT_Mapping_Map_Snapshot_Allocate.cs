using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Map_Snapshot_Allocate : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strMapDescription;
        public string strMapRemarks;
        public int intSelectedRecordID = 0;

        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";
 
        #endregion

        public frm_UT_Mapping_Map_Snapshot_Allocate()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Map_Snapshot_Allocate_Load(object sender, EventArgs e)
        {
            this.FormID = 500049;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.Text += "  [Map: " + (string.IsNullOrEmpty(strMapDescription) ? "No Description" : strMapDescription) + "]";

            sp07207_UT_Mapping_Snapshots_Permission_Document_For_Allocate_MapTableAdapter.Connection.ConnectionString = strConnectionString;

            teMapDescription.EditValue = strMapDescription;
            meMapRemarks.EditValue = strMapRemarks;
            Load_Data();
        }


        #region GridControl1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Permission Documents available");
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }


        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        #endregion


        private void btnRefresh_Click(object sender, EventArgs e)
        {
 
        }

        private void Load_Data()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            if (i_dt_FromDate == Convert.ToDateTime("01/01/0001")) i_dt_FromDate = new DateTime(1900, 1, 1);
            if (i_dt_ToDate == Convert.ToDateTime("01/01/0001")) i_dt_ToDate = new DateTime(2900, 1, 1);
                
            sp07207_UT_Mapping_Snapshots_Permission_Document_For_Allocate_MapTableAdapter.Fill(this.dataSet_UT_Mapping.sp07207_UT_Mapping_Snapshots_Permission_Document_For_Allocate_Map, i_dt_FromDate, i_dt_ToDate);
            view.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Permission Document to allocate the map to before proceeding.", "Allocate Permission Document  Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intSelectedRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PermissionDocumentID"));
            strMapDescription = teMapDescription.EditValue.ToString();
            strMapRemarks = meMapRemarks.EditValue.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion



    }
}


namespace WoodPlan5
{
    partial class frm_UT_Mapping
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                try
                {
                    components.Dispose();

                }
                catch (System.Exception)
                {

                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip20 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem20 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem20 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem16 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip17 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem17 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem17 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip18 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem18 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem18 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip19 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem19 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem19 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip24 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem24 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem24 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip25 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem25 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem25 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip26 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem26 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem26 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip28 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem28 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem28 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip27 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem27 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem27 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip21 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem21 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem21 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip22 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem22 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem22 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip23 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem23 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem23 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip29 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem29 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem29 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip30 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem30 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem30 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip31 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem31 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem31 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Mapping));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip32 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem32 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem32 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip33 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem33 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem33 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip34 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem34 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem34 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip35 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem35 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem35 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip36 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem36 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem36 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip37 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem37 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem37 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip38 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem38 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem38 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip39 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem39 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem39 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip40 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem40 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem40 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip41 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem41 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem41 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip42 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem42 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem42 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip43 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem43 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem43 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip45 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem45 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem45 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip46 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem46 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem46 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip44 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem44 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem44 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem13 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem14 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem15 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01312ATTreePickerGazetteerSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYCoordinate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.mapToolBar1 = new MapInfo.Windows.Controls.MapToolBar();
            this.mapToolBarButtonOpenTable = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonLayerControl = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSperator1 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonArrow = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonZoomIn = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonZoomOut = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonCenter = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonPan = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSeperator2 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonSelect = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRectangle = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRadius = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectPolygon = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonSelectRegion = new MapInfo.Windows.Controls.MapToolBarButton();
            this.toolBarButtonSeperator3 = new System.Windows.Forms.ToolBarButton();
            this.mapToolBarButtonAddPoint = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButton1 = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButtonAddPolyLine = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapToolBarButton2 = new MapInfo.Windows.Controls.MapToolBarButton();
            this.mapControl1 = new MapInfo.Windows.Controls.MapControl();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.bbiNone = new DevExpress.XtraBars.BarCheckItem();
            this.bbiZoomIn = new DevExpress.XtraBars.BarCheckItem();
            this.bbiZoomOut = new DevExpress.XtraBars.BarCheckItem();
            this.bbiPan = new DevExpress.XtraBars.BarCheckItem();
            this.bbiCentre = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelect = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRectangle = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRadius = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bbiSelectRegion = new DevExpress.XtraBars.BarCheckItem();
            this.beiPlotObjectType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemGridLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Mapping = new WoodPlan5.DataSet_UT_Mapping();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colModuleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModuleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bbiAddPoint = new DevExpress.XtraBars.BarCheckItem();
            this.bbiAddPolygon = new DevExpress.XtraBars.BarCheckItem();
            this.bbiAddPolyLine = new DevExpress.XtraBars.BarCheckItem();
            this.bbiMapSelection = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu_MapControl1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bsiSurvey = new DevExpress.XtraBars.BarSubItem();
            this.bbiSurveyPole = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSurveyPoleSetClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTransferPolesToSurvey = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemovePolesFromSurvey = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShutdownRequired = new DevExpress.XtraBars.BarButtonItem();
            this.bbiShutdownClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTrafficManagmentRequired = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTrafficMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTrafficManagmentClear = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeferredSet = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearDeferred = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPermission = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPermissionView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddWorkToWorkOrder = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateAccessMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPasteJobs = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetPoleHistorical = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewPoleOwnership = new DevExpress.XtraBars.BarButtonItem();
            this.bbiViewPictures = new DevExpress.XtraBars.BarButtonItem();
            this.bsiEditMapObject = new DevExpress.XtraBars.BarSubItem();
            this.bbiEditSelectedMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBlockEditselectedMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.bsiMapNearbyObjects = new DevExpress.XtraBars.BarSubItem();
            this.beiGazetteerLoadObjectsWithinRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEditGazetteerRange = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.bbiNearbyObjectsFind = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerClearMapSearch = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSetCentrePoint = new DevExpress.XtraBars.BarSubItem();
            this.bbiSetLocalityCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetDistrictCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetSiteCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetClientCentre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCaptureCustomerPermission = new DevExpress.XtraBars.BarButtonItem();
            this.bbiQueryTool = new DevExpress.XtraBars.BarCheckItem();
            this.bbiMeasureLine = new DevExpress.XtraBars.BarCheckItem();
            this.bbiEditMapObjects = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemMapEdit = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.bciEditingNone = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingMove = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingAdd = new DevExpress.XtraBars.BarCheckItem();
            this.bciEditingEdit = new DevExpress.XtraBars.BarCheckItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.beiPopupContainerScale = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControl2 = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnSetScale = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.seUserDefinedScale = new DevExpress.XtraEditors.SpinEdit();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp01254ATTreePickerscalelistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colScaleDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnViewFullExtent = new DevExpress.XtraEditors.SimpleButton();
            this.bsiScreenCoords = new DevExpress.XtraBars.BarStaticItem();
            this.bsiDistanceMeasured = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSnap = new DevExpress.XtraBars.BarStaticItem();
            this.bsiSyncSelection = new DevExpress.XtraBars.BarStaticItem();
            this.bsiLayerCount = new DevExpress.XtraBars.BarStaticItem();
            this.bsiZoom = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel5 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel5_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.pictureBoxLegend = new System.Windows.Forms.PictureBox();
            this.dockPanelSurvey = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.textEditPlotTreesAgainstPole = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btnGPS = new DevExpress.XtraEditors.SimpleButton();
            this.buttonEditSurvey = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.dockPanelGazetteer = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel7_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.buttonEditGazetteerFindValue = new DevExpress.XtraEditors.ButtonEdit();
            this.lookUpEditGazetteerSearchType = new DevExpress.XtraEditors.LookUpEdit();
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.radioGroupGazetteerMatchPattern = new DevExpress.XtraEditors.RadioGroup();
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.dockPanel6 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel6_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.pictureBoxScaleBar = new System.Windows.Forms.PictureBox();
            this.dockPanelLegend = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer3 = new DevExpress.XtraBars.Docking.ControlContainer();
            this.colorEditTreeLegend5 = new DevExpress.XtraEditors.ColorEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.labelControlTreeLegend5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlTreeLegend4 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditTreeLegend4 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlTreeLegend3 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditTreeLegend3 = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditTreeLegend2 = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditTreeLegend1 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlTreeLegend1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlTreeLegend2 = new DevExpress.XtraEditors.LabelControl();
            this.ComboBoxEditTreeStylingType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.ComboBoxEditPoleStylingType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlPoleLegend7 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditPoleLegend7 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlPoleLegend6 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditPoleLegend6 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlPoleLegend4 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditPoleLegend4 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlPoleLegend5 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditPoleLegend5 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlPoleLegend3 = new DevExpress.XtraEditors.LabelControl();
            this.colorEditPoleLegend3 = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditPoleLegend2 = new DevExpress.XtraEditors.ColorEdit();
            this.colorEditPoleLegend1 = new DevExpress.XtraEditors.ColorEdit();
            this.labelControlPoleLegend1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlPoleLegend2 = new DevExpress.XtraEditors.LabelControl();
            this.btnSurveyRefreshPoles = new DevExpress.XtraEditors.SimpleButton();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanelLayers = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel4_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLayerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkspaceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCE_LayerVisible = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLayerHitable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCE_LayerHitable = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPreserveDefaultStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransparency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUseThematicStyling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicStyleSet = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPointSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDummyLayerPathForSave = new DevExpress.XtraGrid.Columns.GridColumn();
            this.beWorkspace = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanelMapObjects = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnScaleToViewedObjects = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefreshTrees = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07053UTTreePickerTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewTrees = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBandID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSizeBand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXYPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLatitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatLongPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colLastInspectionElapsedDays1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObjectType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colThematicValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTooltip1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnClearMapObjects = new DevExpress.XtraEditors.SimpleButton();
            this.btnSelectHighlighted = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefreshMapObjects = new DevExpress.XtraEditors.SimpleButton();
            this.csScaleMapForHighlighted = new DevExpress.XtraEditors.CheckEdit();
            this.ceCentreMap = new DevExpress.XtraEditors.CheckEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07049UTTreePickerPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionElapsedDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHighlight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditHighlight = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colObjectType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colThematicValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTooltip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colorEditHighlight = new DevExpress.XtraEditors.ColorEdit();
            this.buttonEditFilterCircuits = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRefreshTrees = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dockPanelGPS = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel3_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.statusBar1 = new DevExpress.XtraEditors.TextEdit();
            this.NMEAtabs = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.labelPlottingUsingTimer = new DevExpress.XtraEditors.LabelControl();
            this.btnGps_PauseTimer = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl_Gps_Page1 = new DevExpress.XtraLayout.LayoutControl();
            this.ceLogRawGPSData = new DevExpress.XtraEditors.CheckEdit();
            this.cmbPortName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbBaudRate = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ceGps_PlotWithTimer = new DevExpress.XtraEditors.CheckEdit();
            this.seGps_PlotTimerInterval = new DevExpress.XtraEditors.SpinEdit();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnGPS_PlotStart = new DevExpress.XtraEditors.SimpleButton();
            this.btnGPS_PlotStop = new DevExpress.XtraEditors.SimpleButton();
            this.ceGPS_PlotLine = new DevExpress.XtraEditors.CheckEdit();
            this.btnGPS_Plot = new DevExpress.XtraEditors.SimpleButton();
            this.ceGPS_PlotPolygon = new DevExpress.XtraEditors.CheckEdit();
            this.ceGPS_PlotPoint = new DevExpress.XtraEditors.CheckEdit();
            this.btnGPS_Start = new DevExpress.XtraEditors.SimpleButton();
            this.tabGPRMC = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page2 = new DevExpress.XtraLayout.LayoutControl();
            this.lbRMCGridRef = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCPositionUTM = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCMagneticVariation = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCTimeOfFix = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCSpeed = new DevExpress.XtraEditors.TextEdit();
            this.lbRMCCourse = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new DevExpress.XtraEditors.LabelControl();
            this.lbRMCPosition = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGGA = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page3 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGGADGPSID = new DevExpress.XtraEditors.TextEdit();
            this.lbGGADGPSupdate = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAGeoidHeight = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAHDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAAltitude = new DevExpress.XtraEditors.TextEdit();
            this.lbGGANoOfSats = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAFixQuality = new DevExpress.XtraEditors.TextEdit();
            this.lbGGATimeOfFix = new DevExpress.XtraEditors.TextEdit();
            this.lbGGAPosition = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGLL = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page4 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGLLDataValid = new DevExpress.XtraEditors.TextEdit();
            this.lbGLLTimeOfSolution = new DevExpress.XtraEditors.TextEdit();
            this.lbGLLPosition = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGSA = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl_GPS_Page5 = new DevExpress.XtraLayout.LayoutControl();
            this.lbGSAVDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAHDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAPDOP = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAPRNs = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAFixMode = new DevExpress.XtraEditors.TextEdit();
            this.lbGSAMode = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabGPGSV = new DevExpress.XtraTab.XtraTabPage();
            this.picGSVSignals = new System.Windows.Forms.PictureBox();
            this.picGSVSkyview = new System.Windows.Forms.PictureBox();
            this.label33 = new DevExpress.XtraEditors.LabelControl();
            this.tabRaw = new DevExpress.XtraTab.XtraTabPage();
            this.tbRawLog = new DevExpress.XtraEditors.MemoEdit();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.imageList5 = new System.Windows.Forms.ImageList(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList4 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.imageListLineStyles = new System.Windows.Forms.ImageList(this.components);
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.beiEditMode = new DevExpress.XtraBars.BarEditItem();
            this.bsiEditMode = new DevExpress.XtraBars.BarStaticItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bbiWorkOrderMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrint = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveImage = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiViewOnMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHighlightYes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHighlightNo = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCopyHighlightFromVisible = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisibleYes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVisibleNo = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.gridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupMenu_ThematicGrid = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiBlockEditThematicStyles = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSet = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSetAs = new DevExpress.XtraBars.BarButtonItem();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.popupMenu_LayerManager = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRemoveLayer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLayerManagerProperties = new DevExpress.XtraBars.BarButtonItem();
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter();
            this.sp01274ATTreePickerWorkspaceEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter();
            this.imageListLineStylesLegend = new System.Windows.Forms.ImageList(this.components);
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bciSurvey = new DevExpress.XtraBars.BarCheckItem();
            this.bciGazetteer = new DevExpress.XtraBars.BarCheckItem();
            this.bbiLegend = new DevExpress.XtraBars.BarCheckItem();
            this.bciScaleBar = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter();
            this.bbiGPSSaveLog = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGPSClearLog = new DevExpress.XtraBars.BarButtonItem();
            this.imageListGPSLogMenu = new System.Windows.Forms.ImageList(this.components);
            this.popupMenu_Gazetteer = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiGazetteerShowMatch = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGazetteerLoadMapObjectsInRange = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupStoredMapViews = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bciStoreViewChanges = new DevExpress.XtraBars.BarCheckItem();
            this.bsiGoToMapView = new DevExpress.XtraBars.BarSubItem();
            this.bbiStoreCurrentMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearAllMapViews = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bar6 = new DevExpress.XtraBars.Bar();
            this.bbiStoredMapViews = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLastMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNextMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bsiEditObjects = new DevExpress.XtraBars.BarSubItem();
            this.barCheckItem2 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.bar7 = new DevExpress.XtraBars.Bar();
            this.bar8 = new DevExpress.XtraBars.Bar();
            this.bar9 = new DevExpress.XtraBars.Bar();
            this.sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07049_UT_Tree_Picker_PolesTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07049_UT_Tree_Picker_PolesTableAdapter();
            this.sp07053_UT_Tree_Picker_TreesTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07053_UT_Tree_Picker_TreesTableAdapter();
            this.colorEdit1 = new DevExpress.XtraEditors.ColorEdit();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_MapControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGazetteerRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).BeginInit();
            this.popupContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel5.SuspendLayout();
            this.dockPanel5_Container.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).BeginInit();
            this.dockPanelSurvey.SuspendLayout();
            this.controlContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPlotTreesAgainstPole.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSurvey.Properties)).BeginInit();
            this.dockPanelGazetteer.SuspendLayout();
            this.dockPanel7_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.controlContainer1.SuspendLayout();
            this.dockPanel6.SuspendLayout();
            this.dockPanel6_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScaleBar)).BeginInit();
            this.dockPanelLegend.SuspendLayout();
            this.controlContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEditTreeStylingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEditPoleStylingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend1.Properties)).BeginInit();
            this.panelContainer1.SuspendLayout();
            this.dockPanelLayers.SuspendLayout();
            this.dockPanel4_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01288ATTreePickerWorkspacelayerslistBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerHitable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beWorkspace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            this.dockPanelMapObjects.SuspendLayout();
            this.dockPanel2_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07053UTTreePickerTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.csScaleMapForHighlighted.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCentreMap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07049UTTreePickerPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditHighlight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditFilterCircuits.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.dockPanelGPS.SuspendLayout();
            this.dockPanel3_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statusBar1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NMEAtabs)).BeginInit();
            this.NMEAtabs.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Gps_Page1)).BeginInit();
            this.layoutControl_Gps_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceLogRawGPSData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaudRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGps_PlotWithTimer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seGps_PlotTimerInterval.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotLine.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPolygon.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPoint.Properties)).BeginInit();
            this.tabGPRMC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page2)).BeginInit();
            this.layoutControl_GPS_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCGridRef.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPositionUTM.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCMagneticVariation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCTimeOfFix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCCourse.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            this.tabGPGGA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page3)).BeginInit();
            this.layoutControl_GPS_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSupdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAGeoidHeight.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAHDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAAltitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGANoOfSats.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAFixQuality.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGATimeOfFix.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            this.tabGPGLL.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page4)).BeginInit();
            this.layoutControl_GPS_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLDataValid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLTimeOfSolution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            this.tabGPGSA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page5)).BeginInit();
            this.layoutControl_GPS_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAVDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAHDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPDOP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPRNs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAFixMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAMode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            this.tabGPGSV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSignals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSkyview)).BeginInit();
            this.tabRaw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRawLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_Gazetteer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupStoredMapViews)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.Caption = "Add...";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1356, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 720);
            this.barDockControlBottom.Size = new System.Drawing.Size(1356, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 678);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1356, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 678);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.Caption = "Highlight Where Records in Dataset";
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.Caption = "Create Dataset from Highlighted Records";
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.Caption = "Highlight Where Records NOT in Dataset";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3,
            this.bar4,
            this.bar5,
            this.bar6,
            this.bar7,
            this.bar8,
            this.bar9});
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiNone,
            this.bbiZoomIn,
            this.bbiZoomOut,
            this.bbiPan,
            this.bbiCentre,
            this.bbiSelect,
            this.bbiSelectRectangle,
            this.bbiSelectRadius,
            this.bbiSelectPolygon,
            this.bbiSelectRegion,
            this.bbiAddPoint,
            this.bbiAddPolygon,
            this.bbiAddPolyLine,
            this.bsiZoom,
            this.barButtonItem1,
            this.bsiScreenCoords,
            this.bsiLayerCount,
            this.bsiSnap,
            this.barCheckItem1,
            this.beiEditMode,
            this.barSubItem1,
            this.bciEditingNone,
            this.bciEditingMove,
            this.bciEditingAdd,
            this.bciEditingEdit,
            this.bsiEditMode,
            this.bbiSaveImage,
            this.bbiViewOnMap,
            this.beiPopupContainerScale,
            this.bbiMeasureLine,
            this.bsiDistanceMeasured,
            this.bbiHighlightYes,
            this.bbiHighlightNo,
            this.bbiCopyHighlightFromVisible,
            this.bbiVisibleYes,
            this.bbiVisibleNo,
            this.bbiPrint,
            this.bbiBlockEditThematicStyles,
            this.bbiSaveThematicSet,
            this.bbiSaveThematicSetAs,
            this.bbiLayerManagerProperties,
            this.bbiWorkOrderMap,
            this.bbiAddLayer,
            this.bbiRemoveLayer,
            this.bbiQueryTool,
            this.bbiLegend,
            this.bciScaleBar,
            this.bciGazetteer,
            this.bbiEditMapObjects,
            this.bbiDeleteMapObjects,
            this.bbiSetLocalityCentre,
            this.bbiSetDistrictCentre,
            this.bbiGPSSaveLog,
            this.bbiGPSClearLog,
            this.bbiGazetteerShowMatch,
            this.barSubItemMapEdit,
            this.beiGazetteerLoadObjectsWithinRange,
            this.barEditItem1,
            this.bbiGazetteerLoadMapObjectsInRange,
            this.bciStoreViewChanges,
            this.bbiClearAllMapViews,
            this.bbiStoreCurrentMapView,
            this.barButtonItem4,
            this.bsiGoToMapView,
            this.bbiLastMapView,
            this.bbiMapSelection,
            this.bbiNextMapView,
            this.bbiStoredMapViews,
            this.bbiGazetteerClearMapSearch,
            this.bbiEdit,
            this.bsiEditObjects,
            this.bbiEditSelectedMapObjects,
            this.bbiBlockEditselectedMapObjects,
            this.bsiEditMapObject,
            this.bsiMapNearbyObjects,
            this.bbiNearbyObjectsFind,
            this.barCheckItem2,
            this.barButtonItem3,
            this.beiPlotObjectType,
            this.bsiSetCentrePoint,
            this.bbiSetClientCentre,
            this.bbiSetSiteCentre,
            this.bbiTransferPolesToSurvey,
            this.bsiSyncSelection,
            this.bciSurvey,
            this.bsiSurvey,
            this.bbiSurveyPole,
            this.bbiRemovePolesFromSurvey,
            this.bbiSurveyPoleSetClear,
            this.bbiCaptureCustomerPermission,
            this.bbiViewPoleOwnership,
            this.bbiShutdownRequired,
            this.bbiShutdownClear,
            this.bbiTrafficManagmentRequired,
            this.bbiTrafficManagmentClear,
            this.bbiDeferredSet,
            this.bbiCreateAccessMap,
            this.bbiCopyJobs,
            this.bbiPasteJobs,
            this.bbiClearDeferred,
            this.bbiTrafficMap,
            this.bbiPermission,
            this.bbiPermissionView,
            this.bbiSetPoleHistorical,
            this.bbiAddWorkToWorkOrder,
            this.bbiViewPictures});
            this.barManager1.MaxItemId = 170;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemSpinEditGazetteerRange,
            this.repositoryItemPopupContainerEdit3,
            this.repositoryItemGridLookUpEdit1,
            this.repositoryItemGridLookUpEdit2});
            this.barManager1.StatusBar = this.bar2;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridView6.GridControl = this.gridControl3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Locality ID";
            this.gridColumn5.FieldName = "RecordID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Locality Name";
            this.gridColumn6.FieldName = "AddressLine1";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 211;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "District Name";
            this.gridColumn7.FieldName = "AddressLine2";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 151;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Unused 1";
            this.gridColumn8.FieldName = "AddressLine3";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 145;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Unused 2";
            this.gridColumn9.FieldName = "AddressLine4";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 145;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Unused 3";
            this.gridColumn10.FieldName = "AddressLine5";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 145;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Unused 4";
            this.gridColumn11.FieldName = "Postcode";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 145;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = " X Coordinate";
            this.gridColumn12.FieldName = "XCoordinate";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Y Coordinate";
            this.gridColumn13.FieldName = "YCoordinate";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp01312ATTreePickerGazetteerSearchBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            gridLevelNode1.LevelTemplate = this.gridView6;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl3.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(325, 519);
            this.gridControl3.TabIndex = 6;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3,
            this.gridView6});
            // 
            // sp01312ATTreePickerGazetteerSearchBindingSource
            // 
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataMember = "sp01312_AT_Tree_Picker_Gazetteer_Search";
            this.sp01312ATTreePickerGazetteerSearchBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRecordID,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colPostcode1,
            this.colXCoordinate1,
            this.colYCoordinate1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAddressLine2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colRecordID
            // 
            this.colRecordID.Caption = "Record ID";
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 0;
            this.colAddressLine1.Width = 145;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 1;
            this.colAddressLine2.Width = 145;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 2;
            this.colAddressLine3.Width = 145;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Visible = true;
            this.colAddressLine4.VisibleIndex = 3;
            this.colAddressLine4.Width = 145;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Visible = true;
            this.colAddressLine5.VisibleIndex = 4;
            this.colAddressLine5.Width = 145;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            this.colPostcode1.Visible = true;
            this.colPostcode1.VisibleIndex = 5;
            this.colPostcode1.Width = 145;
            // 
            // colXCoordinate1
            // 
            this.colXCoordinate1.Caption = " X Coordinate";
            this.colXCoordinate1.FieldName = "XCoordinate";
            this.colXCoordinate1.Name = "colXCoordinate1";
            this.colXCoordinate1.OptionsColumn.AllowEdit = false;
            this.colXCoordinate1.OptionsColumn.AllowFocus = false;
            this.colXCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colYCoordinate1
            // 
            this.colYCoordinate1.Caption = "Y Coordinate";
            this.colYCoordinate1.FieldName = "YCoordinate";
            this.colYCoordinate1.Name = "colYCoordinate1";
            this.colYCoordinate1.OptionsColumn.AllowEdit = false;
            this.colYCoordinate1.OptionsColumn.AllowFocus = false;
            this.colYCoordinate1.OptionsColumn.ReadOnly = true;
            // 
            // colObjectTypeID
            // 
            this.colObjectTypeID.Caption = "Object Type ID";
            this.colObjectTypeID.FieldName = "ObjectTypeID";
            this.colObjectTypeID.Name = "colObjectTypeID";
            this.colObjectTypeID.OptionsColumn.AllowEdit = false;
            this.colObjectTypeID.OptionsColumn.AllowFocus = false;
            this.colObjectTypeID.OptionsColumn.FixedWidth = true;
            this.colObjectTypeID.Width = 100;
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Mode", "Edit Mode")});
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            // 
            // mapToolBar1
            // 
            this.mapToolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.mapToolBarButtonOpenTable,
            this.mapToolBarButtonLayerControl,
            this.toolBarButtonSperator1,
            this.mapToolBarButtonArrow,
            this.mapToolBarButtonZoomIn,
            this.mapToolBarButtonZoomOut,
            this.mapToolBarButtonCenter,
            this.mapToolBarButtonPan,
            this.toolBarButtonSeperator2,
            this.mapToolBarButtonSelect,
            this.mapToolBarButtonSelectRectangle,
            this.mapToolBarButtonSelectRadius,
            this.mapToolBarButtonSelectPolygon,
            this.mapToolBarButtonSelectRegion,
            this.toolBarButtonSeperator3,
            this.mapToolBarButtonAddPoint,
            this.mapToolBarButton1,
            this.mapToolBarButtonAddPolyLine,
            this.mapToolBarButton2});
            this.mapToolBar1.ButtonSize = new System.Drawing.Size(25, 22);
            this.mapToolBar1.DropDownArrows = true;
            this.mapToolBar1.Enabled = false;
            this.mapToolBar1.Location = new System.Drawing.Point(340, 42);
            this.mapToolBar1.MapControl = this.mapControl1;
            this.mapToolBar1.Name = "mapToolBar1";
            this.mapToolBar1.ShowToolTips = true;
            this.mapToolBar1.Size = new System.Drawing.Size(1016, 28);
            this.mapToolBar1.TabIndex = 18;
            this.mapToolBar1.Visible = false;
            // 
            // mapToolBarButtonOpenTable
            // 
            this.mapToolBarButtonOpenTable.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.OpenTable;
            this.mapToolBarButtonOpenTable.Name = "mapToolBarButtonOpenTable";
            this.mapToolBarButtonOpenTable.ToolTipText = "Open Table";
            // 
            // mapToolBarButtonLayerControl
            // 
            this.mapToolBarButtonLayerControl.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.LayerControl;
            this.mapToolBarButtonLayerControl.Name = "mapToolBarButtonLayerControl";
            this.mapToolBarButtonLayerControl.ToolTipText = "Layer Control";
            // 
            // toolBarButtonSperator1
            // 
            this.toolBarButtonSperator1.Name = "toolBarButtonSperator1";
            this.toolBarButtonSperator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonArrow
            // 
            this.mapToolBarButtonArrow.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Arrow;
            this.mapToolBarButtonArrow.Name = "mapToolBarButtonArrow";
            this.mapToolBarButtonArrow.ToolTipText = "Arrow";
            // 
            // mapToolBarButtonZoomIn
            // 
            this.mapToolBarButtonZoomIn.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.ZoomIn;
            this.mapToolBarButtonZoomIn.Name = "mapToolBarButtonZoomIn";
            this.mapToolBarButtonZoomIn.ToolTipText = "Zoom-in";
            // 
            // mapToolBarButtonZoomOut
            // 
            this.mapToolBarButtonZoomOut.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.ZoomOut;
            this.mapToolBarButtonZoomOut.Name = "mapToolBarButtonZoomOut";
            this.mapToolBarButtonZoomOut.ToolTipText = "Zoom-out";
            // 
            // mapToolBarButtonCenter
            // 
            this.mapToolBarButtonCenter.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Center;
            this.mapToolBarButtonCenter.Name = "mapToolBarButtonCenter";
            this.mapToolBarButtonCenter.ToolTipText = "Center";
            // 
            // mapToolBarButtonPan
            // 
            this.mapToolBarButtonPan.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Pan;
            this.mapToolBarButtonPan.Name = "mapToolBarButtonPan";
            this.mapToolBarButtonPan.ToolTipText = "Pan";
            // 
            // toolBarButtonSeperator2
            // 
            this.toolBarButtonSeperator2.Name = "toolBarButtonSeperator2";
            this.toolBarButtonSeperator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonSelect
            // 
            this.mapToolBarButtonSelect.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.Select;
            this.mapToolBarButtonSelect.Name = "mapToolBarButtonSelect";
            this.mapToolBarButtonSelect.ToolTipText = "Select";
            // 
            // mapToolBarButtonSelectRectangle
            // 
            this.mapToolBarButtonSelectRectangle.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRectangle;
            this.mapToolBarButtonSelectRectangle.Name = "mapToolBarButtonSelectRectangle";
            this.mapToolBarButtonSelectRectangle.ToolTipText = "Marquee Select";
            // 
            // mapToolBarButtonSelectRadius
            // 
            this.mapToolBarButtonSelectRadius.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRadius;
            this.mapToolBarButtonSelectRadius.Name = "mapToolBarButtonSelectRadius";
            this.mapToolBarButtonSelectRadius.ToolTipText = "Radius Select";
            // 
            // mapToolBarButtonSelectPolygon
            // 
            this.mapToolBarButtonSelectPolygon.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectPolygon;
            this.mapToolBarButtonSelectPolygon.Name = "mapToolBarButtonSelectPolygon";
            this.mapToolBarButtonSelectPolygon.ToolTipText = "Polygon Select";
            // 
            // mapToolBarButtonSelectRegion
            // 
            this.mapToolBarButtonSelectRegion.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.SelectRegion;
            this.mapToolBarButtonSelectRegion.Name = "mapToolBarButtonSelectRegion";
            this.mapToolBarButtonSelectRegion.ToolTipText = "Region Select";
            // 
            // toolBarButtonSeperator3
            // 
            this.toolBarButtonSeperator3.Name = "toolBarButtonSeperator3";
            this.toolBarButtonSeperator3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // mapToolBarButtonAddPoint
            // 
            this.mapToolBarButtonAddPoint.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPoint;
            this.mapToolBarButtonAddPoint.Name = "mapToolBarButtonAddPoint";
            this.mapToolBarButtonAddPoint.ToolTipText = "Add Point";
            // 
            // mapToolBarButton1
            // 
            this.mapToolBarButton1.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPolygon;
            this.mapToolBarButton1.Name = "mapToolBarButton1";
            this.mapToolBarButton1.ToolTipText = "Add Polygon";
            // 
            // mapToolBarButtonAddPolyLine
            // 
            this.mapToolBarButtonAddPolyLine.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddPolyline;
            this.mapToolBarButtonAddPolyLine.Name = "mapToolBarButtonAddPolyLine";
            this.mapToolBarButtonAddPolyLine.ToolTipText = "Add Polyline";
            // 
            // mapToolBarButton2
            // 
            this.mapToolBarButton2.ButtonType = MapInfo.Windows.Controls.MapToolButtonType.AddText;
            this.mapToolBarButton2.Name = "mapToolBarButton2";
            this.mapToolBarButton2.ToolTipText = "Add Text";
            // 
            // mapControl1
            // 
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.IgnoreLostFocusEvent = false;
            this.mapControl1.Location = new System.Drawing.Point(2, 2);
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(1012, 646);
            this.mapControl1.TabIndex = 0;
            this.mapControl1.Text = "mapControl2";
            this.mapControl1.Visible = false;
            this.mapControl1.Enter += new System.EventHandler(this.mapControl1_Enter);
            this.mapControl1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mapControl1_KeyPress);
            this.mapControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseMove);
            this.mapControl1.Tools.LeftButtonTool = null;
            this.mapControl1.Tools.MiddleButtonTool = null;
            this.mapControl1.Tools.RightButtonTool = null;
            // 
            // memoEdit1
            // 
            this.memoEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEdit1.Location = new System.Drawing.Point(0, 0);
            this.memoEdit1.MenuManager = this.barManager1;
            this.memoEdit1.Name = "memoEdit1";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEdit1, true);
            this.memoEdit1.Size = new System.Drawing.Size(1366, 93);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEdit1, optionsSpelling29);
            this.memoEdit1.TabIndex = 21;
            // 
            // bbiNone
            // 
            this.bbiNone.BindableChecked = true;
            this.bbiNone.Caption = "Pointer";
            this.bbiNone.Checked = true;
            this.bbiNone.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiNone.Glyph")));
            this.bbiNone.GroupIndex = 1;
            this.bbiNone.Id = 25;
            this.bbiNone.Name = "bbiNone";
            superToolTip20.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem20.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image18")));
            toolTipTitleItem20.Appearance.Options.UseImage = true;
            toolTipTitleItem20.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem20.Image")));
            toolTipTitleItem20.Text = "No Tool - Information";
            toolTipItem20.LeftIndent = 6;
            toolTipItem20.Text = "This tool does nothing - it allows you to click on the map without doing anything" +
    ".";
            superToolTip20.Items.Add(toolTipTitleItem20);
            superToolTip20.Items.Add(toolTipItem20);
            this.bbiNone.SuperTip = superToolTip20;
            this.bbiNone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNone_ItemClick);
            // 
            // bbiZoomIn
            // 
            this.bbiZoomIn.Caption = "Zoom In";
            this.bbiZoomIn.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiZoomIn.Glyph")));
            this.bbiZoomIn.GroupIndex = 1;
            this.bbiZoomIn.Id = 26;
            this.bbiZoomIn.Name = "bbiZoomIn";
            superToolTip16.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem16.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image14")));
            toolTipTitleItem16.Appearance.Options.UseImage = true;
            toolTipTitleItem16.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem16.Image")));
            toolTipTitleItem16.Text = "Zoom In Tool - Information";
            toolTipItem16.LeftIndent = 6;
            toolTipItem16.Text = "Click me to select the Zoom in Tool.\r\n\r\n<b><color=green>Tip:</color></b> Hold dow" +
    "n the mouse and drag out the displayed rectangle to specify the size of the area" +
    " to zoom in to.\r\n";
            superToolTip16.Items.Add(toolTipTitleItem16);
            superToolTip16.Items.Add(toolTipItem16);
            this.bbiZoomIn.SuperTip = superToolTip16;
            this.bbiZoomIn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiZoomIn_ItemClick);
            // 
            // bbiZoomOut
            // 
            this.bbiZoomOut.Caption = "Zoom Out";
            this.bbiZoomOut.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiZoomOut.Glyph")));
            this.bbiZoomOut.GroupIndex = 1;
            this.bbiZoomOut.Id = 27;
            this.bbiZoomOut.Name = "bbiZoomOut";
            superToolTip17.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem17.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image15")));
            toolTipTitleItem17.Appearance.Options.UseImage = true;
            toolTipTitleItem17.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem17.Image")));
            toolTipTitleItem17.Text = "Zoom Out tool - Infomation";
            toolTipItem17.LeftIndent = 6;
            toolTipItem17.Text = "Click me to select the Zoom Out Tool.";
            superToolTip17.Items.Add(toolTipTitleItem17);
            superToolTip17.Items.Add(toolTipItem17);
            this.bbiZoomOut.SuperTip = superToolTip17;
            this.bbiZoomOut.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiZoomOut_ItemClick);
            // 
            // bbiPan
            // 
            this.bbiPan.Caption = "Pan";
            this.bbiPan.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_HandTool;
            this.bbiPan.GroupIndex = 1;
            this.bbiPan.Id = 28;
            this.bbiPan.Name = "bbiPan";
            superToolTip18.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem18.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image16")));
            toolTipTitleItem18.Appearance.Options.UseImage = true;
            toolTipTitleItem18.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem18.Image")));
            toolTipTitleItem18.Text = "Pan Map Tool - Information";
            toolTipItem18.LeftIndent = 6;
            toolTipItem18.Text = resources.GetString("toolTipItem18.Text");
            superToolTip18.Items.Add(toolTipTitleItem18);
            superToolTip18.Items.Add(toolTipItem18);
            this.bbiPan.SuperTip = superToolTip18;
            this.bbiPan.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPan_ItemClick);
            // 
            // bbiCentre
            // 
            this.bbiCentre.Caption = "Centre";
            this.bbiCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCentre.Glyph")));
            this.bbiCentre.GroupIndex = 1;
            this.bbiCentre.Id = 29;
            this.bbiCentre.Name = "bbiCentre";
            superToolTip19.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem19.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image17")));
            toolTipTitleItem19.Appearance.Options.UseImage = true;
            toolTipTitleItem19.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem19.Image")));
            toolTipTitleItem19.Text = "Centre Map Tool - Information";
            toolTipItem19.LeftIndent = 6;
            toolTipItem19.Text = "This tool centres the map.\r\n\r\nSelect the tool the click on the map where the map " +
    "is to be centred.";
            superToolTip19.Items.Add(toolTipTitleItem19);
            superToolTip19.Items.Add(toolTipItem19);
            this.bbiCentre.SuperTip = superToolTip19;
            this.bbiCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCentre_ItemClick);
            // 
            // bbiSelect
            // 
            this.bbiSelect.Caption = "Select";
            this.bbiSelect.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_Pointer;
            this.bbiSelect.GroupIndex = 1;
            this.bbiSelect.Id = 30;
            this.bbiSelect.Name = "bbiSelect";
            superToolTip24.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem24.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image22")));
            toolTipTitleItem24.Appearance.Options.UseImage = true;
            toolTipTitleItem24.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem24.Image")));
            toolTipTitleItem24.Text = "Select Tool - Information";
            toolTipItem24.LeftIndent = 6;
            toolTipItem24.Text = "This tool is used to select objects on the map [points, polygons and polylines].\r" +
    "\n\r\n<color=green><b>Tip:</b></color> To select objects, the Map Objects layer mus" +
    "t be <b>Hitable</b>.";
            superToolTip24.Items.Add(toolTipTitleItem24);
            superToolTip24.Items.Add(toolTipItem24);
            this.bbiSelect.SuperTip = superToolTip24;
            this.bbiSelect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelect_ItemClick);
            // 
            // bbiSelectRectangle
            // 
            this.bbiSelectRectangle.Caption = "Select Rectangle";
            this.bbiSelectRectangle.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRectangle.Glyph")));
            this.bbiSelectRectangle.GroupIndex = 1;
            this.bbiSelectRectangle.Id = 31;
            this.bbiSelectRectangle.Name = "bbiSelectRectangle";
            superToolTip25.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem25.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image23")));
            toolTipTitleItem25.Appearance.Options.UseImage = true;
            toolTipTitleItem25.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem25.Image")));
            toolTipTitleItem25.Text = "Select Rectangle Tool - Information";
            toolTipItem25.LeftIndent = 6;
            toolTipItem25.Text = "This tool selects all objects within a user-defined rectangle.\r\n\r\n<color=green><b" +
    ">Tip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b" +
    ">.";
            superToolTip25.Items.Add(toolTipTitleItem25);
            superToolTip25.Items.Add(toolTipItem25);
            this.bbiSelectRectangle.SuperTip = superToolTip25;
            this.bbiSelectRectangle.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRectangle_ItemClick);
            // 
            // bbiSelectRadius
            // 
            this.bbiSelectRadius.Caption = "Select Radius";
            this.bbiSelectRadius.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRadius.Glyph")));
            this.bbiSelectRadius.GroupIndex = 1;
            this.bbiSelectRadius.Id = 32;
            this.bbiSelectRadius.Name = "bbiSelectRadius";
            superToolTip26.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem26.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image24")));
            toolTipTitleItem26.Appearance.Options.UseImage = true;
            toolTipTitleItem26.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem26.Image")));
            toolTipTitleItem26.Text = "Select Elipses Tool - Information";
            toolTipItem26.LeftIndent = 6;
            toolTipItem26.Text = "This tool selects all objects within a user-defined elypsis.\r\n\r\n<color=green><b>T" +
    "ip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>." +
    "";
            superToolTip26.Items.Add(toolTipTitleItem26);
            superToolTip26.Items.Add(toolTipItem26);
            this.bbiSelectRadius.SuperTip = superToolTip26;
            this.bbiSelectRadius.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRadius_ItemClick);
            // 
            // bbiSelectPolygon
            // 
            this.bbiSelectPolygon.Caption = "Select Polygon";
            this.bbiSelectPolygon.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectPolygon.Glyph")));
            this.bbiSelectPolygon.GroupIndex = 1;
            this.bbiSelectPolygon.Id = 33;
            this.bbiSelectPolygon.Name = "bbiSelectPolygon";
            superToolTip28.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem28.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image26")));
            toolTipTitleItem28.Appearance.Options.UseImage = true;
            toolTipTitleItem28.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem28.Image")));
            toolTipTitleItem28.Text = "Select Polygon Tool - Information";
            toolTipItem28.LeftIndent = 6;
            toolTipItem28.Text = "This tool selects all objects within a user-defined polygon.\r\n\r\n<color=green><b>T" +
    "ip:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>." +
    "";
            superToolTip28.Items.Add(toolTipTitleItem28);
            superToolTip28.Items.Add(toolTipItem28);
            this.bbiSelectPolygon.SuperTip = superToolTip28;
            this.bbiSelectPolygon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectPolygon_ItemClick);
            // 
            // bbiSelectRegion
            // 
            this.bbiSelectRegion.Caption = "Select Region";
            this.bbiSelectRegion.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSelectRegion.Glyph")));
            this.bbiSelectRegion.GroupIndex = 1;
            this.bbiSelectRegion.Id = 34;
            this.bbiSelectRegion.Name = "bbiSelectRegion";
            superToolTip27.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem27.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image25")));
            toolTipTitleItem27.Appearance.Options.UseImage = true;
            toolTipTitleItem27.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem27.Image")));
            toolTipTitleItem27.Text = "Select Region Tool - Information";
            toolTipItem27.LeftIndent = 6;
            toolTipItem27.Text = "This tool selects all objects within a user-defined region.\r\n\r\n<color=green><b>Ti" +
    "p:</b></color> To select objects, the Map Objects layer must be <b>Hitable</b>.";
            superToolTip27.Items.Add(toolTipTitleItem27);
            superToolTip27.Items.Add(toolTipItem27);
            this.bbiSelectRegion.SuperTip = superToolTip27;
            this.bbiSelectRegion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSelectRegion_ItemClick);
            // 
            // beiPlotObjectType
            // 
            this.beiPlotObjectType.Caption = "Plot:";
            this.beiPlotObjectType.Edit = this.repositoryItemGridLookUpEdit2;
            this.beiPlotObjectType.Id = 140;
            this.beiPlotObjectType.Name = "beiPlotObjectType";
            this.beiPlotObjectType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.beiPlotObjectType.Width = 61;
            this.beiPlotObjectType.EditValueChanged += new System.EventHandler(this.beiPlotObjectType_EditValueChanged);
            // 
            // repositoryItemGridLookUpEdit2
            // 
            this.repositoryItemGridLookUpEdit2.AutoHeight = false;
            this.repositoryItemGridLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit2.DataSource = this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource;
            this.repositoryItemGridLookUpEdit2.DisplayMember = "ObjectTypeDescription";
            this.repositoryItemGridLookUpEdit2.Name = "repositoryItemGridLookUpEdit2";
            this.repositoryItemGridLookUpEdit2.NullText = "";
            this.repositoryItemGridLookUpEdit2.NullValuePrompt = "Select Object Type";
            this.repositoryItemGridLookUpEdit2.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemGridLookUpEdit2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.repositoryItemGridLookUpEdit2.ValueMember = "ObjectTypeID";
            this.repositoryItemGridLookUpEdit2.View = this.repositoryItemGridLookUpEdit2View;
            // 
            // sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource
            // 
            this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource.DataMember = "sp07048_UT_Tree_Picker_Plotting_Object_Types_With_Blank";
            this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // dataSet_UT_Mapping
            // 
            this.dataSet_UT_Mapping.DataSetName = "DataSet_UT_Mapping";
            this.dataSet_UT_Mapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colModuleDescription,
            this.colModuleID,
            this.colObjectTypeDescription,
            this.colObjectTypeID,
            this.colObjectTypeOrder,
            this.colRemarks});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colObjectTypeID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsBehavior.AutoExpandAllGroups = true;
            this.repositoryItemGridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colObjectTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colModuleDescription
            // 
            this.colModuleDescription.Caption = "Module";
            this.colModuleDescription.FieldName = "ModuleDescription";
            this.colModuleDescription.Name = "colModuleDescription";
            this.colModuleDescription.OptionsColumn.AllowEdit = false;
            this.colModuleDescription.OptionsColumn.AllowFocus = false;
            this.colModuleDescription.OptionsColumn.FixedWidth = true;
            this.colModuleDescription.Width = 115;
            // 
            // colModuleID
            // 
            this.colModuleID.Caption = "Module ID";
            this.colModuleID.FieldName = "ModuleID";
            this.colModuleID.Name = "colModuleID";
            this.colModuleID.OptionsColumn.AllowEdit = false;
            this.colModuleID.OptionsColumn.AllowFocus = false;
            this.colModuleID.OptionsColumn.FixedWidth = true;
            // 
            // colObjectTypeDescription
            // 
            this.colObjectTypeDescription.Caption = "Object Type";
            this.colObjectTypeDescription.FieldName = "ObjectTypeDescription";
            this.colObjectTypeDescription.Name = "colObjectTypeDescription";
            this.colObjectTypeDescription.OptionsColumn.AllowEdit = false;
            this.colObjectTypeDescription.OptionsColumn.AllowFocus = false;
            this.colObjectTypeDescription.OptionsColumn.FixedWidth = true;
            this.colObjectTypeDescription.Visible = true;
            this.colObjectTypeDescription.VisibleIndex = 0;
            this.colObjectTypeDescription.Width = 232;
            // 
            // colObjectTypeOrder
            // 
            this.colObjectTypeOrder.Caption = "Object Order";
            this.colObjectTypeOrder.FieldName = "ObjectTypeOrder";
            this.colObjectTypeOrder.Name = "colObjectTypeOrder";
            this.colObjectTypeOrder.OptionsColumn.AllowEdit = false;
            this.colObjectTypeOrder.OptionsColumn.AllowFocus = false;
            this.colObjectTypeOrder.OptionsColumn.FixedWidth = true;
            this.colObjectTypeOrder.Width = 165;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.FixedWidth = true;
            this.colRemarks.Width = 92;
            // 
            // bbiAddPoint
            // 
            this.bbiAddPoint.Caption = "Add Point";
            this.bbiAddPoint.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPoint.Glyph")));
            this.bbiAddPoint.GroupIndex = 1;
            this.bbiAddPoint.Id = 35;
            this.bbiAddPoint.Name = "bbiAddPoint";
            superToolTip21.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem21.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image19")));
            toolTipTitleItem21.Appearance.Options.UseImage = true;
            toolTipTitleItem21.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem21.Image")));
            toolTipTitleItem21.Text = "Add Point Tool - Information";
            toolTipItem21.LeftIndent = 6;
            toolTipItem21.Text = "This tool allows you to plot a new point.\r\n\r\n<b><color=green>Tip:</color></b> To " +
    "add points, the Map Objects layer must be <b>Hitable</b> and <b>Editable</b>.";
            superToolTip21.Items.Add(toolTipTitleItem21);
            superToolTip21.Items.Add(toolTipItem21);
            this.bbiAddPoint.SuperTip = superToolTip21;
            this.bbiAddPoint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPoint_ItemClick);
            // 
            // bbiAddPolygon
            // 
            this.bbiAddPolygon.Caption = "Add Polygon";
            this.bbiAddPolygon.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPolygon.Glyph")));
            this.bbiAddPolygon.GroupIndex = 1;
            this.bbiAddPolygon.Id = 36;
            this.bbiAddPolygon.Name = "bbiAddPolygon";
            superToolTip22.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem22.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image20")));
            toolTipTitleItem22.Appearance.Options.UseImage = true;
            toolTipTitleItem22.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem22.Image")));
            toolTipTitleItem22.Text = "Add Polygon Tool - Information";
            toolTipItem22.LeftIndent = 6;
            toolTipItem22.Text = "This tool allows you to plot a new point.\r\n\r\n<b><color=green>Tip:</color></b> To " +
    "add points, the Map Objects layer must be <b>Hitable</b> and <b>Editable</b>.\r\n";
            superToolTip22.Items.Add(toolTipTitleItem22);
            superToolTip22.Items.Add(toolTipItem22);
            this.bbiAddPolygon.SuperTip = superToolTip22;
            this.bbiAddPolygon.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolygon_ItemClick);
            // 
            // bbiAddPolyLine
            // 
            this.bbiAddPolyLine.Caption = "Add PolyLine";
            this.bbiAddPolyLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddPolyLine.Glyph")));
            this.bbiAddPolyLine.GroupIndex = 1;
            this.bbiAddPolyLine.Id = 37;
            this.bbiAddPolyLine.Name = "bbiAddPolyLine";
            superToolTip23.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem23.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image21")));
            toolTipTitleItem23.Appearance.Options.UseImage = true;
            toolTipTitleItem23.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem23.Image")));
            toolTipTitleItem23.Text = "Add Polyline Tool - Information";
            toolTipItem23.LeftIndent = 6;
            toolTipItem23.Text = "This tool allows you to plot a new polyline.\r\n\r\n<b><color=green>Tip:</color></b> " +
    "To add polylines, the Map Objects layer must be <b>Hitable</b> and <b>Editable</" +
    "b>.\r\n";
            superToolTip23.Items.Add(toolTipTitleItem23);
            superToolTip23.Items.Add(toolTipItem23);
            this.bbiAddPolyLine.SuperTip = superToolTip23;
            this.bbiAddPolyLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPolyLine_ItemClick);
            // 
            // bbiMapSelection
            // 
            this.bbiMapSelection.ActAsDropDown = true;
            this.bbiMapSelection.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiMapSelection.Caption = "Map Selection";
            this.bbiMapSelection.DropDownControl = this.popupMenu_MapControl1;
            this.bbiMapSelection.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMapSelection.Glyph")));
            this.bbiMapSelection.Id = 112;
            this.bbiMapSelection.Name = "bbiMapSelection";
            superToolTip29.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem29.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image27")));
            toolTipTitleItem29.Appearance.Options.UseImage = true;
            toolTipTitleItem29.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem29.Image")));
            toolTipTitleItem29.Text = "Show Map Menu - Information";
            toolTipItem29.LeftIndent = 6;
            toolTipItem29.Text = "Click me to display the Map Menu.\r\n\r\nThe Map Menu is used for editing and deletin" +
    "g map objects and for manipulating Map Markers.";
            superToolTip29.Items.Add(toolTipTitleItem29);
            superToolTip29.Items.Add(toolTipItem29);
            this.bbiMapSelection.SuperTip = superToolTip29;
            // 
            // popupMenu_MapControl1
            // 
            this.popupMenu_MapControl1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSurvey),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewPoleOwnership, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewPictures),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiEditMapObject, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapNearbyObjects, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSetCentrePoint, true)});
            this.popupMenu_MapControl1.Manager = this.barManager1;
            this.popupMenu_MapControl1.MenuCaption = "Map Menu";
            this.popupMenu_MapControl1.Name = "popupMenu_MapControl1";
            this.popupMenu_MapControl1.ShowCaption = true;
            this.popupMenu_MapControl1.CloseUp += new System.EventHandler(this.popupMenu_MapControl1_CloseUp);
            this.popupMenu_MapControl1.Popup += new System.EventHandler(this.popupMenu_MapControl1_Popup);
            // 
            // bsiSurvey
            // 
            this.bsiSurvey.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiSurvey.Caption = "<b>Survey</b>";
            this.bsiSurvey.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSurvey.Glyph")));
            this.bsiSurvey.Id = 147;
            this.bsiSurvey.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSurveyPole),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSurveyPoleSetClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTransferPolesToSurvey, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemovePolesFromSurvey),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShutdownRequired, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiShutdownClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTrafficManagmentRequired, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTrafficMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTrafficManagmentClear),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeferredSet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearDeferred),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPermission, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPermissionView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWorkToWorkOrder, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateAccessMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyJobs, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPasteJobs),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetPoleHistorical, true)});
            this.bsiSurvey.Name = "bsiSurvey";
            // 
            // bbiSurveyPole
            // 
            this.bbiSurveyPole.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSurveyPole.Caption = "<b>Survey</b> Selected Pole...";
            this.bbiSurveyPole.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSurveyPole.Glyph")));
            this.bbiSurveyPole.Id = 148;
            this.bbiSurveyPole.Name = "bbiSurveyPole";
            this.bbiSurveyPole.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSurveyPole_ItemClick);
            // 
            // bbiSurveyPoleSetClear
            // 
            this.bbiSurveyPoleSetClear.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSurveyPoleSetClear.Caption = "Survey Selected Pole(s) - Set as <b>Clear</b>...";
            this.bbiSurveyPoleSetClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSurveyPoleSetClear.Glyph")));
            this.bbiSurveyPoleSetClear.Id = 150;
            this.bbiSurveyPoleSetClear.Name = "bbiSurveyPoleSetClear";
            this.bbiSurveyPoleSetClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSurveyPoleSetClear_ItemClick);
            // 
            // bbiTransferPolesToSurvey
            // 
            this.bbiTransferPolesToSurvey.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTransferPolesToSurvey.Caption = "<b>Transfer</b> Selected Poles To Survey";
            this.bbiTransferPolesToSurvey.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTransferPolesToSurvey.Glyph")));
            this.bbiTransferPolesToSurvey.Id = 144;
            this.bbiTransferPolesToSurvey.Name = "bbiTransferPolesToSurvey";
            this.bbiTransferPolesToSurvey.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTransferPolesToSurvey_ItemClick);
            // 
            // bbiRemovePolesFromSurvey
            // 
            this.bbiRemovePolesFromSurvey.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiRemovePolesFromSurvey.Caption = "<b>Remove</b> Selected Poles From Survey";
            this.bbiRemovePolesFromSurvey.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRemovePolesFromSurvey.Glyph")));
            this.bbiRemovePolesFromSurvey.Id = 149;
            this.bbiRemovePolesFromSurvey.Name = "bbiRemovePolesFromSurvey";
            this.bbiRemovePolesFromSurvey.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemovePolesFromSurvey_ItemClick);
            // 
            // bbiShutdownRequired
            // 
            this.bbiShutdownRequired.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiShutdownRequired.Caption = "Shutdown - Set as <b>Required</b>";
            this.bbiShutdownRequired.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShutdownRequired.Glyph")));
            this.bbiShutdownRequired.Id = 155;
            this.bbiShutdownRequired.Name = "bbiShutdownRequired";
            this.bbiShutdownRequired.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShutdownRequired_ItemClick);
            // 
            // bbiShutdownClear
            // 
            this.bbiShutdownClear.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiShutdownClear.Caption = "Shutdown - <b>Clear</b>";
            this.bbiShutdownClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiShutdownClear.Glyph")));
            this.bbiShutdownClear.Id = 156;
            this.bbiShutdownClear.Name = "bbiShutdownClear";
            this.bbiShutdownClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiShutdownClear_ItemClick);
            // 
            // bbiTrafficManagmentRequired
            // 
            this.bbiTrafficManagmentRequired.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTrafficManagmentRequired.Caption = "Traffic Management - Set as <b>Required</b>...";
            this.bbiTrafficManagmentRequired.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTrafficManagmentRequired.Glyph")));
            this.bbiTrafficManagmentRequired.Id = 157;
            this.bbiTrafficManagmentRequired.Name = "bbiTrafficManagmentRequired";
            this.bbiTrafficManagmentRequired.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTrafficManagmentRequired_ItemClick);
            // 
            // bbiTrafficMap
            // 
            this.bbiTrafficMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTrafficMap.Caption = "Traffic Management - Create <b>Map</b>...";
            this.bbiTrafficMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTrafficMap.Glyph")));
            this.bbiTrafficMap.Id = 164;
            this.bbiTrafficMap.Name = "bbiTrafficMap";
            this.bbiTrafficMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTrafficMap_ItemClick);
            // 
            // bbiTrafficManagmentClear
            // 
            this.bbiTrafficManagmentClear.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTrafficManagmentClear.Caption = "Traffic Management - <b>Clear</b>";
            this.bbiTrafficManagmentClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiTrafficManagmentClear.Glyph")));
            this.bbiTrafficManagmentClear.Id = 158;
            this.bbiTrafficManagmentClear.Name = "bbiTrafficManagmentClear";
            this.bbiTrafficManagmentClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTrafficManagmentClear_ItemClick);
            // 
            // bbiDeferredSet
            // 
            this.bbiDeferredSet.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDeferredSet.Caption = "Set as <b>Deferred</b>...";
            this.bbiDeferredSet.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeferredSet.Glyph")));
            this.bbiDeferredSet.Id = 159;
            this.bbiDeferredSet.Name = "bbiDeferredSet";
            this.bbiDeferredSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeferredSet_ItemClick);
            // 
            // bbiClearDeferred
            // 
            this.bbiClearDeferred.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiClearDeferred.Caption = "<b>Clear</b> Deferred";
            this.bbiClearDeferred.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearDeferred.Glyph")));
            this.bbiClearDeferred.Id = 163;
            this.bbiClearDeferred.Name = "bbiClearDeferred";
            this.bbiClearDeferred.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearDeferred_ItemClick);
            // 
            // bbiPermission
            // 
            this.bbiPermission.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiPermission.Caption = "<b>Permission</b> Work...";
            this.bbiPermission.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPermission.Glyph")));
            this.bbiPermission.Id = 165;
            this.bbiPermission.Name = "bbiPermission";
            this.bbiPermission.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPermission.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPermission_ItemClick);
            // 
            // bbiPermissionView
            // 
            this.bbiPermissionView.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiPermissionView.Caption = "<b>View</b> Permissions";
            this.bbiPermissionView.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPermissionView.Glyph")));
            this.bbiPermissionView.Id = 166;
            this.bbiPermissionView.Name = "bbiPermissionView";
            this.bbiPermissionView.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPermissionView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPermissionView_ItemClick);
            // 
            // bbiAddWorkToWorkOrder
            // 
            this.bbiAddWorkToWorkOrder.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiAddWorkToWorkOrder.Caption = "<b>Work Order</b> - Add Work...";
            this.bbiAddWorkToWorkOrder.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddWorkToWorkOrder.Glyph")));
            this.bbiAddWorkToWorkOrder.Id = 168;
            this.bbiAddWorkToWorkOrder.Name = "bbiAddWorkToWorkOrder";
            this.bbiAddWorkToWorkOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWorkToWorkOrder_ItemClick);
            // 
            // bbiCreateAccessMap
            // 
            this.bbiCreateAccessMap.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCreateAccessMap.Caption = "Create <b>Access</b> Map...";
            this.bbiCreateAccessMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreateAccessMap.Glyph")));
            this.bbiCreateAccessMap.Id = 160;
            this.bbiCreateAccessMap.Name = "bbiCreateAccessMap";
            this.bbiCreateAccessMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateAccessMap_ItemClick);
            // 
            // bbiCopyJobs
            // 
            this.bbiCopyJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiCopyJobs.Caption = "<b>Copy</b> Jobs into Memory...";
            this.bbiCopyJobs.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCopyJobs.Glyph")));
            this.bbiCopyJobs.Id = 161;
            this.bbiCopyJobs.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiCopyJobs.LargeGlyph")));
            this.bbiCopyJobs.Name = "bbiCopyJobs";
            this.bbiCopyJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyJobs_ItemClick);
            // 
            // bbiPasteJobs
            // 
            this.bbiPasteJobs.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiPasteJobs.Caption = "<b>Paste</b> Jobs from Memory...";
            this.bbiPasteJobs.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPasteJobs.Glyph")));
            this.bbiPasteJobs.Id = 162;
            this.bbiPasteJobs.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiPasteJobs.LargeGlyph")));
            this.bbiPasteJobs.Name = "bbiPasteJobs";
            this.bbiPasteJobs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPasteJobs_ItemClick);
            // 
            // bbiSetPoleHistorical
            // 
            this.bbiSetPoleHistorical.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiSetPoleHistorical.Caption = "Set Pole as <b>Historical</b>...";
            this.bbiSetPoleHistorical.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetPoleHistorical.Glyph")));
            this.bbiSetPoleHistorical.Id = 167;
            this.bbiSetPoleHistorical.Name = "bbiSetPoleHistorical";
            this.bbiSetPoleHistorical.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetPoleHistorical_ItemClick);
            // 
            // bbiViewPoleOwnership
            // 
            this.bbiViewPoleOwnership.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewPoleOwnership.Caption = "View Pole <b>Ownership</b>";
            this.bbiViewPoleOwnership.Enabled = false;
            this.bbiViewPoleOwnership.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiViewPoleOwnership.Glyph")));
            this.bbiViewPoleOwnership.Id = 154;
            this.bbiViewPoleOwnership.Name = "bbiViewPoleOwnership";
            this.bbiViewPoleOwnership.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewPoleOwnership_ItemClick);
            // 
            // bbiViewPictures
            // 
            this.bbiViewPictures.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiViewPictures.Caption = "View <b>Pictures</b>";
            this.bbiViewPictures.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiViewPictures.Glyph")));
            this.bbiViewPictures.Id = 169;
            this.bbiViewPictures.Name = "bbiViewPictures";
            this.bbiViewPictures.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewPictures_ItemClick);
            // 
            // bsiEditMapObject
            // 
            this.bsiEditMapObject.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiEditMapObject.Caption = "<b>Edit</b>...";
            this.bsiEditMapObject.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiEditMapObject.Glyph")));
            this.bsiEditMapObject.Id = 131;
            this.bsiEditMapObject.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiEditMapObject.LargeGlyph")));
            this.bsiEditMapObject.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditSelectedMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditselectedMapObjects)});
            this.bsiEditMapObject.Name = "bsiEditMapObject";
            // 
            // bbiEditSelectedMapObjects
            // 
            this.bbiEditSelectedMapObjects.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiEditSelectedMapObjects.Caption = "<b>Edit</b> Selected Map Objects";
            this.bbiEditSelectedMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEditSelectedMapObjects.Glyph")));
            this.bbiEditSelectedMapObjects.Id = 129;
            this.bbiEditSelectedMapObjects.Name = "bbiEditSelectedMapObjects";
            this.bbiEditSelectedMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditSelectedMapObjects_ItemClick);
            // 
            // bbiBlockEditselectedMapObjects
            // 
            this.bbiBlockEditselectedMapObjects.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiBlockEditselectedMapObjects.Caption = "<B>Block Edit</b> Selected Map Objects";
            this.bbiBlockEditselectedMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditselectedMapObjects.Glyph")));
            this.bbiBlockEditselectedMapObjects.Id = 130;
            this.bbiBlockEditselectedMapObjects.Name = "bbiBlockEditselectedMapObjects";
            this.bbiBlockEditselectedMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditselectedMapObjects_ItemClick);
            // 
            // bbiDeleteMapObjects
            // 
            this.bbiDeleteMapObjects.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiDeleteMapObjects.Caption = "<b>Delete</b> Selected Map Objects";
            this.bbiDeleteMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteMapObjects.Glyph")));
            this.bbiDeleteMapObjects.Id = 94;
            this.bbiDeleteMapObjects.Name = "bbiDeleteMapObjects";
            this.bbiDeleteMapObjects.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteMapObjects_ItemClick);
            // 
            // bsiMapNearbyObjects
            // 
            this.bsiMapNearbyObjects.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiMapNearbyObjects.Caption = "<b>Find</b> Nearby Map Objects";
            this.bsiMapNearbyObjects.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bsiMapNearbyObjects.Id = 135;
            this.bsiMapNearbyObjects.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiGazetteerLoadObjectsWithinRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNearbyObjectsFind),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerClearMapSearch, true)});
            this.bsiMapNearbyObjects.Name = "bsiMapNearbyObjects";
            // 
            // beiGazetteerLoadObjectsWithinRange
            // 
            this.beiGazetteerLoadObjectsWithinRange.Caption = "Map Objects Range:";
            this.beiGazetteerLoadObjectsWithinRange.Edit = this.repositoryItemSpinEditGazetteerRange;
            this.beiGazetteerLoadObjectsWithinRange.EditValue = "10.00";
            this.beiGazetteerLoadObjectsWithinRange.Id = 103;
            this.beiGazetteerLoadObjectsWithinRange.Name = "beiGazetteerLoadObjectsWithinRange";
            this.beiGazetteerLoadObjectsWithinRange.Width = 130;
            // 
            // repositoryItemSpinEditGazetteerRange
            // 
            this.repositoryItemSpinEditGazetteerRange.AutoHeight = false;
            this.repositoryItemSpinEditGazetteerRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditGazetteerRange.Mask.EditMask = "#######0.00 Metres";
            this.repositoryItemSpinEditGazetteerRange.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditGazetteerRange.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEditGazetteerRange.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.repositoryItemSpinEditGazetteerRange.Name = "repositoryItemSpinEditGazetteerRange";
            // 
            // bbiNearbyObjectsFind
            // 
            this.bbiNearbyObjectsFind.Caption = "Find Nearby Map Objects";
            this.bbiNearbyObjectsFind.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bbiNearbyObjectsFind.Id = 136;
            this.bbiNearbyObjectsFind.Name = "bbiNearbyObjectsFind";
            this.bbiNearbyObjectsFind.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNearbyObjectsFind_ItemClick);
            // 
            // bbiGazetteerClearMapSearch
            // 
            this.bbiGazetteerClearMapSearch.Caption = "Clear Map Search Results";
            this.bbiGazetteerClearMapSearch.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGazetteerClearMapSearch.Glyph")));
            this.bbiGazetteerClearMapSearch.Id = 116;
            this.bbiGazetteerClearMapSearch.Name = "bbiGazetteerClearMapSearch";
            this.bbiGazetteerClearMapSearch.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerClearMapSearch_ItemClick);
            // 
            // bsiSetCentrePoint
            // 
            this.bsiSetCentrePoint.Caption = "Set Centre Point...";
            this.bsiSetCentrePoint.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSetCentrePoint.Glyph")));
            this.bsiSetCentrePoint.Id = 141;
            this.bsiSetCentrePoint.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetLocalityCentre),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetDistrictCentre),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetSiteCentre, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSetClientCentre)});
            this.bsiSetCentrePoint.Name = "bsiSetCentrePoint";
            // 
            // bbiSetLocalityCentre
            // 
            this.bbiSetLocalityCentre.Caption = "Set Locality Centre Point";
            this.bbiSetLocalityCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetLocalityCentre.Glyph")));
            this.bbiSetLocalityCentre.Id = 96;
            this.bbiSetLocalityCentre.Name = "bbiSetLocalityCentre";
            this.bbiSetLocalityCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetLocalityCentre_ItemClick);
            // 
            // bbiSetDistrictCentre
            // 
            this.bbiSetDistrictCentre.Caption = "Set District Centre Point";
            this.bbiSetDistrictCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetDistrictCentre.Glyph")));
            this.bbiSetDistrictCentre.Id = 98;
            this.bbiSetDistrictCentre.Name = "bbiSetDistrictCentre";
            this.bbiSetDistrictCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetDistrictCentre_ItemClick);
            // 
            // bbiSetSiteCentre
            // 
            this.bbiSetSiteCentre.Caption = "Set Site Centre Point";
            this.bbiSetSiteCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetSiteCentre.Glyph")));
            this.bbiSetSiteCentre.Id = 143;
            this.bbiSetSiteCentre.Name = "bbiSetSiteCentre";
            this.bbiSetSiteCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetSiteCentre_ItemClick);
            // 
            // bbiSetClientCentre
            // 
            this.bbiSetClientCentre.Caption = "Set Client Centre Point";
            this.bbiSetClientCentre.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetClientCentre.Glyph")));
            this.bbiSetClientCentre.Id = 142;
            this.bbiSetClientCentre.Name = "bbiSetClientCentre";
            this.bbiSetClientCentre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetClientCentre_ItemClick);
            // 
            // bbiCaptureCustomerPermission
            // 
            this.bbiCaptureCustomerPermission.Caption = "Capture Customer Permission Signature for Selected Poles...";
            this.bbiCaptureCustomerPermission.Enabled = false;
            this.bbiCaptureCustomerPermission.Id = 151;
            this.bbiCaptureCustomerPermission.Name = "bbiCaptureCustomerPermission";
            // 
            // bbiQueryTool
            // 
            this.bbiQueryTool.Caption = "Query";
            this.bbiQueryTool.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiQueryTool.Glyph")));
            this.bbiQueryTool.GroupIndex = 1;
            this.bbiQueryTool.Id = 85;
            this.bbiQueryTool.Name = "bbiQueryTool";
            superToolTip30.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem30.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image28")));
            toolTipTitleItem30.Appearance.Options.UseImage = true;
            toolTipTitleItem30.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem30.Image")));
            toolTipTitleItem30.Text = "Query Object Tool - Information";
            toolTipItem30.LeftIndent = 6;
            toolTipItem30.Text = resources.GetString("toolTipItem30.Text");
            superToolTip30.Items.Add(toolTipTitleItem30);
            superToolTip30.Items.Add(toolTipItem30);
            this.bbiQueryTool.SuperTip = superToolTip30;
            this.bbiQueryTool.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiQueryTool_ItemClick);
            // 
            // bbiMeasureLine
            // 
            this.bbiMeasureLine.Caption = "Measure";
            this.bbiMeasureLine.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMeasureLine.Glyph")));
            this.bbiMeasureLine.GroupIndex = 1;
            this.bbiMeasureLine.Id = 64;
            this.bbiMeasureLine.Name = "bbiMeasureLine";
            superToolTip31.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem31.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image29")));
            toolTipTitleItem31.Appearance.Options.UseImage = true;
            toolTipTitleItem31.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem31.Image")));
            toolTipTitleItem31.Text = "Measure Tool - Information";
            toolTipItem31.LeftIndent = 6;
            toolTipItem31.Text = resources.GetString("toolTipItem31.Text");
            superToolTip31.Items.Add(toolTipTitleItem31);
            superToolTip31.Items.Add(toolTipItem31);
            this.bbiMeasureLine.SuperTip = superToolTip31;
            this.bbiMeasureLine.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMeasureLine_ItemClick);
            // 
            // bbiEditMapObjects
            // 
            this.bbiEditMapObjects.Caption = "Edit Selected Map Objects";
            this.bbiEditMapObjects.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEditMapObjects.Glyph")));
            this.bbiEditMapObjects.Id = 93;
            this.bbiEditMapObjects.Name = "bbiEditMapObjects";
            // 
            // barSubItemMapEdit
            // 
            this.barSubItemMapEdit.Caption = "Selected Objects";
            this.barSubItemMapEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("barSubItemMapEdit.Glyph")));
            this.barSubItemMapEdit.Id = 102;
            this.barSubItemMapEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditMapObjects),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteMapObjects)});
            this.barSubItemMapEdit.MenuCaption = "Map Menu";
            this.barSubItemMapEdit.Name = "barSubItemMapEdit";
            this.barSubItemMapEdit.ShowMenuCaption = true;
            this.barSubItemMapEdit.Popup += new System.EventHandler(this.barSubItemMapEdit_Popup);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Edit Mode";
            this.barSubItem1.Id = 50;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // bciEditingNone
            // 
            this.bciEditingNone.Caption = "No Editing";
            this.bciEditingNone.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingNone.Glyph")));
            this.bciEditingNone.GroupIndex = 2;
            this.bciEditingNone.Id = 51;
            this.bciEditingNone.Name = "bciEditingNone";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "No Object Vertex Editing - Information\r\n";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to switch <b>off</b> Polygon and Polyline vertex node editing.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bciEditingNone.SuperTip = superToolTip3;
            this.bciEditingNone.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingNone_CheckedChanged);
            // 
            // bciEditingMove
            // 
            this.bciEditingMove.BindableChecked = true;
            this.bciEditingMove.Caption = "Move \\ Resize";
            this.bciEditingMove.Checked = true;
            this.bciEditingMove.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingMove.Glyph")));
            this.bciEditingMove.GroupIndex = 2;
            this.bciEditingMove.Id = 52;
            this.bciEditingMove.Name = "bciEditingMove";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Move \\ Resize Object - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to enable Polygon and Polyline <b>movement</b> and <b>resizing</b>.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciEditingMove.SuperTip = superToolTip4;
            this.bciEditingMove.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingMove_CheckedChanged);
            // 
            // bciEditingAdd
            // 
            this.bciEditingAdd.Caption = "Node Adding";
            this.bciEditingAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingAdd.Glyph")));
            this.bciEditingAdd.GroupIndex = 2;
            this.bciEditingAdd.Id = 53;
            this.bciEditingAdd.Name = "bciEditingAdd";
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Add Object Vertices - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to switch on Polygon and Polyline vertex <b>node adding.</b>\r\n\r\nTo use, " +
    "click on the map polygon\\polyline to select it then click in the position to add" +
    " the new vertex.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciEditingAdd.SuperTip = superToolTip5;
            this.bciEditingAdd.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingAdd_CheckedChanged);
            // 
            // bciEditingEdit
            // 
            this.bciEditingEdit.Caption = "Node Editing";
            this.bciEditingEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bciEditingEdit.Glyph")));
            this.bciEditingEdit.GroupIndex = 2;
            this.bciEditingEdit.Id = 54;
            this.bciEditingEdit.Name = "bciEditingEdit";
            superToolTip6.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem6.Image")));
            toolTipTitleItem6.Text = "Edit Object Vertices - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to switch on Polygon and Polyline vertex <b>node deleting.</b>\r\n\r\nTo use" +
    ", click on the map polygon\\polyline to select it then click on the node to be de" +
    "leted andpress delete.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bciEditingEdit.SuperTip = superToolTip6;
            this.bciEditingEdit.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciEditingEdit_CheckedChanged);
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(((DevExpress.XtraBars.BarLinkUserDefines)((DevExpress.XtraBars.BarLinkUserDefines.Caption | DevExpress.XtraBars.BarLinkUserDefines.Width))), this.beiPopupContainerScale, "Scale:", false, true, true, 106),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiScreenCoords),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiDistanceMeasured),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSnap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiSyncSelection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiLayerCount)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // beiPopupContainerScale
            // 
            this.beiPopupContainerScale.Caption = "Scale:";
            this.beiPopupContainerScale.Edit = this.repositoryItemPopupContainerEdit1;
            this.beiPopupContainerScale.EditValue = "Scale:";
            this.beiPopupContainerScale.Id = 62;
            this.beiPopupContainerScale.Name = "beiPopupContainerScale";
            this.beiPopupContainerScale.Width = 100;
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.CloseOnOuterMouseClick = false;
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControl2;
            this.repositoryItemPopupContainerEdit1.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEdit2_QueryResultValue);
            // 
            // popupContainerControl2
            // 
            this.popupContainerControl2.Controls.Add(this.btnSetScale);
            this.popupContainerControl2.Controls.Add(this.layoutControl4);
            this.popupContainerControl2.Controls.Add(this.btnViewFullExtent);
            this.popupContainerControl2.Location = new System.Drawing.Point(6, 210);
            this.popupContainerControl2.Name = "popupContainerControl2";
            this.popupContainerControl2.Size = new System.Drawing.Size(194, 207);
            this.popupContainerControl2.TabIndex = 23;
            // 
            // btnSetScale
            // 
            this.btnSetScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetScale.Location = new System.Drawing.Point(3, 181);
            this.btnSetScale.Name = "btnSetScale";
            this.btnSetScale.Size = new System.Drawing.Size(75, 23);
            this.btnSetScale.TabIndex = 4;
            this.btnSetScale.Text = "OK";
            this.btnSetScale.Click += new System.EventHandler(this.btnSetScale_Click);
            // 
            // layoutControl4
            // 
            this.layoutControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl4.Controls.Add(this.seUserDefinedScale);
            this.layoutControl4.Controls.Add(this.gridSplitContainer7);
            this.layoutControl4.Location = new System.Drawing.Point(3, 3);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup7;
            this.layoutControl4.Size = new System.Drawing.Size(188, 176);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // seUserDefinedScale
            // 
            this.seUserDefinedScale.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seUserDefinedScale.Location = new System.Drawing.Point(100, 153);
            this.seUserDefinedScale.MenuManager = this.barManager1;
            this.seUserDefinedScale.Name = "seUserDefinedScale";
            this.seUserDefinedScale.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seUserDefinedScale.Properties.IsFloatValue = false;
            this.seUserDefinedScale.Properties.Mask.EditMask = "#####0";
            this.seUserDefinedScale.Properties.MaxValue = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.seUserDefinedScale.Size = new System.Drawing.Size(85, 20);
            this.seUserDefinedScale.StyleController = this.layoutControl4;
            this.seUserDefinedScale.TabIndex = 5;
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Grid = this.gridControl4;
            this.gridSplitContainer7.Location = new System.Drawing.Point(3, 3);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer7.Size = new System.Drawing.Size(182, 146);
            this.gridSplitContainer7.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp01254ATTreePickerscalelistBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(182, 146);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp01254ATTreePickerscalelistBindingSource
            // 
            this.sp01254ATTreePickerscalelistBindingSource.DataMember = "sp01254_AT_Tree_Picker_scale_list";
            this.sp01254ATTreePickerscalelistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colScaleDescription,
            this.colScale});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScale, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseDown);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colScaleDescription
            // 
            this.colScaleDescription.Caption = "Scale";
            this.colScaleDescription.FieldName = "ScaleDescription";
            this.colScaleDescription.Name = "colScaleDescription";
            this.colScaleDescription.OptionsColumn.AllowEdit = false;
            this.colScaleDescription.OptionsColumn.AllowFocus = false;
            this.colScaleDescription.OptionsColumn.ReadOnly = true;
            this.colScaleDescription.Visible = true;
            this.colScaleDescription.VisibleIndex = 0;
            this.colScaleDescription.Width = 162;
            // 
            // colScale
            // 
            this.colScale.Caption = "Scale Number";
            this.colScale.FieldName = "Scale";
            this.colScale.Name = "colScale";
            this.colScale.OptionsColumn.AllowEdit = false;
            this.colScale.OptionsColumn.AllowFocus = false;
            this.colScale.OptionsColumn.ReadOnly = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19,
            this.layoutControlItem20});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(188, 176);
            this.layoutControlGroup7.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.gridSplitContainer7;
            this.layoutControlItem19.CustomizationFormText = "Scale Grid:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(186, 150);
            this.layoutControlItem19.Text = "Scale Grid:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AllowHtmlStringInCaption = true;
            this.layoutControlItem20.Control = this.seUserDefinedScale;
            this.layoutControlItem20.CustomizationFormText = "User Defined Scale:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 150);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(186, 24);
            this.layoutControlItem20.Text = "User Defined Scale:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(94, 13);
            // 
            // btnViewFullExtent
            // 
            this.btnViewFullExtent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnViewFullExtent.Location = new System.Drawing.Point(98, 181);
            this.btnViewFullExtent.Name = "btnViewFullExtent";
            this.btnViewFullExtent.Size = new System.Drawing.Size(93, 22);
            this.btnViewFullExtent.TabIndex = 12;
            this.btnViewFullExtent.Text = "Full Map Extent";
            this.btnViewFullExtent.Click += new System.EventHandler(this.btnViewFullExtent_Click);
            // 
            // bsiScreenCoords
            // 
            this.bsiScreenCoords.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiScreenCoords.Caption = "Location:";
            this.bsiScreenCoords.Id = 42;
            this.bsiScreenCoords.Name = "bsiScreenCoords";
            this.bsiScreenCoords.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiScreenCoords.Width = 200;
            // 
            // bsiDistanceMeasured
            // 
            this.bsiDistanceMeasured.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiDistanceMeasured.Caption = "Distance: 0.00";
            this.bsiDistanceMeasured.Id = 66;
            this.bsiDistanceMeasured.Name = "bsiDistanceMeasured";
            this.bsiDistanceMeasured.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiDistanceMeasured.Width = 350;
            // 
            // bsiSnap
            // 
            this.bsiSnap.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiSnap.Caption = "Snap: Off";
            this.bsiSnap.Id = 46;
            this.bsiSnap.Name = "bsiSnap";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Snap - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiSnap.SuperTip = superToolTip1;
            this.bsiSnap.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiSnap.Width = 50;
            this.bsiSnap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSnap_ItemClick);
            // 
            // bsiSyncSelection
            // 
            this.bsiSyncSelection.Caption = "Sync Map Selection: Off";
            this.bsiSyncSelection.Id = 145;
            this.bsiSyncSelection.Name = "bsiSyncSelection";
            toolTipTitleItem2.Text = "Sync Map Selection - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to toggle on and off the Synchronise Map and List Selection.\r\n\r\nIf set t" +
    "o On, when selecting poles on the map, those poles will be selected in the list " +
    "of available poles. ";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bsiSyncSelection.SuperTip = superToolTip2;
            this.bsiSyncSelection.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiSyncSelection.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bsiSyncSelection_ItemClick);
            // 
            // bsiLayerCount
            // 
            this.bsiLayerCount.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiLayerCount.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiLayerCount.Caption = "Layer Count: 0";
            this.bsiLayerCount.Id = 43;
            this.bsiLayerCount.Name = "bsiLayerCount";
            this.bsiLayerCount.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiLayerCount.Width = 100;
            // 
            // bsiZoom
            // 
            this.bsiZoom.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiZoom.Caption = "Zoom:";
            this.bsiZoom.Id = 38;
            this.bsiZoom.Name = "bsiZoom";
            this.bsiZoom.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiZoom.Width = 100;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Location:";
            this.barButtonItem1.Id = 39;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeFramesCount = 1;
            this.dockManager1.DockModeVS2005FadeSpeed = 1;
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel5,
            this.dockPanelSurvey,
            this.dockPanelGazetteer,
            this.dockPanel1,
            this.dockPanel6,
            this.dockPanelLegend});
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelContainer1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            this.dockManager1.ActivePanelChanged += new DevExpress.XtraBars.Docking.ActivePanelChangedEventHandler(this.dockManager1_ActivePanelChanged);
            this.dockManager1.ClosedPanel += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockManager1_ClosedPanel);
            // 
            // dockPanel5
            // 
            this.dockPanel5.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel5.Appearance.Options.UseBackColor = true;
            this.dockPanel5.Controls.Add(this.dockPanel5_Container);
            this.dockPanel5.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel5.FloatVertical = true;
            this.dockPanel5.ID = new System.Guid("e34b70c8-e235-4065-aa91-83ac9221f164");
            this.dockPanel5.Location = new System.Drawing.Point(1186, 42);
            this.dockPanel5.Name = "dockPanel5";
            this.dockPanel5.OriginalSize = new System.Drawing.Size(195, 771);
            this.dockPanel5.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanel5.SavedIndex = 0;
            this.dockPanel5.Size = new System.Drawing.Size(195, 751);
            this.dockPanel5.Text = "Map Legend";
            this.dockPanel5.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel5_Container
            // 
            this.dockPanel5_Container.Controls.Add(this.xtraScrollableControl1);
            this.dockPanel5_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel5_Container.Name = "dockPanel5_Container";
            this.dockPanel5_Container.Size = new System.Drawing.Size(189, 719);
            this.dockPanel5_Container.TabIndex = 0;
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraScrollableControl1.Appearance.Options.UseBackColor = true;
            this.xtraScrollableControl1.Controls.Add(this.pictureBoxLegend);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(189, 719);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // pictureBoxLegend
            // 
            this.pictureBoxLegend.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxLegend.Name = "pictureBoxLegend";
            this.pictureBoxLegend.Size = new System.Drawing.Size(150, 300);
            this.pictureBoxLegend.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxLegend.TabIndex = 0;
            this.pictureBoxLegend.TabStop = false;
            // 
            // dockPanelSurvey
            // 
            this.dockPanelSurvey.Controls.Add(this.controlContainer2);
            this.dockPanelSurvey.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelSurvey.FloatLocation = new System.Drawing.Point(774, 265);
            this.dockPanelSurvey.FloatSize = new System.Drawing.Size(561, 81);
            this.dockPanelSurvey.ID = new System.Guid("cf16f131-39e9-4d65-91bd-bfd36b9ded93");
            this.dockPanelSurvey.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelSurvey.Name = "dockPanelSurvey";
            this.dockPanelSurvey.Options.ShowCloseButton = false;
            this.dockPanelSurvey.Options.ShowMaximizeButton = false;
            this.dockPanelSurvey.OriginalSize = new System.Drawing.Size(200, 83);
            this.dockPanelSurvey.SavedIndex = 2;
            this.dockPanelSurvey.Size = new System.Drawing.Size(561, 81);
            this.dockPanelSurvey.Text = "Survey";
            this.dockPanelSurvey.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelSurvey.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelSurvey_ClosingPanel);
            this.dockPanelSurvey.DockChanged += new System.EventHandler(this.dockPanelSurvey_DockChanged);
            // 
            // controlContainer2
            // 
            this.controlContainer2.Controls.Add(this.textEditPlotTreesAgainstPole);
            this.controlContainer2.Controls.Add(this.labelControl6);
            this.controlContainer2.Controls.Add(this.btnGPS);
            this.controlContainer2.Controls.Add(this.buttonEditSurvey);
            this.controlContainer2.Controls.Add(this.labelControl4);
            this.controlContainer2.Location = new System.Drawing.Point(2, 28);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(557, 51);
            this.controlContainer2.TabIndex = 0;
            // 
            // textEditPlotTreesAgainstPole
            // 
            this.textEditPlotTreesAgainstPole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEditPlotTreesAgainstPole.EditValue = "No Pole Selected - click on pole on Map or Pole Map Object List";
            this.textEditPlotTreesAgainstPole.Location = new System.Drawing.Point(125, 28);
            this.textEditPlotTreesAgainstPole.MenuManager = this.barManager1;
            this.textEditPlotTreesAgainstPole.Name = "textEditPlotTreesAgainstPole";
            this.textEditPlotTreesAgainstPole.Properties.NullText = "No Pole Selected - Choose pole by clicking on it in either the map or Pole Map Ob" +
    "ject List";
            this.textEditPlotTreesAgainstPole.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEditPlotTreesAgainstPole, true);
            this.textEditPlotTreesAgainstPole.Size = new System.Drawing.Size(310, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEditPlotTreesAgainstPole, optionsSpelling28);
            this.textEditPlotTreesAgainstPole.TabIndex = 30;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(6, 31);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(113, 13);
            this.labelControl6.TabIndex = 29;
            this.labelControl6.Text = "Plot Trees against Pole:";
            // 
            // btnGPS
            // 
            this.btnGPS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGPS.Location = new System.Drawing.Point(443, 26);
            this.btnGPS.Name = "btnGPS";
            this.btnGPS.Size = new System.Drawing.Size(111, 23);
            this.btnGPS.TabIndex = 6;
            this.btnGPS.Text = "Show GPS Position";
            this.btnGPS.Click += new System.EventHandler(this.btnGPS_Click);
            // 
            // buttonEditSurvey
            // 
            this.buttonEditSurvey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditSurvey.Location = new System.Drawing.Point(51, 3);
            this.buttonEditSurvey.MenuManager = this.barManager1;
            this.buttonEditSurvey.Name = "buttonEditSurvey";
            this.buttonEditSurvey.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, "Click to select Survey to work with.", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject10, "Clear Selected Survey", "clear", null, true)});
            this.buttonEditSurvey.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditSurvey.Size = new System.Drawing.Size(503, 20);
            this.buttonEditSurvey.TabIndex = 1;
            this.buttonEditSurvey.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditSurvey_ButtonClick);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(38, 13);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Survey:";
            // 
            // dockPanelGazetteer
            // 
            this.dockPanelGazetteer.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanelGazetteer.Appearance.Options.UseBackColor = true;
            this.dockPanelGazetteer.Controls.Add(this.dockPanel7_Container);
            this.dockPanelGazetteer.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelGazetteer.FloatSize = new System.Drawing.Size(316, 554);
            this.dockPanelGazetteer.ID = new System.Guid("90273104-7905-4f79-ab1c-1b7deb4093f1");
            this.dockPanelGazetteer.Location = new System.Drawing.Point(1037, 42);
            this.dockPanelGazetteer.Name = "dockPanelGazetteer";
            this.dockPanelGazetteer.OriginalSize = new System.Drawing.Size(335, 200);
            this.dockPanelGazetteer.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this.dockPanelGazetteer.SavedIndex = 0;
            this.dockPanelGazetteer.Size = new System.Drawing.Size(335, 678);
            this.dockPanelGazetteer.Text = "Gazetteer";
            this.dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelGazetteer.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelGazetteer_ClosingPanel);
            this.dockPanelGazetteer.DockChanged += new System.EventHandler(this.dockPanelGazetteer_DockChanged);
            // 
            // dockPanel7_Container
            // 
            this.dockPanel7_Container.Controls.Add(this.splitContainerControl1);
            this.dockPanel7_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel7_Container.Name = "dockPanel7_Container";
            this.dockPanel7_Container.Size = new System.Drawing.Size(329, 646);
            this.dockPanel7_Container.TabIndex = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.buttonEditGazetteerFindValue);
            this.splitContainerControl1.Panel1.Controls.Add(this.lookUpEditGazetteerSearchType);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.radioGroupGazetteerMatchPattern);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Find Criteria   [Enter criteria then click Find button]";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Find Results   [Double click on result row to view on map]";
            this.splitContainerControl1.Size = new System.Drawing.Size(329, 646);
            this.splitContainerControl1.SplitterPosition = 98;
            this.splitContainerControl1.TabIndex = 7;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(3, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(64, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Search Type:";
            // 
            // buttonEditGazetteerFindValue
            // 
            this.buttonEditGazetteerFindValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditGazetteerFindValue.Location = new System.Drawing.Point(74, 50);
            this.buttonEditGazetteerFindValue.MenuManager = this.barManager1;
            this.buttonEditGazetteerFindValue.Name = "buttonEditGazetteerFindValue";
            this.buttonEditGazetteerFindValue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Find", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject11, "", null, null, true)});
            this.buttonEditGazetteerFindValue.Properties.MaxLength = 100;
            this.buttonEditGazetteerFindValue.Size = new System.Drawing.Size(249, 20);
            this.buttonEditGazetteerFindValue.TabIndex = 5;
            this.buttonEditGazetteerFindValue.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditGazetteerFindValue_ButtonClick);
            this.buttonEditGazetteerFindValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buttonEditGazetteerFindValue_KeyPress);
            // 
            // lookUpEditGazetteerSearchType
            // 
            this.lookUpEditGazetteerSearchType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEditGazetteerSearchType.Location = new System.Drawing.Point(74, 3);
            this.lookUpEditGazetteerSearchType.MenuManager = this.barManager1;
            this.lookUpEditGazetteerSearchType.Name = "lookUpEditGazetteerSearchType";
            this.lookUpEditGazetteerSearchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditGazetteerSearchType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Search Type", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("RecordOrder", "Record Order", 75, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEditGazetteerSearchType.Properties.DataSource = this.sp01311ATTreePickerGazetteerSearchTypesBindingSource;
            this.lookUpEditGazetteerSearchType.Properties.DisplayMember = "Description";
            this.lookUpEditGazetteerSearchType.Properties.NullText = "";
            this.lookUpEditGazetteerSearchType.Properties.ValueMember = "Description";
            this.lookUpEditGazetteerSearchType.Size = new System.Drawing.Size(249, 20);
            this.lookUpEditGazetteerSearchType.TabIndex = 1;
            this.lookUpEditGazetteerSearchType.EditValueChanged += new System.EventHandler(this.lookUpEditGazetteerSearchType_EditValueChanged);
            // 
            // sp01311ATTreePickerGazetteerSearchTypesBindingSource
            // 
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataMember = "sp01311_AT_Tree_Picker_Gazetteer_Search_Types";
            this.sp01311ATTreePickerGazetteerSearchTypesBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(3, 53);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(66, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Search Value:";
            // 
            // radioGroupGazetteerMatchPattern
            // 
            this.radioGroupGazetteerMatchPattern.EditValue = 0;
            this.radioGroupGazetteerMatchPattern.Location = new System.Drawing.Point(74, 26);
            this.radioGroupGazetteerMatchPattern.MenuManager = this.barManager1;
            this.radioGroupGazetteerMatchPattern.Name = "radioGroupGazetteerMatchPattern";
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.radioGroupGazetteerMatchPattern.Properties.Appearance.Options.UseBackColor = true;
            this.radioGroupGazetteerMatchPattern.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Starts With"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Contains")});
            this.radioGroupGazetteerMatchPattern.Size = new System.Drawing.Size(163, 21);
            this.radioGroupGazetteerMatchPattern.TabIndex = 4;
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.controlContainer1);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel1.ID = new System.Guid("0a8caa4f-7106-4f54-ae4e-fd61a3486fb4");
            this.dockPanel1.Location = new System.Drawing.Point(0, 595);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 125);
            this.dockPanel1.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this.dockPanel1.SavedIndex = 0;
            this.dockPanel1.Size = new System.Drawing.Size(1372, 125);
            this.dockPanel1.Text = "Events";
            this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanel1.DockChanged += new System.EventHandler(this.dockPanel1_DockChanged);
            // 
            // controlContainer1
            // 
            this.controlContainer1.Controls.Add(this.memoEdit1);
            this.controlContainer1.Location = new System.Drawing.Point(3, 29);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(1366, 93);
            this.controlContainer1.TabIndex = 0;
            // 
            // dockPanel6
            // 
            this.dockPanel6.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.dockPanel6.Appearance.Options.UseBackColor = true;
            this.dockPanel6.Controls.Add(this.dockPanel6_Container);
            this.dockPanel6.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanel6.FloatLocation = new System.Drawing.Point(1118, 192);
            this.dockPanel6.FloatSize = new System.Drawing.Size(225, 70);
            this.dockPanel6.ID = new System.Guid("275ce391-de52-42d6-8177-dc0edbbfcecf");
            this.dockPanel6.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanel6.Name = "dockPanel6";
            this.dockPanel6.OriginalSize = new System.Drawing.Size(221, 70);
            this.dockPanel6.SavedIndex = 1;
            this.dockPanel6.Size = new System.Drawing.Size(225, 70);
            this.dockPanel6.Text = "Scale Bar";
            this.dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanel6.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanel6_ClosingPanel);
            this.dockPanel6.DockChanged += new System.EventHandler(this.dockPanel6_DockChanged);
            this.dockPanel6.Resize += new System.EventHandler(this.dockPanel6_Resize);
            // 
            // dockPanel6_Container
            // 
            this.dockPanel6_Container.Controls.Add(this.pictureBoxScaleBar);
            this.dockPanel6_Container.Location = new System.Drawing.Point(2, 28);
            this.dockPanel6_Container.Name = "dockPanel6_Container";
            this.dockPanel6_Container.Size = new System.Drawing.Size(221, 40);
            this.dockPanel6_Container.TabIndex = 0;
            // 
            // pictureBoxScaleBar
            // 
            this.pictureBoxScaleBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxScaleBar.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxScaleBar.Name = "pictureBoxScaleBar";
            this.pictureBoxScaleBar.Size = new System.Drawing.Size(221, 40);
            this.pictureBoxScaleBar.TabIndex = 0;
            this.pictureBoxScaleBar.TabStop = false;
            this.pictureBoxScaleBar.Resize += new System.EventHandler(this.pictureBoxScaleBar_Resize);
            // 
            // dockPanelLegend
            // 
            this.dockPanelLegend.Controls.Add(this.controlContainer3);
            this.dockPanelLegend.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this.dockPanelLegend.FloatLocation = new System.Drawing.Point(1104, 302);
            this.dockPanelLegend.FloatSize = new System.Drawing.Size(160, 434);
            this.dockPanelLegend.ID = new System.Guid("f2317448-fdf1-4f11-8848-25b05d197932");
            this.dockPanelLegend.Location = new System.Drawing.Point(-32768, -32768);
            this.dockPanelLegend.Name = "dockPanelLegend";
            this.dockPanelLegend.Options.ShowCloseButton = false;
            this.dockPanelLegend.Options.ShowMaximizeButton = false;
            this.dockPanelLegend.OriginalSize = new System.Drawing.Size(163, 412);
            this.dockPanelLegend.SavedIndex = 0;
            this.dockPanelLegend.Size = new System.Drawing.Size(160, 434);
            this.dockPanelLegend.Text = "Legend";
            this.dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this.dockPanelLegend.ClosingPanel += new DevExpress.XtraBars.Docking.DockPanelCancelEventHandler(this.dockPanelLegend_ClosingPanel);
            this.dockPanelLegend.DockChanged += new System.EventHandler(this.dockPanelLegend_DockChanged);
            // 
            // controlContainer3
            // 
            this.controlContainer3.Controls.Add(this.colorEditTreeLegend5);
            this.controlContainer3.Controls.Add(this.groupControl3);
            this.controlContainer3.Controls.Add(this.groupControl1);
            this.controlContainer3.Controls.Add(this.btnSurveyRefreshPoles);
            this.controlContainer3.Location = new System.Drawing.Point(2, 28);
            this.controlContainer3.Name = "controlContainer3";
            this.controlContainer3.Size = new System.Drawing.Size(156, 404);
            this.controlContainer3.TabIndex = 0;
            // 
            // colorEditTreeLegend5
            // 
            this.colorEditTreeLegend5.EditValue = System.Drawing.Color.DodgerBlue;
            this.colorEditTreeLegend5.Location = new System.Drawing.Point(6, 353);
            this.colorEditTreeLegend5.MenuManager = this.barManager1;
            this.colorEditTreeLegend5.Name = "colorEditTreeLegend5";
            this.colorEditTreeLegend5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditTreeLegend5.Size = new System.Drawing.Size(43, 20);
            this.colorEditTreeLegend5.TabIndex = 37;
            this.colorEditTreeLegend5.EditValueChanged += new System.EventHandler(this.colorEditTreeLegend5_EditValueChanged);
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.labelControlTreeLegend5);
            this.groupControl3.Controls.Add(this.labelControlTreeLegend4);
            this.groupControl3.Controls.Add(this.colorEditTreeLegend4);
            this.groupControl3.Controls.Add(this.labelControlTreeLegend3);
            this.groupControl3.Controls.Add(this.colorEditTreeLegend3);
            this.groupControl3.Controls.Add(this.colorEditTreeLegend2);
            this.groupControl3.Controls.Add(this.colorEditTreeLegend1);
            this.groupControl3.Controls.Add(this.labelControlTreeLegend1);
            this.groupControl3.Controls.Add(this.labelControlTreeLegend2);
            this.groupControl3.Controls.Add(this.ComboBoxEditTreeStylingType);
            this.groupControl3.Controls.Add(this.labelControl7);
            this.groupControl3.Location = new System.Drawing.Point(1, 214);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(154, 163);
            this.groupControl3.TabIndex = 6;
            this.groupControl3.Text = "Survey - Tree Styling Key";
            this.groupControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl3_Paint);
            // 
            // labelControlTreeLegend5
            // 
            this.labelControlTreeLegend5.Location = new System.Drawing.Point(51, 142);
            this.labelControlTreeLegend5.Name = "labelControlTreeLegend5";
            this.labelControlTreeLegend5.Size = new System.Drawing.Size(48, 13);
            this.labelControlTreeLegend5.TabIndex = 38;
            this.labelControlTreeLegend5.Text = "- No Work";
            // 
            // labelControlTreeLegend4
            // 
            this.labelControlTreeLegend4.Location = new System.Drawing.Point(51, 119);
            this.labelControlTreeLegend4.Name = "labelControlTreeLegend4";
            this.labelControlTreeLegend4.Size = new System.Drawing.Size(45, 13);
            this.labelControlTreeLegend4.TabIndex = 37;
            this.labelControlTreeLegend4.Text = "- On Hold";
            // 
            // colorEditTreeLegend4
            // 
            this.colorEditTreeLegend4.EditValue = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.colorEditTreeLegend4.Location = new System.Drawing.Point(5, 116);
            this.colorEditTreeLegend4.MenuManager = this.barManager1;
            this.colorEditTreeLegend4.Name = "colorEditTreeLegend4";
            this.colorEditTreeLegend4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditTreeLegend4.Size = new System.Drawing.Size(43, 20);
            this.colorEditTreeLegend4.TabIndex = 36;
            this.colorEditTreeLegend4.EditValueChanged += new System.EventHandler(this.colorEditTreeLegend4_EditValueChanged);
            // 
            // labelControlTreeLegend3
            // 
            this.labelControlTreeLegend3.Location = new System.Drawing.Point(51, 96);
            this.labelControlTreeLegend3.Name = "labelControlTreeLegend3";
            this.labelControlTreeLegend3.Size = new System.Drawing.Size(89, 13);
            this.labelControlTreeLegend3.TabIndex = 35;
            this.labelControlTreeLegend3.Text = "- Not Permissioned";
            // 
            // colorEditTreeLegend3
            // 
            this.colorEditTreeLegend3.EditValue = System.Drawing.Color.Red;
            this.colorEditTreeLegend3.Location = new System.Drawing.Point(5, 93);
            this.colorEditTreeLegend3.MenuManager = this.barManager1;
            this.colorEditTreeLegend3.Name = "colorEditTreeLegend3";
            this.colorEditTreeLegend3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditTreeLegend3.Size = new System.Drawing.Size(43, 20);
            this.colorEditTreeLegend3.TabIndex = 34;
            this.colorEditTreeLegend3.EditValueChanged += new System.EventHandler(this.colorEditTreeLegend3_EditValueChanged);
            // 
            // colorEditTreeLegend2
            // 
            this.colorEditTreeLegend2.EditValue = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.colorEditTreeLegend2.Location = new System.Drawing.Point(5, 70);
            this.colorEditTreeLegend2.MenuManager = this.barManager1;
            this.colorEditTreeLegend2.Name = "colorEditTreeLegend2";
            this.colorEditTreeLegend2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditTreeLegend2.Size = new System.Drawing.Size(43, 20);
            this.colorEditTreeLegend2.TabIndex = 33;
            this.colorEditTreeLegend2.EditValueChanged += new System.EventHandler(this.colorEditTreeLegend2_EditValueChanged);
            // 
            // colorEditTreeLegend1
            // 
            this.colorEditTreeLegend1.EditValue = System.Drawing.Color.Gray;
            this.colorEditTreeLegend1.Location = new System.Drawing.Point(5, 47);
            this.colorEditTreeLegend1.MenuManager = this.barManager1;
            this.colorEditTreeLegend1.Name = "colorEditTreeLegend1";
            this.colorEditTreeLegend1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditTreeLegend1.Size = new System.Drawing.Size(43, 20);
            this.colorEditTreeLegend1.TabIndex = 32;
            this.colorEditTreeLegend1.EditValueChanged += new System.EventHandler(this.colorEditTreeLegend1_EditValueChanged);
            // 
            // labelControlTreeLegend1
            // 
            this.labelControlTreeLegend1.Location = new System.Drawing.Point(51, 50);
            this.labelControlTreeLegend1.Name = "labelControlTreeLegend1";
            this.labelControlTreeLegend1.Size = new System.Drawing.Size(70, 13);
            this.labelControlTreeLegend1.TabIndex = 31;
            this.labelControlTreeLegend1.Text = "- Not Specified";
            // 
            // labelControlTreeLegend2
            // 
            this.labelControlTreeLegend2.Location = new System.Drawing.Point(51, 73);
            this.labelControlTreeLegend2.Name = "labelControlTreeLegend2";
            this.labelControlTreeLegend2.Size = new System.Drawing.Size(69, 13);
            this.labelControlTreeLegend2.TabIndex = 30;
            this.labelControlTreeLegend2.Text = "- Permissioned";
            // 
            // ComboBoxEditTreeStylingType
            // 
            this.ComboBoxEditTreeStylingType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxEditTreeStylingType.EditValue = "Permission Status";
            this.ComboBoxEditTreeStylingType.Location = new System.Drawing.Point(45, 23);
            this.ComboBoxEditTreeStylingType.MenuManager = this.barManager1;
            this.ComboBoxEditTreeStylingType.Name = "ComboBoxEditTreeStylingType";
            this.ComboBoxEditTreeStylingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEditTreeStylingType.Properties.Items.AddRange(new object[] {
            "Permission Status",
            "None"});
            this.ComboBoxEditTreeStylingType.Size = new System.Drawing.Size(105, 20);
            this.ComboBoxEditTreeStylingType.TabIndex = 28;
            this.ComboBoxEditTreeStylingType.SelectedValueChanged += new System.EventHandler(this.ComboBoxEditTreeStylingType_SelectedValueChanged);
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(5, 26);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(36, 13);
            this.labelControl7.TabIndex = 29;
            this.labelControl7.Text = "Theme:";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.ComboBoxEditPoleStylingType);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend7);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend7);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend6);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend6);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend4);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend4);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend5);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend5);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend3);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend3);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend2);
            this.groupControl1.Controls.Add(this.colorEditPoleLegend1);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend1);
            this.groupControl1.Controls.Add(this.labelControlPoleLegend2);
            this.groupControl1.Location = new System.Drawing.Point(1, 1);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(154, 210);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Survey - Pole Styling Key";
            // 
            // ComboBoxEditPoleStylingType
            // 
            this.ComboBoxEditPoleStylingType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxEditPoleStylingType.EditValue = "Survey Status";
            this.ComboBoxEditPoleStylingType.Location = new System.Drawing.Point(45, 23);
            this.ComboBoxEditPoleStylingType.MenuManager = this.barManager1;
            this.ComboBoxEditPoleStylingType.Name = "ComboBoxEditPoleStylingType";
            this.ComboBoxEditPoleStylingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEditPoleStylingType.Properties.Items.AddRange(new object[] {
            "Survey Status",
            "Shutdown",
            "Traffic Management"});
            this.ComboBoxEditPoleStylingType.Size = new System.Drawing.Size(105, 20);
            this.ComboBoxEditPoleStylingType.TabIndex = 6;
            this.ComboBoxEditPoleStylingType.SelectedValueChanged += new System.EventHandler(this.ComboBoxEditPoleStylingType_SelectedValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(5, 26);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(36, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Theme:";
            // 
            // labelControlPoleLegend7
            // 
            this.labelControlPoleLegend7.Location = new System.Drawing.Point(51, 190);
            this.labelControlPoleLegend7.Name = "labelControlPoleLegend7";
            this.labelControlPoleLegend7.Size = new System.Drawing.Size(86, 13);
            this.labelControlPoleLegend7.TabIndex = 18;
            this.labelControlPoleLegend7.Text = "- Work Completed";
            // 
            // colorEditPoleLegend7
            // 
            this.colorEditPoleLegend7.EditValue = System.Drawing.Color.DarkOrchid;
            this.colorEditPoleLegend7.Location = new System.Drawing.Point(5, 186);
            this.colorEditPoleLegend7.MenuManager = this.barManager1;
            this.colorEditPoleLegend7.Name = "colorEditPoleLegend7";
            this.colorEditPoleLegend7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend7.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend7.TabIndex = 17;
            this.colorEditPoleLegend7.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend7_EditValueChanged);
            // 
            // labelControlPoleLegend6
            // 
            this.labelControlPoleLegend6.Location = new System.Drawing.Point(51, 167);
            this.labelControlPoleLegend6.Name = "labelControlPoleLegend6";
            this.labelControlPoleLegend6.Size = new System.Drawing.Size(69, 13);
            this.labelControlPoleLegend6.TabIndex = 16;
            this.labelControlPoleLegend6.Text = "- Permissioned";
            // 
            // colorEditPoleLegend6
            // 
            this.colorEditPoleLegend6.EditValue = System.Drawing.Color.LimeGreen;
            this.colorEditPoleLegend6.Location = new System.Drawing.Point(5, 163);
            this.colorEditPoleLegend6.MenuManager = this.barManager1;
            this.colorEditPoleLegend6.Name = "colorEditPoleLegend6";
            this.colorEditPoleLegend6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend6.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend6.TabIndex = 15;
            this.colorEditPoleLegend6.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend6_EditValueChanged);
            // 
            // labelControlPoleLegend4
            // 
            this.labelControlPoleLegend4.Location = new System.Drawing.Point(51, 120);
            this.labelControlPoleLegend4.Name = "labelControlPoleLegend4";
            this.labelControlPoleLegend4.Size = new System.Drawing.Size(95, 13);
            this.labelControlPoleLegend4.TabIndex = 14;
            this.labelControlPoleLegend4.Text = "- Survey Completed";
            // 
            // colorEditPoleLegend4
            // 
            this.colorEditPoleLegend4.EditValue = System.Drawing.Color.DodgerBlue;
            this.colorEditPoleLegend4.Location = new System.Drawing.Point(5, 117);
            this.colorEditPoleLegend4.MenuManager = this.barManager1;
            this.colorEditPoleLegend4.Name = "colorEditPoleLegend4";
            this.colorEditPoleLegend4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend4.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend4.TabIndex = 13;
            this.colorEditPoleLegend4.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend4_EditValueChanged);
            // 
            // labelControlPoleLegend5
            // 
            this.labelControlPoleLegend5.Location = new System.Drawing.Point(51, 143);
            this.labelControlPoleLegend5.Name = "labelControlPoleLegend5";
            this.labelControlPoleLegend5.Size = new System.Drawing.Size(45, 13);
            this.labelControlPoleLegend5.TabIndex = 12;
            this.labelControlPoleLegend5.Text = "- On Hold";
            // 
            // colorEditPoleLegend5
            // 
            this.colorEditPoleLegend5.EditValue = System.Drawing.Color.Red;
            this.colorEditPoleLegend5.Location = new System.Drawing.Point(5, 140);
            this.colorEditPoleLegend5.MenuManager = this.barManager1;
            this.colorEditPoleLegend5.Name = "colorEditPoleLegend5";
            this.colorEditPoleLegend5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend5.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend5.TabIndex = 11;
            this.colorEditPoleLegend5.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend5_EditValueChanged);
            // 
            // labelControlPoleLegend3
            // 
            this.labelControlPoleLegend3.Location = new System.Drawing.Point(51, 97);
            this.labelControlPoleLegend3.Name = "labelControlPoleLegend3";
            this.labelControlPoleLegend3.Size = new System.Drawing.Size(43, 13);
            this.labelControlPoleLegend3.TabIndex = 10;
            this.labelControlPoleLegend3.Text = "- Started";
            // 
            // colorEditPoleLegend3
            // 
            this.colorEditPoleLegend3.EditValue = System.Drawing.Color.Yellow;
            this.colorEditPoleLegend3.Location = new System.Drawing.Point(5, 94);
            this.colorEditPoleLegend3.MenuManager = this.barManager1;
            this.colorEditPoleLegend3.Name = "colorEditPoleLegend3";
            this.colorEditPoleLegend3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend3.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend3.TabIndex = 9;
            this.colorEditPoleLegend3.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend3_EditValueChanged);
            // 
            // colorEditPoleLegend2
            // 
            this.colorEditPoleLegend2.EditValue = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.colorEditPoleLegend2.Location = new System.Drawing.Point(5, 71);
            this.colorEditPoleLegend2.MenuManager = this.barManager1;
            this.colorEditPoleLegend2.Name = "colorEditPoleLegend2";
            this.colorEditPoleLegend2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend2.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend2.TabIndex = 8;
            this.colorEditPoleLegend2.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend2_EditValueChanged);
            // 
            // colorEditPoleLegend1
            // 
            this.colorEditPoleLegend1.EditValue = System.Drawing.Color.Gray;
            this.colorEditPoleLegend1.Location = new System.Drawing.Point(5, 48);
            this.colorEditPoleLegend1.MenuManager = this.barManager1;
            this.colorEditPoleLegend1.Name = "colorEditPoleLegend1";
            this.colorEditPoleLegend1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEditPoleLegend1.Size = new System.Drawing.Size(43, 20);
            this.colorEditPoleLegend1.TabIndex = 7;
            this.colorEditPoleLegend1.EditValueChanged += new System.EventHandler(this.colorEditPoleLegend1_EditValueChanged);
            // 
            // labelControlPoleLegend1
            // 
            this.labelControlPoleLegend1.Location = new System.Drawing.Point(51, 51);
            this.labelControlPoleLegend1.Name = "labelControlPoleLegend1";
            this.labelControlPoleLegend1.Size = new System.Drawing.Size(103, 13);
            this.labelControlPoleLegend1.TabIndex = 3;
            this.labelControlPoleLegend1.Text = "- No Survey Required";
            // 
            // labelControlPoleLegend2
            // 
            this.labelControlPoleLegend2.Location = new System.Drawing.Point(51, 74);
            this.labelControlPoleLegend2.Name = "labelControlPoleLegend2";
            this.labelControlPoleLegend2.Size = new System.Drawing.Size(73, 13);
            this.labelControlPoleLegend2.TabIndex = 2;
            this.labelControlPoleLegend2.Text = "- To Be Started";
            // 
            // btnSurveyRefreshPoles
            // 
            this.btnSurveyRefreshPoles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSurveyRefreshPoles.Appearance.Options.UseTextOptions = true;
            this.btnSurveyRefreshPoles.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnSurveyRefreshPoles.Image = ((System.Drawing.Image)(resources.GetObject("btnSurveyRefreshPoles.Image")));
            this.btnSurveyRefreshPoles.Location = new System.Drawing.Point(1, 380);
            this.btnSurveyRefreshPoles.Name = "btnSurveyRefreshPoles";
            this.btnSurveyRefreshPoles.Size = new System.Drawing.Size(154, 23);
            this.btnSurveyRefreshPoles.TabIndex = 4;
            this.btnSurveyRefreshPoles.Text = "Refresh Map";
            this.btnSurveyRefreshPoles.ToolTip = "Refresh Loaded Map Objects";
            this.btnSurveyRefreshPoles.Click += new System.EventHandler(this.btnSurveyRefreshPoles_Click);
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this.dockPanelLayers;
            this.panelContainer1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panelContainer1.Appearance.Options.UseBackColor = true;
            this.panelContainer1.Controls.Add(this.dockPanelLayers);
            this.panelContainer1.Controls.Add(this.dockPanelMapObjects);
            this.panelContainer1.Controls.Add(this.dockPanelGPS);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.ID = new System.Guid("7fafffa1-da04-4520-9230-b81e0dad9de0");
            this.panelContainer1.Location = new System.Drawing.Point(0, 42);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(340, 200);
            this.panelContainer1.Size = new System.Drawing.Size(340, 678);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.Text = "Map Control";
            // 
            // dockPanelLayers
            // 
            this.dockPanelLayers.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelLayers.Appearance.Options.UseBackColor = true;
            this.dockPanelLayers.Controls.Add(this.dockPanel4_Container);
            this.dockPanelLayers.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelLayers.ID = new System.Guid("c69e68c1-c555-445c-a79b-a9cdadc530a6");
            this.dockPanelLayers.Location = new System.Drawing.Point(3, 29);
            this.dockPanelLayers.Name = "dockPanelLayers";
            this.dockPanelLayers.Options.ShowCloseButton = false;
            this.dockPanelLayers.OriginalSize = new System.Drawing.Size(334, 625);
            this.dockPanelLayers.Size = new System.Drawing.Size(334, 625);
            this.dockPanelLayers.Text = "Layer Manager";
            // 
            // dockPanel4_Container
            // 
            this.dockPanel4_Container.Controls.Add(this.layoutControl6);
            this.dockPanel4_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel4_Container.Name = "dockPanel4_Container";
            this.dockPanel4_Container.Size = new System.Drawing.Size(334, 625);
            this.dockPanel4_Container.TabIndex = 0;
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.gridSplitContainer3);
            this.layoutControl6.Controls.Add(this.beWorkspace);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(0, 0);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.Root = this.layoutControlGroup14;
            this.layoutControl6.Size = new System.Drawing.Size(334, 625);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Grid = this.gridControl5;
            this.gridSplitContainer3.Location = new System.Drawing.Point(2, 28);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl5);
            this.gridSplitContainer3.Size = new System.Drawing.Size(330, 595);
            this.gridSplitContainer3.TabIndex = 5;
            // 
            // gridControl5
            // 
            this.gridControl5.AllowDrop = true;
            this.gridControl5.DataSource = this.sp01288ATTreePickerWorkspacelayerslistBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Move Layer Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 16, true, true, "Move Layer Down", "down")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCE_LayerHitable,
            this.repositoryItemCE_LayerVisible});
            this.gridControl5.Size = new System.Drawing.Size(330, 595);
            this.gridControl5.TabIndex = 5;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            this.gridControl5.DragDrop += new System.Windows.Forms.DragEventHandler(this.gridControl5_DragDrop);
            this.gridControl5.DragOver += new System.Windows.Forms.DragEventHandler(this.gridControl5_DragOver);
            this.gridControl5.DragLeave += new System.EventHandler(this.gridControl5_DragLeave);
            this.gridControl5.Paint += new System.Windows.Forms.PaintEventHandler(this.gridControl5_Paint);
            // 
            // sp01288ATTreePickerWorkspacelayerslistBindingSource
            // 
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource.DataMember = "sp01288_AT_Tree_Picker_Workspace_layers_list";
            this.sp01288ATTreePickerWorkspacelayerslistBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLayerID,
            this.colWorkspaceID,
            this.colLayerType,
            this.colLayerTypeDescription,
            this.colLayerPath,
            this.colLayerOrder,
            this.colLayerVisible,
            this.colLayerHitable,
            this.colPreserveDefaultStyling,
            this.colTransparency,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colUseThematicStyling,
            this.colThematicStyleSet,
            this.colPointSize,
            this.colLayerName1,
            this.colDummyLayerPathForSave});
            this.gridView5.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 518, 208, 191);
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsCustomization.AllowFilter = false;
            this.gridView5.OptionsCustomization.AllowGroup = false;
            this.gridView5.OptionsCustomization.AllowSort = false;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLayerOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.ViewCaption = "Loaded Layers";
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView5_FocusedRowChanged);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseDown);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.MouseMove += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseMove);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colLayerID
            // 
            this.colLayerID.Caption = "Layer ID";
            this.colLayerID.FieldName = "LayerID";
            this.colLayerID.Name = "colLayerID";
            this.colLayerID.OptionsColumn.AllowEdit = false;
            this.colLayerID.OptionsColumn.AllowFocus = false;
            this.colLayerID.OptionsColumn.ReadOnly = true;
            this.colLayerID.Width = 58;
            // 
            // colWorkspaceID
            // 
            this.colWorkspaceID.Caption = "Workspace ID";
            this.colWorkspaceID.FieldName = "WorkspaceID";
            this.colWorkspaceID.Name = "colWorkspaceID";
            this.colWorkspaceID.OptionsColumn.AllowEdit = false;
            this.colWorkspaceID.OptionsColumn.AllowFocus = false;
            this.colWorkspaceID.OptionsColumn.ReadOnly = true;
            this.colWorkspaceID.Width = 78;
            // 
            // colLayerType
            // 
            this.colLayerType.Caption = "Layer Type ID";
            this.colLayerType.FieldName = "LayerType";
            this.colLayerType.Name = "colLayerType";
            this.colLayerType.OptionsColumn.AllowEdit = false;
            this.colLayerType.OptionsColumn.AllowFocus = false;
            this.colLayerType.OptionsColumn.ReadOnly = true;
            this.colLayerType.Width = 79;
            // 
            // colLayerTypeDescription
            // 
            this.colLayerTypeDescription.Caption = "Layer Type";
            this.colLayerTypeDescription.FieldName = "LayerTypeDescription";
            this.colLayerTypeDescription.Name = "colLayerTypeDescription";
            this.colLayerTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLayerTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLayerTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLayerTypeDescription.Visible = true;
            this.colLayerTypeDescription.VisibleIndex = 1;
            // 
            // colLayerPath
            // 
            this.colLayerPath.Caption = "Layer Path";
            this.colLayerPath.FieldName = "LayerPath";
            this.colLayerPath.Name = "colLayerPath";
            this.colLayerPath.OptionsColumn.AllowEdit = false;
            this.colLayerPath.OptionsColumn.AllowFocus = false;
            this.colLayerPath.OptionsColumn.ReadOnly = true;
            this.colLayerPath.Width = 73;
            // 
            // colLayerOrder
            // 
            this.colLayerOrder.Caption = "Order";
            this.colLayerOrder.FieldName = "LayerOrder";
            this.colLayerOrder.Name = "colLayerOrder";
            this.colLayerOrder.OptionsColumn.AllowEdit = false;
            this.colLayerOrder.OptionsColumn.AllowFocus = false;
            this.colLayerOrder.OptionsColumn.ReadOnly = true;
            this.colLayerOrder.Width = 62;
            // 
            // colLayerVisible
            // 
            this.colLayerVisible.Caption = "Visible";
            this.colLayerVisible.ColumnEdit = this.repositoryItemCE_LayerVisible;
            this.colLayerVisible.FieldName = "LayerVisible";
            this.colLayerVisible.Name = "colLayerVisible";
            this.colLayerVisible.Visible = true;
            this.colLayerVisible.VisibleIndex = 2;
            this.colLayerVisible.Width = 50;
            // 
            // repositoryItemCE_LayerVisible
            // 
            this.repositoryItemCE_LayerVisible.AutoHeight = false;
            this.repositoryItemCE_LayerVisible.Caption = "Check";
            this.repositoryItemCE_LayerVisible.Name = "repositoryItemCE_LayerVisible";
            this.repositoryItemCE_LayerVisible.ValueChecked = 1;
            this.repositoryItemCE_LayerVisible.ValueUnchecked = 0;
            this.repositoryItemCE_LayerVisible.CheckedChanged += new System.EventHandler(this.repositoryItemCE_LayerVisible_CheckedChanged);
            // 
            // colLayerHitable
            // 
            this.colLayerHitable.Caption = "Hitable";
            this.colLayerHitable.ColumnEdit = this.repositoryItemCE_LayerHitable;
            this.colLayerHitable.FieldName = "LayerHitable";
            this.colLayerHitable.Name = "colLayerHitable";
            this.colLayerHitable.Visible = true;
            this.colLayerHitable.VisibleIndex = 3;
            this.colLayerHitable.Width = 54;
            // 
            // repositoryItemCE_LayerHitable
            // 
            this.repositoryItemCE_LayerHitable.AutoHeight = false;
            this.repositoryItemCE_LayerHitable.Caption = "Check";
            this.repositoryItemCE_LayerHitable.Name = "repositoryItemCE_LayerHitable";
            this.repositoryItemCE_LayerHitable.ValueChecked = 1;
            this.repositoryItemCE_LayerHitable.ValueUnchecked = 0;
            this.repositoryItemCE_LayerHitable.CheckedChanged += new System.EventHandler(this.repositoryItemCE_LayerHitable_CheckedChanged);
            // 
            // colPreserveDefaultStyling
            // 
            this.colPreserveDefaultStyling.Caption = "Preserve Default Styling";
            this.colPreserveDefaultStyling.FieldName = "PreserveDefaultStyling";
            this.colPreserveDefaultStyling.Name = "colPreserveDefaultStyling";
            this.colPreserveDefaultStyling.OptionsColumn.AllowEdit = false;
            this.colPreserveDefaultStyling.OptionsColumn.AllowFocus = false;
            this.colPreserveDefaultStyling.OptionsColumn.ReadOnly = true;
            this.colPreserveDefaultStyling.Width = 137;
            // 
            // colTransparency
            // 
            this.colTransparency.Caption = "Transparency";
            this.colTransparency.FieldName = "Transparency";
            this.colTransparency.Name = "colTransparency";
            this.colTransparency.OptionsColumn.AllowEdit = false;
            this.colTransparency.OptionsColumn.AllowFocus = false;
            this.colTransparency.OptionsColumn.ReadOnly = true;
            this.colTransparency.Width = 87;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Line Colour";
            this.gridColumn2.FieldName = "LineColour";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 74;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Line Width";
            this.gridColumn3.FieldName = "LineWidth";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 71;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Grey Scale";
            this.gridColumn4.FieldName = "GreyScale";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 72;
            // 
            // colUseThematicStyling
            // 
            this.colUseThematicStyling.Caption = "Use Thematic Styling";
            this.colUseThematicStyling.FieldName = "UseThematicStyling";
            this.colUseThematicStyling.Name = "colUseThematicStyling";
            this.colUseThematicStyling.OptionsColumn.AllowEdit = false;
            this.colUseThematicStyling.OptionsColumn.AllowFocus = false;
            this.colUseThematicStyling.OptionsColumn.ReadOnly = true;
            this.colUseThematicStyling.Width = 120;
            // 
            // colThematicStyleSet
            // 
            this.colThematicStyleSet.Caption = "Thematic Style Set";
            this.colThematicStyleSet.FieldName = "ThematicStyleSet";
            this.colThematicStyleSet.Name = "colThematicStyleSet";
            this.colThematicStyleSet.OptionsColumn.AllowEdit = false;
            this.colThematicStyleSet.OptionsColumn.AllowFocus = false;
            this.colThematicStyleSet.OptionsColumn.ReadOnly = true;
            this.colThematicStyleSet.Width = 110;
            // 
            // colPointSize
            // 
            this.colPointSize.Caption = "Point Size";
            this.colPointSize.FieldName = "PointSize";
            this.colPointSize.Name = "colPointSize";
            this.colPointSize.OptionsColumn.AllowEdit = false;
            this.colPointSize.OptionsColumn.AllowFocus = false;
            this.colPointSize.OptionsColumn.ReadOnly = true;
            this.colPointSize.Width = 67;
            // 
            // colLayerName1
            // 
            this.colLayerName1.Caption = "Layer Name";
            this.colLayerName1.FieldName = "LayerName";
            this.colLayerName1.Name = "colLayerName1";
            this.colLayerName1.OptionsColumn.AllowEdit = false;
            this.colLayerName1.OptionsColumn.AllowFocus = false;
            this.colLayerName1.OptionsColumn.ReadOnly = true;
            this.colLayerName1.Visible = true;
            this.colLayerName1.VisibleIndex = 0;
            this.colLayerName1.Width = 176;
            // 
            // colDummyLayerPathForSave
            // 
            this.colDummyLayerPathForSave.Caption = "Save Layer Path";
            this.colDummyLayerPathForSave.FieldName = "DummyLayerPathForSave";
            this.colDummyLayerPathForSave.Name = "colDummyLayerPathForSave";
            // 
            // beWorkspace
            // 
            this.beWorkspace.Location = new System.Drawing.Point(62, 2);
            this.beWorkspace.MenuManager = this.barManager1;
            this.beWorkspace.Name = "beWorkspace";
            superToolTip32.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem32.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image30")));
            toolTipTitleItem32.Appearance.Options.UseImage = true;
            toolTipTitleItem32.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem32.Image")));
            toolTipTitleItem32.Text = "Select - Information";
            toolTipItem32.LeftIndent = 6;
            toolTipItem32.Text = "Click me to open the <b>Mapping Workspace Selection</b> screen.\r\n\r\n<b><color=gree" +
    "n>Tip:</color></b> This screen can be used to select, create, edit and delete wo" +
    "rkspaces.";
            superToolTip32.Items.Add(toolTipTitleItem32);
            superToolTip32.Items.Add(toolTipItem32);
            toolTipTitleItem33.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image31")));
            toolTipTitleItem33.Appearance.Options.UseImage = true;
            toolTipTitleItem33.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem33.Image")));
            toolTipTitleItem33.Text = "Save Workspace Changes - Information";
            toolTipItem33.LeftIndent = 6;
            toolTipItem33.Text = "Click me to save changes to the current workspace.";
            superToolTip33.Items.Add(toolTipTitleItem33);
            superToolTip33.Items.Add(toolTipItem33);
            superToolTip34.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem34.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image32")));
            toolTipTitleItem34.Appearance.Options.UseImage = true;
            toolTipTitleItem34.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem34.Image")));
            toolTipTitleItem34.Text = "Save Workspace As - Information";
            toolTipItem34.LeftIndent = 6;
            toolTipItem34.Text = "Click me to save a copy of the current workspace as a new workspace.\r\n\r\n<b><color" +
    "=green>Tip:</color></b> You can then make changes to the copy without effecting " +
    "the original workspace.";
            superToolTip34.Items.Add(toolTipTitleItem34);
            superToolTip34.Items.Add(toolTipItem34);
            this.beWorkspace.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "Select", superToolTip32, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Save", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("beWorkspace.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "Save", superToolTip33, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Save AS", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("beWorkspace.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "", "SaveAs", superToolTip34, true)});
            this.beWorkspace.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.beWorkspace.Size = new System.Drawing.Size(270, 22);
            this.beWorkspace.StyleController = this.layoutControl6;
            this.beWorkspace.TabIndex = 4;
            this.beWorkspace.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.beWorkspace_ButtonClick);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Root";
            this.layoutControlGroup14.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup14.GroupBordersVisible = false;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem52,
            this.layoutControlItem53});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup14.Name = "Root";
            this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Size = new System.Drawing.Size(334, 625);
            this.layoutControlGroup14.TextVisible = false;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.beWorkspace;
            this.layoutControlItem52.CustomizationFormText = "Workspace:";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(334, 26);
            this.layoutControlItem52.Text = "Workspace:";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.gridSplitContainer3;
            this.layoutControlItem53.CustomizationFormText = "Layer Grid:";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(334, 599);
            this.layoutControlItem53.Text = "Layer Grid:";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextVisible = false;
            // 
            // dockPanelMapObjects
            // 
            this.dockPanelMapObjects.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelMapObjects.Appearance.Options.UseBackColor = true;
            this.dockPanelMapObjects.Controls.Add(this.dockPanel2_Container);
            this.dockPanelMapObjects.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelMapObjects.ID = new System.Guid("113275df-84df-4518-a683-8d7c0d90732f");
            this.dockPanelMapObjects.Location = new System.Drawing.Point(3, 29);
            this.dockPanelMapObjects.Name = "dockPanelMapObjects";
            this.dockPanelMapObjects.Options.ShowCloseButton = false;
            this.dockPanelMapObjects.OriginalSize = new System.Drawing.Size(334, 625);
            this.dockPanelMapObjects.Size = new System.Drawing.Size(334, 625);
            this.dockPanelMapObjects.Text = "Map Objects";
            this.dockPanelMapObjects.Click += new System.EventHandler(this.dockPanel2_Click);
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Controls.Add(this.layoutControl1);
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(334, 625);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnScaleToViewedObjects);
            this.layoutControl1.Controls.Add(this.btnRefreshTrees);
            this.layoutControl1.Controls.Add(this.gridSplitContainer1);
            this.layoutControl1.Controls.Add(this.btnClearMapObjects);
            this.layoutControl1.Controls.Add(this.btnSelectHighlighted);
            this.layoutControl1.Controls.Add(this.btnRefreshMapObjects);
            this.layoutControl1.Controls.Add(this.csScaleMapForHighlighted);
            this.layoutControl1.Controls.Add(this.ceCentreMap);
            this.layoutControl1.Controls.Add(this.gridSplitContainer2);
            this.layoutControl1.Controls.Add(this.colorEditHighlight);
            this.layoutControl1.Controls.Add(this.buttonEditFilterCircuits);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(828, 219, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(334, 625);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnScaleToViewedObjects
            // 
            this.btnScaleToViewedObjects.Image = ((System.Drawing.Image)(resources.GetObject("btnScaleToViewedObjects.Image")));
            this.btnScaleToViewedObjects.Location = new System.Drawing.Point(259, 491);
            this.btnScaleToViewedObjects.Name = "btnScaleToViewedObjects";
            this.btnScaleToViewedObjects.Size = new System.Drawing.Size(53, 22);
            this.btnScaleToViewedObjects.StyleController = this.layoutControl1;
            toolTipTitleItem35.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image33")));
            toolTipTitleItem35.Appearance.Options.UseImage = true;
            toolTipTitleItem35.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem35.Image")));
            toolTipTitleItem35.Text = "Scale to Viewed Objects - Information";
            toolTipItem35.LeftIndent = 6;
            toolTipItem35.Text = "Click me to scale the map to the Viewed Map Objects. The map will be drawn at a s" +
    "cale where all viewed map objects can be fitted on the screen.";
            superToolTip35.Items.Add(toolTipTitleItem35);
            superToolTip35.Items.Add(toolTipItem35);
            this.btnScaleToViewedObjects.SuperTip = superToolTip35;
            this.btnScaleToViewedObjects.TabIndex = 19;
            this.btnScaleToViewedObjects.Text = "Scale";
            this.btnScaleToViewedObjects.Click += new System.EventHandler(this.btnScaleToViewedObjects_Click);
            // 
            // btnRefreshTrees
            // 
            this.btnRefreshTrees.Location = new System.Drawing.Point(238, 31);
            this.btnRefreshTrees.Name = "btnRefreshTrees";
            this.btnRefreshTrees.Size = new System.Drawing.Size(86, 22);
            this.btnRefreshTrees.StyleController = this.layoutControl1;
            this.btnRefreshTrees.TabIndex = 18;
            this.btnRefreshTrees.Text = "Refresh Trees";
            this.btnRefreshTrees.Click += new System.EventHandler(this.btnRefreshTrees_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl6;
            this.gridSplitContainer1.Location = new System.Drawing.Point(10, 57);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer1.Size = new System.Drawing.Size(314, 422);
            this.gridSplitContainer1.TabIndex = 16;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp07053UTTreePickerTreesBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridViewTrees;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemCheckEdit2});
            this.gridControl6.Size = new System.Drawing.Size(314, 422);
            this.gridControl6.TabIndex = 13;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTrees});
            // 
            // sp07053UTTreePickerTreesBindingSource
            // 
            this.sp07053UTTreePickerTreesBindingSource.DataMember = "sp07053_UT_Tree_Picker_Trees";
            this.sp07053UTTreePickerTreesBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // gridViewTrees
            // 
            this.gridViewTrees.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTreeID,
            this.colPoleID1,
            this.colTypeID,
            this.colTreeType,
            this.colTreeReferenceNumber,
            this.colStatusID,
            this.colSizeBandID,
            this.colSizeBand,
            this.colX1,
            this.colY1,
            this.colXYPairs,
            this.colLatitude1,
            this.colLongitude1,
            this.colLatLongPairs,
            this.colArea,
            this.colLength,
            this.colRemarks1,
            this.colMapID1,
            this.colStatus1,
            this.colPoleNumber1,
            this.colLastInspectionDate1,
            this.colLastInspectionElapsedDays1,
            this.colNextInspectionDate1,
            this.colCircuitName1,
            this.colCircuitNumber1,
            this.colClientName,
            this.colRegionName1,
            this.colSubAreaName1,
            this.colObjectType1,
            this.colHighlight1,
            this.colThematicValue,
            this.colTooltip1});
            this.gridViewTrees.GridControl = this.gridControl6;
            this.gridViewTrees.GroupCount = 2;
            this.gridViewTrees.Name = "gridViewTrees";
            this.gridViewTrees.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewTrees.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewTrees.OptionsLayout.StoreAppearance = true;
            this.gridViewTrees.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewTrees.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewTrees.OptionsSelection.MultiSelect = true;
            this.gridViewTrees.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewTrees.OptionsView.ColumnAutoWidth = false;
            this.gridViewTrees.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewTrees.OptionsView.ShowGroupPanel = false;
            this.gridViewTrees.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewTrees.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewTrees.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewTrees.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewTrees.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewTrees.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewTrees.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAssetObjects_MouseUp);
            this.gridViewTrees.GotFocus += new System.EventHandler(this.gridViewAssetObjects_GotFocus);
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            this.colTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleID1
            // 
            this.colPoleID1.Caption = "Pole ID";
            this.colPoleID1.FieldName = "PoleID";
            this.colPoleID1.Name = "colPoleID1";
            this.colPoleID1.OptionsColumn.AllowEdit = false;
            this.colPoleID1.OptionsColumn.AllowFocus = false;
            this.colPoleID1.OptionsColumn.ReadOnly = true;
            this.colPoleID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Tree Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTypeID.Width = 84;
            // 
            // colTreeType
            // 
            this.colTreeType.Caption = "Tree Type";
            this.colTreeType.FieldName = "TreeType";
            this.colTreeType.Name = "colTreeType";
            this.colTreeType.OptionsColumn.AllowEdit = false;
            this.colTreeType.OptionsColumn.AllowFocus = false;
            this.colTreeType.OptionsColumn.ReadOnly = true;
            this.colTreeType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeType.Visible = true;
            this.colTreeType.VisibleIndex = 4;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Reference Number";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 1;
            this.colTreeReferenceNumber.Width = 130;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSizeBandID
            // 
            this.colSizeBandID.Caption = "Size Band ID";
            this.colSizeBandID.FieldName = "SizeBandID";
            this.colSizeBandID.Name = "colSizeBandID";
            this.colSizeBandID.OptionsColumn.AllowEdit = false;
            this.colSizeBandID.OptionsColumn.AllowFocus = false;
            this.colSizeBandID.OptionsColumn.ReadOnly = true;
            this.colSizeBandID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSizeBandID.Width = 81;
            // 
            // colSizeBand
            // 
            this.colSizeBand.Caption = "Size Band";
            this.colSizeBand.FieldName = "SizeBand";
            this.colSizeBand.Name = "colSizeBand";
            this.colSizeBand.OptionsColumn.AllowEdit = false;
            this.colSizeBand.OptionsColumn.AllowFocus = false;
            this.colSizeBand.OptionsColumn.ReadOnly = true;
            this.colSizeBand.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSizeBand.Visible = true;
            this.colSizeBand.VisibleIndex = 6;
            // 
            // colX1
            // 
            this.colX1.Caption = "X";
            this.colX1.FieldName = "X";
            this.colX1.Name = "colX1";
            this.colX1.OptionsColumn.AllowEdit = false;
            this.colX1.OptionsColumn.AllowFocus = false;
            this.colX1.OptionsColumn.ReadOnly = true;
            this.colX1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colY1
            // 
            this.colY1.Caption = "Y";
            this.colY1.FieldName = "Y";
            this.colY1.Name = "colY1";
            this.colY1.OptionsColumn.AllowEdit = false;
            this.colY1.OptionsColumn.AllowFocus = false;
            this.colY1.OptionsColumn.ReadOnly = true;
            this.colY1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colXYPairs
            // 
            this.colXYPairs.Caption = "XY Pairs";
            this.colXYPairs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colXYPairs.FieldName = "XYPairs";
            this.colXYPairs.Name = "colXYPairs";
            this.colXYPairs.OptionsColumn.ReadOnly = true;
            this.colXYPairs.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLatitude1
            // 
            this.colLatitude1.Caption = "Latitude";
            this.colLatitude1.FieldName = "Latitude";
            this.colLatitude1.Name = "colLatitude1";
            this.colLatitude1.OptionsColumn.AllowEdit = false;
            this.colLatitude1.OptionsColumn.AllowFocus = false;
            this.colLatitude1.OptionsColumn.ReadOnly = true;
            this.colLatitude1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLongitude1
            // 
            this.colLongitude1.Caption = "Longitude";
            this.colLongitude1.FieldName = "Longitude";
            this.colLongitude1.Name = "colLongitude1";
            this.colLongitude1.OptionsColumn.AllowEdit = false;
            this.colLongitude1.OptionsColumn.AllowFocus = false;
            this.colLongitude1.OptionsColumn.ReadOnly = true;
            this.colLongitude1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLatLongPairs
            // 
            this.colLatLongPairs.Caption = "Lat\\Long Pairs";
            this.colLatLongPairs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colLatLongPairs.FieldName = "LatLongPairs";
            this.colLatLongPairs.Name = "colLatLongPairs";
            this.colLatLongPairs.OptionsColumn.ReadOnly = true;
            this.colLatLongPairs.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatLongPairs.Width = 89;
            // 
            // colArea
            // 
            this.colArea.Caption = "Area";
            this.colArea.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colArea.FieldName = "Area";
            this.colArea.Name = "colArea";
            this.colArea.OptionsColumn.AllowEdit = false;
            this.colArea.OptionsColumn.AllowFocus = false;
            this.colArea.OptionsColumn.ReadOnly = true;
            this.colArea.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colArea.Visible = true;
            this.colArea.VisibleIndex = 2;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colLength
            // 
            this.colLength.Caption = "Length";
            this.colLength.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colLength.FieldName = "Length";
            this.colLength.Name = "colLength";
            this.colLength.OptionsColumn.AllowEdit = false;
            this.colLength.OptionsColumn.AllowFocus = false;
            this.colLength.OptionsColumn.ReadOnly = true;
            this.colLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLength.Visible = true;
            this.colLength.VisibleIndex = 3;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 10;
            // 
            // colMapID1
            // 
            this.colMapID1.Caption = "Map ID";
            this.colMapID1.FieldName = "MapID";
            this.colMapID1.Name = "colMapID1";
            this.colMapID1.OptionsColumn.AllowEdit = false;
            this.colMapID1.OptionsColumn.AllowFocus = false;
            this.colMapID1.OptionsColumn.ReadOnly = true;
            this.colMapID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 5;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber1.Width = 81;
            // 
            // colLastInspectionDate1
            // 
            this.colLastInspectionDate1.Caption = "Last Inspection";
            this.colLastInspectionDate1.ColumnEdit = this.repositoryItemDateEdit1;
            this.colLastInspectionDate1.FieldName = "LastInspectionDate";
            this.colLastInspectionDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate1.Name = "colLastInspectionDate1";
            this.colLastInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate1.Visible = true;
            this.colLastInspectionDate1.VisibleIndex = 7;
            this.colLastInspectionDate1.Width = 94;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colLastInspectionElapsedDays1
            // 
            this.colLastInspectionElapsedDays1.Caption = "Days Since Last Inspection";
            this.colLastInspectionElapsedDays1.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays1.Name = "colLastInspectionElapsedDays1";
            this.colLastInspectionElapsedDays1.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays1.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays1.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionElapsedDays1.Visible = true;
            this.colLastInspectionElapsedDays1.VisibleIndex = 8;
            this.colLastInspectionElapsedDays1.Width = 149;
            // 
            // colNextInspectionDate1
            // 
            this.colNextInspectionDate1.Caption = "Next Inspection";
            this.colNextInspectionDate1.ColumnEdit = this.repositoryItemDateEdit1;
            this.colNextInspectionDate1.FieldName = "NextInspectionDate";
            this.colNextInspectionDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate1.Name = "colNextInspectionDate1";
            this.colNextInspectionDate1.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate1.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate1.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate1.Visible = true;
            this.colNextInspectionDate1.VisibleIndex = 9;
            this.colNextInspectionDate1.Width = 97;
            // 
            // colCircuitName1
            // 
            this.colCircuitName1.Caption = "Circuit Name";
            this.colCircuitName1.FieldName = "CircuitName";
            this.colCircuitName1.Name = "colCircuitName1";
            this.colCircuitName1.OptionsColumn.AllowEdit = false;
            this.colCircuitName1.OptionsColumn.AllowFocus = false;
            this.colCircuitName1.OptionsColumn.ReadOnly = true;
            this.colCircuitName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName1.Width = 81;
            // 
            // colCircuitNumber1
            // 
            this.colCircuitNumber1.Caption = "Circuit Number";
            this.colCircuitNumber1.FieldName = "CircuitNumber";
            this.colCircuitNumber1.Name = "colCircuitNumber1";
            this.colCircuitNumber1.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber1.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber1.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber1.Width = 91;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 11;
            this.colClientName.Width = 110;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 12;
            this.colRegionName1.Width = 109;
            // 
            // colSubAreaName1
            // 
            this.colSubAreaName1.Caption = "Sub-Area";
            this.colSubAreaName1.FieldName = "SubAreaName";
            this.colSubAreaName1.Name = "colSubAreaName1";
            this.colSubAreaName1.OptionsColumn.AllowEdit = false;
            this.colSubAreaName1.OptionsColumn.AllowFocus = false;
            this.colSubAreaName1.OptionsColumn.ReadOnly = true;
            this.colSubAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName1.Visible = true;
            this.colSubAreaName1.VisibleIndex = 13;
            this.colSubAreaName1.Width = 110;
            // 
            // colObjectType1
            // 
            this.colObjectType1.Caption = "Object Type";
            this.colObjectType1.FieldName = "ObjectType";
            this.colObjectType1.Name = "colObjectType1";
            this.colObjectType1.OptionsColumn.AllowEdit = false;
            this.colObjectType1.OptionsColumn.AllowFocus = false;
            this.colObjectType1.OptionsColumn.ReadOnly = true;
            this.colObjectType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colObjectType1.Width = 80;
            // 
            // colHighlight1
            // 
            this.colHighlight1.Caption = "Highlight";
            this.colHighlight1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHighlight1.FieldName = "Highlight";
            this.colHighlight1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colHighlight1.Name = "colHighlight1";
            this.colHighlight1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHighlight1.Visible = true;
            this.colHighlight1.VisibleIndex = 0;
            this.colHighlight1.Width = 61;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colThematicValue
            // 
            this.colThematicValue.Caption = "Thematic Value";
            this.colThematicValue.FieldName = "ThematicValue";
            this.colThematicValue.Name = "colThematicValue";
            this.colThematicValue.OptionsColumn.AllowEdit = false;
            this.colThematicValue.OptionsColumn.AllowFocus = false;
            this.colThematicValue.OptionsColumn.ReadOnly = true;
            this.colThematicValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colThematicValue.Visible = true;
            this.colThematicValue.VisibleIndex = 14;
            this.colThematicValue.Width = 93;
            // 
            // colTooltip1
            // 
            this.colTooltip1.Caption = "Tooltip";
            this.colTooltip1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colTooltip1.FieldName = "Tooltip";
            this.colTooltip1.Name = "colTooltip1";
            this.colTooltip1.OptionsColumn.ReadOnly = true;
            this.colTooltip1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // btnClearMapObjects
            // 
            this.btnClearMapObjects.Image = ((System.Drawing.Image)(resources.GetObject("btnClearMapObjects.Image")));
            this.btnClearMapObjects.Location = new System.Drawing.Point(137, 491);
            this.btnClearMapObjects.Name = "btnClearMapObjects";
            this.btnClearMapObjects.Size = new System.Drawing.Size(118, 22);
            this.btnClearMapObjects.StyleController = this.layoutControl1;
            this.btnClearMapObjects.TabIndex = 11;
            this.btnClearMapObjects.Text = "Clear Map Objects";
            this.btnClearMapObjects.Click += new System.EventHandler(this.btnClearMapObjects_Click);
            // 
            // btnSelectHighlighted
            // 
            this.btnSelectHighlighted.Location = new System.Drawing.Point(228, 570);
            this.btnSelectHighlighted.Name = "btnSelectHighlighted";
            this.btnSelectHighlighted.Size = new System.Drawing.Size(96, 22);
            this.btnSelectHighlighted.StyleController = this.layoutControl1;
            toolTipTitleItem36.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image34")));
            toolTipTitleItem36.Appearance.Options.UseImage = true;
            toolTipTitleItem36.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem36.Image")));
            toolTipTitleItem36.Text = "Select Highlighted - Information";
            toolTipItem36.LeftIndent = 6;
            toolTipItem36.Text = "Click me to select all highlighted map objects (for Editing \\ Deleting \\ Linking " +
    "to Work Orders).";
            superToolTip36.Items.Add(toolTipTitleItem36);
            superToolTip36.Items.Add(toolTipItem36);
            this.btnSelectHighlighted.SuperTip = superToolTip36;
            this.btnSelectHighlighted.TabIndex = 9;
            this.btnSelectHighlighted.Text = "Select Highlighted";
            this.btnSelectHighlighted.Click += new System.EventHandler(this.btnSelectHighlighted_Click);
            // 
            // btnRefreshMapObjects
            // 
            this.btnRefreshMapObjects.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefreshMapObjects.Image = ((System.Drawing.Image)(resources.GetObject("btnRefreshMapObjects.Image")));
            this.btnRefreshMapObjects.Location = new System.Drawing.Point(2, 491);
            this.btnRefreshMapObjects.Name = "btnRefreshMapObjects";
            this.btnRefreshMapObjects.Size = new System.Drawing.Size(131, 22);
            this.btnRefreshMapObjects.StyleController = this.layoutControl1;
            this.btnRefreshMapObjects.TabIndex = 7;
            this.btnRefreshMapObjects.Text = "Refresh Map Objects";
            this.btnRefreshMapObjects.Click += new System.EventHandler(this.btnRefreshMapObjects_Click);
            // 
            // csScaleMapForHighlighted
            // 
            this.csScaleMapForHighlighted.Location = new System.Drawing.Point(10, 596);
            this.csScaleMapForHighlighted.MenuManager = this.barManager1;
            this.csScaleMapForHighlighted.Name = "csScaleMapForHighlighted";
            this.csScaleMapForHighlighted.Properties.Caption = "Scale To Show All Highlighted";
            this.csScaleMapForHighlighted.Size = new System.Drawing.Size(314, 19);
            this.csScaleMapForHighlighted.StyleController = this.layoutControl1;
            this.csScaleMapForHighlighted.TabIndex = 5;
            this.csScaleMapForHighlighted.CheckedChanged += new System.EventHandler(this.csScaleMapForHighlighted_CheckedChanged);
            // 
            // ceCentreMap
            // 
            this.ceCentreMap.EditValue = true;
            this.ceCentreMap.Location = new System.Drawing.Point(10, 570);
            this.ceCentreMap.MenuManager = this.barManager1;
            this.ceCentreMap.Name = "ceCentreMap";
            this.ceCentreMap.Properties.Caption = "Centre on Last Highlighted";
            this.ceCentreMap.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ceCentreMap.Size = new System.Drawing.Size(214, 19);
            this.ceCentreMap.StyleController = this.layoutControl1;
            this.ceCentreMap.TabIndex = 4;
            this.ceCentreMap.CheckedChanged += new System.EventHandler(this.ceCentreMap_CheckedChanged);
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(10, 55);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(314, 424);
            this.gridSplitContainer2.TabIndex = 17;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07049UTTreePickerPolesBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditHighlight,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEditDateRange});
            this.gridControl2.Size = new System.Drawing.Size(314, 424);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07049UTTreePickerPolesBindingSource
            // 
            this.sp07049UTTreePickerPolesBindingSource.DataMember = "sp07049_UT_Tree_Picker_Poles";
            this.sp07049UTTreePickerPolesBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMapID,
            this.colCircuitID,
            this.colPoleNumber,
            this.colPoleTypeID,
            this.gridColumn1,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colX,
            this.colY,
            this.colLatitude,
            this.colLongitude,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.gridColumn14,
            this.colGUID,
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colCircuitStatus,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colRegionName,
            this.colStatus,
            this.colPrimaryName,
            this.colSubAreaName,
            this.colRegionID,
            this.colSubAreaID,
            this.colPrimaryID,
            this.colFeederID,
            this.colPoleType,
            this.colLastInspectionElapsedDays,
            this.colHighlight,
            this.colObjectType,
            this.colPoleID,
            this.colSurveyDone,
            this.colSurveyStatus,
            this.colThematicValue1,
            this.colTooltip});
            this.gridView2.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 20;
            // 
            // colCircuitID
            // 
            this.colCircuitID.Caption = "Circuit ID";
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            this.colCircuitID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 1;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleTypeID
            // 
            this.colPoleTypeID.Caption = "Pole Type ID";
            this.colPoleTypeID.FieldName = "PoleTypeID";
            this.colPoleTypeID.Name = "colPoleTypeID";
            this.colPoleTypeID.OptionsColumn.AllowEdit = false;
            this.colPoleTypeID.OptionsColumn.AllowFocus = false;
            this.colPoleTypeID.OptionsColumn.ReadOnly = true;
            this.colPoleTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Status ID";
            this.gridColumn1.FieldName = "StatusID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateRange;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 13;
            this.colLastInspectionDate.Width = 94;
            // 
            // repositoryItemTextEditDateRange
            // 
            this.repositoryItemTextEditDateRange.AutoHeight = false;
            this.repositoryItemTextEditDateRange.Mask.EditMask = "g";
            this.repositoryItemTextEditDateRange.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateRange.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateRange.Name = "repositoryItemTextEditDateRange";
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 15;
            this.colInspectionCycle.Width = 55;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Cycle Unit ID";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnit.Width = 136;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Cycle Unit";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnitDesc.Visible = true;
            this.colInspectionUnitDesc.VisibleIndex = 16;
            this.colInspectionUnitDesc.Width = 69;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateRange;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 17;
            this.colNextInspectionDate.Width = 97;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colX.Width = 59;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colY.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Width = 66;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Width = 68;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 10;
            this.colIsTransformer.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransformerNumber.Visible = true;
            this.colTransformerNumber.VisibleIndex = 11;
            this.colTransformerNumber.Width = 120;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Remarks";
            this.gridColumn14.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn14.FieldName = "Remarks";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            this.colGUID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 5;
            this.colClientName1.Width = 130;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientCode1.Width = 76;
            // 
            // colCircuitStatus
            // 
            this.colCircuitStatus.Caption = "Client Status";
            this.colCircuitStatus.FieldName = "CircuitStatus";
            this.colCircuitStatus.Name = "colCircuitStatus";
            this.colCircuitStatus.OptionsColumn.AllowEdit = false;
            this.colCircuitStatus.OptionsColumn.AllowFocus = false;
            this.colCircuitStatus.OptionsColumn.ReadOnly = true;
            this.colCircuitStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitStatus.Width = 82;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 2;
            this.colCircuitName.Width = 130;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 19;
            this.colCircuitNumber.Width = 91;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 4;
            this.colVoltageType.Width = 84;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 9;
            this.colFeederName.Width = 130;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 6;
            this.colRegionName.Width = 130;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 3;
            this.colStatus.Width = 63;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 8;
            this.colPrimaryName.Width = 130;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 7;
            this.colSubAreaName.Width = 130;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            this.colRegionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSubAreaID
            // 
            this.colSubAreaID.Caption = "Sub-Area ID";
            this.colSubAreaID.FieldName = "SubAreaID";
            this.colSubAreaID.Name = "colSubAreaID";
            this.colSubAreaID.OptionsColumn.AllowEdit = false;
            this.colSubAreaID.OptionsColumn.AllowFocus = false;
            this.colSubAreaID.OptionsColumn.ReadOnly = true;
            this.colSubAreaID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaID.Width = 80;
            // 
            // colPrimaryID
            // 
            this.colPrimaryID.Caption = "Primary ID";
            this.colPrimaryID.FieldName = "PrimaryID";
            this.colPrimaryID.Name = "colPrimaryID";
            this.colPrimaryID.OptionsColumn.AllowEdit = false;
            this.colPrimaryID.OptionsColumn.AllowFocus = false;
            this.colPrimaryID.OptionsColumn.ReadOnly = true;
            this.colPrimaryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFeederID
            // 
            this.colFeederID.Caption = "Feeder ID";
            this.colFeederID.FieldName = "FeederID";
            this.colFeederID.Name = "colFeederID";
            this.colFeederID.OptionsColumn.AllowEdit = false;
            this.colFeederID.OptionsColumn.AllowFocus = false;
            this.colFeederID.OptionsColumn.ReadOnly = true;
            this.colFeederID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleType.Visible = true;
            this.colPoleType.VisibleIndex = 2;
            // 
            // colLastInspectionElapsedDays
            // 
            this.colLastInspectionElapsedDays.Caption = "Elapsed Days";
            this.colLastInspectionElapsedDays.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.Name = "colLastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionElapsedDays.Visible = true;
            this.colLastInspectionElapsedDays.VisibleIndex = 14;
            this.colLastInspectionElapsedDays.Width = 85;
            // 
            // colHighlight
            // 
            this.colHighlight.Caption = "Highlight";
            this.colHighlight.ColumnEdit = this.repositoryItemCheckEditHighlight;
            this.colHighlight.FieldName = "Highlight";
            this.colHighlight.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colHighlight.Name = "colHighlight";
            this.colHighlight.Visible = true;
            this.colHighlight.VisibleIndex = 0;
            this.colHighlight.Width = 62;
            // 
            // repositoryItemCheckEditHighlight
            // 
            this.repositoryItemCheckEditHighlight.AutoHeight = false;
            this.repositoryItemCheckEditHighlight.Caption = "Check";
            this.repositoryItemCheckEditHighlight.Name = "repositoryItemCheckEditHighlight";
            this.repositoryItemCheckEditHighlight.ValueChecked = 1;
            this.repositoryItemCheckEditHighlight.ValueUnchecked = 0;
            this.repositoryItemCheckEditHighlight.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEditHighlight_CheckedChanged);
            // 
            // colObjectType
            // 
            this.colObjectType.FieldName = "ObjectType";
            this.colObjectType.Name = "colObjectType";
            this.colObjectType.Width = 80;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyDone
            // 
            this.colSurveyDone.Caption = "Survey Done";
            this.colSurveyDone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSurveyDone.FieldName = "SurveyDone";
            this.colSurveyDone.Name = "colSurveyDone";
            this.colSurveyDone.OptionsColumn.AllowEdit = false;
            this.colSurveyDone.OptionsColumn.AllowFocus = false;
            this.colSurveyDone.OptionsColumn.ReadOnly = true;
            this.colSurveyDone.Width = 83;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 12;
            this.colSurveyStatus.Width = 120;
            // 
            // colThematicValue1
            // 
            this.colThematicValue1.Caption = "Thematic Value";
            this.colThematicValue1.FieldName = "ThematicValue";
            this.colThematicValue1.Name = "colThematicValue1";
            this.colThematicValue1.OptionsColumn.AllowEdit = false;
            this.colThematicValue1.OptionsColumn.AllowFocus = false;
            this.colThematicValue1.OptionsColumn.ReadOnly = true;
            this.colThematicValue1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colThematicValue1.Visible = true;
            this.colThematicValue1.VisibleIndex = 21;
            this.colThematicValue1.Width = 93;
            // 
            // colTooltip
            // 
            this.colTooltip.Caption = "Tooltip";
            this.colTooltip.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTooltip.FieldName = "Tooltip";
            this.colTooltip.Name = "colTooltip";
            this.colTooltip.OptionsColumn.ReadOnly = true;
            this.colTooltip.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "g";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colorEditHighlight
            // 
            this.colorEditHighlight.EditValue = System.Drawing.Color.CornflowerBlue;
            this.colorEditHighlight.Location = new System.Drawing.Point(60, 546);
            this.colorEditHighlight.MenuManager = this.barManager1;
            this.colorEditHighlight.Name = "colorEditHighlight";
            this.colorEditHighlight.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", "DropDown", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Highlight", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", "Highlight", null, false),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", "Clear", null, false)});
            this.colorEditHighlight.Size = new System.Drawing.Size(264, 20);
            this.colorEditHighlight.StyleController = this.layoutControl1;
            this.colorEditHighlight.TabIndex = 3;
            this.colorEditHighlight.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.colorEditHighlight_ButtonClick);
            this.colorEditHighlight.EditValueChanged += new System.EventHandler(this.colorEditHighlight_EditValueChanged);
            // 
            // buttonEditFilterCircuits
            // 
            this.buttonEditFilterCircuits.EditValue = "No Circuit Filter";
            this.buttonEditFilterCircuits.Location = new System.Drawing.Point(60, 31);
            this.buttonEditFilterCircuits.MenuManager = this.barManager1;
            this.buttonEditFilterCircuits.Name = "buttonEditFilterCircuits";
            toolTipTitleItem37.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image35")));
            toolTipTitleItem37.Appearance.Options.UseImage = true;
            toolTipTitleItem37.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem37.Image")));
            toolTipTitleItem37.Text = "Circuit(s) Filter � Information";
            toolTipItem37.LeftIndent = 6;
            toolTipItem37.Text = "Click me to specify the Circuits \\ Poles to load into the map.";
            superToolTip37.Items.Add(toolTipTitleItem37);
            superToolTip37.Items.Add(toolTipItem37);
            superToolTip38.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem38.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image36")));
            toolTipTitleItem38.Appearance.Options.UseImage = true;
            toolTipTitleItem38.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem38.Image")));
            toolTipTitleItem38.Text = "Refresh Poles button - Information";
            toolTipItem38.LeftIndent = 6;
            toolTipItem38.Text = "Click me to reload the list of available map objects.";
            superToolTip38.Items.Add(toolTipTitleItem38);
            superToolTip38.Items.Add(toolTipItem38);
            this.buttonEditFilterCircuits.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", "choose", superToolTip37, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Refresh Poles", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, "", "refresh poles", superToolTip38, true)});
            this.buttonEditFilterCircuits.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEditFilterCircuits.Size = new System.Drawing.Size(264, 20);
            this.buttonEditFilterCircuits.StyleController = this.layoutControl1;
            superToolTip39.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem39.Text = "Circuit(s) Filter - Information";
            toolTipItem39.LeftIndent = 6;
            superToolTip39.Items.Add(toolTipTitleItem39);
            superToolTip39.Items.Add(toolTipItem39);
            this.buttonEditFilterCircuits.SuperTip = superToolTip39;
            this.buttonEditFilterCircuits.TabIndex = 8;
            this.buttonEditFilterCircuits.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEditFilterCircuits_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem55,
            this.emptySpaceItem3,
            this.layoutControlItem10,
            this.tabbedControlGroup1,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(334, 625);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Highlighting";
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem51,
            this.layoutControlItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 515);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(334, 110);
            this.layoutControlGroup2.Text = "Highlighting";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.colorEditHighlight;
            this.layoutControlItem1.CustomizationFormText = "Colour:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem1.Text = "Colour:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(47, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ceCentreMap;
            this.layoutControlItem2.CustomizationFormText = "Centre Map:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(218, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.csScaleMapForHighlighted;
            this.layoutControlItem51.CustomizationFormText = "layoutControlItem51";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(318, 23);
            this.layoutControlItem51.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem51.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSelectHighlighted;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(218, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(100, 26);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.btnRefreshMapObjects;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 489);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(135, 26);
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(314, 489);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(20, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnClearMapObjects;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(135, 489);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(122, 26);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(334, 489);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Poles";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem54,
            this.layoutControlItem7});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(318, 452);
            this.layoutControlGroup3.Text = "Poles";
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.gridSplitContainer2;
            this.layoutControlItem54.CustomizationFormText = "Map Object Grid:";
            this.layoutControlItem54.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(318, 428);
            this.layoutControlItem54.Text = "Map Object Grid:";
            this.layoutControlItem54.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem54.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.buttonEditFilterCircuits;
            this.layoutControlItem7.CustomizationFormText = "Circuit(s):";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem7.Text = "Circuit(s):";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(47, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Linked Trees";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem12,
            this.emptySpaceItem1,
            this.ItemForRefreshTrees});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(318, 452);
            this.layoutControlGroup5.Text = "Linked Trees";
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.gridSplitContainer1;
            this.layoutControlItem12.CustomizationFormText = "Asset Management Objects Grid:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(318, 426);
            this.layoutControlItem12.Text = "Asset Management Objects Grid:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(228, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRefreshTrees
            // 
            this.ItemForRefreshTrees.Control = this.btnRefreshTrees;
            this.ItemForRefreshTrees.CustomizationFormText = "Refresh Trees Button:";
            this.ItemForRefreshTrees.Location = new System.Drawing.Point(228, 0);
            this.ItemForRefreshTrees.MaxSize = new System.Drawing.Size(90, 26);
            this.ItemForRefreshTrees.MinSize = new System.Drawing.Size(90, 26);
            this.ItemForRefreshTrees.Name = "ItemForRefreshTrees";
            this.ItemForRefreshTrees.Size = new System.Drawing.Size(90, 26);
            this.ItemForRefreshTrees.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefreshTrees.Text = "Refresh Trees Button:";
            this.ItemForRefreshTrees.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRefreshTrees.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnScaleToViewedObjects;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(257, 489);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(57, 26);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // dockPanelGPS
            // 
            this.dockPanelGPS.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.dockPanelGPS.Appearance.Options.UseBackColor = true;
            this.dockPanelGPS.Controls.Add(this.dockPanel3_Container);
            this.dockPanelGPS.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanelGPS.FloatSize = new System.Drawing.Size(279, 385);
            this.dockPanelGPS.FloatVertical = true;
            this.dockPanelGPS.ID = new System.Guid("59c82752-c9f0-4a41-ad5f-04fc17d69dce");
            this.dockPanelGPS.Location = new System.Drawing.Point(3, 29);
            this.dockPanelGPS.Name = "dockPanelGPS";
            this.dockPanelGPS.Options.ShowCloseButton = false;
            this.dockPanelGPS.OriginalSize = new System.Drawing.Size(334, 625);
            this.dockPanelGPS.Size = new System.Drawing.Size(334, 625);
            this.dockPanelGPS.Text = "GPS";
            // 
            // dockPanel3_Container
            // 
            this.dockPanel3_Container.Controls.Add(this.statusBar1);
            this.dockPanel3_Container.Controls.Add(this.NMEAtabs);
            this.dockPanel3_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel3_Container.Name = "dockPanel3_Container";
            this.dockPanel3_Container.Size = new System.Drawing.Size(334, 625);
            this.dockPanel3_Container.TabIndex = 0;
            // 
            // statusBar1
            // 
            this.statusBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusBar1.Location = new System.Drawing.Point(0, 602);
            this.statusBar1.MenuManager = this.barManager1;
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.statusBar1.Properties.Appearance.Options.UseFont = true;
            this.statusBar1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.statusBar1, true);
            this.statusBar1.Size = new System.Drawing.Size(334, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.statusBar1, optionsSpelling1);
            this.statusBar1.TabIndex = 1;
            // 
            // NMEAtabs
            // 
            this.NMEAtabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NMEAtabs.Location = new System.Drawing.Point(0, 0);
            this.NMEAtabs.Name = "NMEAtabs";
            this.NMEAtabs.SelectedTabPage = this.xtraTabPage1;
            this.NMEAtabs.Size = new System.Drawing.Size(335, 599);
            this.NMEAtabs.TabIndex = 0;
            this.NMEAtabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.tabGPRMC,
            this.tabGPGGA,
            this.tabGPGLL,
            this.tabGPGSA,
            this.tabGPGSV,
            this.tabRaw});
            this.NMEAtabs.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.NMEAtabs_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.labelPlottingUsingTimer);
            this.xtraTabPage1.Controls.Add(this.btnGps_PauseTimer);
            this.xtraTabPage1.Controls.Add(this.layoutControl_Gps_Page1);
            this.xtraTabPage1.Controls.Add(this.groupControl2);
            this.xtraTabPage1.Controls.Add(this.btnGPS_Start);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(330, 573);
            this.xtraTabPage1.Text = "Main";
            // 
            // labelPlottingUsingTimer
            // 
            this.labelPlottingUsingTimer.AllowHtmlString = true;
            this.labelPlottingUsingTimer.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelPlottingUsingTimer.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelPlottingUsingTimer.Appearance.Image")));
            this.labelPlottingUsingTimer.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelPlottingUsingTimer.Location = new System.Drawing.Point(95, 217);
            this.labelPlottingUsingTimer.Name = "labelPlottingUsingTimer";
            this.labelPlottingUsingTimer.Size = new System.Drawing.Size(134, 20);
            this.labelPlottingUsingTimer.TabIndex = 6;
            this.labelPlottingUsingTimer.Text = "Plotting using Timer";
            this.labelPlottingUsingTimer.Visible = false;
            // 
            // btnGps_PauseTimer
            // 
            this.btnGps_PauseTimer.Enabled = false;
            this.btnGps_PauseTimer.Location = new System.Drawing.Point(9, 135);
            this.btnGps_PauseTimer.Name = "btnGps_PauseTimer";
            this.btnGps_PauseTimer.Size = new System.Drawing.Size(75, 23);
            this.btnGps_PauseTimer.TabIndex = 5;
            this.btnGps_PauseTimer.Text = "Pause Timer";
            this.btnGps_PauseTimer.Click += new System.EventHandler(this.btnGps_PauseTimer_Click);
            // 
            // layoutControl_Gps_Page1
            // 
            this.layoutControl_Gps_Page1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.layoutControl_Gps_Page1.Controls.Add(this.ceLogRawGPSData);
            this.layoutControl_Gps_Page1.Controls.Add(this.cmbPortName);
            this.layoutControl_Gps_Page1.Controls.Add(this.cmbBaudRate);
            this.layoutControl_Gps_Page1.Controls.Add(this.ceGps_PlotWithTimer);
            this.layoutControl_Gps_Page1.Controls.Add(this.seGps_PlotTimerInterval);
            this.layoutControl_Gps_Page1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_Gps_Page1.MinimumSize = new System.Drawing.Size(0, 90);
            this.layoutControl_Gps_Page1.Name = "layoutControl_Gps_Page1";
            this.layoutControl_Gps_Page1.Root = this.layoutControlGroup12;
            this.layoutControl_Gps_Page1.Size = new System.Drawing.Size(328, 102);
            this.layoutControl_Gps_Page1.TabIndex = 4;
            this.layoutControl_Gps_Page1.Text = "layoutControl6";
            // 
            // ceLogRawGPSData
            // 
            this.ceLogRawGPSData.Location = new System.Drawing.Point(7, 52);
            this.ceLogRawGPSData.MenuManager = this.barManager1;
            this.ceLogRawGPSData.Name = "ceLogRawGPSData";
            this.ceLogRawGPSData.Properties.Caption = "Log Raw GPS Data";
            this.ceLogRawGPSData.Size = new System.Drawing.Size(314, 19);
            this.ceLogRawGPSData.StyleController = this.layoutControl_Gps_Page1;
            this.ceLogRawGPSData.TabIndex = 6;
            this.ceLogRawGPSData.CheckedChanged += new System.EventHandler(this.ceLogRawGPSData_CheckedChanged);
            // 
            // cmbPortName
            // 
            this.cmbPortName.Location = new System.Drawing.Point(36, 28);
            this.cmbPortName.MenuManager = this.barManager1;
            this.cmbPortName.Name = "cmbPortName";
            this.cmbPortName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbPortName.Size = new System.Drawing.Size(128, 20);
            this.cmbPortName.StyleController = this.layoutControl_Gps_Page1;
            this.cmbPortName.TabIndex = 2;
            // 
            // cmbBaudRate
            // 
            this.cmbBaudRate.Location = new System.Drawing.Point(227, 28);
            this.cmbBaudRate.MenuManager = this.barManager1;
            this.cmbBaudRate.Name = "cmbBaudRate";
            this.cmbBaudRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbBaudRate.Properties.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "28800",
            "36000",
            "38400",
            "115000"});
            this.cmbBaudRate.Size = new System.Drawing.Size(94, 20);
            this.cmbBaudRate.StyleController = this.layoutControl_Gps_Page1;
            this.cmbBaudRate.TabIndex = 3;
            // 
            // ceGps_PlotWithTimer
            // 
            this.ceGps_PlotWithTimer.Enabled = false;
            this.ceGps_PlotWithTimer.Location = new System.Drawing.Point(7, 75);
            this.ceGps_PlotWithTimer.MenuManager = this.barManager1;
            this.ceGps_PlotWithTimer.Name = "ceGps_PlotWithTimer";
            this.ceGps_PlotWithTimer.Properties.Caption = "Plot Using Timer";
            this.ceGps_PlotWithTimer.Size = new System.Drawing.Size(135, 19);
            this.ceGps_PlotWithTimer.StyleController = this.layoutControl_Gps_Page1;
            this.ceGps_PlotWithTimer.TabIndex = 4;
            this.ceGps_PlotWithTimer.CheckedChanged += new System.EventHandler(this.ceGps_PlotWithTimer_CheckedChanged);
            // 
            // seGps_PlotTimerInterval
            // 
            this.seGps_PlotTimerInterval.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Enabled = false;
            this.seGps_PlotTimerInterval.Location = new System.Drawing.Point(220, 75);
            this.seGps_PlotTimerInterval.MenuManager = this.barManager1;
            this.seGps_PlotTimerInterval.Name = "seGps_PlotTimerInterval";
            this.seGps_PlotTimerInterval.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.seGps_PlotTimerInterval.Properties.IsFloatValue = false;
            this.seGps_PlotTimerInterval.Properties.Mask.EditMask = "N00";
            this.seGps_PlotTimerInterval.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.seGps_PlotTimerInterval.Size = new System.Drawing.Size(101, 20);
            this.seGps_PlotTimerInterval.StyleController = this.layoutControl_Gps_Page1;
            superToolTip40.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem40.Text = "GPS Timer Interval - Information";
            toolTipItem40.LeftIndent = 6;
            superToolTip40.Items.Add(toolTipTitleItem40);
            superToolTip40.Items.Add(toolTipItem40);
            this.seGps_PlotTimerInterval.SuperTip = superToolTip40;
            this.seGps_PlotTimerInterval.TabIndex = 5;
            this.seGps_PlotTimerInterval.EditValueChanged += new System.EventHandler(this.seGps_PlotTimerInterval_EditValueChanged);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Root";
            this.layoutControlGroup12.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup12.GroupBordersVisible = false;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "Root";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Size = new System.Drawing.Size(328, 102);
            this.layoutControlGroup12.TextVisible = false;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "GPG Settings";
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem6});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup13.Size = new System.Drawing.Size(328, 102);
            this.layoutControlGroup13.Text = "GPG Settings";
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AllowHtmlStringInCaption = true;
            this.layoutControlItem47.Control = this.cmbPortName;
            this.layoutControlItem47.CustomizationFormText = "Port:";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem47.Text = "Port:";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(24, 13);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AllowHtmlStringInCaption = true;
            this.layoutControlItem48.Control = this.cmbBaudRate;
            this.layoutControlItem48.CustomizationFormText = "Baud Rate:";
            this.layoutControlItem48.Location = new System.Drawing.Point(161, 0);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(157, 24);
            this.layoutControlItem48.Text = "Baud Rate:";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(54, 13);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.ceGps_PlotWithTimer;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(0, 47);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(139, 24);
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.seGps_PlotTimerInterval;
            this.layoutControlItem50.CustomizationFormText = "Timer Interval:";
            this.layoutControlItem50.Location = new System.Drawing.Point(139, 47);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem50.Text = "Timer Interval:";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(71, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ceLogRawGPSData;
            this.layoutControlItem6.CustomizationFormText = "Log Raw GPS Data";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(318, 23);
            this.layoutControlItem6.Text = "Log Raw GPS Data";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnGPS_PlotStart);
            this.groupControl2.Controls.Add(this.btnGPS_PlotStop);
            this.groupControl2.Controls.Add(this.ceGPS_PlotLine);
            this.groupControl2.Controls.Add(this.btnGPS_Plot);
            this.groupControl2.Controls.Add(this.ceGPS_PlotPolygon);
            this.groupControl2.Controls.Add(this.ceGPS_PlotPoint);
            this.groupControl2.Enabled = false;
            this.groupControl2.Location = new System.Drawing.Point(91, 110);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(147, 103);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Plot";
            // 
            // btnGPS_PlotStart
            // 
            this.btnGPS_PlotStart.Location = new System.Drawing.Point(67, 25);
            this.btnGPS_PlotStart.Name = "btnGPS_PlotStart";
            this.btnGPS_PlotStart.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_PlotStart.TabIndex = 4;
            this.btnGPS_PlotStart.Text = "Start";
            this.btnGPS_PlotStart.Click += new System.EventHandler(this.btnGPS_PlotStart_Click);
            // 
            // btnGPS_PlotStop
            // 
            this.btnGPS_PlotStop.Enabled = false;
            this.btnGPS_PlotStop.Location = new System.Drawing.Point(67, 75);
            this.btnGPS_PlotStop.Name = "btnGPS_PlotStop";
            this.btnGPS_PlotStop.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_PlotStop.TabIndex = 3;
            this.btnGPS_PlotStop.Text = "End";
            this.btnGPS_PlotStop.Click += new System.EventHandler(this.btnGPS_PlotStop_Click);
            // 
            // ceGPS_PlotLine
            // 
            this.ceGPS_PlotLine.Location = new System.Drawing.Point(5, 77);
            this.ceGPS_PlotLine.MenuManager = this.barManager1;
            this.ceGPS_PlotLine.Name = "ceGPS_PlotLine";
            this.ceGPS_PlotLine.Properties.Caption = "Line";
            this.ceGPS_PlotLine.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotLine.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotLine.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotLine.TabIndex = 2;
            this.ceGPS_PlotLine.TabStop = false;
            this.ceGPS_PlotLine.CheckedChanged += new System.EventHandler(this.ceGPS_PlotLine_CheckedChanged);
            // 
            // btnGPS_Plot
            // 
            this.btnGPS_Plot.Location = new System.Drawing.Point(67, 50);
            this.btnGPS_Plot.Name = "btnGPS_Plot";
            this.btnGPS_Plot.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_Plot.TabIndex = 2;
            this.btnGPS_Plot.Text = "Plot";
            this.btnGPS_Plot.Click += new System.EventHandler(this.btnGPS_Plot_Click);
            // 
            // ceGPS_PlotPolygon
            // 
            this.ceGPS_PlotPolygon.Location = new System.Drawing.Point(5, 52);
            this.ceGPS_PlotPolygon.MenuManager = this.barManager1;
            this.ceGPS_PlotPolygon.Name = "ceGPS_PlotPolygon";
            this.ceGPS_PlotPolygon.Properties.Caption = "Polygon";
            this.ceGPS_PlotPolygon.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotPolygon.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotPolygon.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotPolygon.TabIndex = 1;
            this.ceGPS_PlotPolygon.TabStop = false;
            this.ceGPS_PlotPolygon.CheckedChanged += new System.EventHandler(this.ceGPS_PlotPolygon_CheckedChanged);
            // 
            // ceGPS_PlotPoint
            // 
            this.ceGPS_PlotPoint.EditValue = true;
            this.ceGPS_PlotPoint.Location = new System.Drawing.Point(5, 27);
            this.ceGPS_PlotPoint.MenuManager = this.barManager1;
            this.ceGPS_PlotPoint.Name = "ceGPS_PlotPoint";
            this.ceGPS_PlotPoint.Properties.Caption = "Point";
            this.ceGPS_PlotPoint.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.ceGPS_PlotPoint.Properties.RadioGroupIndex = 1;
            this.ceGPS_PlotPoint.Size = new System.Drawing.Size(60, 19);
            this.ceGPS_PlotPoint.TabIndex = 0;
            this.ceGPS_PlotPoint.CheckedChanged += new System.EventHandler(this.ceGPS_PlotPoint_CheckedChanged);
            // 
            // btnGPS_Start
            // 
            this.btnGPS_Start.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnGPS_Start.Appearance.Options.UseFont = true;
            this.btnGPS_Start.Location = new System.Drawing.Point(9, 110);
            this.btnGPS_Start.Name = "btnGPS_Start";
            this.btnGPS_Start.Size = new System.Drawing.Size(75, 23);
            this.btnGPS_Start.TabIndex = 0;
            this.btnGPS_Start.Text = "Start GPS";
            this.btnGPS_Start.Click += new System.EventHandler(this.btnGPS_Start_Click);
            // 
            // tabGPRMC
            // 
            this.tabGPRMC.Controls.Add(this.layoutControl_GPS_Page2);
            this.tabGPRMC.Name = "tabGPRMC";
            this.tabGPRMC.Size = new System.Drawing.Size(330, 573);
            this.tabGPRMC.Text = "GPRMC";
            // 
            // layoutControl_GPS_Page2
            // 
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCGridRef);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCPositionUTM);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCMagneticVariation);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCTimeOfFix);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCSpeed);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCCourse);
            this.layoutControl_GPS_Page2.Controls.Add(this.label5);
            this.layoutControl_GPS_Page2.Controls.Add(this.lbRMCPosition);
            this.layoutControl_GPS_Page2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page2.Name = "layoutControl_GPS_Page2";
            this.layoutControl_GPS_Page2.Root = this.layoutControlGroup4;
            this.layoutControl_GPS_Page2.Size = new System.Drawing.Size(330, 573);
            this.layoutControl_GPS_Page2.TabIndex = 1;
            this.layoutControl_GPS_Page2.Text = "layoutControl5";
            // 
            // lbRMCGridRef
            // 
            this.lbRMCGridRef.Location = new System.Drawing.Point(97, 163);
            this.lbRMCGridRef.MenuManager = this.barManager1;
            this.lbRMCGridRef.Name = "lbRMCGridRef";
            this.lbRMCGridRef.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCGridRef, true);
            this.lbRMCGridRef.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCGridRef, optionsSpelling2);
            this.lbRMCGridRef.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCGridRef.TabIndex = 10;
            // 
            // lbRMCPositionUTM
            // 
            this.lbRMCPositionUTM.Location = new System.Drawing.Point(97, 139);
            this.lbRMCPositionUTM.MenuManager = this.barManager1;
            this.lbRMCPositionUTM.Name = "lbRMCPositionUTM";
            this.lbRMCPositionUTM.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCPositionUTM, true);
            this.lbRMCPositionUTM.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCPositionUTM, optionsSpelling3);
            this.lbRMCPositionUTM.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCPositionUTM.TabIndex = 9;
            // 
            // lbRMCMagneticVariation
            // 
            this.lbRMCMagneticVariation.Location = new System.Drawing.Point(97, 115);
            this.lbRMCMagneticVariation.MenuManager = this.barManager1;
            this.lbRMCMagneticVariation.Name = "lbRMCMagneticVariation";
            this.lbRMCMagneticVariation.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCMagneticVariation, true);
            this.lbRMCMagneticVariation.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCMagneticVariation, optionsSpelling4);
            this.lbRMCMagneticVariation.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCMagneticVariation.TabIndex = 8;
            // 
            // lbRMCTimeOfFix
            // 
            this.lbRMCTimeOfFix.Location = new System.Drawing.Point(97, 91);
            this.lbRMCTimeOfFix.MenuManager = this.barManager1;
            this.lbRMCTimeOfFix.Name = "lbRMCTimeOfFix";
            this.lbRMCTimeOfFix.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCTimeOfFix, true);
            this.lbRMCTimeOfFix.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCTimeOfFix, optionsSpelling5);
            this.lbRMCTimeOfFix.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCTimeOfFix.TabIndex = 7;
            // 
            // lbRMCSpeed
            // 
            this.lbRMCSpeed.Location = new System.Drawing.Point(97, 67);
            this.lbRMCSpeed.MenuManager = this.barManager1;
            this.lbRMCSpeed.Name = "lbRMCSpeed";
            this.lbRMCSpeed.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCSpeed, true);
            this.lbRMCSpeed.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCSpeed, optionsSpelling6);
            this.lbRMCSpeed.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCSpeed.TabIndex = 6;
            // 
            // lbRMCCourse
            // 
            this.lbRMCCourse.Location = new System.Drawing.Point(97, 43);
            this.lbRMCCourse.MenuManager = this.barManager1;
            this.lbRMCCourse.Name = "lbRMCCourse";
            this.lbRMCCourse.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCCourse, true);
            this.lbRMCCourse.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCCourse, optionsSpelling7);
            this.lbRMCCourse.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCCourse.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(2, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.StyleController = this.layoutControl_GPS_Page2;
            this.label5.TabIndex = 0;
            this.label5.Text = "GPS/Transit data";
            // 
            // lbRMCPosition
            // 
            this.lbRMCPosition.Location = new System.Drawing.Point(97, 19);
            this.lbRMCPosition.MenuManager = this.barManager1;
            this.lbRMCPosition.Name = "lbRMCPosition";
            this.lbRMCPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbRMCPosition, true);
            this.lbRMCPosition.Size = new System.Drawing.Size(231, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbRMCPosition, optionsSpelling8);
            this.lbRMCPosition.StyleController = this.layoutControl_GPS_Page2;
            this.lbRMCPosition.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem9,
            this.layoutControlItem46});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(330, 573);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lbRMCPosition;
            this.layoutControlItem4.CustomizationFormText = "Position:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem4.Text = "Position:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lbRMCCourse;
            this.layoutControlItem5.CustomizationFormText = "Course:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem5.Text = "Course:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.lbRMCSpeed;
            this.layoutControlItem18.CustomizationFormText = "Speed:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem18.Text = "Speed:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lbRMCTimeOfFix;
            this.layoutControlItem21.CustomizationFormText = "Time of Fix:";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem21.Text = "Time of Fix:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lbRMCMagneticVariation;
            this.layoutControlItem22.CustomizationFormText = "Magnetic Variation:";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem22.Text = "Magnetic Variation:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.lbRMCPositionUTM;
            this.layoutControlItem23.CustomizationFormText = "UTM Position:";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem23.Text = "UTM Position:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(92, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.label5;
            this.layoutControlItem9.CustomizationFormText = "Title:";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem9.Text = "Title:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.lbRMCGridRef;
            this.layoutControlItem46.CustomizationFormText = "Grid Ref:";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(330, 412);
            this.layoutControlItem46.Text = "Grid Ref:";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(92, 13);
            // 
            // tabGPGGA
            // 
            this.tabGPGGA.Controls.Add(this.layoutControl_GPS_Page3);
            this.tabGPGGA.Name = "tabGPGGA";
            this.tabGPGGA.Size = new System.Drawing.Size(330, 573);
            this.tabGPGGA.Text = "GPGGA";
            // 
            // layoutControl_GPS_Page3
            // 
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGADGPSID);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGADGPSupdate);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAGeoidHeight);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAHDOP);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAAltitude);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGANoOfSats);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAFixQuality);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGATimeOfFix);
            this.layoutControl_GPS_Page3.Controls.Add(this.lbGGAPosition);
            this.layoutControl_GPS_Page3.Controls.Add(this.label12);
            this.layoutControl_GPS_Page3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page3.Name = "layoutControl_GPS_Page3";
            this.layoutControl_GPS_Page3.Root = this.layoutControlGroup8;
            this.layoutControl_GPS_Page3.Size = new System.Drawing.Size(330, 573);
            this.layoutControl_GPS_Page3.TabIndex = 2;
            this.layoutControl_GPS_Page3.Text = "layoutControl5";
            // 
            // lbGGADGPSID
            // 
            this.lbGGADGPSID.Location = new System.Drawing.Point(93, 211);
            this.lbGGADGPSID.MenuManager = this.barManager1;
            this.lbGGADGPSID.Name = "lbGGADGPSID";
            this.lbGGADGPSID.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGADGPSID, true);
            this.lbGGADGPSID.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGADGPSID, optionsSpelling9);
            this.lbGGADGPSID.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGADGPSID.TabIndex = 12;
            // 
            // lbGGADGPSupdate
            // 
            this.lbGGADGPSupdate.Location = new System.Drawing.Point(93, 187);
            this.lbGGADGPSupdate.MenuManager = this.barManager1;
            this.lbGGADGPSupdate.Name = "lbGGADGPSupdate";
            this.lbGGADGPSupdate.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGADGPSupdate, true);
            this.lbGGADGPSupdate.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGADGPSupdate, optionsSpelling10);
            this.lbGGADGPSupdate.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGADGPSupdate.TabIndex = 11;
            // 
            // lbGGAGeoidHeight
            // 
            this.lbGGAGeoidHeight.Location = new System.Drawing.Point(93, 163);
            this.lbGGAGeoidHeight.MenuManager = this.barManager1;
            this.lbGGAGeoidHeight.Name = "lbGGAGeoidHeight";
            this.lbGGAGeoidHeight.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAGeoidHeight, true);
            this.lbGGAGeoidHeight.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAGeoidHeight, optionsSpelling11);
            this.lbGGAGeoidHeight.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAGeoidHeight.TabIndex = 10;
            // 
            // lbGGAHDOP
            // 
            this.lbGGAHDOP.Location = new System.Drawing.Point(93, 139);
            this.lbGGAHDOP.MenuManager = this.barManager1;
            this.lbGGAHDOP.Name = "lbGGAHDOP";
            this.lbGGAHDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAHDOP, true);
            this.lbGGAHDOP.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAHDOP, optionsSpelling12);
            this.lbGGAHDOP.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAHDOP.TabIndex = 9;
            // 
            // lbGGAAltitude
            // 
            this.lbGGAAltitude.Location = new System.Drawing.Point(93, 115);
            this.lbGGAAltitude.MenuManager = this.barManager1;
            this.lbGGAAltitude.Name = "lbGGAAltitude";
            this.lbGGAAltitude.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAAltitude, true);
            this.lbGGAAltitude.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAAltitude, optionsSpelling13);
            this.lbGGAAltitude.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAAltitude.TabIndex = 8;
            // 
            // lbGGANoOfSats
            // 
            this.lbGGANoOfSats.Location = new System.Drawing.Point(93, 91);
            this.lbGGANoOfSats.MenuManager = this.barManager1;
            this.lbGGANoOfSats.Name = "lbGGANoOfSats";
            this.lbGGANoOfSats.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGANoOfSats, true);
            this.lbGGANoOfSats.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGANoOfSats, optionsSpelling14);
            this.lbGGANoOfSats.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGANoOfSats.TabIndex = 7;
            // 
            // lbGGAFixQuality
            // 
            this.lbGGAFixQuality.Location = new System.Drawing.Point(93, 67);
            this.lbGGAFixQuality.MenuManager = this.barManager1;
            this.lbGGAFixQuality.Name = "lbGGAFixQuality";
            this.lbGGAFixQuality.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAFixQuality, true);
            this.lbGGAFixQuality.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAFixQuality, optionsSpelling15);
            this.lbGGAFixQuality.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAFixQuality.TabIndex = 6;
            // 
            // lbGGATimeOfFix
            // 
            this.lbGGATimeOfFix.Location = new System.Drawing.Point(93, 43);
            this.lbGGATimeOfFix.MenuManager = this.barManager1;
            this.lbGGATimeOfFix.Name = "lbGGATimeOfFix";
            this.lbGGATimeOfFix.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGATimeOfFix, true);
            this.lbGGATimeOfFix.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGATimeOfFix, optionsSpelling16);
            this.lbGGATimeOfFix.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGATimeOfFix.TabIndex = 5;
            // 
            // lbGGAPosition
            // 
            this.lbGGAPosition.Location = new System.Drawing.Point(93, 19);
            this.lbGGAPosition.MenuManager = this.barManager1;
            this.lbGGAPosition.Name = "lbGGAPosition";
            this.lbGGAPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGGAPosition, true);
            this.lbGGAPosition.Size = new System.Drawing.Size(235, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGGAPosition, optionsSpelling17);
            this.lbGGAPosition.StyleController = this.layoutControl_GPS_Page3;
            this.lbGGAPosition.TabIndex = 4;
            // 
            // label12
            // 
            this.label12.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(2, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(195, 13);
            this.label12.StyleController = this.layoutControl_GPS_Page3;
            this.label12.TabIndex = 1;
            this.label12.Text = "Global Positioning System Fix Data";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(330, 573);
            this.layoutControlGroup8.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.label12;
            this.layoutControlItem24.CustomizationFormText = "Title:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem24.Text = "Title:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.lbGGAPosition;
            this.layoutControlItem28.CustomizationFormText = "Position:";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem28.Text = "Position:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.lbGGATimeOfFix;
            this.layoutControlItem29.CustomizationFormText = "Time of Fix:";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem29.Text = "Time of Fix:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.lbGGAFixQuality;
            this.layoutControlItem30.CustomizationFormText = "Fix Quality:";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem30.Text = "Fix Quality:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.lbGGANoOfSats;
            this.layoutControlItem31.CustomizationFormText = "Tracked Satellites:";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem31.Text = "Tracked Satellites:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.lbGGAAltitude;
            this.layoutControlItem32.CustomizationFormText = "Altitude:";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem32.Text = "Altitude:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.lbGGAHDOP;
            this.layoutControlItem33.CustomizationFormText = "HDOP:";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem33.Text = "HDOP:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.lbGGAGeoidHeight;
            this.layoutControlItem34.CustomizationFormText = "Height of Geoid:";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem34.Text = "Height of Geoid:";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.lbGGADGPSupdate;
            this.layoutControlItem35.CustomizationFormText = "DGPS Update:";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 185);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem35.Text = "DGPS Update:";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.lbGGADGPSID;
            this.layoutControlItem36.CustomizationFormText = "DGPS Station ID:";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 209);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(330, 364);
            this.layoutControlItem36.Text = "DGPS Station ID:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(88, 13);
            // 
            // tabGPGLL
            // 
            this.tabGPGLL.Controls.Add(this.layoutControl_GPS_Page4);
            this.tabGPGLL.Name = "tabGPGLL";
            this.tabGPGLL.Size = new System.Drawing.Size(330, 573);
            this.tabGPGLL.Text = "GPGLL";
            // 
            // layoutControl_GPS_Page4
            // 
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLDataValid);
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLTimeOfSolution);
            this.layoutControl_GPS_Page4.Controls.Add(this.lbGLLPosition);
            this.layoutControl_GPS_Page4.Controls.Add(this.label30);
            this.layoutControl_GPS_Page4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page4.Name = "layoutControl_GPS_Page4";
            this.layoutControl_GPS_Page4.Root = this.layoutControlGroup10;
            this.layoutControl_GPS_Page4.Size = new System.Drawing.Size(330, 573);
            this.layoutControl_GPS_Page4.TabIndex = 3;
            this.layoutControl_GPS_Page4.Text = "layoutControl6";
            // 
            // lbGLLDataValid
            // 
            this.lbGLLDataValid.Location = new System.Drawing.Point(85, 67);
            this.lbGLLDataValid.MenuManager = this.barManager1;
            this.lbGLLDataValid.Name = "lbGLLDataValid";
            this.lbGLLDataValid.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLDataValid, true);
            this.lbGLLDataValid.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLDataValid, optionsSpelling18);
            this.lbGLLDataValid.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLDataValid.TabIndex = 6;
            // 
            // lbGLLTimeOfSolution
            // 
            this.lbGLLTimeOfSolution.Location = new System.Drawing.Point(85, 43);
            this.lbGLLTimeOfSolution.MenuManager = this.barManager1;
            this.lbGLLTimeOfSolution.Name = "lbGLLTimeOfSolution";
            this.lbGLLTimeOfSolution.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLTimeOfSolution, true);
            this.lbGLLTimeOfSolution.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLTimeOfSolution, optionsSpelling19);
            this.lbGLLTimeOfSolution.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLTimeOfSolution.TabIndex = 5;
            // 
            // lbGLLPosition
            // 
            this.lbGLLPosition.Location = new System.Drawing.Point(85, 19);
            this.lbGLLPosition.MenuManager = this.barManager1;
            this.lbGLLPosition.Name = "lbGLLPosition";
            this.lbGLLPosition.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGLLPosition, true);
            this.lbGLLPosition.Size = new System.Drawing.Size(243, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGLLPosition, optionsSpelling20);
            this.lbGLLPosition.StyleController = this.layoutControl_GPS_Page4;
            this.lbGLLPosition.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(2, 2);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 13);
            this.label30.StyleController = this.layoutControl_GPS_Page4;
            this.label30.TabIndex = 2;
            this.label30.Text = "Geographic Position";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.GroupBordersVisible = false;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(330, 573);
            this.layoutControlGroup10.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.label30;
            this.layoutControlItem26.CustomizationFormText = "Title:";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem26.Text = "Title:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.lbGLLPosition;
            this.layoutControlItem37.CustomizationFormText = "Position:";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem37.Text = "Position:";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.lbGLLTimeOfSolution;
            this.layoutControlItem38.CustomizationFormText = "Time of Solution:";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem38.Text = "Time of Solution:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.lbGLLDataValid;
            this.layoutControlItem39.CustomizationFormText = "Data Valid:";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(330, 508);
            this.layoutControlItem39.Text = "Data Valid:";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(80, 13);
            // 
            // tabGPGSA
            // 
            this.tabGPGSA.Controls.Add(this.layoutControl_GPS_Page5);
            this.tabGPGSA.Name = "tabGPGSA";
            this.tabGPGSA.Size = new System.Drawing.Size(330, 573);
            this.tabGPGSA.Text = "GPGSA";
            // 
            // layoutControl_GPS_Page5
            // 
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAVDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAHDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAPDOP);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAPRNs);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAFixMode);
            this.layoutControl_GPS_Page5.Controls.Add(this.lbGSAMode);
            this.layoutControl_GPS_Page5.Controls.Add(this.label32);
            this.layoutControl_GPS_Page5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl_GPS_Page5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl_GPS_Page5.Name = "layoutControl_GPS_Page5";
            this.layoutControl_GPS_Page5.Root = this.layoutControlGroup11;
            this.layoutControl_GPS_Page5.Size = new System.Drawing.Size(330, 573);
            this.layoutControl_GPS_Page5.TabIndex = 4;
            this.layoutControl_GPS_Page5.Text = "layoutControl7";
            // 
            // lbGSAVDOP
            // 
            this.lbGSAVDOP.Location = new System.Drawing.Point(86, 139);
            this.lbGSAVDOP.MenuManager = this.barManager1;
            this.lbGSAVDOP.Name = "lbGSAVDOP";
            this.lbGSAVDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAVDOP, true);
            this.lbGSAVDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAVDOP, optionsSpelling21);
            this.lbGSAVDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAVDOP.TabIndex = 9;
            // 
            // lbGSAHDOP
            // 
            this.lbGSAHDOP.Location = new System.Drawing.Point(86, 115);
            this.lbGSAHDOP.MenuManager = this.barManager1;
            this.lbGSAHDOP.Name = "lbGSAHDOP";
            this.lbGSAHDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAHDOP, true);
            this.lbGSAHDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAHDOP, optionsSpelling22);
            this.lbGSAHDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAHDOP.TabIndex = 8;
            // 
            // lbGSAPDOP
            // 
            this.lbGSAPDOP.Location = new System.Drawing.Point(86, 91);
            this.lbGSAPDOP.MenuManager = this.barManager1;
            this.lbGSAPDOP.Name = "lbGSAPDOP";
            this.lbGSAPDOP.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAPDOP, true);
            this.lbGSAPDOP.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAPDOP, optionsSpelling23);
            this.lbGSAPDOP.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAPDOP.TabIndex = 7;
            // 
            // lbGSAPRNs
            // 
            this.lbGSAPRNs.Location = new System.Drawing.Point(86, 67);
            this.lbGSAPRNs.MenuManager = this.barManager1;
            this.lbGSAPRNs.Name = "lbGSAPRNs";
            this.lbGSAPRNs.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAPRNs, true);
            this.lbGSAPRNs.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAPRNs, optionsSpelling24);
            this.lbGSAPRNs.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAPRNs.TabIndex = 6;
            // 
            // lbGSAFixMode
            // 
            this.lbGSAFixMode.Location = new System.Drawing.Point(86, 43);
            this.lbGSAFixMode.MenuManager = this.barManager1;
            this.lbGSAFixMode.Name = "lbGSAFixMode";
            this.lbGSAFixMode.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAFixMode, true);
            this.lbGSAFixMode.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAFixMode, optionsSpelling25);
            this.lbGSAFixMode.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAFixMode.TabIndex = 5;
            // 
            // lbGSAMode
            // 
            this.lbGSAMode.Location = new System.Drawing.Point(86, 19);
            this.lbGSAMode.MenuManager = this.barManager1;
            this.lbGSAMode.Name = "lbGSAMode";
            this.lbGSAMode.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.lbGSAMode, true);
            this.lbGSAMode.Size = new System.Drawing.Size(242, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.lbGSAMode, optionsSpelling26);
            this.lbGSAMode.StyleController = this.layoutControl_GPS_Page5;
            this.lbGSAMode.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(2, 2);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(161, 13);
            this.label32.StyleController = this.layoutControl_GPS_Page5;
            this.label32.TabIndex = 3;
            this.label32.Text = "GPS DOP and Active Satellite";
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "layoutControlGroup11";
            this.layoutControlGroup11.GroupBordersVisible = false;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem27,
            this.layoutControlItem40,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(330, 573);
            this.layoutControlGroup11.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.label32;
            this.layoutControlItem27.CustomizationFormText = "Title:";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(330, 17);
            this.layoutControlItem27.Text = "Title:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.lbGSAMode;
            this.layoutControlItem40.CustomizationFormText = "Mode:";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem40.Text = "Mode:";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.lbGSAFixMode;
            this.layoutControlItem41.CustomizationFormText = "Fix Mode:";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem41.Text = "Fix Mode:";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.lbGSAPRNs;
            this.layoutControlItem42.CustomizationFormText = "PRNs in Solution:";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem42.Text = "PRNs in Solution:";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.lbGSAPDOP;
            this.layoutControlItem43.CustomizationFormText = "PDOP:";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem43.Text = "PDOP:";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.lbGSAHDOP;
            this.layoutControlItem44.CustomizationFormText = "HDOP:";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem44.Text = "HDOP:";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(81, 13);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.lbGSAVDOP;
            this.layoutControlItem45.CustomizationFormText = "VDOP:";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 137);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(330, 436);
            this.layoutControlItem45.Text = "VDOP:";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(81, 13);
            // 
            // tabGPGSV
            // 
            this.tabGPGSV.Controls.Add(this.picGSVSignals);
            this.tabGPGSV.Controls.Add(this.picGSVSkyview);
            this.tabGPGSV.Controls.Add(this.label33);
            this.tabGPGSV.Name = "tabGPGSV";
            this.tabGPGSV.Size = new System.Drawing.Size(330, 573);
            this.tabGPGSV.Text = "GPGSV";
            // 
            // picGSVSignals
            // 
            this.picGSVSignals.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picGSVSignals.Location = new System.Drawing.Point(2, 194);
            this.picGSVSignals.Name = "picGSVSignals";
            this.picGSVSignals.Size = new System.Drawing.Size(326, 377);
            this.picGSVSignals.TabIndex = 6;
            this.picGSVSignals.TabStop = false;
            // 
            // picGSVSkyview
            // 
            this.picGSVSkyview.Location = new System.Drawing.Point(2, 18);
            this.picGSVSkyview.Name = "picGSVSkyview";
            this.picGSVSkyview.Size = new System.Drawing.Size(170, 170);
            this.picGSVSkyview.TabIndex = 5;
            this.picGSVSkyview.TabStop = false;
            // 
            // label33
            // 
            this.label33.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(3, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(95, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Satellites in View";
            // 
            // tabRaw
            // 
            this.tabRaw.Controls.Add(this.tbRawLog);
            this.tabRaw.Name = "tabRaw";
            this.tabRaw.Size = new System.Drawing.Size(330, 573);
            this.tabRaw.Text = "Raw";
            // 
            // tbRawLog
            // 
            this.tbRawLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRawLog.Location = new System.Drawing.Point(0, 0);
            this.tbRawLog.MenuManager = this.barManager1;
            this.tbRawLog.Name = "tbRawLog";
            this.tbRawLog.Properties.BeforeShowMenu += new DevExpress.XtraEditors.Controls.BeforeShowMenuEventHandler(this.tbRawLog_Properties_BeforeShowMenu);
            this.scSpellChecker.SetShowSpellCheckMenu(this.tbRawLog, true);
            this.tbRawLog.Size = new System.Drawing.Size(330, 573);
            this.scSpellChecker.SetSpellCheckerOptions(this.tbRawLog, optionsSpelling27);
            this.tbRawLog.TabIndex = 7;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList5
            // 
            this.imageList5.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList5.ImageStream")));
            this.imageList5.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList5.Images.SetKeyName(0, "add_16.png");
            this.imageList5.Images.SetKeyName(1, "edit_16.png");
            this.imageList5.Images.SetKeyName(2, "delete_16.png");
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "symbol_circle_16.png");
            this.imageList1.Images.SetKeyName(1, "symbol_square_16.png");
            this.imageList1.Images.SetKeyName(2, "symbol_diamond_16.png");
            this.imageList1.Images.SetKeyName(3, "symbol_triangle1_16.png");
            this.imageList1.Images.SetKeyName(4, "symbol_triangle2_16.png");
            this.imageList1.Images.SetKeyName(5, "symbol_star_16.png");
            this.imageList1.Images.SetKeyName(6, "symbol_hollow_circle_16.png");
            this.imageList1.Images.SetKeyName(7, "symbol_hollow_square_16.png");
            this.imageList1.Images.SetKeyName(8, "symbol_hollow_diamond_16.png");
            this.imageList1.Images.SetKeyName(9, "symbol_hollow_triangle1_16.png");
            this.imageList1.Images.SetKeyName(10, "symbol_hollow_triangle2_16.png");
            this.imageList1.Images.SetKeyName(11, "symbol_hollow_star_16.png");
            // 
            // imageList4
            // 
            this.imageList4.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList4.ImageStream")));
            this.imageList4.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList4.Images.SetKeyName(0, "pattern_02.png");
            this.imageList4.Images.SetKeyName(1, "pattern_03.png");
            this.imageList4.Images.SetKeyName(2, "pattern_04.png");
            this.imageList4.Images.SetKeyName(3, "pattern_06.png");
            this.imageList4.Images.SetKeyName(4, "pattern_08.png");
            this.imageList4.Images.SetKeyName(5, "pattern_14.png");
            this.imageList4.Images.SetKeyName(6, "pattern_17.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "Border_01.png");
            this.imageList3.Images.SetKeyName(1, "Border_02.png");
            this.imageList3.Images.SetKeyName(2, "Border_03.png");
            this.imageList3.Images.SetKeyName(3, "Border_04.png");
            this.imageList3.Images.SetKeyName(4, "Border_05.png");
            this.imageList3.Images.SetKeyName(5, "Border_06.png");
            this.imageList3.Images.SetKeyName(6, "Border_07.png");
            // 
            // imageListLineStyles
            // 
            this.imageListLineStyles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStyles.ImageStream")));
            this.imageListLineStyles.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStyles.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStyles.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStyles.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStyles.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStyles.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStyles.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStyles.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStyles.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStyles.Images.SetKeyName(8, "line_93.png");
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.popupContainerControl2);
            this.panelControl1.Controls.Add(this.mapControl1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(340, 70);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(1016, 650);
            this.panelControl1.TabIndex = 26;
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 48;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // beiEditMode
            // 
            this.beiEditMode.Caption = "Edit Mode";
            this.beiEditMode.Edit = this.repositoryItemLookUpEdit2;
            this.beiEditMode.Id = 49;
            this.beiEditMode.Name = "beiEditMode";
            // 
            // bsiEditMode
            // 
            this.bsiEditMode.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.bsiEditMode.Caption = "Edit Mode: Move and Resize";
            this.bsiEditMode.Id = 55;
            this.bsiEditMode.Name = "bsiEditMode";
            this.bsiEditMode.TextAlignment = System.Drawing.StringAlignment.Near;
            this.bsiEditMode.Width = 150;
            // 
            // bar3
            // 
            this.bar3.BarName = "EditMode";
            this.bar3.DockCol = 3;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingNone),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingMove),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciEditingEdit)});
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.Text = "Map Object Edit Mode";
            // 
            // bar4
            // 
            this.bar4.BarName = "MapOutput";
            this.bar4.DockCol = 6;
            this.bar4.DockRow = 0;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.FloatLocation = new System.Drawing.Point(1426, 204);
            this.bar4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiWorkOrderMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrint)});
            this.bar4.OptionsBar.DisableClose = true;
            this.bar4.OptionsBar.DrawDragBorder = false;
            this.bar4.Text = "Map Output";
            // 
            // bbiWorkOrderMap
            // 
            this.bbiWorkOrderMap.Caption = "WO Map";
            this.bbiWorkOrderMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiWorkOrderMap.Glyph")));
            this.bbiWorkOrderMap.Id = 82;
            this.bbiWorkOrderMap.Name = "bbiWorkOrderMap";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem7.Image")));
            toolTipTitleItem7.Text = "Map Shanpshots - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to open the Map Snapshots screen.\r\n\r\nThe Map Snapshots screen is used to" +
    " create and link saved maps to Work Orders and Permission Documents.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiWorkOrderMap.SuperTip = superToolTip7;
            this.bbiWorkOrderMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiWorkOrderMap_ItemClick);
            // 
            // bbiPrint
            // 
            this.bbiPrint.Caption = "Print Map";
            this.bbiPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrint.Glyph")));
            this.bbiPrint.Id = 73;
            this.bbiPrint.Name = "bbiPrint";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem8.Image")));
            toolTipTitleItem8.Text = "Print Map - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to open the Print Map screen.\r\n";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiPrint.SuperTip = superToolTip8;
            this.bbiPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrint_ItemClick);
            // 
            // bbiSaveImage
            // 
            this.bbiSaveImage.Caption = "Save Map as Image";
            this.bbiSaveImage.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ExportFileLarge;
            this.bbiSaveImage.Id = 56;
            this.bbiSaveImage.Name = "bbiSaveImage";
            superToolTip41.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem41.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem41.Appearance.Options.UseImage = true;
            toolTipTitleItem41.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem41.Text = "Export Loaded Map Objects to TAB File - Information";
            toolTipItem41.LeftIndent = 6;
            superToolTip41.Items.Add(toolTipTitleItem41);
            superToolTip41.Items.Add(toolTipItem41);
            this.bbiSaveImage.SuperTip = superToolTip41;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiViewOnMap),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHighlightYes, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiHighlightNo),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCopyHighlightFromVisible),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVisibleYes, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiVisibleNo)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.MenuCaption = "Map Objects Menu";
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.ShowCaption = true;
            // 
            // bbiViewOnMap
            // 
            this.bbiViewOnMap.Caption = "View Object on Map";
            this.bbiViewOnMap.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.bbiViewOnMap.Id = 59;
            this.bbiViewOnMap.Name = "bbiViewOnMap";
            superToolTip42.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem42.Text = "View on Map - Information";
            toolTipItem42.LeftIndent = 6;
            toolTipItem42.Text = "Click me to view the current object on the map.\r\n\r\nIf the object is not within th" +
    "e viewed area of the map, the map will automatically pan to show the object.";
            superToolTip42.Items.Add(toolTipTitleItem42);
            superToolTip42.Items.Add(toolTipItem42);
            this.bbiViewOnMap.SuperTip = superToolTip42;
            this.bbiViewOnMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiViewOnMap_ItemClick);
            // 
            // bbiHighlightYes
            // 
            this.bbiHighlightYes.Caption = "Highlight Selected Objects";
            this.bbiHighlightYes.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiHighlightYes.Glyph")));
            this.bbiHighlightYes.Id = 68;
            this.bbiHighlightYes.Name = "bbiHighlightYes";
            this.bbiHighlightYes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHighlightYes_ItemClick);
            // 
            // bbiHighlightNo
            // 
            this.bbiHighlightNo.Caption = "Un-Highlight Selected Objects";
            this.bbiHighlightNo.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiHighlightNo.Glyph")));
            this.bbiHighlightNo.Id = 69;
            this.bbiHighlightNo.Name = "bbiHighlightNo";
            this.bbiHighlightNo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHighlightNo_ItemClick);
            // 
            // bbiCopyHighlightFromVisible
            // 
            this.bbiCopyHighlightFromVisible.Caption = "Copy Highlight from Visible Settings";
            this.bbiCopyHighlightFromVisible.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCopyHighlightFromVisible.Glyph")));
            this.bbiCopyHighlightFromVisible.Id = 70;
            this.bbiCopyHighlightFromVisible.Name = "bbiCopyHighlightFromVisible";
            superToolTip43.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem43.Text = "Copy Highlight from Visible Settings - Information";
            toolTipItem43.LeftIndent = 6;
            toolTipItem43.Text = "Click me to <b>Copy</b> the Map Object <b>Visibility</b> settings to the Map Obje" +
    "ct <b>Highlight</b> settings.";
            superToolTip43.Items.Add(toolTipTitleItem43);
            superToolTip43.Items.Add(toolTipItem43);
            this.bbiCopyHighlightFromVisible.SuperTip = superToolTip43;
            this.bbiCopyHighlightFromVisible.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCopyHighlightFromVisible_ItemClick);
            // 
            // bbiVisibleYes
            // 
            this.bbiVisibleYes.Caption = "Set Selected Objects Visible";
            this.bbiVisibleYes.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiVisibleYes.Glyph")));
            this.bbiVisibleYes.Id = 71;
            this.bbiVisibleYes.Name = "bbiVisibleYes";
            this.bbiVisibleYes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisibleYes_ItemClick);
            // 
            // bbiVisibleNo
            // 
            this.bbiVisibleNo.Caption = "Set Selected Objects Hidden";
            this.bbiVisibleNo.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiVisibleNo.Glyph")));
            this.bbiVisibleNo.Id = 72;
            this.bbiVisibleNo.Name = "bbiVisibleNo";
            this.bbiVisibleNo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiVisibleNo_ItemClick);
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp01254_AT_Tree_Picker_scale_listTableAdapter
            // 
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.labelControl1);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup9;
            this.layoutControl5.Size = new System.Drawing.Size(272, 212);
            this.layoutControl5.TabIndex = 2;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(2, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(195, 13);
            this.labelControl1.StyleController = this.layoutControl5;
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Global Positioning System Fix Data";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup9.GroupBordersVisible = false;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem25});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup8";
            this.layoutControlGroup9.Size = new System.Drawing.Size(272, 212);
            this.layoutControlGroup9.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.labelControl1;
            this.layoutControlItem25.CustomizationFormText = "Title:";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem25.Name = "layoutControlItem24";
            this.layoutControlItem25.Size = new System.Drawing.Size(272, 212);
            this.layoutControlItem25.Text = "Title:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // gridLookUpEdit3View
            // 
            this.gridLookUpEdit3View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35});
            this.gridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit3View.GroupCount = 1;
            this.gridLookUpEdit3View.Name = "gridLookUpEdit3View";
            this.gridLookUpEdit3View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit3View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit3View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit3View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit3View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit3View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit3View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.FieldName = "ColumnID";
            this.gridColumn17.Name = "gridColumn17";
            // 
            // gridColumn18
            // 
            this.gridColumn18.FieldName = "InternalColumnName";
            this.gridColumn18.Name = "gridColumn18";
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "ExternalColumnName";
            this.gridColumn19.Name = "gridColumn19";
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Style Field  [Screen Label]";
            this.gridColumn20.FieldName = "ColumnLabel";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 277;
            // 
            // gridColumn21
            // 
            this.gridColumn21.FieldName = "ColumnType";
            this.gridColumn21.Name = "gridColumn21";
            // 
            // gridColumn22
            // 
            this.gridColumn22.FieldName = "ColumnLength";
            this.gridColumn22.Name = "gridColumn22";
            // 
            // gridColumn23
            // 
            this.gridColumn23.FieldName = "GroupName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 1;
            this.gridColumn23.Width = 127;
            // 
            // gridColumn24
            // 
            this.gridColumn24.FieldName = "TableName";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn25
            // 
            this.gridColumn25.FieldName = "ColumnOrder";
            this.gridColumn25.Name = "gridColumn25";
            // 
            // gridColumn26
            // 
            this.gridColumn26.FieldName = "PickList";
            this.gridColumn26.Name = "gridColumn26";
            // 
            // gridColumn27
            // 
            this.gridColumn27.FieldName = "PicklistSummery";
            this.gridColumn27.Name = "gridColumn27";
            // 
            // gridColumn28
            // 
            this.gridColumn28.FieldName = "Remarks";
            this.gridColumn28.Name = "gridColumn28";
            // 
            // gridColumn29
            // 
            this.gridColumn29.FieldName = "Required";
            this.gridColumn29.Name = "gridColumn29";
            // 
            // gridColumn30
            // 
            this.gridColumn30.FieldName = "GroupOrder";
            this.gridColumn30.Name = "gridColumn30";
            // 
            // gridColumn31
            // 
            this.gridColumn31.FieldName = "Editable";
            this.gridColumn31.Name = "gridColumn31";
            // 
            // gridColumn32
            // 
            this.gridColumn32.FieldName = "Searchable";
            this.gridColumn32.Name = "gridColumn32";
            // 
            // gridColumn33
            // 
            this.gridColumn33.FieldName = "Manditory";
            this.gridColumn33.Name = "gridColumn33";
            // 
            // gridColumn34
            // 
            this.gridColumn34.FieldName = "UseLastRecordAvailable";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // gridColumn35
            // 
            this.gridColumn35.FieldName = "UseLastRecord";
            this.gridColumn35.Name = "gridColumn35";
            // 
            // popupMenu_ThematicGrid
            // 
            this.popupMenu_ThematicGrid.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditThematicStyles),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSetAs)});
            this.popupMenu_ThematicGrid.Manager = this.barManager1;
            this.popupMenu_ThematicGrid.Name = "popupMenu_ThematicGrid";
            // 
            // bbiBlockEditThematicStyles
            // 
            this.bbiBlockEditThematicStyles.Caption = "Block Edit Thematic Styles";
            this.bbiBlockEditThematicStyles.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditThematicStyles.Glyph")));
            this.bbiBlockEditThematicStyles.Id = 78;
            this.bbiBlockEditThematicStyles.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditThematicStyles.LargeGlyph")));
            this.bbiBlockEditThematicStyles.Name = "bbiBlockEditThematicStyles";
            // 
            // bbiSaveThematicSet
            // 
            this.bbiSaveThematicSet.Caption = "Save Thematic Set";
            this.bbiSaveThematicSet.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSet.Glyph")));
            this.bbiSaveThematicSet.Id = 79;
            this.bbiSaveThematicSet.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSet.LargeGlyph")));
            this.bbiSaveThematicSet.Name = "bbiSaveThematicSet";
            // 
            // bbiSaveThematicSetAs
            // 
            this.bbiSaveThematicSetAs.Caption = "Save Thematic Set As";
            this.bbiSaveThematicSetAs.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSetAs.Glyph")));
            this.bbiSaveThematicSetAs.Id = 80;
            this.bbiSaveThematicSetAs.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSetAs.LargeGlyph")));
            this.bbiSaveThematicSetAs.Name = "bbiSaveThematicSetAs";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            this.imageList2.Images.SetKeyName(6, "");
            this.imageList2.Images.SetKeyName(7, "");
            this.imageList2.Images.SetKeyName(8, "");
            this.imageList2.Images.SetKeyName(9, "");
            this.imageList2.Images.SetKeyName(10, "");
            this.imageList2.Images.SetKeyName(11, "");
            this.imageList2.Images.SetKeyName(12, "");
            this.imageList2.Images.SetKeyName(13, "");
            // 
            // popupMenu_LayerManager
            // 
            this.popupMenu_LayerManager.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRemoveLayer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLayerManagerProperties, true)});
            this.popupMenu_LayerManager.Manager = this.barManager1;
            this.popupMenu_LayerManager.MenuCaption = "Layer Manager Menu";
            this.popupMenu_LayerManager.Name = "popupMenu_LayerManager";
            this.popupMenu_LayerManager.ShowCaption = true;
            // 
            // bbiAddLayer
            // 
            this.bbiAddLayer.Caption = "Add Layer";
            this.bbiAddLayer.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddLayer.Glyph")));
            this.bbiAddLayer.Id = 83;
            this.bbiAddLayer.Name = "bbiAddLayer";
            toolTipTitleItem45.Text = "Add Layer - Information";
            toolTipItem45.LeftIndent = 6;
            toolTipItem45.Text = "Add a layer temporarily to the map.\r\n\r\n<b>Tip:</b> To add the layer permanently, " +
    "edit the Map Workspace and add it there.";
            superToolTip45.Items.Add(toolTipTitleItem45);
            superToolTip45.Items.Add(toolTipItem45);
            this.bbiAddLayer.SuperTip = superToolTip45;
            this.bbiAddLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddLayer_ItemClick);
            // 
            // bbiRemoveLayer
            // 
            this.bbiRemoveLayer.Caption = "Remove Layer";
            this.bbiRemoveLayer.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRemoveLayer.Glyph")));
            this.bbiRemoveLayer.Id = 84;
            this.bbiRemoveLayer.Name = "bbiRemoveLayer";
            toolTipTitleItem46.Text = "Remove Layer - Information";
            toolTipItem46.LeftIndent = 6;
            toolTipItem46.Text = "Remove a layer temporarily from the map.\r\n\r\n<b>Tip:</b> To remove the layer perma" +
    "nently, edit the Map Workspace and remove it from there.";
            superToolTip46.Items.Add(toolTipTitleItem46);
            superToolTip46.Items.Add(toolTipItem46);
            this.bbiRemoveLayer.SuperTip = superToolTip46;
            this.bbiRemoveLayer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRemoveLayer_ItemClick);
            // 
            // bbiLayerManagerProperties
            // 
            this.bbiLayerManagerProperties.Caption = "Properties";
            this.bbiLayerManagerProperties.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLayerManagerProperties.Glyph")));
            this.bbiLayerManagerProperties.Id = 81;
            this.bbiLayerManagerProperties.Name = "bbiLayerManagerProperties";
            toolTipTitleItem44.Text = "Layer Properties - Information";
            toolTipItem44.LeftIndent = 6;
            superToolTip44.Items.Add(toolTipTitleItem44);
            superToolTip44.Items.Add(toolTipItem44);
            this.bbiLayerManagerProperties.SuperTip = superToolTip44;
            this.bbiLayerManagerProperties.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLayerManagerProperties_ItemClick);
            // 
            // sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter
            // 
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.ClearBeforeFill = true;
            // 
            // sp01274ATTreePickerWorkspaceEditBindingSource
            // 
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataMember = "sp01274_AT_Tree_Picker_Workspace_Edit";
            this.sp01274ATTreePickerWorkspaceEditBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // sp01274_AT_Tree_Picker_Workspace_EditTableAdapter
            // 
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageListLineStylesLegend
            // 
            this.imageListLineStylesLegend.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStylesLegend.ImageStream")));
            this.imageListLineStylesLegend.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStylesLegend.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStylesLegend.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStylesLegend.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStylesLegend.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStylesLegend.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStylesLegend.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStylesLegend.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStylesLegend.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStylesLegend.Images.SetKeyName(8, "line_93.png");
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 6";
            this.bar5.DockCol = 5;
            this.bar5.DockRow = 0;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciSurvey),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciGazetteer),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLegend),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciScaleBar)});
            this.bar5.OptionsBar.DisableClose = true;
            this.bar5.OptionsBar.DrawDragBorder = false;
            this.bar5.Text = "Map Adornments";
            // 
            // bciSurvey
            // 
            this.bciSurvey.Caption = "Survey";
            this.bciSurvey.Glyph = ((System.Drawing.Image)(resources.GetObject("bciSurvey.Glyph")));
            this.bciSurvey.Id = 146;
            this.bciSurvey.Name = "bciSurvey";
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem9.Image")));
            toolTipTitleItem9.Text = "Survey - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "Click me to Show \\ Hide the Survey Panel. \r\n\r\nThe Survey Panel is used to control" +
    " the map from a selected survey.";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.bciSurvey.SuperTip = superToolTip9;
            this.bciSurvey.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciSurvey_CheckedChanged);
            // 
            // bciGazetteer
            // 
            this.bciGazetteer.Caption = "Gazetteer";
            this.bciGazetteer.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_FindLarge;
            this.bciGazetteer.Id = 92;
            this.bciGazetteer.Name = "bciGazetteer";
            superToolTip10.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem10.Image")));
            toolTipTitleItem10.Text = "Gazetteer - Information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to Show \\ Hide the Gazetteer. \r\n\r\nThe Gazetteer is used to search for Lo" +
    "calities and Street Addresses on the Map.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.bciGazetteer.SuperTip = superToolTip10;
            this.bciGazetteer.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciGazetteer_CheckedChanged);
            // 
            // bbiLegend
            // 
            this.bbiLegend.Caption = "Legend";
            this.bbiLegend.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLegend.Glyph")));
            this.bbiLegend.Id = 88;
            this.bbiLegend.Name = "bbiLegend";
            superToolTip11.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem11.Image")));
            toolTipTitleItem11.Text = "Legend - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to Show \\ Hide the Map Legend.\r\n";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.bbiLegend.SuperTip = superToolTip11;
            this.bbiLegend.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLegend_CheckedChanged);
            // 
            // bciScaleBar
            // 
            this.bciScaleBar.Caption = "Scale Bar";
            this.bciScaleBar.Glyph = ((System.Drawing.Image)(resources.GetObject("bciScaleBar.Glyph")));
            this.bciScaleBar.Id = 89;
            this.bciScaleBar.Name = "bciScaleBar";
            superToolTip12.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem12.Image")));
            toolTipTitleItem12.Text = "Map Scale Bar - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to Show \\ Hide the Map Scale Bar.\r\n";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.bciScaleBar.SuperTip = superToolTip12;
            this.bciScaleBar.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciScaleBar_CheckedChanged);
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter
            // 
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter
            // 
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // bbiGPSSaveLog
            // 
            this.bbiGPSSaveLog.Caption = "Save Log...";
            this.bbiGPSSaveLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGPSSaveLog.Glyph")));
            this.bbiGPSSaveLog.Id = 99;
            this.bbiGPSSaveLog.Name = "bbiGPSSaveLog";
            this.bbiGPSSaveLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGPSSaveLog_ItemClick);
            // 
            // bbiGPSClearLog
            // 
            this.bbiGPSClearLog.Caption = "Clear Log";
            this.bbiGPSClearLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiGPSClearLog.Glyph")));
            this.bbiGPSClearLog.Id = 100;
            this.bbiGPSClearLog.Name = "bbiGPSClearLog";
            this.bbiGPSClearLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGPSClearLog_ItemClick);
            // 
            // imageListGPSLogMenu
            // 
            this.imageListGPSLogMenu.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListGPSLogMenu.ImageStream")));
            this.imageListGPSLogMenu.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListGPSLogMenu.Images.SetKeyName(0, "save_as_32.png");
            this.imageListGPSLogMenu.Images.SetKeyName(1, "delete_32.png");
            // 
            // popupMenu_Gazetteer
            // 
            this.popupMenu_Gazetteer.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerShowMatch),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiGazetteerLoadObjectsWithinRange, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerLoadMapObjectsInRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiGazetteerClearMapSearch, true)});
            this.popupMenu_Gazetteer.Manager = this.barManager1;
            this.popupMenu_Gazetteer.MenuCaption = "Gazetteer Menu";
            this.popupMenu_Gazetteer.Name = "popupMenu_Gazetteer";
            this.popupMenu_Gazetteer.ShowCaption = true;
            // 
            // bbiGazetteerShowMatch
            // 
            this.bbiGazetteerShowMatch.Caption = "Show Selected Match on Map";
            this.bbiGazetteerShowMatch.Glyph = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_ScaleLarge;
            this.bbiGazetteerShowMatch.Id = 101;
            this.bbiGazetteerShowMatch.Name = "bbiGazetteerShowMatch";
            this.bbiGazetteerShowMatch.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerShowMatch_ItemClick);
            // 
            // bbiGazetteerLoadMapObjectsInRange
            // 
            this.bbiGazetteerLoadMapObjectsInRange.Caption = "Load Map Objects in Range";
            this.bbiGazetteerLoadMapObjectsInRange.Id = 105;
            this.bbiGazetteerLoadMapObjectsInRange.Name = "bbiGazetteerLoadMapObjectsInRange";
            this.bbiGazetteerLoadMapObjectsInRange.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGazetteerLoadMapObjectsInRange_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Load Map Objects Within";
            this.barEditItem1.Edit = this.repositoryItemPopupContainerEdit3;
            this.barEditItem1.Id = 104;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            // 
            // popupStoredMapViews
            // 
            this.popupStoredMapViews.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bciStoreViewChanges),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiGoToMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStoreCurrentMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearAllMapViews, true)});
            this.popupStoredMapViews.Manager = this.barManager1;
            this.popupStoredMapViews.MenuCaption = "Map Views Menu";
            this.popupStoredMapViews.Name = "popupStoredMapViews";
            this.popupStoredMapViews.ShowCaption = true;
            this.popupStoredMapViews.Popup += new System.EventHandler(this.popupStoredMapViews_Popup);
            // 
            // bciStoreViewChanges
            // 
            this.bciStoreViewChanges.BindableChecked = true;
            this.bciStoreViewChanges.Caption = "Store Map View Changes";
            this.bciStoreViewChanges.Checked = true;
            this.bciStoreViewChanges.Id = 106;
            this.bciStoreViewChanges.Name = "bciStoreViewChanges";
            this.bciStoreViewChanges.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciStoreViewChanges_CheckedChanged);
            // 
            // bsiGoToMapView
            // 
            this.bsiGoToMapView.Caption = "Go To Map View";
            this.bsiGoToMapView.Id = 110;
            this.bsiGoToMapView.Name = "bsiGoToMapView";
            // 
            // bbiStoreCurrentMapView
            // 
            this.bbiStoreCurrentMapView.Caption = "Store Current Map View";
            this.bbiStoreCurrentMapView.Id = 108;
            this.bbiStoreCurrentMapView.Name = "bbiStoreCurrentMapView";
            this.bbiStoreCurrentMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStoreCurrentMapView_ItemClick);
            // 
            // bbiClearAllMapViews
            // 
            this.bbiClearAllMapViews.Caption = "Clear All Stored Map Views";
            this.bbiClearAllMapViews.Id = 107;
            this.bbiClearAllMapViews.Name = "bbiClearAllMapViews";
            this.bbiClearAllMapViews.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearAllMapViews_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 109;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bar6
            // 
            this.bar6.BarName = "MapView";
            this.bar6.DockCol = 0;
            this.bar6.DockRow = 0;
            this.bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar6.FloatLocation = new System.Drawing.Point(348, 184);
            this.bar6.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiStoredMapViews),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLastMapView, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiNextMapView),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiZoomIn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiZoomOut),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPan, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCentre, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiNone, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.bar6.OptionsBar.DisableClose = true;
            this.bar6.OptionsBar.DrawDragBorder = false;
            this.bar6.Text = "Map View";
            // 
            // bbiStoredMapViews
            // 
            this.bbiStoredMapViews.ActAsDropDown = true;
            this.bbiStoredMapViews.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiStoredMapViews.Caption = "Stored Views";
            this.bbiStoredMapViews.DropDownControl = this.popupStoredMapViews;
            this.bbiStoredMapViews.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStoredMapViews.Glyph")));
            this.bbiStoredMapViews.Id = 115;
            this.bbiStoredMapViews.Name = "bbiStoredMapViews";
            superToolTip13.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem13.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem13.Appearance.Options.UseImage = true;
            toolTipTitleItem13.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem13.Image")));
            toolTipTitleItem13.Text = "Stored Views - Information";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "Click me to display the Stored Views Menu.";
            superToolTip13.Items.Add(toolTipTitleItem13);
            superToolTip13.Items.Add(toolTipItem13);
            this.bbiStoredMapViews.SuperTip = superToolTip13;
            // 
            // bbiLastMapView
            // 
            this.bbiLastMapView.Caption = "Previous View";
            this.bbiLastMapView.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLastMapView.Glyph")));
            this.bbiLastMapView.Id = 111;
            this.bbiLastMapView.Name = "bbiLastMapView";
            superToolTip14.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem14.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipTitleItem14.Appearance.Options.UseImage = true;
            toolTipTitleItem14.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem14.Image")));
            toolTipTitleItem14.Text = "Previous Map View - Information";
            toolTipItem14.LeftIndent = 6;
            toolTipItem14.Text = "Click me to navigate to the previous stored Map view.";
            superToolTip14.Items.Add(toolTipTitleItem14);
            superToolTip14.Items.Add(toolTipItem14);
            this.bbiLastMapView.SuperTip = superToolTip14;
            this.bbiLastMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLastMapView_ItemClick);
            // 
            // bbiNextMapView
            // 
            this.bbiNextMapView.Caption = "Next View";
            this.bbiNextMapView.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiNextMapView.Glyph")));
            this.bbiNextMapView.Id = 113;
            this.bbiNextMapView.Name = "bbiNextMapView";
            superToolTip15.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem15.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            toolTipTitleItem15.Appearance.Options.UseImage = true;
            toolTipTitleItem15.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem15.Image")));
            toolTipTitleItem15.Text = "Next Map View - Information";
            toolTipItem15.LeftIndent = 6;
            toolTipItem15.Text = "Click me to navigate to the next stored Map view.";
            superToolTip15.Items.Add(toolTipTitleItem15);
            superToolTip15.Items.Add(toolTipItem15);
            this.bbiNextMapView.SuperTip = superToolTip15;
            this.bbiNextMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNextMapView_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "Edit...";
            this.bbiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEdit.Glyph")));
            this.bbiEdit.Id = 123;
            this.bbiEdit.Name = "bbiEdit";
            // 
            // bsiEditObjects
            // 
            this.bsiEditObjects.Caption = "Edit...";
            this.bsiEditObjects.Id = 124;
            this.bsiEditObjects.Name = "bsiEditObjects";
            // 
            // barCheckItem2
            // 
            this.barCheckItem2.Caption = "barCheckItem2";
            this.barCheckItem2.Id = 137;
            this.barCheckItem2.Name = "barCheckItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Clear Search Results";
            this.barButtonItem3.Id = 138;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.View = this.repositoryItemGridLookUpEdit1View;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // bar7
            // 
            this.bar7.BarName = "Plotting";
            this.bar7.DockCol = 1;
            this.bar7.DockRow = 0;
            this.bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar7.FloatLocation = new System.Drawing.Point(783, 209);
            this.bar7.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiPlotObjectType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPoint),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolygon),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPolyLine)});
            this.bar7.OptionsBar.DisableClose = true;
            this.bar7.OptionsBar.DrawDragBorder = false;
            this.bar7.Text = "Plotting";
            // 
            // bar8
            // 
            this.bar8.BarName = "ObjectSelection";
            this.bar8.DockCol = 2;
            this.bar8.DockRow = 0;
            this.bar8.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar8.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSelect, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRectangle),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRadius),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectRegion),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSelectPolygon)});
            this.bar8.OptionsBar.DisableClose = true;
            this.bar8.OptionsBar.DrawDragBorder = false;
            this.bar8.Text = "Object Selection";
            // 
            // bar9
            // 
            this.bar9.BarName = "MapTools";
            this.bar9.DockCol = 4;
            this.bar9.DockRow = 0;
            this.bar9.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar9.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMapSelection),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiQueryTool, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMeasureLine, true)});
            this.bar9.OptionsBar.DisableClose = true;
            this.bar9.OptionsBar.DrawDragBorder = false;
            this.bar9.Text = "Map Tools";
            // 
            // sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter
            // 
            this.sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Asset Type Description";
            this.gridColumn46.FieldName = "AssetTypeDescription";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 0;
            this.gridColumn46.Width = 291;
            // 
            // sp07049_UT_Tree_Picker_PolesTableAdapter
            // 
            this.sp07049_UT_Tree_Picker_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07053_UT_Tree_Picker_TreesTableAdapter
            // 
            this.sp07053_UT_Tree_Picker_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // colorEdit1
            // 
            this.colorEdit1.EditValue = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.colorEdit1.Location = new System.Drawing.Point(5, 116);
            this.colorEdit1.MenuManager = this.barManager1;
            this.colorEdit1.Name = "colorEdit1";
            this.colorEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.colorEdit1.Size = new System.Drawing.Size(43, 20);
            this.colorEdit1.TabIndex = 36;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // frm_UT_Mapping
            // 
            this.ClientSize = new System.Drawing.Size(1356, 748);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.mapToolBar1);
            this.Controls.Add(this.panelContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Mapping";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Utilities - Mapping";
            this.Activated += new System.EventHandler(this.frm_UT_Mapping_Activated);
            this.Deactivate += new System.EventHandler(this.frm_UT_Mapping_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Mapping_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_UT_Mapping_FormClosed);
            this.Load += new System.EventHandler(this.frm_UT_Mapping_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.frm_UT_Mapping_MouseClick);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.panelContainer1, 0);
            this.Controls.SetChildIndex(this.mapToolBar1, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01312ATTreePickerGazetteerSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_MapControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditGazetteerRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl2)).EndInit();
            this.popupContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.seUserDefinedScale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01254ATTreePickerscalelistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel5.ResumeLayout(false);
            this.dockPanel5_Container.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.xtraScrollableControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLegend)).EndInit();
            this.dockPanelSurvey.ResumeLayout(false);
            this.controlContainer2.ResumeLayout(false);
            this.controlContainer2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPlotTreesAgainstPole.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditSurvey.Properties)).EndInit();
            this.dockPanelGazetteer.ResumeLayout(false);
            this.dockPanel7_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditGazetteerFindValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditGazetteerSearchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01311ATTreePickerGazetteerSearchTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroupGazetteerMatchPattern.Properties)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.controlContainer1.ResumeLayout(false);
            this.dockPanel6.ResumeLayout(false);
            this.dockPanel6_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxScaleBar)).EndInit();
            this.dockPanelLegend.ResumeLayout(false);
            this.controlContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditTreeLegend1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEditTreeStylingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEditPoleStylingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditPoleLegend1.Properties)).EndInit();
            this.panelContainer1.ResumeLayout(false);
            this.dockPanelLayers.ResumeLayout(false);
            this.dockPanel4_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01288ATTreePickerWorkspacelayerslistBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCE_LayerHitable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beWorkspace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            this.dockPanelMapObjects.ResumeLayout(false);
            this.dockPanel2_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07053UTTreePickerTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.csScaleMapForHighlighted.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceCentreMap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07049UTTreePickerPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditHighlight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEditHighlight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEditFilterCircuits.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.dockPanelGPS.ResumeLayout(false);
            this.dockPanel3_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statusBar1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NMEAtabs)).EndInit();
            this.NMEAtabs.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Gps_Page1)).EndInit();
            this.layoutControl_Gps_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceLogRawGPSData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbPortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbBaudRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGps_PlotWithTimer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seGps_PlotTimerInterval.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotLine.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPolygon.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceGPS_PlotPoint.Properties)).EndInit();
            this.tabGPRMC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page2)).EndInit();
            this.layoutControl_GPS_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCGridRef.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPositionUTM.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCMagneticVariation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCTimeOfFix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCCourse.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbRMCPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            this.tabGPGGA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page3)).EndInit();
            this.layoutControl_GPS_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGADGPSupdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAGeoidHeight.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAHDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAAltitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGANoOfSats.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAFixQuality.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGATimeOfFix.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGGAPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            this.tabGPGLL.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page4)).EndInit();
            this.layoutControl_GPS_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLDataValid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLTimeOfSolution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGLLPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            this.tabGPGSA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_GPS_Page5)).EndInit();
            this.layoutControl_GPS_Page5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAVDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAHDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPDOP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAPRNs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAFixMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbGSAMode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            this.tabGPGSV.ResumeLayout(false);
            this.tabGPGSV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSignals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picGSVSkyview)).EndInit();
            this.tabRaw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbRawLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_LayerManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01274ATTreePickerWorkspaceEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_Gazetteer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupStoredMapViews)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MapInfo.Windows.Controls.MapToolBar mapToolBar1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonOpenTable;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonLayerControl;
        private System.Windows.Forms.ToolBarButton toolBarButtonSperator1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonArrow;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonZoomIn;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonZoomOut;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRectangle;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonCenter;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonPan;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelect;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRadius;
        private System.Windows.Forms.ToolBarButton toolBarButtonSeperator2;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectPolygon;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonSelectRegion;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPoint;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolygon;
        private System.Windows.Forms.ToolBarButton toolBarButtonSeperator3;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolygon2;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButtonAddPolyLine;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButton1;
        private DevExpress.XtraBars.BarCheckItem bbiZoomIn;
        private DevExpress.XtraBars.BarCheckItem bbiZoomOut;
        private DevExpress.XtraBars.BarCheckItem bbiPan;
        private DevExpress.XtraBars.BarCheckItem bbiCentre;
        private DevExpress.XtraBars.BarCheckItem bbiSelect;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRectangle;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRadius;
        private DevExpress.XtraBars.BarCheckItem bbiSelectPolygon;
        private DevExpress.XtraBars.BarCheckItem bbiSelectRegion;
        private DevExpress.XtraBars.BarCheckItem bbiAddPoint;
        private DevExpress.XtraBars.BarCheckItem bbiAddPolygon;
        private DevExpress.XtraBars.BarCheckItem bbiAddPolyLine;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem bsiZoom;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarStaticItem bsiScreenCoords;
        private DevExpress.XtraBars.BarStaticItem bsiLayerCount;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private MapInfo.Windows.Controls.MapControl mapControl1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
        private DevExpress.XtraBars.BarStaticItem bsiSnap;
        private DevExpress.XtraBars.BarCheckItem bbiNone;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarCheckItem bciEditingNone;
        private DevExpress.XtraBars.BarCheckItem bciEditingMove;
        private DevExpress.XtraBars.BarEditItem beiEditMode;
        private DevExpress.XtraBars.BarCheckItem bciEditingAdd;
        private DevExpress.XtraBars.BarCheckItem bciEditingEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraBars.BarStaticItem bsiEditMode;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.BarButtonItem bbiSaveImage;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelMapObjects;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.ColorEdit colorEditHighlight;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.CheckEdit ceCentreMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem bbiViewOnMap;
        private DevExpress.XtraEditors.SimpleButton btnViewFullExtent;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraBars.BarEditItem beiPopupContainerScale;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl2;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.SimpleButton btnSetScale;
        private DevExpress.XtraEditors.SpinEdit seUserDefinedScale;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private System.Windows.Forms.BindingSource sp01254ATTreePickerscalelistBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01254_AT_Tree_Picker_scale_listTableAdapter sp01254_AT_Tree_Picker_scale_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colScaleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colScale;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarCheckItem bbiMeasureLine;
        private DevExpress.XtraBars.BarStaticItem bsiDistanceMeasured;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGPS;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel3_Container;
        private DevExpress.XtraTab.XtraTabControl NMEAtabs;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage tabGPRMC;
        private DevExpress.XtraTab.XtraTabPage tabGPGGA;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        public DevExpress.XtraEditors.SimpleButton btnGPS_Start;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotPoint;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotLine;
        private DevExpress.XtraEditors.CheckEdit ceGPS_PlotPolygon;
        private DevExpress.XtraEditors.SimpleButton btnGPS_Plot;
        private DevExpress.XtraEditors.SimpleButton btnGPS_PlotStop;
        private DevExpress.XtraTab.XtraTabPage tabGPGLL;
        private DevExpress.XtraTab.XtraTabPage tabGPGSA;
        private DevExpress.XtraTab.XtraTabPage tabGPGSV;
        private DevExpress.XtraTab.XtraTabPage tabRaw;
        private DevExpress.XtraEditors.LabelControl label5;
        private DevExpress.XtraEditors.LabelControl label12;
        private DevExpress.XtraEditors.LabelControl label30;
        private DevExpress.XtraEditors.LabelControl label32;
        private DevExpress.XtraEditors.LabelControl label33;
        private DevExpress.XtraEditors.MemoEdit tbRawLog;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page2;
        private DevExpress.XtraEditors.TextEdit lbRMCCourse;
        private DevExpress.XtraEditors.TextEdit lbRMCPosition;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit lbRMCPositionUTM;
        private DevExpress.XtraEditors.TextEdit lbRMCMagneticVariation;
        private DevExpress.XtraEditors.TextEdit lbRMCTimeOfFix;
        private DevExpress.XtraEditors.TextEdit lbRMCSpeed;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControl layoutControl_GPS_Page5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.TextEdit lbGGAPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.TextEdit lbGGAHDOP;
        private DevExpress.XtraEditors.TextEdit lbGGAAltitude;
        private DevExpress.XtraEditors.TextEdit lbGGANoOfSats;
        private DevExpress.XtraEditors.TextEdit lbGGAFixQuality;
        private DevExpress.XtraEditors.TextEdit lbGGATimeOfFix;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.TextEdit lbGGAGeoidHeight;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.TextEdit lbGGADGPSID;
        private DevExpress.XtraEditors.TextEdit lbGGADGPSupdate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit lbGLLPosition;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraEditors.TextEdit lbGLLTimeOfSolution;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.TextEdit lbGLLDataValid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.TextEdit lbGSAVDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAHDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAPDOP;
        private DevExpress.XtraEditors.TextEdit lbGSAPRNs;
        private DevExpress.XtraEditors.TextEdit lbGSAFixMode;
        private DevExpress.XtraEditors.TextEdit lbGSAMode;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private System.Windows.Forms.PictureBox picGSVSignals;
        private System.Windows.Forms.PictureBox picGSVSkyview;
        private DevExpress.XtraEditors.TextEdit statusBar1;
        private DevExpress.XtraEditors.TextEdit lbRMCGridRef;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraEditors.ComboBoxEdit cmbPortName;
        private DevExpress.XtraEditors.ComboBoxEdit cmbBaudRate;
        private DevExpress.XtraLayout.LayoutControl layoutControl_Gps_Page1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraEditors.SimpleButton btnGPS_PlotStart;
        private DevExpress.XtraEditors.CheckEdit ceGps_PlotWithTimer;
        private DevExpress.XtraEditors.SpinEdit seGps_PlotTimerInterval;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraEditors.SimpleButton btnGps_PauseTimer;
        private DevExpress.XtraEditors.LabelControl labelPlottingUsingTimer;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditHighlight;
        private DevExpress.XtraEditors.CheckEdit csScaleMapForHighlighted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraBars.BarButtonItem bbiHighlightYes;
        private DevExpress.XtraBars.BarButtonItem bbiHighlightNo;
        private DevExpress.XtraBars.BarButtonItem bbiCopyHighlightFromVisible;
        private DevExpress.XtraBars.BarButtonItem bbiVisibleYes;
        private DevExpress.XtraBars.BarButtonItem bbiVisibleNo;
        private DevExpress.XtraEditors.SimpleButton btnRefreshMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiPrint;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit3View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private System.Windows.Forms.ImageList imageList4;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ImageList imageListLineStyles;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditThematicStyles;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSet;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSetAs;
        private DevExpress.XtraBars.PopupMenu popupMenu_ThematicGrid;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraBars.BarButtonItem bbiLayerManagerProperties;
        private DevExpress.XtraBars.PopupMenu popupMenu_LayerManager;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLayers;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel4_Container;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraEditors.ButtonEdit beWorkspace;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkspaceID;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerType;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerPath;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerVisible;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCE_LayerVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerHitable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCE_LayerHitable;
        private DevExpress.XtraGrid.Columns.GridColumn colPreserveDefaultStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colTransparency;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colUseThematicStyling;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicStyleSet;
        private DevExpress.XtraGrid.Columns.GridColumn colPointSize;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private System.Windows.Forms.BindingSource sp01288ATTreePickerWorkspacelayerslistBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLayerName1;
        private System.Windows.Forms.BindingSource sp01274ATTreePickerWorkspaceEditBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter sp01274_AT_Tree_Picker_Workspace_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraBars.BarButtonItem bbiWorkOrderMap;
        private DevExpress.XtraBars.BarButtonItem bbiAddLayer;
        private DevExpress.XtraBars.BarButtonItem bbiRemoveLayer;
        private MapInfo.Windows.Controls.MapToolBarButton mapToolBarButton2;
        private DevExpress.XtraBars.BarCheckItem bbiQueryTool;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyLayerPathForSave;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel5;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel5_Container;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        public System.Windows.Forms.PictureBox pictureBoxLegend;  // Must be set as Public for child form to see //
        private System.Windows.Forms.ImageList imageListLineStylesLegend;
        private DevExpress.XtraBars.Bar bar5;
        public DevExpress.XtraBars.BarCheckItem bbiLegend;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel6;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel6_Container;
        private System.Windows.Forms.PictureBox pictureBoxScaleBar;
        private DevExpress.XtraBars.BarCheckItem bciScaleBar;
        private DevExpress.XtraEditors.RadioGroup radioGroupGazetteerMatchPattern;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditGazetteerSearchType;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditGazetteerFindValue;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private System.Windows.Forms.BindingSource sp01311ATTreePickerGazetteerSearchTypesBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarCheckItem bciGazetteer;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelGazetteer;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel7_Container;
        private System.Windows.Forms.BindingSource sp01312ATTreePickerGazetteerSearchBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colXCoordinate1;
        private DevExpress.XtraGrid.Columns.GridColumn colYCoordinate1;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraBars.PopupMenu popupMenu_MapControl1;
        private DevExpress.XtraBars.BarButtonItem bbiEditMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteMapObjects;
        private DevExpress.XtraEditors.SimpleButton btnSelectHighlighted;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.BarButtonItem bbiSetLocalityCentre;
        private DevExpress.XtraBars.BarButtonItem bbiSetDistrictCentre;
        private DevExpress.XtraBars.BarButtonItem bbiGPSSaveLog;
        private DevExpress.XtraBars.BarButtonItem bbiGPSClearLog;
        private System.Windows.Forms.ImageList imageListGPSLogMenu;
        private DevExpress.XtraEditors.CheckEdit ceLogRawGPSData;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerShowMatch;
        private DevExpress.XtraBars.PopupMenu popupMenu_Gazetteer;
        private DevExpress.XtraBars.BarSubItem barSubItemMapEdit;
        private DevExpress.XtraBars.BarEditItem beiGazetteerLoadObjectsWithinRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditGazetteerRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerLoadMapObjectsInRange;
        private DevExpress.XtraBars.BarCheckItem bciStoreViewChanges;
        private DevExpress.XtraBars.BarButtonItem bbiClearAllMapViews;
        private DevExpress.XtraBars.BarButtonItem bbiStoreCurrentMapView;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarSubItem bsiGoToMapView;
        private DevExpress.XtraBars.PopupMenu popupStoredMapViews;
        private DevExpress.XtraBars.Bar bar6;
        private DevExpress.XtraBars.BarButtonItem bbiLastMapView;
        private DevExpress.XtraBars.BarButtonItem bbiMapSelection;
        private DevExpress.XtraBars.BarButtonItem bbiNextMapView;
        private DevExpress.XtraBars.BarButtonItem bbiStoredMapViews;
        private DevExpress.XtraBars.BarButtonItem bbiGazetteerClearMapSearch;
        private DevExpress.XtraEditors.SimpleButton btnClearMapObjects;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraBars.BarSubItem bsiEditObjects;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.ImageList imageList5;
        private DevExpress.XtraBars.BarButtonItem bbiEditSelectedMapObjects;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditselectedMapObjects;
        private DevExpress.XtraBars.BarSubItem bsiEditMapObject;
        private DevExpress.XtraBars.BarSubItem bsiMapNearbyObjects;
        private DevExpress.XtraBars.BarButtonItem bbiNearbyObjectsFind;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarCheckItem barCheckItem2;
        private DevExpress.XtraBars.BarEditItem beiPlotObjectType;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colModuleID;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTrees;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DataSet_EP dataSet_EP;
        private DevExpress.XtraBars.BarSubItem bsiSetCentrePoint;
        private DevExpress.XtraBars.BarButtonItem bbiSetClientCentre;
        private DevExpress.XtraBars.BarButtonItem bbiSetSiteCentre;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.XtraBars.Bar bar7;
        private DevExpress.XtraBars.Bar bar8;
        private DevExpress.XtraBars.Bar bar9;
        private System.Windows.Forms.BindingSource sp07048UTTreePickerPlottingObjectTypesWithBlankBindingSource;
        private DataSet_UT_Mapping dataSet_UT_Mapping;
        private DataSet_UT_MappingTableAdapters.sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraBars.BarButtonItem bbiTransferPolesToSurvey;
        private DevExpress.XtraEditors.ButtonEdit buttonEditFilterCircuits;
        private DevExpress.XtraBars.BarStaticItem bsiSyncSelection;
        private System.Windows.Forms.BindingSource sp07049UTTreePickerPolesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType;
        private DataSet_UT_MappingTableAdapters.sp07049_UT_Tree_Picker_PolesTableAdapter sp07049_UT_Tree_Picker_PolesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private System.Windows.Forms.BindingSource sp07053UTTreePickerTreesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBandID;
        private DevExpress.XtraGrid.Columns.GridColumn colSizeBand;
        private DevExpress.XtraGrid.Columns.GridColumn colX1;
        private DevExpress.XtraGrid.Columns.GridColumn colY1;
        private DevExpress.XtraGrid.Columns.GridColumn colXYPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude1;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colArea;
        private DevExpress.XtraGrid.Columns.GridColumn colLength;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colObjectType1;
        private DevExpress.XtraGrid.Columns.GridColumn colHighlight1;
        private DataSet_UT_MappingTableAdapters.sp07053_UT_Tree_Picker_TreesTableAdapter sp07053_UT_Tree_Picker_TreesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeType;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.SimpleButton btnRefreshTrees;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshTrees;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelSurvey;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
        private DevExpress.XtraEditors.ButtonEdit buttonEditSurvey;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnSurveyRefreshPoles;
        private DevExpress.XtraBars.BarCheckItem bciSurvey;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend1;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend2;
        private DevExpress.XtraEditors.SimpleButton btnGPS;
        private DevExpress.XtraBars.BarSubItem bsiSurvey;
        private DevExpress.XtraBars.BarButtonItem bbiSurveyPole;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDone;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend1;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend2;
        private DevExpress.XtraBars.BarButtonItem bbiRemovePolesFromSurvey;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEditPlotTreesAgainstPole;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiSurveyPoleSetClear;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend3;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend3;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend5;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend4;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend4;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend5;
        private DevExpress.XtraBars.Docking.DockPanel dockPanelLegend;
        private DevExpress.XtraBars.Docking.ControlContainer controlContainer3;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraBars.BarButtonItem bbiCaptureCustomerPermission;
        private DevExpress.XtraBars.BarButtonItem bbiViewPoleOwnership;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend7;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend7;
        private DevExpress.XtraEditors.LabelControl labelControlPoleLegend6;
        private DevExpress.XtraEditors.ColorEdit colorEditPoleLegend6;
        private DevExpress.XtraEditors.SimpleButton btnScaleToViewedObjects;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.BarButtonItem bbiShutdownRequired;
        private DevExpress.XtraBars.BarButtonItem bbiShutdownClear;
        private DevExpress.XtraBars.BarButtonItem bbiTrafficManagmentRequired;
        private DevExpress.XtraBars.BarButtonItem bbiTrafficManagmentClear;
        private DevExpress.XtraBars.BarButtonItem bbiDeferredSet;
        private DevExpress.XtraBars.BarButtonItem bbiCreateAccessMap;
        private DevExpress.XtraBars.BarButtonItem bbiCopyJobs;
        private DevExpress.XtraBars.BarButtonItem bbiPasteJobs;
        private DevExpress.XtraBars.BarButtonItem bbiClearDeferred;
        private DevExpress.XtraBars.BarButtonItem bbiTrafficMap;
        private DevExpress.XtraBars.BarButtonItem bbiPermission;
        private DevExpress.XtraBars.BarButtonItem bbiPermissionView;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl labelControlTreeLegend4;
        private DevExpress.XtraEditors.ColorEdit colorEditTreeLegend4;
        private DevExpress.XtraEditors.LabelControl labelControlTreeLegend3;
        private DevExpress.XtraEditors.ColorEdit colorEditTreeLegend3;
        private DevExpress.XtraEditors.ColorEdit colorEditTreeLegend2;
        private DevExpress.XtraEditors.ColorEdit colorEditTreeLegend1;
        private DevExpress.XtraEditors.LabelControl labelControlTreeLegend1;
        private DevExpress.XtraEditors.LabelControl labelControlTreeLegend2;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEditTreeStylingType;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEditPoleStylingType;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ColorEdit colorEditTreeLegend5;
        private DevExpress.XtraEditors.LabelControl labelControlTreeLegend5;
        private DevExpress.XtraEditors.ColorEdit colorEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicValue;
        private DevExpress.XtraGrid.Columns.GridColumn colThematicValue1;
        private DevExpress.XtraBars.BarButtonItem bbiSetPoleHistorical;
        private DevExpress.XtraBars.BarButtonItem bbiAddWorkToWorkOrder;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Columns.GridColumn colTooltip1;
        private DevExpress.XtraGrid.Columns.GridColumn colTooltip;
        private DevExpress.XtraBars.BarButtonItem bbiViewPictures;  // Must be set as Public for child form to see //
    }
}

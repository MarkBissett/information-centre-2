﻿namespace WoodPlan5
{
    partial class frm_UT_Resource_Rate_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Resource_Rate_Manager));
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.managerGridControl = new DevExpress.XtraGrid.GridControl();
            this.sp07438UTResourceRateListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.managerGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colResourceRateID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResourceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResourceDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEditBuyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEditSellRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp07438_UT_Resource_Rate_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438UTResourceRateListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.Manager = null;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 748);
            this.barDockControlBottom.Size = new System.Drawing.Size(1372, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 748);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1372, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 748);
            // 
            // bbiSave
            // 
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.LargeGlyph")));
            // 
            // pmEditContextMenu
            // 
            this.pmEditContextMenu.Manager = null;
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiCancel});
            this.barManager1.MaxItemId = 31;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("save_16x16.png", "images/save/save_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/save/save_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "save_16x16.png");
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1675, 521, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 748);
            this.dataLayoutControl1.TabIndex = 4;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 748);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // managerGridControl
            // 
            this.managerGridControl.DataSource = this.sp07438UTResourceRateListBindingSource;
            this.managerGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.ImageIndex = 0;
            this.managerGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.ImageIndex = 1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.ImageIndex = 4;
            this.managerGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.managerGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.ImageIndex = 2;
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Tag = "";
            this.managerGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.managerGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "", "delete")});
            this.managerGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.resourceRateGridControl_EmbeddedNavigator_ButtonClick);
            this.managerGridControl.Location = new System.Drawing.Point(0, 0);
            this.managerGridControl.MainView = this.managerGridView;
            this.managerGridControl.Name = "managerGridControl";
            this.managerGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.moneyTextEdit});
            this.managerGridControl.Size = new System.Drawing.Size(1372, 748);
            this.managerGridControl.TabIndex = 5;
            this.managerGridControl.UseEmbeddedNavigator = true;
            this.managerGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.managerGridView});
            this.managerGridControl.Click += new System.EventHandler(this.managerGridControl_Click);
            // 
            // sp07438UTResourceRateListBindingSource
            // 
            this.sp07438UTResourceRateListBindingSource.DataMember = "sp07438_UT_Resource_Rate_List";
            this.sp07438UTResourceRateListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // managerGridView
            // 
            this.managerGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colResourceRateID,
            this.colQuoteCategoryID,
            this.colQuoteCategory,
            this.colResourceID,
            this.colResourceDescription,
            this.colClientContractTypeID,
            this.colContractType,
            this.colDescription,
            this.colBuyRate,
            this.colSellRate,
            this.colItemUnitsID,
            this.colUnits,
            this.colAllowEditBuyRate,
            this.colAllowEditSellRate,
            this.colMode,
            this.colRecordID});
            this.managerGridView.GridControl = this.managerGridControl;
            this.managerGridView.Name = "managerGridView";
            this.managerGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.managerGridView.OptionsFind.AlwaysVisible = true;
            this.managerGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.managerGridView.OptionsLayout.StoreAppearance = true;
            this.managerGridView.OptionsMenu.ShowConditionalFormattingItem = true;
            this.managerGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.managerGridView.OptionsSelection.MultiSelect = true;
            this.managerGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.managerGridView.OptionsView.ColumnAutoWidth = false;
            this.managerGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.managerGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAllowEditBuyRate, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.managerGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.resourceRateGridView_PopupMenuShowing);
            this.managerGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.resourceRateGridView_CustomDrawEmptyForeground);
            this.managerGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.managerGridView_CustomFilterDialog);
            this.managerGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.managerGridView_MouseUp);
            this.managerGridView.DoubleClick += new System.EventHandler(this.resourceRateGridView_DoubleClick);
            // 
            // colResourceRateID
            // 
            this.colResourceRateID.FieldName = "ResourceRateID";
            this.colResourceRateID.Name = "colResourceRateID";
            this.colResourceRateID.OptionsColumn.AllowEdit = false;
            this.colResourceRateID.OptionsColumn.AllowFocus = false;
            this.colResourceRateID.OptionsColumn.ReadOnly = true;
            this.colResourceRateID.OptionsColumn.ShowInCustomizationForm = false;
            this.colResourceRateID.OptionsColumn.ShowInExpressionEditor = false;
            this.colResourceRateID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colResourceRateID.Width = 106;
            // 
            // colQuoteCategoryID
            // 
            this.colQuoteCategoryID.FieldName = "QuoteCategoryID";
            this.colQuoteCategoryID.Name = "colQuoteCategoryID";
            this.colQuoteCategoryID.OptionsColumn.AllowEdit = false;
            this.colQuoteCategoryID.OptionsColumn.AllowFocus = false;
            this.colQuoteCategoryID.OptionsColumn.ReadOnly = true;
            this.colQuoteCategoryID.OptionsColumn.ShowInCustomizationForm = false;
            this.colQuoteCategoryID.OptionsColumn.ShowInExpressionEditor = false;
            this.colQuoteCategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQuoteCategoryID.Width = 113;
            // 
            // colQuoteCategory
            // 
            this.colQuoteCategory.FieldName = "QuoteCategory";
            this.colQuoteCategory.Name = "colQuoteCategory";
            this.colQuoteCategory.OptionsColumn.AllowEdit = false;
            this.colQuoteCategory.OptionsColumn.AllowFocus = false;
            this.colQuoteCategory.OptionsColumn.ReadOnly = true;
            this.colQuoteCategory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQuoteCategory.Visible = true;
            this.colQuoteCategory.VisibleIndex = 0;
            this.colQuoteCategory.Width = 180;
            // 
            // colResourceID
            // 
            this.colResourceID.FieldName = "ResourceID";
            this.colResourceID.Name = "colResourceID";
            this.colResourceID.OptionsColumn.AllowEdit = false;
            this.colResourceID.OptionsColumn.AllowFocus = false;
            this.colResourceID.OptionsColumn.ReadOnly = true;
            this.colResourceID.OptionsColumn.ShowInCustomizationForm = false;
            this.colResourceID.OptionsColumn.ShowInExpressionEditor = false;
            this.colResourceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colResourceID.Width = 80;
            // 
            // colResourceDescription
            // 
            this.colResourceDescription.FieldName = "ResourceDescription";
            this.colResourceDescription.Name = "colResourceDescription";
            this.colResourceDescription.OptionsColumn.AllowEdit = false;
            this.colResourceDescription.OptionsColumn.AllowFocus = false;
            this.colResourceDescription.OptionsColumn.ReadOnly = true;
            this.colResourceDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colResourceDescription.Visible = true;
            this.colResourceDescription.VisibleIndex = 1;
            this.colResourceDescription.Width = 217;
            // 
            // colClientContractTypeID
            // 
            this.colClientContractTypeID.FieldName = "ClientContractTypeID";
            this.colClientContractTypeID.Name = "colClientContractTypeID";
            this.colClientContractTypeID.OptionsColumn.AllowEdit = false;
            this.colClientContractTypeID.OptionsColumn.AllowFocus = false;
            this.colClientContractTypeID.OptionsColumn.ReadOnly = true;
            this.colClientContractTypeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colClientContractTypeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colClientContractTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientContractTypeID.Width = 134;
            // 
            // colContractType
            // 
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 2;
            this.colContractType.Width = 150;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 3;
            this.colDescription.Width = 369;
            // 
            // colBuyRate
            // 
            this.colBuyRate.ColumnEdit = this.moneyTextEdit;
            this.colBuyRate.FieldName = "BuyRate";
            this.colBuyRate.Name = "colBuyRate";
            this.colBuyRate.OptionsColumn.AllowEdit = false;
            this.colBuyRate.OptionsColumn.AllowFocus = false;
            this.colBuyRate.OptionsColumn.ReadOnly = true;
            this.colBuyRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBuyRate.Visible = true;
            this.colBuyRate.VisibleIndex = 4;
            this.colBuyRate.Width = 103;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colSellRate
            // 
            this.colSellRate.ColumnEdit = this.moneyTextEdit;
            this.colSellRate.FieldName = "SellRate";
            this.colSellRate.Name = "colSellRate";
            this.colSellRate.OptionsColumn.AllowEdit = false;
            this.colSellRate.OptionsColumn.AllowFocus = false;
            this.colSellRate.OptionsColumn.ReadOnly = true;
            this.colSellRate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSellRate.Visible = true;
            this.colSellRate.VisibleIndex = 5;
            this.colSellRate.Width = 96;
            // 
            // colItemUnitsID
            // 
            this.colItemUnitsID.FieldName = "ItemUnitsID";
            this.colItemUnitsID.Name = "colItemUnitsID";
            this.colItemUnitsID.OptionsColumn.AllowEdit = false;
            this.colItemUnitsID.OptionsColumn.AllowFocus = false;
            this.colItemUnitsID.OptionsColumn.ReadOnly = true;
            this.colItemUnitsID.OptionsColumn.ShowInCustomizationForm = false;
            this.colItemUnitsID.OptionsColumn.ShowInExpressionEditor = false;
            this.colItemUnitsID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colItemUnitsID.Width = 84;
            // 
            // colUnits
            // 
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.OptionsColumn.AllowEdit = false;
            this.colUnits.OptionsColumn.AllowFocus = false;
            this.colUnits.OptionsColumn.ReadOnly = true;
            this.colUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUnits.Visible = true;
            this.colUnits.VisibleIndex = 6;
            this.colUnits.Width = 164;
            // 
            // colAllowEditBuyRate
            // 
            this.colAllowEditBuyRate.FieldName = "AllowEditBuyRate";
            this.colAllowEditBuyRate.Name = "colAllowEditBuyRate";
            this.colAllowEditBuyRate.OptionsColumn.AllowEdit = false;
            this.colAllowEditBuyRate.OptionsColumn.AllowFocus = false;
            this.colAllowEditBuyRate.OptionsColumn.ReadOnly = true;
            this.colAllowEditBuyRate.Visible = true;
            this.colAllowEditBuyRate.VisibleIndex = 7;
            this.colAllowEditBuyRate.Width = 127;
            // 
            // colAllowEditSellRate
            // 
            this.colAllowEditSellRate.FieldName = "AllowEditSellRate";
            this.colAllowEditSellRate.Name = "colAllowEditSellRate";
            this.colAllowEditSellRate.OptionsColumn.AllowEdit = false;
            this.colAllowEditSellRate.OptionsColumn.AllowFocus = false;
            this.colAllowEditSellRate.OptionsColumn.ReadOnly = true;
            this.colAllowEditSellRate.Visible = true;
            this.colAllowEditSellRate.VisibleIndex = 8;
            this.colAllowEditSellRate.Width = 112;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            this.colMode.OptionsColumn.ShowInCustomizationForm = false;
            this.colMode.OptionsColumn.ShowInExpressionEditor = false;
            this.colMode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            this.colRecordID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRecordID.OptionsColumn.ShowInExpressionEditor = false;
            this.colRecordID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // bbiCancel
            // 
            this.bbiCancel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.bbiCancel.Caption = "Cancel and Close";
            this.bbiCancel.DropDownEnabled = false;
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 30;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp07438_UT_Resource_Rate_ListTableAdapter
            // 
            this.sp07438_UT_Resource_Rate_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.managerGridControl;
            // 
            // frm_UT_Resource_Rate_Manager
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1372, 748);
            this.Controls.Add(this.managerGridControl);
            this.Controls.Add(this.dataLayoutControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Resource_Rate_Manager";
            this.Text = "Manager Resource Rates";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.frm_UT_Resource_Rate_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_UT_Resource_Rate_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            this.Controls.SetChildIndex(this.managerGridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438UTResourceRateListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.managerGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl managerGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView managerGridView;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp07438UTResourceRateListBindingSource;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private DevExpress.XtraGrid.Columns.GridColumn colResourceRateID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colResourceID;
        private DevExpress.XtraGrid.Columns.GridColumn colResourceDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRate;
        private DevExpress.XtraGrid.Columns.GridColumn colItemUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter sp07438_UT_Resource_Rate_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEditBuyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEditSellRate;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

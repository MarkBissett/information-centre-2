using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;  // Required for Images Hashtable //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Map_Snapshot_Step1 : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int _LinkedToRecordTypeID = 0;
        public int _LinkedToRecordID = 0;
        public string _LinkedToRecordDescription = "";

        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        string strMapsPath = "";
        Hashtable Images = new Hashtable();
        public frm_UT_Mapping frmParentMappingForm = null;
        public int intCurrentMapScale = 0; 
        public int intDPI = 0;  // DotsPerInch [Passed through to create map screen] // 
  
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        int i_int_FocusedGrid = 1;
        bool boolOrderChanged = false;  // Determins if code fires to save record order changes //

        #endregion

        public frm_UT_Mapping_Map_Snapshot_Step1()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Map_Snapshot_Step1_Load(object sender, EventArgs e)
        {
            this.FormID = 500047;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strMapsPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strMapsPath.EndsWith("\\")) strMapsPath += "\\";

            RefreshGridViewState1 = new RefreshGridState(gridView1, "LinkedMapID");
            RefreshGridViewState2 = new RefreshGridState(gridView2, "LinkedMapID");

            sp07203_UT_Mapping_Snapshots_Unallocated_SnapshotsTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Unlinked_Maps_Grid();
            gridView2.ExpandAllGroups();

            sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Connection.ConnectionString = strConnectionString;

            if (!string.IsNullOrEmpty(_LinkedToRecordDescription))
            {
                beChooseRecord.Text = _LinkedToRecordDescription;
            }
            if (_LinkedToRecordTypeID == 1)
            {
                this.Text = "Mapping - Map Snapshots  [Permission Documents]";
            }
            else if (_LinkedToRecordTypeID == 2)
            {
                this.Text = "Mapping - Map Snapshots  [Work Orders]";
            }
            Load_Maps_Grid();
            GridView view = (GridView)gridControl1.MainView;
            view.ExpandAllGroups();
        }

        private void frm_UT_Mapping_Map_Snapshot_Step1_FormClosing(object sender, FormClosingEventArgs e)
        {           
            base.ImagePreviewClear();
            GC.GetTotalMemory(true);
        }

        private void Load_Maps_Grid()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Fill(this.dataSet_UT_Mapping.sp07202_UT_Mapping_Snapshots_Linked_Snapshots, _LinkedToRecordID.ToString() + ",", _LinkedToRecordTypeID);
            view.EndUpdate();
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
        }

        private void Load_Unlinked_Maps_Grid()
        {
            GridView view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp07203_UT_Mapping_Snapshots_Unallocated_SnapshotsTableAdapter.Fill(dataSet_UT_Mapping.sp07203_UT_Mapping_Snapshots_Unallocated_Snapshots, _LinkedToRecordTypeID);
            view.EndUpdate();
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No maps available - right-click or choose the Add button to add maps";
                    break;
                case "gridView2":
                    message = "No maps available - right-click or choose the Add button to add maps";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl1

        private void gridView1_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(33, 33);
            if (e.Column.FieldName == "gridColumnThumbNail" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = strMapsPath + view.GetRowCellValue(rHandle, "DocumentPath").ToString() + "_thumb.jpg";
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 33, 33);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*bbiSave.Enabled = false;
                bbiBlockAdd.Enabled = false;
                bbiBlockEdit.Enabled = false;
                bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
                bbiDelete.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                bsiDataset.Enabled = false;
                bbiPreviewMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);*/
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        AddRecord(gridControl1);
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        DeleteRecord(gridControl1);
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "MapOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "MapOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("save sort".Equals(e.Button.Tag))
                    {
                        gridControl1.BeginUpdate();
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            view.SetRowCellValue(i, "MapOrder", i + 1);  // Add 1 to value as DataSet is 0 based //
                        }
                        view.EndSort();
                        gridControl1.EndUpdate();
                        SaveChangesToOrder();  // Save any changes to record Orders //
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_MouseMove(object sender, MouseEventArgs e)
        {
            // Note that the form must have the Form frmMain2 as its Owner for the message to be passed through to show the Image in the preview pane. Clear message sent on Form Closing //
            GridView view = (GridView)gridControl1.MainView;

            Point ptMousePosition = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo ghi = view.CalcHitInfo(ptMousePosition);
            if (ghi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                if (ghi.Column.FieldName == "gridColumnThumbNail")
                {
                    string strImagePath = strMapsPath + view.GetRowCellValue(ghi.RowHandle, "DocumentPath").ToString() + ".gif";
                    try
                    {
                        Image PassedImage = Image.FromFile(strImagePath);
                        base.ImagePreview(PassedImage, "scale", "no");
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        #endregion


        #region GridControl2

        private void gridView2_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            Bitmap bmpTest = new Bitmap(33, 33);
            if (e.Column.FieldName == "gridColumnThumbNail" && e.IsGetData)
            {
                DevExpress.XtraGrid.Views.Grid.GridView view = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                string strImagePath = strMapsPath + view.GetRowCellValue(rHandle, "DocumentPath").ToString() + "_thumb.jpg";
                strImagePath = strImagePath.ToLower();
                if (!Images.ContainsKey(strImagePath))
                {
                    Image img = null;
                    try
                    {
                        img = Image.FromFile(strImagePath);
                        bmpTest = new Bitmap(img, 33, 33);
                        img = null;
                        img.Dispose();
                    }
                    catch
                    {
                    }
                    Images.Add(strImagePath, bmpTest);
                }
                e.Value = Images[strImagePath];
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /* bbiSave.Enabled = false;
                 bbiBlockAdd.Enabled = false;
                 bbiBlockEdit.Enabled = false;
                 bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
                 bbiDelete.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                 bsiEdit.Enabled = false;
                 bbiSingleEdit.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                 bsiDataset.Enabled = false;
                 bbiPreviewMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);*/
                pmDataContextMenu.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        AddRecord(gridControl2);
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        DeleteRecord(gridControl2);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControl2_MouseMove(object sender, MouseEventArgs e)
        {
            // Note that the form must have the Form frmMain2 as its Owner for the message to be passed through to show the Image in the preview pane. Clear message sent on Form Closing //
            GridView view = (GridView)gridControl2.MainView;

            Point ptMousePosition = new Point(e.X, e.Y);
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo ghi = view.CalcHitInfo(ptMousePosition);
            if (ghi.HitTest == DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitTest.RowCell)
            {
                if (ghi.Column.FieldName == "gridColumnThumbNail")
                {
                    string strImagePath = strMapsPath + view.GetRowCellValue(ghi.RowHandle, "DocumentPath").ToString() + ".gif";
                    try
                    {
                        Image PassedImage = Image.FromFile(strImagePath);
                        base.ImagePreview(PassedImage, "scale", "no");
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        #endregion


        private void SaveChangesToOrder()
        {
            if (!boolOrderChanged) return;

            // Save Changes to Workspace //
            
            sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.EndEdit();
            try
            {
                this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Update(dataSet_UT_Mapping);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Unable to save Map Order changes - an error occurred while saving [" + ex.Message + "]!", "Save Map Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            boolOrderChanged = false;  // Reset flag //
        }

        public void SetMenuStatus()
        {
            GridView view = null;
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 1: 
                    view = (GridView)gridControl1.MainView;
                    break;
                default:
                    view = (GridView)gridControl2.MainView;
                    break;
            }
            intRowHandles = view.GetSelectedRows();
            bbiSave.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiSingleAdd.Enabled = (view.DataRowCount > 10 ? false : true);
            bbiDelete.Enabled = (intRowHandles.Length > 0 ? true : false);
            bbiSingleEdit.Enabled = false;
            bsiEdit.Enabled = false;
            bsiDataset.Enabled = false;
            bbiPreviewMap.Enabled = (intRowHandles.Length == 1 ? true : false);
            bbiAllocate.Enabled = (intRowHandles.Length == 1 && i_int_FocusedGrid == 2 ? true : false);
            bbiUnallocate.Enabled = (intRowHandles.Length == 1 && i_int_FocusedGrid == 1 ? true : false);
            bsiMapTools.Enabled = (intRowHandles.Length == 1 ? true : false);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            if (gridControl1.Focused)
            {
                AddRecord(gridControl1);
            }
            else
            {
                AddRecord(gridControl2);
            }
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            if (gridControl1.Focused)
            {
                DeleteRecord(gridControl1);
                Update_Callers("");
            }
            else
            {
                DeleteRecord(gridControl2);
            }
        }

        private void AddRecord(GridControl gridControl)
        {           
            if (gridControl.Name == "gridControl1")
            {
                if (_LinkedToRecordID == 0)  // No record selected so abort //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add, no Record To Link To selected.\n\nSelect one or more records to link to by click Choose Record button then try again.", "Add Map Snapshot", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            WaitDialogForm loading = new WaitDialogForm("Generating Map Preview...", "Mapping");
            loading.Show();

            Image image = null;
            frmParentMappingForm.Generate_Work_Order_Map(ref image);

            frm_UT_Mapping_Map_Snapshot_Map frm_child = new frm_UT_Mapping_Map_Snapshot_Map();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.strFormMode = "Add";
            frm_child.imageMap = image;
            frm_child.frmParentMappingForm = this.frmParentMappingForm;
            frm_child.intCurrentMapScale = this.intCurrentMapScale;
            frm_child.intDPI = this.intDPI;
            frm_child._LinkedToRecordID = (gridControl.Name == "gridControl1" ? _LinkedToRecordID : 0);
            frm_child._LinkedToRecordTypeID = _LinkedToRecordTypeID;
            frm_child._LinkedToRecordDescription = (gridControl.Name == "gridControl1" ? _LinkedToRecordDescription : "");
            frm_child.strMapsPath = strMapsPath;

            loading.Close();

            if (frm_child.ShowDialog() == DialogResult.OK)
            {
                if (gridControl.Name == "gridControl1")
                {
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    Load_Maps_Grid();
                }
                else
                {
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    Load_Unlinked_Maps_Grid();
                }
                Update_Callers("");
            }
            image = null;
            GC.GetTotalMemory(true);
        }

        private void DeleteRecord(GridControl gridControl)
        {
            GridView view = (GridView)gridControl.MainView;
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            int intUpdateProgressThreshhold = intCount / 10;
            int intUpdateProgressTempCount = 0;

            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Maps to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = (intCount == 1 ? "You have 1 Map selected for delete!\n\nProceed?" : "You have " + Convert.ToString(intRowHandles.Length) + " Maps selected for delete!\n\nProceed?");
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {               
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                int intRecordID = 0;
                string strRecordIDs = "";
                string strImagePath = "";
                string strImageThumbPath = "";
                gridControl.BeginUpdate();
                int intRowHandle = 0;

                //foreach (int intRowHandle in intRowHandles)
                for (int i = intRowHandles.Length - 1; i >= 0; i--)  // Process backwards //
                {
                    intRowHandle = intRowHandles[i];
                    intRecordID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedMapID"));
                    strImageThumbPath = strMapsPath + view.GetRowCellValue(intRowHandle, "DocumentPath").ToString() + "_thumb" + ".jpg";
                    strImagePath = strMapsPath + view.GetRowCellValue(intRowHandle, "DocumentPath").ToString() + ".gif";

                    try
                    {
                        Images.Remove(strImageThumbPath);  // Remove Thumbnail from Hashtable used by gridView1_CustomUnboundColumnData and gridView2_CustomUnboundColumnData //
                        
                        base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                        GC.GetTotalMemory(true);

                        System.IO.File.Delete(strImageThumbPath);
                        System.IO.File.Delete(strImagePath);
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete the Map and\\or Thumbnail Image [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        continue;
                    }

                    strRecordIDs += view.GetRowCellValue(intRowHandle, "LinkedMapID").ToString() + ",";

                    view.DeleteRow(intRowHandle);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
                // Remove records from DB in one hit //
                try
                {
                    DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp07000_UT_Delete("linked_map", strRecordIDs);  // Remove the records from the DB in one go //
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete the Work Order Map record(s) [" + ex.Message + "].\n\nTry deleting again. If the problem persists contact technical support.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

                gridControl.EndUpdate();
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void bbiPreviewMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
            GridView view = null;
            string strImagePath = "";
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + ".gif";

            frm_preview.strImage = strImagePath;
            frm_preview.ShowDialog();
        }

        private void bbiAllocate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intMapID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedMapID"));

            if (_LinkedToRecordTypeID == 1)
            {
                frm_UT_Mapping_Map_Snapshot_Allocate frmAllocate = new frm_UT_Mapping_Map_Snapshot_Allocate();
                frmAllocate.GlobalSettings = this.GlobalSettings;
                frmAllocate.strMapDescription = view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString(); ;
                frmAllocate.strMapRemarks = view.GetRowCellValue(view.FocusedRowHandle, "Remarks").ToString(); ;
                if (frmAllocate.ShowDialog() == DialogResult.OK)
                {
                    int intRecordID = frmAllocate.intSelectedRecordID;
                    string strMapDescription = frmAllocate.strMapDescription;
                    string strMapRemarks = frmAllocate.strMapRemarks;
                    // Allocate the Map to the WorkOrder //
                    try
                    {
                        DataSet_UT_MappingTableAdapters.QueriesTableAdapter AllocateMap = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                        AllocateMap.ChangeConnectionString(strConnectionString);
                        AllocateMap.sp07208_UT_Mapping_Snapshots_Reallocate_Map(intMapID, intRecordID, _LinkedToRecordTypeID, strMapDescription, strMapRemarks);

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        Load_Maps_Grid();

                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        Load_Unlinked_Maps_Grid();
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to allocate the linked Map [" + ex.Message + "].\n\nTry allocating again. If the problem persists contact technical support.", "Allocate Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            else
            {
                frm_UT_Mapping_Map_Snapshot_Allocate_WO frmAllocate = new frm_UT_Mapping_Map_Snapshot_Allocate_WO();
                frmAllocate.GlobalSettings = this.GlobalSettings;
                frmAllocate.strMapDescription = view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString(); ;
                frmAllocate.strMapRemarks = view.GetRowCellValue(view.FocusedRowHandle, "Remarks").ToString(); ;
                if (frmAllocate.ShowDialog() == DialogResult.OK)
                {
                    int intRecordID = frmAllocate.intSelectedRecordID;
                    string strMapDescription = frmAllocate.strMapDescription;
                    string strMapRemarks = frmAllocate.strMapRemarks;
                    // Allocate the Map to the WorkOrder //
                    try
                    {
                        DataSet_UT_MappingTableAdapters.QueriesTableAdapter AllocateMap = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                        AllocateMap.ChangeConnectionString(strConnectionString);
                        AllocateMap.sp07208_UT_Mapping_Snapshots_Reallocate_Map(intMapID, intRecordID, _LinkedToRecordTypeID, strMapDescription, strMapRemarks);

                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        Load_Maps_Grid();

                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        Load_Unlinked_Maps_Grid();

                        Update_Callers(intRecordID + ";");
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to allocate the linked Map [" + ex.Message + "].\n\nTry allocating again. If the problem persists contact technical support.", "Allocate Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
        }

        private void bbiUnallocate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intMapID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedMapID"));
            string strLinkedtoRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToDescription").ToString();

            frm_UT_Mapping_Map_Snapshot_Unallocate frmDeAllocate = new frm_UT_Mapping_Map_Snapshot_Unallocate();
            frmDeAllocate.GlobalSettings = this.GlobalSettings;
            frmDeAllocate.strMapDescription = view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString(); ;
            frmDeAllocate.strMapRemarks = view.GetRowCellValue(view.FocusedRowHandle, "Remarks").ToString(); ;
            frmDeAllocate._LinkedToRecordDescription = strLinkedtoRecordDescription;
            if (frmDeAllocate.ShowDialog() == DialogResult.OK)
            {               
                string strMapDescription = frmDeAllocate.strMapDescription;
                string strMapRemarks = frmDeAllocate.strMapRemarks;
                // De-allocate the Map from the WorkOrder //
                try
                {
                    DataSet_UT_MappingTableAdapters.QueriesTableAdapter AllocateMap = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                    AllocateMap.ChangeConnectionString(strConnectionString);
                    AllocateMap.sp07208_UT_Mapping_Snapshots_Reallocate_Map(intMapID, 0, _LinkedToRecordTypeID, strMapDescription, strMapRemarks);
                  
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    Load_Maps_Grid();
                    
                    this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                    Load_Unlinked_Maps_Grid();

                    Update_Callers("");
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to de-allocate the linked Map [" + ex.Message + "].\n\nTry de-allocating again. If the problem persists contact technical support.", "De-allocate Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }


        }

        private void bbiRotateAntiClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            try
            {
                base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                string strImagePath;
                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + "_thumb.jpg";
                Images.Remove(strImagePath.ToLower());  // Remove old thumbnail image stored in hashtable for grid control column preview //
                GC.GetTotalMemory(true);

                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + ".gif";
                Image newImage = Image.FromFile(strImagePath);
                newImage = RotateImage(newImage, RotateFlipType.Rotate270FlipNone);
                newImage.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Gif);

                Image newImage2 = newImage.GetThumbnailImage(50, 50, null, new IntPtr());
                strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + "_thumb.jpg";
                newImage2.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);

                Bitmap bmpTest = new Bitmap(33, 33);  // Add thumbnail image to hashtable for grid control column preview with new version //
                bmpTest = new Bitmap(newImage2, 33, 33);
                Images.Add(strImagePath.ToLower(), bmpTest);
                view.RefreshRow(view.FocusedRowHandle);  // Force Unbound image preview to be reloaded //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to rotate the image [" + ex.Message + "]!\n\nThe image may be open elsewhere within the program. Try closing any open versions of the image before trying again.", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private void bbiRotateClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = null;
            if (gridControl1.Focused)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl2.MainView;
            }
            try
            {
            base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
            string strImagePath;
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + "_thumb.jpg";
            Images.Remove(strImagePath.ToLower());  // Remove old thumbnail image stored in hashtable for grid control column preview //
            GC.GetTotalMemory(true);

            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + ".gif";
            Image newImage = Image.FromFile(strImagePath);
            newImage = RotateImage(newImage, RotateFlipType.Rotate90FlipNone);
            newImage.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Gif);

            Image newImage2 = newImage.GetThumbnailImage(50, 50, null, new IntPtr());
            strImagePath = strMapsPath + view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString() + "_thumb.jpg";
            newImage2.Save(strImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);

            Bitmap bmpTest = new Bitmap(33, 33);  // Add thumbnail image to hashtable for grid control column preview with new version //
            bmpTest = new Bitmap(newImage2, 33, 33);
            Images.Add(strImagePath.ToLower(), bmpTest);
            view.RefreshRow(view.FocusedRowHandle);  // Force Unbound image preview to be reloaded //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred when attempting to rotate the image [" + ex.Message + "]!\n\nThe image may be open elsewhere within the program. Try closing any open versions of the image before trying again.", "Work Order Mapping", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static Image RotateImage(Image img, RotateFlipType rotate_type)
        {
            img.RotateFlip(rotate_type);
            return img;
        }

        private void beChooseRecord_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (_LinkedToRecordTypeID)
            {
                case 1:  // Utilties Permission Document //
                    {
                        frm_UT_Select_Permission_Document fChildForm = new frm_UT_Select_Permission_Document();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        int intOriginalValue = _LinkedToRecordID;
                        fChildForm.intOriginalSelectedID = intOriginalValue;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            _LinkedToRecordID = fChildForm.intSelectedID;
                            _LinkedToRecordDescription = fChildForm.strSelectedDescription;
                            beChooseRecord.Text = _LinkedToRecordDescription;
                            Load_Maps_Grid();
                            GridView view = (GridView)gridControl1.MainView;
                            view.ExpandAllGroups();
                        }
                    }
                    break;
                case 2:  // Utilities Work Order //
                    {
                        frm_UT_Select_Work_Order fChildForm = new frm_UT_Select_Work_Order();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        int intOriginalValue = _LinkedToRecordID;
                        fChildForm.intOriginalSelectedValue = intOriginalValue;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            _LinkedToRecordID = fChildForm.intSelectedID;
                            _LinkedToRecordDescription = fChildForm.strSelectedValue;
                            beChooseRecord.Text = _LinkedToRecordDescription;
                            Load_Maps_Grid();
                            GridView view = (GridView)gridControl1.MainView;
                            view.ExpandAllGroups();
                        }
                    }
                    break;

            }
        }

        private void Update_Callers(string strNewIDs)
        {

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            foreach (Form frmChild in frmParentMappingForm.ParentForm.MdiChildren)
            {
                if (frmChild.Name == "frm_UT_WorkOrder_Manager")
                {
                    frm_UT_WorkOrder_Manager fParentForm;
                    fParentForm = (frm_UT_WorkOrder_Manager)frmChild;
                    fParentForm.UpdateFormRefreshStatus(2, strNewIDs, "", "", "", "");
                }
                if (frmChild.Name == "frm_UT_WorkOrder_Edit")
                {
                    frm_UT_WorkOrder_Edit fParentForm;
                    fParentForm = (frm_UT_WorkOrder_Edit)frmChild;
                    fParentForm.UpdateFormRefreshStatus(3, "");
                }
            }
        }


    
    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // For Path Combine //

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using DevExpress.XtraGrid.Views.BandedGrid;
using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Survey_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToClientID = 0;
        public int intLinkedToCircuitID = 0;
        public int intLinkedToPoleID = 0;
        public string strLinkedToPoleName = "";
        public string strLinkedToPoleNumber = "";
        public string strLinkedToCircuitNumber = "";
        public string strLinkedToClientName = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowDelete = true;
        private string strDefaultMapPath = "";

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        public string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        public string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        public string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        public string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState7;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;
        RepositoryItemTextEdit disabledCostTotalEditor;  // Used to conditionally hide the hyperlink editor //
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string strSignaturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strPermissionFilePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_UT_Survey_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Survey_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500022;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            xtraTabControl1.ShowTabHeader = DefaultBoolean.False;  // Hide main pagframe headers //

            disabledCostTotalEditor = new RepositoryItemTextEdit();
            disabledCostTotalEditor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            disabledCostTotalEditor.Mask.EditMask = "c";
            disabledCostTotalEditor.Mask.UseMaskAsDisplayFormat = true;

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Signature Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!strSignaturePath.EndsWith("\\")) strSignaturePath += "\\";

            try
            {
                strPermissionFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Permission Document Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
                if (!strDefaultMapPath.EndsWith("\\")) strDefaultMapPath += "\\";
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07138_UT_Unit_Descriptors_With_Blank);

            sp07105_UT_Map_Start_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07105_UT_Map_Start_TypesTableAdapter.Fill(dataSet_UT_Edit.sp07105_UT_Map_Start_Types);

            sp07328_UT_Surveyor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07328_UT_Surveyor_List_With_BlankTableAdapter.Fill(dataSet_UT.sp07328_UT_Surveyor_List_With_Blank);

            sp07106_UT_Survey_StatusesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07106_UT_Survey_StatusesTableAdapter.Fill(dataSet_UT_Edit.sp07106_UT_Survey_Statuses);

            sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter.Fill(dataSet_UT.sp07405_UT_Reactive_Category_Types_With_Blank);

            sp07468_UT_Contract_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07468_UT_Contract_Types_With_BlankTableAdapter.Fill(dataSet_UT.sp07468_UT_Contract_Types_With_Blank);

            sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SurveyedPoleID");

            sp07220_UT_Survey_Costing_HeadersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "SurveyCostingHeaderID");

            sp07221_UT_Survey_Costing_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(bandedGridView3, "SurveyCostingPoleID");

            sp07222_UT_Survey_Costing_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "SurveyCostingItemID");

            sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState7 = new RefreshGridState(gridView7, "PermissionDocumentID");

            emptyEditor = new RepositoryItem();

            // Populate Main Dataset //
            sp07103_UT_Survey_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07103_UT_Survey_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToClientID;
                        drNewRow["ClientName"] = strLinkedToClientName;
                        drNewRow["SurveyDate"] = DateTime.Today;
                        drNewRow["SurveyStatusID"] = 0;
                        drNewRow["SurveyorID"] = GlobalSettings.UserID;
                        drNewRow["Reactive"] = 0;
                        drNewRow["MapStartTypeID"] = 0;  // Region //
                        drNewRow["LastMapRecordID"] = 0;
                        drNewRow["LastMapX"] = 0.00;
                        drNewRow["LastMapY"] = 0.00;
                        drNewRow["TotalIncome"] = 0.00;
                        drNewRow["ReactiveCategoryID"] = 0;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07103_UT_Survey_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToClientID;
                        drNewRow["ClientName"] = strLinkedToClientName;
                        drNewRow["SurveyDate"] = DateTime.Today;
                        drNewRow["SurveyStatusID"] = 0;
                        drNewRow["SurveyorID"] = GlobalSettings.UserID;
                        drNewRow["Reactive"] = 0;
                        drNewRow["MapStartTypeID"] = 0;  // Region //
                        drNewRow["LastMapRecordID"] = 0;
                        drNewRow["LastMapX"] = 0.00;
                        drNewRow["LastMapY"] = 0.00;
                        this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows.Add(drNewRow);
                        this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                        iBool_AllowDelete = false;
                        iBool_AllowAdd = false;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07103_UT_Survey_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07103_UT_Survey_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    Load_Surveyed_Poles();
                    Load_Survey_Costings();

                    if (strFormMode == "view")
                    {
                        iBool_AllowAdd = false;
                        iBool_AllowEdit = false;
                        iBool_AllowDelete = false;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DevExpress.XtraTab.XtraTabControl) this.Attach_EditValueChanged_To_Children(item.Controls);  // Required for controls within Panels //
                if (item2 is DevExpress.XtraTab.XtraTabPage) this.Attach_EditValueChanged_To_Children(item.Controls);  // Required for controls within Panels //
                if (item2 is PanelControl) this.Attach_EditValueChanged_To_Children(item.Controls);  // Required for controls within Panels //
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                if (item2 is DevExpress.XtraTab.XtraTabControl) this.Detach_EditValuechanged_From_Children(item.Controls);  // Required for controls within Panels //
                if (item2 is DevExpress.XtraTab.XtraTabPage) this.Detach_EditValuechanged_From_Children(item.Controls);  // Required for controls within Panels //
                if (item2 is PanelControl) this.Detach_EditValuechanged_From_Children(item.Controls);  // Required for controls within Panels //
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = true;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = false;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ClientNameButtonEdit.Focus();

                        ClientNameButtonEdit.Properties.ReadOnly = true;
                        ClientNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                btnCalculateTotals.Enabled = false;
            }
            else
            {
                Set_ClientPONumber_Enabled_Status(null);
                btnCalculateTotals.Enabled = (strFormMode != "add");  // Don't switch on until after first save //
            }
            SurveyDateDateEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ContractYearSpinEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ContractTypeIDGridLookUpEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ClientPONumberTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            QuoteReceivedDateDateEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            QuoteReceivedDateDateEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ExchequerNumberTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            QuoteApprovedDateDateEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            CopyAddressButton1.Enabled = (GlobalSettings.SystemDataTransferMode.ToLower() != "tablet");
            ReportedByNameTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ReportedByAddressMemoEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ReportedByPostcodeTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ReportedByTelephoneTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ReportedByEmailTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            CopyAddressButton2.Enabled = (GlobalSettings.SystemDataTransferMode.ToLower() != "tablet");
            IncidentAddressMemoEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            IncidentPostcodeTextEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
            ReactiveRemarksMemoEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() == "tablet");
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            /*SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9153", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9153:  // Size Bands Picklist //    
                        {
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }*/
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            sp07107UTSurveyItemLinkedSurveyedPolesBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //

            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intSurveyID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow != null)
            {
                intSurveyID = (currentRow["SurveyID"] == null ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            }

            if (i_int_FocusedGrid == 1)  // Surveyed Poles //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit" && intSurveyID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && intSurveyID > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && intSurveyID > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            if (i_int_FocusedGrid == 2)  // Costing Headers //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit" && intSurveyID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && intSurveyID > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && intSurveyID > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            if (i_int_FocusedGrid == 6)  // Costing Items //
            {
                view = (GridView)gridControl6.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit" && intSurveyID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && intSurveyID > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            if (i_int_FocusedGrid == 7)  // Permission Documents //
            {
                view = (GridView)gridControl7.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowEdit && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            btnLoadMapWithSurvey.Enabled = (strFormMode == "add" || strFormMode == "edit") && view.DataRowCount > 0;

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && strFormMode != "blockedit" && intSurveyID > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 && iBool_AllowEdit && strFormMode != "blockedit" && intSurveyID > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && iBool_AllowDelete && intSurveyID > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length == 1 && iBool_AllowEdit && strFormMode != "blockedit" && intSurveyID > 0);

            // Set enabled status of GridView2 navigator custom buttons //
            GridView parentView = (GridView)gridControl2.MainView;
            int[] intParentRowHandles = parentView.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd && strFormMode != "blockedit" && intSurveyID > 0;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = iBool_AllowEdit && strFormMode != "blockedit" && intSurveyID > 0 && intParentRowHandles.Length > 0;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = iBool_AllowDelete && intSurveyID > 0 && intParentRowHandles.Length > 0;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intSurveyID > 0 && intParentRowHandles.Length > 0;
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = iBool_AllowEdit && strFormMode != "blockedit" && intSurveyID > 0 && intParentRowHandles.Length == 1;

            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd && strFormMode != "blockedit" && intParentRowHandles.Length == 1;
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = iBool_AllowDelete && intRowHandles.Length > 0;

            // Set enabled status of GridView7 navigator custom buttons //
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length == 1;
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode != "blockedit" && intRowHandles.Length == 1;

            barCheckItemSurveyCostings.Enabled = intSurveyID > 0;  // Survey Costings page only enabled if Survey has been saved //
        }


        private void frm_UT_Survey_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Surveyed_Poles();
                    FilterGrids();
                }
                else if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDs2))
                {
                    Load_Survey_Costings();
                    FilterGrids();
                }
                else if (UpdateRefreshStatus == 3)
                {
                    Load_Linked_Permission_Documents();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Survey_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07103UTSurveyItemBindingSource.EndEdit();
            try
            {
                this.sp07103_UT_Survey_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view = (GridView)gridControl3.MainView;
            view.PostEditor();
            view = (GridView)gridControl6.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["SurveyID"]) + ";";
                    this.strRecordIDs = Convert.ToInt32(currentRow["SurveyID"]) + ",";
                }

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    btnCalculateTotals.Enabled = true;
                }
            }


            this.sp07221UTSurveyCostingPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            try  // Attempt to save any changes to Linked Cost Poles... //
            {
                sp07221_UT_Survey_Costing_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked costing pole changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            this.sp07222UTSurveyCostingItemBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            try  // Attempt to save any changes to Linked Costing Items... //
            {
                sp07222_UT_Survey_Costing_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked costing item changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Manager")
                    {
                        var fParentForm = (frm_UT_Survey_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07103_UT_Survey_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            GridView view = (GridView)gridControl3.MainView;
            view.PostEditor();
            int intPolesNew = 0;
            int intPolesModified = 0;
            int intPolesDeleted = 0;
            this.sp07221UTSurveyCostingPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07221_UT_Survey_Costing_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07221_UT_Survey_Costing_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intPolesNew++;
                        break;
                    case DataRowState.Modified:
                        intPolesModified++;
                        break;
                    case DataRowState.Deleted:
                        intPolesDeleted++;
                        break;
                }
            }

            view = (GridView)gridControl6.MainView;
            view.PostEditor();
            int intItemsNew = 0;
            int intItemsModified = 0;
            int intItemsDeleted = 0;
            this.sp07222UTSurveyCostingItemBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intItemsNew++;
                        break;
                    case DataRowState.Modified:
                        intItemsModified++;
                        break;
                    case DataRowState.Deleted:
                        intItemsDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intPolesNew > 0 || intPolesModified > 0 || intPolesDeleted > 0 || intItemsNew > 0 || intItemsModified > 0 || intItemsDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Survey record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Survey record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Survey record(s)\n";

                if (intPolesNew > 0) strMessage += Convert.ToString(intPolesNew) + " New Costing Pole record(s)\n";
                if (intPolesModified > 0) strMessage += Convert.ToString(intPolesModified) + " Updated Costing Pole record(s)\n";
                if (intPolesDeleted > 0) strMessage += Convert.ToString(intPolesDeleted) + " Deleted Costing Pole record(s)\n";

                if (intItemsNew > 0) strMessage += Convert.ToString(intItemsNew) + " New Costing Item record(s)\n";
                if (intItemsModified > 0) strMessage += Convert.ToString(intItemsModified) + " Updated Costing Item record(s)\n";
                if (intItemsDeleted > 0) strMessage += Convert.ToString(intItemsDeleted) + " Deleted Costing Item record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            // Check for unsaved changes in the costing and prompt user if any //
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before moving to the next record?\n\nif you click No these changes will be lost!";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Change Viewed Survey - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort record navigation //
                        e.Handled = true;
                        return;
                    case DialogResult.No:
                        // Proceed with record navigation and don't save //
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true)))  // Save Failed so abort record navigation to allow user to correct //
                        {
                            e.Handled = true;
                            return;
                        }
                        break;
                }
            }
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                    Load_Surveyed_Poles();
                    Load_Survey_Costings();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ClientNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                frm_EP_Select_Client fChildForm = new frm_EP_Select_Client();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalClientID = intClientID;
                fChildForm.intFilterUtilityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    sp07103UTSurveyItemBindingSource.EndEdit();
                }
            }
        }
        private void ClientNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientNameButtonEdit, "");
            }
        }

        private void SurveyorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SurveyorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SurveyorIDGridLookUpEdit, "");
            }
        }

        private void ContractTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ContractTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = Convert.ToInt32(glue.EditValue);

            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intValue2 = Convert.ToInt32(currentRow["Reactive"]);
            if (intValue == 2 && intValue2 != 1)  // Contract Type = Reactive and Reactive Tickbox not ticked //
            {
                currentRow["Reactive"] = 1;
                sp07103UTSurveyItemBindingSource.EndEdit();
            }
            if (GlobalSettings.SystemDataTransferMode.ToLower() == "desktop") Set_ClientPONumber_Enabled_Status(null);  // Don't fire if in Tablet mode //

        }

        private void LastMapRecordDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow != null)
            {
                int intRecordTypeID = Convert.ToInt32(currentRow["MapStartTypeID"]);
                int intLastMapRecordID = Convert.ToInt32(currentRow["LastMapRecordID"]);
                int intClientID = Convert.ToInt32(currentRow["ClientID"]);
                switch (intRecordTypeID)
                {
                    case 0:  // Utilties Region //
                        {
                            frm_UT_Select_Region fChildForm = new frm_UT_Select_Region();
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.intPassedInRegionID = intLastMapRecordID;
                            fChildForm.intPassedInClientID = intClientID;
                            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                            {
                                currentRow["ClientID"] = fChildForm.intSelectedClientID;
                                currentRow["ClientName"] = fChildForm.strSelectedClientName;
                                currentRow["LastMapRecordID"] = fChildForm.intSelectedRegionID;
                                currentRow["LastMapRecordDescription"] = fChildForm.strSelectedValue;
                                currentRow["WorkspaceID"] = fChildForm.intSelectedWorkspaceID;
                                currentRow["WorkspaceName"] = fChildForm.strSelectedWorkspace;
                                sp07103UTSurveyItemBindingSource.EndEdit();
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }
        
        private void ReactiveCheckEdit_Validated(object sender, EventArgs e)
        {
            if (GlobalSettings.SystemDataTransferMode.ToLower() == "desktop") Set_ClientPONumber_Enabled_Status((CheckEdit)sender);  // Don't fire if in Tablet mode //
        }
        private void Set_ClientPONumber_Enabled_Status(CheckEdit ce)
        {
            bool boolReactive = false;
            if (strFormMode == "view") return;
            if (ce == null)
            {
                DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
                if (currentRow == null) return;
                boolReactive = (Convert.ToInt32(currentRow["Reactive"]) == 0 ? false : true);
            }
            else
            {
                boolReactive = ce.Checked;
            }
            if (!boolReactive)
            {
                DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
                if (currentRow == null) return;
                ClientPONumberTextEdit.Properties.ReadOnly = true;
                ExchequerNumberTextEdit.Properties.ReadOnly = true;
                QuoteReceivedDateDateEdit.Properties.ReadOnly = true;
                QuoteApprovedDateDateEdit.Properties.ReadOnly = true;
                TotalCostSpinEdit.Properties.ReadOnly = true;
                TotalIncomeSpinEdit.Properties.ReadOnly = true;

                ReportedByNameTextEdit.Properties.ReadOnly = true;
                ReportedByAddressMemoEdit.Properties.ReadOnly = true;
                ReportedByPostcodeTextEdit.Properties.ReadOnly = true;
                ReportedByTelephoneTextEdit.Properties.ReadOnly = true;
                ReportedByEmailTextEdit.Properties.ReadOnly = true;
                IncidentAddressMemoEdit.Properties.ReadOnly = true;
                IncidentPostcodeTextEdit.Properties.ReadOnly = true;
                ReactiveCategoryIDGridLookUpEdit.Properties.ReadOnly = true;
                ReactiveRemarksMemoEdit.Properties.ReadOnly = true;

                CopyAddressButton1.Enabled = false;
                CopyAddressButton2.Enabled = false;

                dxErrorProvider1.SetError(ClientPONumberTextEdit, "");

                if (!ibool_FormStillLoading)
                {
                    if (!string.IsNullOrEmpty(currentRow["ClientPONumber"].ToString())) currentRow["ClientPONumber"] = "";  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ExchequerNumber"].ToString())) currentRow["ExchequerNumber"] = "";  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["QuoteReceivedDate"].ToString())) currentRow["QuoteReceivedDate"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["QuoteApprovedDate"].ToString())) currentRow["QuoteApprovedDate"] = DBNull.Value;  // Clear as this is not a reactive survey //

                    if (!string.IsNullOrEmpty(currentRow["ReportedByName"].ToString())) currentRow["ReportedByName"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReportedByAddress"].ToString())) currentRow["ReportedByAddress"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReportedByPostcode"].ToString())) currentRow["ReportedByPostcode"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReportedByTelephone"].ToString())) currentRow["ReportedByTelephone"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReportedByEmail"].ToString())) currentRow["ReportedByEmail"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["IncidentAddress"].ToString())) currentRow["IncidentAddress"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["IncidentPostcode"].ToString())) currentRow["IncidentPostcode"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReactiveCategoryID"].ToString())) currentRow["ReactiveCategoryID"] = DBNull.Value;  // Clear as this is not a reactive survey //
                    if (!string.IsNullOrEmpty(currentRow["ReactiveRemarks"].ToString())) currentRow["ReactiveRemarks"] = DBNull.Value;  // Clear as this is not a reactive survey //

                    Calculate_Total_Costs();  // Calculate Totals since this is not reactive //
                    sp07103UTSurveyItemBindingSource.EndEdit();
                }
            }
            else
            {
                ClientPONumberTextEdit.Properties.ReadOnly = false;
                ExchequerNumberTextEdit.Properties.ReadOnly = false;
                QuoteReceivedDateDateEdit.Properties.ReadOnly = false;
                QuoteApprovedDateDateEdit.Properties.ReadOnly = false;
                TotalCostSpinEdit.Properties.ReadOnly = false;
                TotalIncomeSpinEdit.Properties.ReadOnly = false;

                ReportedByNameTextEdit.Properties.ReadOnly = false;
                ReportedByAddressMemoEdit.Properties.ReadOnly = false;
                ReportedByPostcodeTextEdit.Properties.ReadOnly = false;
                ReportedByTelephoneTextEdit.Properties.ReadOnly = false;
                ReportedByEmailTextEdit.Properties.ReadOnly = false;
                IncidentAddressMemoEdit.Properties.ReadOnly = false;
                IncidentPostcodeTextEdit.Properties.ReadOnly = false;
                ReactiveCategoryIDGridLookUpEdit.Properties.ReadOnly = false;
                ReactiveRemarksMemoEdit.Properties.ReadOnly = (GlobalSettings.SystemDataTransferMode.ToLower() != "desktop");

                CopyAddressButton1.Enabled = true;
                CopyAddressButton2.Enabled = true;

                DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
                if (currentRow != null)
                {
                    string strPONumber = currentRow["ClientPONumber"].ToString();
                    dxErrorProvider1.SetError(ClientPONumberTextEdit, (string.IsNullOrWhiteSpace(strPONumber) ? "Enter a value if Reactive ticked." : ""));
                }
            }
        }

        private void ClientPONumberTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (strFormMode == "view") return;
            TextEdit te = (TextEdit)sender;
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;

            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()) && Convert.ToInt32(currentRow["Reactive"]) != 0)
            {
                dxErrorProvider1.SetError(ClientPONumberTextEdit, "Enter a value if Reactive ticked.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ClientPONumberTextEdit, "");
            }
        }

        private void btnCalculateTotals_Click(object sender, EventArgs e)
        {
            Calculate_Total_Costs();
        }

        private void Calculate_Total_Costs()
        {
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            if (intSurveyID <= 0) return;
            DataSet_UT_EditTableAdapters.QueriesTableAdapter CalculateTotals = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            CalculateTotals.ChangeConnectionString(strConnectionString);
            try
            {
                currentRow["TotalIncome"] = Convert.ToDecimal(CalculateTotals.sp07385_UT_Calculate_Survey_Total_Income(intSurveyID));
                sp07103UTSurveyItemBindingSource.EndEdit();
            }
            catch (Exception)
            {
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Surveyed Poles NOT Shown When Block Adding and Block Editing" : "No Linked Surveyed Poles Available - Click the Add button to Create Linked Surveyed Poles");
                    break;
                case "gridView2":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Costings NOT Shown When Block Adding and Block Editing" : "No Linked Costings Available - Click the Add button to Create Linked Costings");
                    break;
                case "gridVie3":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Costing Sites NOT Shown When Block Adding and Block Editing" : "No Linked Costing Sites Available - Select a Costing Header to View Linked Sites");
                    break;
                case "gridView6":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Costing Items NOT Shown When Block Adding and Block Editing" : "No Linked Costing Items Available - Select a Costing Header to View Linked Costing Items");
                    break;
                case "gridView7":
                    message = "No Permission Documents - Select one or more Surveyed Poles to load linked Permission Documents";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            //if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                if (!iBool_AllowEdit) return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            
            Load_Linked_Permission_Documents();
            GridView view = (GridView)gridControl7.MainView;
            view.ExpandAllGroups();

            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        Load_Surveyed_Poles();
                        FilterGrids();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.Name)
            {
                case "colTrafficMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "TrafficMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
                case "colAccessMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "AccessMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "TrafficMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "AccessMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("TrafficMapPath").ToString())) e.Cancel = true;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("AccessMapPath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                if (!iBool_AllowEdit) return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl2.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("clone".Equals(e.Button.Tag))
                    {
                        Clone_Costings();
                    }
                    break;
                default:
                    break;
            }
        }

        bool internalRowFocusing;
        private void gridView2_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            // Note grid's MouseDown event also updated to handle Expand and Contract button clicking //
            GridView view = (GridView)sender;
            if (internalRowFocusing) return;

            // Check for unsaved changes in the costings and prompt user if any //
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before changing the viewed costing set?\n\nif you click No these changes will be lost!";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Change Viewed Costing Set - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort cloning //
                        return;
                    case DialogResult.No:
                        // Proceed with clone and don't save //
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;// Save Failed so abort record clone to allow user to correct //
                        break;
                }
            }

            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            Load_Survey_Costing_Poles();
            Load_Survey_Costing_Items();
            view = (GridView)gridControl6.MainView;
            view.ExpandAllGroups();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void Clone_Costings()
        {
            if (strFormMode == "view") return;

            GridView view = (GridView)gridControl2.MainView; ;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the costing record to be cloned before proceeding.", "Clone Costings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clone the selected costing data - are you sure you wish to proceed?", "Clone Costings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            
            // Check for unsaved changes in the costing and prompt user if any //
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before cloning the costs record?\n\nif you click No these changes will be lost!";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Clone Costings - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort cloning //
                        return;
                    case DialogResult.No:
                        // Proceed with clone and don't save //
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) return;// Save Failed so abort record clone to allow user to correct //
                        break;
                }
            }

            int intSurveyCostingHeader = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyCostingHeaderID"));
            if (intSurveyCostingHeader <= 0) return;

            DataSet_UT_EditTableAdapters.QueriesTableAdapter CloneRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            CloneRecords.ChangeConnectionString(strConnectionString);
            int intNewID = 0;
            try
            {
                intNewID = Convert.ToInt32(CloneRecords.sp07230_UT_Survey_Costings_Clone(intSurveyCostingHeader, GlobalSettings.UserID));
            }
            catch (Exception)
            {
            }
            if (intNewID > 0)  // Load Selected Costings //
            {
                i_str_AddedRecordIDs2 = intNewID.ToString() + ";";
                Load_Survey_Costings();
      
                if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
                {
                    SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                }
            }

        }

        #endregion


        #region GridView3

        private void bandedGridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void bandedGridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            BandedGridView view = sender as BandedGridView;
            downHitInfo = null;

            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                string strColumn = view.FocusedColumn.Name;
                if (strColumn.EndsWith("Poles"))
                {
                    bbiAddPoles.Enabled = true;
                }
                else
                {
                    bbiAddPoles.Enabled = false;
                }
                pmSurveyPoles.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }


        private void bbiAddPoles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;

            GridView view = (GridView)gridControl3.MainView;
            string strColumnDescription = view.FocusedColumn.Name;
            if (!strColumnDescription.EndsWith("Poles"))
            {
                return;
            }
            strColumnDescription = strColumnDescription.Remove(0, 3);  // Remove leading "col" from column name //
            string strColumnID = strColumnDescription.Substring(0, strColumnDescription.Length - (strColumnDescription.Length - strColumnDescription.IndexOf("Poles"))) + "PoleIDs";

            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            frm_UT_Select_Survey_Costing_Pole fChildForm = new frm_UT_Select_Survey_Costing_Pole();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInSurveyID = intSurveyID;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            string strSelectedIDs = fChildForm.strSelectedIDs;
            string strSelectedDescriptions = fChildForm.strSelectedPoleNumbers;
            if (string.IsNullOrEmpty(strSelectedIDs)) return;
            
            currentRow = (DataRowView)sp07221UTSurveyCostingPoleBindingSource.Current;
            if (currentRow == null) return;
            string strOriginalIDs = currentRow[strColumnID].ToString();
            string strOriginalDescriptions = currentRow[strColumnDescription].ToString();

            if (!string.IsNullOrEmpty(strOriginalIDs))
            {
                if (!strOriginalIDs.EndsWith(",")) strOriginalIDs += ", ";
            }
            strOriginalIDs += strSelectedIDs;

            if (!string.IsNullOrEmpty(strOriginalDescriptions))
            {
                if (!strOriginalDescriptions.EndsWith(",")) strOriginalDescriptions += ", ";
            }
            strOriginalDescriptions += strSelectedDescriptions;

            currentRow[strColumnDescription] = strOriginalDescriptions;
            currentRow[strColumnID] = strOriginalIDs;
            sp07221UTSurveyCostingPoleBindingSource.EndEdit();
            
            view.PostEditor();
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void bbiClearPoles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;

            GridView view = (GridView)gridControl3.MainView;
            string strColumnDescription = view.FocusedColumn.Name;
            if (!strColumnDescription.EndsWith("Poles"))
            {
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the linked surveyed poles from this costing - are you sure you wish to proceed?", "Clear Linked Surveyed Poles from Costing", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
            
            strColumnDescription = strColumnDescription.Remove(0, 3);  // Remove leading "col" from column name //
            string strColumnID = strColumnDescription.Substring(0, strColumnDescription.Length - (strColumnDescription.Length - strColumnDescription.IndexOf("Poles"))) + "PoleIDs";

            DataRowView currentRow = (DataRowView)sp07221UTSurveyCostingPoleBindingSource.Current;
            if (currentRow == null) return;

            currentRow[strColumnDescription] = "";
            currentRow[strColumnID] = "";
            sp07221UTSurveyCostingPoleBindingSource.EndEdit();

            view.PostEditor();
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }




        #endregion


        #region GridView6

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                 pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl6.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_LeftCoordChanged(object sender, EventArgs e)
        {
            bandedGridView3.LeftCoord = gridView6.LeftCoord;
        }

        private void gridView6_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            // Custom sorting is enabled for the GroupName column (its GridColumn.SortMode property is set to ColumnSortMode.Custom). //
            GridView view = (GridView)sender;
            if (e.Column.FieldName == "TypeDescription")
            {
                int val1 = Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex1, "TypeOrder"));
                int val2 = Convert.ToInt32(view.GetListSourceRowCellValue(e.ListSourceRowIndex2, "TypeOrder"));
                e.Handled = true;
                e.Result = val1 > val2 ? 1 : val1 == val2 ? 0 : -1;
            }
        }

        private void gridView6_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LiveWorkCost":
                case "SD1Cost":
                case "SD2Cost":
                case "SD3Cost":
                case "SD4Cost":
                case "SD5Cost":
                case "SD6Cost":
                case "Year3DeferralCost":
                case "Year5DeferralCost":
                case "Year1RevisitCost":
                case "Year2RevisitCost":
                case "Year3RevisitCost":
                case "Year4RevisitCost":
                case "Year5RevisitCost":
                    if (Convert.ToBoolean(barEditItemEditTotals.EditValue) == false) e.RepositoryItem = disabledCostTotalEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView6_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent showing of editor if barEditItem EditTotals is false //
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LiveWorkCost":
                case "SD1Cost":
                case "SD2Cost":
                case "SD3Cost":
                case "SD4Cost":
                case "SD5Cost":
                case "SD6Cost":
                case "Year3DeferralCost":
                case "Year5DeferralCost":
                case "Year1RevisitCost":
                case "Year2RevisitCost":
                case "Year3RevisitCost":
                case "Year4RevisitCost":
                case "Year5RevisitCost":
                    if (Convert.ToBoolean(barEditItemEditTotals.EditValue) == false) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }


        private void repositoryItemSpinEditUnits_EditValueChanged(object sender, EventArgs e)
        {
            gridView6.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }
        private void repositoryItemSpinEditUnits_Validating(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Grid_Row_Unit_Totals(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
            Calculate_Column_Cost(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
        }

        private void repositoryItemSpinEditCost_EditValueChanged(object sender, EventArgs e)
        {
            gridView6.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }
        private void repositoryItemSpinEditCost_Validating(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)gridControl6.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Grid_Row_Cost_Totals(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);
        }



        private void Calculate_Grid_Row_Unit_Totals(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotal = (decimal)0.00;
            decTotal = (strCurrentColumn == "LiveWorkUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "LiveWorkUnits"))) +
                        (strCurrentColumn == "SD1Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD1Units"))) +
                        (strCurrentColumn == "SD2Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD2Units"))) +
                        (strCurrentColumn == "SD3Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD3Units"))) +
                        (strCurrentColumn == "SD4Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD4Units"))) +
                        (strCurrentColumn == "SD5Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD5Units"))) +
                        (strCurrentColumn == "SD6Units" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD6Units"))) +
                        (strCurrentColumn == "Year3DeferralUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year3DeferralUnits"))) +
                        (strCurrentColumn == "Year5DeferralUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year5DeferralUnits"))) +
                        (strCurrentColumn == "Year1RevisitUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year1RevisitUnits"))) +
                        (strCurrentColumn == "Year2RevisitUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year2RevisitUnits"))) +
                        (strCurrentColumn == "Year3RevisitUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year3RevisitUnits"))) +
                        (strCurrentColumn == "Year4RevisitUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year4RevisitUnits"))) +
                        (strCurrentColumn == "Year5RevisitUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year5RevisitUnits")));
            view.SetRowCellValue(intRow, "TotalUnits", decTotal);
        }
        private void Calculate_Column_Cost(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decRate = Convert.ToDecimal(view.GetRowCellValue(intRow, "Rate"));
            decimal decTotal = decRate * decValue;
            string strColumnToSet = strCurrentColumn.Substring(0, strCurrentColumn.Length - (strCurrentColumn.Length - strCurrentColumn.IndexOf("Units"))) + "Cost";
            view.SetRowCellValue(intRow, strColumnToSet, decTotal);
            Calculate_Grid_Row_Cost_Totals(view, intRow, strCurrentColumn, decTotal);
        }


        private void Calculate_Grid_Row_Cost_Totals(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            decimal decTotal = (decimal)0.00;
            decTotal = (strCurrentColumn == "LiveWorkCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "LiveWorkCost"))) +
                        (strCurrentColumn == "SD1Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD1Cost"))) +
                        (strCurrentColumn == "SD2Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD2Cost"))) +
                        (strCurrentColumn == "SD3Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD3Cost"))) +
                        (strCurrentColumn == "SD4Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD4Cost"))) +
                        (strCurrentColumn == "SD5Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD5Cost"))) +
                        (strCurrentColumn == "SD6Cost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SD6Cost"))) +
                        (strCurrentColumn == "Year3DeferralCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year3DeferralCost"))) +
                        (strCurrentColumn == "Year5DeferralCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year5DeferralCost"))) +
                        (strCurrentColumn == "Year1RevisitCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year1RevisitCost"))) +
                        (strCurrentColumn == "Year2RevisitCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year2RevisitCost"))) +
                        (strCurrentColumn == "Year3RevisitCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year3RevisitCost"))) +
                        (strCurrentColumn == "Year4RevisitCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year4RevisitCost"))) +
                        (strCurrentColumn == "Year5RevisitCost" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "Year5RevisitCost")));
            view.SetRowCellValue(intRow, "TotalCost", decTotal);
            if (IsCostingHeaderLive()) Calculate_Total_Survey_Cost();  // Update main survey Total Cost if Costing Set is Live //
        }


        #endregion


        #region GridView7

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        Load_Linked_Permission_Documents();
                    }
                   break;
                default:
                    break;
            }
        }

        private void gridView7_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 7;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 7;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView7_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView7_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SignatureFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PDFFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedToDoActionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToDoActionCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView7_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SignatureFile").ToString())) e.Cancel = true;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PDFFile").ToString())) e.Cancel = true;
                    break;
                case "LinkedActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedActionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedToDoActionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedToDoActionCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView7_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "LinkedActionCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedActionCount"));

                if (intValue <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else
                {
                    int intValue2 = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "PermissionedCount"));
                    if (intValue2 >= intValue)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                        e.Appearance.BackColor2 = Color.PaleGreen;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
            else if (e.Column.FieldName == "LinkedToDoActionCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToDoActionCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "NotPermissionedCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "NotPermissionedCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "OnHoldCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "OnHoldCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colSignatureFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "SignatureFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strSignaturePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    System.Diagnostics.Process.Start(strFilePath);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (view.FocusedColumn.Name == "colPDFFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PDFFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strPermissionFilePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    //System.Diagnostics.Process.Start(strFilePath);

                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFilePath;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (intSurveyID <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Open Mapping in Survey Mode - Save changes to this survey first then try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                        int intWorkSpaceID = (string.IsNullOrEmpty(currentRow["WorkspaceID"].ToString()) ? 0 : Convert.ToInt32(currentRow["WorkspaceID"]));
                        if (intWorkSpaceID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Open Mapping in Survey Mode - Select a Map Start Record with a linked Mapping Workspace then Save and try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        string strWorkspaceName = (string.IsNullOrEmpty(currentRow["WorkspaceName"].ToString()) ? "" : currentRow["WorkspaceName"].ToString());
                        string strSurveyDescription = "";

                        // Get Surveyor Name from grid lookup Edit //
                        string strSurveyor = "Unknown Surveyor";
                        GridLookUpEdit glue = SurveyorIDGridLookUpEdit;
                        if (!(glue.EditValue == DBNull.Value || string.IsNullOrEmpty(glue.EditValue.ToString()))) strSurveyor = glue.Text;

                        strSurveyDescription = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientName"])) ? "Unknown Client" : Convert.ToString(currentRow["ClientName"])) + " \\ " +
                            (String.IsNullOrEmpty(Convert.ToString(currentRow["SurveyDate"])) ? "Unknown Date" : Convert.ToDateTime(currentRow["SurveyDate"]).ToString("dd/MM/yyyy")) + " \\ " +
                            strSurveyor;

                        // Open Map and show Survey Panel //
                        int intWorkspaceOwner = 0;
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

                        frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();

                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance._SelectedSurveyID = intSurveyID;
                        frmInstance._SelectedSurveyClientID = intClientID;
                        frmInstance._SelectedSurveyDescription = strSurveyDescription;
                        frmInstance._PassedInClientIDs = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientID"])) ? "" : Convert.ToString(currentRow["ClientID"]) + ",");
                        frmInstance._PassedInRegionIDs = (String.IsNullOrEmpty(Convert.ToString(currentRow["LastMapRecordID"])) ? "" : Convert.ToString(currentRow["LastMapRecordID"]) + ",");
                        frmInstance._SurveyMode = "AddPolesToSurvey";
                        frmInstance.Show();
                        // Invoke Post Open event on Base form //
                        method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null)
                        {
                            method.Invoke(frmInstance, new object[] { null });
                        }
                    }
                    break;
                case 2:     // Costing Header //
                    {
                        if (!iBool_AllowAdd) return;
                        if (intSurveyID <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Add Costing Header - Save changes to this survey first then try again.", "Add Costing Header", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Costing_Header_Edit fChildForm = new frm_UT_Survey_Costing_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        fChildForm.intLinkedToRecordID = intSurveyID;
                        fChildForm.strLinkedToRecordDesc = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientName"])) ? "Unknown Client" : Convert.ToString(currentRow["ClientName"])) + " - Date " +
                            (String.IsNullOrEmpty(Convert.ToString(currentRow["SurveyDate"])) ? "Unknown Date" : Convert.ToDateTime(currentRow["SurveyDate"]).ToString("dd/MM/yyyy"));
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 6:     // Costing Item //
                    {
                        if (!iBool_AllowAdd) return;
                        if (intSurveyID <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Add Costing Item - Save changes to this survey first then try again.", "Add Costing Item", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }

                        view = (GridView)gridControl2.MainView; ;
                        if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select the costing record from the top grid to add to before proceeding.", "Add Costing Item", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intSurveyCostingHeader = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyCostingHeaderID"));
                        if (intSurveyCostingHeader <= 0) return;

                        view = (GridView)gridControl6.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //

                        frm_UT_Survey_Edit_Costing_Item_Add fChildForm = new frm_UT_Survey_Edit_Costing_Item_Add();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        
                        // Get Type Order //
                        int intTypeOrder = 0;
                        DataSet_UTTableAdapters.QueriesTableAdapter GetOrder = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetOrder.ChangeConnectionString(strConnectionString);
                        try
                        {
                            intTypeOrder = Convert.ToInt32(GetOrder.sp07232_UT_Survey_Costing_Cost_Type_Order(fChildForm.intTypeID));
                        }
                        catch (Exception)
                        {
                        }

                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = strRecordIDs;
                            drNewRow["SurveyCostingHeaderID"] = intSurveyCostingHeader;
                            drNewRow["CostingTypeID"] = fChildForm.intTypeID;
                            drNewRow["TypeOrder"] = intTypeOrder;
                            drNewRow["EquipmentID"] = fChildForm.intEquipmentID;
                            drNewRow["TypeDescription"] = fChildForm.strTypeDescription;
                            drNewRow["Description"] = fChildForm.strEquipmentDescription;
                            drNewRow["Rate"] = fChildForm.decEquipmentRate;
                            drNewRow["SortOrder"] = 0;
                            drNewRow["UnitDescriptorID"] = 6;
                            drNewRow["LiveWorkUnits"] = (decimal)0.00;
                            drNewRow["LiveWorkCost"] = (decimal)0.00;
                            drNewRow["SD1Units"] = (decimal)0.00;
                            drNewRow["SD1Cost"] = (decimal)0.00;
                            drNewRow["SD2Units"] = (decimal)0.00;
                            drNewRow["SD2Cost"] = (decimal)0.00;
                            drNewRow["SD3Units"] = (decimal)0.00;
                            drNewRow["SD3Cost"] = (decimal)0.00;
                            drNewRow["SD4Units"] = (decimal)0.00;
                            drNewRow["SD4Cost"] = (decimal)0.00;
                            drNewRow["SD5Units"] = (decimal)0.00;
                            drNewRow["SD5Cost"] = (decimal)0.00;
                            drNewRow["SD6Units"] = (decimal)0.00;
                            drNewRow["SD6Cost"] = (decimal)0.00;
                            drNewRow["Year3DeferralUnits"] = (decimal)0.00;
                            drNewRow["Year3DeferralCost"] = (decimal)0.00;
                            drNewRow["Year5DeferralUnits"] = (decimal)0.00;
                            drNewRow["Year5DeferralCost"] = (decimal)0.00;
                            drNewRow["Year1RevisitUnits"] = (decimal)0.00;
                            drNewRow["Year1RevisitCost"] = (decimal)0.00;
                            drNewRow["Year2RevisitUnits"] = (decimal)0.00;
                            drNewRow["Year2RevisitCost"] = (decimal)0.00;
                            drNewRow["Year3RevisitUnits"] = (decimal)0.00;
                            drNewRow["Year3RevisitCost"] = (decimal)0.00;
                            drNewRow["Year4RevisitUnits"] = (decimal)0.00;
                            drNewRow["Year4RevisitCost"] = (decimal)0.00;
                            drNewRow["Year5RevisitUnits"] = (decimal)0.00;
                            drNewRow["Year5RevisitCost"] = (decimal)0.00;
                            drNewRow["TotalUnits"] = (decimal)0.00;
                            drNewRow["TotalCost"] = (decimal)0.00;
                            this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.Rows.Add(drNewRow);
                        }
                        catch (Exception)
                        {
                        }
                        sp07222UTSurveyCostingItemBindingSource.EndEdit();

                    }
                    break;
           }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            switch (i_int_FocusedGrid)
            {
                case 1:     // Inspections //
                    {
                        if (intSurveyID <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Edit Surveyed Pole - Save changes to this survey first then try again.", "Edit Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one pole to survey before proceeding then try again.", "Edit Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strPoleIDs = "";
                        int intSurveyPoleID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strPoleIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                            intSurveyPoleID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyedPoleID"));
                        }

                        frm_UT_Survey_Pole2 fChildForm = new frm_UT_Survey_Pole2();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.strRecordIDs = strPoleIDs;
                        fChildForm.intPassedInSurveyPoleID = intSurveyPoleID;
                        fChildForm._SurveyID = intSurveyID;
                        fChildForm.strFormMode = "edit";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Costing Headers //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyCostingHeaderID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Costing_Header_Edit fChildForm = new frm_UT_Survey_Costing_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 7:  // Permission Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        string strLayout = "";
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to edit before proceeding.", "Edit Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += view.GetRowCellValue(intRowHandle, "PermissionDocumentID").ToString() + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //

                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            switch (i_int_FocusedGrid)
            {
                case 1:  // Surveyed Poles //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Surveyed Poles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Surveyed Pole" : Convert.ToString(intRowHandles.Length) + " Surveyed Poles") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Surveyed Pole" : "these Surveyed Poles") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyedPoleID")) + ",";
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp07000_UT_Delete("surveyed_pole", strRecordIDs);  // Remove the records from the DB in one go //
                            Load_Surveyed_Poles();
                            Load_Survey_Costings();
                            FilterGrids();
                            Calculate_Total_Survey_Income();  // Re-calculate the survey total income //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //SetChangesPendingLabel();
                        }
                        break;
                    }
                case 2:  // Costing Header //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl2.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Costing Headers to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Costing Header" : Convert.ToString(intRowHandles.Length) + " Costing Headers") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Costing Header" : "these Costing Headers") + " will no longer be available for selection and any related records will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyCostingHeaderID")) + ",";
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("costing_header", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Survey_Costings();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 6:  // Costing Item //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Costing Headers to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Costing " + (intRowHandles.Length == 1 ? "Item" : "Items") + " selected for delete!\n\nImportant Note: Costing Item Deletions are only finalised on clicking the Save button.\n\nProceed?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            gridControl6.BeginUpdate();
                            for (int intRowHandle = intRowHandles.Length - 1; intRowHandle >= 0; intRowHandle--)
                            {
                                 view.DeleteRow(intRowHandles[intRowHandle]);
                            }
                            gridControl6.EndUpdate();
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (IsCostingHeaderLive()) Calculate_Total_Survey_Cost();  // Update main survey Total Cost if Costing Set is Live //
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for deletion.\n\nImportant Note: Costing Item Deletions are only finalised on clicking the Save button.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        break;
                    }
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (intSurveyID <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to View Surveyed Pole - Save changes to this survey first then try again.", "View Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        view = null;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one pole to view surveyed results before proceeding then try again.", "View Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strPoleIDs = "";
                        int intSurveyPoleID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strPoleIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                            intSurveyPoleID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyedPoleID"));
                        }

                        frm_UT_Survey_Pole2 fChildForm = new frm_UT_Survey_Pole2();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.strRecordIDs = strPoleIDs;
                        fChildForm.intPassedInSurveyPoleID = intSurveyPoleID;
                        fChildForm._SurveyID = intSurveyID;
                        fChildForm.strFormMode = "view";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Costing Header //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyCostingHeaderID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Costing_Header_Edit fChildForm = new frm_UT_Survey_Costing_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 7:  // Permission Documents //
                    {
                        view = (GridView)gridControl7.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;

                        string strLayout = "";
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one record to view before proceeding.", "View Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += view.GetRowCellValue(intRowHandle, "PermissionDocumentID").ToString() + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //

                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
            }
        }


        public void Load_Surveyed_Poles()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            this.RefreshGridViewState1.SaveViewInfo();  // Store expanded groups and selected rows //
            gridControl1.BeginUpdate();
            try
            {
                sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter.Fill(dataSet_UT_Edit.sp07107_UT_Survey_Item_Linked_Surveyed_Poles, strRecordIDs);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
                Calculate_Total_Survey_Income();  // Re-calculate the survey total income //
            }
        }

        public void Load_Survey_Costings()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            this.RefreshGridViewState2.SaveViewInfo();  // Store expanded groups and selected rows //
            gridControl2.BeginUpdate();
            try
            {
                sp07220_UT_Survey_Costing_HeadersTableAdapter.Fill(dataSet_UT.sp07220_UT_Survey_Costing_Headers, strRecordIDs);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyCostingHeaderID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intRowHandle;
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
            if (view.DataRowCount <= 0)  // No Rows so reload linked records (will emtey their grids) //
            {
                Load_Survey_Costing_Poles();
                Load_Survey_Costing_Items();
            }
            if (IsCostingHeaderLive()) Calculate_Total_Survey_Cost();  // Update main survey Total Cost if Costing Set is Live //

        }

        public void Load_Survey_Costing_Poles()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            int intSelectedID = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                intSelectedID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyCostingHeaderID"]));
            }
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07221_UT_Survey_Costing_Pole.Clear();
            }
            else
            {
                sp07221_UT_Survey_Costing_PoleTableAdapter.Fill(dataSet_UT_Edit.sp07221_UT_Survey_Costing_Pole, intSelectedID);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyCostingPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }

        public void Load_Survey_Costing_Items()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            int intSelectedID = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                intSelectedID = Convert.ToInt32(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyCostingHeaderID"]));
            }
            gridControl6.MainView.BeginUpdate();
            this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.Clear();
            }
            else
            {
                sp07222_UT_Survey_Costing_ItemTableAdapter.Fill(dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item, intSelectedID);
                this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl6.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyCostingItemID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }

        public void Load_Linked_Permission_Documents()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            string strSurvededPoleIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSurvededPoleIDs += view.GetRowCellValue(intRowHandle, "SurveyedPoleID").ToString() + ",";
            }
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            this.RefreshGridViewState7.SaveViewInfo();  // Store expanded groups and selected rows //
            gridControl7.BeginUpdate();
            try
            {
                sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter.Fill(dataSet_UT_WorkOrder.sp07466_UT_Permission_Documents_For_Surveyed_Pole, strSurvededPoleIDs);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState7.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl7.EndUpdate();
        }

        public void FilterGrids()
        {
            string strSurveyID = "";
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow != null) strSurveyID = (currentRow["SurveyID"] == null ? "0" : currentRow["SurveyID"].ToString());

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[SurveyID] = " + Convert.ToString(strSurveyID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[SurveyID] = " + Convert.ToString(strSurveyID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyCostingHeaderID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
            FilterGrids();  // Required just in case this is a save for the first time (ID of record will change) //
        }

        private void btnLoadMapWithSurvey_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (view.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load survey into Mapping - the current Survey has no surveyed poles - click the add button below the Surveyed Poles grid to specify the poles to be surveyed before proceeding.", "Load Survey into Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            System.Reflection.MethodInfo method = null;
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
            int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));

            int intPoleID = 0;
            string strPoleNumber = "";
            if (intRowHandles.Length > 0)
            {
                intPoleID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PoleID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString());
            }
            else
            {
                intPoleID = (string.IsNullOrEmpty(view.GetRowCellValue(0, "PoleID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(0, "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(view.GetRowCellValue(0, "PoleNumber").ToString()) ? "" : view.GetRowCellValue(0, "PoleNumber").ToString());
            }
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Activate();
                        frm_UT_Mapping frmMapping = (frm_UT_Mapping)frmChild;
                        frmMapping._CurrentPoleID = intPoleID;
                        frmMapping._SelectedSurveyID = intSurveyID;
                        frmMapping._SurveyMode = "PlotTrees";
                        frmMapping.LoadSurveyIntoMap();
                        frmMapping.SetModeToSurvey(strPoleNumber);
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            // Get the Workspace ID from the Region on the Survey //
            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(intSurveyID).ToString();
                char[] delimiters = new char[] { '|' };
                string[] strArray = null;
                strArray = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray[0]);
                    strWorkspaceName = strArray[1].ToString();
                    intRegionID = Convert.ToInt32(strArray[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to plot tree via Mapping - the current Survey is linked to a region which has no Mapping Workspace ID.\n\nUpdate the Linked Region with a Mapping Workspace [via the Region Manager] then try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string strSurveyDescription = "";

            // Get Surveyor Name from grid lookup Edit //
            string strSurveyor = SurveyorIDGridLookUpEdit.Text;
            if (string.IsNullOrEmpty(strSurveyDescription)) strSurveyor = "Surveyor Not Specified";

            strSurveyDescription = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientName"])) ? "Unknown Client" : Convert.ToString(currentRow["ClientName"])) + " \\ " +
                (String.IsNullOrEmpty(Convert.ToString(currentRow["SurveyDate"])) ? "Unknown Date" : Convert.ToDateTime(currentRow["SurveyDate"]).ToString("dd/MM/yyyy")) + " \\ " +
                    strSurveyor;

            // Open Map and show Survey Panel //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSurveyID;
            frmInstance._SelectedSurveyClientID = intClientID;
            frmInstance._SelectedSurveyDescription = strSurveyDescription;
            frmInstance._PassedInClientIDs = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientID"])) ? "" : Convert.ToString(currentRow["ClientID"]) + ",");
            frmInstance._PassedInRegionIDs = (String.IsNullOrEmpty(intRegionID.ToString()) ? "" : intRegionID.ToString() + ",");
            frmInstance._SurveyMode = "PlotTrees";
            frmInstance._CurrentPoleID = intPoleID;
            frmInstance.SetModeToSurvey(strPoleNumber);
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }

            method = typeof(frm_UT_Mapping).GetMethod("SetToolToSelectPoint");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
        }


        private void barCheckItemSurveyDetails_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (barCheckItemSurveyDetails.Checked)
            {
                xtraTabControl1.SelectedTabPage = xtraTabPage1;
                barEditItemEditTotals.Enabled = false;
            }
        }

        private void barCheckItemSurveyCostings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (barCheckItemSurveyCostings.Checked)
            {
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
                barEditItemEditTotals.Enabled = true;
            }
        }

        private void repositoryItemCheckEditEditTotals_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                colLiveWorkCost.OptionsColumn.AllowEdit = true;
                colLiveWorkCost.OptionsColumn.AllowFocus = true;
                colLiveWorkCost.OptionsColumn.ReadOnly = false;
                colSD1Cost.OptionsColumn.AllowEdit = true;
                colSD1Cost.OptionsColumn.AllowFocus = true;
                colSD1Cost.OptionsColumn.ReadOnly = false;
                colSD2Cost.OptionsColumn.AllowEdit = true;
                colSD2Cost.OptionsColumn.AllowFocus = true;
                colSD2Cost.OptionsColumn.ReadOnly = false;
                colSD3Cost.OptionsColumn.AllowEdit = true;
                colSD3Cost.OptionsColumn.AllowFocus = true;
                colSD3Cost.OptionsColumn.ReadOnly = false;
                colSD4Cost.OptionsColumn.AllowEdit = true;
                colSD4Cost.OptionsColumn.AllowFocus = true;
                colSD4Cost.OptionsColumn.ReadOnly = false;
                colSD5Cost.OptionsColumn.AllowEdit = true;
                colSD5Cost.OptionsColumn.AllowFocus = true;
                colSD5Cost.OptionsColumn.ReadOnly = false;
                colSD6Cost.OptionsColumn.AllowEdit = true;
                colSD6Cost.OptionsColumn.AllowFocus = true;
                colSD6Cost.OptionsColumn.ReadOnly = false;
                colYear3DeferralCost.OptionsColumn.AllowEdit = true;
                colYear3DeferralCost.OptionsColumn.AllowFocus = true;
                colYear3DeferralCost.OptionsColumn.ReadOnly = false;
                colYear5DeferralCost.OptionsColumn.AllowEdit = true;
                colYear5DeferralCost.OptionsColumn.AllowFocus = true;
                colYear5DeferralCost.OptionsColumn.ReadOnly = false;
                colYear1RevisitCost.OptionsColumn.AllowEdit = true;
                colYear1RevisitCost.OptionsColumn.AllowFocus = true;
                colYear1RevisitCost.OptionsColumn.ReadOnly = false;
                colYear2RevisitCost.OptionsColumn.AllowEdit = true;
                colYear2RevisitCost.OptionsColumn.AllowFocus = true;
                colYear2RevisitCost.OptionsColumn.ReadOnly = false;
                colYear3RevisitCost.OptionsColumn.AllowEdit = true;
                colYear3RevisitCost.OptionsColumn.AllowFocus = true;
                colYear3RevisitCost.OptionsColumn.ReadOnly = false;
                colYear4RevisitCost.OptionsColumn.AllowEdit = true;
                colYear4RevisitCost.OptionsColumn.AllowFocus = true;
                colYear4RevisitCost.OptionsColumn.ReadOnly = false;
                colYear5RevisitCost.OptionsColumn.AllowEdit = true;
                colYear5RevisitCost.OptionsColumn.AllowFocus = true;
                colYear5RevisitCost.OptionsColumn.ReadOnly = false;
            }
            else
            {
                colLiveWorkCost.OptionsColumn.AllowEdit = false;
                colLiveWorkCost.OptionsColumn.AllowFocus = false;
                colLiveWorkCost.OptionsColumn.ReadOnly = true;
                colSD1Cost.OptionsColumn.AllowEdit = false;
                colSD1Cost.OptionsColumn.AllowFocus = false;
                colSD1Cost.OptionsColumn.ReadOnly = true;
                colSD2Cost.OptionsColumn.AllowEdit = false;
                colSD2Cost.OptionsColumn.AllowFocus = false;
                colSD2Cost.OptionsColumn.ReadOnly = true;
                colSD3Cost.OptionsColumn.AllowEdit = false;
                colSD3Cost.OptionsColumn.AllowFocus = false;
                colSD3Cost.OptionsColumn.ReadOnly = true;
                colSD4Cost.OptionsColumn.AllowEdit = false;
                colSD4Cost.OptionsColumn.AllowFocus = false;
                colSD4Cost.OptionsColumn.ReadOnly = true;
                colSD5Cost.OptionsColumn.AllowEdit = false;
                colSD5Cost.OptionsColumn.AllowFocus = false;
                colSD5Cost.OptionsColumn.ReadOnly = true;
                colSD6Cost.OptionsColumn.AllowEdit = false;
                colSD6Cost.OptionsColumn.AllowFocus = false;
                colSD6Cost.OptionsColumn.ReadOnly = true;
                colYear3DeferralCost.OptionsColumn.AllowEdit = false;
                colYear3DeferralCost.OptionsColumn.AllowFocus = false;
                colYear3DeferralCost.OptionsColumn.ReadOnly = true;
                colYear5DeferralCost.OptionsColumn.AllowEdit = false;
                colYear5DeferralCost.OptionsColumn.AllowFocus = false;
                colYear5DeferralCost.OptionsColumn.ReadOnly = true;
                colYear1RevisitCost.OptionsColumn.AllowEdit = false;
                colYear1RevisitCost.OptionsColumn.AllowFocus = false;
                colYear1RevisitCost.OptionsColumn.ReadOnly = true;
                colYear2RevisitCost.OptionsColumn.AllowEdit = false;
                colYear2RevisitCost.OptionsColumn.AllowFocus = false;
                colYear2RevisitCost.OptionsColumn.ReadOnly = true;
                colYear3RevisitCost.OptionsColumn.AllowEdit = false;
                colYear3RevisitCost.OptionsColumn.AllowFocus = false;
                colYear3RevisitCost.OptionsColumn.ReadOnly = true;
                colYear4RevisitCost.OptionsColumn.AllowEdit = false;
                colYear4RevisitCost.OptionsColumn.AllowFocus = false;
                colYear4RevisitCost.OptionsColumn.ReadOnly = true;
                colYear5RevisitCost.OptionsColumn.AllowEdit = false;
                colYear5RevisitCost.OptionsColumn.AllowFocus = false;
                colYear5RevisitCost.OptionsColumn.ReadOnly = true;
            }
        }


        private bool IsCostingHeaderLive()
        {
            DataRowView currentRow = (DataRowView)sp07220UTSurveyCostingHeadersBindingSource.Current;
            if (currentRow == null) return false;
            return (Convert.ToInt32(currentRow["Live"]) == 1 ? true : false);
        }
        private void Calculate_Total_Survey_Cost()
        {
            if (ibool_FormStillLoading) return;
            GridView view = (GridView)gridControl6.MainView;
            decimal decTotal = (decimal)0.00;
            foreach (DataRow dr in this.dataSet_UT_Edit.sp07222_UT_Survey_Costing_Item.Rows)
	        {
                decTotal += Convert.ToDecimal(dr["TotalCost"]);
            }
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            if (Convert.ToDecimal(currentRow["TotalCost"]) != decTotal) currentRow["TotalCost"] = decTotal;
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void Calculate_Total_Survey_Income()
        {
            if (ibool_FormStillLoading) return;
            GridView view = (GridView)gridControl1.MainView;
            decimal decTotal = (decimal)0.00;
            for (int i = 0; i < view.RowCount; i++)  // RowCount used, not DataRowCount in case more than 1 survey is loaded and grid is filtered //
            {
                decTotal += Convert.ToDecimal(view.GetRowCellValue(i, "Value"));
            }
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            if (Convert.ToDecimal(currentRow["TotalIncome"]) != decTotal) currentRow["TotalIncome"] = decTotal;
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void CopyAddressButton1_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            currentRow["IncidentAddress"] = currentRow["ReportedByAddress"].ToString();
            currentRow["IncidentPostcode"] = currentRow["ReportedByPostcode"].ToString();
            sp07103UTSurveyItemBindingSource.EndEdit();
        }

        private void CopyAddressButton2_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp07103UTSurveyItemBindingSource.Current;
            if (currentRow == null) return;
            currentRow["ReportedByAddress"] = currentRow["IncidentAddress"].ToString();
            currentRow["ReportedByPostcode"] = currentRow["IncidentPostcode"].ToString();
            sp07103UTSurveyItemBindingSource.EndEdit();
        }




    }
}


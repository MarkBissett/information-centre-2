namespace WoodPlan5
{
    partial class frm_UT_Analysis_Permissioned_Work
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Analysis_Permissioned_Work));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup4 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup5 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup6 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup7 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup8 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.fieldPermissionDocumentDateRaisedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionDocumentDateRaisedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionDocumentDateRaisedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionDocumentDateRaisedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToClientDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToClientDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToClientDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToClientDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToCustomerDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToCustomerDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToCustomerDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToCustomerDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnAnalyse = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlSelectedCount = new DevExpress.XtraEditors.LabelControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionPermissionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTrafficManagement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colHotGloveAccessAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessAgreedWithLandOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessDetailsOnMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadsideAccessOnly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearArisingsType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandOwnerRestrictedCut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningConservation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWildlifeDesignation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownLOA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagedUnitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnvironmentalRANumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandscapeImpactNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostageRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlActionFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.ActionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07322UTMasterJobTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp07326UTAnalysisPermissionsForAnalysisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldActionPermissionID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOwnerType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLandOwnerName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionDocumentDateRaised1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRaisedByName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToClientDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEmailedToCustomerDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionDocumentRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTrafficManagement1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHotGloveAccessAvailable1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccessAgreedWithLandOwner1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAccessDetailsOnMap1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRoadsideAccessOnly1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClearArisingsType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLandOwnerRestrictedCut1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedRevisitDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTPOTree1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPlanningConservation1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWildlifeDesignation1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldShutdownLOA1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementWhips1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementStandards1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementNone1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentEcoPlugs1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentSpraying1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentPaint1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentNone1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldManagedUnitNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEnvironmentalRANumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLandscapeImpactNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSiteGridReference1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobTypeDescription1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReferenceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaised1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduled1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompleted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCycle1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUnitDesc1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsTransformer1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTransformerNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientCode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVoltageType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFeederName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRegionName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPrimaryName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubAreaName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionEmailed = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPostageRequired = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSentByPost = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSentByStaffName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter();
            this.sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter();
            this.sp07322_UT_Master_Job_TypesTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07322_UT_Master_Job_TypesTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToggleAvailableColumnsVisibility = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.buttonEditFilterCircuits = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemButtonEditFilterCircuits = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barEditItemAction = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditActionFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalyse = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStatusFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlActionFilter)).BeginInit();
            this.popupContainerControlActionFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07322UTMasterJobTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).BeginInit();
            this.popupContainerControlDateFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07326UTAnalysisPermissionsForAnalysisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditActionFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.barEditItemStatusFilter,
            this.barEditItemDateFilter,
            this.bbiReloadData,
            this.bbiAnalyse,
            this.bbiToggleAvailableColumnsVisibility,
            this.barEditItem1,
            this.buttonEditFilterCircuits,
            this.barEditItemAction});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditStatusFilter,
            this.repositoryItemPopupContainerEditDateFilter,
            this.repositoryItemTextEdit2,
            this.repositoryItemButtonEditFilterCircuits,
            this.repositoryItemPopupContainerEditActionFilter});
            // 
            // fieldPermissionDocumentDateRaisedYear
            // 
            this.fieldPermissionDocumentDateRaisedYear.AreaIndex = 56;
            this.fieldPermissionDocumentDateRaisedYear.Caption = "PD Year Raised";
            this.fieldPermissionDocumentDateRaisedYear.ExpandedInFieldsGroup = false;
            this.fieldPermissionDocumentDateRaisedYear.FieldName = "PermissionDocumentDateRaised";
            this.fieldPermissionDocumentDateRaisedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldPermissionDocumentDateRaisedYear.Name = "fieldPermissionDocumentDateRaisedYear";
            this.fieldPermissionDocumentDateRaisedYear.UnboundFieldName = "fieldPermissionDocumentDateRaisedYear";
            // 
            // fieldPermissionDocumentDateRaisedQuarter
            // 
            this.fieldPermissionDocumentDateRaisedQuarter.AreaIndex = 0;
            this.fieldPermissionDocumentDateRaisedQuarter.Caption = "PD Quarter Raised";
            this.fieldPermissionDocumentDateRaisedQuarter.ExpandedInFieldsGroup = false;
            this.fieldPermissionDocumentDateRaisedQuarter.FieldName = "PermissionDocumentDateRaised";
            this.fieldPermissionDocumentDateRaisedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldPermissionDocumentDateRaisedQuarter.Name = "fieldPermissionDocumentDateRaisedQuarter";
            this.fieldPermissionDocumentDateRaisedQuarter.UnboundFieldName = "fieldPermissionDocumentDateRaisedQuarter";
            this.fieldPermissionDocumentDateRaisedQuarter.ValueFormat.FormatString = "Quarter {0}";
            this.fieldPermissionDocumentDateRaisedQuarter.Visible = false;
            // 
            // fieldPermissionDocumentDateRaisedMonth
            // 
            this.fieldPermissionDocumentDateRaisedMonth.AreaIndex = 0;
            this.fieldPermissionDocumentDateRaisedMonth.Caption = "PD Month Raised";
            this.fieldPermissionDocumentDateRaisedMonth.ExpandedInFieldsGroup = false;
            this.fieldPermissionDocumentDateRaisedMonth.FieldName = "PermissionDocumentDateRaised";
            this.fieldPermissionDocumentDateRaisedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldPermissionDocumentDateRaisedMonth.Name = "fieldPermissionDocumentDateRaisedMonth";
            this.fieldPermissionDocumentDateRaisedMonth.UnboundFieldName = "fieldPermissionDocumentDateRaisedMonth";
            this.fieldPermissionDocumentDateRaisedMonth.Visible = false;
            // 
            // fieldPermissionDocumentDateRaisedWeek
            // 
            this.fieldPermissionDocumentDateRaisedWeek.AreaIndex = 0;
            this.fieldPermissionDocumentDateRaisedWeek.Caption = "PD Week Raised";
            this.fieldPermissionDocumentDateRaisedWeek.ExpandedInFieldsGroup = false;
            this.fieldPermissionDocumentDateRaisedWeek.FieldName = "PermissionDocumentDateRaised";
            this.fieldPermissionDocumentDateRaisedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldPermissionDocumentDateRaisedWeek.Name = "fieldPermissionDocumentDateRaisedWeek";
            this.fieldPermissionDocumentDateRaisedWeek.UnboundFieldName = "fieldPermissionDocumentDateRaisedWeek";
            this.fieldPermissionDocumentDateRaisedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldPermissionDocumentDateRaisedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldPermissionDocumentDateRaisedWeek.Visible = false;
            // 
            // fieldEmailedToClientDateYear
            // 
            this.fieldEmailedToClientDateYear.AreaIndex = 57;
            this.fieldEmailedToClientDateYear.Caption = "Emailed To Client Year";
            this.fieldEmailedToClientDateYear.ExpandedInFieldsGroup = false;
            this.fieldEmailedToClientDateYear.FieldName = "EmailedToClientDate";
            this.fieldEmailedToClientDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldEmailedToClientDateYear.Name = "fieldEmailedToClientDateYear";
            this.fieldEmailedToClientDateYear.UnboundFieldName = "fieldEmailedToClientDateYear";
            // 
            // fieldEmailedToClientDateQuarter
            // 
            this.fieldEmailedToClientDateQuarter.AreaIndex = 1;
            this.fieldEmailedToClientDateQuarter.Caption = "Emailed To Client Quarter";
            this.fieldEmailedToClientDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldEmailedToClientDateQuarter.FieldName = "EmailedToClientDate";
            this.fieldEmailedToClientDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldEmailedToClientDateQuarter.Name = "fieldEmailedToClientDateQuarter";
            this.fieldEmailedToClientDateQuarter.UnboundFieldName = "fieldEmailedToClientDateQuarter";
            this.fieldEmailedToClientDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldEmailedToClientDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEmailedToClientDateQuarter.Visible = false;
            // 
            // fieldEmailedToClientDateMonth
            // 
            this.fieldEmailedToClientDateMonth.AreaIndex = 1;
            this.fieldEmailedToClientDateMonth.Caption = "Emailed To Client Month";
            this.fieldEmailedToClientDateMonth.ExpandedInFieldsGroup = false;
            this.fieldEmailedToClientDateMonth.FieldName = "EmailedToClientDate";
            this.fieldEmailedToClientDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldEmailedToClientDateMonth.Name = "fieldEmailedToClientDateMonth";
            this.fieldEmailedToClientDateMonth.UnboundFieldName = "fieldEmailedToClientDateMonth";
            this.fieldEmailedToClientDateMonth.Visible = false;
            // 
            // fieldEmailedToClientDateWeek
            // 
            this.fieldEmailedToClientDateWeek.AreaIndex = 1;
            this.fieldEmailedToClientDateWeek.Caption = "Emailed To Client Week";
            this.fieldEmailedToClientDateWeek.ExpandedInFieldsGroup = false;
            this.fieldEmailedToClientDateWeek.FieldName = "EmailedToClientDate";
            this.fieldEmailedToClientDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldEmailedToClientDateWeek.Name = "fieldEmailedToClientDateWeek";
            this.fieldEmailedToClientDateWeek.UnboundFieldName = "fieldEmailedToClientDateWeek";
            this.fieldEmailedToClientDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldEmailedToClientDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEmailedToClientDateWeek.Visible = false;
            // 
            // fieldEmailedToCustomerDateYear
            // 
            this.fieldEmailedToCustomerDateYear.AreaIndex = 58;
            this.fieldEmailedToCustomerDateYear.Caption = "Emailed To Customer Year";
            this.fieldEmailedToCustomerDateYear.ExpandedInFieldsGroup = false;
            this.fieldEmailedToCustomerDateYear.FieldName = "EmailedToCustomerDate";
            this.fieldEmailedToCustomerDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldEmailedToCustomerDateYear.Name = "fieldEmailedToCustomerDateYear";
            this.fieldEmailedToCustomerDateYear.UnboundFieldName = "fieldEmailedToCustomerDateYear";
            // 
            // fieldEmailedToCustomerDateQuarter
            // 
            this.fieldEmailedToCustomerDateQuarter.AreaIndex = 1;
            this.fieldEmailedToCustomerDateQuarter.Caption = "Emailed To Customer Quarter";
            this.fieldEmailedToCustomerDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldEmailedToCustomerDateQuarter.FieldName = "EmailedToCustomerDate";
            this.fieldEmailedToCustomerDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldEmailedToCustomerDateQuarter.Name = "fieldEmailedToCustomerDateQuarter";
            this.fieldEmailedToCustomerDateQuarter.UnboundFieldName = "fieldEmailedToCustomerDateQuarter";
            this.fieldEmailedToCustomerDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldEmailedToCustomerDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEmailedToCustomerDateQuarter.Visible = false;
            // 
            // fieldEmailedToCustomerDateMonth
            // 
            this.fieldEmailedToCustomerDateMonth.AreaIndex = 1;
            this.fieldEmailedToCustomerDateMonth.Caption = "Emailed To Customer Month";
            this.fieldEmailedToCustomerDateMonth.ExpandedInFieldsGroup = false;
            this.fieldEmailedToCustomerDateMonth.FieldName = "EmailedToCustomerDate";
            this.fieldEmailedToCustomerDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldEmailedToCustomerDateMonth.Name = "fieldEmailedToCustomerDateMonth";
            this.fieldEmailedToCustomerDateMonth.UnboundFieldName = "fieldEmailedToCustomerDateMonth";
            this.fieldEmailedToCustomerDateMonth.Visible = false;
            // 
            // fieldEmailedToCustomerDateWeek
            // 
            this.fieldEmailedToCustomerDateWeek.AreaIndex = 1;
            this.fieldEmailedToCustomerDateWeek.Caption = "Emailed To Customer Week";
            this.fieldEmailedToCustomerDateWeek.ExpandedInFieldsGroup = false;
            this.fieldEmailedToCustomerDateWeek.FieldName = "EmailedToCustomerDate";
            this.fieldEmailedToCustomerDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldEmailedToCustomerDateWeek.Name = "fieldEmailedToCustomerDateWeek";
            this.fieldEmailedToCustomerDateWeek.UnboundFieldName = "fieldEmailedToCustomerDateWeek";
            this.fieldEmailedToCustomerDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldEmailedToCustomerDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEmailedToCustomerDateWeek.Visible = false;
            // 
            // fieldDateRaisedYear
            // 
            this.fieldDateRaisedYear.AreaIndex = 60;
            this.fieldDateRaisedYear.Caption = "Job Year Raised";
            this.fieldDateRaisedYear.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedYear.FieldName = "DateRaised";
            this.fieldDateRaisedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateRaisedYear.Name = "fieldDateRaisedYear";
            this.fieldDateRaisedYear.UnboundFieldName = "fieldDateRaisedYear";
            this.fieldDateRaisedYear.Visible = false;
            // 
            // fieldDateRaisedQuarter
            // 
            this.fieldDateRaisedQuarter.AreaIndex = 1;
            this.fieldDateRaisedQuarter.Caption = "Job Quarter Raised";
            this.fieldDateRaisedQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedQuarter.FieldName = "DateRaised";
            this.fieldDateRaisedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateRaisedQuarter.Name = "fieldDateRaisedQuarter";
            this.fieldDateRaisedQuarter.UnboundFieldName = "fieldDateRaisedQuarter";
            this.fieldDateRaisedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateRaisedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateRaisedQuarter.Visible = false;
            // 
            // fieldDateRaisedMonth
            // 
            this.fieldDateRaisedMonth.AreaIndex = 1;
            this.fieldDateRaisedMonth.Caption = "Job Month Raised";
            this.fieldDateRaisedMonth.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedMonth.FieldName = "DateRaised";
            this.fieldDateRaisedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateRaisedMonth.Name = "fieldDateRaisedMonth";
            this.fieldDateRaisedMonth.UnboundFieldName = "fieldDateRaisedMonth";
            this.fieldDateRaisedMonth.Visible = false;
            // 
            // fieldDateRaisedWeek
            // 
            this.fieldDateRaisedWeek.AreaIndex = 1;
            this.fieldDateRaisedWeek.Caption = "Job Week Raised";
            this.fieldDateRaisedWeek.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedWeek.FieldName = "DateRaised";
            this.fieldDateRaisedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateRaisedWeek.Name = "fieldDateRaisedWeek";
            this.fieldDateRaisedWeek.UnboundFieldName = "fieldDateRaisedWeek";
            this.fieldDateRaisedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateRaisedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateRaisedWeek.Visible = false;
            // 
            // fieldDateScheduledYear
            // 
            this.fieldDateScheduledYear.AreaIndex = 61;
            this.fieldDateScheduledYear.Caption = "Job Year Scheduled";
            this.fieldDateScheduledYear.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledYear.FieldName = "DateScheduled";
            this.fieldDateScheduledYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateScheduledYear.Name = "fieldDateScheduledYear";
            this.fieldDateScheduledYear.UnboundFieldName = "fieldDateScheduledYear";
            this.fieldDateScheduledYear.Visible = false;
            // 
            // fieldDateScheduledQuarter
            // 
            this.fieldDateScheduledQuarter.AreaIndex = 2;
            this.fieldDateScheduledQuarter.Caption = "Job Quarter Scheduled";
            this.fieldDateScheduledQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledQuarter.FieldName = "DateScheduled";
            this.fieldDateScheduledQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateScheduledQuarter.Name = "fieldDateScheduledQuarter";
            this.fieldDateScheduledQuarter.UnboundFieldName = "fieldDateScheduledQuarter";
            this.fieldDateScheduledQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateScheduledQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateScheduledQuarter.Visible = false;
            // 
            // fieldDateScheduledMonth
            // 
            this.fieldDateScheduledMonth.AreaIndex = 2;
            this.fieldDateScheduledMonth.Caption = "Job Month Scheduled";
            this.fieldDateScheduledMonth.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledMonth.FieldName = "DateScheduled";
            this.fieldDateScheduledMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateScheduledMonth.Name = "fieldDateScheduledMonth";
            this.fieldDateScheduledMonth.UnboundFieldName = "fieldDateScheduledMonth";
            this.fieldDateScheduledMonth.Visible = false;
            // 
            // fieldDateScheduledWeek
            // 
            this.fieldDateScheduledWeek.AreaIndex = 2;
            this.fieldDateScheduledWeek.Caption = "Job Week Scheduled";
            this.fieldDateScheduledWeek.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledWeek.FieldName = "DateScheduled";
            this.fieldDateScheduledWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateScheduledWeek.Name = "fieldDateScheduledWeek";
            this.fieldDateScheduledWeek.UnboundFieldName = "fieldDateScheduledWeek";
            this.fieldDateScheduledWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateScheduledWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateScheduledWeek.Visible = false;
            // 
            // fieldDateCompletedYear
            // 
            this.fieldDateCompletedYear.AreaIndex = 62;
            this.fieldDateCompletedYear.Caption = "Job Year Completed";
            this.fieldDateCompletedYear.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedYear.FieldName = "DateCompleted";
            this.fieldDateCompletedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateCompletedYear.Name = "fieldDateCompletedYear";
            this.fieldDateCompletedYear.UnboundFieldName = "fieldDateCompletedYear";
            this.fieldDateCompletedYear.Visible = false;
            // 
            // fieldDateCompletedQuarter
            // 
            this.fieldDateCompletedQuarter.AreaIndex = 3;
            this.fieldDateCompletedQuarter.Caption = "Job Quarter Completed";
            this.fieldDateCompletedQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedQuarter.FieldName = "DateCompleted";
            this.fieldDateCompletedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateCompletedQuarter.Name = "fieldDateCompletedQuarter";
            this.fieldDateCompletedQuarter.UnboundFieldName = "fieldDateCompletedQuarter";
            this.fieldDateCompletedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateCompletedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateCompletedQuarter.Visible = false;
            // 
            // fieldDateCompletedMonth
            // 
            this.fieldDateCompletedMonth.AreaIndex = 3;
            this.fieldDateCompletedMonth.Caption = "Job Month Completed";
            this.fieldDateCompletedMonth.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedMonth.FieldName = "DateCompleted";
            this.fieldDateCompletedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateCompletedMonth.Name = "fieldDateCompletedMonth";
            this.fieldDateCompletedMonth.UnboundFieldName = "fieldDateCompletedMonth";
            this.fieldDateCompletedMonth.Visible = false;
            // 
            // fieldDateCompletedWeek
            // 
            this.fieldDateCompletedWeek.AreaIndex = 3;
            this.fieldDateCompletedWeek.Caption = "Job Week Completed";
            this.fieldDateCompletedWeek.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedWeek.FieldName = "DateCompleted";
            this.fieldDateCompletedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateCompletedWeek.Name = "fieldDateCompletedWeek";
            this.fieldDateCompletedWeek.UnboundFieldName = "fieldDateCompletedWeek";
            this.fieldDateCompletedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateCompletedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateCompletedWeek.Visible = false;
            // 
            // fieldLastInspectionDateYear
            // 
            this.fieldLastInspectionDateYear.AreaIndex = 63;
            this.fieldLastInspectionDateYear.Caption = "Last Inspection Year";
            this.fieldLastInspectionDateYear.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateYear.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldLastInspectionDateYear.Name = "fieldLastInspectionDateYear";
            this.fieldLastInspectionDateYear.UnboundFieldName = "fieldLastInspectionDateYear";
            this.fieldLastInspectionDateYear.Visible = false;
            // 
            // fieldLastInspectionDateQuarter
            // 
            this.fieldLastInspectionDateQuarter.AreaIndex = 1;
            this.fieldLastInspectionDateQuarter.Caption = "Last Inspection Quarter";
            this.fieldLastInspectionDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateQuarter.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldLastInspectionDateQuarter.Name = "fieldLastInspectionDateQuarter";
            this.fieldLastInspectionDateQuarter.UnboundFieldName = "fieldLastInspectionDateQuarter";
            this.fieldLastInspectionDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldLastInspectionDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionDateQuarter.Visible = false;
            // 
            // fieldLastInspectionDateMonth
            // 
            this.fieldLastInspectionDateMonth.AreaIndex = 1;
            this.fieldLastInspectionDateMonth.Caption = "Last Inspection Month";
            this.fieldLastInspectionDateMonth.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateMonth.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldLastInspectionDateMonth.Name = "fieldLastInspectionDateMonth";
            this.fieldLastInspectionDateMonth.UnboundFieldName = "fieldLastInspectionDateMonth";
            this.fieldLastInspectionDateMonth.Visible = false;
            // 
            // fieldLastInspectionDateWeek
            // 
            this.fieldLastInspectionDateWeek.AreaIndex = 1;
            this.fieldLastInspectionDateWeek.Caption = "Last Inspection Week";
            this.fieldLastInspectionDateWeek.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateWeek.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldLastInspectionDateWeek.Name = "fieldLastInspectionDateWeek";
            this.fieldLastInspectionDateWeek.UnboundFieldName = "fieldLastInspectionDateWeek";
            this.fieldLastInspectionDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldLastInspectionDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionDateWeek.Visible = false;
            // 
            // fieldNextInspectionDateYear
            // 
            this.fieldNextInspectionDateYear.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateYear.AreaIndex = 0;
            this.fieldNextInspectionDateYear.Caption = "Next Inspection Year";
            this.fieldNextInspectionDateYear.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateYear.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldNextInspectionDateYear.Name = "fieldNextInspectionDateYear";
            this.fieldNextInspectionDateYear.UnboundFieldName = "fieldNextInspectionDateYear";
            this.fieldNextInspectionDateYear.Visible = false;
            // 
            // fieldNextInspectionDateQuarter
            // 
            this.fieldNextInspectionDateQuarter.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateQuarter.AreaIndex = 1;
            this.fieldNextInspectionDateQuarter.Caption = "Next Inspection Quarter";
            this.fieldNextInspectionDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateQuarter.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldNextInspectionDateQuarter.Name = "fieldNextInspectionDateQuarter";
            this.fieldNextInspectionDateQuarter.UnboundFieldName = "fieldNextInspectionDateQuarter";
            this.fieldNextInspectionDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldNextInspectionDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionDateQuarter.Visible = false;
            // 
            // fieldNextInspectionDateMonth
            // 
            this.fieldNextInspectionDateMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateMonth.AreaIndex = 1;
            this.fieldNextInspectionDateMonth.Caption = "Next Inspection Month";
            this.fieldNextInspectionDateMonth.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateMonth.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldNextInspectionDateMonth.Name = "fieldNextInspectionDateMonth";
            this.fieldNextInspectionDateMonth.UnboundFieldName = "fieldNextInspectionDateMonth";
            this.fieldNextInspectionDateMonth.Visible = false;
            // 
            // fieldNextInspectionDateWeek
            // 
            this.fieldNextInspectionDateWeek.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateWeek.AreaIndex = 1;
            this.fieldNextInspectionDateWeek.Caption = "Next Inspection Week";
            this.fieldNextInspectionDateWeek.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateWeek.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldNextInspectionDateWeek.Name = "fieldNextInspectionDateWeek";
            this.fieldNextInspectionDateWeek.UnboundFieldName = "fieldNextInspectionDateWeek";
            this.fieldNextInspectionDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldNextInspectionDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionDateWeek.Visible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(444, 200);
            this.dockPanel1.Size = new System.Drawing.Size(444, 494);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnAnalyse);
            this.dockPanel1_Container.Controls.Add(this.labelControlSelectedCount);
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.gridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(438, 462);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnalyse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAnalyse.Appearance.Options.UseFont = true;
            this.btnAnalyse.Image = ((System.Drawing.Image)(resources.GetObject("btnAnalyse.Image")));
            this.btnAnalyse.Location = new System.Drawing.Point(326, 436);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(112, 26);
            this.btnAnalyse.TabIndex = 5;
            this.btnAnalyse.Text = "Analyse Data";
            this.btnAnalyse.Click += new System.EventHandler(this.btnAnalyse_Click);
            // 
            // labelControlSelectedCount
            // 
            this.labelControlSelectedCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedCount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControlSelectedCount.Appearance.Image")));
            this.labelControlSelectedCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedCount.Location = new System.Drawing.Point(2, 440);
            this.labelControlSelectedCount.Name = "labelControlSelectedCount";
            this.labelControlSelectedCount.Size = new System.Drawing.Size(318, 18);
            this.labelControlSelectedCount.TabIndex = 7;
            this.labelControlSelectedCount.Text = "        0 Permissioned Work Records Selected For Analysis";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(438, 34);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 34);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControl1.Size = new System.Drawing.Size(438, 401);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource
            // 
            this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource.DataMember = "sp07325_UT_Analysis_Permissions_Available_Permissions";
            this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionPermissionID,
            this.colPermissionStatus,
            this.colOwnerType,
            this.colLandOwnerName,
            this.colPermissionDocumentDateRaised,
            this.colRaisedByName,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.colPermissionDocumentRemarks,
            this.colTrafficManagement,
            this.colHotGloveAccessAvailable,
            this.colAccessAgreedWithLandOwner,
            this.colAccessDetailsOnMap,
            this.colRoadsideAccessOnly,
            this.colClearArisingsType,
            this.colLandOwnerRestrictedCut,
            this.colEstimatedRevisitDate,
            this.colTPOTree,
            this.colPlanningConservation,
            this.colWildlifeDesignation,
            this.colShutdownLOA,
            this.colTreeReplacementWhips,
            this.colTreeReplacementStandards,
            this.colTreeReplacementNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentSpraying,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentNone,
            this.colManagedUnitNumber,
            this.colEnvironmentalRANumber,
            this.colLandscapeImpactNumber,
            this.colSiteGridReference,
            this.colJobTypeDescription,
            this.colReferenceNumber,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colActionStatus,
            this.colPoleNumber,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.colClientName,
            this.colClientCode,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageType,
            this.colFeederName,
            this.colRegionName,
            this.colPoleStatus,
            this.colPrimaryName,
            this.colSubAreaName,
            this.colPoleType,
            this.colSurveyorName,
            this.colPostageRequired,
            this.colPermissionEmailed,
            this.colSentByPost,
            this.colSentByStaffName});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 3;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colActionPermissionID
            // 
            this.colActionPermissionID.Caption = "Action Permission ID";
            this.colActionPermissionID.FieldName = "ActionPermissionID";
            this.colActionPermissionID.Name = "colActionPermissionID";
            this.colActionPermissionID.OptionsColumn.AllowEdit = false;
            this.colActionPermissionID.OptionsColumn.AllowFocus = false;
            this.colActionPermissionID.OptionsColumn.ReadOnly = true;
            this.colActionPermissionID.Width = 118;
            // 
            // colPermissionStatus
            // 
            this.colPermissionStatus.Caption = "Permission Status";
            this.colPermissionStatus.FieldName = "PermissionStatus";
            this.colPermissionStatus.Name = "colPermissionStatus";
            this.colPermissionStatus.OptionsColumn.AllowEdit = false;
            this.colPermissionStatus.OptionsColumn.AllowFocus = false;
            this.colPermissionStatus.OptionsColumn.ReadOnly = true;
            this.colPermissionStatus.Visible = true;
            this.colPermissionStatus.VisibleIndex = 2;
            this.colPermissionStatus.Width = 105;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 3;
            this.colOwnerType.Width = 80;
            // 
            // colLandOwnerName
            // 
            this.colLandOwnerName.Caption = "Landowner Name";
            this.colLandOwnerName.FieldName = "LandOwnerName";
            this.colLandOwnerName.Name = "colLandOwnerName";
            this.colLandOwnerName.OptionsColumn.AllowEdit = false;
            this.colLandOwnerName.OptionsColumn.AllowFocus = false;
            this.colLandOwnerName.OptionsColumn.ReadOnly = true;
            this.colLandOwnerName.Visible = true;
            this.colLandOwnerName.VisibleIndex = 4;
            this.colLandOwnerName.Width = 141;
            // 
            // colPermissionDocumentDateRaised
            // 
            this.colPermissionDocumentDateRaised.Caption = "PD Date Raised";
            this.colPermissionDocumentDateRaised.FieldName = "PermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.Name = "colPermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentDateRaised.Visible = true;
            this.colPermissionDocumentDateRaised.VisibleIndex = 5;
            this.colPermissionDocumentDateRaised.Width = 95;
            // 
            // colRaisedByName
            // 
            this.colRaisedByName.Caption = "PD Raised By";
            this.colRaisedByName.FieldName = "RaisedByName";
            this.colRaisedByName.Name = "colRaisedByName";
            this.colRaisedByName.OptionsColumn.AllowEdit = false;
            this.colRaisedByName.OptionsColumn.AllowFocus = false;
            this.colRaisedByName.OptionsColumn.ReadOnly = true;
            this.colRaisedByName.Visible = true;
            this.colRaisedByName.VisibleIndex = 6;
            this.colRaisedByName.Width = 155;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "PD Emailed To Client Date";
            this.colEmailedToClientDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.Width = 144;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "PD Emailed To Customer Date";
            this.colEmailedToCustomerDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.Width = 163;
            // 
            // colPermissionDocumentRemarks
            // 
            this.colPermissionDocumentRemarks.Caption = "PD Remarks";
            this.colPermissionDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colPermissionDocumentRemarks.FieldName = "PermissionDocumentRemarks";
            this.colPermissionDocumentRemarks.Name = "colPermissionDocumentRemarks";
            this.colPermissionDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentRemarks.Visible = true;
            this.colPermissionDocumentRemarks.VisibleIndex = 7;
            this.colPermissionDocumentRemarks.Width = 78;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colTrafficManagement
            // 
            this.colTrafficManagement.Caption = "Traffic Management";
            this.colTrafficManagement.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagement.FieldName = "TrafficManagement";
            this.colTrafficManagement.Name = "colTrafficManagement";
            this.colTrafficManagement.OptionsColumn.AllowEdit = false;
            this.colTrafficManagement.OptionsColumn.AllowFocus = false;
            this.colTrafficManagement.OptionsColumn.ReadOnly = true;
            this.colTrafficManagement.Visible = true;
            this.colTrafficManagement.VisibleIndex = 8;
            this.colTrafficManagement.Width = 117;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colHotGloveAccessAvailable
            // 
            this.colHotGloveAccessAvailable.Caption = "Hot Glove Available";
            this.colHotGloveAccessAvailable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHotGloveAccessAvailable.FieldName = "HotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.Name = "colHotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.OptionsColumn.AllowEdit = false;
            this.colHotGloveAccessAvailable.OptionsColumn.AllowFocus = false;
            this.colHotGloveAccessAvailable.OptionsColumn.ReadOnly = true;
            this.colHotGloveAccessAvailable.Visible = true;
            this.colHotGloveAccessAvailable.VisibleIndex = 9;
            this.colHotGloveAccessAvailable.Width = 114;
            // 
            // colAccessAgreedWithLandOwner
            // 
            this.colAccessAgreedWithLandOwner.Caption = "Access Agreed";
            this.colAccessAgreedWithLandOwner.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccessAgreedWithLandOwner.FieldName = "AccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.Name = "colAccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowEdit = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowFocus = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.ReadOnly = true;
            this.colAccessAgreedWithLandOwner.Visible = true;
            this.colAccessAgreedWithLandOwner.VisibleIndex = 10;
            this.colAccessAgreedWithLandOwner.Width = 92;
            // 
            // colAccessDetailsOnMap
            // 
            this.colAccessDetailsOnMap.Caption = "Access Details On Map";
            this.colAccessDetailsOnMap.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAccessDetailsOnMap.FieldName = "AccessDetailsOnMap";
            this.colAccessDetailsOnMap.Name = "colAccessDetailsOnMap";
            this.colAccessDetailsOnMap.OptionsColumn.AllowEdit = false;
            this.colAccessDetailsOnMap.OptionsColumn.AllowFocus = false;
            this.colAccessDetailsOnMap.OptionsColumn.ReadOnly = true;
            this.colAccessDetailsOnMap.Width = 129;
            // 
            // colRoadsideAccessOnly
            // 
            this.colRoadsideAccessOnly.Caption = "Roadside Access Only";
            this.colRoadsideAccessOnly.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colRoadsideAccessOnly.FieldName = "RoadsideAccessOnly";
            this.colRoadsideAccessOnly.Name = "colRoadsideAccessOnly";
            this.colRoadsideAccessOnly.OptionsColumn.AllowEdit = false;
            this.colRoadsideAccessOnly.OptionsColumn.AllowFocus = false;
            this.colRoadsideAccessOnly.OptionsColumn.ReadOnly = true;
            this.colRoadsideAccessOnly.Visible = true;
            this.colRoadsideAccessOnly.VisibleIndex = 11;
            this.colRoadsideAccessOnly.Width = 126;
            // 
            // colClearArisingsType
            // 
            this.colClearArisingsType.Caption = "Clear Arisings Type";
            this.colClearArisingsType.FieldName = "ClearArisingsType";
            this.colClearArisingsType.Name = "colClearArisingsType";
            this.colClearArisingsType.OptionsColumn.AllowEdit = false;
            this.colClearArisingsType.OptionsColumn.AllowFocus = false;
            this.colClearArisingsType.OptionsColumn.ReadOnly = true;
            this.colClearArisingsType.Width = 113;
            // 
            // colLandOwnerRestrictedCut
            // 
            this.colLandOwnerRestrictedCut.Caption = "Landowner Restricted Cut ";
            this.colLandOwnerRestrictedCut.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLandOwnerRestrictedCut.FieldName = "LandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.Name = "colLandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowEdit = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowFocus = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.ReadOnly = true;
            this.colLandOwnerRestrictedCut.Visible = true;
            this.colLandOwnerRestrictedCut.VisibleIndex = 12;
            this.colLandOwnerRestrictedCut.Width = 149;
            // 
            // colEstimatedRevisitDate
            // 
            this.colEstimatedRevisitDate.Caption = "Estimated Revisit Date";
            this.colEstimatedRevisitDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEstimatedRevisitDate.FieldName = "EstimatedRevisitDate";
            this.colEstimatedRevisitDate.Name = "colEstimatedRevisitDate";
            this.colEstimatedRevisitDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedRevisitDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedRevisitDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedRevisitDate.Visible = true;
            this.colEstimatedRevisitDate.VisibleIndex = 13;
            this.colEstimatedRevisitDate.Width = 129;
            // 
            // colTPOTree
            // 
            this.colTPOTree.Caption = "TPO Tree";
            this.colTPOTree.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTPOTree.FieldName = "TPOTree";
            this.colTPOTree.Name = "colTPOTree";
            this.colTPOTree.OptionsColumn.AllowEdit = false;
            this.colTPOTree.OptionsColumn.AllowFocus = false;
            this.colTPOTree.OptionsColumn.ReadOnly = true;
            this.colTPOTree.Visible = true;
            this.colTPOTree.VisibleIndex = 14;
            // 
            // colPlanningConservation
            // 
            this.colPlanningConservation.Caption = "Planning Conservation";
            this.colPlanningConservation.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPlanningConservation.FieldName = "PlanningConservation";
            this.colPlanningConservation.Name = "colPlanningConservation";
            this.colPlanningConservation.OptionsColumn.AllowEdit = false;
            this.colPlanningConservation.OptionsColumn.AllowFocus = false;
            this.colPlanningConservation.OptionsColumn.ReadOnly = true;
            this.colPlanningConservation.Visible = true;
            this.colPlanningConservation.VisibleIndex = 15;
            this.colPlanningConservation.Width = 128;
            // 
            // colWildlifeDesignation
            // 
            this.colWildlifeDesignation.Caption = "Wildlife Designation";
            this.colWildlifeDesignation.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colWildlifeDesignation.FieldName = "WildlifeDesignation";
            this.colWildlifeDesignation.Name = "colWildlifeDesignation";
            this.colWildlifeDesignation.OptionsColumn.AllowEdit = false;
            this.colWildlifeDesignation.OptionsColumn.AllowFocus = false;
            this.colWildlifeDesignation.OptionsColumn.ReadOnly = true;
            this.colWildlifeDesignation.Visible = true;
            this.colWildlifeDesignation.VisibleIndex = 16;
            this.colWildlifeDesignation.Width = 114;
            // 
            // colShutdownLOA
            // 
            this.colShutdownLOA.Caption = "Shutdown LOA";
            this.colShutdownLOA.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colShutdownLOA.FieldName = "ShutdownLOA";
            this.colShutdownLOA.Name = "colShutdownLOA";
            this.colShutdownLOA.OptionsColumn.AllowEdit = false;
            this.colShutdownLOA.OptionsColumn.AllowFocus = false;
            this.colShutdownLOA.OptionsColumn.ReadOnly = true;
            this.colShutdownLOA.Visible = true;
            this.colShutdownLOA.VisibleIndex = 17;
            this.colShutdownLOA.Width = 92;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement Whips";
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Width = 140;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement Standards";
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Width = 160;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Width = 136;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatement Eco Plugs";
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Width = 158;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatement Spraying";
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Width = 155;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatement Paint";
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Width = 137;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatement None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Width = 138;
            // 
            // colManagedUnitNumber
            // 
            this.colManagedUnitNumber.Caption = "Managed Unit #";
            this.colManagedUnitNumber.FieldName = "ManagedUnitNumber";
            this.colManagedUnitNumber.Name = "colManagedUnitNumber";
            this.colManagedUnitNumber.OptionsColumn.AllowEdit = false;
            this.colManagedUnitNumber.OptionsColumn.AllowFocus = false;
            this.colManagedUnitNumber.OptionsColumn.ReadOnly = true;
            this.colManagedUnitNumber.Visible = true;
            this.colManagedUnitNumber.VisibleIndex = 18;
            this.colManagedUnitNumber.Width = 98;
            // 
            // colEnvironmentalRANumber
            // 
            this.colEnvironmentalRANumber.Caption = "Environmental RA #";
            this.colEnvironmentalRANumber.FieldName = "EnvironmentalRANumber";
            this.colEnvironmentalRANumber.Name = "colEnvironmentalRANumber";
            this.colEnvironmentalRANumber.OptionsColumn.AllowEdit = false;
            this.colEnvironmentalRANumber.OptionsColumn.AllowFocus = false;
            this.colEnvironmentalRANumber.OptionsColumn.ReadOnly = true;
            this.colEnvironmentalRANumber.Visible = true;
            this.colEnvironmentalRANumber.VisibleIndex = 19;
            this.colEnvironmentalRANumber.Width = 117;
            // 
            // colLandscapeImpactNumber
            // 
            this.colLandscapeImpactNumber.Caption = "Landscape Impact #";
            this.colLandscapeImpactNumber.FieldName = "LandscapeImpactNumber";
            this.colLandscapeImpactNumber.Name = "colLandscapeImpactNumber";
            this.colLandscapeImpactNumber.OptionsColumn.AllowEdit = false;
            this.colLandscapeImpactNumber.OptionsColumn.AllowFocus = false;
            this.colLandscapeImpactNumber.OptionsColumn.ReadOnly = true;
            this.colLandscapeImpactNumber.Visible = true;
            this.colLandscapeImpactNumber.VisibleIndex = 20;
            this.colLandscapeImpactNumber.Width = 119;
            // 
            // colSiteGridReference
            // 
            this.colSiteGridReference.Caption = "Site Grid Ref";
            this.colSiteGridReference.FieldName = "SiteGridReference";
            this.colSiteGridReference.Name = "colSiteGridReference";
            this.colSiteGridReference.OptionsColumn.AllowEdit = false;
            this.colSiteGridReference.OptionsColumn.AllowFocus = false;
            this.colSiteGridReference.OptionsColumn.ReadOnly = true;
            this.colSiteGridReference.Visible = true;
            this.colSiteGridReference.VisibleIndex = 21;
            this.colSiteGridReference.Width = 82;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 0;
            this.colJobTypeDescription.Width = 185;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Job Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 1;
            this.colReferenceNumber.Width = 131;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Job Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 22;
            this.colDateRaised.Width = 99;
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Job Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 23;
            this.colDateScheduled.Width = 116;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Job Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 24;
            this.colDateCompleted.Width = 118;
            // 
            // colActionStatus
            // 
            this.colActionStatus.Caption = "Job Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 25;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 26;
            this.colPoleNumber.Width = 81;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection Date";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 27;
            this.colLastInspectionDate.Width = 120;
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Inspection Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 28;
            this.colInspectionCycle.Width = 100;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Inspection Unit Desc";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.Visible = true;
            this.colInspectionUnitDesc.VisibleIndex = 29;
            this.colInspectionUnitDesc.Width = 119;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection Date";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 30;
            this.colNextInspectionDate.Width = 123;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 31;
            this.colIsTransformer.Width = 92;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.Visible = true;
            this.colTransformerNumber.VisibleIndex = 32;
            this.colTransformerNumber.Width = 120;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 46;
            this.colClientName.Width = 165;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 76;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 47;
            this.colCircuitName.Width = 136;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 33;
            this.colCircuitNumber.Width = 138;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 34;
            this.colVoltageType.Width = 84;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 37;
            this.colFeederName.Width = 165;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 51;
            this.colRegionName.Width = 151;
            // 
            // colPoleStatus
            // 
            this.colPoleStatus.Caption = "Pole Status";
            this.colPoleStatus.FieldName = "PoleStatus";
            this.colPoleStatus.Name = "colPoleStatus";
            this.colPoleStatus.OptionsColumn.AllowEdit = false;
            this.colPoleStatus.OptionsColumn.AllowFocus = false;
            this.colPoleStatus.OptionsColumn.ReadOnly = true;
            this.colPoleStatus.Visible = true;
            this.colPoleStatus.VisibleIndex = 38;
            this.colPoleStatus.Width = 118;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 36;
            this.colPrimaryName.Width = 152;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 35;
            this.colSubAreaName.Width = 153;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.Visible = true;
            this.colPoleType.VisibleIndex = 39;
            this.colPoleType.Width = 100;
            // 
            // colSurveyorName
            // 
            this.colSurveyorName.Caption = "Surveyor Name";
            this.colSurveyorName.FieldName = "SurveyorName";
            this.colSurveyorName.Name = "colSurveyorName";
            this.colSurveyorName.OptionsColumn.AllowEdit = false;
            this.colSurveyorName.OptionsColumn.AllowFocus = false;
            this.colSurveyorName.OptionsColumn.ReadOnly = true;
            this.colSurveyorName.Visible = true;
            this.colSurveyorName.VisibleIndex = 40;
            this.colSurveyorName.Width = 126;
            // 
            // colPostageRequired
            // 
            this.colPostageRequired.Caption = "Postage Required";
            this.colPostageRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPostageRequired.FieldName = "PostageRequired";
            this.colPostageRequired.Name = "colPostageRequired";
            this.colPostageRequired.OptionsColumn.AllowEdit = false;
            this.colPostageRequired.OptionsColumn.AllowFocus = false;
            this.colPostageRequired.OptionsColumn.ReadOnly = true;
            this.colPostageRequired.Width = 106;
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Width = 110;
            // 
            // colSentByPost
            // 
            this.colSentByPost.Caption = "Sent By Post";
            this.colSentByPost.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSentByPost.FieldName = "SentByPost";
            this.colSentByPost.Name = "colSentByPost";
            this.colSentByPost.OptionsColumn.AllowEdit = false;
            this.colSentByPost.OptionsColumn.AllowFocus = false;
            this.colSentByPost.OptionsColumn.ReadOnly = true;
            // 
            // colSentByStaffName
            // 
            this.colSentByStaffName.Caption = "Sent By";
            this.colSentByStaffName.FieldName = "SentByStaffName";
            this.colSentByStaffName.Name = "colSentByStaffName";
            this.colSentByStaffName.OptionsColumn.AllowEdit = false;
            this.colSentByStaffName.OptionsColumn.AllowFocus = false;
            this.colSentByStaffName.OptionsColumn.ReadOnly = true;
            this.colSentByStaffName.Width = 142;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P2";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(444, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlActionFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(849, 494);
            this.splitContainerControl1.SplitterPosition = 388;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl2";
            // 
            // popupContainerControlActionFilter
            // 
            this.popupContainerControlActionFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlActionFilter.Controls.Add(this.ActionFilterOK);
            this.popupContainerControlActionFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlActionFilter.Location = new System.Drawing.Point(403, 190);
            this.popupContainerControlActionFilter.Name = "popupContainerControlActionFilter";
            this.popupContainerControlActionFilter.Size = new System.Drawing.Size(230, 198);
            this.popupContainerControlActionFilter.TabIndex = 36;
            // 
            // ActionFilterOK
            // 
            this.ActionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ActionFilterOK.Location = new System.Drawing.Point(3, 173);
            this.ActionFilterOK.Name = "ActionFilterOK";
            this.ActionFilterOK.Size = new System.Drawing.Size(35, 22);
            this.ActionFilterOK.TabIndex = 19;
            this.ActionFilterOK.Text = "OK";
            this.ActionFilterOK.Click += new System.EventHandler(this.ActionFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07322UTMasterJobTypesBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(224, 168);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07322UTMasterJobTypesBindingSource
            // 
            this.sp07322UTMasterJobTypesBindingSource.DataMember = "sp07322_UT_Master_Job_Types";
            this.sp07322UTMasterJobTypesBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colJobCode,
            this.colJobDescription});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Action ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Action Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobCode.Width = 80;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Action Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 195;
            // 
            // popupContainerControlDateFilter
            // 
            this.popupContainerControlDateFilter.Controls.Add(this.btnDateFilterOK);
            this.popupContainerControlDateFilter.Controls.Add(this.groupControl2);
            this.popupContainerControlDateFilter.Location = new System.Drawing.Point(6, 283);
            this.popupContainerControlDateFilter.Name = "popupContainerControlDateFilter";
            this.popupContainerControlDateFilter.Size = new System.Drawing.Size(313, 82);
            this.popupContainerControlDateFilter.TabIndex = 35;
            // 
            // btnDateFilterOK
            // 
            this.btnDateFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateFilterOK.Location = new System.Drawing.Point(3, 57);
            this.btnDateFilterOK.Name = "btnDateFilterOK";
            this.btnDateFilterOK.Size = new System.Drawing.Size(35, 22);
            this.btnDateFilterOK.TabIndex = 18;
            this.btnDateFilterOK.Text = "OK";
            this.btnDateFilterOK.Click += new System.EventHandler(this.btnDateFilterOK_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(307, 51);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(170, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(41, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.Mask.EditMask = "g";
            this.deFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(108, 20);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "From Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Only Actions with a Date Raised between the From and To Date will be included.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.deFromDate.SuperTip = superToolTip1;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(192, 25);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.Mask.EditMask = "g";
            this.deToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(108, 20);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "To Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Only Actions with a Date Raised between the From and To Date will be included.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.deToDate.SuperTip = superToolTip2;
            this.deToDate.TabIndex = 7;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp07326UTAnalysisPermissionsForAnalysisBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldActionPermissionID1,
            this.fieldPermissionStatus1,
            this.fieldOwnerType1,
            this.fieldLandOwnerName1,
            this.fieldPermissionDocumentDateRaised1,
            this.fieldRaisedByName1,
            this.fieldEmailedToClientDate1,
            this.fieldEmailedToCustomerDate1,
            this.fieldPermissionDocumentRemarks1,
            this.fieldTrafficManagement1,
            this.fieldHotGloveAccessAvailable1,
            this.fieldAccessAgreedWithLandOwner1,
            this.fieldAccessDetailsOnMap1,
            this.fieldRoadsideAccessOnly1,
            this.fieldClearArisingsType1,
            this.fieldLandOwnerRestrictedCut1,
            this.fieldEstimatedRevisitDate1,
            this.fieldTPOTree1,
            this.fieldPlanningConservation1,
            this.fieldWildlifeDesignation1,
            this.fieldShutdownLOA1,
            this.fieldTreeReplacementWhips1,
            this.fieldTreeReplacementStandards1,
            this.fieldTreeReplacementNone1,
            this.fieldStumpTreatmentEcoPlugs1,
            this.fieldStumpTreatmentSpraying1,
            this.fieldStumpTreatmentPaint1,
            this.fieldStumpTreatmentNone1,
            this.fieldManagedUnitNumber1,
            this.fieldEnvironmentalRANumber1,
            this.fieldLandscapeImpactNumber1,
            this.fieldSiteGridReference1,
            this.fieldJobTypeDescription1,
            this.fieldReferenceNumber1,
            this.fieldDateRaised1,
            this.fieldDateScheduled1,
            this.fieldDateCompleted1,
            this.fieldActionStatus1,
            this.fieldPoleNumber1,
            this.fieldLastInspectionDate1,
            this.fieldInspectionCycle1,
            this.fieldInspectionUnitDesc1,
            this.fieldNextInspectionDate1,
            this.fieldIsTransformer1,
            this.fieldTransformerNumber1,
            this.fieldClientName1,
            this.fieldClientCode1,
            this.fieldCircuitName1,
            this.fieldCircuitNumber1,
            this.fieldVoltageType1,
            this.fieldFeederName1,
            this.fieldRegionName1,
            this.fieldPoleStatus1,
            this.fieldPrimaryName1,
            this.fieldSubAreaName1,
            this.fieldPoleType1,
            this.fieldSurveyorName1,
            this.fieldPermissionCount1,
            this.fieldPermissionDocumentDateRaisedYear,
            this.fieldPermissionDocumentDateRaisedQuarter,
            this.fieldPermissionDocumentDateRaisedMonth,
            this.fieldPermissionDocumentDateRaisedWeek,
            this.fieldEmailedToClientDateYear,
            this.fieldEmailedToClientDateQuarter,
            this.fieldEmailedToClientDateMonth,
            this.fieldEmailedToClientDateWeek,
            this.fieldEmailedToCustomerDateYear,
            this.fieldEmailedToCustomerDateQuarter,
            this.fieldEmailedToCustomerDateMonth,
            this.fieldEmailedToCustomerDateWeek,
            this.fieldDateRaisedYear,
            this.fieldDateRaisedQuarter,
            this.fieldDateRaisedMonth,
            this.fieldDateRaisedWeek,
            this.fieldDateScheduledYear,
            this.fieldDateScheduledQuarter,
            this.fieldDateScheduledMonth,
            this.fieldDateScheduledWeek,
            this.fieldDateCompletedYear,
            this.fieldDateCompletedQuarter,
            this.fieldDateCompletedMonth,
            this.fieldDateCompletedWeek,
            this.fieldLastInspectionDateYear,
            this.fieldLastInspectionDateQuarter,
            this.fieldLastInspectionDateMonth,
            this.fieldLastInspectionDateWeek,
            this.fieldNextInspectionDateYear,
            this.fieldNextInspectionDateQuarter,
            this.fieldNextInspectionDateMonth,
            this.fieldNextInspectionDateWeek,
            this.fieldPermissionEmailed,
            this.fieldPostageRequired,
            this.fieldSentByPost,
            this.fieldSentByStaffName});
            pivotGridGroup1.Caption = "PD Date Raised Drill Down";
            pivotGridGroup1.Fields.Add(this.fieldPermissionDocumentDateRaisedYear);
            pivotGridGroup1.Fields.Add(this.fieldPermissionDocumentDateRaisedQuarter);
            pivotGridGroup1.Fields.Add(this.fieldPermissionDocumentDateRaisedMonth);
            pivotGridGroup1.Fields.Add(this.fieldPermissionDocumentDateRaisedWeek);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Emailed To Client Date Drill Down";
            pivotGridGroup2.Fields.Add(this.fieldEmailedToClientDateYear);
            pivotGridGroup2.Fields.Add(this.fieldEmailedToClientDateQuarter);
            pivotGridGroup2.Fields.Add(this.fieldEmailedToClientDateMonth);
            pivotGridGroup2.Fields.Add(this.fieldEmailedToClientDateWeek);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Emailed To Customer Date Drill Down";
            pivotGridGroup3.Fields.Add(this.fieldEmailedToCustomerDateYear);
            pivotGridGroup3.Fields.Add(this.fieldEmailedToCustomerDateQuarter);
            pivotGridGroup3.Fields.Add(this.fieldEmailedToCustomerDateMonth);
            pivotGridGroup3.Fields.Add(this.fieldEmailedToCustomerDateWeek);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            pivotGridGroup4.Caption = "Job Date Raised Drill Down";
            pivotGridGroup4.Fields.Add(this.fieldDateRaisedYear);
            pivotGridGroup4.Fields.Add(this.fieldDateRaisedQuarter);
            pivotGridGroup4.Fields.Add(this.fieldDateRaisedMonth);
            pivotGridGroup4.Fields.Add(this.fieldDateRaisedWeek);
            pivotGridGroup4.Hierarchy = null;
            pivotGridGroup4.ShowNewValues = true;
            pivotGridGroup5.Caption = "Job Date Scheduled Drill Down";
            pivotGridGroup5.Fields.Add(this.fieldDateScheduledYear);
            pivotGridGroup5.Fields.Add(this.fieldDateScheduledQuarter);
            pivotGridGroup5.Fields.Add(this.fieldDateScheduledMonth);
            pivotGridGroup5.Fields.Add(this.fieldDateScheduledWeek);
            pivotGridGroup5.Hierarchy = null;
            pivotGridGroup5.ShowNewValues = true;
            pivotGridGroup6.Caption = "Job Date Completed Drill Down";
            pivotGridGroup6.Fields.Add(this.fieldDateCompletedYear);
            pivotGridGroup6.Fields.Add(this.fieldDateCompletedQuarter);
            pivotGridGroup6.Fields.Add(this.fieldDateCompletedMonth);
            pivotGridGroup6.Fields.Add(this.fieldDateCompletedWeek);
            pivotGridGroup6.Hierarchy = null;
            pivotGridGroup6.ShowNewValues = true;
            pivotGridGroup7.Caption = "Last Inspection Date Drill Down";
            pivotGridGroup7.Fields.Add(this.fieldLastInspectionDateYear);
            pivotGridGroup7.Fields.Add(this.fieldLastInspectionDateQuarter);
            pivotGridGroup7.Fields.Add(this.fieldLastInspectionDateMonth);
            pivotGridGroup7.Fields.Add(this.fieldLastInspectionDateWeek);
            pivotGridGroup7.Hierarchy = null;
            pivotGridGroup7.ShowNewValues = true;
            pivotGridGroup8.Caption = "Next Inspection Date Drill Down";
            pivotGridGroup8.Fields.Add(this.fieldNextInspectionDateYear);
            pivotGridGroup8.Fields.Add(this.fieldNextInspectionDateQuarter);
            pivotGridGroup8.Fields.Add(this.fieldNextInspectionDateMonth);
            pivotGridGroup8.Fields.Add(this.fieldNextInspectionDateWeek);
            pivotGridGroup8.Hierarchy = null;
            pivotGridGroup8.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3,
            pivotGridGroup4,
            pivotGridGroup5,
            pivotGridGroup6,
            pivotGridGroup7,
            pivotGridGroup8});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.Size = new System.Drawing.Size(849, 388);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp07326UTAnalysisPermissionsForAnalysisBindingSource
            // 
            this.sp07326UTAnalysisPermissionsForAnalysisBindingSource.DataMember = "sp07326_UT_Analysis_Permissions_For_Analysis";
            this.sp07326UTAnalysisPermissionsForAnalysisBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // fieldActionPermissionID1
            // 
            this.fieldActionPermissionID1.AreaIndex = 0;
            this.fieldActionPermissionID1.Caption = "Action Permission ID";
            this.fieldActionPermissionID1.FieldName = "ActionPermissionID";
            this.fieldActionPermissionID1.Name = "fieldActionPermissionID1";
            this.fieldActionPermissionID1.Visible = false;
            // 
            // fieldPermissionStatus1
            // 
            this.fieldPermissionStatus1.AreaIndex = 0;
            this.fieldPermissionStatus1.Caption = "Permission Status";
            this.fieldPermissionStatus1.FieldName = "PermissionStatus";
            this.fieldPermissionStatus1.Name = "fieldPermissionStatus1";
            // 
            // fieldOwnerType1
            // 
            this.fieldOwnerType1.AreaIndex = 1;
            this.fieldOwnerType1.Caption = "Landowner Type";
            this.fieldOwnerType1.FieldName = "OwnerType";
            this.fieldOwnerType1.Name = "fieldOwnerType1";
            // 
            // fieldLandOwnerName1
            // 
            this.fieldLandOwnerName1.AreaIndex = 2;
            this.fieldLandOwnerName1.Caption = "Landowner Name";
            this.fieldLandOwnerName1.FieldName = "LandOwnerName";
            this.fieldLandOwnerName1.Name = "fieldLandOwnerName1";
            // 
            // fieldPermissionDocumentDateRaised1
            // 
            this.fieldPermissionDocumentDateRaised1.AreaIndex = 3;
            this.fieldPermissionDocumentDateRaised1.Caption = "PD Date Raised";
            this.fieldPermissionDocumentDateRaised1.CellFormat.FormatString = "d";
            this.fieldPermissionDocumentDateRaised1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldPermissionDocumentDateRaised1.FieldName = "PermissionDocumentDateRaised";
            this.fieldPermissionDocumentDateRaised1.Name = "fieldPermissionDocumentDateRaised1";
            // 
            // fieldRaisedByName1
            // 
            this.fieldRaisedByName1.AreaIndex = 4;
            this.fieldRaisedByName1.Caption = "Raised By Name";
            this.fieldRaisedByName1.FieldName = "RaisedByName";
            this.fieldRaisedByName1.Name = "fieldRaisedByName1";
            // 
            // fieldEmailedToClientDate1
            // 
            this.fieldEmailedToClientDate1.AreaIndex = 5;
            this.fieldEmailedToClientDate1.Caption = "Emailed To Client Date";
            this.fieldEmailedToClientDate1.CellFormat.FormatString = "d";
            this.fieldEmailedToClientDate1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldEmailedToClientDate1.FieldName = "EmailedToClientDate";
            this.fieldEmailedToClientDate1.Name = "fieldEmailedToClientDate1";
            // 
            // fieldEmailedToCustomerDate1
            // 
            this.fieldEmailedToCustomerDate1.AreaIndex = 6;
            this.fieldEmailedToCustomerDate1.Caption = "Emailed To Customer Date";
            this.fieldEmailedToCustomerDate1.CellFormat.FormatString = "d";
            this.fieldEmailedToCustomerDate1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldEmailedToCustomerDate1.FieldName = "EmailedToCustomerDate";
            this.fieldEmailedToCustomerDate1.Name = "fieldEmailedToCustomerDate1";
            // 
            // fieldPermissionDocumentRemarks1
            // 
            this.fieldPermissionDocumentRemarks1.AreaIndex = 7;
            this.fieldPermissionDocumentRemarks1.Caption = "PD Remarks";
            this.fieldPermissionDocumentRemarks1.FieldName = "PermissionDocumentRemarks";
            this.fieldPermissionDocumentRemarks1.Name = "fieldPermissionDocumentRemarks1";
            // 
            // fieldTrafficManagement1
            // 
            this.fieldTrafficManagement1.AreaIndex = 8;
            this.fieldTrafficManagement1.Caption = "Traffic Management";
            this.fieldTrafficManagement1.FieldName = "TrafficManagement";
            this.fieldTrafficManagement1.Name = "fieldTrafficManagement1";
            // 
            // fieldHotGloveAccessAvailable1
            // 
            this.fieldHotGloveAccessAvailable1.AreaIndex = 9;
            this.fieldHotGloveAccessAvailable1.Caption = "Hot Glove Access Available";
            this.fieldHotGloveAccessAvailable1.FieldName = "HotGloveAccessAvailable";
            this.fieldHotGloveAccessAvailable1.Name = "fieldHotGloveAccessAvailable1";
            // 
            // fieldAccessAgreedWithLandOwner1
            // 
            this.fieldAccessAgreedWithLandOwner1.AreaIndex = 10;
            this.fieldAccessAgreedWithLandOwner1.Caption = "Access Agreed With Landowner";
            this.fieldAccessAgreedWithLandOwner1.FieldName = "AccessAgreedWithLandOwner";
            this.fieldAccessAgreedWithLandOwner1.Name = "fieldAccessAgreedWithLandOwner1";
            // 
            // fieldAccessDetailsOnMap1
            // 
            this.fieldAccessDetailsOnMap1.AreaIndex = 11;
            this.fieldAccessDetailsOnMap1.Caption = "Access Details On Map";
            this.fieldAccessDetailsOnMap1.FieldName = "AccessDetailsOnMap";
            this.fieldAccessDetailsOnMap1.Name = "fieldAccessDetailsOnMap1";
            // 
            // fieldRoadsideAccessOnly1
            // 
            this.fieldRoadsideAccessOnly1.AreaIndex = 12;
            this.fieldRoadsideAccessOnly1.Caption = "Roadside Access Only";
            this.fieldRoadsideAccessOnly1.FieldName = "RoadsideAccessOnly";
            this.fieldRoadsideAccessOnly1.Name = "fieldRoadsideAccessOnly1";
            // 
            // fieldClearArisingsType1
            // 
            this.fieldClearArisingsType1.AreaIndex = 13;
            this.fieldClearArisingsType1.Caption = "Clear Arisings Type";
            this.fieldClearArisingsType1.FieldName = "ClearArisingsType";
            this.fieldClearArisingsType1.Name = "fieldClearArisingsType1";
            // 
            // fieldLandOwnerRestrictedCut1
            // 
            this.fieldLandOwnerRestrictedCut1.AreaIndex = 14;
            this.fieldLandOwnerRestrictedCut1.Caption = "Land Owner Restricted Cut";
            this.fieldLandOwnerRestrictedCut1.FieldName = "LandOwnerRestrictedCut";
            this.fieldLandOwnerRestrictedCut1.Name = "fieldLandOwnerRestrictedCut1";
            // 
            // fieldEstimatedRevisitDate1
            // 
            this.fieldEstimatedRevisitDate1.AreaIndex = 15;
            this.fieldEstimatedRevisitDate1.Caption = "Estimated Revisit Date";
            this.fieldEstimatedRevisitDate1.CellFormat.FormatString = "d";
            this.fieldEstimatedRevisitDate1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldEstimatedRevisitDate1.FieldName = "EstimatedRevisitDate";
            this.fieldEstimatedRevisitDate1.Name = "fieldEstimatedRevisitDate1";
            // 
            // fieldTPOTree1
            // 
            this.fieldTPOTree1.AreaIndex = 16;
            this.fieldTPOTree1.Caption = "TPO Tree";
            this.fieldTPOTree1.FieldName = "TPOTree";
            this.fieldTPOTree1.Name = "fieldTPOTree1";
            // 
            // fieldPlanningConservation1
            // 
            this.fieldPlanningConservation1.AreaIndex = 17;
            this.fieldPlanningConservation1.Caption = "Planning Conservation";
            this.fieldPlanningConservation1.FieldName = "PlanningConservation";
            this.fieldPlanningConservation1.Name = "fieldPlanningConservation1";
            // 
            // fieldWildlifeDesignation1
            // 
            this.fieldWildlifeDesignation1.AreaIndex = 18;
            this.fieldWildlifeDesignation1.Caption = "Wildlife Designation";
            this.fieldWildlifeDesignation1.FieldName = "WildlifeDesignation";
            this.fieldWildlifeDesignation1.Name = "fieldWildlifeDesignation1";
            // 
            // fieldShutdownLOA1
            // 
            this.fieldShutdownLOA1.AreaIndex = 19;
            this.fieldShutdownLOA1.Caption = "Shutdown LOA";
            this.fieldShutdownLOA1.FieldName = "ShutdownLOA";
            this.fieldShutdownLOA1.Name = "fieldShutdownLOA1";
            // 
            // fieldTreeReplacementWhips1
            // 
            this.fieldTreeReplacementWhips1.AreaIndex = 20;
            this.fieldTreeReplacementWhips1.Caption = "Tree Replacement Whips";
            this.fieldTreeReplacementWhips1.FieldName = "TreeReplacementWhips";
            this.fieldTreeReplacementWhips1.Name = "fieldTreeReplacementWhips1";
            // 
            // fieldTreeReplacementStandards1
            // 
            this.fieldTreeReplacementStandards1.AreaIndex = 21;
            this.fieldTreeReplacementStandards1.Caption = "Tree Replacement Standards";
            this.fieldTreeReplacementStandards1.FieldName = "TreeReplacementStandards";
            this.fieldTreeReplacementStandards1.Name = "fieldTreeReplacementStandards1";
            // 
            // fieldTreeReplacementNone1
            // 
            this.fieldTreeReplacementNone1.AreaIndex = 22;
            this.fieldTreeReplacementNone1.Caption = "Tree Replacement None";
            this.fieldTreeReplacementNone1.FieldName = "TreeReplacementNone";
            this.fieldTreeReplacementNone1.Name = "fieldTreeReplacementNone1";
            // 
            // fieldStumpTreatmentEcoPlugs1
            // 
            this.fieldStumpTreatmentEcoPlugs1.AreaIndex = 23;
            this.fieldStumpTreatmentEcoPlugs1.Caption = "Stump Treatment Eco Plugs";
            this.fieldStumpTreatmentEcoPlugs1.FieldName = "StumpTreatmentEcoPlugs";
            this.fieldStumpTreatmentEcoPlugs1.Name = "fieldStumpTreatmentEcoPlugs1";
            // 
            // fieldStumpTreatmentSpraying1
            // 
            this.fieldStumpTreatmentSpraying1.AreaIndex = 24;
            this.fieldStumpTreatmentSpraying1.Caption = "Stump Treatment Spraying";
            this.fieldStumpTreatmentSpraying1.FieldName = "StumpTreatmentSpraying";
            this.fieldStumpTreatmentSpraying1.Name = "fieldStumpTreatmentSpraying1";
            // 
            // fieldStumpTreatmentPaint1
            // 
            this.fieldStumpTreatmentPaint1.AreaIndex = 25;
            this.fieldStumpTreatmentPaint1.Caption = "Stump Treatment Paint";
            this.fieldStumpTreatmentPaint1.FieldName = "StumpTreatmentPaint";
            this.fieldStumpTreatmentPaint1.Name = "fieldStumpTreatmentPaint1";
            // 
            // fieldStumpTreatmentNone1
            // 
            this.fieldStumpTreatmentNone1.AreaIndex = 26;
            this.fieldStumpTreatmentNone1.Caption = "Stump Treatment None";
            this.fieldStumpTreatmentNone1.FieldName = "StumpTreatmentNone";
            this.fieldStumpTreatmentNone1.Name = "fieldStumpTreatmentNone1";
            // 
            // fieldManagedUnitNumber1
            // 
            this.fieldManagedUnitNumber1.AreaIndex = 27;
            this.fieldManagedUnitNumber1.Caption = "Managed Unit #";
            this.fieldManagedUnitNumber1.FieldName = "ManagedUnitNumber";
            this.fieldManagedUnitNumber1.Name = "fieldManagedUnitNumber1";
            // 
            // fieldEnvironmentalRANumber1
            // 
            this.fieldEnvironmentalRANumber1.AreaIndex = 28;
            this.fieldEnvironmentalRANumber1.Caption = "Environmental RA #";
            this.fieldEnvironmentalRANumber1.FieldName = "EnvironmentalRANumber";
            this.fieldEnvironmentalRANumber1.Name = "fieldEnvironmentalRANumber1";
            // 
            // fieldLandscapeImpactNumber1
            // 
            this.fieldLandscapeImpactNumber1.AreaIndex = 29;
            this.fieldLandscapeImpactNumber1.Caption = "Landscape Impact #";
            this.fieldLandscapeImpactNumber1.FieldName = "LandscapeImpactNumber";
            this.fieldLandscapeImpactNumber1.Name = "fieldLandscapeImpactNumber1";
            // 
            // fieldSiteGridReference1
            // 
            this.fieldSiteGridReference1.AreaIndex = 30;
            this.fieldSiteGridReference1.Caption = "Site Grid Reference";
            this.fieldSiteGridReference1.FieldName = "SiteGridReference";
            this.fieldSiteGridReference1.Name = "fieldSiteGridReference1";
            // 
            // fieldJobTypeDescription1
            // 
            this.fieldJobTypeDescription1.AreaIndex = 31;
            this.fieldJobTypeDescription1.Caption = "Job Type";
            this.fieldJobTypeDescription1.FieldName = "JobTypeDescription";
            this.fieldJobTypeDescription1.Name = "fieldJobTypeDescription1";
            // 
            // fieldReferenceNumber1
            // 
            this.fieldReferenceNumber1.AreaIndex = 32;
            this.fieldReferenceNumber1.Caption = "Job Reference #";
            this.fieldReferenceNumber1.FieldName = "ReferenceNumber";
            this.fieldReferenceNumber1.Name = "fieldReferenceNumber1";
            // 
            // fieldDateRaised1
            // 
            this.fieldDateRaised1.AreaIndex = 33;
            this.fieldDateRaised1.Caption = "Job Date Raised";
            this.fieldDateRaised1.CellFormat.FormatString = "d";
            this.fieldDateRaised1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateRaised1.FieldName = "DateRaised";
            this.fieldDateRaised1.Name = "fieldDateRaised1";
            // 
            // fieldDateScheduled1
            // 
            this.fieldDateScheduled1.AreaIndex = 34;
            this.fieldDateScheduled1.Caption = "Job Date Scheduled";
            this.fieldDateScheduled1.CellFormat.FormatString = "d";
            this.fieldDateScheduled1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateScheduled1.FieldName = "DateScheduled";
            this.fieldDateScheduled1.Name = "fieldDateScheduled1";
            // 
            // fieldDateCompleted1
            // 
            this.fieldDateCompleted1.AreaIndex = 35;
            this.fieldDateCompleted1.Caption = "Job Date Completed";
            this.fieldDateCompleted1.CellFormat.FormatString = "d";
            this.fieldDateCompleted1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateCompleted1.FieldName = "DateCompleted";
            this.fieldDateCompleted1.Name = "fieldDateCompleted1";
            // 
            // fieldActionStatus1
            // 
            this.fieldActionStatus1.AreaIndex = 36;
            this.fieldActionStatus1.Caption = "Job Status";
            this.fieldActionStatus1.FieldName = "ActionStatus";
            this.fieldActionStatus1.Name = "fieldActionStatus1";
            // 
            // fieldPoleNumber1
            // 
            this.fieldPoleNumber1.AreaIndex = 51;
            this.fieldPoleNumber1.Caption = "Pole #";
            this.fieldPoleNumber1.FieldName = "PoleNumber";
            this.fieldPoleNumber1.Name = "fieldPoleNumber1";
            // 
            // fieldLastInspectionDate1
            // 
            this.fieldLastInspectionDate1.AreaIndex = 37;
            this.fieldLastInspectionDate1.Caption = "Last Inspection Date";
            this.fieldLastInspectionDate1.CellFormat.FormatString = "d";
            this.fieldLastInspectionDate1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldLastInspectionDate1.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDate1.Name = "fieldLastInspectionDate1";
            // 
            // fieldInspectionCycle1
            // 
            this.fieldInspectionCycle1.AreaIndex = 38;
            this.fieldInspectionCycle1.Caption = "Inspection Cycle";
            this.fieldInspectionCycle1.FieldName = "InspectionCycle";
            this.fieldInspectionCycle1.Name = "fieldInspectionCycle1";
            // 
            // fieldInspectionUnitDesc1
            // 
            this.fieldInspectionUnitDesc1.AreaIndex = 39;
            this.fieldInspectionUnitDesc1.Caption = "Inspection Unit Desc";
            this.fieldInspectionUnitDesc1.FieldName = "InspectionUnitDesc";
            this.fieldInspectionUnitDesc1.Name = "fieldInspectionUnitDesc1";
            // 
            // fieldNextInspectionDate1
            // 
            this.fieldNextInspectionDate1.AreaIndex = 40;
            this.fieldNextInspectionDate1.Caption = "Next Inspection Date";
            this.fieldNextInspectionDate1.CellFormat.FormatString = "d";
            this.fieldNextInspectionDate1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldNextInspectionDate1.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDate1.Name = "fieldNextInspectionDate1";
            // 
            // fieldIsTransformer1
            // 
            this.fieldIsTransformer1.AreaIndex = 41;
            this.fieldIsTransformer1.Caption = "Is Transformer";
            this.fieldIsTransformer1.FieldName = "IsTransformer";
            this.fieldIsTransformer1.Name = "fieldIsTransformer1";
            // 
            // fieldTransformerNumber1
            // 
            this.fieldTransformerNumber1.AreaIndex = 42;
            this.fieldTransformerNumber1.Caption = "Transformer #";
            this.fieldTransformerNumber1.FieldName = "TransformerNumber";
            this.fieldTransformerNumber1.Name = "fieldTransformerNumber1";
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 43;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            // 
            // fieldClientCode1
            // 
            this.fieldClientCode1.AreaIndex = 44;
            this.fieldClientCode1.Caption = "Client Code";
            this.fieldClientCode1.FieldName = "ClientCode";
            this.fieldClientCode1.Name = "fieldClientCode1";
            // 
            // fieldCircuitName1
            // 
            this.fieldCircuitName1.AreaIndex = 49;
            this.fieldCircuitName1.Caption = "Circuit Name";
            this.fieldCircuitName1.FieldName = "CircuitName";
            this.fieldCircuitName1.Name = "fieldCircuitName1";
            // 
            // fieldCircuitNumber1
            // 
            this.fieldCircuitNumber1.AreaIndex = 50;
            this.fieldCircuitNumber1.Caption = "Circuit #";
            this.fieldCircuitNumber1.FieldName = "CircuitNumber";
            this.fieldCircuitNumber1.Name = "fieldCircuitNumber1";
            // 
            // fieldVoltageType1
            // 
            this.fieldVoltageType1.AreaIndex = 54;
            this.fieldVoltageType1.Caption = "Voltage Type";
            this.fieldVoltageType1.FieldName = "VoltageType";
            this.fieldVoltageType1.Name = "fieldVoltageType1";
            // 
            // fieldFeederName1
            // 
            this.fieldFeederName1.AreaIndex = 48;
            this.fieldFeederName1.Caption = "Feeder Name";
            this.fieldFeederName1.FieldName = "FeederName";
            this.fieldFeederName1.Name = "fieldFeederName1";
            // 
            // fieldRegionName1
            // 
            this.fieldRegionName1.AreaIndex = 45;
            this.fieldRegionName1.Caption = "Region Name";
            this.fieldRegionName1.FieldName = "RegionName";
            this.fieldRegionName1.Name = "fieldRegionName1";
            // 
            // fieldPoleStatus1
            // 
            this.fieldPoleStatus1.AreaIndex = 52;
            this.fieldPoleStatus1.Caption = "Pole Status";
            this.fieldPoleStatus1.FieldName = "PoleStatus";
            this.fieldPoleStatus1.Name = "fieldPoleStatus1";
            // 
            // fieldPrimaryName1
            // 
            this.fieldPrimaryName1.AreaIndex = 47;
            this.fieldPrimaryName1.Caption = "Primary Name";
            this.fieldPrimaryName1.FieldName = "PrimaryName";
            this.fieldPrimaryName1.Name = "fieldPrimaryName1";
            // 
            // fieldSubAreaName1
            // 
            this.fieldSubAreaName1.AreaIndex = 46;
            this.fieldSubAreaName1.Caption = "Sub Area Name";
            this.fieldSubAreaName1.FieldName = "SubAreaName";
            this.fieldSubAreaName1.Name = "fieldSubAreaName1";
            // 
            // fieldPoleType1
            // 
            this.fieldPoleType1.AreaIndex = 53;
            this.fieldPoleType1.Caption = "Pole Type";
            this.fieldPoleType1.FieldName = "PoleType";
            this.fieldPoleType1.Name = "fieldPoleType1";
            // 
            // fieldSurveyorName1
            // 
            this.fieldSurveyorName1.AreaIndex = 55;
            this.fieldSurveyorName1.Caption = "Surveyor Name";
            this.fieldSurveyorName1.FieldName = "SurveyorName";
            this.fieldSurveyorName1.Name = "fieldSurveyorName1";
            // 
            // fieldPermissionCount1
            // 
            this.fieldPermissionCount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldPermissionCount1.AreaIndex = 0;
            this.fieldPermissionCount1.Caption = "Permission Count";
            this.fieldPermissionCount1.FieldName = "PermissionCount";
            this.fieldPermissionCount1.Name = "fieldPermissionCount1";
            // 
            // fieldPermissionEmailed
            // 
            this.fieldPermissionEmailed.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldPermissionEmailed.AreaIndex = 0;
            this.fieldPermissionEmailed.Caption = "Permission Doc Emailed";
            this.fieldPermissionEmailed.FieldName = "PermissionEmailed";
            this.fieldPermissionEmailed.Name = "fieldPermissionEmailed";
            this.fieldPermissionEmailed.Visible = false;
            // 
            // fieldPostageRequired
            // 
            this.fieldPostageRequired.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldPostageRequired.AreaIndex = 1;
            this.fieldPostageRequired.Caption = "Postage Required";
            this.fieldPostageRequired.FieldName = "PostageRequired";
            this.fieldPostageRequired.Name = "fieldPostageRequired";
            this.fieldPostageRequired.Visible = false;
            // 
            // fieldSentByPost
            // 
            this.fieldSentByPost.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldSentByPost.AreaIndex = 2;
            this.fieldSentByPost.Caption = "Sent By Post";
            this.fieldSentByPost.FieldName = "SentByPost";
            this.fieldSentByPost.Name = "fieldSentByPost";
            this.fieldSentByPost.Visible = false;
            // 
            // fieldSentByStaffName
            // 
            this.fieldSentByStaffName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldSentByStaffName.AreaIndex = 3;
            this.fieldSentByStaffName.Caption = "Sent By Staff";
            this.fieldSentByStaffName.FieldName = "SentByStaffName";
            this.fieldSentByStaffName.Name = "fieldSentByStaffName";
            this.fieldSentByStaffName.Visible = false;
            // 
            // chartControl1
            // 
            this.chartControl1.DataSource = this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.Size = new System.Drawing.Size(849, 100);
            this.chartControl1.TabIndex = 1;
            // 
            // sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter
            // 
            this.sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter.ClearBeforeFill = true;
            // 
            // sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter
            // 
            this.sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07322_UT_Master_Job_TypesTableAdapter
            // 
            this.sp07322_UT_Master_Job_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToggleAvailableColumnsVisibility, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiToggleAvailableColumnsVisibility
            // 
            this.bbiToggleAvailableColumnsVisibility.Caption = "Toggle Available Columns Visibility";
            this.bbiToggleAvailableColumnsVisibility.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiToggleAvailableColumnsVisibility.Glyph")));
            this.bbiToggleAvailableColumnsVisibility.Id = 32;
            this.bbiToggleAvailableColumnsVisibility.Name = "bbiToggleAvailableColumnsVisibility";
            this.bbiToggleAvailableColumnsVisibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToggleAvailableColumnsVisibility_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(580, 206);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonEditFilterCircuits),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemAction),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemDateFilter
            // 
            this.barEditItemDateFilter.Caption = "Date Filter";
            this.barEditItemDateFilter.Edit = this.repositoryItemPopupContainerEditDateFilter;
            this.barEditItemDateFilter.EditValue = "No Date Filter";
            this.barEditItemDateFilter.EditWidth = 97;
            this.barEditItemDateFilter.Id = 29;
            this.barEditItemDateFilter.Name = "barEditItemDateFilter";
            // 
            // repositoryItemPopupContainerEditDateFilter
            // 
            this.repositoryItemPopupContainerEditDateFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateFilter.Name = "repositoryItemPopupContainerEditDateFilter";
            this.repositoryItemPopupContainerEditDateFilter.PopupControl = this.popupContainerControlDateFilter;
            this.repositoryItemPopupContainerEditDateFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditDateFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateFilter_QueryResultValue);
            // 
            // buttonEditFilterCircuits
            // 
            this.buttonEditFilterCircuits.Caption = "Circuit Filter";
            this.buttonEditFilterCircuits.Edit = this.repositoryItemButtonEditFilterCircuits;
            this.buttonEditFilterCircuits.EditValue = "No Circuit Filter";
            this.buttonEditFilterCircuits.EditWidth = 130;
            this.buttonEditFilterCircuits.Id = 34;
            this.buttonEditFilterCircuits.Name = "buttonEditFilterCircuits";
            // 
            // repositoryItemButtonEditFilterCircuits
            // 
            this.repositoryItemButtonEditFilterCircuits.AutoHeight = false;
            this.repositoryItemButtonEditFilterCircuits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "choose", null, true)});
            this.repositoryItemButtonEditFilterCircuits.Name = "repositoryItemButtonEditFilterCircuits";
            this.repositoryItemButtonEditFilterCircuits.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditFilterCircuits.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFilterCircuits_ButtonClick);
            // 
            // barEditItemAction
            // 
            this.barEditItemAction.Caption = "Action Filter";
            this.barEditItemAction.Edit = this.repositoryItemPopupContainerEditActionFilter;
            this.barEditItemAction.EditValue = "No Action Filter";
            this.barEditItemAction.EditWidth = 102;
            this.barEditItemAction.Id = 35;
            this.barEditItemAction.Name = "barEditItemAction";
            // 
            // repositoryItemPopupContainerEditActionFilter
            // 
            this.repositoryItemPopupContainerEditActionFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditActionFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditActionFilter.Name = "repositoryItemPopupContainerEditActionFilter";
            this.repositoryItemPopupContainerEditActionFilter.PopupControl = this.popupContainerControlActionFilter;
            this.repositoryItemPopupContainerEditActionFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditActionFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditActionFilter_QueryResultValue);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Caption = "Reload";
            this.bbiReloadData.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiReloadData.Glyph")));
            this.bbiReloadData.Id = 30;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Reload Data - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReloadData.SuperTip = superToolTip3;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // bbiAnalyse
            // 
            this.bbiAnalyse.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAnalyse.Caption = "Analyse";
            this.bbiAnalyse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAnalyse.Glyph")));
            this.bbiAnalyse.Id = 31;
            this.bbiAnalyse.Name = "bbiAnalyse";
            this.bbiAnalyse.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "Analyse - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the Analysis Grid and Chart with the records selected in the Dat" +
    "a Supply list.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAnalyse.SuperTip = superToolTip4;
            // 
            // barEditItemStatusFilter
            // 
            this.barEditItemStatusFilter.Caption = "Status Filter";
            this.barEditItemStatusFilter.Edit = this.repositoryItemPopupContainerEditStatusFilter;
            this.barEditItemStatusFilter.EditValue = "All Statuses";
            this.barEditItemStatusFilter.EditWidth = 83;
            this.barEditItemStatusFilter.Id = 28;
            this.barEditItemStatusFilter.Name = "barEditItemStatusFilter";
            // 
            // repositoryItemPopupContainerEditStatusFilter
            // 
            this.repositoryItemPopupContainerEditStatusFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditStatusFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStatusFilter.Name = "repositoryItemPopupContainerEditStatusFilter";
            this.repositoryItemPopupContainerEditStatusFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditStatusFilter.ShowPopupCloseButton = false;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit2;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Analysis_Permissioned_Work
            // 
            this.ClientSize = new System.Drawing.Size(1293, 494);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Analysis_Permissioned_Work";
            this.Text = "Utilities - Permissioned Work Analysis";
            this.Activated += new System.EventHandler(this.frm_UT_Analysis_Permissioned_Work_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Analysis_Permissioned_Work_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_UT_Analysis_Permissioned_Work_FormClosed);
            this.Load += new System.EventHandler(this.frm_UT_Analysis_Permissioned_Work_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlActionFilter)).EndInit();
            this.popupContainerControlActionFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07322UTMasterJobTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).EndInit();
            this.popupContainerControlDateFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07326UTAnalysisPermissionsForAnalysisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditActionFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStatusFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateFilter;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraBars.BarButtonItem bbiAnalyse;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateFilter;
        private DevExpress.XtraEditors.SimpleButton btnDateFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraBars.BarButtonItem bbiToggleAvailableColumnsVisibility;
        private DevExpress.XtraBars.BarEditItem buttonEditFilterCircuits;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditFilterCircuits;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.SimpleButton btnAnalyse;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedCount;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private DevExpress.XtraBars.BarEditItem barEditItemAction;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditActionFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlActionFilter;
        private DevExpress.XtraEditors.SimpleButton ActionFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DataSet_UT_ReportingTableAdapters.sp07322_UT_Master_Job_TypesTableAdapter sp07322_UT_Master_Job_TypesTableAdapter;
        private System.Windows.Forms.BindingSource sp07322UTMasterJobTypesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DataSet_UT_ReportingTableAdapters.sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter sp07325_UT_Analysis_Permissions_Available_PermissionsTableAdapter;
        private System.Windows.Forms.BindingSource sp07325UTAnalysisPermissionsAvailablePermissionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPermissionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagement;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveAccessAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessAgreedWithLandOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessDetailsOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadsideAccessOnly;
        private DevExpress.XtraGrid.Columns.GridColumn colClearArisingsType;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerRestrictedCut;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOTree;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningConservation;
        private DevExpress.XtraGrid.Columns.GridColumn colWildlifeDesignation;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownLOA;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colManagedUnitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colEnvironmentalRANumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLandscapeImpactNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyorName;
        private System.Windows.Forms.BindingSource sp07326UTAnalysisPermissionsForAnalysisBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionPermissionID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOwnerType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLandOwnerName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentDateRaised1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRaisedByName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToClientDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToCustomerDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTrafficManagement1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHotGloveAccessAvailable1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccessAgreedWithLandOwner1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAccessDetailsOnMap1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRoadsideAccessOnly1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClearArisingsType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLandOwnerRestrictedCut1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedRevisitDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTPOTree1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPlanningConservation1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWildlifeDesignation1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldShutdownLOA1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementWhips1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementStandards1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementNone1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentEcoPlugs1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentSpraying1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentPaint1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentNone1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldManagedUnitNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEnvironmentalRANumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLandscapeImpactNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSiteGridReference1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobTypeDescription1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReferenceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaised1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduled1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompleted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCycle1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUnitDesc1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsTransformer1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTransformerNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientCode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVoltageType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFeederName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRegionName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPrimaryName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubAreaName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCount1;
        private DataSet_UT_ReportingTableAdapters.sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter sp07326_UT_Analysis_Permissions_For_AnalysisTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentDateRaisedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentDateRaisedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentDateRaisedMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionDocumentDateRaisedWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToClientDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToClientDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToClientDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToClientDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToCustomerDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToCustomerDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToCustomerDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEmailedToCustomerDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionEmailed;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPostageRequired;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSentByPost;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSentByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colPostageRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByPost;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraLayout;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Permission_Doc_Refuse_Permission : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        public string _PassedInReferenceNumber = "";
        public string _PassedInPoleNumber = "";
        public string _PassedInAgreedWork = "";
        public int _SelectedReasonID = 0;
        public string _SelectedRemarks = "";
        public string _SelectedReason = "";
 
        #endregion
        
        public frm_UT_Permission_Doc_Refuse_Permission()
        {
            InitializeComponent();
        }

        private void frm_UT_Permission_Doc_Refuse_Permission_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500083;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp07399_UT_PD_Refusal_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07399_UT_PD_Refusal_Reasons_With_BlankTableAdapter.Fill(dataSet_UT_WorkOrder.sp07399_UT_PD_Refusal_Reasons_With_Blank);

            ReferenceNumberTextEdit.EditValue = _PassedInReferenceNumber;
            PoleNumberTextEdit.EditValue = _PassedInPoleNumber;
            AgreedWorkTextEdit.EditValue = _PassedInAgreedWork;
            RefusalReasonIDGridLookUpEdit.EditValue = 0;
            RemarksMemoEdit.EditValue = "";

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            dxErrorProvider1.ClearErrors();
            this.ValidateChildren();
        }

        public override void PostLoadView(object objParameter)
        {
        }


        #region Editors

        private void RefusalReasonIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(RefusalReasonIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RefusalReasonIDGridLookUpEdit, "");
            }
        }

        #endregion


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, LayoutControl Layoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                //layoutControl1.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = Layoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        Layoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        Layoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, layoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Block Add Dummy Inspections", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            _SelectedReasonID = Convert.ToInt32(RefusalReasonIDGridLookUpEdit.EditValue);
            _SelectedRemarks = RemarksMemoEdit.EditValue.ToString();
            _SelectedReason = RefusalReasonIDGridLookUpEdit.Text.ToString();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }




    }
}


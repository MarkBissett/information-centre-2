namespace WoodPlan5
{
    partial class frm_UT_WorkOrder_Add_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_WorkOrder_Add_Actions));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditDateFrom = new DevExpress.XtraEditors.DateEdit();
            this.dateEditDateTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(954, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 602);
            this.barDockControlBottom.Size = new System.Drawing.Size(954, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 576);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(954, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 576);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageList1;
            this.barManager1.MaxItemId = 31;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl3);
            this.splitContainerControl1.Panel1.Controls.Add(this.pictureEdit1);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.groupControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.btnRefresh);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(954, 539);
            this.splitContainerControl1.SplitterPosition = 84;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Location = new System.Drawing.Point(26, 9);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(492, 13);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "Adjust Filters and click Load Data if required. <b>Tick Actions</b> to add to wor" +
    "k order then click <b>OK</b> button.";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(3, 4);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(21, 22);
            this.pictureEdit1.TabIndex = 25;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.dateEditDateFrom);
            this.groupControl2.Controls.Add(this.dateEditDateTo);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Location = new System.Drawing.Point(3, 29);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(308, 52);
            this.groupControl2.TabIndex = 24;
            this.groupControl2.Text = "Date Range";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "From:";
            // 
            // dateEditDateFrom
            // 
            this.dateEditDateFrom.EditValue = null;
            this.dateEditDateFrom.Location = new System.Drawing.Point(38, 26);
            this.dateEditDateFrom.MenuManager = this.barManager1;
            this.dateEditDateFrom.Name = "dateEditDateFrom";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditDateFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, superToolTip1, true)});
            this.dateEditDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateFrom.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateFrom.Properties.NullValuePrompt = "No Date";
            this.dateEditDateFrom.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateFrom.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateFrom.TabIndex = 21;
            this.dateEditDateFrom.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateFrom_ButtonClick);
            // 
            // dateEditDateTo
            // 
            this.dateEditDateTo.EditValue = null;
            this.dateEditDateTo.Location = new System.Drawing.Point(183, 27);
            this.dateEditDateTo.MenuManager = this.barManager1;
            this.dateEditDateTo.Name = "dateEditDateTo";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditDateTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, superToolTip2, true)});
            this.dateEditDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditDateTo.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditDateTo.Properties.NullValuePrompt = "No Date";
            this.dateEditDateTo.Properties.NullValuePromptShowForEmptyValue = true;
            this.dateEditDateTo.Size = new System.Drawing.Size(120, 20);
            this.dateEditDateTo.TabIndex = 22;
            this.dateEditDateTo.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditDateTo_ButtonClick);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(163, 30);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "To:";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit3);
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(317, 30);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(551, 51);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Action Type";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(336, 26);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Actions Already Linked to a Work Order";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(211, 19);
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Actions Already Linked To a Work Order - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Use this option to load a list of actions which have already been linked to a pri" +
    "or work order. If actions are then selected from this list, they will be removed" +
    " from the original work order first.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.checkEdit3.SuperTip = superToolTip3;
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(178, 26);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Complete Unlinked Actions";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(152, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            // 
            // checkEdit1
            // 
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Outstanding Unlinked Actions";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(166, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(874, 58);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(73, 23);
            this.btnRefresh.TabIndex = 20;
            this.btnRefresh.Text = "Load Data";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(954, 449);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(954, 449);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07275UTWorkOrderActionsAvailableForAddingBindingSource
            // 
            this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource.DataMember = "sp07275_UT_Work_Order_Actions_Available_For_Adding";
            this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks2,
            this.colGUID,
            this.colLinkedRecordDescription,
            this.colJobDescription,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.colPaddedWorkOrderID,
            this.colWorkOrderSortOrder,
            this.colRegionName,
            this.colRegionNumber,
            this.colSubAreaName,
            this.colSubAreaNumber,
            this.colPrimaryName,
            this.colPrimaryNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colPoleNumber,
            this.colTreeReferenceNumber,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colEstimatedHours,
            this.colActualHours,
            this.colPossibleLiveWork,
            this.colTeamOnHold,
            this.colTreeSpeciesList,
            this.colHeldUpType,
            this.colIsShutdownRequired});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colJobDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 12;
            this.colReferenceNumber.Width = 112;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 14;
            this.colDateRaised.Width = 99;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 15;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 13;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 16;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 27;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 28;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 35;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 36;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 31;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 32;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 23;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 24;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderID.Width = 77;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 17;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To Tree";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedRecordDescription.Visible = true;
            this.colLinkedRecordDescription.VisibleIndex = 0;
            this.colLinkedRecordDescription.Width = 235;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 1;
            this.colJobDescription.Width = 162;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 25;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 26;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 29;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 30;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 33;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 34;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 37;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 38;
            this.colActualMaterialsSell.Width = 101;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Linked To Work Order";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaddedWorkOrderID.Width = 125;
            // 
            // colWorkOrderSortOrder
            // 
            this.colWorkOrderSortOrder.Caption = "Order";
            this.colWorkOrderSortOrder.FieldName = "WorkOrderSortOrder";
            this.colWorkOrderSortOrder.Name = "colWorkOrderSortOrder";
            this.colWorkOrderSortOrder.OptionsColumn.AllowEdit = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowFocus = false;
            this.colWorkOrderSortOrder.OptionsColumn.ReadOnly = true;
            this.colWorkOrderSortOrder.Width = 62;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 2;
            this.colRegionName.Width = 125;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region #";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 3;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 4;
            this.colSubAreaName.Width = 125;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area #";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 5;
            this.colSubAreaNumber.Width = 77;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 6;
            this.colPrimaryName.Width = 125;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary #";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 7;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 8;
            this.colFeederName.Width = 125;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 9;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 10;
            this.colCircuitName.Width = 125;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit #";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 11;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole #";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 39;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Ref #";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 40;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 42;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 43;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 44;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 45;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 46;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 47;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 48;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 21;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 22;
            this.colActualHours.Width = 82;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 20;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.OptionsColumn.AllowEdit = false;
            this.colTeamOnHold.OptionsColumn.AllowFocus = false;
            this.colTeamOnHold.OptionsColumn.ReadOnly = true;
            this.colTeamOnHold.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTeamOnHold.Width = 87;
            // 
            // colTreeSpeciesList
            // 
            this.colTreeSpeciesList.Caption = "Tree Species";
            this.colTreeSpeciesList.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colTreeSpeciesList.FieldName = "TreeSpeciesList";
            this.colTreeSpeciesList.Name = "colTreeSpeciesList";
            this.colTreeSpeciesList.OptionsColumn.AllowEdit = false;
            this.colTreeSpeciesList.OptionsColumn.AllowFocus = false;
            this.colTreeSpeciesList.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesList.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeSpeciesList.Visible = true;
            this.colTreeSpeciesList.VisibleIndex = 41;
            this.colTreeSpeciesList.Width = 80;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 18;
            this.colHeldUpType.Width = 104;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 19;
            this.colIsShutdownRequired.Width = 113;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(873, 573);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(789, 573);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add_16.png");
            this.imageList1.Images.SetKeyName(1, "edit_16.png");
            this.imageList1.Images.SetKeyName(2, "info_16.png");
            this.imageList1.Images.SetKeyName(3, "attention_16.png");
            this.imageList1.Images.SetKeyName(4, "delete_16.png");
            this.imageList1.Images.SetKeyName(5, "arrow_up_blue_round.png");
            this.imageList1.Images.SetKeyName(6, "arrow_down_blue_round.png");
            // 
            // sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter
            // 
            this.sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_WorkOrder_Add_Actions
            // 
            this.AcceptButton = this.btnOK;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(954, 602);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_WorkOrder_Add_Actions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilities Work Order - Link Actions";
            this.Load += new System.EventHandler(this.frm_UT_WorkOrder_Add_Actions_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditDateTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07275UTWorkOrderActionsAvailableForAddingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.DateEdit dateEditDateTo;
        private DevExpress.XtraEditors.DateEdit dateEditDateFrom;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp07275UTWorkOrderActionsAvailableForAddingBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderSortOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesList;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
    }
}

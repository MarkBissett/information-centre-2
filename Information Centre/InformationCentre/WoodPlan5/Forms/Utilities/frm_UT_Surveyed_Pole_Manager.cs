using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Surveyed_Pole_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInSurveyedPoleIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        private string i_str_selected_client_ids = "";
        private string i_str_selected_client_names = "";
        private string i_str_selected_region_ids = "";
        private string i_str_selected_region_names = "";
        private string i_str_selected_subarea_ids = "";
        private string i_str_selected_subarea_names = "";
        private string i_str_selected_primary_ids = "";
        private string i_str_selected_primary_names = "";
        private string i_str_selected_feeder_ids = "";
        private string i_str_selected_feeder_names = "";
        private string i_str_selected_circuit_ids = "";
        private string i_str_selected_circuit_names = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;
        BaseObjects.GridCheckMarksSelection selection5;
        BaseObjects.GridCheckMarksSelection selection6;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_UT_Surveyed_Pole_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Surveyed_Pole_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10022;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp07001_UT_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07001_UT_Client_FilterTableAdapter.Fill(dataSet_UT.sp07001_UT_Client_Filter);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl5.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl6.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl7.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl8.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection6 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection6.CheckMarkColumn.VisibleIndex = 0;
            selection6.CheckMarkColumn.Width = 30;

            popupContainerControlClients.Size = new System.Drawing.Size(312, 423);
            popupContainerControlRegions.Size = new System.Drawing.Size(312, 423);
            popupContainerControlSubAreas.Size = new System.Drawing.Size(312, 423);
            popupContainerControlPrimarys.Size = new System.Drawing.Size(312, 423);
            popupContainerControlFeeders.Size = new System.Drawing.Size(312, 423);
            popupContainerControlCircuits.Size = new System.Drawing.Size(512, 523);
            
            sp07336_UT_Survey_Pole_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SurveyedPoleID");

            if (strPassedInSurveyedPoleIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit1.EditValue = "Custom Filter";
                popupContainerEdit2.EditValue = "Custom Filter";
                popupContainerEdit3.EditValue = "Custom Filter";
                popupContainerEdit4.EditValue = "Custom Filter";
                popupContainerEdit5.EditValue = "Custom Filter";
                popupContainerEdit6.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInSurveyedPoleIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_UT_Surveyed_Pole_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Surveyed_Pole_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInSurveyedPoleIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RegionFilter", i_str_selected_region_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SubAreaFilter", i_str_selected_subarea_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PrimaryFilter", i_str_selected_primary_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FeederFilter", i_str_selected_feeder_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CircuitFilter", i_str_selected_circuit_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Client Filter //
                int intFoundRow = 0;
                string strDistrictFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strDistrictFilter))
                {
                    Array arrayDistricts = strDistrictFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewClients = (GridView)gridControl2.MainView;
                    viewClients.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayDistricts)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewClients.LocateByValue(0, viewClients.Columns["ClientID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewClients.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewClients.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewClients.EndUpdate();
                    popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                    Load_Region_Filter();
                }

                // Region Filter //
                string strRegionFilter = default_screen_settings.RetrieveSetting("RegionFilter");
                if (!string.IsNullOrEmpty(strRegionFilter))
                {
                    Array arrayRegions = strRegionFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewRegions = (GridView)gridControl4.MainView;
                    viewRegions.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRegions)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewRegions.LocateByValue(0, viewRegions.Columns["RegionID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewRegions.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewRegions.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewRegions.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit2_Get_Selected();
                    Load_SubArea_Filter();
                }

                // SubArea Filter //
                string strSubAreaFilter = default_screen_settings.RetrieveSetting("SubAreaFilter");
                if (!string.IsNullOrEmpty(strSubAreaFilter))
                {
                    Array arraySubAreas = strSubAreaFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewSubAreas = (GridView)gridControl5.MainView;
                    viewSubAreas.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arraySubAreas)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewSubAreas.LocateByValue(0, viewSubAreas.Columns["SubAreaID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewSubAreas.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewSubAreas.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewSubAreas.EndUpdate();
                    popupContainerEdit3.EditValue = PopupContainerEdit3_Get_Selected();
                    Load_Primary_Filter();
                }

                // Primary Filter //
                string strPrimaryFilter = default_screen_settings.RetrieveSetting("PrimaryFilter");
                if (!string.IsNullOrEmpty(strPrimaryFilter))
                {
                    Array arrayPrimarys = strPrimaryFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewPrimarys = (GridView)gridControl6.MainView;
                    viewPrimarys.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayPrimarys)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewPrimarys.LocateByValue(0, viewPrimarys.Columns["PrimaryID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewPrimarys.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewPrimarys.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewPrimarys.EndUpdate();
                    popupContainerEdit4.EditValue = PopupContainerEdit4_Get_Selected();
                    Load_Feeder_Filter();
                }

                // Feeder Filter //
                string strFeederFilter = default_screen_settings.RetrieveSetting("FeederFilter");
                if (!string.IsNullOrEmpty(strFeederFilter))
                {
                    Array arrayFeeders = strFeederFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFeeders = (GridView)gridControl7.MainView;
                    viewFeeders.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayFeeders)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFeeders.LocateByValue(0, viewFeeders.Columns["FeederID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFeeders.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFeeders.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFeeders.EndUpdate();
                    popupContainerEdit5.EditValue = PopupContainerEdit5_Get_Selected();
                }
                bbiRefresh.PerformClick();

                // Feeder Filter //
                string strCircuitFilter = default_screen_settings.RetrieveSetting("CircuitFilter");
                if (!string.IsNullOrEmpty(strCircuitFilter))
                {
                    Array arrayCircuits = strCircuitFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewCircuits = (GridView)gridControl8.MainView;
                    viewCircuits.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayCircuits)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewCircuits.LocateByValue(0, viewCircuits.Columns["CircuitID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewCircuits.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewCircuits.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewCircuits.EndUpdate();
                    popupContainerEdit6.EditValue = PopupContainerEdit6_Get_Selected();
                }
                bbiRefreshPoles.PerformClick();

            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
 
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length == 1 ? true : false);
 
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_circuit_ids == null) i_str_selected_circuit_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerEdit6.EditValue.ToString() == "Custom Filter" && strPassedInSurveyedPoleIDs != "")  // Load passed in Trees //
            {
                sp07336_UT_Survey_Pole_ManagerTableAdapter.Fill(this.dataSet_UT.sp07336_UT_Survey_Pole_Manager, "", strPassedInSurveyedPoleIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp07336_UT_Survey_Pole_ManagerTableAdapter.Fill(this.dataSet_UT.sp07336_UT_Survey_Pole_Manager, i_str_selected_circuit_ids, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strPoleIDs = "";
                        int intSurveyPoleID = 0;
                        int intSurveyID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strPoleIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                            intSurveyPoleID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyedPoleID"));
                            intSurveyID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyID"));
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Pole2 fChildForm = new frm_UT_Survey_Pole2();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.strRecordIDs = strPoleIDs;
                        fChildForm.intPassedInSurveyPoleID = intSurveyPoleID;
                        fChildForm._SurveyID = intSurveyID;
                        fChildForm.strFormMode = "edit";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Surveyed Poles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Surveyed Pole" : Convert.ToString(intRowHandles.Length) + " Surveyed Poles") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Surveyed Pole" : "these Surveyed Poles") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyedPoleID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("surveyed_pole", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Poles //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one pole to view surveyed results before proceeding then try again.", "View Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strPoleIDs = "";
                        int intSurveyPoleID = 0;
                        int intSurveyID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strPoleIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                            intSurveyPoleID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyedPoleID"));
                            intSurveyID += Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyID"));
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Pole2 fChildForm = new frm_UT_Survey_Pole2();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.strRecordIDs = strPoleIDs;
                        fChildForm.intPassedInSurveyPoleID = intSurveyPoleID;
                        fChildForm._SurveyID = intSurveyID;
                        fChildForm.strFormMode = "view";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }


        #region Client Filter Panel

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalClientIDs = i_str_selected_client_ids;
            e.Value = PopupContainerEdit1_Get_Selected();
            if (strOriginalClientIDs != i_str_selected_client_ids)  // Only load if a the parent level selection changed //
            {
                Load_Region_Filter();
                Load_SubArea_Filter();
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_client_ids = "";    // Reset any prior values first //
            i_str_selected_client_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_client_ids += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_client_names = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_client_names += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_client_names) ? "No Client Filter" : i_str_selected_client_names);
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            if (i_str_selected_region_ids == null) i_str_selected_region_ids = "";
            if (i_str_selected_subarea_ids == null) i_str_selected_subarea_ids = "";
            if (i_str_selected_primary_ids == null) i_str_selected_primary_ids = "";
            if (i_str_selected_feeder_ids == null) i_str_selected_feeder_ids = "";
            GridView view = (GridView)gridControl8.MainView;
            view.BeginUpdate();
            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Fill(this.dataSet_UT.sp07040_UT_Circuit_Popup_Filtered_By_Various, i_str_selected_client_ids, i_str_selected_region_ids, i_str_selected_subarea_ids, i_str_selected_primary_ids, i_str_selected_feeder_ids);
            view.EndUpdate();
            Clear_Circuit_Filter();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        #endregion


        #region Region Filter Panel

        private void btnRegionFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Regions - Select one or more Clients from the Client Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalRegionIDs = i_str_selected_region_ids;
            e.Value = PopupContainerEdit2_Get_Selected();
            if (strOriginalRegionIDs != i_str_selected_region_ids)
            {
                Load_SubArea_Filter();  // Only load if a the parent level selection changed //
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_region_ids = "";    // Reset any prior values first //
            i_str_selected_region_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_region_ids += Convert.ToString(view.GetRowCellValue(i, "RegionID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_region_names = Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_region_names += ", " + Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_region_names) ? "No Region Filter" : i_str_selected_region_names);
        }

        #endregion


        #region Sub-Area Filter Panel

        private void btnSubAreaFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Sub-Areas - Select one or more Regions from the Region Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalSubAreaIDs = i_str_selected_subarea_ids;
            e.Value = PopupContainerEdit3_Get_Selected();
            if (strOriginalSubAreaIDs != i_str_selected_subarea_ids)
            {
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_subarea_ids = "";    // Reset any prior values first //
            i_str_selected_subarea_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_subarea_ids += Convert.ToString(view.GetRowCellValue(i, "SubAreaID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_subarea_names = Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_subarea_names += ", " + Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_subarea_names) ? "No Sub-Area Filter" : i_str_selected_subarea_names);
        }

        #endregion


        #region Primary Filter Panel

        private void btnPrimaryFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView6_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Primarys - Select one or more Sub-Areas from the Sub-Area Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit4_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalPrimaryIDs = i_str_selected_primary_ids;
            e.Value = PopupContainerEdit4_Get_Selected();
            if (strOriginalPrimaryIDs != i_str_selected_primary_ids)
            {
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit4_Get_Selected()
        {
            i_str_selected_primary_ids = "";    // Reset any prior values first //
            i_str_selected_primary_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl6.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_primary_ids += Convert.ToString(view.GetRowCellValue(i, "PrimaryID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_primary_names = Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_primary_names += ", " + Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_primary_names) ? "No Primary Filter" : i_str_selected_primary_names);
        }

        #endregion


        #region Feeder Filter Panel

        private void btnFeederFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView7_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Feeders - Select one or more Primarys from the Primary Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit5_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit5_Get_Selected();
        }

        private string PopupContainerEdit5_Get_Selected()
        {
            i_str_selected_feeder_ids = "";    // Reset any prior values first //
            i_str_selected_feeder_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_feeder_ids += Convert.ToString(view.GetRowCellValue(i, "FeederID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_feeder_names = Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_feeder_names += ", " + Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_feeder_names) ? "No Feeder Filter" : i_str_selected_feeder_names);
        }

        #endregion


        #region Circuit Filter Panel

        private void btnCircuitFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView8_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Circuits - Select any pre-filters from the other filter lists first then click Refresh Circuits", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit6_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit6_Get_Selected();
        }

        private string PopupContainerEdit6_Get_Selected()
        {
            i_str_selected_circuit_ids = "";    // Reset any prior values first //
            i_str_selected_circuit_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";

            }
            else if (selection6.SelectedCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_circuit_ids += Convert.ToString(view.GetRowCellValue(i, "CircuitID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_circuit_names = Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_circuit_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_circuit_names) ? "No Circuit Filter" : i_str_selected_circuit_names);
        }

        #endregion


        #region GridView1
        
        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Poles - Click Refresh Poles button");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "SurveyedPoleID").ToString() + ",";

            DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strRecordIDs = GetSetting.sp07347_UT_Survey_Manager_Get_Revisit_Actions(strParentIDs).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_action_revisits");
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPoleCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RevisitCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPoleCount")) == 0) e.Cancel = true;
                    break;
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RevisitCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
           /* GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CircuitID")) + ',';
            }
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "locality");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }*/
        }

        #endregion


        private void Load_Region_Filter()
        {
            popupContainerEdit2.EditValue = "No Region Filter";
            gridControl4.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_client_ids))
            {
                dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client.Clear();
                selection2.ClearSelection();
             }
            else
            {
                sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Fill(dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client, i_str_selected_client_ids);
            }
            gridControl4.EndUpdate();
        }

        private void Load_SubArea_Filter()
        {
            popupContainerEdit3.EditValue = "No Sub-Area Filter";
            gridControl5.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_region_ids))
            {
                dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region.Clear();
                selection3.ClearSelection();
            }
            else
            {
                sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Fill(dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region, i_str_selected_region_ids);
            }
            gridControl5.EndUpdate();
        }

        private void Load_Primary_Filter()
        {
            popupContainerEdit4.EditValue = "No Primary Filter";
            gridControl6.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_subarea_ids))
            {
                dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea.Clear();
                selection4.ClearSelection();
            }
            else
            {
                sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Fill(dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea, i_str_selected_subarea_ids);
            }
            gridControl6.EndUpdate();
        }

        private void Load_Feeder_Filter()
        {
            popupContainerEdit5.EditValue = "No Feeder Filter";
            gridControl7.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_primary_ids))
            {
                dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary.Clear();
                selection5.ClearSelection();
            }
            else
            {
                sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Fill(dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary, i_str_selected_primary_ids);
            }
            gridControl7.EndUpdate();
        }

        private void Clear_Circuit_Filter()
        {
            popupContainerEdit6.EditValue = "No Circuit Filter";
            i_str_selected_circuit_ids = "";
            selection6.ClearSelection();
         }

        private void bbiRefreshPoles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }



 

    }
}


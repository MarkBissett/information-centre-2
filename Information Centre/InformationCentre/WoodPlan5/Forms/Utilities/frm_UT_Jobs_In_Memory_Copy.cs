using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Jobs_In_Memory_Copy : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _PassedInSurveyedPoleIDs = "";
        private string strSelectedActionIDs = "";

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_UT_Jobs_In_Memory_Copy()
        {
            InitializeComponent();
        }

        private void frm_UT_Jobs_In_Memory_Copy_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 50007800;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            
            sp07382_UT_Copy_jobs_Into_Memory_Actions_ListTableAdapter.Fill(dataSet_UT.sp07382_UT_Copy_jobs_Into_Memory_Actions_List, _PassedInSurveyedPoleIDs);
            GridView view = (GridView)gridControl1.MainView;
            //view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

            LoadData();
            gridControl1.ForceInitialize();

            gridControl1.BeginUpdate();
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            GridColumn gc = view.Columns["CheckMarkSelection"];
            gc.Fixed = FixedStyle.Left;
            view.ExpandAllGroups();
            selection1.SelectAll();
            gridControl1.EndUpdate();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07382_UT_Copy_jobs_Into_Memory_Actions_ListTableAdapter.Fill(this.dataSet_UT.sp07382_UT_Copy_jobs_Into_Memory_Actions_List, _PassedInSurveyedPoleIDs);
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Jobs Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

       private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

            // Need following to handle expand and contract groups since grid's FocusedRowChanged event is interfering with it due to no selection of groups //
            GridHitInfo hi = view.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.HitTest == GridHitTest.RowGroupButton)
            {
                if (view.GetRowExpanded(hi.RowHandle))
                {
                    view.CollapseGroupRow(hi.RowHandle);
                }
                else
                {
                    view.ExpandGroupRow(hi.RowHandle);
                }
                DXMouseEventArgs.GetMouseArgs(e).Handled = true;
            }
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedActionIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Action records to copy into Memory by ticking them before proceeding.", "Copy Actions into Memeory", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GlobalSettings.CopiedUtilityJobIDs = strSelectedActionIDs;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            strSelectedActionIDs = "";  // Reset any prior values first //
            if (view.DataRowCount <= 0)
            {
                strSelectedActionIDs = "";
                return;

            }
            else if (selection1.SelectedCount <= 0)
            {
                strSelectedActionIDs = "";
                return;
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedActionIDs += Convert.ToString(view.GetRowCellValue(i, "ActionID")) + ",";
                    }
                }
            }
            return;
        }

    }
}


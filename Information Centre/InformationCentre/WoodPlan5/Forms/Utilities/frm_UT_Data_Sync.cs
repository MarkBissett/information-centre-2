﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Collections;

using System.Net;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Utilities_WebService;

namespace WoodPlan5
{
    public partial class frm_UT_Data_Sync : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        string _MachineID = "";
        string _SystemDataTransferMode = "";
        string _LastRetrievalOn = "";
        DateTime _LastRetrievalDateTime = new DateTime();
        DateTime _LastSentDateTime = new DateTime();
        private string strPermissionDocumentPath = "";
        private string strSignaturePath = "";
        private string strMapPath = "";
        private string strPicturePath = "";
        private string strReferenceFilePath = "";
        private string strWorkOrderPath = "";
 
        Timer timer;

        #endregion

        public frm_UT_Data_Sync()
        {
            InitializeComponent();
        }

        private void frm_UT_Data_Sync_Load(object sender, EventArgs e)
        {
            splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.FormID = 10016;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);

            // Timer used to lock import for 5 minutes to stop crash on subsequent runs //
            timer = new Timer() { Interval = 10000 };  // 10000 = 10 seconds //
            timer.Tick += TimerEventProcessor;

            try
            {
                _MachineID = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "DataSynchronisationMachineID").ToString();
            }
            catch (Exception)
            {
            }
            if (string.IsNullOrEmpty(_MachineID))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Machine ID has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Machine ID is set in System Settings table manually - contact Technical Support for further info.", "Get Machine ID - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                _SystemDataTransferMode = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "SystemDataTransferMode").ToString().ToLower();
            }
            catch (Exception)
            {
            }
            if (string.IsNullOrEmpty(_SystemDataTransferMode))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The System Data Transfer Mode has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe System Data Transfer Mode is set in System Settings table manually - contact Technical Support for further info.", "Get System Data Transfer Mode - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }      

            try
            {
                strPermissionDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strPermissionDocumentPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Permission Document path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strSignaturePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Signature Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Signature Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                strMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strMapPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Map Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Map Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                strPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strPicturePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Picture Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Picture Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
 
            try
            {
                strReferenceFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedReferenceFiles").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Reference Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strReferenceFilePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Reference Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Reference Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                strWorkOrderPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Work Order Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strWorkOrderPath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order Files path has not been set for this computer - unable to proceed with Data Synchronisation.\n\nThe Work Order Files path is set in the System Configuration Screen - contact Technical Support for further info.", "Get Default Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            try
            {
                _LastRetrievalOn = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "DataSynchronisationLastRetrieved").ToString();
                _LastRetrievalOn = (string.IsNullOrEmpty(_LastRetrievalOn) ? "None" : String.Format("{0:dd/MM/yyyy HH:mm:ss}", _LastRetrievalOn));
                _LastRetrievalDateTime = (_LastRetrievalOn == "None" ? DateTime.MinValue : Convert.ToDateTime(_LastRetrievalOn));
                if (_LastRetrievalDateTime == DateTime.MinValue) _LastRetrievalDateTime = new DateTime(1900, 1, 1);
            }
            catch (Exception)
            {
            }
            _LastSentDateTime = _LastRetrievalDateTime.AddSeconds(10);  // Make sure we don't pick up anything unless it was after last import (10 seconds is a safe margin) //

            labelLastImportDate.Text = "        Last Successfull Import: " + _LastRetrievalOn;

            sp07502_UT_WebService_Tablet_Get_Export_OverviewTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07502_UT_WebService_Tablet_Get_Export_OverviewTableAdapter.Fill(dataSet_UT_DataTransfer.sp07502_UT_WebService_Tablet_Get_Export_Overview, _LastSentDateTime);
            gridControl1.ForceInitialize();

            // REMEMBER TO UNCOMMENT OUT THE FOLLOWING //
            if (this.GlobalSettings.SystemDataTransferMode.ToLower() == "desktop")
            {
                splashScreenManager.CloseWaitForm(); 
                DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process can only be run when the software is in Tablet Mode. The software on this computer is running in Server Mode - unable to proceed with Data Synchronisation.", "Check Software Mode - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            SetExportImportStatus(); 
            PostOpen();
        }

        private void PostOpen()
        {
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            Application.DoEvents();  // Allow Form time to repaint itself //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_UT_Data_Sync_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Data to Export.", "Export Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                CleanUp();
                return;
            }

            if (!GetInternetConnectionStatus()) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - please try later or move to an area with a stronger signal.", "Check Internet Connection - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            ShowProgressBar(progressBarControlExport, view.DataRowCount);

            string strTableName = "";
            string strStoredProcNameGet = "";
            string strStoredProcNameSet = "";
            string strReturn = "";
            SqlConnection SQlConn;

            DataSet_UT_DataTransferTableAdapters.QueriesTableAdapter UpdateMobileDeviceChangeLog = new DataSet_UT_DataTransferTableAdapters.QueriesTableAdapter();
            UpdateMobileDeviceChangeLog.ChangeConnectionString(strConnectionString);


            // Get Version Number for this database //
            int intCurrentVersionNumber = 0;
            try
            {
                intCurrentVersionNumber = Convert.ToInt32(UpdateMobileDeviceChangeLog.sp07701_UT_Get_Data_Transfer_Version_Number());
            }
            catch (Exception ex)
            {
            }
            if (intCurrentVersionNumber <= 0)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Error - Unable to retrieve database version number.\n\nYou may need to install an update for this application - contact Technical Support for further info.", "Get Version Number - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            UtilitiesSoapClient ws2 = new UtilitiesSoapClient();
            try  // Call Web Service and send data to export //
            {
                ws2.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                SOAPHeaderContent header2 = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

                // Check version number against central version number //
                int intCentralVersionNumber = ws2.GetVersionNumber(header2);  // Call Web Service to sync file //
                if (intCentralVersionNumber != intCurrentVersionNumber)
                {
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The version of this database does not match the version of the central server.\n\nYou will need to install an update to this application - contact Technical Support.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get the version number from the central server.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }


            DateTime dtCurrentStartTime = DateTime.Now;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                strTableName = view.GetRowCellValue(i, "RecordType").ToString();
                strStoredProcNameGet = view.GetRowCellValue(i, "StoredProcGet").ToString();
                strStoredProcNameSet = view.GetRowCellValue(i, "StoredProcSet").ToString();
                strReturn = "";  // Added 16/04/2014 //

                DataSet ds = new DataSet("NewDataSet");
                SqlDataAdapter sda = new SqlDataAdapter();
                SQlConn = new SqlConnection(strConnectionString);
                try  // Get Data To Export //
                {
                    SQlConn.Open();
                    SqlCommand cmd = new SqlCommand(strStoredProcNameGet, SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DeviceID", _MachineID));
                    cmd.Parameters.Add(new SqlParameter("@TableName", strTableName));
                    ds.Clear();
                    sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds, "Table");
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show(String.Format("The Synchronisation Process failed trying to load {0} export data.\n\nError: {1}\n\nThis screen will now close. Try exporting again.", strTableName, ex.Message), "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                UtilitiesSoapClient ws = new UtilitiesSoapClient();
                try  // Call Web Service and send data to export //
                {
                    ws.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                    SOAPHeaderContent header = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };
                    if (strTableName == "UT_Survey_Picture" || strTableName == "UT_Linked_Map" || strTableName == "UT_Reference_File" || strTableName == "UT_Permission_Document" || strTableName == "UT_Risk_Assessment_Team_Member_Signature")
                    {
                        string strColumnName = "";
                        string strPath = "";
                        switch (strTableName)
                        {
                            case "UT_Survey_Picture":
                                strColumnName = "PicturePath";
                                strPath = strPicturePath;
                                break;
                            case "UT_Linked_Map":
                                strColumnName = "DocumentPath";
                                strPath = strMapPath;
                                break;
                            case "UT_Reference_File":
                                strColumnName = "DocumentPath";
                                strPath = strReferenceFilePath;
                                break;
                            case "UT_Permission_Document":
                                strColumnName = "PDFFile";
                                strPath = strPermissionDocumentPath;
                                break;
                            case "UT_Work_Order":
                                strColumnName = "WorkOrderPDF";
                                strPath = strWorkOrderPath;
                                break;
                            case "UT_Risk_Assessment_Team_Member_Signature":
                                strColumnName = "SignatureFile";
                                strPath = strSignaturePath;
                                break;
                            default:
                                break;
                        }
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string strFileName = dr[strColumnName].ToString();
                            if (string.IsNullOrEmpty(strFileName)) continue;

                            if (strTableName == "UT_Linked_Map") strFileName += ".gif";  // Linked Maps don't have a file extension, so add it.

                            if (!File.Exists((Path.Combine(strPath, strFileName)))) continue;
                            FileInfo fInfo = new FileInfo(Path.Combine(strPath, strFileName));  // Get the file information form the selected file //

                            // Get the length of the file to see if it is possible to upload it (with the standard 4 MB limit) //
                            long numBytes = fInfo.Length;
                            double dLen = Convert.ToDouble(fInfo.Length / 1000000);

                            // Default limit of 4 MB on web server. Have to change the web.config to allow larger uploads //
                            if (dLen < 4)
                            {
                                // Set up a file stream and binary reader for the selected file //                          
                                FileStream fStream = new FileStream(Path.Combine(strPath, strFileName), FileMode.Open, FileAccess.Read);
                                BinaryReader br = new BinaryReader(fStream);
                                byte[] data = br.ReadBytes((int)numBytes);  // Convert the file to a byte array //
                                br.Close();
                                br.Dispose();

                                // pass the byte array (file) and file name to the web service
                                strReturn = ws.FileSet(header, data, strFileName, strTableName);  // Call Web Service to sync file //
                                fStream.Close();
                                fStream.Dispose();
                            }

                            if (strTableName == "UT_Linked_Map")  // Attempt to upload the associated thumbnail image //
                            {
                                strFileName = dr[strColumnName].ToString() + "_thumb.jpg";
                                if (!File.Exists((Path.Combine(strPath, strFileName)))) continue;
                                fInfo = new FileInfo(Path.Combine(strPath, strFileName));  // Get the file information form the selected file //

                                // Get the length of the file to see if it is possible to upload it (with the standard 4 MB limit) //
                                numBytes = fInfo.Length;
                                dLen = Convert.ToDouble(fInfo.Length / 1000000);

                                // Default limit of 4 MB on web server. Have to change the web.config to allow larger uploads //
                                if (dLen < 4)
                                {
                                    // Set up a file stream and binary reader for the selected file //                          
                                    FileStream fStream = new FileStream(Path.Combine(strPath, strFileName), FileMode.Open, FileAccess.Read);
                                    BinaryReader br = new BinaryReader(fStream);
                                    byte[] data = br.ReadBytes((int)numBytes);  // Convert the file to a byte array //
                                    br.Close();
                                    br.Dispose();

                                    // pass the byte array (file) and file name to the web service
                                    strReturn = ws.FileSet(header, data, strFileName, strTableName);  // Call Web Service to sync file //
                                    fStream.Close();
                                    fStream.Dispose();
                                }
                            }
                        }
                    }
                    strReturn = ws.SetData(header, _MachineID, strTableName, strStoredProcNameSet, ds);  // Call Web Service to sync data //
                    if (!strReturn.ToLower().StartsWith("success"))  // Web Service was unsuccessfull //
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        sda.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + strReturn + "\n\nThis screen will now close. Try exporting again. If the problem persists contact Technical Support.", "Export Data - Failed on Clear Change Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    ws.Close();
                    ws = null;
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + ex.Message + "\n\nThe Web Service may not be running.\n\nThis screen will now close. Try exporting again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                ws.Close();
                ws = null;
                if (!strReturn.ToLower().StartsWith("success"))  // Web Service was unsuccessfull //
                {
                    if (string.IsNullOrEmpty(strReturn))
                    {
                        strReturn = "A problem occurred - the web service may not be running.";
                    }
                    SQlConn.Close();
                    SQlConn.Dispose();
                    sda.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to export " + strTableName + " data.\n\nError: " + strReturn + "\n\nThis screen will now close. Try exporting again. If the problem persists contact Technical Support.", "Export Data - Failed on Clear Change Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                else  // Web service was successfull so remove records just exported from MobileDeviceChangeLog //
                {
                    try
                    {
                        strReturn = UpdateMobileDeviceChangeLog.sp07503_UT_WebService_Tablet_Remove_Exported_From_Change_Log(strTableName, dtCurrentStartTime).ToString();
                    }
                    catch (Exception ex)
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        sda.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to remove the exported " + strTableName + " data from the Change Log.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                SQlConn.Close();
                SQlConn.Dispose();
                sda.Dispose();

                progressBarControlExport.PerformStep();
                progressBarControlExport.Update();
                Application.DoEvents();   // Allow Form time to repaint itself //
            }
            _LastSentDateTime = DateTime.Now;
            CleanUp();
            DevExpress.XtraEditors.XtraMessageBox.Show("The Export Synchronisation Process completed Successfully.", "Export Data - Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (!GetInternetConnectionStatus()) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - please try later or move to an area with a stronger signal.", "Check Internet Connection - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get Version Number for this database //
            int intCurrentVersionNumber = 0;
            try
            {
                DataSet_UT_DataTransferTableAdapters.QueriesTableAdapter UpdateMobileDeviceChangeLog = new DataSet_UT_DataTransferTableAdapters.QueriesTableAdapter();
                UpdateMobileDeviceChangeLog.ChangeConnectionString(strConnectionString);
                intCurrentVersionNumber = Convert.ToInt32(UpdateMobileDeviceChangeLog.sp07701_UT_Get_Data_Transfer_Version_Number());
            }
            catch (Exception ex)
            {
            }
            if (intCurrentVersionNumber <= 0)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Error - Unable to retrieve database version number.\n\nYou may need to install an update for this application - contact Technical Support for further info.", "Get Version Number - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }
            UtilitiesSoapClient ws2 = new UtilitiesSoapClient();
            try  // Call Web Service and send data to export //
            {
                ws2.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //

                SOAPHeaderContent header2 = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

                // Check version number against central version number //
                int intCentralVersionNumber = ws2.GetVersionNumber(header2);  // Call Web Service to sync file //
                if (intCentralVersionNumber != intCurrentVersionNumber)
                {
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The version of this database does not match the version of the central server.\n\nYou will need to install an update to this application - contact Technical Support.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            catch (Exception ex)
            {
                CleanUp();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to get the version number from the central server.\n\nError: " + ex.Message + "\n\nThis screen will now close. Try exporting again.", "Export Data - Failed on Version Check", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }

             
            ArrayList arrayListTableNames = new ArrayList();
            arrayListTableNames.Add("EP_Client");
            arrayListTableNames.Add("UT_Region");
            arrayListTableNames.Add("tblPickListHeader");
            arrayListTableNames.Add("tblPickListItem");
            arrayListTableNames.Add("tblSequence");
            arrayListTableNames.Add("tblSpecies");
            arrayListTableNames.Add("tblStaff");
            arrayListTableNames.Add("UT_Sub_Area");
            arrayListTableNames.Add("UT_Primary");
            arrayListTableNames.Add("UT_Feeder");
            arrayListTableNames.Add("UT_Circuit");
            arrayListTableNames.Add("UT_Feeder_Contract");
            arrayListTableNames.Add("UT_Pole");
            arrayListTableNames.Add("UT_Pole_Access_Issue");
            arrayListTableNames.Add("UT_Pole_Electrical_Hazard");
            arrayListTableNames.Add("UT_Pole_Environmental_Issue");
            arrayListTableNames.Add("UT_Pole_Site_Hazard");
            arrayListTableNames.Add("UT_Tree");
            arrayListTableNames.Add("UT_Tree_Species");
            arrayListTableNames.Add("UT_Unit");
            arrayListTableNames.Add("UT_Costing_Type");
            arrayListTableNames.Add("UT_Costing_Item_Master");
            arrayListTableNames.Add("UT_Master_Equipment");
            arrayListTableNames.Add("UT_Master_Material");
            arrayListTableNames.Add("UT_Master_Tree_Defect");
            arrayListTableNames.Add("tblJobMaster");
            arrayListTableNames.Add("UT_Default_Required_Equipment");
            arrayListTableNames.Add("UT_Default_Required_Material");
            arrayListTableNames.Add("UT_Survey_Status");
            arrayListTableNames.Add("UT_Survey");
            arrayListTableNames.Add("UT_Shutdown");
            arrayListTableNames.Add("UT_Surveyed_Pole");
            arrayListTableNames.Add("UT_Surveyed_Tree");
            arrayListTableNames.Add("UT_Surveyed_Tree_Defect");
            arrayListTableNames.Add("tblContractor");
            arrayListTableNames.Add("UT_Work_Order");
            arrayListTableNames.Add("UT_Self_Billing_Invoice");
            arrayListTableNames.Add("UT_Completion_Sheet");
            arrayListTableNames.Add("UT_Action");
            arrayListTableNames.Add("UT_Action_Equipment");
            arrayListTableNames.Add("UT_Action_Material");
            arrayListTableNames.Add("UT_Permission_Document");
            arrayListTableNames.Add("UT_Action_Permission");
            arrayListTableNames.Add("UT_Survey_Costing_Header");
            arrayListTableNames.Add("UT_Survey_Costing_Item");
            arrayListTableNames.Add("UT_Survey_Costing_Pole");
            arrayListTableNames.Add("UT_Survey_Picture");
            arrayListTableNames.Add("UT_Linked_Map");
            arrayListTableNames.Add("UT_Reference_File");
            arrayListTableNames.Add("UT_Master_Completion_Question");
            arrayListTableNames.Add("UT_Risk_Assessment");
            arrayListTableNames.Add("UT_Master_Risk_Question");
            arrayListTableNames.Add("UT_Risk_Assessment_Question");
            arrayListTableNames.Add("UT_Master_Answer");
            arrayListTableNames.Add("UT_Master_Question_Answer");
            arrayListTableNames.Add("UT_Work_Order_Team");
            arrayListTableNames.Add("GC_SubContractor_Team_Member");
            arrayListTableNames.Add("UT_Work_Order_Team_Member");
            arrayListTableNames.Add("UT_Completion_Sheet_Question");
            arrayListTableNames.Add("UT_Risk_Assessment_Team_Member_Signature");
            arrayListTableNames.Add("UT_Surveyed_Pole_TM");

            ArrayList arrayListTableFriendlyNames = new ArrayList();
            arrayListTableFriendlyNames.Add("Client");
            arrayListTableFriendlyNames.Add("Region");
            arrayListTableFriendlyNames.Add("PickList Header");
            arrayListTableFriendlyNames.Add("PickList Item");
            arrayListTableFriendlyNames.Add("Sequence");
            arrayListTableFriendlyNames.Add("Species");
            arrayListTableFriendlyNames.Add("Staff");
            arrayListTableFriendlyNames.Add("Sub-Area");
            arrayListTableFriendlyNames.Add("Primary");
            arrayListTableFriendlyNames.Add("Feeder");
            arrayListTableFriendlyNames.Add("Circuit");
            arrayListTableFriendlyNames.Add("Feeder Contract");
            arrayListTableFriendlyNames.Add("Pole");
            arrayListTableFriendlyNames.Add("Pole Access Issue");
            arrayListTableFriendlyNames.Add("Pole Electrical Hazard");
            arrayListTableFriendlyNames.Add("Pole Environmental Issue");
            arrayListTableFriendlyNames.Add("Pole Site Hazard");
            arrayListTableFriendlyNames.Add("Tree");
            arrayListTableFriendlyNames.Add("Tree Species");
            arrayListTableFriendlyNames.Add("Unit Descriptors");
            arrayListTableFriendlyNames.Add("Costing Type");
            arrayListTableFriendlyNames.Add("Costing Item Master");
            arrayListTableFriendlyNames.Add("Master Equipment");
            arrayListTableFriendlyNames.Add("Master Material");
            arrayListTableFriendlyNames.Add("Master Tree Defect");
            arrayListTableFriendlyNames.Add("Master Job");
            arrayListTableFriendlyNames.Add("Default Required Equipment");
            arrayListTableFriendlyNames.Add("Default Required Material");
            arrayListTableFriendlyNames.Add("Survey Status");
            arrayListTableFriendlyNames.Add("Survey");
            arrayListTableFriendlyNames.Add("Shutdown");
            arrayListTableFriendlyNames.Add("Surveyed Pole");
            arrayListTableFriendlyNames.Add("Surveyed Tree");
            arrayListTableFriendlyNames.Add("Surveyed Tree Defect");
            arrayListTableFriendlyNames.Add("Contractor");
            arrayListTableFriendlyNames.Add("Work Order");
            arrayListTableFriendlyNames.Add("Self Billing Invoice");
            arrayListTableFriendlyNames.Add("Completion Sheet");
            arrayListTableFriendlyNames.Add("Surveyed Tree Action");
            arrayListTableFriendlyNames.Add("Action Equipment");
            arrayListTableFriendlyNames.Add("Action Material");
            arrayListTableFriendlyNames.Add("Permission Document");
            arrayListTableFriendlyNames.Add("Action Permission");
            arrayListTableFriendlyNames.Add("Survey Costing Header");
            arrayListTableFriendlyNames.Add("Survey Costing Item");
            arrayListTableFriendlyNames.Add("Survey Costing Pole");
            arrayListTableFriendlyNames.Add("Picture");
            arrayListTableFriendlyNames.Add("Linked Map");
            arrayListTableFriendlyNames.Add("Reference File");
            arrayListTableFriendlyNames.Add("Master Completion Question");
            arrayListTableFriendlyNames.Add("Risk Assessment");
            arrayListTableFriendlyNames.Add("Master Risk Question");
            arrayListTableFriendlyNames.Add("Risk Assessment Question");
            arrayListTableFriendlyNames.Add("Master Answer");
            arrayListTableFriendlyNames.Add("Master Question Answer");       
            arrayListTableFriendlyNames.Add("Work Order Team");
            arrayListTableFriendlyNames.Add("SubContractor Team Member");
            arrayListTableFriendlyNames.Add("Work Order Team Member");
            arrayListTableFriendlyNames.Add("Completion Sheet Question");
            arrayListTableFriendlyNames.Add("Risk Assessment Team Member Signature");
            arrayListTableFriendlyNames.Add("Surveyed Pole Traffic Management");
      
            ArrayList arrayListStoredProcNamesGet = new ArrayList();
            arrayListStoredProcNamesGet.Add("sp07500_UT_WebService_Clients_Get");
            arrayListStoredProcNamesGet.Add("sp07505_UT_WebService_Regions_Get");
            arrayListStoredProcNamesGet.Add("sp07507_tblPickListHeader_Get");
            arrayListStoredProcNamesGet.Add("sp07509_tblPickListItem_Get");
            arrayListStoredProcNamesGet.Add("sp07511_tblSequence_Get");
            arrayListStoredProcNamesGet.Add("sp07513_tblSpecies_Get");
            arrayListStoredProcNamesGet.Add("sp07515_tblStaff_Get");
            arrayListStoredProcNamesGet.Add("sp07517_UT_WebService_Sub_Areas_Get");
            arrayListStoredProcNamesGet.Add("sp07519_UT_WebService_Primarys_Get");
            arrayListStoredProcNamesGet.Add("sp07521_UT_WebService_Feeders_Get");
            arrayListStoredProcNamesGet.Add("sp07523_UT_WebService_Circuits_Get");
            arrayListStoredProcNamesGet.Add("sp07525_UT_WebService_Feeder_Contracts_Get");
            arrayListStoredProcNamesGet.Add("sp07529_UT_WebService_Poles_Get");
            arrayListStoredProcNamesGet.Add("sp07531_UT_WebService_Pole_Access_Issues_Get");
            arrayListStoredProcNamesGet.Add("sp07533_UT_WebService_Pole_Electrical_Hazards_Get");
            arrayListStoredProcNamesGet.Add("sp07535_UT_WebService_Pole_Environmental_Issue_Get");
            arrayListStoredProcNamesGet.Add("sp07537_UT_WebService_Pole_Site_Hazard_Get");
            arrayListStoredProcNamesGet.Add("sp07541_UT_WebService_Tree_Get");
            arrayListStoredProcNamesGet.Add("sp07543_UT_WebService_Tree_Species_Get");
            arrayListStoredProcNamesGet.Add("sp07545_UT_WebService_Unit_Get");
            arrayListStoredProcNamesGet.Add("sp07547_UT_WebService_Costing_Type_Get");
            arrayListStoredProcNamesGet.Add("sp07549_UT_WebService_Costing_Item_Master_Get");
            arrayListStoredProcNamesGet.Add("sp07551_UT_WebService_Master_Equipment_Get");
            arrayListStoredProcNamesGet.Add("sp07553_UT_WebService_Master_Material_Get");
            arrayListStoredProcNamesGet.Add("sp07555_UT_WebService_Master_Tree_Defect_Get");
            arrayListStoredProcNamesGet.Add("sp07557_UT_WebService_tblJobMaster_Get");
            arrayListStoredProcNamesGet.Add("sp07559_UT_WebService_Default_Required_Equipment_Get");
            arrayListStoredProcNamesGet.Add("sp07561_UT_WebService_Default_Required_Material_Get");
            arrayListStoredProcNamesGet.Add("sp07563_UT_WebService_Survey_Status_Get");
            arrayListStoredProcNamesGet.Add("sp07565_UT_WebService_Survey_Get");
            arrayListStoredProcNamesGet.Add("sp07627_UT_WebService_Shutdown_Get");
            arrayListStoredProcNamesGet.Add("sp07567_UT_WebService_Surveyed_Pole_Get");
            arrayListStoredProcNamesGet.Add("sp07569_UT_WebService_Surveyed_Tree_Get");
            arrayListStoredProcNamesGet.Add("sp07571_UT_WebService_Surveyed_Tree_Defect_Get");
            arrayListStoredProcNamesGet.Add("sp07575_UT_WebService_tblContractor_Get");
            arrayListStoredProcNamesGet.Add("sp07577_UT_WebService_Work_Order_Get");
            arrayListStoredProcNamesGet.Add("sp07579_UT_WebService_Self_Billing_Invoice_Get");
            arrayListStoredProcNamesGet.Add("sp07615_UT_Completion_Sheet_Get");
            arrayListStoredProcNamesGet.Add("sp07573_UT_WebService_Action_Get");
            arrayListStoredProcNamesGet.Add("sp07581_UT_WebService_Action_Equipment_Get");
            arrayListStoredProcNamesGet.Add("sp07583_UT_WebService_Action_Material_Get");
            arrayListStoredProcNamesGet.Add("sp07589_UT_Permission_Document_Get");
            arrayListStoredProcNamesGet.Add("sp07587_UT_Action_Permission_Get");
            arrayListStoredProcNamesGet.Add("sp07591_UT_Survey_Costing_Header_Get");
            arrayListStoredProcNamesGet.Add("sp07593_UT_Survey_Costing_Item_Get");
            arrayListStoredProcNamesGet.Add("sp07595_UT_Survey_Costing_Pole_Get");
            arrayListStoredProcNamesGet.Add("sp07597_UT_Survey_Picture_Get");
            arrayListStoredProcNamesGet.Add("sp07599_UT_Linked_Map_Get");
            arrayListStoredProcNamesGet.Add("sp07601_UT_Reference_File_Get");
            arrayListStoredProcNamesGet.Add("sp07603_UT_Master_Completion_Question_Get");
            arrayListStoredProcNamesGet.Add("sp07607_UT_Risk_Assessment_Get");
            arrayListStoredProcNamesGet.Add("sp07611_UT_Master_Risk_Question_Get");
            arrayListStoredProcNamesGet.Add("sp07613_UT_Risk_Assessment_Question_Get");
            arrayListStoredProcNamesGet.Add("sp07629_UT_WebService_Master_Answer_Get");
            arrayListStoredProcNamesGet.Add("sp07631_UT_WebService_Master_Question_Answer_Get");          
            arrayListStoredProcNamesGet.Add("sp07617_UT_Work_Order_Team_Get");
            arrayListStoredProcNamesGet.Add("sp07621_UT_SubContractor_Team_Member_Get");
            arrayListStoredProcNamesGet.Add("sp07619_UT_Work_Order_Team_Member_Member_Get");
            arrayListStoredProcNamesGet.Add("sp07623_UT_Completion_Sheet_Question_Get");
            arrayListStoredProcNamesGet.Add("sp07609_UT_Risk_Assessment_TeamMember_Signature_Get");
            arrayListStoredProcNamesGet.Add("sp07625_UT_Surveyed_Pole_TM_Get");

            ArrayList arrayListStoredProcNamesSet = new ArrayList();
            arrayListStoredProcNamesSet.Add("sp07501_UT_WebService_Clients_Set");
            arrayListStoredProcNamesSet.Add("sp07506_UT_WebService_Regions_Set");
            arrayListStoredProcNamesSet.Add("sp07508_tblPickListHeader_Set");
            arrayListStoredProcNamesSet.Add("sp07510_tblPickListItem_Set");
            arrayListStoredProcNamesSet.Add("sp07512_tblSequence_Set");
            arrayListStoredProcNamesSet.Add("sp07514_tblSpecies_Set");
            arrayListStoredProcNamesSet.Add("sp07516_tblStaff_Set");
            arrayListStoredProcNamesSet.Add("sp07518_UT_WebService_Sub_Areas_Set");
            arrayListStoredProcNamesSet.Add("sp07520_UT_WebService_Primarys_Set");
            arrayListStoredProcNamesSet.Add("sp07522_UT_WebService_Feeders_Set");
            arrayListStoredProcNamesSet.Add("sp07524_UT_WebService_Circuits_Set");
            arrayListStoredProcNamesSet.Add("sp07526_UT_WebService_Feeder_Contracts_Set");
            arrayListStoredProcNamesSet.Add("sp07530_UT_WebService_Poles_Set");
            arrayListStoredProcNamesSet.Add("sp07532_UT_WebService_Pole_Access_Issues_Set");
            arrayListStoredProcNamesSet.Add("sp07534_UT_WebService_Pole_Electrical_Hazard_Set");
            arrayListStoredProcNamesSet.Add("sp07536_UT_WebService_Pole_Environmental_Issue_Set");
            arrayListStoredProcNamesSet.Add("sp07538_UT_WebService_Pole_Site_Hazard_Set");
            arrayListStoredProcNamesSet.Add("sp07542_UT_WebService_Tree_Set");
            arrayListStoredProcNamesSet.Add("sp07544_UT_WebService_Tree_Species_Set");
            arrayListStoredProcNamesSet.Add("sp07546_UT_WebService_Unit_Set");
            arrayListStoredProcNamesSet.Add("sp07548_UT_WebService_Costing_Type_Set");
            arrayListStoredProcNamesSet.Add("sp07550_UT_WebService_Costing_Item_Master_Set");
            arrayListStoredProcNamesSet.Add("sp07552_UT_WebService_Master_Equipment_Set");
            arrayListStoredProcNamesSet.Add("sp07554_UT_WebService_Master_Material_Set");
            arrayListStoredProcNamesSet.Add("sp07556_UT_WebService_Master_Tree_Defect_Set");
            arrayListStoredProcNamesSet.Add("sp07558_UT_WebService_tblJobMaster_Set");
            arrayListStoredProcNamesSet.Add("sp07560_UT_WebService_Default_Required_Equipment_Set");
            arrayListStoredProcNamesSet.Add("sp07562_UT_WebService_Default_Required_Material_Set");
            arrayListStoredProcNamesSet.Add("sp07564_UT_WebService_Survey_Status_Set");
            arrayListStoredProcNamesSet.Add("sp07566_UT_WebService_Survey_Set");
            arrayListStoredProcNamesSet.Add("sp07628_UT_WebService_Shutdown_Set");
            arrayListStoredProcNamesSet.Add("sp07568_UT_WebService_Surveyed_Pole_Set");
            arrayListStoredProcNamesSet.Add("sp07570_UT_WebService_Surveyed_Tree_Set");
            arrayListStoredProcNamesSet.Add("sp07572_UT_WebService_Surveyed_Tree_Defect_Set");
            arrayListStoredProcNamesSet.Add("sp07576_UT_WebService_tblContractor_Set");
            arrayListStoredProcNamesSet.Add("sp07578_UT_WebService_Work_Order_Set");
            arrayListStoredProcNamesSet.Add("sp07580_UT_WebService_Self_Billing_Invoice_Set");
            arrayListStoredProcNamesSet.Add("sp07616_UT_Completion_Sheet_Set");
            arrayListStoredProcNamesSet.Add("sp07574_UT_WebService_Action_Set");
            arrayListStoredProcNamesSet.Add("sp07582_UT_WebService_Action_Equipment_Set");
            arrayListStoredProcNamesSet.Add("sp07584_UT_WebService_Action_Material_Set");
            arrayListStoredProcNamesSet.Add("sp07590_UT_Permission_Document_Set");
            arrayListStoredProcNamesSet.Add("sp07588_UT_Action_Permission_Set");
            arrayListStoredProcNamesSet.Add("sp07592_UT_Survey_Costing_Header_Set");
            arrayListStoredProcNamesSet.Add("sp07594_UT_Survey_Costing_Item_Set");
            arrayListStoredProcNamesSet.Add("sp07596_UT_Survey_Costing_Pole_Set");
            arrayListStoredProcNamesSet.Add("sp07598_UT_Survey_Picture_Set");
            arrayListStoredProcNamesSet.Add("sp07600_UT_Linked_Map_Set");
            arrayListStoredProcNamesSet.Add("sp07602_UT_Reference_File_Set");
            arrayListStoredProcNamesSet.Add("sp07604_UT_Master_Completion_Question_Set");
            arrayListStoredProcNamesSet.Add("sp07608_UT_Risk_Assessment_Set");
            arrayListStoredProcNamesSet.Add("sp07612_UT_Master_Risk_Question_Set");
            arrayListStoredProcNamesSet.Add("sp07614_UT_Risk_Assessment_Question_Set");
            arrayListStoredProcNamesSet.Add("sp07630_UT_WebService_Master_Answer_Set");
            arrayListStoredProcNamesSet.Add("sp07632_UT_WebService_Master_Question_Answer_Set");        
            arrayListStoredProcNamesSet.Add("sp07618_UT_Work_Order_Team_Set");
            arrayListStoredProcNamesSet.Add("sp07622_UT_SubContractor_Team_Member_Set");
            arrayListStoredProcNamesSet.Add("sp07620_UT_Work_Order_Team_Member_Member_Set");
            arrayListStoredProcNamesSet.Add("sp07624_UT_Completion_Sheet_Question_Set");
            arrayListStoredProcNamesSet.Add("sp07610_UT_Risk_Assessment_TeamMember_Signature_Set");
            arrayListStoredProcNamesSet.Add("sp07626_UT_Surveyed_Pole_TM_Set");

            ShowProgressBar(progressBarControlImport, arrayListTableNames.Count);

            DataSet dsTemp = new DataSet();
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SQlConn.Open();

            UtilitiesSoapClient ws = new UtilitiesSoapClient();
            ws.InnerChannel.OperationTimeout = new TimeSpan(0, 10, 0);  // Set Timeout to 10 minutes //          

            SOAPHeaderContent header = new SOAPHeaderContent() { Password = "***Ch4r10tte***", UserName = "MarkBissett" };

            int intCounter = -1;  // Start on -1 as first iteration of loop will move it to 0. Critical that intCounter increments at top of loop as there are various exist point in loop //
            int intStoredProcReturnValue = 0;
            foreach (string TableName in arrayListTableNames)
            {
                intCounter++;
                progressBarControlImport.PerformStep();
                progressBarControlImport.Update();
                Application.DoEvents();   // Allow Form time to repaint itself //

                string strFriendlyTableName = arrayListTableFriendlyNames[intCounter].ToString();
                string strStoredProcGet = arrayListStoredProcNamesGet[intCounter].ToString();
                string strStoredProcSet = arrayListStoredProcNamesSet[intCounter].ToString();
                try
                {
                    dsTemp = ws.GetData(header, _MachineID, TableName, strStoredProcGet);
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    ws.Close();
                    ws = null;
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nError: " + ex.Message + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    GC.GetTotalMemory(true);
                    GC.Collect();
                    this.Close();
                    return;
                }
                if (dsTemp == null)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    ws.Close();
                    ws = null;
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nThe connection may have been lost.\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    GC.GetTotalMemory(true);
                    GC.Collect();
                    this.Close();
                    return;
                }
                if (dsTemp.Tables.Count == 0) continue;   // Nothing to process //
                if (dsTemp.Tables[0].Rows.Count == 1)
                {
                    if (dsTemp.Tables[0].Columns.Count == 1)
                    {
                        string strError = dsTemp.Tables[0].Columns["Error"].ToString();
                        if (string.IsNullOrEmpty(strError) || strError == "Error") strError = "Unknown Error";
                        SQlConn.Close();
                        SQlConn.Dispose();
                        ws.Close();
                        ws = null;
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + strFriendlyTableName + " data.\n\nError: " + strError + ". The Web Service may not be running.\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }
                if (dsTemp.Tables[0].Rows.Count == 0) continue;   // Nothing to process //
 
                // Error checks passed so process data - Check if there are images to import //
                if (TableName == "UT_Survey_Picture" || TableName == "UT_Linked_Map" || TableName == "UT_Reference_File" || TableName == "UT_Permission_Document" || TableName == "UT_Work_Order" || TableName == "UT_Risk_Assessment_Team_Member_Signature")
                {
                    string strColumnName = "";
                    string strPath = "";
                    switch (TableName)
                    {
                        case "UT_Survey_Picture":
                            strColumnName = "PicturePath";
                            strPath = strPicturePath;
                            break;
                        case "UT_Linked_Map":
                            strColumnName = "DocumentPath";
                            strPath = strMapPath;
                            break;
                        case "UT_Reference_File":
                            strColumnName = "DocumentPath";
                            strPath = strReferenceFilePath;
                            break;
                        case "UT_Permission_Document":
                            strColumnName = "PDFFile";
                            strPath = strPermissionDocumentPath;
                            break;
                        case "UT_Work_Order":
                            strColumnName = "WorkOrderPDF";
                            strPath = strWorkOrderPath;
                            break;
                        case "UT_Risk_Assessment_Team_Member_Signature":
                            strColumnName = "SignatureFile";
                            strPath = strSignaturePath;
                            break;
                        default:
                            break;
                    }
                    foreach (DataRow dr in dsTemp.Tables[0].Rows)
                    {
                        byte[] byteData = null;
                        string fileName = dr[strColumnName].ToString();
                        if (string.IsNullOrEmpty(fileName)) continue;
                        if (TableName == "UT_Linked_Map") fileName += ".gif";  // Linked Maps don't have a file extension, so add it.
                        try
                        {
                            byteData = ws.FileGet(header, fileName, TableName);  // Get document as a stream from web service //
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        if (byteData == null) continue;

                        MemoryStream ms;
                        try
                        {
                            // instance a memory stream and pass the byte array to its constructor //
                            ms = new MemoryStream(byteData);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                        if (File.Exists(Path.Combine(strPath, fileName)))  // Attempt to delete old version and re-create.//
                        {
                            try
                            {
                                File.Delete(Path.Combine(strPath, fileName));
                            }
                            catch (Exception ex)
                            {
                                continue; // We can safely bomb out as the old version wasn't deleted so the new version doesn't really need to be created. //
                            }
                            try
                            {
                                FileStream fs = new FileStream(Path.Combine(strPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                ms.Close();
                                ms.Dispose();
                                fs.Close();
                                fs.Dispose();
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        else  // File doesn't exist so create it //
                        {
                            try
                            {
                                FileStream fs = new FileStream(Path.Combine(strPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                ms.Close();
                                ms.Dispose();
                                fs.Close();
                                fs.Dispose();
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }

                        // Handle any associated thumbnail //
                        if (TableName == "UT_Linked_Map")
                        {
                            fileName = dr[strColumnName].ToString();
                            if (string.IsNullOrEmpty(fileName)) continue;
                            fileName += "_thumb.jpg";  // Linked Maps don't have a file extension, so add it.
                            try
                            {
                                byteData = ws.FileGet(header, fileName, TableName);  // Get document as a stream from web service //
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                            if (byteData == null) continue;

                            try
                            {
                                // instance a memory stream and pass the byte array to its constructor //
                                ms = new MemoryStream(byteData);
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                            if (File.Exists(Path.Combine(strPath, fileName)))  // Attempt to delete old version and re-create.//
                            {
                                try
                                {
                                    File.Delete(Path.Combine(strPath, fileName));
                                }
                                catch (Exception ex)
                                {
                                    continue; // We can safely bomb out as the old version wasn't deleted so the new version doesn't really need to be created. //
                                }
                                try
                                {
                                    FileStream fs = new FileStream(Path.Combine(strPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                    ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                    ms.Close();
                                    ms.Dispose();
                                    fs.Close();
                                    fs.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    continue;
                                }
                            }
                            else  // File doesn't exist so create it //
                            {
                                try
                                {
                                    FileStream fs = new FileStream(Path.Combine(strPath, fileName), FileMode.Create);  // instance a filestream pointing to the storage folder, use the original file name to name the resulting file //
                                    ms.WriteTo(fs);  // write the memory stream containing the original file as a byte array to the filestream //
                                    ms.Close();
                                    ms.Dispose();
                                    fs.Close();
                                    fs.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    continue;
                                }
                            }
                        }
                    }
                }

                // Now Import Data //
                SqlCommand cmd = new SqlCommand(strStoredProcSet, SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                // Add paramaters to Stored Proc //
                foreach (DataColumn c in dsTemp.Tables[0].Columns)
                {
                    cmd.Parameters.Add(new SqlParameter("@" + c.ColumnName.Trim(), BaseObjects.ExtensionFunctions.ToSqlDbType(c.DataType), c.MaxLength));
                }
                cmd.Parameters.Add(new SqlParameter("@SystemDataTransferMode", _SystemDataTransferMode));  // Extra parameter to control if table trigger is fired on updating data in SP //

                // Merge Rows into Database... //
                foreach (DataRow r in dsTemp.Tables[0].Rows)
                {
                    foreach (DataColumn c in dsTemp.Tables[0].Columns)
                    {
                        cmd.Parameters["@" + c.ColumnName.Trim()].Value = r[c.ColumnName.Trim()];  // Populate parameters //
                    }
                    try
                    {
                        intStoredProcReturnValue = Convert.ToInt32(cmd.ExecuteScalar());  // Fire Stored Proc to merge data //
                    }
                    catch (Exception ex)
                    {
                        SQlConn.Close();
                        SQlConn.Dispose();
                        CleanUp();
                        DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to import " + arrayListTableFriendlyNames[intCounter] + " data.\n\nError: [" + ex.Message + "]!\n\nThis screen will now close. Try importing again. If the problem persists contact Technical Support.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                        return;
                    }
                }

                // Call Web Service to update last successfull retrieval of this type of data on the server database //
                string strReturn = "";
                try
                {
                    strReturn = ws.SetLastRetrieved(header, _MachineID, TableName);
                }
                catch (Exception ex)
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to update the Last Retrieved Date for " + strFriendlyTableName + " data on the Server.\n\nThe connection may have been lost..\n\nError: " + ex.Message + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
                if (!strReturn.ToLower().StartsWith("success"))
                {
                    SQlConn.Close();
                    SQlConn.Dispose();
                    CleanUp();
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Synchronisation Process failed trying to update the Last Retrieved Date for " + strFriendlyTableName + " data on the Server.\n\nError: " + strReturn + ".\n\nThis screen will now close. Try importing again.", "Import Data - Failed", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                    return;
                }
            }
            SQlConn.Close();
            SQlConn.Dispose();
            ws.Close();
            ws = null;

            using (WoodPlanDataSetTableAdapters.QueriesTableAdapter SetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter())
            {
                SetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    // Update Last Imported date in System Settings table //
                    _LastRetrievalDateTime = DateTime.Now;
                    _LastRetrievalOn = _LastRetrievalDateTime.ToString("dd/MM/yyyy HH:mm:ss");
                    SetSetting.sp00048_Update_System_Setting(8, "DataSynchronisationLastRetrieved", _LastRetrievalOn);
                    labelLastImportDate.Text = "        Last Successfull Import: " + _LastRetrievalOn;
                    _LastSentDateTime = _LastRetrievalDateTime.AddSeconds(10);  // Make sure we don't pick up anything unless it was after last import (10 seconds is a safe margin) //

                }
                catch (Exception)
                {
                }
            }

            CleanUp();
            DevExpress.XtraEditors.XtraMessageBox.Show("The Import Synchronisation Process completed Successfully.", "Import Data - Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }


        private void ShowProgressBar(DevExpress.XtraEditors.ProgressBarControl pb, int intProgressCount)
        {
            pb.Visible = true;
            pb.Properties.Step = 1;
            pb.Properties.PercentView = true;
            pb.Properties.Maximum = intProgressCount;
            pb.Properties.Minimum = 0;
            Application.DoEvents();
        }

        private void CleanUp()
        {
            progressBarControlExport.Visible = false;
            progressBarControlExport.Position = 0;
            btnExport.Enabled = true;

            progressBarControlImport.Visible = false;
            progressBarControlImport.Position = 0;

            // Reload Export objects grid to reflect what has been exported //
            sp07502_UT_WebService_Tablet_Get_Export_OverviewTableAdapter.Fill(dataSet_UT_DataTransfer.sp07502_UT_WebService_Tablet_Get_Export_Overview, _LastSentDateTime);
            Application.DoEvents();
            SetExportImportStatus();
        }

        private void SetExportImportStatus()
        {
            // Check if 5 minutes has passed since last import - if not, prevent a re-import until it expires //
            TimeSpan span = DateTime.Now.Subtract(_LastRetrievalDateTime);
            if (span.TotalMinutes < (double)5)
            {
                if (!timer.Enabled)
                {
                    btnExport.Enabled = false;
                    btnImport.Enabled = false;
                    TimeoutLabel.Text = "        You cannot Import again until: " + String.Format("{0:dd/MM/yyyy HH:mm:ss}", _LastRetrievalDateTime.AddMinutes(5));
                    TimeoutLabel.Visible = true;
                    timer.Start();
                }
            }
            else
            {
                timer.Stop();
                TimeoutLabel.Visible = false;

                GridView view = (GridView)gridControl1.MainView;
                if (view.DataRowCount > 0)
                {
                    btnExport.Enabled = true;
                    btnImport.Enabled = false;
                }
                else
                {
                    btnExport.Enabled = false;
                    btnImport.Enabled = true;
                }
                //btnExport.Enabled = true;
                //btnImport.Enabled = true;
            }
        }

        private bool GetInternetConnectionStatus()
        {
            // Check for internet Connectivity //
            bool boolInternet = PingTest("www.google.com");
            if (!boolInternet) PingTest("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolInternet) PingTest("www.yahoo.com");  // try another site just in case that one is down //
            return boolInternet;
        }

        public bool PingTest(string strSite)
        {
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                System.Net.NetworkInformation.PingReply pingStatus = ping.Send(strSite, 1000);
                return pingStatus.Status == System.Net.NetworkInformation.IPStatus.Success;
            }
            catch (Exception){ }
            return false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TimerEventProcessor(object sender, EventArgs e)
        {
            SetExportImportStatus();
        }

        private void frm_UT_Data_Sync_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WebEye;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Picture_Capture_Camera_Stream : DevExpress.XtraEditors.XtraForm
    {
        public Image imageCaptured = null;
        public frm_UT_Picture_Capture_Camera_Stream()
        {
            InitializeComponent();
        }

        private class ComboBoxItem
        {
            public ComboBoxItem(WebCameraId id)
            {
                _id = id;
            }

            private readonly WebCameraId _id;
            public WebCameraId Id
            {
                get { return _id; }
            }

            public override string ToString()
            {
                // Generates the text shown in the combo box. //
                return _id.Name;
            }
        }

        private void frm_UT_Picture_Capture_Camera_Stream_Load(object sender, EventArgs e)
        {
            foreach (WebCameraId camera in webCameraControl1.GetVideoCaptureDevices())
            {
                repositoryItemComboBox1.Items.Add(new ComboBoxItem(camera));
            }

            if (repositoryItemComboBox1.Items.Count > 0)
            {
                beiCameraList.EditValue = repositoryItemComboBox1.Items[0];
                Application.DoEvents();
                bbiStartCamera.PerformClick();
            }

        }

        private void bbiStartCamera_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ComboBoxItem i = (ComboBoxItem)beiCameraList.EditValue;

            try
            {
                webCameraControl1.StartCapture(i.Id);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                UpdateButtons();
            }

        }

        private void bbiStopCamera_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                webCameraControl1.StopCapture();
            }
            catch (Exception ex)
            {
            }
            UpdateButtons();
        }

        private void bbiTakePicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                imageCaptured = webCameraControl1.GetCurrentImage();
                bbiStopCamera.PerformClick();
            }
            catch (Exception ex)
            {
            }
            this.Close();

        }

        private void beiCameraList_EditValueChanged(object sender, EventArgs e)
        {
            UpdateButtons();
        }

        private void UpdateButtons()
        {
            bbiStartCamera.Enabled = beiCameraList.EditValue != null;
            bbiStopCamera.Enabled = webCameraControl1.IsCapturing;
            bbiTakePicture.Enabled = webCameraControl1.IsCapturing;
        }

        private void bbiClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                bbiStopCamera.PerformClick();
            }
            catch (Exception ex)
            {
            }
            this.Close();
        }



    }
}
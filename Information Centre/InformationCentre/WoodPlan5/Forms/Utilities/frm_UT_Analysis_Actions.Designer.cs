namespace WoodPlan5
{
    partial class frm_UT_Analysis_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Analysis_Actions));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup4 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup5 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup6 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.fieldDateRaisedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaisedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduledWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDateWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnAnalyse = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlSelectedCount = new DevExpress.XtraEditors.LabelControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07323UTAnalysisActionsAvailableActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionElapsedDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnSurvey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveySpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionTeamRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPossibleFlail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlActionFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.ActionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07322UTMasterJobTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colJobID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp07324UTAnalysisActionsForAnalysisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldActionID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldJobTypeDescription1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReferenceNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateRaised1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateScheduled1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompleted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldApproved1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedHours = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualHours = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPossibleLiveWork = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedLabourCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualLabourCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedLabourSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualLabourSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedMaterialsCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualMaterialsCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedMaterialsSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualMaterialsSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedEquipmentCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualEquipmentCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedEquipmentSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualEquipmentSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedTotalCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedTotalSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualTotalCost1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualTotalSell1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldWorkOrderID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSelfBillingInvoiceID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFinanceSystemBillingID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCompletionSheetID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTotalTimeTaken1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionTeamRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCycle1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUnitDesc1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsTransformer1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTransformerNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientCode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVoltageType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFeederName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRegionName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPrimaryName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubAreaName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionElapsedDays1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDateCompletedYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReactive = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCountAwaitingPermission = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCountPermissioned = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCountNotPermissioned = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPermissionCountOnHold = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPossibleFlail = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAchievableClearance = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentNone = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentEcoPlugs = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentSpraying = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStumpTreatmentPaint = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementNone = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementWhips = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeReplacementStandards = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter();
            this.sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter();
            this.sp07322_UT_Master_Job_TypesTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07322_UT_Master_Job_TypesTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToggleAvailableColumnsVisibility = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.buttonEditFilterCircuits = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemButtonEditFilterCircuits = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barEditItemAction = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditActionFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalyse = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStatusFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07323UTAnalysisActionsAvailableActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlActionFilter)).BeginInit();
            this.popupContainerControlActionFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07322UTMasterJobTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).BeginInit();
            this.popupContainerControlDateFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07324UTAnalysisActionsForAnalysisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditActionFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.barEditItemStatusFilter,
            this.barEditItemDateFilter,
            this.bbiReloadData,
            this.bbiAnalyse,
            this.bbiToggleAvailableColumnsVisibility,
            this.barEditItem1,
            this.buttonEditFilterCircuits,
            this.barEditItemAction});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditStatusFilter,
            this.repositoryItemPopupContainerEditDateFilter,
            this.repositoryItemTextEdit2,
            this.repositoryItemButtonEditFilterCircuits,
            this.repositoryItemPopupContainerEditActionFilter});
            // 
            // fieldDateRaisedYear
            // 
            this.fieldDateRaisedYear.AreaIndex = 52;
            this.fieldDateRaisedYear.Caption = "Year Raised";
            this.fieldDateRaisedYear.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedYear.FieldName = "DateRaised";
            this.fieldDateRaisedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateRaisedYear.Name = "fieldDateRaisedYear";
            this.fieldDateRaisedYear.UnboundFieldName = "fieldDateRaisedYear";
            // 
            // fieldDateRaisedQuarter
            // 
            this.fieldDateRaisedQuarter.AreaIndex = 1;
            this.fieldDateRaisedQuarter.Caption = "Quarter Raised";
            this.fieldDateRaisedQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedQuarter.FieldName = "DateRaised";
            this.fieldDateRaisedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateRaisedQuarter.Name = "fieldDateRaisedQuarter";
            this.fieldDateRaisedQuarter.UnboundFieldName = "fieldDateRaisedQuarter";
            this.fieldDateRaisedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateRaisedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateRaisedQuarter.Visible = false;
            // 
            // fieldDateRaisedMonth
            // 
            this.fieldDateRaisedMonth.AreaIndex = 2;
            this.fieldDateRaisedMonth.Caption = "Month Raised";
            this.fieldDateRaisedMonth.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedMonth.FieldName = "DateRaised";
            this.fieldDateRaisedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateRaisedMonth.Name = "fieldDateRaisedMonth";
            this.fieldDateRaisedMonth.UnboundFieldName = "fieldDateRaisedMonth";
            this.fieldDateRaisedMonth.Visible = false;
            // 
            // fieldDateRaisedWeek
            // 
            this.fieldDateRaisedWeek.AreaIndex = 3;
            this.fieldDateRaisedWeek.Caption = "Week Raised";
            this.fieldDateRaisedWeek.ExpandedInFieldsGroup = false;
            this.fieldDateRaisedWeek.FieldName = "DateRaised";
            this.fieldDateRaisedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateRaisedWeek.Name = "fieldDateRaisedWeek";
            this.fieldDateRaisedWeek.UnboundFieldName = "fieldDateRaisedWeek";
            this.fieldDateRaisedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateRaisedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateRaisedWeek.Visible = false;
            // 
            // fieldDateScheduledYear
            // 
            this.fieldDateScheduledYear.AreaIndex = 53;
            this.fieldDateScheduledYear.Caption = "Year Scheduled";
            this.fieldDateScheduledYear.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledYear.FieldName = "DateScheduled";
            this.fieldDateScheduledYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateScheduledYear.Name = "fieldDateScheduledYear";
            this.fieldDateScheduledYear.UnboundFieldName = "fieldDateScheduledYear";
            // 
            // fieldDateScheduledQuarter
            // 
            this.fieldDateScheduledQuarter.AreaIndex = 1;
            this.fieldDateScheduledQuarter.Caption = "Quarter Scheduled";
            this.fieldDateScheduledQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledQuarter.FieldName = "DateScheduled";
            this.fieldDateScheduledQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateScheduledQuarter.Name = "fieldDateScheduledQuarter";
            this.fieldDateScheduledQuarter.UnboundFieldName = "fieldDateScheduledQuarter";
            this.fieldDateScheduledQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateScheduledQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateScheduledQuarter.Visible = false;
            // 
            // fieldDateScheduledMonth
            // 
            this.fieldDateScheduledMonth.AreaIndex = 2;
            this.fieldDateScheduledMonth.Caption = "Month Scheduled";
            this.fieldDateScheduledMonth.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledMonth.FieldName = "DateScheduled";
            this.fieldDateScheduledMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateScheduledMonth.Name = "fieldDateScheduledMonth";
            this.fieldDateScheduledMonth.UnboundFieldName = "fieldDateScheduledMonth";
            this.fieldDateScheduledMonth.Visible = false;
            // 
            // fieldDateScheduledWeek
            // 
            this.fieldDateScheduledWeek.AreaIndex = 3;
            this.fieldDateScheduledWeek.Caption = "Week Scheduled";
            this.fieldDateScheduledWeek.ExpandedInFieldsGroup = false;
            this.fieldDateScheduledWeek.FieldName = "DateScheduled";
            this.fieldDateScheduledWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateScheduledWeek.Name = "fieldDateScheduledWeek";
            this.fieldDateScheduledWeek.UnboundFieldName = "fieldDateScheduledWeek";
            this.fieldDateScheduledWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateScheduledWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateScheduledWeek.Visible = false;
            // 
            // fieldDateCompletedQuarter
            // 
            this.fieldDateCompletedQuarter.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldDateCompletedQuarter.AreaIndex = 1;
            this.fieldDateCompletedQuarter.Caption = "Quarter Completed";
            this.fieldDateCompletedQuarter.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedQuarter.FieldName = "DateCompleted";
            this.fieldDateCompletedQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldDateCompletedQuarter.Name = "fieldDateCompletedQuarter";
            this.fieldDateCompletedQuarter.UnboundFieldName = "fieldDateCompletedQuarter";
            this.fieldDateCompletedQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldDateCompletedQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateCompletedQuarter.Visible = false;
            // 
            // fieldDateCompletedMonth
            // 
            this.fieldDateCompletedMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldDateCompletedMonth.AreaIndex = 2;
            this.fieldDateCompletedMonth.Caption = "Month Completed";
            this.fieldDateCompletedMonth.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedMonth.FieldName = "DateCompleted";
            this.fieldDateCompletedMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldDateCompletedMonth.Name = "fieldDateCompletedMonth";
            this.fieldDateCompletedMonth.UnboundFieldName = "fieldDateCompletedMonth";
            this.fieldDateCompletedMonth.Visible = false;
            // 
            // fieldDateCompletedWeek
            // 
            this.fieldDateCompletedWeek.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldDateCompletedWeek.AreaIndex = 3;
            this.fieldDateCompletedWeek.Caption = "Week Completed";
            this.fieldDateCompletedWeek.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedWeek.FieldName = "DateCompleted";
            this.fieldDateCompletedWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldDateCompletedWeek.Name = "fieldDateCompletedWeek";
            this.fieldDateCompletedWeek.UnboundFieldName = "fieldDateCompletedWeek";
            this.fieldDateCompletedWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldDateCompletedWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldDateCompletedWeek.Visible = false;
            // 
            // fieldSurveyDateYear
            // 
            this.fieldSurveyDateYear.AreaIndex = 55;
            this.fieldSurveyDateYear.Caption = "Survey Year";
            this.fieldSurveyDateYear.ExpandedInFieldsGroup = false;
            this.fieldSurveyDateYear.FieldName = "SurveyDate";
            this.fieldSurveyDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldSurveyDateYear.Name = "fieldSurveyDateYear";
            this.fieldSurveyDateYear.UnboundFieldName = "fieldSurveyDateYear";
            // 
            // fieldSurveyDateQuarter
            // 
            this.fieldSurveyDateQuarter.AreaIndex = 1;
            this.fieldSurveyDateQuarter.Caption = "Survey Quarter";
            this.fieldSurveyDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldSurveyDateQuarter.FieldName = "SurveyDate";
            this.fieldSurveyDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldSurveyDateQuarter.Name = "fieldSurveyDateQuarter";
            this.fieldSurveyDateQuarter.UnboundFieldName = "fieldSurveyDateQuarter";
            this.fieldSurveyDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldSurveyDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSurveyDateQuarter.Visible = false;
            // 
            // fieldSurveyDateMonth
            // 
            this.fieldSurveyDateMonth.AreaIndex = 1;
            this.fieldSurveyDateMonth.Caption = "Survey Month";
            this.fieldSurveyDateMonth.ExpandedInFieldsGroup = false;
            this.fieldSurveyDateMonth.FieldName = "SurveyDate";
            this.fieldSurveyDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldSurveyDateMonth.Name = "fieldSurveyDateMonth";
            this.fieldSurveyDateMonth.UnboundFieldName = "fieldSurveyDateMonth";
            this.fieldSurveyDateMonth.Visible = false;
            // 
            // fieldSurveyDateWeek
            // 
            this.fieldSurveyDateWeek.AreaIndex = 1;
            this.fieldSurveyDateWeek.Caption = "Survey Week";
            this.fieldSurveyDateWeek.ExpandedInFieldsGroup = false;
            this.fieldSurveyDateWeek.FieldName = "SurveyDate";
            this.fieldSurveyDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldSurveyDateWeek.Name = "fieldSurveyDateWeek";
            this.fieldSurveyDateWeek.UnboundFieldName = "fieldSurveyDateWeek";
            this.fieldSurveyDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldSurveyDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSurveyDateWeek.Visible = false;
            // 
            // fieldLastInspectionDateYear
            // 
            this.fieldLastInspectionDateYear.AreaIndex = 58;
            this.fieldLastInspectionDateYear.Caption = "Last Inspection Year";
            this.fieldLastInspectionDateYear.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldLastInspectionDateYear.Name = "fieldLastInspectionDateYear";
            this.fieldLastInspectionDateYear.UnboundFieldName = "fieldLastInspectionDateYear";
            this.fieldLastInspectionDateYear.Visible = false;
            // 
            // fieldLastInspectionDateQuarter
            // 
            this.fieldLastInspectionDateQuarter.AreaIndex = 59;
            this.fieldLastInspectionDateQuarter.Caption = "Last Inspection Quarter";
            this.fieldLastInspectionDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateQuarter.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldLastInspectionDateQuarter.Name = "fieldLastInspectionDateQuarter";
            this.fieldLastInspectionDateQuarter.UnboundFieldName = "fieldLastInspectionDateQuarter";
            this.fieldLastInspectionDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldLastInspectionDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionDateQuarter.Visible = false;
            // 
            // fieldLastInspectionDateMonth
            // 
            this.fieldLastInspectionDateMonth.AreaIndex = 1;
            this.fieldLastInspectionDateMonth.Caption = "Last Inspection Month";
            this.fieldLastInspectionDateMonth.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateMonth.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldLastInspectionDateMonth.Name = "fieldLastInspectionDateMonth";
            this.fieldLastInspectionDateMonth.UnboundFieldName = "fieldLastInspectionDateMonth";
            this.fieldLastInspectionDateMonth.Visible = false;
            // 
            // fieldLastInspectionDateWeek
            // 
            this.fieldLastInspectionDateWeek.AreaIndex = 1;
            this.fieldLastInspectionDateWeek.Caption = "Last Inspection Week";
            this.fieldLastInspectionDateWeek.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionDateWeek.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldLastInspectionDateWeek.Name = "fieldLastInspectionDateWeek";
            this.fieldLastInspectionDateWeek.UnboundFieldName = "fieldLastInspectionDateWeek";
            this.fieldLastInspectionDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldLastInspectionDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionDateWeek.Visible = false;
            // 
            // fieldNextInspectionDateYear
            // 
            this.fieldNextInspectionDateYear.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateYear.AreaIndex = 0;
            this.fieldNextInspectionDateYear.Caption = "Next Inspection Year";
            this.fieldNextInspectionDateYear.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateYear.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldNextInspectionDateYear.Name = "fieldNextInspectionDateYear";
            this.fieldNextInspectionDateYear.UnboundFieldName = "fieldNextInspectionDateYear";
            this.fieldNextInspectionDateYear.Visible = false;
            // 
            // fieldNextInspectionDateQuarter
            // 
            this.fieldNextInspectionDateQuarter.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateQuarter.AreaIndex = 1;
            this.fieldNextInspectionDateQuarter.Caption = "Next Inspection Quarter";
            this.fieldNextInspectionDateQuarter.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateQuarter.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldNextInspectionDateQuarter.Name = "fieldNextInspectionDateQuarter";
            this.fieldNextInspectionDateQuarter.UnboundFieldName = "fieldNextInspectionDateQuarter";
            this.fieldNextInspectionDateQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldNextInspectionDateQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionDateQuarter.Visible = false;
            // 
            // fieldNextInspectionDateMonth
            // 
            this.fieldNextInspectionDateMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateMonth.AreaIndex = 2;
            this.fieldNextInspectionDateMonth.Caption = "Next Inspection Month";
            this.fieldNextInspectionDateMonth.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateMonth.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldNextInspectionDateMonth.Name = "fieldNextInspectionDateMonth";
            this.fieldNextInspectionDateMonth.UnboundFieldName = "fieldNextInspectionDateMonth";
            this.fieldNextInspectionDateMonth.Visible = false;
            // 
            // fieldNextInspectionDateWeek
            // 
            this.fieldNextInspectionDateWeek.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.fieldNextInspectionDateWeek.AreaIndex = 3;
            this.fieldNextInspectionDateWeek.Caption = "Next Inspection Week";
            this.fieldNextInspectionDateWeek.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionDateWeek.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDateWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldNextInspectionDateWeek.Name = "fieldNextInspectionDateWeek";
            this.fieldNextInspectionDateWeek.UnboundFieldName = "fieldNextInspectionDateWeek";
            this.fieldNextInspectionDateWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldNextInspectionDateWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionDateWeek.Visible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(444, 200);
            this.dockPanel1.Size = new System.Drawing.Size(444, 494);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnAnalyse);
            this.dockPanel1_Container.Controls.Add(this.labelControlSelectedCount);
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.gridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(438, 462);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnalyse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAnalyse.Appearance.Options.UseFont = true;
            this.btnAnalyse.Image = ((System.Drawing.Image)(resources.GetObject("btnAnalyse.Image")));
            this.btnAnalyse.Location = new System.Drawing.Point(326, 436);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(112, 26);
            this.btnAnalyse.TabIndex = 5;
            this.btnAnalyse.Text = "Analyse Data";
            this.btnAnalyse.Click += new System.EventHandler(this.btnAnalyse_Click);
            // 
            // labelControlSelectedCount
            // 
            this.labelControlSelectedCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedCount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControlSelectedCount.Appearance.Image")));
            this.labelControlSelectedCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedCount.Location = new System.Drawing.Point(2, 440);
            this.labelControlSelectedCount.Name = "labelControlSelectedCount";
            this.labelControlSelectedCount.Size = new System.Drawing.Size(253, 18);
            this.labelControlSelectedCount.TabIndex = 7;
            this.labelControlSelectedCount.Text = "        0 Actions Selected For Analysis";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(438, 34);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp07323UTAnalysisActionsAvailableActionsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 34);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditNumeric2DP,
            this.repositoryItemTextEditMeters});
            this.gridControl1.Size = new System.Drawing.Size(438, 401);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07323UTAnalysisActionsAvailableActionsBindingSource
            // 
            this.sp07323UTAnalysisActionsAvailableActionsBindingSource.DataMember = "sp07323_UT_Analysis_Actions_Available_Actions";
            this.sp07323UTAnalysisActionsAvailableActionsBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCircuitID,
            this.colPoleNumber,
            this.colPoleTypeID,
            this.colStatusID,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colX,
            this.colY,
            this.colLatitude,
            this.colLongitude,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colCircuitStatus,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colRegionName,
            this.colStatus,
            this.colPrimaryName,
            this.colSubAreaName,
            this.colRegionID,
            this.colSubAreaID,
            this.colPrimaryID,
            this.colFeederID,
            this.colPoleType,
            this.colLastInspectionElapsedDays,
            this.colMapID,
            this.colOnSurvey,
            this.colSurveySpanClear,
            this.colSurveyorName,
            this.colActionID,
            this.colActionRemarks,
            this.colActionStatus,
            this.colActionStatusID,
            this.colActionTeamRemarks,
            this.colActualEquipmentCost,
            this.colActualEquipmentSell,
            this.colActualLabourCost,
            this.colActualLabourSell,
            this.colActualMaterialsCost,
            this.colActualMaterialsSell,
            this.colActualTotalCost,
            this.colActualTotalSell,
            this.colApproved,
            this.colCompletionSheetID,
            this.colDateCompleted,
            this.colDateRaised,
            this.colDateScheduled,
            this.colEstimatedEquipmentCost,
            this.colEstimatedEquipmentSell,
            this.colEstimatedLabourCost,
            this.colEstimatedLabourSell,
            this.colEstimatedMaterialsCost,
            this.colEstimatedMaterialsSell,
            this.colEstimatedTotalCost,
            this.colEstimatedTotalSell,
            this.colFinanceSystemBillingID,
            this.colJobTypeDescription,
            this.colJobTypeID,
            this.colReferenceNumber,
            this.colSelfBillingInvoiceID,
            this.colSurveyRemarks,
            this.colTotalTimeTaken,
            this.colWorkOrderID,
            this.colAchievableClearance,
            this.colPossibleFlail,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colActualHours,
            this.colEstimatedHours,
            this.colPossibleLiveWork});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 4;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colCircuitID
            // 
            this.colCircuitID.Caption = "Circuit ID";
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            this.colCircuitID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 3;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleTypeID
            // 
            this.colPoleTypeID.Caption = "Pole Type ID";
            this.colPoleTypeID.FieldName = "PoleTypeID";
            this.colPoleTypeID.Name = "colPoleTypeID";
            this.colPoleTypeID.OptionsColumn.AllowEdit = false;
            this.colPoleTypeID.OptionsColumn.AllowFocus = false;
            this.colPoleTypeID.OptionsColumn.ReadOnly = true;
            this.colPoleTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Width = 94;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionCycle.Width = 55;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Cycle Unit";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnitDesc.Width = 69;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Width = 97;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colX.Width = 59;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colY.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Width = 66;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Width = 68;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsTransformer.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransformerNumber.Width = 120;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 130;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientCode1.Width = 76;
            // 
            // colCircuitStatus
            // 
            this.colCircuitStatus.Caption = "Client Status";
            this.colCircuitStatus.FieldName = "CircuitStatus";
            this.colCircuitStatus.Name = "colCircuitStatus";
            this.colCircuitStatus.OptionsColumn.AllowEdit = false;
            this.colCircuitStatus.OptionsColumn.AllowFocus = false;
            this.colCircuitStatus.OptionsColumn.ReadOnly = true;
            this.colCircuitStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitStatus.Width = 82;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 0;
            this.colCircuitName.Width = 130;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Width = 91;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 14;
            this.colVoltageType.Width = 84;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 2;
            this.colFeederName.Width = 130;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 0;
            this.colRegionName.Width = 130;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Width = 63;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 1;
            this.colPrimaryName.Width = 130;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 0;
            this.colSubAreaName.Width = 130;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            this.colRegionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSubAreaID
            // 
            this.colSubAreaID.Caption = "Sub-Area ID";
            this.colSubAreaID.FieldName = "SubAreaID";
            this.colSubAreaID.Name = "colSubAreaID";
            this.colSubAreaID.OptionsColumn.AllowEdit = false;
            this.colSubAreaID.OptionsColumn.AllowFocus = false;
            this.colSubAreaID.OptionsColumn.ReadOnly = true;
            this.colSubAreaID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaID.Width = 80;
            // 
            // colPrimaryID
            // 
            this.colPrimaryID.Caption = "Primary ID";
            this.colPrimaryID.FieldName = "PrimaryID";
            this.colPrimaryID.Name = "colPrimaryID";
            this.colPrimaryID.OptionsColumn.AllowEdit = false;
            this.colPrimaryID.OptionsColumn.AllowFocus = false;
            this.colPrimaryID.OptionsColumn.ReadOnly = true;
            this.colPrimaryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFeederID
            // 
            this.colFeederID.Caption = "Feeder ID";
            this.colFeederID.FieldName = "FeederID";
            this.colFeederID.Name = "colFeederID";
            this.colFeederID.OptionsColumn.AllowEdit = false;
            this.colFeederID.OptionsColumn.AllowFocus = false;
            this.colFeederID.OptionsColumn.ReadOnly = true;
            this.colFeederID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLastInspectionElapsedDays
            // 
            this.colLastInspectionElapsedDays.Caption = "Elapsed Days";
            this.colLastInspectionElapsedDays.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.Name = "colLastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionElapsedDays.Width = 85;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Width = 240;
            // 
            // colOnSurvey
            // 
            this.colOnSurvey.Caption = "Surveyed";
            this.colOnSurvey.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colOnSurvey.FieldName = "OnSurvey";
            this.colOnSurvey.Name = "colOnSurvey";
            this.colOnSurvey.OptionsColumn.AllowEdit = false;
            this.colOnSurvey.OptionsColumn.AllowFocus = false;
            this.colOnSurvey.OptionsColumn.ReadOnly = true;
            this.colOnSurvey.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOnSurvey.Width = 67;
            // 
            // colSurveySpanClear
            // 
            this.colSurveySpanClear.Caption = "Span Clear";
            this.colSurveySpanClear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSurveySpanClear.FieldName = "SurveySpanClear";
            this.colSurveySpanClear.Name = "colSurveySpanClear";
            this.colSurveySpanClear.OptionsColumn.AllowEdit = false;
            this.colSurveySpanClear.OptionsColumn.AllowFocus = false;
            this.colSurveySpanClear.OptionsColumn.ReadOnly = true;
            this.colSurveySpanClear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveySpanClear.Width = 73;
            // 
            // colSurveyorName
            // 
            this.colSurveyorName.Caption = "Surveyor Name";
            this.colSurveyorName.FieldName = "SurveyorName";
            this.colSurveyorName.Name = "colSurveyorName";
            this.colSurveyorName.OptionsColumn.AllowEdit = false;
            this.colSurveyorName.OptionsColumn.AllowFocus = false;
            this.colSurveyorName.OptionsColumn.ReadOnly = true;
            this.colSurveyorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyorName.Visible = true;
            this.colSurveyorName.VisibleIndex = 3;
            this.colSurveyorName.Width = 95;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            this.colActionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colActionRemarks
            // 
            this.colActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colActionRemarks.CustomizationCaption = "Actoin Remarks";
            this.colActionRemarks.FieldName = "ActionRemarks";
            this.colActionRemarks.Name = "colActionRemarks";
            this.colActionRemarks.OptionsColumn.ReadOnly = true;
            this.colActionRemarks.Visible = true;
            this.colActionRemarks.VisibleIndex = 32;
            this.colActionRemarks.Width = 95;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colActionStatus
            // 
            this.colActionStatus.CustomizationCaption = "Action Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 4;
            this.colActionStatus.Width = 85;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.CustomizationCaption = "Action Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            this.colActionStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionStatusID.Width = 99;
            // 
            // colActionTeamRemarks
            // 
            this.colActionTeamRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colActionTeamRemarks.CustomizationCaption = "Action Team Remarks";
            this.colActionTeamRemarks.FieldName = "ActionTeamRemarks";
            this.colActionTeamRemarks.Name = "colActionTeamRemarks";
            this.colActionTeamRemarks.OptionsColumn.ReadOnly = true;
            this.colActionTeamRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionTeamRemarks.Visible = true;
            this.colActionTeamRemarks.VisibleIndex = 33;
            this.colActionTeamRemarks.Width = 124;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Actual Equipment Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 25;
            this.colActualEquipmentCost.Width = 129;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Actual Equipment Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 27;
            this.colActualEquipmentSell.Width = 123;
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Actual Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 21;
            this.colActualLabourCost.Width = 112;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Actual Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 23;
            this.colActualLabourSell.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Actual Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 29;
            this.colActualMaterialsCost.Width = 117;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Actual Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 31;
            this.colActualMaterialsSell.Width = 111;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Actual Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalCost.Width = 103;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Actual Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalSell.Width = 97;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 11;
            // 
            // colCompletionSheetID
            // 
            this.colCompletionSheetID.Caption = "Completion Sheet ID";
            this.colCompletionSheetID.FieldName = "CompletionSheetID";
            this.colCompletionSheetID.Name = "colCompletionSheetID";
            this.colCompletionSheetID.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetID.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetID.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletionSheetID.Width = 119;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 9;
            this.colDateCompleted.Width = 98;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 7;
            this.colDateRaised.Width = 79;
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 8;
            this.colDateScheduled.Width = 96;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Estimated Equipment Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 24;
            this.colEstimatedEquipmentCost.Width = 146;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Estimated Equipment Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 26;
            this.colEstimatedEquipmentSell.Width = 140;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Estimated Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 20;
            this.colEstimatedLabourCost.Width = 129;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = " Estimated Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 22;
            this.colEstimatedLabourSell.Width = 126;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Estimated Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 28;
            this.colEstimatedMaterialsCost.Width = 134;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Estimated Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 30;
            this.colEstimatedMaterialsSell.Width = 128;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Estimated Total Cost";
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 18;
            this.colEstimatedTotalCost.Width = 120;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Estimated Total Sell";
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 19;
            this.colEstimatedTotalSell.Width = 114;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colJobTypeDescription
            // 
            this.colJobTypeDescription.Caption = "Job Type Description";
            this.colJobTypeDescription.FieldName = "JobTypeDescription";
            this.colJobTypeDescription.Name = "colJobTypeDescription";
            this.colJobTypeDescription.OptionsColumn.AllowEdit = false;
            this.colJobTypeDescription.OptionsColumn.AllowFocus = false;
            this.colJobTypeDescription.OptionsColumn.ReadOnly = true;
            this.colJobTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobTypeDescription.Visible = true;
            this.colJobTypeDescription.VisibleIndex = 5;
            this.colJobTypeDescription.Width = 121;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 6;
            this.colReferenceNumber.Width = 111;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing Invoice ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSelfBillingInvoiceID.Width = 120;
            // 
            // colSurveyRemarks
            // 
            this.colSurveyRemarks.Caption = "Survey Remarks";
            this.colSurveyRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colSurveyRemarks.FieldName = "SurveyRemarks";
            this.colSurveyRemarks.Name = "colSurveyRemarks";
            this.colSurveyRemarks.OptionsColumn.ReadOnly = true;
            this.colSurveyRemarks.Width = 99;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.ColumnEdit = this.repositoryItemTextEditHours;
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTotalTimeTaken.Visible = true;
            this.colTotalTimeTaken.VisibleIndex = 10;
            this.colTotalTimeTaken.Width = 102;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderID.Visible = true;
            this.colWorkOrderID.VisibleIndex = 34;
            this.colWorkOrderID.Width = 91;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Achievable Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 13;
            this.colAchievableClearance.Width = 124;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "######0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colPossibleFlail
            // 
            this.colPossibleFlail.Caption = "Possible Flail";
            this.colPossibleFlail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleFlail.FieldName = "PossibleFlail";
            this.colPossibleFlail.Name = "colPossibleFlail";
            this.colPossibleFlail.OptionsColumn.AllowEdit = false;
            this.colPossibleFlail.OptionsColumn.AllowFocus = false;
            this.colPossibleFlail.OptionsColumn.ReadOnly = true;
            this.colPossibleFlail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPossibleFlail.Visible = true;
            this.colPossibleFlail.VisibleIndex = 12;
            this.colPossibleFlail.Width = 80;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 35;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 36;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 37;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 38;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 39;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 40;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 41;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 17;
            this.colActualHours.Width = 82;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 16;
            this.colEstimatedHours.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 15;
            this.colPossibleLiveWork.Width = 109;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P2";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(444, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlActionFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(849, 494);
            this.splitContainerControl1.SplitterPosition = 388;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl2";
            // 
            // popupContainerControlActionFilter
            // 
            this.popupContainerControlActionFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlActionFilter.Controls.Add(this.ActionFilterOK);
            this.popupContainerControlActionFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlActionFilter.Location = new System.Drawing.Point(403, 190);
            this.popupContainerControlActionFilter.Name = "popupContainerControlActionFilter";
            this.popupContainerControlActionFilter.Size = new System.Drawing.Size(230, 198);
            this.popupContainerControlActionFilter.TabIndex = 36;
            // 
            // ActionFilterOK
            // 
            this.ActionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ActionFilterOK.Location = new System.Drawing.Point(3, 173);
            this.ActionFilterOK.Name = "ActionFilterOK";
            this.ActionFilterOK.Size = new System.Drawing.Size(35, 22);
            this.ActionFilterOK.TabIndex = 19;
            this.ActionFilterOK.Text = "OK";
            this.ActionFilterOK.Click += new System.EventHandler(this.ActionFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07322UTMasterJobTypesBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(224, 168);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07322UTMasterJobTypesBindingSource
            // 
            this.sp07322UTMasterJobTypesBindingSource.DataMember = "sp07322_UT_Master_Job_Types";
            this.sp07322UTMasterJobTypesBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colJobID,
            this.colJobCode,
            this.colJobDescription});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colJobID
            // 
            this.colJobID.Caption = "Action ID";
            this.colJobID.FieldName = "JobID";
            this.colJobID.Name = "colJobID";
            this.colJobID.OptionsColumn.AllowEdit = false;
            this.colJobID.OptionsColumn.AllowFocus = false;
            this.colJobID.OptionsColumn.ReadOnly = true;
            this.colJobID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colJobCode
            // 
            this.colJobCode.Caption = "Action Code";
            this.colJobCode.FieldName = "JobCode";
            this.colJobCode.Name = "colJobCode";
            this.colJobCode.OptionsColumn.AllowEdit = false;
            this.colJobCode.OptionsColumn.AllowFocus = false;
            this.colJobCode.OptionsColumn.ReadOnly = true;
            this.colJobCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobCode.Width = 80;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Action Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 195;
            // 
            // popupContainerControlDateFilter
            // 
            this.popupContainerControlDateFilter.Controls.Add(this.btnDateFilterOK);
            this.popupContainerControlDateFilter.Controls.Add(this.groupControl2);
            this.popupContainerControlDateFilter.Location = new System.Drawing.Point(57, 279);
            this.popupContainerControlDateFilter.Name = "popupContainerControlDateFilter";
            this.popupContainerControlDateFilter.Size = new System.Drawing.Size(313, 82);
            this.popupContainerControlDateFilter.TabIndex = 35;
            // 
            // btnDateFilterOK
            // 
            this.btnDateFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateFilterOK.Location = new System.Drawing.Point(3, 57);
            this.btnDateFilterOK.Name = "btnDateFilterOK";
            this.btnDateFilterOK.Size = new System.Drawing.Size(35, 22);
            this.btnDateFilterOK.TabIndex = 18;
            this.btnDateFilterOK.Text = "OK";
            this.btnDateFilterOK.Click += new System.EventHandler(this.btnDateFilterOK_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(307, 51);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(170, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(41, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.Mask.EditMask = "g";
            this.deFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(108, 20);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "From Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Only Actions with a Date Raised between the From and To Date will be included.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.deFromDate.SuperTip = superToolTip1;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(192, 25);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.Mask.EditMask = "g";
            this.deToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(108, 20);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "To Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Only Actions with a Date Raised between the From and To Date will be included.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.deToDate.SuperTip = superToolTip2;
            this.deToDate.TabIndex = 7;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp07324UTAnalysisActionsForAnalysisBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldActionID1,
            this.fieldJobTypeDescription1,
            this.fieldReferenceNumber1,
            this.fieldDateRaised1,
            this.fieldDateScheduled1,
            this.fieldDateCompleted1,
            this.fieldApproved1,
            this.fieldEstimatedHours,
            this.fieldActualHours,
            this.fieldPossibleLiveWork,
            this.fieldEstimatedLabourCost1,
            this.fieldActualLabourCost1,
            this.fieldEstimatedLabourSell1,
            this.fieldActualLabourSell1,
            this.fieldEstimatedMaterialsCost1,
            this.fieldActualMaterialsCost1,
            this.fieldEstimatedMaterialsSell1,
            this.fieldActualMaterialsSell1,
            this.fieldEstimatedEquipmentCost1,
            this.fieldActualEquipmentCost1,
            this.fieldEstimatedEquipmentSell1,
            this.fieldActualEquipmentSell1,
            this.fieldEstimatedTotalCost1,
            this.fieldEstimatedTotalSell1,
            this.fieldActualTotalCost1,
            this.fieldActualTotalSell1,
            this.fieldWorkOrderID1,
            this.fieldSelfBillingInvoiceID1,
            this.fieldFinanceSystemBillingID1,
            this.fieldActionRemarks1,
            this.fieldCompletionSheetID1,
            this.fieldTotalTimeTaken1,
            this.fieldActionTeamRemarks1,
            this.fieldActionStatus1,
            this.fieldPoleNumber1,
            this.fieldLastInspectionDate1,
            this.fieldInspectionCycle1,
            this.fieldInspectionUnitDesc1,
            this.fieldNextInspectionDate1,
            this.fieldIsTransformer1,
            this.fieldTransformerNumber1,
            this.fieldRemarks1,
            this.fieldClientName1,
            this.fieldClientCode1,
            this.fieldCircuitStatus1,
            this.fieldCircuitName1,
            this.fieldCircuitNumber1,
            this.fieldVoltageType1,
            this.fieldFeederName1,
            this.fieldRegionName1,
            this.fieldPoleStatus1,
            this.fieldPrimaryName1,
            this.fieldSubAreaName1,
            this.fieldPoleType1,
            this.fieldLastInspectionElapsedDays1,
            this.fieldSurveyorName1,
            this.fieldActionCount1,
            this.fieldDateRaisedYear,
            this.fieldDateRaisedQuarter,
            this.fieldDateRaisedMonth,
            this.fieldDateRaisedWeek,
            this.fieldDateScheduledYear,
            this.fieldDateScheduledQuarter,
            this.fieldDateScheduledMonth,
            this.fieldDateScheduledWeek,
            this.fieldDateCompletedYear,
            this.fieldDateCompletedQuarter,
            this.fieldDateCompletedMonth,
            this.fieldDateCompletedWeek,
            this.fieldSurveyDate,
            this.fieldReactive,
            this.fieldSurveyDateYear,
            this.fieldSurveyDateQuarter,
            this.fieldSurveyDateMonth,
            this.fieldSurveyDateWeek,
            this.fieldLastInspectionDateYear,
            this.fieldLastInspectionDateQuarter,
            this.fieldLastInspectionDateMonth,
            this.fieldLastInspectionDateWeek,
            this.fieldNextInspectionDateYear,
            this.fieldNextInspectionDateQuarter,
            this.fieldNextInspectionDateMonth,
            this.fieldNextInspectionDateWeek,
            this.fieldPermissionCount,
            this.fieldPermissionCountAwaitingPermission,
            this.fieldPermissionCountPermissioned,
            this.fieldPermissionCountNotPermissioned,
            this.fieldPermissionCountOnHold,
            this.fieldPossibleFlail,
            this.fieldAchievableClearance,
            this.fieldStumpTreatmentNone,
            this.fieldStumpTreatmentEcoPlugs,
            this.fieldStumpTreatmentSpraying,
            this.fieldStumpTreatmentPaint,
            this.fieldTreeReplacementNone,
            this.fieldTreeReplacementWhips,
            this.fieldTreeReplacementStandards});
            pivotGridGroup1.Caption = "Date Raised Drill Down";
            pivotGridGroup1.Fields.Add(this.fieldDateRaisedYear);
            pivotGridGroup1.Fields.Add(this.fieldDateRaisedQuarter);
            pivotGridGroup1.Fields.Add(this.fieldDateRaisedMonth);
            pivotGridGroup1.Fields.Add(this.fieldDateRaisedWeek);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Date Scheduled Drill Down";
            pivotGridGroup2.Fields.Add(this.fieldDateScheduledYear);
            pivotGridGroup2.Fields.Add(this.fieldDateScheduledQuarter);
            pivotGridGroup2.Fields.Add(this.fieldDateScheduledMonth);
            pivotGridGroup2.Fields.Add(this.fieldDateScheduledWeek);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Date Completed Drill Down";
            pivotGridGroup3.Fields.Add(this.fieldDateCompletedQuarter);
            pivotGridGroup3.Fields.Add(this.fieldDateCompletedMonth);
            pivotGridGroup3.Fields.Add(this.fieldDateCompletedWeek);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            pivotGridGroup4.Caption = "Date Surveyed Drill Down";
            pivotGridGroup4.Fields.Add(this.fieldSurveyDateYear);
            pivotGridGroup4.Fields.Add(this.fieldSurveyDateQuarter);
            pivotGridGroup4.Fields.Add(this.fieldSurveyDateMonth);
            pivotGridGroup4.Fields.Add(this.fieldSurveyDateWeek);
            pivotGridGroup4.Hierarchy = null;
            pivotGridGroup4.ShowNewValues = true;
            pivotGridGroup5.Caption = "Last Inspection Date Drill Down";
            pivotGridGroup5.Fields.Add(this.fieldLastInspectionDateYear);
            pivotGridGroup5.Fields.Add(this.fieldLastInspectionDateQuarter);
            pivotGridGroup5.Fields.Add(this.fieldLastInspectionDateMonth);
            pivotGridGroup5.Fields.Add(this.fieldLastInspectionDateWeek);
            pivotGridGroup5.Hierarchy = null;
            pivotGridGroup5.ShowNewValues = true;
            pivotGridGroup6.Caption = "Next Inspection Date Drill Down";
            pivotGridGroup6.Fields.Add(this.fieldNextInspectionDateYear);
            pivotGridGroup6.Fields.Add(this.fieldNextInspectionDateQuarter);
            pivotGridGroup6.Fields.Add(this.fieldNextInspectionDateMonth);
            pivotGridGroup6.Fields.Add(this.fieldNextInspectionDateWeek);
            pivotGridGroup6.Hierarchy = null;
            pivotGridGroup6.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3,
            pivotGridGroup4,
            pivotGridGroup5,
            pivotGridGroup6});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.Size = new System.Drawing.Size(849, 388);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp07324UTAnalysisActionsForAnalysisBindingSource
            // 
            this.sp07324UTAnalysisActionsForAnalysisBindingSource.DataMember = "sp07324_UT_Analysis_Actions_For_Analysis";
            this.sp07324UTAnalysisActionsForAnalysisBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // fieldActionID1
            // 
            this.fieldActionID1.AreaIndex = 0;
            this.fieldActionID1.Caption = "Action ID";
            this.fieldActionID1.FieldName = "ActionID";
            this.fieldActionID1.Name = "fieldActionID1";
            this.fieldActionID1.Visible = false;
            // 
            // fieldJobTypeDescription1
            // 
            this.fieldJobTypeDescription1.AreaIndex = 0;
            this.fieldJobTypeDescription1.Caption = "Job Type";
            this.fieldJobTypeDescription1.FieldName = "JobTypeDescription";
            this.fieldJobTypeDescription1.Name = "fieldJobTypeDescription1";
            // 
            // fieldReferenceNumber1
            // 
            this.fieldReferenceNumber1.AreaIndex = 1;
            this.fieldReferenceNumber1.Caption = "Job Reference #";
            this.fieldReferenceNumber1.FieldName = "ReferenceNumber";
            this.fieldReferenceNumber1.Name = "fieldReferenceNumber1";
            // 
            // fieldDateRaised1
            // 
            this.fieldDateRaised1.AreaIndex = 2;
            this.fieldDateRaised1.Caption = "Date Raised";
            this.fieldDateRaised1.CellFormat.FormatString = "d";
            this.fieldDateRaised1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateRaised1.FieldName = "DateRaised";
            this.fieldDateRaised1.Name = "fieldDateRaised1";
            this.fieldDateRaised1.ValueFormat.FormatString = "d";
            this.fieldDateRaised1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldDateScheduled1
            // 
            this.fieldDateScheduled1.AreaIndex = 3;
            this.fieldDateScheduled1.Caption = "Date Scheduled";
            this.fieldDateScheduled1.CellFormat.FormatString = "d";
            this.fieldDateScheduled1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateScheduled1.FieldName = "DateScheduled";
            this.fieldDateScheduled1.Name = "fieldDateScheduled1";
            this.fieldDateScheduled1.ValueFormat.FormatString = "d";
            this.fieldDateScheduled1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldDateCompleted1
            // 
            this.fieldDateCompleted1.AreaIndex = 4;
            this.fieldDateCompleted1.Caption = "Date Completed";
            this.fieldDateCompleted1.CellFormat.FormatString = "d";
            this.fieldDateCompleted1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldDateCompleted1.FieldName = "DateCompleted";
            this.fieldDateCompleted1.Name = "fieldDateCompleted1";
            this.fieldDateCompleted1.ValueFormat.FormatString = "d";
            this.fieldDateCompleted1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldApproved1
            // 
            this.fieldApproved1.AreaIndex = 5;
            this.fieldApproved1.Caption = "Approved";
            this.fieldApproved1.FieldName = "Approved";
            this.fieldApproved1.Name = "fieldApproved1";
            // 
            // fieldEstimatedHours
            // 
            this.fieldEstimatedHours.AreaIndex = 8;
            this.fieldEstimatedHours.Caption = "Estimated Hours";
            this.fieldEstimatedHours.CellFormat.FormatString = "######0.00 Hours";
            this.fieldEstimatedHours.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedHours.FieldName = "EstimatedHours";
            this.fieldEstimatedHours.Name = "fieldEstimatedHours";
            this.fieldEstimatedHours.ValueFormat.FormatString = "######0.00 Hours";
            this.fieldEstimatedHours.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualHours
            // 
            this.fieldActualHours.AreaIndex = 9;
            this.fieldActualHours.Caption = "Actual Hours";
            this.fieldActualHours.CellFormat.FormatString = "######0.00 Hours";
            this.fieldActualHours.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualHours.FieldName = "ActualHours";
            this.fieldActualHours.Name = "fieldActualHours";
            this.fieldActualHours.ValueFormat.FormatString = "######0.00 Hours";
            this.fieldActualHours.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldPossibleLiveWork
            // 
            this.fieldPossibleLiveWork.AreaIndex = 10;
            this.fieldPossibleLiveWork.Caption = "Possible Live Work";
            this.fieldPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.fieldPossibleLiveWork.Name = "fieldPossibleLiveWork";
            // 
            // fieldEstimatedLabourCost1
            // 
            this.fieldEstimatedLabourCost1.AreaIndex = 11;
            this.fieldEstimatedLabourCost1.Caption = "Estimated Labour Cost";
            this.fieldEstimatedLabourCost1.CellFormat.FormatString = "c";
            this.fieldEstimatedLabourCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedLabourCost1.FieldName = "EstimatedLabourCost";
            this.fieldEstimatedLabourCost1.Name = "fieldEstimatedLabourCost1";
            this.fieldEstimatedLabourCost1.ValueFormat.FormatString = "c";
            this.fieldEstimatedLabourCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualLabourCost1
            // 
            this.fieldActualLabourCost1.AreaIndex = 12;
            this.fieldActualLabourCost1.Caption = "Actual Labour Cost";
            this.fieldActualLabourCost1.CellFormat.FormatString = "c";
            this.fieldActualLabourCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualLabourCost1.FieldName = "ActualLabourCost";
            this.fieldActualLabourCost1.Name = "fieldActualLabourCost1";
            this.fieldActualLabourCost1.ValueFormat.FormatString = "c";
            this.fieldActualLabourCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedLabourSell1
            // 
            this.fieldEstimatedLabourSell1.AreaIndex = 13;
            this.fieldEstimatedLabourSell1.Caption = "Estimated Labour Sell";
            this.fieldEstimatedLabourSell1.CellFormat.FormatString = "c";
            this.fieldEstimatedLabourSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedLabourSell1.FieldName = "EstimatedLabourSell";
            this.fieldEstimatedLabourSell1.Name = "fieldEstimatedLabourSell1";
            this.fieldEstimatedLabourSell1.ValueFormat.FormatString = "c";
            this.fieldEstimatedLabourSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualLabourSell1
            // 
            this.fieldActualLabourSell1.AreaIndex = 14;
            this.fieldActualLabourSell1.Caption = "Actual Labour Sell";
            this.fieldActualLabourSell1.CellFormat.FormatString = "c";
            this.fieldActualLabourSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualLabourSell1.FieldName = "ActualLabourSell";
            this.fieldActualLabourSell1.Name = "fieldActualLabourSell1";
            this.fieldActualLabourSell1.ValueFormat.FormatString = "c";
            this.fieldActualLabourSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedMaterialsCost1
            // 
            this.fieldEstimatedMaterialsCost1.AreaIndex = 15;
            this.fieldEstimatedMaterialsCost1.Caption = "Estimated Materials Cost";
            this.fieldEstimatedMaterialsCost1.CellFormat.FormatString = "c";
            this.fieldEstimatedMaterialsCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedMaterialsCost1.FieldName = "EstimatedMaterialsCost";
            this.fieldEstimatedMaterialsCost1.Name = "fieldEstimatedMaterialsCost1";
            this.fieldEstimatedMaterialsCost1.ValueFormat.FormatString = "c";
            this.fieldEstimatedMaterialsCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualMaterialsCost1
            // 
            this.fieldActualMaterialsCost1.AreaIndex = 16;
            this.fieldActualMaterialsCost1.Caption = "Actual Materials Cost";
            this.fieldActualMaterialsCost1.CellFormat.FormatString = "c";
            this.fieldActualMaterialsCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualMaterialsCost1.FieldName = "ActualMaterialsCost";
            this.fieldActualMaterialsCost1.Name = "fieldActualMaterialsCost1";
            this.fieldActualMaterialsCost1.ValueFormat.FormatString = "c";
            this.fieldActualMaterialsCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedMaterialsSell1
            // 
            this.fieldEstimatedMaterialsSell1.AreaIndex = 17;
            this.fieldEstimatedMaterialsSell1.Caption = "Estimated Materials Sell";
            this.fieldEstimatedMaterialsSell1.CellFormat.FormatString = "c";
            this.fieldEstimatedMaterialsSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedMaterialsSell1.FieldName = "EstimatedMaterialsSell";
            this.fieldEstimatedMaterialsSell1.Name = "fieldEstimatedMaterialsSell1";
            this.fieldEstimatedMaterialsSell1.ValueFormat.FormatString = "c";
            this.fieldEstimatedMaterialsSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualMaterialsSell1
            // 
            this.fieldActualMaterialsSell1.AreaIndex = 18;
            this.fieldActualMaterialsSell1.Caption = "Actual Materials Sell";
            this.fieldActualMaterialsSell1.CellFormat.FormatString = "c";
            this.fieldActualMaterialsSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualMaterialsSell1.FieldName = "ActualMaterialsSell";
            this.fieldActualMaterialsSell1.Name = "fieldActualMaterialsSell1";
            this.fieldActualMaterialsSell1.ValueFormat.FormatString = "c";
            this.fieldActualMaterialsSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedEquipmentCost1
            // 
            this.fieldEstimatedEquipmentCost1.AreaIndex = 19;
            this.fieldEstimatedEquipmentCost1.Caption = "Estimated Equipment Cost";
            this.fieldEstimatedEquipmentCost1.CellFormat.FormatString = "c";
            this.fieldEstimatedEquipmentCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedEquipmentCost1.FieldName = "EstimatedEquipmentCost";
            this.fieldEstimatedEquipmentCost1.Name = "fieldEstimatedEquipmentCost1";
            this.fieldEstimatedEquipmentCost1.ValueFormat.FormatString = "c";
            this.fieldEstimatedEquipmentCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualEquipmentCost1
            // 
            this.fieldActualEquipmentCost1.AreaIndex = 20;
            this.fieldActualEquipmentCost1.Caption = "Actual Equipment Cost";
            this.fieldActualEquipmentCost1.CellFormat.FormatString = "c";
            this.fieldActualEquipmentCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualEquipmentCost1.FieldName = "ActualEquipmentCost";
            this.fieldActualEquipmentCost1.Name = "fieldActualEquipmentCost1";
            this.fieldActualEquipmentCost1.ValueFormat.FormatString = "c";
            this.fieldActualEquipmentCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedEquipmentSell1
            // 
            this.fieldEstimatedEquipmentSell1.AreaIndex = 21;
            this.fieldEstimatedEquipmentSell1.Caption = "Estimated Equipment Sell";
            this.fieldEstimatedEquipmentSell1.CellFormat.FormatString = "c";
            this.fieldEstimatedEquipmentSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedEquipmentSell1.FieldName = "EstimatedEquipmentSell";
            this.fieldEstimatedEquipmentSell1.Name = "fieldEstimatedEquipmentSell1";
            this.fieldEstimatedEquipmentSell1.ValueFormat.FormatString = "c";
            this.fieldEstimatedEquipmentSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualEquipmentSell1
            // 
            this.fieldActualEquipmentSell1.AreaIndex = 22;
            this.fieldActualEquipmentSell1.Caption = "Actual Equipment Sell";
            this.fieldActualEquipmentSell1.CellFormat.FormatString = "c";
            this.fieldActualEquipmentSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualEquipmentSell1.FieldName = "ActualEquipmentSell";
            this.fieldActualEquipmentSell1.Name = "fieldActualEquipmentSell1";
            this.fieldActualEquipmentSell1.ValueFormat.FormatString = "c";
            this.fieldActualEquipmentSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedTotalCost1
            // 
            this.fieldEstimatedTotalCost1.AreaIndex = 23;
            this.fieldEstimatedTotalCost1.Caption = "Estimated Total Cost";
            this.fieldEstimatedTotalCost1.CellFormat.FormatString = "c";
            this.fieldEstimatedTotalCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedTotalCost1.FieldName = "EstimatedTotalCost";
            this.fieldEstimatedTotalCost1.Name = "fieldEstimatedTotalCost1";
            this.fieldEstimatedTotalCost1.ValueFormat.FormatString = "c";
            this.fieldEstimatedTotalCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldEstimatedTotalSell1
            // 
            this.fieldEstimatedTotalSell1.AreaIndex = 24;
            this.fieldEstimatedTotalSell1.Caption = "Estimated Total Sell";
            this.fieldEstimatedTotalSell1.CellFormat.FormatString = "c";
            this.fieldEstimatedTotalSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedTotalSell1.FieldName = "EstimatedTotalSell";
            this.fieldEstimatedTotalSell1.Name = "fieldEstimatedTotalSell1";
            this.fieldEstimatedTotalSell1.ValueFormat.FormatString = "c";
            this.fieldEstimatedTotalSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualTotalCost1
            // 
            this.fieldActualTotalCost1.AreaIndex = 25;
            this.fieldActualTotalCost1.Caption = "Actual Total Cost";
            this.fieldActualTotalCost1.CellFormat.FormatString = "c";
            this.fieldActualTotalCost1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualTotalCost1.FieldName = "ActualTotalCost";
            this.fieldActualTotalCost1.Name = "fieldActualTotalCost1";
            this.fieldActualTotalCost1.ValueFormat.FormatString = "c";
            this.fieldActualTotalCost1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualTotalSell1
            // 
            this.fieldActualTotalSell1.AreaIndex = 26;
            this.fieldActualTotalSell1.Caption = "Actual Total Sell";
            this.fieldActualTotalSell1.CellFormat.FormatString = "c";
            this.fieldActualTotalSell1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualTotalSell1.FieldName = "ActualTotalSell";
            this.fieldActualTotalSell1.Name = "fieldActualTotalSell1";
            this.fieldActualTotalSell1.ValueFormat.FormatString = "c";
            this.fieldActualTotalSell1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldWorkOrderID1
            // 
            this.fieldWorkOrderID1.AreaIndex = 28;
            this.fieldWorkOrderID1.Caption = "Work Order ID";
            this.fieldWorkOrderID1.FieldName = "WorkOrderID";
            this.fieldWorkOrderID1.Name = "fieldWorkOrderID1";
            this.fieldWorkOrderID1.Visible = false;
            // 
            // fieldSelfBillingInvoiceID1
            // 
            this.fieldSelfBillingInvoiceID1.AreaIndex = 28;
            this.fieldSelfBillingInvoiceID1.Caption = "Self Billing Invoice ID";
            this.fieldSelfBillingInvoiceID1.FieldName = "SelfBillingInvoiceID";
            this.fieldSelfBillingInvoiceID1.Name = "fieldSelfBillingInvoiceID1";
            this.fieldSelfBillingInvoiceID1.Visible = false;
            // 
            // fieldFinanceSystemBillingID1
            // 
            this.fieldFinanceSystemBillingID1.AreaIndex = 28;
            this.fieldFinanceSystemBillingID1.Caption = "Finance System Billing ID";
            this.fieldFinanceSystemBillingID1.FieldName = "FinanceSystemBillingID";
            this.fieldFinanceSystemBillingID1.Name = "fieldFinanceSystemBillingID1";
            this.fieldFinanceSystemBillingID1.Visible = false;
            // 
            // fieldActionRemarks1
            // 
            this.fieldActionRemarks1.AreaIndex = 28;
            this.fieldActionRemarks1.Caption = "Action Remarks";
            this.fieldActionRemarks1.FieldName = "ActionRemarks";
            this.fieldActionRemarks1.Name = "fieldActionRemarks1";
            this.fieldActionRemarks1.Visible = false;
            // 
            // fieldCompletionSheetID1
            // 
            this.fieldCompletionSheetID1.AreaIndex = 29;
            this.fieldCompletionSheetID1.Caption = "Completion Sheet ID";
            this.fieldCompletionSheetID1.FieldName = "CompletionSheetID";
            this.fieldCompletionSheetID1.Name = "fieldCompletionSheetID1";
            this.fieldCompletionSheetID1.Visible = false;
            // 
            // fieldTotalTimeTaken1
            // 
            this.fieldTotalTimeTaken1.AreaIndex = 27;
            this.fieldTotalTimeTaken1.Caption = "Total Time Taken";
            this.fieldTotalTimeTaken1.CellFormat.FormatString = "######0.00 Hours";
            this.fieldTotalTimeTaken1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldTotalTimeTaken1.FieldName = "TotalTimeTaken";
            this.fieldTotalTimeTaken1.Name = "fieldTotalTimeTaken1";
            this.fieldTotalTimeTaken1.ValueFormat.FormatString = "######0.00 Hours";
            this.fieldTotalTimeTaken1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActionTeamRemarks1
            // 
            this.fieldActionTeamRemarks1.AreaIndex = 29;
            this.fieldActionTeamRemarks1.Caption = "Action Team Remarks";
            this.fieldActionTeamRemarks1.FieldName = "ActionTeamRemarks";
            this.fieldActionTeamRemarks1.Name = "fieldActionTeamRemarks1";
            this.fieldActionTeamRemarks1.Visible = false;
            // 
            // fieldActionStatus1
            // 
            this.fieldActionStatus1.AreaIndex = 28;
            this.fieldActionStatus1.Caption = "Action Status";
            this.fieldActionStatus1.FieldName = "ActionStatus";
            this.fieldActionStatus1.Name = "fieldActionStatus1";
            // 
            // fieldPoleNumber1
            // 
            this.fieldPoleNumber1.AreaIndex = 38;
            this.fieldPoleNumber1.Caption = "Pole #";
            this.fieldPoleNumber1.FieldName = "PoleNumber";
            this.fieldPoleNumber1.Name = "fieldPoleNumber1";
            // 
            // fieldLastInspectionDate1
            // 
            this.fieldLastInspectionDate1.AreaIndex = 42;
            this.fieldLastInspectionDate1.Caption = "Last Inspection Date";
            this.fieldLastInspectionDate1.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDate1.Name = "fieldLastInspectionDate1";
            this.fieldLastInspectionDate1.ValueFormat.FormatString = "d";
            this.fieldLastInspectionDate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldInspectionCycle1
            // 
            this.fieldInspectionCycle1.AreaIndex = 43;
            this.fieldInspectionCycle1.Caption = "Inspection Cycle";
            this.fieldInspectionCycle1.CellFormat.FormatString = "n";
            this.fieldInspectionCycle1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldInspectionCycle1.FieldName = "InspectionCycle";
            this.fieldInspectionCycle1.Name = "fieldInspectionCycle1";
            this.fieldInspectionCycle1.ValueFormat.FormatString = "n";
            this.fieldInspectionCycle1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldInspectionUnitDesc1
            // 
            this.fieldInspectionUnitDesc1.AreaIndex = 44;
            this.fieldInspectionUnitDesc1.Caption = "Inspection Unit Desc";
            this.fieldInspectionUnitDesc1.FieldName = "InspectionUnitDesc";
            this.fieldInspectionUnitDesc1.Name = "fieldInspectionUnitDesc1";
            // 
            // fieldNextInspectionDate1
            // 
            this.fieldNextInspectionDate1.AreaIndex = 45;
            this.fieldNextInspectionDate1.Caption = "Next Inspection Date";
            this.fieldNextInspectionDate1.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDate1.Name = "fieldNextInspectionDate1";
            this.fieldNextInspectionDate1.ValueFormat.FormatString = "d";
            this.fieldNextInspectionDate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldIsTransformer1
            // 
            this.fieldIsTransformer1.AreaIndex = 47;
            this.fieldIsTransformer1.Caption = "Is Transformer";
            this.fieldIsTransformer1.FieldName = "IsTransformer";
            this.fieldIsTransformer1.Name = "fieldIsTransformer1";
            // 
            // fieldTransformerNumber1
            // 
            this.fieldTransformerNumber1.AreaIndex = 48;
            this.fieldTransformerNumber1.Caption = "Transformer #";
            this.fieldTransformerNumber1.FieldName = "TransformerNumber";
            this.fieldTransformerNumber1.Name = "fieldTransformerNumber1";
            // 
            // fieldRemarks1
            // 
            this.fieldRemarks1.AreaIndex = 39;
            this.fieldRemarks1.Caption = "Pole Remarks";
            this.fieldRemarks1.FieldName = "Remarks";
            this.fieldRemarks1.Name = "fieldRemarks1";
            this.fieldRemarks1.Visible = false;
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 29;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            // 
            // fieldClientCode1
            // 
            this.fieldClientCode1.AreaIndex = 30;
            this.fieldClientCode1.Caption = "Client Code";
            this.fieldClientCode1.FieldName = "ClientCode";
            this.fieldClientCode1.Name = "fieldClientCode1";
            // 
            // fieldCircuitStatus1
            // 
            this.fieldCircuitStatus1.AreaIndex = 37;
            this.fieldCircuitStatus1.Caption = "Circuit Status";
            this.fieldCircuitStatus1.FieldName = "CircuitStatus";
            this.fieldCircuitStatus1.Name = "fieldCircuitStatus1";
            // 
            // fieldCircuitName1
            // 
            this.fieldCircuitName1.AreaIndex = 35;
            this.fieldCircuitName1.Caption = "Circuit Name";
            this.fieldCircuitName1.FieldName = "CircuitName";
            this.fieldCircuitName1.Name = "fieldCircuitName1";
            // 
            // fieldCircuitNumber1
            // 
            this.fieldCircuitNumber1.AreaIndex = 36;
            this.fieldCircuitNumber1.Caption = "Circuit #";
            this.fieldCircuitNumber1.FieldName = "CircuitNumber";
            this.fieldCircuitNumber1.Name = "fieldCircuitNumber1";
            // 
            // fieldVoltageType1
            // 
            this.fieldVoltageType1.AreaIndex = 41;
            this.fieldVoltageType1.Caption = "Voltage Type";
            this.fieldVoltageType1.FieldName = "VoltageType";
            this.fieldVoltageType1.Name = "fieldVoltageType1";
            // 
            // fieldFeederName1
            // 
            this.fieldFeederName1.AreaIndex = 34;
            this.fieldFeederName1.Caption = "Feeder Name";
            this.fieldFeederName1.FieldName = "FeederName";
            this.fieldFeederName1.Name = "fieldFeederName1";
            // 
            // fieldRegionName1
            // 
            this.fieldRegionName1.AreaIndex = 31;
            this.fieldRegionName1.Caption = "Region Name";
            this.fieldRegionName1.FieldName = "RegionName";
            this.fieldRegionName1.Name = "fieldRegionName1";
            // 
            // fieldPoleStatus1
            // 
            this.fieldPoleStatus1.AreaIndex = 39;
            this.fieldPoleStatus1.Caption = "Pole Status";
            this.fieldPoleStatus1.FieldName = "PoleStatus";
            this.fieldPoleStatus1.Name = "fieldPoleStatus1";
            // 
            // fieldPrimaryName1
            // 
            this.fieldPrimaryName1.AreaIndex = 33;
            this.fieldPrimaryName1.Caption = "Primary Name";
            this.fieldPrimaryName1.FieldName = "PrimaryName";
            this.fieldPrimaryName1.Name = "fieldPrimaryName1";
            // 
            // fieldSubAreaName1
            // 
            this.fieldSubAreaName1.AreaIndex = 32;
            this.fieldSubAreaName1.Caption = "Sub Area Name";
            this.fieldSubAreaName1.FieldName = "SubAreaName";
            this.fieldSubAreaName1.Name = "fieldSubAreaName1";
            // 
            // fieldPoleType1
            // 
            this.fieldPoleType1.AreaIndex = 40;
            this.fieldPoleType1.Caption = "Pole Type";
            this.fieldPoleType1.FieldName = "PoleType";
            this.fieldPoleType1.Name = "fieldPoleType1";
            // 
            // fieldLastInspectionElapsedDays1
            // 
            this.fieldLastInspectionElapsedDays1.AreaIndex = 46;
            this.fieldLastInspectionElapsedDays1.Caption = "Last Inspection Elapsed Days";
            this.fieldLastInspectionElapsedDays1.FieldName = "LastInspectionElapsedDays";
            this.fieldLastInspectionElapsedDays1.Name = "fieldLastInspectionElapsedDays1";
            // 
            // fieldSurveyorName1
            // 
            this.fieldSurveyorName1.AreaIndex = 49;
            this.fieldSurveyorName1.Caption = "Surveyor Name";
            this.fieldSurveyorName1.FieldName = "SurveyorName";
            this.fieldSurveyorName1.Name = "fieldSurveyorName1";
            // 
            // fieldActionCount1
            // 
            this.fieldActionCount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldActionCount1.AreaIndex = 0;
            this.fieldActionCount1.Caption = "Action Count";
            this.fieldActionCount1.FieldName = "ActionCount";
            this.fieldActionCount1.Name = "fieldActionCount1";
            // 
            // fieldDateCompletedYear
            // 
            this.fieldDateCompletedYear.AreaIndex = 54;
            this.fieldDateCompletedYear.Caption = "Year Completed";
            this.fieldDateCompletedYear.ExpandedInFieldsGroup = false;
            this.fieldDateCompletedYear.FieldName = "DateCompleted";
            this.fieldDateCompletedYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldDateCompletedYear.Name = "fieldDateCompletedYear";
            this.fieldDateCompletedYear.UnboundFieldName = "fieldDateCompletedYear";
            // 
            // fieldSurveyDate
            // 
            this.fieldSurveyDate.AreaIndex = 50;
            this.fieldSurveyDate.Caption = "Survey Date";
            this.fieldSurveyDate.CellFormat.FormatString = "d";
            this.fieldSurveyDate.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.fieldSurveyDate.FieldName = "SurveyDate";
            this.fieldSurveyDate.Name = "fieldSurveyDate";
            this.fieldSurveyDate.ValueFormat.FormatString = "d";
            this.fieldSurveyDate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldReactive
            // 
            this.fieldReactive.AreaIndex = 51;
            this.fieldReactive.Caption = "Reactive";
            this.fieldReactive.FieldName = "Reactive";
            this.fieldReactive.Name = "fieldReactive";
            // 
            // fieldPermissionCount
            // 
            this.fieldPermissionCount.AreaIndex = 56;
            this.fieldPermissionCount.Caption = "Total Permission Count";
            this.fieldPermissionCount.FieldName = "PermissionCount";
            this.fieldPermissionCount.Name = "fieldPermissionCount";
            // 
            // fieldPermissionCountAwaitingPermission
            // 
            this.fieldPermissionCountAwaitingPermission.AreaIndex = 57;
            this.fieldPermissionCountAwaitingPermission.Caption = "Awaiting Permission Count";
            this.fieldPermissionCountAwaitingPermission.FieldName = "PermissionCountAwaitingPermission";
            this.fieldPermissionCountAwaitingPermission.Name = "fieldPermissionCountAwaitingPermission";
            // 
            // fieldPermissionCountPermissioned
            // 
            this.fieldPermissionCountPermissioned.AreaIndex = 58;
            this.fieldPermissionCountPermissioned.Caption = "Permissioned Count";
            this.fieldPermissionCountPermissioned.FieldName = "PermissionCountPermissioned";
            this.fieldPermissionCountPermissioned.Name = "fieldPermissionCountPermissioned";
            // 
            // fieldPermissionCountNotPermissioned
            // 
            this.fieldPermissionCountNotPermissioned.AreaIndex = 59;
            this.fieldPermissionCountNotPermissioned.Caption = "Not Permissioned Count";
            this.fieldPermissionCountNotPermissioned.FieldName = "PermissionCountNotPermissioned";
            this.fieldPermissionCountNotPermissioned.Name = "fieldPermissionCountNotPermissioned";
            // 
            // fieldPermissionCountOnHold
            // 
            this.fieldPermissionCountOnHold.AreaIndex = 60;
            this.fieldPermissionCountOnHold.Caption = "On-Hold Permission Count";
            this.fieldPermissionCountOnHold.FieldName = "PermissionCountOnHold";
            this.fieldPermissionCountOnHold.Name = "fieldPermissionCountOnHold";
            // 
            // fieldPossibleFlail
            // 
            this.fieldPossibleFlail.AreaIndex = 6;
            this.fieldPossibleFlail.Caption = "Possible Flail";
            this.fieldPossibleFlail.FieldName = "PossibleFlail";
            this.fieldPossibleFlail.Name = "fieldPossibleFlail";
            // 
            // fieldAchievableClearance
            // 
            this.fieldAchievableClearance.AreaIndex = 7;
            this.fieldAchievableClearance.Caption = "Achievable Clearance";
            this.fieldAchievableClearance.FieldName = "AchievableClearance";
            this.fieldAchievableClearance.Name = "fieldAchievableClearance";
            // 
            // fieldStumpTreatmentNone
            // 
            this.fieldStumpTreatmentNone.AreaIndex = 61;
            this.fieldStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.fieldStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.fieldStumpTreatmentNone.Name = "fieldStumpTreatmentNone";
            // 
            // fieldStumpTreatmentEcoPlugs
            // 
            this.fieldStumpTreatmentEcoPlugs.AreaIndex = 62;
            this.fieldStumpTreatmentEcoPlugs.Caption = "Stemp Treatment - Eco Plugs";
            this.fieldStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.fieldStumpTreatmentEcoPlugs.Name = "fieldStumpTreatmentEcoPlugs";
            // 
            // fieldStumpTreatmentSpraying
            // 
            this.fieldStumpTreatmentSpraying.AreaIndex = 63;
            this.fieldStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.fieldStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.fieldStumpTreatmentSpraying.Name = "fieldStumpTreatmentSpraying";
            // 
            // fieldStumpTreatmentPaint
            // 
            this.fieldStumpTreatmentPaint.AreaIndex = 64;
            this.fieldStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.fieldStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.fieldStumpTreatmentPaint.Name = "fieldStumpTreatmentPaint";
            // 
            // fieldTreeReplacementNone
            // 
            this.fieldTreeReplacementNone.AreaIndex = 65;
            this.fieldTreeReplacementNone.Caption = "Tree Replacement - None";
            this.fieldTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.fieldTreeReplacementNone.Name = "fieldTreeReplacementNone";
            // 
            // fieldTreeReplacementWhips
            // 
            this.fieldTreeReplacementWhips.AreaIndex = 66;
            this.fieldTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.fieldTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.fieldTreeReplacementWhips.Name = "fieldTreeReplacementWhips";
            // 
            // fieldTreeReplacementStandards
            // 
            this.fieldTreeReplacementStandards.AreaIndex = 67;
            this.fieldTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.fieldTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.fieldTreeReplacementStandards.Name = "fieldTreeReplacementStandards";
            // 
            // chartControl1
            // 
            this.chartControl1.DataSource = this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.Size = new System.Drawing.Size(849, 100);
            this.chartControl1.TabIndex = 1;
            // 
            // sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter
            // 
            this.sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter.ClearBeforeFill = true;
            // 
            // sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter
            // 
            this.sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07322_UT_Master_Job_TypesTableAdapter
            // 
            this.sp07322_UT_Master_Job_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToggleAvailableColumnsVisibility, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiToggleAvailableColumnsVisibility
            // 
            this.bbiToggleAvailableColumnsVisibility.Caption = "Toggle Available Columns Visibility";
            this.bbiToggleAvailableColumnsVisibility.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiToggleAvailableColumnsVisibility.Glyph")));
            this.bbiToggleAvailableColumnsVisibility.Id = 32;
            this.bbiToggleAvailableColumnsVisibility.Name = "bbiToggleAvailableColumnsVisibility";
            this.bbiToggleAvailableColumnsVisibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToggleAvailableColumnsVisibility_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(580, 206);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonEditFilterCircuits),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemAction),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemDateFilter
            // 
            this.barEditItemDateFilter.Caption = "Date Filter";
            this.barEditItemDateFilter.Edit = this.repositoryItemPopupContainerEditDateFilter;
            this.barEditItemDateFilter.EditValue = "No Date Filter";
            this.barEditItemDateFilter.EditWidth = 97;
            this.barEditItemDateFilter.Id = 29;
            this.barEditItemDateFilter.Name = "barEditItemDateFilter";
            // 
            // repositoryItemPopupContainerEditDateFilter
            // 
            this.repositoryItemPopupContainerEditDateFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateFilter.Name = "repositoryItemPopupContainerEditDateFilter";
            this.repositoryItemPopupContainerEditDateFilter.PopupControl = this.popupContainerControlDateFilter;
            this.repositoryItemPopupContainerEditDateFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditDateFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateFilter_QueryResultValue);
            // 
            // buttonEditFilterCircuits
            // 
            this.buttonEditFilterCircuits.Caption = "Circuit Filter";
            this.buttonEditFilterCircuits.Edit = this.repositoryItemButtonEditFilterCircuits;
            this.buttonEditFilterCircuits.EditValue = "No Circuit Filter";
            this.buttonEditFilterCircuits.EditWidth = 130;
            this.buttonEditFilterCircuits.Id = 34;
            this.buttonEditFilterCircuits.Name = "buttonEditFilterCircuits";
            // 
            // repositoryItemButtonEditFilterCircuits
            // 
            this.repositoryItemButtonEditFilterCircuits.AutoHeight = false;
            this.repositoryItemButtonEditFilterCircuits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "choose", null, true)});
            this.repositoryItemButtonEditFilterCircuits.Name = "repositoryItemButtonEditFilterCircuits";
            this.repositoryItemButtonEditFilterCircuits.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditFilterCircuits.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFilterCircuits_ButtonClick);
            // 
            // barEditItemAction
            // 
            this.barEditItemAction.Caption = "Action Filter";
            this.barEditItemAction.Edit = this.repositoryItemPopupContainerEditActionFilter;
            this.barEditItemAction.EditValue = "No Action Filter";
            this.barEditItemAction.EditWidth = 102;
            this.barEditItemAction.Id = 35;
            this.barEditItemAction.Name = "barEditItemAction";
            // 
            // repositoryItemPopupContainerEditActionFilter
            // 
            this.repositoryItemPopupContainerEditActionFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditActionFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditActionFilter.Name = "repositoryItemPopupContainerEditActionFilter";
            this.repositoryItemPopupContainerEditActionFilter.PopupControl = this.popupContainerControlActionFilter;
            this.repositoryItemPopupContainerEditActionFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditActionFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditActionFilter_QueryResultValue);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Caption = "Reload";
            this.bbiReloadData.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiReloadData.Glyph")));
            this.bbiReloadData.Id = 30;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Reload Data - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReloadData.SuperTip = superToolTip3;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // bbiAnalyse
            // 
            this.bbiAnalyse.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAnalyse.Caption = "Analyse";
            this.bbiAnalyse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAnalyse.Glyph")));
            this.bbiAnalyse.Id = 31;
            this.bbiAnalyse.Name = "bbiAnalyse";
            this.bbiAnalyse.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "Analyse - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the Analysis Grid and Chart with the records selected in the Dat" +
    "a Supply list.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAnalyse.SuperTip = superToolTip4;
            // 
            // barEditItemStatusFilter
            // 
            this.barEditItemStatusFilter.Caption = "Status Filter";
            this.barEditItemStatusFilter.Edit = this.repositoryItemPopupContainerEditStatusFilter;
            this.barEditItemStatusFilter.EditValue = "All Statuses";
            this.barEditItemStatusFilter.EditWidth = 83;
            this.barEditItemStatusFilter.Id = 28;
            this.barEditItemStatusFilter.Name = "barEditItemStatusFilter";
            // 
            // repositoryItemPopupContainerEditStatusFilter
            // 
            this.repositoryItemPopupContainerEditStatusFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditStatusFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStatusFilter.Name = "repositoryItemPopupContainerEditStatusFilter";
            this.repositoryItemPopupContainerEditStatusFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditStatusFilter.ShowPopupCloseButton = false;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit2;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Analysis_Actions
            // 
            this.ClientSize = new System.Drawing.Size(1293, 494);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Analysis_Actions";
            this.Text = "Utilities - Action Analysis";
            this.Activated += new System.EventHandler(this.frm_UT_Analysis_Actions_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Analysis_Actions_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_UT_Analysis_Actions_FormClosed);
            this.Load += new System.EventHandler(this.frm_UT_Analysis_Actions_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07323UTAnalysisActionsAvailableActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlActionFilter)).EndInit();
            this.popupContainerControlActionFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07322UTMasterJobTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).EndInit();
            this.popupContainerControlDateFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07324UTAnalysisActionsForAnalysisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditActionFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStatusFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateFilter;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraBars.BarButtonItem bbiAnalyse;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateFilter;
        private DevExpress.XtraEditors.SimpleButton btnDateFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraBars.BarButtonItem bbiToggleAvailableColumnsVisibility;
        private DevExpress.XtraBars.BarEditItem buttonEditFilterCircuits;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditFilterCircuits;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.SimpleButton btnAnalyse;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedCount;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnSurvey;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveySpanClear;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyorName;
        private DevExpress.XtraBars.BarEditItem barEditItemAction;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditActionFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlActionFilter;
        private DevExpress.XtraEditors.SimpleButton ActionFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DataSet_UT_ReportingTableAdapters.sp07322_UT_Master_Job_TypesTableAdapter sp07322_UT_Master_Job_TypesTableAdapter;
        private System.Windows.Forms.BindingSource sp07322UTMasterJobTypesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colJobID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCode;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DataSet_UT_ReportingTableAdapters.sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter;
        private System.Windows.Forms.BindingSource sp07323UTAnalysisActionsAvailableActionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionTeamRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private System.Windows.Forms.BindingSource sp07324UTAnalysisActionsForAnalysisBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldJobTypeDescription1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReferenceNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaised1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduled1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompleted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldApproved1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedHours;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualHours;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPossibleLiveWork;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedLabourCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualLabourCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedLabourSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualLabourSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedMaterialsCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualMaterialsCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedMaterialsSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualMaterialsSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedEquipmentCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualEquipmentCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedEquipmentSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualEquipmentSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedTotalCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedTotalSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualTotalCost1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualTotalSell1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldWorkOrderID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSelfBillingInvoiceID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFinanceSystemBillingID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCompletionSheetID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTotalTimeTaken1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionTeamRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCycle1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUnitDesc1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsTransformer1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTransformerNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientCode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVoltageType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFeederName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRegionName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPrimaryName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubAreaName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionElapsedDays1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateRaisedWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateScheduledWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDateCompletedWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDate;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReactive;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDateWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCountAwaitingPermission;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCountPermissioned;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCountNotPermissioned;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPermissionCountOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleFlail;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPossibleFlail;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentNone;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentEcoPlugs;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentSpraying;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStumpTreatmentPaint;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementNone;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementWhips;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

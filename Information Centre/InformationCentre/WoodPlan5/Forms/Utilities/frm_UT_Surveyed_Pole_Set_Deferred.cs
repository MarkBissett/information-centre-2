﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Surveyed_Pole_Set_Deferred : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intRecordCount = 0;

        public int _Units = 0;
        public int _UnitDescriptorID = 0;
        public int _ReasonID = 0;
        public DateTime _RevisitDate = DateTime.MinValue;
        public string _Remarks = "";
        #endregion

        public frm_UT_Surveyed_Pole_Set_Deferred()
        {
            InitializeComponent();
        }

        private void frm_UT_Surveyed_Pole_Set_Deferred_Load(object sender, EventArgs e)
        {
            this.FormID = 500074;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            barStaticItemInformation.Caption = "Records Selected For Update: " + intRecordCount.ToString();
            
            sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07357_UT_Deferral_Reasons_With_Blank);

            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07046_UT_Inspection_Cycles_With_Blank);          

            DeferredUnitsSpinEdit.EditValue = 5;
            DeferredUnitDescriptiorIDGridLookUpEdit.EditValue = 4;
            DeferralReasonIDGridLookUpEdit.EditValue = 0;
            Calculate_Revisit_Date();

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        #region Editors

        private void DeferredUnitsSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (Convert.ToInt32(se.EditValue) <= 0)
            {
                dxErrorProvider1.SetError(DeferredUnitsSpinEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DeferredUnitsSpinEdit, "");
            }
        }
        private void DeferredUnitsSpinEdit_Validated(object sender, EventArgs e)
        {
            Calculate_Revisit_Date();
        }

        private void DeferredUnitDescriptiorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (Convert.ToInt32(glue.EditValue) <= 0)
            {
                dxErrorProvider1.SetError(DeferredUnitDescriptiorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DeferredUnitDescriptiorIDGridLookUpEdit, "");
            }
        }
        private void DeferredUnitDescriptiorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Calculate_Revisit_Date();
        }
        
        private void Calculate_Revisit_Date()
        {
            int DeferredUnits = Convert.ToInt32(DeferredUnitsSpinEdit.EditValue);
            int DeferredUnitDescriptiorID = Convert.ToInt32(DeferredUnitDescriptiorIDGridLookUpEdit.EditValue);

            DateTime CurrentDate = DateTime.Today;
            DateTime NextDate = DateTime.MinValue;
            if (DeferredUnits > 0 && DeferredUnitDescriptiorID > 0)
            {
                switch (DeferredUnitDescriptiorID)
                {
                    case 1:  // Days //
                        NextDate = CurrentDate.AddDays((double)DeferredUnits);
                        break;
                    case 2:  // Weeks //
                        NextDate = CurrentDate.AddDays((double)DeferredUnits * 7);
                        break;
                    case 3:  // Months //
                        NextDate = CurrentDate.AddMonths(DeferredUnits);
                        break;
                    case 4:  // Years //
                        NextDate = CurrentDate.AddYears(DeferredUnits);
                        break;
                    default:
                        break;
                }
                if (NextDate != DateTime.MinValue)
                {
                    RevisitDateDateEdit.EditValue = NextDate;
                }
                else
                {
                    RevisitDateDateEdit.EditValue = DBNull.Value;
                }
            }
            else
            {
                RevisitDateDateEdit.EditValue = DBNull.Value;
            }
            //this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }

        private void RevisitDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "");
            }
        }

        private void DeferralReasonIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (Convert.ToInt32(glue.EditValue) <= 0)
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "");
            }
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\nTip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Set Surveyed Poles As Deferred", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(DeferredUnitsSpinEdit.EditValue) <= 0 || Convert.ToInt32(DeferredUnitDescriptiorIDGridLookUpEdit.EditValue) <= 0 || Convert.ToInt32(DeferralReasonIDGridLookUpEdit.EditValue) <= 0)
            {
                XtraMessageBox.Show("Ensure the Deferred Units, Unit Descriptor and Reason all have a value before proceeding.", "Set Surveyed Poles As Deferred", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            _Units = Convert.ToInt32(DeferredUnitsSpinEdit.EditValue);
            _UnitDescriptorID = Convert.ToInt32(DeferredUnitDescriptiorIDGridLookUpEdit.EditValue);
            _ReasonID = Convert.ToInt32(DeferralReasonIDGridLookUpEdit.EditValue);
            _RevisitDate = RevisitDateDateEdit.DateTime;
            _Remarks = (DeferralRemarksMemoEdit.EditValue == null ? null : DeferralRemarksMemoEdit.EditValue.ToString());

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }











    }
}

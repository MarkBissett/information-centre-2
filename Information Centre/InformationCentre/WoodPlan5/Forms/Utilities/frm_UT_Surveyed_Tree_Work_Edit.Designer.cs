namespace WoodPlan5
{
    partial class frm_UT_Surveyed_Tree_Work_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Surveyed_Tree_Work_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SurveyDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07128UTSurveyedTreeWorkEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TeamOnHoldCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.InformationLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.PossibleLiveWorkCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RemarksInformationLabel = new DevExpress.XtraEditors.LabelControl();
            this.TreeReplacementNoneCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StumpTreatmentNoneCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.RevisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AchievableClearanceSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.PossibleFlailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ActualEquipmentSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedEquipmentSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualMaterialsSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedMaterialsSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualLabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedLabourSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualTotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedTotalSellSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.ApprovedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FinanceSystemBillingIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SelfBillingInvoiceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.WorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ActualTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedTotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedEquipmentCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualMaterialsCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedMaterialsCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedLabourCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EstimatedHoursSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.DateCompletedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.JobDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ApprovedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.JobTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnitDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07138UTUnitDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaterialDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditMaterialDescription = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCostPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualSell1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedFromDefaultRequiredEquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCost = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colActualCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditEquipment = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCostPerUnit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellPerUnit1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.DateScheduledDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DateRaisedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ActionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SurveyedTreeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReferenceNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.StumpTreatmentEcoPlugsSpinEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StumpTreatmentSprayingSpinEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StumpTreatmentPaintSpinEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TreeReplacementWhipsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TreeReplacementStandardsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForApprovedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyedTreeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForApproved = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEstimatedPZHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActualPZHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEstimatedLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedMaterialsCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForActualLabourCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualMaterialsCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEquipmentCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEstimatedLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedMaterialsSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedEquipmentSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForActualLabourSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualMaterialsSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualEquipmentSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualTotalSell = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem4 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStumpTreatmentEcoPlugs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStumpTreatmentSpraying = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStumpTreatmentPaint = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStumpTreatmentNone = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTreeReplacementWhips = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeReplacementStandards = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTreeReplacementNone = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSelfBillingInvoiceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFinanceSystemBillingID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRevisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTeamOnHold = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForJobDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateRaised = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPossibleFlail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateScheduled = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCompleted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAchievableClearance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPossibleLiveWork = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter();
            this.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter();
            this.sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter();
            this.sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter();
            this.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07128UTSurveyedTreeWorkEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamOnHoldCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PossibleLiveWorkCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementNoneCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentNoneCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AchievableClearanceSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PossibleFlailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedEquipmentSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialsSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedMaterialsSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedLabourSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedTotalSellSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceSystemBillingIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedTotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedEquipmentCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialsCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedMaterialsCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedLabourCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedHoursSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07138UTUnitDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditMaterialDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07130UTSurveyedPoleWorkPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateScheduledDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateScheduledDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedTreeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentEcoPlugsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentSprayingSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentPaintSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementWhipsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementStandardsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedTreeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApproved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedPZHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualPZHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedMaterialsCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialsCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedMaterialsSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedEquipmentSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialsSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalSell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentEcoPlugs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentSpraying)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentPaint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementWhips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementStandards)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementNone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceSystemBillingID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamOnHold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPossibleFlail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateScheduled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAchievableClearance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPossibleLiveWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(981, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 615);
            this.barDockControlBottom.Size = new System.Drawing.Size(981, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(981, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 589);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID2
            // 
            this.colID2.Caption = "Unit ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(981, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 615);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(981, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(981, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 589);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "BlockAdd_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SurveyDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TeamOnHoldCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.InformationLabelControl);
            this.dataLayoutControl1.Controls.Add(this.PossibleLiveWorkCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksInformationLabel);
            this.dataLayoutControl1.Controls.Add(this.TreeReplacementNoneCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.StumpTreatmentNoneCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.RevisitDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AchievableClearanceSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.PossibleFlailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualEquipmentSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedEquipmentSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualMaterialsSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedMaterialsSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualLabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedLabourSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualTotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedTotalSellSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.ApprovedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FinanceSystemBillingIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SelfBillingInvoiceIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedTotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedEquipmentCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualMaterialsCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedMaterialsCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedLabourCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EstimatedHoursSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCompletedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.JobDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ApprovedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.JobTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl3);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.DateScheduledDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DateRaisedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ActionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SurveyedTreeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.StumpTreatmentEcoPlugsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StumpTreatmentSprayingSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.StumpTreatmentPaintSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeReplacementWhipsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TreeReplacementStandardsSpinEdit);
            this.dataLayoutControl1.DataSource = this.sp07128UTSurveyedTreeWorkEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForApprovedByStaffID,
            this.ItemForSurveyedTreeID,
            this.ItemForJobTypeID,
            this.ItemForActionID,
            this.ItemForApproved,
            this.ItemForCreatedByStaffID,
            this.ItemForSurveyDate});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(68, 116, 271, 414);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(981, 589);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SurveyDateTextEdit
            // 
            this.SurveyDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "SurveyDate", true));
            this.SurveyDateTextEdit.Location = new System.Drawing.Point(134, 365);
            this.SurveyDateTextEdit.MenuManager = this.barManager1;
            this.SurveyDateTextEdit.Name = "SurveyDateTextEdit";
            this.SurveyDateTextEdit.Properties.Mask.EditMask = "d";
            this.SurveyDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.SurveyDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SurveyDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyDateTextEdit, true);
            this.SurveyDateTextEdit.Size = new System.Drawing.Size(823, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyDateTextEdit, optionsSpelling1);
            this.SurveyDateTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyDateTextEdit.TabIndex = 102;
            // 
            // sp07128UTSurveyedTreeWorkEditBindingSource
            // 
            this.sp07128UTSurveyedTreeWorkEditBindingSource.DataMember = "sp07128_UT_Surveyed_Tree_Work_Edit";
            this.sp07128UTSurveyedTreeWorkEditBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(134, 365);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(823, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling2);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 101;
            // 
            // TeamOnHoldCheckEdit
            // 
            this.TeamOnHoldCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "TeamOnHold", true));
            this.TeamOnHoldCheckEdit.Location = new System.Drawing.Point(146, 317);
            this.TeamOnHoldCheckEdit.MenuManager = this.barManager1;
            this.TeamOnHoldCheckEdit.Name = "TeamOnHoldCheckEdit";
            this.TeamOnHoldCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.TeamOnHoldCheckEdit.Properties.ValueChecked = 1;
            this.TeamOnHoldCheckEdit.Properties.ValueUnchecked = 0;
            this.TeamOnHoldCheckEdit.Size = new System.Drawing.Size(342, 19);
            this.TeamOnHoldCheckEdit.StyleController = this.dataLayoutControl1;
            this.TeamOnHoldCheckEdit.TabIndex = 103;
            // 
            // InformationLabelControl
            // 
            this.InformationLabelControl.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InformationLabelControl.Appearance.ImageIndex = 2;
            this.InformationLabelControl.Appearance.ImageList = this.imageCollection1;
            this.InformationLabelControl.Appearance.Options.UseImageAlign = true;
            this.InformationLabelControl.Appearance.Options.UseImageIndex = true;
            this.InformationLabelControl.Appearance.Options.UseImageList = true;
            this.InformationLabelControl.Location = new System.Drawing.Point(36, 221);
            this.InformationLabelControl.Name = "InformationLabelControl";
            this.InformationLabelControl.Size = new System.Drawing.Size(516, 21);
            this.InformationLabelControl.StyleController = this.dataLayoutControl1;
            this.InformationLabelControl.TabIndex = 100;
            this.InformationLabelControl.Text = "       Hours Recorded are Single Man Hours ";
            // 
            // PossibleLiveWorkCheckEdit
            // 
            this.PossibleLiveWorkCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "PossibleLiveWork", true));
            this.PossibleLiveWorkCheckEdit.Location = new System.Drawing.Point(464, 117);
            this.PossibleLiveWorkCheckEdit.MenuManager = this.barManager1;
            this.PossibleLiveWorkCheckEdit.Name = "PossibleLiveWorkCheckEdit";
            this.PossibleLiveWorkCheckEdit.Properties.Caption = "(Tick Yes)";
            this.PossibleLiveWorkCheckEdit.Properties.ValueChecked = 1;
            this.PossibleLiveWorkCheckEdit.Properties.ValueUnchecked = 0;
            this.PossibleLiveWorkCheckEdit.Size = new System.Drawing.Size(200, 19);
            this.PossibleLiveWorkCheckEdit.StyleController = this.dataLayoutControl1;
            this.PossibleLiveWorkCheckEdit.TabIndex = 6;
            // 
            // RemarksInformationLabel
            // 
            this.RemarksInformationLabel.AllowHtmlString = true;
            this.RemarksInformationLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.RemarksInformationLabel.Appearance.ImageIndex = 2;
            this.RemarksInformationLabel.Appearance.ImageList = this.imageCollection1;
            this.RemarksInformationLabel.Appearance.Options.UseImageAlign = true;
            this.RemarksInformationLabel.Appearance.Options.UseImageIndex = true;
            this.RemarksInformationLabel.Appearance.Options.UseImageList = true;
            this.RemarksInformationLabel.Location = new System.Drawing.Point(36, 221);
            this.RemarksInformationLabel.Name = "RemarksInformationLabel";
            this.RemarksInformationLabel.Size = new System.Drawing.Size(909, 18);
            this.RemarksInformationLabel.StyleController = this.dataLayoutControl1;
            this.RemarksInformationLabel.TabIndex = 98;
            this.RemarksInformationLabel.Text = "       These Remarks <b>are</b> shown on the Consent Form";
            // 
            // TreeReplacementNoneCheckEdit
            // 
            this.TreeReplacementNoneCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "TreeReplacementNone", true));
            this.TreeReplacementNoneCheckEdit.Location = new System.Drawing.Point(616, 251);
            this.TreeReplacementNoneCheckEdit.MenuManager = this.barManager1;
            this.TreeReplacementNoneCheckEdit.Name = "TreeReplacementNoneCheckEdit";
            this.TreeReplacementNoneCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.TreeReplacementNoneCheckEdit.Properties.ValueChecked = 1;
            this.TreeReplacementNoneCheckEdit.Properties.ValueUnchecked = 0;
            this.TreeReplacementNoneCheckEdit.Size = new System.Drawing.Size(103, 19);
            this.TreeReplacementNoneCheckEdit.StyleController = this.dataLayoutControl1;
            this.TreeReplacementNoneCheckEdit.TabIndex = 96;
            this.TreeReplacementNoneCheckEdit.CheckedChanged += new System.EventHandler(this.TreeReplacementNoneCheckEdit_CheckedChanged);
            // 
            // StumpTreatmentNoneCheckEdit
            // 
            this.StumpTreatmentNoneCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "StumpTreatmentNone", true));
            this.StumpTreatmentNoneCheckEdit.Location = new System.Drawing.Point(154, 251);
            this.StumpTreatmentNoneCheckEdit.MenuManager = this.barManager1;
            this.StumpTreatmentNoneCheckEdit.Name = "StumpTreatmentNoneCheckEdit";
            this.StumpTreatmentNoneCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.StumpTreatmentNoneCheckEdit.Properties.ValueChecked = 1;
            this.StumpTreatmentNoneCheckEdit.Properties.ValueUnchecked = 0;
            this.StumpTreatmentNoneCheckEdit.Size = new System.Drawing.Size(106, 19);
            this.StumpTreatmentNoneCheckEdit.StyleController = this.dataLayoutControl1;
            this.StumpTreatmentNoneCheckEdit.TabIndex = 96;
            this.StumpTreatmentNoneCheckEdit.CheckedChanged += new System.EventHandler(this.StumpTreatmentNoneCheckEdit_CheckedChanged);
            // 
            // RevisitDateDateEdit
            // 
            this.RevisitDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "RevisitDate", true));
            this.RevisitDateDateEdit.EditValue = null;
            this.RevisitDateDateEdit.Location = new System.Drawing.Point(146, 293);
            this.RevisitDateDateEdit.MenuManager = this.barManager1;
            this.RevisitDateDateEdit.Name = "RevisitDateDateEdit";
            this.RevisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RevisitDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.RevisitDateDateEdit.Size = new System.Drawing.Size(342, 20);
            this.RevisitDateDateEdit.StyleController = this.dataLayoutControl1;
            this.RevisitDateDateEdit.TabIndex = 80;
            this.RevisitDateDateEdit.EditValueChanged += new System.EventHandler(this.RevisitDateDateEdit_EditValueChanged);
            this.RevisitDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RevisitDateDateEdit_Validating);
            // 
            // AchievableClearanceSpinEdit
            // 
            this.AchievableClearanceSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "AchievableClearance", true));
            this.AchievableClearanceSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AchievableClearanceSpinEdit.Location = new System.Drawing.Point(122, 117);
            this.AchievableClearanceSpinEdit.MenuManager = this.barManager1;
            this.AchievableClearanceSpinEdit.Name = "AchievableClearanceSpinEdit";
            this.AchievableClearanceSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.AchievableClearanceSpinEdit.Properties.Mask.EditMask = "######0.00 M";
            this.AchievableClearanceSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AchievableClearanceSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.AchievableClearanceSpinEdit.Size = new System.Drawing.Size(228, 20);
            this.AchievableClearanceSpinEdit.StyleController = this.dataLayoutControl1;
            this.AchievableClearanceSpinEdit.TabIndex = 5;
            this.AchievableClearanceSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AchievableClearanceSpinEdit_Validating);
            // 
            // PossibleFlailCheckEdit
            // 
            this.PossibleFlailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "PossibleFlail", true));
            this.PossibleFlailCheckEdit.Location = new System.Drawing.Point(778, 117);
            this.PossibleFlailCheckEdit.MenuManager = this.barManager1;
            this.PossibleFlailCheckEdit.Name = "PossibleFlailCheckEdit";
            this.PossibleFlailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.PossibleFlailCheckEdit.Properties.ValueChecked = 1;
            this.PossibleFlailCheckEdit.Properties.ValueUnchecked = 0;
            this.PossibleFlailCheckEdit.Size = new System.Drawing.Size(191, 19);
            this.PossibleFlailCheckEdit.StyleController = this.dataLayoutControl1;
            this.PossibleFlailCheckEdit.TabIndex = 7;
            // 
            // ActualEquipmentSellSpinEdit
            // 
            this.ActualEquipmentSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualEquipmentSell", true));
            this.ActualEquipmentSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualEquipmentSellSpinEdit.Location = new System.Drawing.Point(857, 299);
            this.ActualEquipmentSellSpinEdit.MenuManager = this.barManager1;
            this.ActualEquipmentSellSpinEdit.Name = "ActualEquipmentSellSpinEdit";
            this.ActualEquipmentSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualEquipmentSellSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualEquipmentSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualEquipmentSellSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.ActualEquipmentSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualEquipmentSellSpinEdit.TabIndex = 77;
            this.ActualEquipmentSellSpinEdit.EditValueChanged += new System.EventHandler(this.ActualEquipmentSellSpinEdit_EditValueChanged);
            this.ActualEquipmentSellSpinEdit.Validated += new System.EventHandler(this.ActualEquipmentSellSpinEdit_Validated);
            // 
            // EstimatedEquipmentSellSpinEdit
            // 
            this.EstimatedEquipmentSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedEquipmentSell", true));
            this.EstimatedEquipmentSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedEquipmentSellSpinEdit.Location = new System.Drawing.Point(626, 299);
            this.EstimatedEquipmentSellSpinEdit.MenuManager = this.barManager1;
            this.EstimatedEquipmentSellSpinEdit.Name = "EstimatedEquipmentSellSpinEdit";
            this.EstimatedEquipmentSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedEquipmentSellSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedEquipmentSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedEquipmentSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.EstimatedEquipmentSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedEquipmentSellSpinEdit.TabIndex = 76;
            this.EstimatedEquipmentSellSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedEquipmentSellSpinEdit_EditValueChanged);
            this.EstimatedEquipmentSellSpinEdit.Validated += new System.EventHandler(this.EstimatedEquipmentSellSpinEdit_Validated);
            // 
            // ActualMaterialsSellSpinEdit
            // 
            this.ActualMaterialsSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualMaterialsSell", true));
            this.ActualMaterialsSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualMaterialsSellSpinEdit.Location = new System.Drawing.Point(857, 275);
            this.ActualMaterialsSellSpinEdit.MenuManager = this.barManager1;
            this.ActualMaterialsSellSpinEdit.Name = "ActualMaterialsSellSpinEdit";
            this.ActualMaterialsSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualMaterialsSellSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualMaterialsSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualMaterialsSellSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.ActualMaterialsSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualMaterialsSellSpinEdit.TabIndex = 75;
            this.ActualMaterialsSellSpinEdit.EditValueChanged += new System.EventHandler(this.ActualMaterialsSellSpinEdit_EditValueChanged);
            this.ActualMaterialsSellSpinEdit.Validated += new System.EventHandler(this.ActualMaterialsSellSpinEdit_Validated);
            // 
            // EstimatedMaterialsSellSpinEdit
            // 
            this.EstimatedMaterialsSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedMaterialsSell", true));
            this.EstimatedMaterialsSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedMaterialsSellSpinEdit.Location = new System.Drawing.Point(626, 275);
            this.EstimatedMaterialsSellSpinEdit.MenuManager = this.barManager1;
            this.EstimatedMaterialsSellSpinEdit.Name = "EstimatedMaterialsSellSpinEdit";
            this.EstimatedMaterialsSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedMaterialsSellSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedMaterialsSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedMaterialsSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.EstimatedMaterialsSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedMaterialsSellSpinEdit.TabIndex = 74;
            this.EstimatedMaterialsSellSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedMaterialsSellSpinEdit_EditValueChanged);
            this.EstimatedMaterialsSellSpinEdit.Validated += new System.EventHandler(this.EstimatedMaterialsSellSpinEdit_Validated);
            // 
            // ActualLabourSellSpinEdit
            // 
            this.ActualLabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualLabourSell", true));
            this.ActualLabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualLabourSellSpinEdit.Location = new System.Drawing.Point(857, 251);
            this.ActualLabourSellSpinEdit.MenuManager = this.barManager1;
            this.ActualLabourSellSpinEdit.Name = "ActualLabourSellSpinEdit";
            this.ActualLabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualLabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualLabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualLabourSellSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.ActualLabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualLabourSellSpinEdit.TabIndex = 73;
            this.ActualLabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.ActualLabourSellSpinEdit_EditValueChanged);
            this.ActualLabourSellSpinEdit.Validated += new System.EventHandler(this.ActualLabourSellSpinEdit_Validated);
            // 
            // EstimatedLabourSellSpinEdit
            // 
            this.EstimatedLabourSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedLabourSell", true));
            this.EstimatedLabourSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedLabourSellSpinEdit.Location = new System.Drawing.Point(626, 251);
            this.EstimatedLabourSellSpinEdit.MenuManager = this.barManager1;
            this.EstimatedLabourSellSpinEdit.Name = "EstimatedLabourSellSpinEdit";
            this.EstimatedLabourSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedLabourSellSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedLabourSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedLabourSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.EstimatedLabourSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedLabourSellSpinEdit.TabIndex = 72;
            this.EstimatedLabourSellSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedLabourSellSpinEdit_EditValueChanged);
            this.EstimatedLabourSellSpinEdit.Validated += new System.EventHandler(this.EstimatedLabourSellSpinEdit_Validated);
            // 
            // ActualTotalSellSpinEdit
            // 
            this.ActualTotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualTotalSell", true));
            this.ActualTotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualTotalSellSpinEdit.Location = new System.Drawing.Point(857, 323);
            this.ActualTotalSellSpinEdit.MenuManager = this.barManager1;
            this.ActualTotalSellSpinEdit.Name = "ActualTotalSellSpinEdit";
            this.ActualTotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualTotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualTotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualTotalSellSpinEdit.Properties.ReadOnly = true;
            this.ActualTotalSellSpinEdit.Size = new System.Drawing.Size(80, 20);
            this.ActualTotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualTotalSellSpinEdit.TabIndex = 71;
            // 
            // EstimatedTotalSellSpinEdit
            // 
            this.EstimatedTotalSellSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedTotalSell", true));
            this.EstimatedTotalSellSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedTotalSellSpinEdit.Location = new System.Drawing.Point(626, 323);
            this.EstimatedTotalSellSpinEdit.MenuManager = this.barManager1;
            this.EstimatedTotalSellSpinEdit.Name = "EstimatedTotalSellSpinEdit";
            this.EstimatedTotalSellSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedTotalSellSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedTotalSellSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedTotalSellSpinEdit.Properties.ReadOnly = true;
            this.EstimatedTotalSellSpinEdit.Size = new System.Drawing.Size(95, 20);
            this.EstimatedTotalSellSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedTotalSellSpinEdit.TabIndex = 70;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(12, 379);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ApprovedByStaffIDTextEdit
            // 
            this.ApprovedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ApprovedByStaffID", true));
            this.ApprovedByStaffIDTextEdit.Location = new System.Drawing.Point(448, 399);
            this.ApprovedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.ApprovedByStaffIDTextEdit.Name = "ApprovedByStaffIDTextEdit";
            this.ApprovedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ApprovedByStaffIDTextEdit, true);
            this.ApprovedByStaffIDTextEdit.Size = new System.Drawing.Size(156, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ApprovedByStaffIDTextEdit, optionsSpelling3);
            this.ApprovedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ApprovedByStaffIDTextEdit.TabIndex = 51;
            // 
            // FinanceSystemBillingIDTextEdit
            // 
            this.FinanceSystemBillingIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "FinanceSystemBillingID", true));
            this.FinanceSystemBillingIDTextEdit.Location = new System.Drawing.Point(146, 269);
            this.FinanceSystemBillingIDTextEdit.MenuManager = this.barManager1;
            this.FinanceSystemBillingIDTextEdit.Name = "FinanceSystemBillingIDTextEdit";
            this.FinanceSystemBillingIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FinanceSystemBillingIDTextEdit, true);
            this.FinanceSystemBillingIDTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FinanceSystemBillingIDTextEdit, optionsSpelling4);
            this.FinanceSystemBillingIDTextEdit.StyleController = this.dataLayoutControl1;
            this.FinanceSystemBillingIDTextEdit.TabIndex = 68;
            // 
            // SelfBillingInvoiceIDTextEdit
            // 
            this.SelfBillingInvoiceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "SelfBillingInvoiceID", true));
            this.SelfBillingInvoiceIDTextEdit.Location = new System.Drawing.Point(146, 245);
            this.SelfBillingInvoiceIDTextEdit.MenuManager = this.barManager1;
            this.SelfBillingInvoiceIDTextEdit.Name = "SelfBillingInvoiceIDTextEdit";
            this.SelfBillingInvoiceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SelfBillingInvoiceIDTextEdit, true);
            this.SelfBillingInvoiceIDTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SelfBillingInvoiceIDTextEdit, optionsSpelling5);
            this.SelfBillingInvoiceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SelfBillingInvoiceIDTextEdit.TabIndex = 67;
            // 
            // WorkOrderIDTextEdit
            // 
            this.WorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "WorkOrderID", true));
            this.WorkOrderIDTextEdit.Location = new System.Drawing.Point(146, 221);
            this.WorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.WorkOrderIDTextEdit.Name = "WorkOrderIDTextEdit";
            this.WorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkOrderIDTextEdit, true);
            this.WorkOrderIDTextEdit.Size = new System.Drawing.Size(342, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkOrderIDTextEdit, optionsSpelling6);
            this.WorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderIDTextEdit.TabIndex = 66;
            // 
            // ActualTotalCostSpinEdit
            // 
            this.ActualTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualTotalCost", true));
            this.ActualTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualTotalCostSpinEdit.Location = new System.Drawing.Point(393, 323);
            this.ActualTotalCostSpinEdit.MenuManager = this.barManager1;
            this.ActualTotalCostSpinEdit.Name = "ActualTotalCostSpinEdit";
            this.ActualTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualTotalCostSpinEdit.Properties.ReadOnly = true;
            this.ActualTotalCostSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.ActualTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualTotalCostSpinEdit.TabIndex = 65;
            // 
            // EstimatedTotalCostSpinEdit
            // 
            this.EstimatedTotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedTotalCost", true));
            this.EstimatedTotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedTotalCostSpinEdit.Location = new System.Drawing.Point(154, 323);
            this.EstimatedTotalCostSpinEdit.MenuManager = this.barManager1;
            this.EstimatedTotalCostSpinEdit.Name = "EstimatedTotalCostSpinEdit";
            this.EstimatedTotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedTotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedTotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedTotalCostSpinEdit.Properties.ReadOnly = true;
            this.EstimatedTotalCostSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.EstimatedTotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedTotalCostSpinEdit.TabIndex = 64;
            // 
            // ActualEquipmentCostSpinEdit
            // 
            this.ActualEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualEquipmentCost", true));
            this.ActualEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualEquipmentCostSpinEdit.Location = new System.Drawing.Point(393, 299);
            this.ActualEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.ActualEquipmentCostSpinEdit.Name = "ActualEquipmentCostSpinEdit";
            this.ActualEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualEquipmentCostSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.ActualEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualEquipmentCostSpinEdit.TabIndex = 63;
            this.ActualEquipmentCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualEquipmentCostSpinEdit_EditValueChanged);
            this.ActualEquipmentCostSpinEdit.Validated += new System.EventHandler(this.ActualEquipmentCostSpinEdit_Validated);
            // 
            // EstimatedEquipmentCostSpinEdit
            // 
            this.EstimatedEquipmentCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedEquipmentCost", true));
            this.EstimatedEquipmentCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedEquipmentCostSpinEdit.Location = new System.Drawing.Point(154, 299);
            this.EstimatedEquipmentCostSpinEdit.MenuManager = this.barManager1;
            this.EstimatedEquipmentCostSpinEdit.Name = "EstimatedEquipmentCostSpinEdit";
            this.EstimatedEquipmentCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedEquipmentCostSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedEquipmentCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedEquipmentCostSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.EstimatedEquipmentCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedEquipmentCostSpinEdit.TabIndex = 62;
            this.EstimatedEquipmentCostSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedEquipmentCostSpinEdit_EditValueChanged);
            this.EstimatedEquipmentCostSpinEdit.Validated += new System.EventHandler(this.EstimatedEquipmentCostSpinEdit_Validated);
            // 
            // ActualMaterialsCostSpinEdit
            // 
            this.ActualMaterialsCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualMaterialsCost", true));
            this.ActualMaterialsCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualMaterialsCostSpinEdit.Location = new System.Drawing.Point(393, 275);
            this.ActualMaterialsCostSpinEdit.MenuManager = this.barManager1;
            this.ActualMaterialsCostSpinEdit.Name = "ActualMaterialsCostSpinEdit";
            this.ActualMaterialsCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualMaterialsCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualMaterialsCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualMaterialsCostSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.ActualMaterialsCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualMaterialsCostSpinEdit.TabIndex = 61;
            this.ActualMaterialsCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualMaterialsCostSpinEdit_EditValueChanged);
            this.ActualMaterialsCostSpinEdit.Validated += new System.EventHandler(this.ActualMaterialsCostSpinEdit_Validated);
            // 
            // EstimatedMaterialsCostSpinEdit
            // 
            this.EstimatedMaterialsCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedMaterialsCost", true));
            this.EstimatedMaterialsCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedMaterialsCostSpinEdit.Location = new System.Drawing.Point(154, 275);
            this.EstimatedMaterialsCostSpinEdit.MenuManager = this.barManager1;
            this.EstimatedMaterialsCostSpinEdit.Name = "EstimatedMaterialsCostSpinEdit";
            this.EstimatedMaterialsCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedMaterialsCostSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedMaterialsCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedMaterialsCostSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.EstimatedMaterialsCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedMaterialsCostSpinEdit.TabIndex = 60;
            this.EstimatedMaterialsCostSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedMaterialsCostSpinEdit_EditValueChanged);
            this.EstimatedMaterialsCostSpinEdit.Validated += new System.EventHandler(this.EstimatedMaterialsCostSpinEdit_Validated);
            // 
            // ActualLabourCostSpinEdit
            // 
            this.ActualLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualLabourCost", true));
            this.ActualLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualLabourCostSpinEdit.Location = new System.Drawing.Point(393, 251);
            this.ActualLabourCostSpinEdit.MenuManager = this.barManager1;
            this.ActualLabourCostSpinEdit.Name = "ActualLabourCostSpinEdit";
            this.ActualLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.ActualLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualLabourCostSpinEdit.Size = new System.Drawing.Size(97, 20);
            this.ActualLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualLabourCostSpinEdit.TabIndex = 59;
            this.ActualLabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.ActualLabourCostSpinEdit_EditValueChanged);
            this.ActualLabourCostSpinEdit.Validated += new System.EventHandler(this.ActualLabourCostSpinEdit_Validated);
            // 
            // EstimatedLabourCostSpinEdit
            // 
            this.EstimatedLabourCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedLabourCost", true));
            this.EstimatedLabourCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedLabourCostSpinEdit.Location = new System.Drawing.Point(154, 251);
            this.EstimatedLabourCostSpinEdit.MenuManager = this.barManager1;
            this.EstimatedLabourCostSpinEdit.Name = "EstimatedLabourCostSpinEdit";
            this.EstimatedLabourCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedLabourCostSpinEdit.Properties.Mask.EditMask = "c";
            this.EstimatedLabourCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedLabourCostSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.EstimatedLabourCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedLabourCostSpinEdit.TabIndex = 58;
            this.EstimatedLabourCostSpinEdit.EditValueChanged += new System.EventHandler(this.EstimatedLabourCostSpinEdit_EditValueChanged);
            this.EstimatedLabourCostSpinEdit.Validated += new System.EventHandler(this.EstimatedLabourCostSpinEdit_Validated);
            // 
            // ActualHoursSpinEdit
            // 
            this.ActualHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActualHours", true));
            this.ActualHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ActualHoursSpinEdit.Location = new System.Drawing.Point(406, 246);
            this.ActualHoursSpinEdit.MenuManager = this.barManager1;
            this.ActualHoursSpinEdit.Name = "ActualHoursSpinEdit";
            this.ActualHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ActualHoursSpinEdit.Properties.Mask.EditMask = "######0.00 Hours";
            this.ActualHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualHoursSpinEdit.Size = new System.Drawing.Size(146, 20);
            this.ActualHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.ActualHoursSpinEdit.TabIndex = 9;
            // 
            // EstimatedHoursSpinEdit
            // 
            this.EstimatedHoursSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "EstimatedHours", true));
            this.EstimatedHoursSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.EstimatedHoursSpinEdit.Location = new System.Drawing.Point(146, 246);
            this.EstimatedHoursSpinEdit.MenuManager = this.barManager1;
            this.EstimatedHoursSpinEdit.Name = "EstimatedHoursSpinEdit";
            this.EstimatedHoursSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedHoursSpinEdit.Properties.Mask.EditMask = "######0.00 Hours";
            this.EstimatedHoursSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedHoursSpinEdit.Size = new System.Drawing.Size(146, 20);
            this.EstimatedHoursSpinEdit.StyleController = this.dataLayoutControl1;
            this.EstimatedHoursSpinEdit.TabIndex = 8;
            this.EstimatedHoursSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EstimatedHoursSpinEdit_Validating);
            // 
            // DateCompletedDateEdit
            // 
            this.DateCompletedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "DateCompleted", true));
            this.DateCompletedDateEdit.EditValue = null;
            this.DateCompletedDateEdit.Location = new System.Drawing.Point(778, 93);
            this.DateCompletedDateEdit.MenuManager = this.barManager1;
            this.DateCompletedDateEdit.Name = "DateCompletedDateEdit";
            this.DateCompletedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateCompletedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCompletedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCompletedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCompletedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCompletedDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Size = new System.Drawing.Size(191, 20);
            this.DateCompletedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCompletedDateEdit.TabIndex = 4;
            // 
            // JobDescriptionButtonEdit
            // 
            this.JobDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "JobDescription", true));
            this.JobDescriptionButtonEdit.Location = new System.Drawing.Point(122, 35);
            this.JobDescriptionButtonEdit.MenuManager = this.barManager1;
            this.JobDescriptionButtonEdit.Name = "JobDescriptionButtonEdit";
            this.JobDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View Linked Reference Files", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "reference_files", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.JobDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.JobDescriptionButtonEdit.Size = new System.Drawing.Size(847, 20);
            this.JobDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.JobDescriptionButtonEdit.TabIndex = 0;
            this.JobDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.JobDescriptionButtonEdit_ButtonClick);
            this.JobDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.JobDescriptionButtonEdit_Validating);
            // 
            // ApprovedCheckEdit
            // 
            this.ApprovedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "Approved", true));
            this.ApprovedCheckEdit.Location = new System.Drawing.Point(122, 117);
            this.ApprovedCheckEdit.MenuManager = this.barManager1;
            this.ApprovedCheckEdit.Name = "ApprovedCheckEdit";
            this.ApprovedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ApprovedCheckEdit.Properties.ValueChecked = 1;
            this.ApprovedCheckEdit.Properties.ValueUnchecked = 0;
            this.ApprovedCheckEdit.Size = new System.Drawing.Size(228, 19);
            this.ApprovedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ApprovedCheckEdit.TabIndex = 43;
            // 
            // JobTypeIDTextEdit
            // 
            this.JobTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "JobTypeID", true));
            this.JobTypeIDTextEdit.Location = new System.Drawing.Point(156, 399);
            this.JobTypeIDTextEdit.MenuManager = this.barManager1;
            this.JobTypeIDTextEdit.Name = "JobTypeIDTextEdit";
            this.JobTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.JobTypeIDTextEdit, true);
            this.JobTypeIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.JobTypeIDTextEdit, optionsSpelling7);
            this.JobTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.JobTypeIDTextEdit.TabIndex = 36;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(24, 448);
            this.gridControl3.MainView = this.gridView5;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemSpinEdit2DP,
            this.repositoryItemGridLookUpEditUnitDescriptor,
            this.repositoryItemSpinEditCurrency,
            this.repositoryItemButtonEditMaterialDescription});
            this.gridControl3.Size = new System.Drawing.Size(933, 117);
            this.gridControl3.TabIndex = 47;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource
            // 
            this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.DataMember = "sp07132_UT_Surveyed_Tree_Work_Linked_Materials";
            this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionMaterialID,
            this.colActionID1,
            this.colMaterialID,
            this.colEstimatedUnits,
            this.colActualUnits,
            this.colUnitsDescriptorID,
            this.colEstimatedCost1,
            this.colActualCost1,
            this.colRemarks2,
            this.colGUID2,
            this.colstrMode1,
            this.colstrRecordIDs1,
            this.colMaterialDescription,
            this.colCostPerUnit,
            this.colSellPerUnit,
            this.colEstimatedSell1,
            this.colActualSell1});
            this.gridView5.GridControl = this.gridControl3;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView5.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView5.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colActionMaterialID
            // 
            this.colActionMaterialID.Caption = "Action Material ID";
            this.colActionMaterialID.FieldName = "ActionMaterialID";
            this.colActionMaterialID.Name = "colActionMaterialID";
            this.colActionMaterialID.OptionsColumn.AllowEdit = false;
            this.colActionMaterialID.OptionsColumn.AllowFocus = false;
            this.colActionMaterialID.OptionsColumn.ReadOnly = true;
            this.colActionMaterialID.Width = 106;
            // 
            // colActionID1
            // 
            this.colActionID1.Caption = "Action ID";
            this.colActionID1.FieldName = "ActionID";
            this.colActionID1.Name = "colActionID1";
            this.colActionID1.OptionsColumn.AllowEdit = false;
            this.colActionID1.OptionsColumn.AllowFocus = false;
            this.colActionID1.OptionsColumn.ReadOnly = true;
            // 
            // colMaterialID
            // 
            this.colMaterialID.Caption = "Material ID";
            this.colMaterialID.FieldName = "MaterialID";
            this.colMaterialID.Name = "colMaterialID";
            this.colMaterialID.OptionsColumn.AllowEdit = false;
            this.colMaterialID.OptionsColumn.AllowFocus = false;
            this.colMaterialID.OptionsColumn.ReadOnly = true;
            // 
            // colEstimatedUnits
            // 
            this.colEstimatedUnits.Caption = "Estimated Units";
            this.colEstimatedUnits.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colEstimatedUnits.FieldName = "EstimatedUnits";
            this.colEstimatedUnits.Name = "colEstimatedUnits";
            this.colEstimatedUnits.Visible = true;
            this.colEstimatedUnits.VisibleIndex = 1;
            this.colEstimatedUnits.Width = 95;
            // 
            // repositoryItemSpinEdit2DP
            // 
            this.repositoryItemSpinEdit2DP.AutoHeight = false;
            this.repositoryItemSpinEdit2DP.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemSpinEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DP.Name = "repositoryItemSpinEdit2DP";
            this.repositoryItemSpinEdit2DP.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DP_EditValueChanged);
            this.repositoryItemSpinEdit2DP.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEdit2DP_Validating);
            // 
            // colActualUnits
            // 
            this.colActualUnits.Caption = "Actual Units";
            this.colActualUnits.ColumnEdit = this.repositoryItemSpinEdit2DP;
            this.colActualUnits.FieldName = "ActualUnits";
            this.colActualUnits.Name = "colActualUnits";
            this.colActualUnits.Visible = true;
            this.colActualUnits.VisibleIndex = 2;
            this.colActualUnits.Width = 78;
            // 
            // colUnitsDescriptorID
            // 
            this.colUnitsDescriptorID.Caption = "Units Descriptor";
            this.colUnitsDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnitDescriptor;
            this.colUnitsDescriptorID.FieldName = "UnitsDescriptorID";
            this.colUnitsDescriptorID.Name = "colUnitsDescriptorID";
            this.colUnitsDescriptorID.Visible = true;
            this.colUnitsDescriptorID.VisibleIndex = 3;
            this.colUnitsDescriptorID.Width = 97;
            // 
            // repositoryItemGridLookUpEditUnitDescriptor
            // 
            this.repositoryItemGridLookUpEditUnitDescriptor.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnitDescriptor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnitDescriptor.DataSource = this.sp07138UTUnitDescriptorsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditUnitDescriptor.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnitDescriptor.Name = "repositoryItemGridLookUpEditUnitDescriptor";
            this.repositoryItemGridLookUpEditUnitDescriptor.NullText = "";
            this.repositoryItemGridLookUpEditUnitDescriptor.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditUnitDescriptor.ValueMember = "ID";
            // 
            // sp07138UTUnitDescriptorsWithBlankBindingSource
            // 
            this.sp07138UTUnitDescriptorsWithBlankBindingSource.DataMember = "sp07138_UT_Unit_Descriptors_With_Blank";
            this.sp07138UTUnitDescriptorsWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Unit Descriptor";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colEstimatedCost1
            // 
            this.colEstimatedCost1.Caption = "Estimated Cost";
            this.colEstimatedCost1.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colEstimatedCost1.FieldName = "EstimatedCost";
            this.colEstimatedCost1.Name = "colEstimatedCost1";
            this.colEstimatedCost1.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost1.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost1.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost1.Visible = true;
            this.colEstimatedCost1.VisibleIndex = 6;
            this.colEstimatedCost1.Width = 93;
            // 
            // repositoryItemSpinEditCurrency
            // 
            this.repositoryItemSpinEditCurrency.AutoHeight = false;
            this.repositoryItemSpinEditCurrency.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditCurrency.Mask.EditMask = "c";
            this.repositoryItemSpinEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCurrency.Name = "repositoryItemSpinEditCurrency";
            this.repositoryItemSpinEditCurrency.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCurrency_EditValueChanged);
            this.repositoryItemSpinEditCurrency.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditCurrency_Validating);
            // 
            // colActualCost1
            // 
            this.colActualCost1.Caption = "Actual Cost";
            this.colActualCost1.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colActualCost1.FieldName = "ActualCost";
            this.colActualCost1.Name = "colActualCost1";
            this.colActualCost1.OptionsColumn.AllowEdit = false;
            this.colActualCost1.OptionsColumn.AllowFocus = false;
            this.colActualCost1.OptionsColumn.ReadOnly = true;
            this.colActualCost1.Visible = true;
            this.colActualCost1.VisibleIndex = 7;
            this.colActualCost1.Width = 88;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID2
            // 
            this.colGUID2.Caption = "GUID";
            this.colGUID2.FieldName = "GUID";
            this.colGUID2.Name = "colGUID2";
            this.colGUID2.OptionsColumn.AllowEdit = false;
            this.colGUID2.OptionsColumn.AllowFocus = false;
            this.colGUID2.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode1
            // 
            this.colstrMode1.Caption = "Mode";
            this.colstrMode1.FieldName = "strMode";
            this.colstrMode1.Name = "colstrMode1";
            this.colstrMode1.OptionsColumn.AllowEdit = false;
            this.colstrMode1.OptionsColumn.AllowFocus = false;
            this.colstrMode1.OptionsColumn.ReadOnly = true;
            this.colstrMode1.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs1
            // 
            this.colstrRecordIDs1.Caption = "Record IDs";
            this.colstrRecordIDs1.FieldName = "strRecordIDs";
            this.colstrRecordIDs1.Name = "colstrRecordIDs1";
            this.colstrRecordIDs1.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs1.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs1.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs1.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colMaterialDescription
            // 
            this.colMaterialDescription.Caption = "Material";
            this.colMaterialDescription.ColumnEdit = this.repositoryItemButtonEditMaterialDescription;
            this.colMaterialDescription.FieldName = "MaterialDescription";
            this.colMaterialDescription.Name = "colMaterialDescription";
            this.colMaterialDescription.Visible = true;
            this.colMaterialDescription.VisibleIndex = 0;
            this.colMaterialDescription.Width = 231;
            // 
            // repositoryItemButtonEditMaterialDescription
            // 
            this.repositoryItemButtonEditMaterialDescription.AutoHeight = false;
            editorButtonImageOptions3.Location = DevExpress.XtraEditors.ImageLocation.Default;
            this.repositoryItemButtonEditMaterialDescription.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to Open the Select Material screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditMaterialDescription.Name = "repositoryItemButtonEditMaterialDescription";
            this.repositoryItemButtonEditMaterialDescription.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditMaterialDescription.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditMaterialDescription_ButtonClick);
            // 
            // colCostPerUnit
            // 
            this.colCostPerUnit.Caption = "Cost Per Unit";
            this.colCostPerUnit.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colCostPerUnit.FieldName = "CostPerUnit";
            this.colCostPerUnit.Name = "colCostPerUnit";
            this.colCostPerUnit.Visible = true;
            this.colCostPerUnit.VisibleIndex = 4;
            this.colCostPerUnit.Width = 84;
            // 
            // colSellPerUnit
            // 
            this.colSellPerUnit.Caption = "Sell Per Unit";
            this.colSellPerUnit.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colSellPerUnit.FieldName = "SellPerUnit";
            this.colSellPerUnit.Name = "colSellPerUnit";
            this.colSellPerUnit.Visible = true;
            this.colSellPerUnit.VisibleIndex = 5;
            this.colSellPerUnit.Width = 78;
            // 
            // colEstimatedSell1
            // 
            this.colEstimatedSell1.Caption = "Estimated Sell";
            this.colEstimatedSell1.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colEstimatedSell1.FieldName = "EstimatedSell";
            this.colEstimatedSell1.Name = "colEstimatedSell1";
            this.colEstimatedSell1.OptionsColumn.AllowEdit = false;
            this.colEstimatedSell1.OptionsColumn.AllowFocus = false;
            this.colEstimatedSell1.OptionsColumn.ReadOnly = true;
            this.colEstimatedSell1.Visible = true;
            this.colEstimatedSell1.VisibleIndex = 8;
            this.colEstimatedSell1.Width = 87;
            // 
            // colActualSell1
            // 
            this.colActualSell1.Caption = "Actual Sell";
            this.colActualSell1.ColumnEdit = this.repositoryItemSpinEditCurrency;
            this.colActualSell1.FieldName = "ActualSell";
            this.colActualSell1.Name = "colActualSell1";
            this.colActualSell1.OptionsColumn.AllowEdit = false;
            this.colActualSell1.OptionsColumn.AllowFocus = false;
            this.colActualSell1.OptionsColumn.ReadOnly = true;
            this.colActualSell1.Visible = true;
            this.colActualSell1.VisibleIndex = 9;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(24, 448);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemButtonEditEquipment,
            this.repositoryItemSpinEditHours,
            this.repositoryItemSpinEditCost});
            this.gridControl2.Size = new System.Drawing.Size(933, 117);
            this.gridControl2.TabIndex = 47;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource
            // 
            this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.DataMember = "sp07131_UT_Surveyed_Tree_Work_Linked_Equipment";
            this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionEquipmentID,
            this.colActionID,
            this.colEquipmentID,
            this.colCreatedFromDefaultRequiredEquipmentID,
            this.colEstimatedHours,
            this.colActualHours,
            this.colEstimatedCost,
            this.colActualCost,
            this.colRemarks1,
            this.colGUID1,
            this.colstrMode,
            this.colstrRecordIDs,
            this.colEquipmentDescription,
            this.colCostPerUnit1,
            this.colSellPerUnit1,
            this.colEstimatedSell,
            this.colActualSell});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colActionEquipmentID
            // 
            this.colActionEquipmentID.Caption = "Action Equipment ID";
            this.colActionEquipmentID.FieldName = "ActionEquipmentID";
            this.colActionEquipmentID.Name = "colActionEquipmentID";
            this.colActionEquipmentID.OptionsColumn.AllowEdit = false;
            this.colActionEquipmentID.OptionsColumn.AllowFocus = false;
            this.colActionEquipmentID.OptionsColumn.ReadOnly = true;
            this.colActionEquipmentID.Width = 118;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colEquipmentID
            // 
            this.colEquipmentID.Caption = "Equipment ID";
            this.colEquipmentID.FieldName = "EquipmentID";
            this.colEquipmentID.Name = "colEquipmentID";
            this.colEquipmentID.OptionsColumn.AllowEdit = false;
            this.colEquipmentID.OptionsColumn.AllowFocus = false;
            this.colEquipmentID.OptionsColumn.ReadOnly = true;
            this.colEquipmentID.Width = 85;
            // 
            // colCreatedFromDefaultRequiredEquipmentID
            // 
            this.colCreatedFromDefaultRequiredEquipmentID.Caption = "Created From Default Required Equipment ID";
            this.colCreatedFromDefaultRequiredEquipmentID.FieldName = "CreatedFromDefaultRequiredEquipmentID";
            this.colCreatedFromDefaultRequiredEquipmentID.Name = "colCreatedFromDefaultRequiredEquipmentID";
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.AllowEdit = false;
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.AllowFocus = false;
            this.colCreatedFromDefaultRequiredEquipmentID.OptionsColumn.ReadOnly = true;
            this.colCreatedFromDefaultRequiredEquipmentID.Width = 238;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemSpinEditHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 1;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemSpinEditHours
            // 
            this.repositoryItemSpinEditHours.AutoHeight = false;
            this.repositoryItemSpinEditHours.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemSpinEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditHours.Name = "repositoryItemSpinEditHours";
            this.repositoryItemSpinEditHours.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditHours_EditValueChanged);
            this.repositoryItemSpinEditHours.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditHours_Validating);
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemSpinEditHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 2;
            this.colActualHours.Width = 82;
            // 
            // colEstimatedCost
            // 
            this.colEstimatedCost.Caption = "Estimated Cost";
            this.colEstimatedCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colEstimatedCost.FieldName = "EstimatedCost";
            this.colEstimatedCost.Name = "colEstimatedCost";
            this.colEstimatedCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedCost.Visible = true;
            this.colEstimatedCost.VisibleIndex = 5;
            this.colEstimatedCost.Width = 93;
            // 
            // repositoryItemSpinEditCost
            // 
            this.repositoryItemSpinEditCost.AutoHeight = false;
            this.repositoryItemSpinEditCost.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditCost.Mask.EditMask = "c";
            this.repositoryItemSpinEditCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCost.Name = "repositoryItemSpinEditCost";
            this.repositoryItemSpinEditCost.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCost_EditValueChanged);
            this.repositoryItemSpinEditCost.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditCost_Validating);
            // 
            // colActualCost
            // 
            this.colActualCost.Caption = "Actual Cost";
            this.colActualCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colActualCost.FieldName = "ActualCost";
            this.colActualCost.Name = "colActualCost";
            this.colActualCost.OptionsColumn.AllowEdit = false;
            this.colActualCost.OptionsColumn.AllowFocus = false;
            this.colActualCost.OptionsColumn.ReadOnly = true;
            this.colActualCost.Visible = true;
            this.colActualCost.VisibleIndex = 6;
            this.colActualCost.Width = 76;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode
            // 
            this.colstrMode.Caption = "Mode";
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            this.colstrMode.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.Caption = "Record IDs";
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colEquipmentDescription
            // 
            this.colEquipmentDescription.Caption = "Equipment";
            this.colEquipmentDescription.ColumnEdit = this.repositoryItemButtonEditEquipment;
            this.colEquipmentDescription.FieldName = "EquipmentDescription";
            this.colEquipmentDescription.Name = "colEquipmentDescription";
            this.colEquipmentDescription.Visible = true;
            this.colEquipmentDescription.VisibleIndex = 0;
            this.colEquipmentDescription.Width = 304;
            // 
            // repositoryItemButtonEditEquipment
            // 
            this.repositoryItemButtonEditEquipment.AutoHeight = false;
            this.repositoryItemButtonEditEquipment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Opens Select Equipment screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditEquipment.Name = "repositoryItemButtonEditEquipment";
            this.repositoryItemButtonEditEquipment.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditEquipment_ButtonClick);
            // 
            // colCostPerUnit1
            // 
            this.colCostPerUnit1.Caption = "Cost Per Hour";
            this.colCostPerUnit1.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colCostPerUnit1.FieldName = "CostPerUnit";
            this.colCostPerUnit1.Name = "colCostPerUnit1";
            this.colCostPerUnit1.Visible = true;
            this.colCostPerUnit1.VisibleIndex = 3;
            this.colCostPerUnit1.Width = 88;
            // 
            // colSellPerUnit1
            // 
            this.colSellPerUnit1.Caption = "Sell Per Hour";
            this.colSellPerUnit1.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSellPerUnit1.FieldName = "SellPerUnit";
            this.colSellPerUnit1.Name = "colSellPerUnit1";
            this.colSellPerUnit1.Visible = true;
            this.colSellPerUnit1.VisibleIndex = 4;
            this.colSellPerUnit1.Width = 82;
            // 
            // colEstimatedSell
            // 
            this.colEstimatedSell.Caption = "Estimated Sell";
            this.colEstimatedSell.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colEstimatedSell.FieldName = "EstimatedSell";
            this.colEstimatedSell.Name = "colEstimatedSell";
            this.colEstimatedSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedSell.Visible = true;
            this.colEstimatedSell.VisibleIndex = 7;
            this.colEstimatedSell.Width = 87;
            // 
            // colActualSell
            // 
            this.colActualSell.Caption = "Actual Sell";
            this.colActualSell.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colActualSell.FieldName = "ActualSell";
            this.colActualSell.Name = "colActualSell";
            this.colActualSell.OptionsColumn.AllowEdit = false;
            this.colActualSell.OptionsColumn.AllowFocus = false;
            this.colActualSell.OptionsColumn.ReadOnly = true;
            this.colActualSell.Visible = true;
            this.colActualSell.VisibleIndex = 8;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07130UTSurveyedPoleWorkPicturesListBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 448);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(933, 117);
            this.gridControl1.TabIndex = 46;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07130UTSurveyedPoleWorkPicturesListBindingSource
            // 
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource.DataMember = "sp07130_UT_Surveyed_Pole_Work_Pictures_List";
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.colShortLinkedRecordDescription});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn28, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Survey Picture ID";
            this.gridColumn23.FieldName = "SurveyPictureID";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Width = 105;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Linked To Record ID";
            this.gridColumn24.FieldName = "LinkedToRecordID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 117;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Linked To Record Type ID";
            this.gridColumn25.FieldName = "LinkedToRecordTypeID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 144;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Picture Type ID";
            this.gridColumn26.FieldName = "PictureTypeID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 95;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Picture Path";
            this.gridColumn27.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.gridColumn27.FieldName = "PicturePath";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 2;
            this.gridColumn27.Width = 323;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Date Taken";
            this.gridColumn28.FieldName = "DateTimeTaken";
            this.gridColumn28.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 0;
            this.gridColumn28.Width = 99;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Added By Staff ID";
            this.gridColumn29.FieldName = "AddedByStaffID";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 108;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "GUID";
            this.gridColumn30.FieldName = "GUID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Remarks";
            this.gridColumn31.FieldName = "Remarks";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 3;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Linked To Survey";
            this.gridColumn32.FieldName = "LinkedRecordDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 303;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Added By";
            this.gridColumn33.FieldName = "AddedByStaffName";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 1;
            this.gridColumn33.Width = 108;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To Tree";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 231;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // DateScheduledDateEdit
            // 
            this.DateScheduledDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "DateScheduled", true));
            this.DateScheduledDateEdit.EditValue = null;
            this.DateScheduledDateEdit.Location = new System.Drawing.Point(464, 93);
            this.DateScheduledDateEdit.MenuManager = this.barManager1;
            this.DateScheduledDateEdit.Name = "DateScheduledDateEdit";
            this.DateScheduledDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateScheduledDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateScheduledDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateScheduledDateEdit.Properties.Mask.EditMask = "g";
            this.DateScheduledDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateScheduledDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.DateScheduledDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateScheduledDateEdit.Size = new System.Drawing.Size(200, 20);
            this.DateScheduledDateEdit.StyleController = this.dataLayoutControl1;
            this.DateScheduledDateEdit.TabIndex = 3;
            // 
            // DateRaisedDateEdit
            // 
            this.DateRaisedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "DateRaised", true));
            this.DateRaisedDateEdit.EditValue = null;
            this.DateRaisedDateEdit.Location = new System.Drawing.Point(122, 93);
            this.DateRaisedDateEdit.MenuManager = this.barManager1;
            this.DateRaisedDateEdit.Name = "DateRaisedDateEdit";
            this.DateRaisedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateRaisedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateRaisedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateRaisedDateEdit.Properties.Mask.EditMask = "g";
            this.DateRaisedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateRaisedDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Size = new System.Drawing.Size(228, 20);
            this.DateRaisedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateRaisedDateEdit.TabIndex = 2;
            // 
            // ActionIDTextEdit
            // 
            this.ActionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ActionID", true));
            this.ActionIDTextEdit.Location = new System.Drawing.Point(156, 375);
            this.ActionIDTextEdit.MenuManager = this.barManager1;
            this.ActionIDTextEdit.Name = "ActionIDTextEdit";
            this.ActionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ActionIDTextEdit, true);
            this.ActionIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ActionIDTextEdit, optionsSpelling8);
            this.ActionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ActionIDTextEdit.TabIndex = 31;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07128UTSurveyedTreeWorkEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(144, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(226, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SurveyedTreeIDTextEdit
            // 
            this.SurveyedTreeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "SurveyedTreeID", true));
            this.SurveyedTreeIDTextEdit.Location = new System.Drawing.Point(156, 423);
            this.SurveyedTreeIDTextEdit.MenuManager = this.barManager1;
            this.SurveyedTreeIDTextEdit.Name = "SurveyedTreeIDTextEdit";
            this.SurveyedTreeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyedTreeIDTextEdit, true);
            this.SurveyedTreeIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyedTreeIDTextEdit, optionsSpelling9);
            this.SurveyedTreeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyedTreeIDTextEdit.TabIndex = 4;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 243);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(909, 108);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling10);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // ReferenceNumberButtonEdit
            // 
            this.ReferenceNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberButtonEdit.Location = new System.Drawing.Point(122, 59);
            this.ReferenceNumberButtonEdit.MenuManager = this.barManager1;
            this.ReferenceNumberButtonEdit.Name = "ReferenceNumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Text = "Sequence button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem5.Text = "Number button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.ReferenceNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "Sequence", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "Number", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ReferenceNumberButtonEdit.Properties.MaxLength = 50;
            this.ReferenceNumberButtonEdit.Size = new System.Drawing.Size(847, 20);
            this.ReferenceNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberButtonEdit.TabIndex = 1;
            this.ReferenceNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ReferenceNumberButtonEdit_ButtonClick);
            this.ReferenceNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ReferenceNumberButtonEdit_Validating);
            // 
            // StumpTreatmentEcoPlugsSpinEdit
            // 
            this.StumpTreatmentEcoPlugsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "StumpTreatmentEcoPlugs", true));
            this.StumpTreatmentEcoPlugsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentEcoPlugsSpinEdit.Location = new System.Drawing.Point(154, 274);
            this.StumpTreatmentEcoPlugsSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentEcoPlugsSpinEdit.Name = "StumpTreatmentEcoPlugsSpinEdit";
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.Caption = "(Tick if Yes)";
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.ValueChecked = 1;
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.ValueUnchecked = 0;
            this.StumpTreatmentEcoPlugsSpinEdit.Size = new System.Drawing.Size(106, 19);
            this.StumpTreatmentEcoPlugsSpinEdit.StyleController = this.dataLayoutControl1;
            this.StumpTreatmentEcoPlugsSpinEdit.TabIndex = 93;
            // 
            // StumpTreatmentSprayingSpinEdit
            // 
            this.StumpTreatmentSprayingSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "StumpTreatmentSpraying", true));
            this.StumpTreatmentSprayingSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentSprayingSpinEdit.Location = new System.Drawing.Point(154, 297);
            this.StumpTreatmentSprayingSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentSprayingSpinEdit.Name = "StumpTreatmentSprayingSpinEdit";
            this.StumpTreatmentSprayingSpinEdit.Properties.Caption = "(Tick if Yes)";
            this.StumpTreatmentSprayingSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.StumpTreatmentSprayingSpinEdit.Properties.ValueChecked = 1;
            this.StumpTreatmentSprayingSpinEdit.Properties.ValueUnchecked = 0;
            this.StumpTreatmentSprayingSpinEdit.Size = new System.Drawing.Size(106, 19);
            this.StumpTreatmentSprayingSpinEdit.StyleController = this.dataLayoutControl1;
            this.StumpTreatmentSprayingSpinEdit.TabIndex = 94;
            // 
            // StumpTreatmentPaintSpinEdit
            // 
            this.StumpTreatmentPaintSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "StumpTreatmentPaint", true));
            this.StumpTreatmentPaintSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentPaintSpinEdit.Location = new System.Drawing.Point(154, 320);
            this.StumpTreatmentPaintSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentPaintSpinEdit.Name = "StumpTreatmentPaintSpinEdit";
            this.StumpTreatmentPaintSpinEdit.Properties.Caption = "(Tick if Yes)";
            this.StumpTreatmentPaintSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.StumpTreatmentPaintSpinEdit.Properties.ValueChecked = 1;
            this.StumpTreatmentPaintSpinEdit.Properties.ValueUnchecked = 0;
            this.StumpTreatmentPaintSpinEdit.Size = new System.Drawing.Size(106, 19);
            this.StumpTreatmentPaintSpinEdit.StyleController = this.dataLayoutControl1;
            this.StumpTreatmentPaintSpinEdit.TabIndex = 95;
            // 
            // TreeReplacementWhipsSpinEdit
            // 
            this.TreeReplacementWhipsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "TreeReplacementWhips", true));
            this.TreeReplacementWhipsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TreeReplacementWhipsSpinEdit.Location = new System.Drawing.Point(616, 274);
            this.TreeReplacementWhipsSpinEdit.MenuManager = this.barManager1;
            this.TreeReplacementWhipsSpinEdit.Name = "TreeReplacementWhipsSpinEdit";
            this.TreeReplacementWhipsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TreeReplacementWhipsSpinEdit.Properties.IsFloatValue = false;
            this.TreeReplacementWhipsSpinEdit.Properties.Mask.EditMask = "N00";
            this.TreeReplacementWhipsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TreeReplacementWhipsSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.TreeReplacementWhipsSpinEdit.StyleController = this.dataLayoutControl1;
            this.TreeReplacementWhipsSpinEdit.TabIndex = 97;
            // 
            // TreeReplacementStandardsSpinEdit
            // 
            this.TreeReplacementStandardsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07128UTSurveyedTreeWorkEditBindingSource, "TreeReplacementStandards", true));
            this.TreeReplacementStandardsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TreeReplacementStandardsSpinEdit.Location = new System.Drawing.Point(616, 298);
            this.TreeReplacementStandardsSpinEdit.MenuManager = this.barManager1;
            this.TreeReplacementStandardsSpinEdit.Name = "TreeReplacementStandardsSpinEdit";
            this.TreeReplacementStandardsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TreeReplacementStandardsSpinEdit.Properties.IsFloatValue = false;
            this.TreeReplacementStandardsSpinEdit.Properties.Mask.EditMask = "N00";
            this.TreeReplacementStandardsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TreeReplacementStandardsSpinEdit.Size = new System.Drawing.Size(103, 20);
            this.TreeReplacementStandardsSpinEdit.StyleController = this.dataLayoutControl1;
            this.TreeReplacementStandardsSpinEdit.TabIndex = 91;
            // 
            // ItemForApprovedByStaffID
            // 
            this.ItemForApprovedByStaffID.Control = this.ApprovedByStaffIDTextEdit;
            this.ItemForApprovedByStaffID.CustomizationFormText = "Approved By Staff ID:";
            this.ItemForApprovedByStaffID.Location = new System.Drawing.Point(292, 168);
            this.ItemForApprovedByStaffID.Name = "ItemForApprovedByStaffID";
            this.ItemForApprovedByStaffID.Size = new System.Drawing.Size(292, 48);
            this.ItemForApprovedByStaffID.Text = "Approved By Staff ID:";
            this.ItemForApprovedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyedTreeID
            // 
            this.ItemForSurveyedTreeID.Control = this.SurveyedTreeIDTextEdit;
            this.ItemForSurveyedTreeID.CustomizationFormText = "Surveyed Tree ID:";
            this.ItemForSurveyedTreeID.Location = new System.Drawing.Point(0, 192);
            this.ItemForSurveyedTreeID.Name = "ItemForSurveyedTreeID";
            this.ItemForSurveyedTreeID.Size = new System.Drawing.Size(584, 24);
            this.ItemForSurveyedTreeID.Text = "Surveyed Tree ID:";
            this.ItemForSurveyedTreeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForJobTypeID
            // 
            this.ItemForJobTypeID.Control = this.JobTypeIDTextEdit;
            this.ItemForJobTypeID.CustomizationFormText = "Job Type ID:";
            this.ItemForJobTypeID.Location = new System.Drawing.Point(0, 168);
            this.ItemForJobTypeID.Name = "ItemForJobTypeID";
            this.ItemForJobTypeID.Size = new System.Drawing.Size(584, 24);
            this.ItemForJobTypeID.Text = "Job Type ID:";
            this.ItemForJobTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForActionID
            // 
            this.ItemForActionID.Control = this.ActionIDTextEdit;
            this.ItemForActionID.CustomizationFormText = "Action ID:";
            this.ItemForActionID.Location = new System.Drawing.Point(0, 144);
            this.ItemForActionID.Name = "ItemForActionID";
            this.ItemForActionID.Size = new System.Drawing.Size(584, 24);
            this.ItemForActionID.Text = "Action ID:";
            this.ItemForActionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForApproved
            // 
            this.ItemForApproved.Control = this.ApprovedCheckEdit;
            this.ItemForApproved.CustomizationFormText = "Approved:";
            this.ItemForApproved.Location = new System.Drawing.Point(0, 105);
            this.ItemForApproved.Name = "ItemForApproved";
            this.ItemForApproved.Size = new System.Drawing.Size(342, 24);
            this.ItemForApproved.Text = "Approved:";
            this.ItemForApproved.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 181);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(937, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyDate
            // 
            this.ItemForSurveyDate.Control = this.SurveyDateTextEdit;
            this.ItemForSurveyDate.Location = new System.Drawing.Point(0, 181);
            this.ItemForSurveyDate.Name = "ItemForSurveyDate";
            this.ItemForSurveyDate.Size = new System.Drawing.Size(937, 24);
            this.ItemForSurveyDate.Text = "Survey Date:";
            this.ItemForSurveyDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(981, 589);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForReferenceNumber,
            this.tabbedControlGroup2,
            this.ItemForJobDescription,
            this.ItemForDateRaised,
            this.emptySpaceItem3,
            this.layoutControlItem6,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForPossibleFlail,
            this.ItemForDateScheduled,
            this.ItemForDateCompleted,
            this.ItemForAchievableClearance,
            this.ItemForPossibleLiveWork});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(961, 569);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(132, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(132, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(132, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(362, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(599, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(132, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(230, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(961, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 139);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(961, 228);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(937, 182);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.ItemForEstimatedPZHours,
            this.layoutControlItem8,
            this.emptySpaceItem8,
            this.ItemForActualPZHours});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(913, 134);
            this.layoutControlGroup5.Text = "Hours";
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 49);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(520, 85);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEstimatedPZHours
            // 
            this.ItemForEstimatedPZHours.Control = this.EstimatedHoursSpinEdit;
            this.ItemForEstimatedPZHours.CustomizationFormText = "Estimated Hours:";
            this.ItemForEstimatedPZHours.Location = new System.Drawing.Point(0, 25);
            this.ItemForEstimatedPZHours.Name = "ItemForEstimatedPZHours";
            this.ItemForEstimatedPZHours.Size = new System.Drawing.Size(260, 24);
            this.ItemForEstimatedPZHours.Text = "Estimated Hours:";
            this.ItemForEstimatedPZHours.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.InformationLabelControl;
            this.layoutControlItem8.CustomizationFormText = "Hours Information:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 25);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(319, 25);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(520, 25);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Hours Information:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(520, 0);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(393, 134);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActualPZHours
            // 
            this.ItemForActualPZHours.Control = this.ActualHoursSpinEdit;
            this.ItemForActualPZHours.CustomizationFormText = "Actual Hours:";
            this.ItemForActualPZHours.Location = new System.Drawing.Point(260, 25);
            this.ItemForActualPZHours.Name = "ItemForActualPZHours";
            this.ItemForActualPZHours.Size = new System.Drawing.Size(260, 24);
            this.ItemForActualPZHours.Text = "Actual Hours:";
            this.ItemForActualPZHours.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Mapping";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup16,
            this.layoutControlGroup17,
            this.layoutControlGroup18,
            this.layoutControlGroup19,
            this.splitterItem2,
            this.splitterItem3,
            this.splitterItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(913, 134);
            this.layoutControlGroup7.Text = "Costs";
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.AllowHtmlStringInCaption = true;
            this.layoutControlGroup16.CustomizationFormText = "Estimated <b>Cost</b>";
            this.layoutControlGroup16.ExpandButtonVisible = true;
            this.layoutControlGroup16.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEstimatedLabourCost,
            this.ItemForEstimatedMaterialsCost,
            this.ItemForEstimatedEquipmentCost,
            this.ItemForEstimatedTotalCost});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup16.Size = new System.Drawing.Size(233, 134);
            this.layoutControlGroup16.Text = "Estimated <b>Cost</b>";
            // 
            // ItemForEstimatedLabourCost
            // 
            this.ItemForEstimatedLabourCost.Control = this.EstimatedLabourCostSpinEdit;
            this.ItemForEstimatedLabourCost.CustomizationFormText = "Estimated Labour Cost:";
            this.ItemForEstimatedLabourCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForEstimatedLabourCost.Name = "ItemForEstimatedLabourCost";
            this.ItemForEstimatedLabourCost.Size = new System.Drawing.Size(217, 24);
            this.ItemForEstimatedLabourCost.Text = "Labour:";
            this.ItemForEstimatedLabourCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedMaterialsCost
            // 
            this.ItemForEstimatedMaterialsCost.Control = this.EstimatedMaterialsCostSpinEdit;
            this.ItemForEstimatedMaterialsCost.CustomizationFormText = "Estimated Materials Cost:";
            this.ItemForEstimatedMaterialsCost.Location = new System.Drawing.Point(0, 24);
            this.ItemForEstimatedMaterialsCost.Name = "ItemForEstimatedMaterialsCost";
            this.ItemForEstimatedMaterialsCost.Size = new System.Drawing.Size(217, 24);
            this.ItemForEstimatedMaterialsCost.Text = "Materials:";
            this.ItemForEstimatedMaterialsCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedEquipmentCost
            // 
            this.ItemForEstimatedEquipmentCost.Control = this.EstimatedEquipmentCostSpinEdit;
            this.ItemForEstimatedEquipmentCost.CustomizationFormText = "Estimated Equipment Cost:";
            this.ItemForEstimatedEquipmentCost.Location = new System.Drawing.Point(0, 48);
            this.ItemForEstimatedEquipmentCost.Name = "ItemForEstimatedEquipmentCost";
            this.ItemForEstimatedEquipmentCost.Size = new System.Drawing.Size(217, 24);
            this.ItemForEstimatedEquipmentCost.Text = "Equipment:";
            this.ItemForEstimatedEquipmentCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedTotalCost
            // 
            this.ItemForEstimatedTotalCost.Control = this.EstimatedTotalCostSpinEdit;
            this.ItemForEstimatedTotalCost.CustomizationFormText = "Estimated Total Cost:";
            this.ItemForEstimatedTotalCost.Location = new System.Drawing.Point(0, 72);
            this.ItemForEstimatedTotalCost.Name = "ItemForEstimatedTotalCost";
            this.ItemForEstimatedTotalCost.Size = new System.Drawing.Size(217, 24);
            this.ItemForEstimatedTotalCost.Text = "Total:";
            this.ItemForEstimatedTotalCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.AllowHtmlStringInCaption = true;
            this.layoutControlGroup17.CustomizationFormText = "Actual <b>Cost</b>";
            this.layoutControlGroup17.ExpandButtonVisible = true;
            this.layoutControlGroup17.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActualLabourCost,
            this.ItemForActualMaterialsCost,
            this.ItemForActualEquipmentCost,
            this.ItemForActualTotalCost});
            this.layoutControlGroup17.Location = new System.Drawing.Point(239, 0);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup17.Size = new System.Drawing.Size(227, 134);
            this.layoutControlGroup17.Text = "Actual <b>Cost</b>";
            // 
            // ItemForActualLabourCost
            // 
            this.ItemForActualLabourCost.Control = this.ActualLabourCostSpinEdit;
            this.ItemForActualLabourCost.CustomizationFormText = "Actual Labour Cost:";
            this.ItemForActualLabourCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualLabourCost.Name = "ItemForActualLabourCost";
            this.ItemForActualLabourCost.Size = new System.Drawing.Size(211, 24);
            this.ItemForActualLabourCost.Text = "Labour:";
            this.ItemForActualLabourCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualMaterialsCost
            // 
            this.ItemForActualMaterialsCost.Control = this.ActualMaterialsCostSpinEdit;
            this.ItemForActualMaterialsCost.CustomizationFormText = "Actual Materials Cost:";
            this.ItemForActualMaterialsCost.Location = new System.Drawing.Point(0, 24);
            this.ItemForActualMaterialsCost.Name = "ItemForActualMaterialsCost";
            this.ItemForActualMaterialsCost.Size = new System.Drawing.Size(211, 24);
            this.ItemForActualMaterialsCost.Text = "Materials:";
            this.ItemForActualMaterialsCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualEquipmentCost
            // 
            this.ItemForActualEquipmentCost.Control = this.ActualEquipmentCostSpinEdit;
            this.ItemForActualEquipmentCost.CustomizationFormText = "Actual Equipment Cost:";
            this.ItemForActualEquipmentCost.Location = new System.Drawing.Point(0, 48);
            this.ItemForActualEquipmentCost.Name = "ItemForActualEquipmentCost";
            this.ItemForActualEquipmentCost.Size = new System.Drawing.Size(211, 24);
            this.ItemForActualEquipmentCost.Text = "Equipment:";
            this.ItemForActualEquipmentCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualTotalCost
            // 
            this.ItemForActualTotalCost.Control = this.ActualTotalCostSpinEdit;
            this.ItemForActualTotalCost.CustomizationFormText = "Actual Total Cost:";
            this.ItemForActualTotalCost.Location = new System.Drawing.Point(0, 72);
            this.ItemForActualTotalCost.Name = "ItemForActualTotalCost";
            this.ItemForActualTotalCost.Size = new System.Drawing.Size(211, 24);
            this.ItemForActualTotalCost.Text = "Total:";
            this.ItemForActualTotalCost.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.AllowHtmlStringInCaption = true;
            this.layoutControlGroup18.CustomizationFormText = "Estimated <b>Sell</b>";
            this.layoutControlGroup18.ExpandButtonVisible = true;
            this.layoutControlGroup18.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEstimatedLabourSell,
            this.ItemForEstimatedMaterialsSell,
            this.ItemForEstimatedEquipmentSell,
            this.ItemForEstimatedTotalSell});
            this.layoutControlGroup18.Location = new System.Drawing.Point(472, 0);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup18.Size = new System.Drawing.Size(225, 134);
            this.layoutControlGroup18.Text = "Estimated <b>Sell</b>";
            // 
            // ItemForEstimatedLabourSell
            // 
            this.ItemForEstimatedLabourSell.Control = this.EstimatedLabourSellSpinEdit;
            this.ItemForEstimatedLabourSell.CustomizationFormText = "Estimated Labour Sell:";
            this.ItemForEstimatedLabourSell.Location = new System.Drawing.Point(0, 0);
            this.ItemForEstimatedLabourSell.Name = "ItemForEstimatedLabourSell";
            this.ItemForEstimatedLabourSell.Size = new System.Drawing.Size(209, 24);
            this.ItemForEstimatedLabourSell.Text = "Labour:";
            this.ItemForEstimatedLabourSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedMaterialsSell
            // 
            this.ItemForEstimatedMaterialsSell.Control = this.EstimatedMaterialsSellSpinEdit;
            this.ItemForEstimatedMaterialsSell.CustomizationFormText = "Estimated Materials Sell:";
            this.ItemForEstimatedMaterialsSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForEstimatedMaterialsSell.Name = "ItemForEstimatedMaterialsSell";
            this.ItemForEstimatedMaterialsSell.Size = new System.Drawing.Size(209, 24);
            this.ItemForEstimatedMaterialsSell.Text = "Materials:";
            this.ItemForEstimatedMaterialsSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedEquipmentSell
            // 
            this.ItemForEstimatedEquipmentSell.Control = this.EstimatedEquipmentSellSpinEdit;
            this.ItemForEstimatedEquipmentSell.CustomizationFormText = "Estimated Equipment Sell:";
            this.ItemForEstimatedEquipmentSell.Location = new System.Drawing.Point(0, 48);
            this.ItemForEstimatedEquipmentSell.Name = "ItemForEstimatedEquipmentSell";
            this.ItemForEstimatedEquipmentSell.Size = new System.Drawing.Size(209, 24);
            this.ItemForEstimatedEquipmentSell.Text = "Equipment:";
            this.ItemForEstimatedEquipmentSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForEstimatedTotalSell
            // 
            this.ItemForEstimatedTotalSell.Control = this.EstimatedTotalSellSpinEdit;
            this.ItemForEstimatedTotalSell.CustomizationFormText = "Estimated Total Sell:";
            this.ItemForEstimatedTotalSell.Location = new System.Drawing.Point(0, 72);
            this.ItemForEstimatedTotalSell.Name = "ItemForEstimatedTotalSell";
            this.ItemForEstimatedTotalSell.Size = new System.Drawing.Size(209, 24);
            this.ItemForEstimatedTotalSell.Text = "Total:";
            this.ItemForEstimatedTotalSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.AllowHtmlStringInCaption = true;
            this.layoutControlGroup19.CustomizationFormText = "Actual <b>Sell</b>";
            this.layoutControlGroup19.ExpandButtonVisible = true;
            this.layoutControlGroup19.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForActualLabourSell,
            this.ItemForActualMaterialsSell,
            this.ItemForActualEquipmentSell,
            this.ItemForActualTotalSell});
            this.layoutControlGroup19.Location = new System.Drawing.Point(703, 0);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup19.Size = new System.Drawing.Size(210, 134);
            this.layoutControlGroup19.Text = "Actual <b>Sell</b>";
            // 
            // ItemForActualLabourSell
            // 
            this.ItemForActualLabourSell.Control = this.ActualLabourSellSpinEdit;
            this.ItemForActualLabourSell.CustomizationFormText = "Actual Labour Sell:";
            this.ItemForActualLabourSell.Location = new System.Drawing.Point(0, 0);
            this.ItemForActualLabourSell.Name = "ItemForActualLabourSell";
            this.ItemForActualLabourSell.Size = new System.Drawing.Size(194, 24);
            this.ItemForActualLabourSell.Text = "Labour:";
            this.ItemForActualLabourSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualMaterialsSell
            // 
            this.ItemForActualMaterialsSell.Control = this.ActualMaterialsSellSpinEdit;
            this.ItemForActualMaterialsSell.CustomizationFormText = "Actual Materials Sell:";
            this.ItemForActualMaterialsSell.Location = new System.Drawing.Point(0, 24);
            this.ItemForActualMaterialsSell.Name = "ItemForActualMaterialsSell";
            this.ItemForActualMaterialsSell.Size = new System.Drawing.Size(194, 24);
            this.ItemForActualMaterialsSell.Text = "Materials:";
            this.ItemForActualMaterialsSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualEquipmentSell
            // 
            this.ItemForActualEquipmentSell.Control = this.ActualEquipmentSellSpinEdit;
            this.ItemForActualEquipmentSell.CustomizationFormText = "Actual Equipment Sell:";
            this.ItemForActualEquipmentSell.Location = new System.Drawing.Point(0, 48);
            this.ItemForActualEquipmentSell.Name = "ItemForActualEquipmentSell";
            this.ItemForActualEquipmentSell.Size = new System.Drawing.Size(194, 24);
            this.ItemForActualEquipmentSell.Text = "Equipment:";
            this.ItemForActualEquipmentSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForActualTotalSell
            // 
            this.ItemForActualTotalSell.Control = this.ActualTotalSellSpinEdit;
            this.ItemForActualTotalSell.CustomizationFormText = "Actual Total Sell:";
            this.ItemForActualTotalSell.Location = new System.Drawing.Point(0, 72);
            this.ItemForActualTotalSell.Name = "ItemForActualTotalSell";
            this.ItemForActualTotalSell.Size = new System.Drawing.Size(194, 24);
            this.ItemForActualTotalSell.Text = "Total:";
            this.ItemForActualTotalSell.TextSize = new System.Drawing.Size(107, 13);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(233, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 134);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.CustomizationFormText = "splitterItem3";
            this.splitterItem3.Location = new System.Drawing.Point(466, 0);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(6, 134);
            // 
            // splitterItem4
            // 
            this.splitterItem4.AllowHotTrack = true;
            this.splitterItem4.CustomizationFormText = "splitterItem4";
            this.splitterItem4.Location = new System.Drawing.Point(697, 0);
            this.splitterItem4.Name = "splitterItem4";
            this.splitterItem4.Size = new System.Drawing.Size(6, 134);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Stump Treatment \\ Tree Replacement";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.splitterItem1});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(913, 134);
            this.layoutControlGroup8.Text = "Stump Treatment \\ Tree Replacement";
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Stump Treatment";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStumpTreatmentEcoPlugs,
            this.ItemForStumpTreatmentSpraying,
            this.ItemForStumpTreatmentPaint,
            this.ItemForStumpTreatmentNone,
            this.emptySpaceItem9,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup12.Size = new System.Drawing.Size(456, 134);
            this.layoutControlGroup12.Text = "Stump Treatment";
            // 
            // ItemForStumpTreatmentEcoPlugs
            // 
            this.ItemForStumpTreatmentEcoPlugs.Control = this.StumpTreatmentEcoPlugsSpinEdit;
            this.ItemForStumpTreatmentEcoPlugs.CustomizationFormText = "Eco Plugs:";
            this.ItemForStumpTreatmentEcoPlugs.Location = new System.Drawing.Point(0, 23);
            this.ItemForStumpTreatmentEcoPlugs.MaxSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentEcoPlugs.MinSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentEcoPlugs.Name = "ItemForStumpTreatmentEcoPlugs";
            this.ItemForStumpTreatmentEcoPlugs.Size = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentEcoPlugs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStumpTreatmentEcoPlugs.Text = "Eco Plugs:";
            this.ItemForStumpTreatmentEcoPlugs.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForStumpTreatmentSpraying
            // 
            this.ItemForStumpTreatmentSpraying.Control = this.StumpTreatmentSprayingSpinEdit;
            this.ItemForStumpTreatmentSpraying.CustomizationFormText = "Spraying:";
            this.ItemForStumpTreatmentSpraying.Location = new System.Drawing.Point(0, 46);
            this.ItemForStumpTreatmentSpraying.MaxSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentSpraying.MinSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentSpraying.Name = "ItemForStumpTreatmentSpraying";
            this.ItemForStumpTreatmentSpraying.Size = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentSpraying.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStumpTreatmentSpraying.Text = "Spraying:";
            this.ItemForStumpTreatmentSpraying.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForStumpTreatmentPaint
            // 
            this.ItemForStumpTreatmentPaint.Control = this.StumpTreatmentPaintSpinEdit;
            this.ItemForStumpTreatmentPaint.CustomizationFormText = "Paint:";
            this.ItemForStumpTreatmentPaint.Location = new System.Drawing.Point(0, 69);
            this.ItemForStumpTreatmentPaint.MaxSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentPaint.MinSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentPaint.Name = "ItemForStumpTreatmentPaint";
            this.ItemForStumpTreatmentPaint.Size = new System.Drawing.Size(220, 27);
            this.ItemForStumpTreatmentPaint.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStumpTreatmentPaint.Text = "Paint:";
            this.ItemForStumpTreatmentPaint.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForStumpTreatmentNone
            // 
            this.ItemForStumpTreatmentNone.Control = this.StumpTreatmentNoneCheckEdit;
            this.ItemForStumpTreatmentNone.CustomizationFormText = "None:";
            this.ItemForStumpTreatmentNone.Location = new System.Drawing.Point(0, 0);
            this.ItemForStumpTreatmentNone.MaxSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentNone.MinSize = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentNone.Name = "ItemForStumpTreatmentNone";
            this.ItemForStumpTreatmentNone.Size = new System.Drawing.Size(220, 23);
            this.ItemForStumpTreatmentNone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStumpTreatmentNone.Text = "None:";
            this.ItemForStumpTreatmentNone.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(220, 0);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(220, 23);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(220, 23);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(220, 23);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.Location = new System.Drawing.Point(220, 46);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(220, 23);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(220, 69);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(220, 27);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Tree Replacement";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTreeReplacementWhips,
            this.ItemForTreeReplacementStandards,
            this.ItemForTreeReplacementNone,
            this.emptySpaceItem13,
            this.emptySpaceItem14,
            this.emptySpaceItem15});
            this.layoutControlGroup13.Location = new System.Drawing.Point(462, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup13.Size = new System.Drawing.Size(451, 134);
            this.layoutControlGroup13.Text = "Tree Replacement";
            // 
            // ItemForTreeReplacementWhips
            // 
            this.ItemForTreeReplacementWhips.Control = this.TreeReplacementWhipsSpinEdit;
            this.ItemForTreeReplacementWhips.CustomizationFormText = "Whips:";
            this.ItemForTreeReplacementWhips.Location = new System.Drawing.Point(0, 23);
            this.ItemForTreeReplacementWhips.MaxSize = new System.Drawing.Size(217, 24);
            this.ItemForTreeReplacementWhips.MinSize = new System.Drawing.Size(217, 24);
            this.ItemForTreeReplacementWhips.Name = "ItemForTreeReplacementWhips";
            this.ItemForTreeReplacementWhips.Size = new System.Drawing.Size(217, 24);
            this.ItemForTreeReplacementWhips.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreeReplacementWhips.Text = "Whips:";
            this.ItemForTreeReplacementWhips.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForTreeReplacementStandards
            // 
            this.ItemForTreeReplacementStandards.Control = this.TreeReplacementStandardsSpinEdit;
            this.ItemForTreeReplacementStandards.CustomizationFormText = "Standards:";
            this.ItemForTreeReplacementStandards.Location = new System.Drawing.Point(0, 47);
            this.ItemForTreeReplacementStandards.MaxSize = new System.Drawing.Size(217, 24);
            this.ItemForTreeReplacementStandards.MinSize = new System.Drawing.Size(217, 24);
            this.ItemForTreeReplacementStandards.Name = "ItemForTreeReplacementStandards";
            this.ItemForTreeReplacementStandards.Size = new System.Drawing.Size(217, 49);
            this.ItemForTreeReplacementStandards.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreeReplacementStandards.Text = "Standards:";
            this.ItemForTreeReplacementStandards.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForTreeReplacementNone
            // 
            this.ItemForTreeReplacementNone.Control = this.TreeReplacementNoneCheckEdit;
            this.ItemForTreeReplacementNone.CustomizationFormText = "None:";
            this.ItemForTreeReplacementNone.Location = new System.Drawing.Point(0, 0);
            this.ItemForTreeReplacementNone.MaxSize = new System.Drawing.Size(217, 23);
            this.ItemForTreeReplacementNone.MinSize = new System.Drawing.Size(217, 23);
            this.ItemForTreeReplacementNone.Name = "ItemForTreeReplacementNone";
            this.ItemForTreeReplacementNone.Size = new System.Drawing.Size(217, 23);
            this.ItemForTreeReplacementNone.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreeReplacementNone.Text = "None:";
            this.ItemForTreeReplacementNone.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(217, 0);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(218, 23);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(217, 23);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(218, 24);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(217, 47);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(218, 49);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(456, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 134);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Other";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWorkOrderID,
            this.ItemForSelfBillingInvoiceID,
            this.ItemForFinanceSystemBillingID,
            this.ItemForRevisitDate,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.emptySpaceItem18,
            this.emptySpaceItem19,
            this.ItemForTeamOnHold});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(913, 134);
            this.layoutControlGroup3.Text = "Other";
            // 
            // ItemForWorkOrderID
            // 
            this.ItemForWorkOrderID.Control = this.WorkOrderIDTextEdit;
            this.ItemForWorkOrderID.CustomizationFormText = "Work Order:";
            this.ItemForWorkOrderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForWorkOrderID.Name = "ItemForWorkOrderID";
            this.ItemForWorkOrderID.Size = new System.Drawing.Size(456, 24);
            this.ItemForWorkOrderID.Text = "Work Order:";
            this.ItemForWorkOrderID.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForSelfBillingInvoiceID
            // 
            this.ItemForSelfBillingInvoiceID.Control = this.SelfBillingInvoiceIDTextEdit;
            this.ItemForSelfBillingInvoiceID.CustomizationFormText = "Self Billing Invoice Number:";
            this.ItemForSelfBillingInvoiceID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSelfBillingInvoiceID.Name = "ItemForSelfBillingInvoiceID";
            this.ItemForSelfBillingInvoiceID.Size = new System.Drawing.Size(456, 24);
            this.ItemForSelfBillingInvoiceID.Text = "Self Billing Invoice #:";
            this.ItemForSelfBillingInvoiceID.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForFinanceSystemBillingID
            // 
            this.ItemForFinanceSystemBillingID.Control = this.FinanceSystemBillingIDTextEdit;
            this.ItemForFinanceSystemBillingID.CustomizationFormText = "Finance Billing Number:";
            this.ItemForFinanceSystemBillingID.Location = new System.Drawing.Point(0, 48);
            this.ItemForFinanceSystemBillingID.Name = "ItemForFinanceSystemBillingID";
            this.ItemForFinanceSystemBillingID.Size = new System.Drawing.Size(456, 24);
            this.ItemForFinanceSystemBillingID.Text = "Finance Billing #:";
            this.ItemForFinanceSystemBillingID.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForRevisitDate
            // 
            this.ItemForRevisitDate.Control = this.RevisitDateDateEdit;
            this.ItemForRevisitDate.CustomizationFormText = "Revisit Date:";
            this.ItemForRevisitDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForRevisitDate.Name = "ItemForRevisitDate";
            this.ItemForRevisitDate.Size = new System.Drawing.Size(456, 24);
            this.ItemForRevisitDate.Text = "Revisit Date:";
            this.ItemForRevisitDate.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(456, 0);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(457, 24);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(456, 24);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(457, 24);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(456, 48);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(457, 24);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(456, 72);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(457, 62);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTeamOnHold
            // 
            this.ItemForTeamOnHold.Control = this.TeamOnHoldCheckEdit;
            this.ItemForTeamOnHold.Location = new System.Drawing.Point(0, 96);
            this.ItemForTeamOnHold.Name = "ItemForTeamOnHold";
            this.ItemForTeamOnHold.Size = new System.Drawing.Size(456, 38);
            this.ItemForTeamOnHold.Text = "Team On-Hold:";
            this.ItemForTeamOnHold.TextSize = new System.Drawing.Size(107, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks,
            this.layoutControlItem7});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(913, 134);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 22);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(913, 112);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.RemarksInformationLabel;
            this.layoutControlItem7.CustomizationFormText = "Remarks Information:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 22);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(368, 22);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(913, 22);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Remarks Information:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.AllowHide = false;
            this.ItemForReferenceNumber.Control = this.ReferenceNumberButtonEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(0, 47);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(961, 24);
            this.ItemForReferenceNumber.Text = "Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(107, 13);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 403);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup9;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(961, 166);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup10,
            this.layoutControlGroup11});
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Linked Access Issues";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(937, 121);
            this.layoutControlGroup9.Text = "Work Pictures";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Access Issues Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(937, 121);
            this.layoutControlItem2.Text = "Linked Access Issues Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Linked Environmental Issues";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(937, 121);
            this.layoutControlGroup10.Text = "Equipment";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "Linked Environmental Issues Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(937, 121);
            this.layoutControlItem3.Text = "Linked Environmental Issues Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Linked Site Hazards";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(937, 121);
            this.layoutControlGroup11.Text = "Materials";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl3;
            this.layoutControlItem5.CustomizationFormText = "Linked Site Hazards Grid:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(937, 121);
            this.layoutControlItem5.Text = "Linked Site Hazards Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // ItemForJobDescription
            // 
            this.ItemForJobDescription.Control = this.JobDescriptionButtonEdit;
            this.ItemForJobDescription.CustomizationFormText = "Job Description:";
            this.ItemForJobDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForJobDescription.Name = "ItemForJobDescription";
            this.ItemForJobDescription.Size = new System.Drawing.Size(961, 24);
            this.ItemForJobDescription.Text = "Job Description:";
            this.ItemForJobDescription.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForDateRaised
            // 
            this.ItemForDateRaised.Control = this.DateRaisedDateEdit;
            this.ItemForDateRaised.CustomizationFormText = "Date Raised:";
            this.ItemForDateRaised.Location = new System.Drawing.Point(0, 81);
            this.ItemForDateRaised.Name = "ItemForDateRaised";
            this.ItemForDateRaised.Size = new System.Drawing.Size(342, 24);
            this.ItemForDateRaised.Text = "Date Raised:";
            this.ItemForDateRaised.TextSize = new System.Drawing.Size(107, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 129);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(961, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnSave;
            this.layoutControlItem6.CustomizationFormText = "Save Button:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 367);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Save Button:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 393);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(961, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(103, 367);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(858, 26);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPossibleFlail
            // 
            this.ItemForPossibleFlail.Control = this.PossibleFlailCheckEdit;
            this.ItemForPossibleFlail.CustomizationFormText = "Possible Flail:";
            this.ItemForPossibleFlail.Location = new System.Drawing.Point(656, 105);
            this.ItemForPossibleFlail.Name = "ItemForPossibleFlail";
            this.ItemForPossibleFlail.Size = new System.Drawing.Size(305, 24);
            this.ItemForPossibleFlail.Text = "Possible Flail:";
            this.ItemForPossibleFlail.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForDateScheduled
            // 
            this.ItemForDateScheduled.Control = this.DateScheduledDateEdit;
            this.ItemForDateScheduled.CustomizationFormText = "Date Scheduled:";
            this.ItemForDateScheduled.Location = new System.Drawing.Point(342, 81);
            this.ItemForDateScheduled.Name = "ItemForDateScheduled";
            this.ItemForDateScheduled.Size = new System.Drawing.Size(314, 24);
            this.ItemForDateScheduled.Text = "Date Scheduled:";
            this.ItemForDateScheduled.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForDateCompleted
            // 
            this.ItemForDateCompleted.Control = this.DateCompletedDateEdit;
            this.ItemForDateCompleted.CustomizationFormText = "Date Completed:";
            this.ItemForDateCompleted.Location = new System.Drawing.Point(656, 81);
            this.ItemForDateCompleted.Name = "ItemForDateCompleted";
            this.ItemForDateCompleted.Size = new System.Drawing.Size(305, 24);
            this.ItemForDateCompleted.Text = "Date Completed:";
            this.ItemForDateCompleted.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForAchievableClearance
            // 
            this.ItemForAchievableClearance.Control = this.AchievableClearanceSpinEdit;
            this.ItemForAchievableClearance.CustomizationFormText = "Achievable Clearance:";
            this.ItemForAchievableClearance.Location = new System.Drawing.Point(0, 105);
            this.ItemForAchievableClearance.Name = "ItemForAchievableClearance";
            this.ItemForAchievableClearance.Size = new System.Drawing.Size(342, 24);
            this.ItemForAchievableClearance.Text = "Achievable Clearance:";
            this.ItemForAchievableClearance.TextSize = new System.Drawing.Size(107, 13);
            // 
            // ItemForPossibleLiveWork
            // 
            this.ItemForPossibleLiveWork.Control = this.PossibleLiveWorkCheckEdit;
            this.ItemForPossibleLiveWork.CustomizationFormText = "Possible Live Work:";
            this.ItemForPossibleLiveWork.Location = new System.Drawing.Point(342, 105);
            this.ItemForPossibleLiveWork.Name = "ItemForPossibleLiveWork";
            this.ItemForPossibleLiveWork.Size = new System.Drawing.Size(314, 24);
            this.ItemForPossibleLiveWork.Text = "Possible Live Work:";
            this.ItemForPossibleLiveWork.TextSize = new System.Drawing.Size(107, 13);
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl2;
            this.layoutControlItem4.CustomizationFormText = "Linked Environmental Issues Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem3";
            this.layoutControlItem4.Size = new System.Drawing.Size(584, 127);
            this.layoutControlItem4.Text = "Linked Environmental Issues Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(161, 13);
            // 
            // sp07128_UT_Surveyed_Tree_Work_EditTableAdapter
            // 
            this.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter
            // 
            this.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter
            // 
            this.sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter.ClearBeforeFill = true;
            // 
            // sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter
            // 
            this.sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07138_UT_Unit_Descriptors_With_BlankTableAdapter
            // 
            this.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_UT_Surveyed_Tree_Work_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(981, 645);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Surveyed_Tree_Work_Edit";
            this.Text = "Edit Surveyed Tree Work";
            this.Activated += new System.EventHandler(this.frm_UT_Surveyed_Tree_Work_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Surveyed_Tree_Work_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Surveyed_Tree_Work_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07128UTSurveyedTreeWorkEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamOnHoldCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PossibleLiveWorkCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementNoneCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentNoneCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RevisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AchievableClearanceSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PossibleFlailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedEquipmentSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialsSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedMaterialsSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedLabourSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedTotalSellSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FinanceSystemBillingIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelfBillingInvoiceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedTotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedEquipmentCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualMaterialsCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedMaterialsCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedLabourCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedHoursSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JobTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07138UTUnitDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditMaterialDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07130UTSurveyedPoleWorkPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateScheduledDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateScheduledDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyedTreeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentEcoPlugsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentSprayingSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentPaintSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementWhipsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementStandardsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyedTreeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApproved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedPZHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualPZHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedMaterialsCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialsCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedMaterialsSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedEquipmentSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualLabourSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualMaterialsSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualEquipmentSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualTotalSell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentEcoPlugs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentSpraying)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentPaint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpTreatmentNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementWhips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementStandards)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacementNone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSelfBillingInvoiceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFinanceSystemBillingID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRevisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamOnHold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForJobDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPossibleFlail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateScheduled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAchievableClearance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPossibleLiveWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit SurveyedTreeIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyedTreeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraEditors.TextEdit ActionIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionID;
        private DevExpress.XtraEditors.DateEdit DateRaisedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRaised;
        private DevExpress.XtraEditors.DateEdit DateScheduledDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateScheduled;
        private DevExpress.XtraEditors.TextEdit JobTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.CheckEdit ApprovedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApproved;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp07128UTSurveyedTreeWorkEditBindingSource;
        private DataSet_UT_EditTableAdapters.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter sp07128_UT_Surveyed_Tree_Work_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit JobDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForJobDescription;
        private DevExpress.XtraEditors.ButtonEdit ReferenceNumberButtonEdit;
        private DevExpress.XtraEditors.DateEdit DateCompletedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCompleted;
        private DevExpress.XtraEditors.TextEdit ApprovedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApprovedByStaffID;
        private DevExpress.XtraEditors.SpinEdit EstimatedHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedPZHours;
        private DevExpress.XtraEditors.SpinEdit ActualHoursSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualPZHours;
        private DevExpress.XtraEditors.SpinEdit ActualTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedTotalCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedEquipmentCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualMaterialsCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedMaterialsCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit ActualLabourCostSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedLabourCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualLabourCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedMaterialsCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualMaterialsCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedEquipmentCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEquipmentCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedTotalCost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualTotalCost;
        private DevExpress.XtraEditors.TextEdit WorkOrderIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderID;
        private DevExpress.XtraEditors.TextEdit SelfBillingInvoiceIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSelfBillingInvoiceID;
        private DevExpress.XtraEditors.TextEdit FinanceSystemBillingIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFinanceSystemBillingID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource sp07130UTSurveyedPoleWorkPicturesListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private System.Windows.Forms.BindingSource sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromDefaultRequiredEquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualCost;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DataSet_UT_EditTableAdapters.sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditEquipment;
        private System.Windows.Forms.BindingSource sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colActionMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colActualUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsDescriptorID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnitDescriptor;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedCost1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colActualCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs1;
        private DevExpress.XtraGrid.Columns.GridColumn colMaterialDescription;
        private DataSet_UT_EditTableAdapters.sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter;
        private System.Windows.Forms.BindingSource sp07138UTUnitDescriptorsWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter sp07138_UT_Unit_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditMaterialDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colCostPerUnit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSellPerUnit1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.SpinEdit ActualTotalSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedTotalSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedTotalSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedSell1;
        private DevExpress.XtraGrid.Columns.GridColumn colActualSell1;
        private DevExpress.XtraEditors.SpinEdit EstimatedLabourSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedLabourSell;
        private DevExpress.XtraEditors.SpinEdit ActualLabourSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualLabourSell;
        private DevExpress.XtraEditors.SpinEdit ActualMaterialsSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedMaterialsSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedMaterialsSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualMaterialsSell;
        private DevExpress.XtraEditors.SpinEdit ActualEquipmentSellSpinEdit;
        private DevExpress.XtraEditors.SpinEdit EstimatedEquipmentSellSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedEquipmentSell;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualEquipmentSell;
        private DevExpress.XtraEditors.CheckEdit PossibleFlailCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPossibleFlail;
        private DevExpress.XtraEditors.SpinEdit AchievableClearanceSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAchievableClearance;
        private DevExpress.XtraEditors.DateEdit RevisitDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRevisitDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.CheckEdit StumpTreatmentNoneCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStumpTreatmentNone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeReplacementWhips;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeReplacementStandards;
        private DevExpress.XtraEditors.CheckEdit TreeReplacementNoneCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeReplacementNone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStumpTreatmentEcoPlugs;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStumpTreatmentSpraying;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStumpTreatmentPaint;
        private DevExpress.XtraEditors.CheckEdit StumpTreatmentEcoPlugsSpinEdit;
        private DevExpress.XtraEditors.CheckEdit StumpTreatmentSprayingSpinEdit;
        private DevExpress.XtraEditors.CheckEdit StumpTreatmentPaintSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraLayout.SplitterItem splitterItem4;
        private DevExpress.XtraEditors.LabelControl RemarksInformationLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.CheckEdit PossibleLiveWorkCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPossibleLiveWork;
        private DevExpress.XtraEditors.LabelControl InformationLabelControl;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SpinEdit TreeReplacementWhipsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TreeReplacementStandardsSpinEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraEditors.TextEdit SurveyDateTextEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraEditors.CheckEdit TeamOnHoldCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamOnHold;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Team_WorkOrder_Edit_Clearance_Achieved : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int _intClearanceAchieved = 1;
        public DateTime? _dtRevisitDate = null;
        public DateTime _dtSurveyDate = DateTime.MinValue;
        #endregion

        public frm_UT_Team_WorkOrder_Edit_Clearance_Achieved()
        {
            InitializeComponent();
        }

        private void frm_UT_Team_WorkOrder_Edit_Clearance_Achieved_Load(object sender, EventArgs e)
        {
            this.FormID = 500073;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            if (_dtRevisitDate != DateTime.MinValue) dateEditRevisitDate.DateTime = Convert.ToDateTime(_dtRevisitDate);

            PostOpen();
        }


        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            this.ValidateChildren();
        }

        public override void PostLoadView(object objParameter)
        {
            Set_Revisit_Date_Visibility();
        }

        private void radioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            Set_Revisit_Date_Visibility();
        }

        private void Set_Revisit_Date_Visibility()
        {
            if (Convert.ToInt32(radioGroup1.EditValue) == 1)
            {
                labelRevisitDate.Visible = false;
                dateEditRevisitDate.Visible = false;
                dateEditRevisitDate.Enabled = false;
            }
            else
            {
                labelRevisitDate.Visible = true;
                dateEditRevisitDate.Visible = true;
                dateEditRevisitDate.Enabled = true;
            }
            this.ValidateChildren();
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();

            _intClearanceAchieved = Convert.ToInt32(radioGroup1.EditValue);
            if (_intClearanceAchieved == 0)
            {
                _dtRevisitDate = dateEditRevisitDate.DateTime;
                if (_dtRevisitDate == DateTime.MinValue)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You must enter a revisit date for this pole before proceeding.", "5 Year Clearance Not Achieved", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void dateEditRevisitDate_Validating(object sender, CancelEventArgs e)
        {
            int intResult = Convert.ToInt32(radioGroup1.EditValue);
            DateEdit de = (DateEdit)sender;
            bool boolError = false;
            if (intResult == 0)
            {
                if (de.DateTime == DateTime.MinValue)
                {
                    boolError = true;
                }
                else if (de.DateTime.Year - _dtSurveyDate.Year < 1)
                {
                    boolError = true;
                }
            }

            if (boolError)
            {
                dxErrorProvider1.SetError(dateEditRevisitDate, "Enter a Date at least a year later than the Survey Date [" + _dtSurveyDate.ToString("dd/MM/yyyy") + "].");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(dateEditRevisitDate, "");
            }

        }

    }
}

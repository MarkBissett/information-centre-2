namespace WoodPlan5
{
    partial class frm_UT_Permission_Doc_Edit_WPD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Permission_Doc_Edit_WPD));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddWorkToPermission = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreatePermissionDocument = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateMap = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEmailToCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPostedToCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07389UTPDItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.SentByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PermissionDocumentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Mapping = new WoodPlan5.DataSet_UT_Mapping();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl23 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.StumpTreatmentNoneCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StumpTreatmentEcoPlugsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StumpTreatmentSprayingSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.StumpTreatmentPaintSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.panelControl22 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.TreeReplacementNoneCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.TreeReplacementWhipsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TreeReplacementStandardsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.panelControl21 = new DevExpress.XtraEditors.PanelControl();
            this.checkEdit4x4 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl20 = new DevExpress.XtraEditors.PanelControl();
            this.TipperCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl19 = new DevExpress.XtraEditors.PanelControl();
            this.ChipperCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl18 = new DevExpress.XtraEditors.PanelControl();
            this.MewpCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl17 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.HotGloveAccessAvailableCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl16 = new DevExpress.XtraEditors.PanelControl();
            this.EquipmentMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl15 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.TrafficManagementCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl14 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.SpecialInstructionsMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.panelControl13 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.ShutdownLOACheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl12 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.WildlifeDesignationCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl11 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.PlanningConservationCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl10 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.TPOTreeCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl9 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.EstimatedRevisitDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.panelControl8 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.LandOwnerRestrictedCutCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.ArisingsOtherTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.ArisingsStackOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.ArisingsChipOnSiteCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.ArisingsChipRemoveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.RoadsideAccessOnlyCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.AccessDetailsOnMapCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.AccessAgreedWithLandOwnerCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.InfoLabel1 = new DevExpress.XtraEditors.LabelControl();
            this.EmergencyAccessMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.OwnerAddressMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.SiteAddressMemoExEdit = new DevExpress.XtraEditors.MemoExEdit();
            this.OwnerTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07170UTLandOwnerTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SignatureFileButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.OwnerEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OwnerSalutationTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.ContractorTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.GetDetailsFromLinkedWork = new DevExpress.XtraEditors.SimpleButton();
            this.SentByStaffNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SentByPostCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PostageRequiredCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.PermissionEmailedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SiteGridReferenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LandscapeImpactNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EnvironmentalRANumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ManagedUnitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LVSubNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LVSubNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LineNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FeederNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FeederNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrimarySubNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrimarySubNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NearestAandETextEdit = new DevExpress.XtraEditors.TextEdit();
            this.GridReferenceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NearestTelephonePointTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SignatureDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmailedToClientDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmailedToCustomerDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PDFFileHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.SignatureFileHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.RaisedByIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00226StaffListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateRaisedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07392UTPDLinkedWorkBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionPermissionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditStatus = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07197UTPermissionStatusesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colESQCRCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55Category = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedSpecies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandOwnerRestrictedCut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditWork = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colRefusalReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefusalRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.OwnerNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForSentByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermissionDocumentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.autoGeneratedGroup0 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForOwnerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractor = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRaisedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerSalutation = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForOwnerPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateRaised = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNearestTelephonePoint = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmergencyAccess = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOwnerEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGridReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNearestAandE = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInfoLabel1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArisingsStackOnSite = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArisingsOther = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLandOwnerRestrictedCut = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEstimatedRevisitDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTPOTree = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPlanningConservation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWildlifeDesignation = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShutdownLOA = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSpecialInstructions = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEquipment = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTrafficManagement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHotGloveAccessAvailable = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForMewp = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChipper = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTipper = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFor4x4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTreeReplacement = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStumpReplacement = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPrimarySubName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLVSubName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLVSubNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEnvironmentalRANumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLandscapeImpactNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLineNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForManagedUnitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteGridReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrimarySubNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForWarningLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSignatureFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSignatureDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDFFile = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPostageRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentByPost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSentByStaffName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPermissionEmailed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmailedToCustomerDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmailedToClientDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter();
            this.sp00226_Staff_List_With_BlankTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter();
            this.sp07197_UT_Permission_Statuses_ListTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07197_UT_Permission_Statuses_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07389_UT_PD_ItemTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07389_UT_PD_ItemTableAdapter();
            this.sp07392_UT_PD_Linked_WorkTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter();
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07389UTPDItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionDocumentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).BeginInit();
            this.panelControl23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentNoneCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentEcoPlugsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentSprayingSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentPaintSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).BeginInit();
            this.panelControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementNoneCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementWhipsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementStandardsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).BeginInit();
            this.panelControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4x4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).BeginInit();
            this.panelControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TipperCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).BeginInit();
            this.panelControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChipperCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).BeginInit();
            this.panelControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MewpCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).BeginInit();
            this.panelControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveAccessAvailableCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).BeginInit();
            this.panelControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).BeginInit();
            this.panelControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).BeginInit();
            this.panelControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialInstructionsMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).BeginInit();
            this.panelControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownLOACheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).BeginInit();
            this.panelControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WildlifeDesignationCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).BeginInit();
            this.panelControl11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningConservationCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).BeginInit();
            this.panelControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TPOTreeCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).BeginInit();
            this.panelControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedRevisitDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedRevisitDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).BeginInit();
            this.panelControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LandOwnerRestrictedCutCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsOtherTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsStackOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipOnSiteCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipRemoveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsideAccessOnlyCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccessDetailsOnMapCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccessAgreedWithLandOwnerCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyAccessMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerAddressMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07170UTLandOwnerTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerSalutationTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByPostCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostageRequiredCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionEmailedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGridReferenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandscapeImpactNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentalRANumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagedUnitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LVSubNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LVSubNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimarySubNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimarySubNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandETextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReferenceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestTelephonePointTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFFileHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07197UTPermissionStatusesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditWork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionDocumentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerSalutation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestTelephonePoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGridReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfoLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArisingsStackOnSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArisingsOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandOwnerRestrictedCut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedRevisitDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPOTree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningConservation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWildlifeDesignation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownLOA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialInstructions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHotGloveAccessAvailable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMewp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChipper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTipper)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFor4x4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpReplacement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimarySubName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLVSubName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLVSubNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmentalRANumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandscapeImpactNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagedUnitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGridReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimarySubNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDFFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostageRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionEmailed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToCustomerDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToClientDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(957, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 653);
            this.barDockControlBottom.Size = new System.Drawing.Size(957, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(957, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 627);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.CheckAsYouTypeOptions.CheckControlsInParentContainer = true;
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiAddWorkToPermission,
            this.bbiCreatePermissionDocument,
            this.bbiCreateMap,
            this.bbiEmailToCustomer,
            this.bbiPostedToCustomer});
            this.barManager2.MaxItemId = 20;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Tasks Bar";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(590, 162);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddWorkToPermission),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreatePermissionDocument, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEmailToCustomer, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPostedToCustomer, true)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Tasks Bar";
            // 
            // bbiAddWorkToPermission
            // 
            this.bbiAddWorkToPermission.Caption = "Add More Jobs";
            this.bbiAddWorkToPermission.Id = 15;
            this.bbiAddWorkToPermission.ImageOptions.ImageIndex = 0;
            this.bbiAddWorkToPermission.Name = "bbiAddWorkToPermission";
            this.bbiAddWorkToPermission.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiAddWorkToPermission.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddWorkToPermission_ItemClick);
            // 
            // bbiCreatePermissionDocument
            // 
            this.bbiCreatePermissionDocument.Caption = "View Permission Document";
            this.bbiCreatePermissionDocument.Id = 16;
            this.bbiCreatePermissionDocument.ImageOptions.ImageIndex = 4;
            this.bbiCreatePermissionDocument.Name = "bbiCreatePermissionDocument";
            this.bbiCreatePermissionDocument.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCreatePermissionDocument.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatePermissionDocument_ItemClick);
            // 
            // bbiCreateMap
            // 
            this.bbiCreateMap.Caption = "Create Work Map";
            this.bbiCreateMap.Id = 17;
            this.bbiCreateMap.ImageOptions.ImageIndex = 8;
            this.bbiCreateMap.Name = "bbiCreateMap";
            this.bbiCreateMap.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiCreateMap.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateMap_ItemClick);
            // 
            // bbiEmailToCustomer
            // 
            this.bbiEmailToCustomer.Caption = "Email To Customer";
            this.bbiEmailToCustomer.Id = 18;
            this.bbiEmailToCustomer.ImageOptions.ImageIndex = 9;
            this.bbiEmailToCustomer.Name = "bbiEmailToCustomer";
            this.bbiEmailToCustomer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiEmailToCustomer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEmailToCustomer_ItemClick);
            // 
            // bbiPostedToCustomer
            // 
            this.bbiPostedToCustomer.Caption = "Posted To Customer";
            this.bbiPostedToCustomer.Id = 19;
            this.bbiPostedToCustomer.ImageOptions.ImageIndex = 10;
            this.bbiPostedToCustomer.Name = "bbiPostedToCustomer";
            this.bbiPostedToCustomer.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPostedToCustomer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPostedToCustomer_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(957, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 653);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(957, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 627);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(957, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 627);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Refresh_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "refresh_16x16.png");
            this.imageCollection1.Images.SetKeyName(8, "show_map_16.png");
            this.imageCollection1.InsertGalleryImage("mail_16x16.png", "images/mail/mail_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/mail_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "mail_16x16.png");
            this.imageCollection1.InsertGalleryImage("send_16x16.png", "images/mail/send_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/mail/send_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "send_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl30);
            this.dataLayoutControl1.Controls.Add(this.SentByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PermissionDocumentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl4);
            this.dataLayoutControl1.Controls.Add(this.gridControl6);
            this.dataLayoutControl1.Controls.Add(this.panelControl23);
            this.dataLayoutControl1.Controls.Add(this.panelControl22);
            this.dataLayoutControl1.Controls.Add(this.panelControl21);
            this.dataLayoutControl1.Controls.Add(this.panelControl20);
            this.dataLayoutControl1.Controls.Add(this.panelControl19);
            this.dataLayoutControl1.Controls.Add(this.panelControl18);
            this.dataLayoutControl1.Controls.Add(this.panelControl17);
            this.dataLayoutControl1.Controls.Add(this.panelControl16);
            this.dataLayoutControl1.Controls.Add(this.panelControl15);
            this.dataLayoutControl1.Controls.Add(this.panelControl14);
            this.dataLayoutControl1.Controls.Add(this.panelControl13);
            this.dataLayoutControl1.Controls.Add(this.panelControl12);
            this.dataLayoutControl1.Controls.Add(this.panelControl11);
            this.dataLayoutControl1.Controls.Add(this.panelControl10);
            this.dataLayoutControl1.Controls.Add(this.panelControl9);
            this.dataLayoutControl1.Controls.Add(this.panelControl8);
            this.dataLayoutControl1.Controls.Add(this.panelControl7);
            this.dataLayoutControl1.Controls.Add(this.panelControl6);
            this.dataLayoutControl1.Controls.Add(this.panelControl5);
            this.dataLayoutControl1.Controls.Add(this.panelControl4);
            this.dataLayoutControl1.Controls.Add(this.panelControl3);
            this.dataLayoutControl1.Controls.Add(this.panelControl2);
            this.dataLayoutControl1.Controls.Add(this.panelControl1);
            this.dataLayoutControl1.Controls.Add(this.InfoLabel1);
            this.dataLayoutControl1.Controls.Add(this.EmergencyAccessMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerAddressMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteAddressMemoExEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureFileButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerSalutationTextEdit);
            this.dataLayoutControl1.Controls.Add(this.textEdit4);
            this.dataLayoutControl1.Controls.Add(this.ContractorTextEdit);
            this.dataLayoutControl1.Controls.Add(this.labelControl1);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.pictureEdit1);
            this.dataLayoutControl1.Controls.Add(this.GetDetailsFromLinkedWork);
            this.dataLayoutControl1.Controls.Add(this.SentByStaffNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SentByPostCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PostageRequiredCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.PermissionEmailedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteGridReferenceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LandscapeImpactNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EnvironmentalRANumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ManagedUnitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LVSubNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LVSubNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LineNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimarySubNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimarySubNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NearestAandETextEdit);
            this.dataLayoutControl1.Controls.Add(this.GridReferenceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NearestTelephonePointTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailedToClientDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailedToCustomerDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PDFFileHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.SignatureFileHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.RaisedByIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.DateRaisedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.OwnerNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07389UTPDItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSentByStaffID,
            this.ItemForPermissionDocumentID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(20, 219, 287, 390);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(957, 627);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(563, 306);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.MaxLength = 50;
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(113, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 13;
            // 
            // sp07389UTPDItemBindingSource
            // 
            this.sp07389UTPDItemBindingSource.DataMember = "sp07389_UT_PD_Item";
            this.sp07389UTPDItemBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // labelControl30
            // 
            this.labelControl30.AllowHtmlString = true;
            this.labelControl30.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl30.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl30.Appearance.Image")));
            this.labelControl30.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl30.Appearance.Options.UseForeColor = true;
            this.labelControl30.Appearance.Options.UseImage = true;
            this.labelControl30.Appearance.Options.UseImageAlign = true;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Location = new System.Drawing.Point(114, 35);
            this.labelControl30.MinimumSize = new System.Drawing.Size(0, 18);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(583, 22);
            this.labelControl30.StyleController = this.dataLayoutControl1;
            this.labelControl30.TabIndex = 160;
            this.labelControl30.Text = "       There is no Saved Permission Document on this form. Click <b>View Permissi" +
    "on Document</b> button to View and Save";
            // 
            // SentByStaffIDTextEdit
            // 
            this.SentByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByStaffID", true));
            this.SentByStaffIDTextEdit.Location = new System.Drawing.Point(172, 194);
            this.SentByStaffIDTextEdit.MenuManager = this.barManager1;
            this.SentByStaffIDTextEdit.Name = "SentByStaffIDTextEdit";
            this.SentByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SentByStaffIDTextEdit, true);
            this.SentByStaffIDTextEdit.Size = new System.Drawing.Size(172, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SentByStaffIDTextEdit, optionsSpelling2);
            this.SentByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SentByStaffIDTextEdit.TabIndex = 110;
            // 
            // PermissionDocumentIDTextEdit
            // 
            this.PermissionDocumentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PermissionDocumentID", true));
            this.PermissionDocumentIDTextEdit.Location = new System.Drawing.Point(172, 194);
            this.PermissionDocumentIDTextEdit.MenuManager = this.barManager1;
            this.PermissionDocumentIDTextEdit.Name = "PermissionDocumentIDTextEdit";
            this.PermissionDocumentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PermissionDocumentIDTextEdit, true);
            this.PermissionDocumentIDTextEdit.Size = new System.Drawing.Size(536, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PermissionDocumentIDTextEdit, optionsSpelling3);
            this.PermissionDocumentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PermissionDocumentIDTextEdit.TabIndex = 4;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(11, 35);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditDateTime2});
            this.gridControl4.Size = new System.Drawing.Size(918, 306);
            this.gridControl4.TabIndex = 13;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.gridColumn2,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 283;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 102;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridControl6
            // 
            this.gridControl6.AllowDrop = true;
            this.gridControl6.DataSource = this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Map", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Map", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Preview Map", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Reload Data", "reload")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(11, 35);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEdit1});
            this.gridControl6.Size = new System.Drawing.Size(918, 306);
            this.gridControl6.TabIndex = 13;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource
            // 
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataMember = "sp07202_UT_Mapping_Snapshots_Linked_Snapshots";
            this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // dataSet_UT_Mapping
            // 
            this.dataSet_UT_Mapping.DataSetName = "DataSet_UT_Mapping";
            this.dataSet_UT_Mapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedMapID,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colMapOrder,
            this.colCreatedByID,
            this.colDateTimeCreated,
            this.gridColumn8,
            this.colCreatedByName,
            this.colLinkedToDescription});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsCustomization.AllowGroup = false;
            this.gridView6.OptionsCustomization.AllowSort = false;
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.EnableGroupPanelMenu = false;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.RowAutoHeight = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colLinkedMapID
            // 
            this.colLinkedMapID.Caption = "Linked Map ID";
            this.colLinkedMapID.FieldName = "LinkedMapID";
            this.colLinkedMapID.Name = "colLinkedMapID";
            this.colLinkedMapID.OptionsColumn.AllowEdit = false;
            this.colLinkedMapID.OptionsColumn.AllowFocus = false;
            this.colLinkedMapID.OptionsColumn.ReadOnly = true;
            this.colLinkedMapID.Width = 78;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Linked To Record ID";
            this.gridColumn4.FieldName = "LinkedToRecordID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 107;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "LinkedToRecordTypeID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 134;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Map Description";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 1;
            this.gridColumn6.Width = 250;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Saved File";
            this.gridColumn7.FieldName = "DocumentPath";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 152;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 54;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 79;
            // 
            // colDateTimeCreated
            // 
            this.colDateTimeCreated.Caption = "Date Created";
            this.colDateTimeCreated.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateTimeCreated.FieldName = "DateTimeCreated";
            this.colDateTimeCreated.Name = "colDateTimeCreated";
            this.colDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colDateTimeCreated.Visible = true;
            this.colDateTimeCreated.VisibleIndex = 3;
            this.colDateTimeCreated.Width = 96;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Remarks";
            this.gridColumn8.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn8.FieldName = "Remarks";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Created By";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Visible = true;
            this.colCreatedByName.VisibleIndex = 2;
            this.colCreatedByName.Width = 114;
            // 
            // colLinkedToDescription
            // 
            this.colLinkedToDescription.Caption = "Linked To";
            this.colLinkedToDescription.FieldName = "LinkedToDescription";
            this.colLinkedToDescription.Name = "colLinkedToDescription";
            this.colLinkedToDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedToDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedToDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedToDescription.Width = 316;
            // 
            // panelControl23
            // 
            this.panelControl23.Controls.Add(this.labelControl29);
            this.panelControl23.Controls.Add(this.labelControl28);
            this.panelControl23.Controls.Add(this.labelControl27);
            this.panelControl23.Controls.Add(this.labelControl26);
            this.panelControl23.Controls.Add(this.StumpTreatmentNoneCheckEdit);
            this.panelControl23.Controls.Add(this.StumpTreatmentEcoPlugsSpinEdit);
            this.panelControl23.Controls.Add(this.StumpTreatmentSprayingSpinEdit);
            this.panelControl23.Controls.Add(this.StumpTreatmentPaintSpinEdit);
            this.panelControl23.Location = new System.Drawing.Point(11, 1008);
            this.panelControl23.Name = "panelControl23";
            this.panelControl23.Size = new System.Drawing.Size(644, 33);
            this.panelControl23.TabIndex = 159;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(453, 10);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(28, 13);
            this.labelControl29.TabIndex = 98;
            this.labelControl29.Text = "Paint:";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(305, 10);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(46, 13);
            this.labelControl28.TabIndex = 97;
            this.labelControl28.Text = "Spraying:";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(149, 10);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(44, 13);
            this.labelControl27.TabIndex = 96;
            this.labelControl27.Text = "Eco Plug:";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(5, 3);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(83, 26);
            this.labelControl26.TabIndex = 0;
            this.labelControl26.Text = "Stump Treatment\r\n(Calculated)";
            // 
            // StumpTreatmentNoneCheckEdit
            // 
            this.StumpTreatmentNoneCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "StumpTreatmentNone", true));
            this.StumpTreatmentNoneCheckEdit.Location = new System.Drawing.Point(588, 8);
            this.StumpTreatmentNoneCheckEdit.MenuManager = this.barManager1;
            this.StumpTreatmentNoneCheckEdit.Name = "StumpTreatmentNoneCheckEdit";
            this.StumpTreatmentNoneCheckEdit.Properties.Caption = "None:";
            this.StumpTreatmentNoneCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.StumpTreatmentNoneCheckEdit.Properties.ReadOnly = true;
            this.StumpTreatmentNoneCheckEdit.Properties.ValueChecked = 1;
            this.StumpTreatmentNoneCheckEdit.Properties.ValueUnchecked = 0;
            this.StumpTreatmentNoneCheckEdit.Size = new System.Drawing.Size(52, 19);
            this.StumpTreatmentNoneCheckEdit.TabIndex = 3;
            this.StumpTreatmentNoneCheckEdit.CheckedChanged += new System.EventHandler(this.StumpTreatmentNoneCheckEdit_CheckedChanged);
            // 
            // StumpTreatmentEcoPlugsSpinEdit
            // 
            this.StumpTreatmentEcoPlugsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "StumpTreatmentEcoPlugs", true));
            this.StumpTreatmentEcoPlugsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentEcoPlugsSpinEdit.Location = new System.Drawing.Point(199, 7);
            this.StumpTreatmentEcoPlugsSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentEcoPlugsSpinEdit.Name = "StumpTreatmentEcoPlugsSpinEdit";
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.IsFloatValue = false;
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.Mask.EditMask = "n0";
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.StumpTreatmentEcoPlugsSpinEdit.Properties.ReadOnly = true;
            this.StumpTreatmentEcoPlugsSpinEdit.Size = new System.Drawing.Size(63, 20);
            this.StumpTreatmentEcoPlugsSpinEdit.TabIndex = 0;
            // 
            // StumpTreatmentSprayingSpinEdit
            // 
            this.StumpTreatmentSprayingSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "StumpTreatmentSpraying", true));
            this.StumpTreatmentSprayingSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentSprayingSpinEdit.Location = new System.Drawing.Point(358, 6);
            this.StumpTreatmentSprayingSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentSprayingSpinEdit.Name = "StumpTreatmentSprayingSpinEdit";
            this.StumpTreatmentSprayingSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StumpTreatmentSprayingSpinEdit.Properties.IsFloatValue = false;
            this.StumpTreatmentSprayingSpinEdit.Properties.Mask.EditMask = "n0";
            this.StumpTreatmentSprayingSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StumpTreatmentSprayingSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.StumpTreatmentSprayingSpinEdit.Properties.ReadOnly = true;
            this.StumpTreatmentSprayingSpinEdit.Size = new System.Drawing.Size(60, 20);
            this.StumpTreatmentSprayingSpinEdit.TabIndex = 1;
            // 
            // StumpTreatmentPaintSpinEdit
            // 
            this.StumpTreatmentPaintSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "StumpTreatmentPaint", true));
            this.StumpTreatmentPaintSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.StumpTreatmentPaintSpinEdit.Location = new System.Drawing.Point(488, 7);
            this.StumpTreatmentPaintSpinEdit.MenuManager = this.barManager1;
            this.StumpTreatmentPaintSpinEdit.Name = "StumpTreatmentPaintSpinEdit";
            this.StumpTreatmentPaintSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.StumpTreatmentPaintSpinEdit.Properties.IsFloatValue = false;
            this.StumpTreatmentPaintSpinEdit.Properties.Mask.EditMask = "n0";
            this.StumpTreatmentPaintSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StumpTreatmentPaintSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.StumpTreatmentPaintSpinEdit.Properties.ReadOnly = true;
            this.StumpTreatmentPaintSpinEdit.Size = new System.Drawing.Size(63, 20);
            this.StumpTreatmentPaintSpinEdit.TabIndex = 2;
            // 
            // panelControl22
            // 
            this.panelControl22.Controls.Add(this.labelControl25);
            this.panelControl22.Controls.Add(this.labelControl24);
            this.panelControl22.Controls.Add(this.labelControl23);
            this.panelControl22.Controls.Add(this.TreeReplacementNoneCheckEdit);
            this.panelControl22.Controls.Add(this.TreeReplacementWhipsSpinEdit);
            this.panelControl22.Controls.Add(this.TreeReplacementStandardsSpinEdit);
            this.panelControl22.Location = new System.Drawing.Point(11, 961);
            this.panelControl22.Name = "panelControl22";
            this.panelControl22.Size = new System.Drawing.Size(508, 33);
            this.panelControl22.TabIndex = 158;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(279, 10);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(72, 13);
            this.labelControl25.TabIndex = 94;
            this.labelControl25.Text = "Standards qty:";
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(141, 10);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(52, 13);
            this.labelControl24.TabIndex = 93;
            this.labelControl24.Text = "Whips qty:";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(5, 3);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(92, 26);
            this.labelControl23.TabIndex = 92;
            this.labelControl23.Text = "Tree Replacements\r\n(Calculated)";
            // 
            // TreeReplacementNoneCheckEdit
            // 
            this.TreeReplacementNoneCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TreeReplacementNone", true));
            this.TreeReplacementNoneCheckEdit.Location = new System.Drawing.Point(452, 8);
            this.TreeReplacementNoneCheckEdit.MenuManager = this.barManager1;
            this.TreeReplacementNoneCheckEdit.Name = "TreeReplacementNoneCheckEdit";
            this.TreeReplacementNoneCheckEdit.Properties.Caption = "None:";
            this.TreeReplacementNoneCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TreeReplacementNoneCheckEdit.Properties.ReadOnly = true;
            this.TreeReplacementNoneCheckEdit.Properties.ValueChecked = 1;
            this.TreeReplacementNoneCheckEdit.Properties.ValueUnchecked = 0;
            this.TreeReplacementNoneCheckEdit.Size = new System.Drawing.Size(52, 19);
            this.TreeReplacementNoneCheckEdit.TabIndex = 1;
            this.TreeReplacementNoneCheckEdit.CheckedChanged += new System.EventHandler(this.TreeReplacementNoneCheckEdit_CheckedChanged);
            // 
            // TreeReplacementWhipsSpinEdit
            // 
            this.TreeReplacementWhipsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TreeReplacementWhips", true));
            this.TreeReplacementWhipsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TreeReplacementWhipsSpinEdit.Location = new System.Drawing.Point(199, 7);
            this.TreeReplacementWhipsSpinEdit.MenuManager = this.barManager1;
            this.TreeReplacementWhipsSpinEdit.Name = "TreeReplacementWhipsSpinEdit";
            this.TreeReplacementWhipsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TreeReplacementWhipsSpinEdit.Properties.IsFloatValue = false;
            this.TreeReplacementWhipsSpinEdit.Properties.Mask.EditMask = "n0";
            this.TreeReplacementWhipsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TreeReplacementWhipsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TreeReplacementWhipsSpinEdit.Properties.ReadOnly = true;
            this.TreeReplacementWhipsSpinEdit.Size = new System.Drawing.Size(63, 20);
            this.TreeReplacementWhipsSpinEdit.TabIndex = 0;
            // 
            // TreeReplacementStandardsSpinEdit
            // 
            this.TreeReplacementStandardsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TreeReplacementStandards", true));
            this.TreeReplacementStandardsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TreeReplacementStandardsSpinEdit.Location = new System.Drawing.Point(357, 7);
            this.TreeReplacementStandardsSpinEdit.MenuManager = this.barManager1;
            this.TreeReplacementStandardsSpinEdit.Name = "TreeReplacementStandardsSpinEdit";
            this.TreeReplacementStandardsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TreeReplacementStandardsSpinEdit.Properties.IsFloatValue = false;
            this.TreeReplacementStandardsSpinEdit.Properties.Mask.EditMask = "n0";
            this.TreeReplacementStandardsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TreeReplacementStandardsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.TreeReplacementStandardsSpinEdit.Properties.ReadOnly = true;
            this.TreeReplacementStandardsSpinEdit.Size = new System.Drawing.Size(61, 20);
            this.TreeReplacementStandardsSpinEdit.TabIndex = 90;
            // 
            // panelControl21
            // 
            this.panelControl21.Controls.Add(this.checkEdit4x4);
            this.panelControl21.Controls.Add(this.labelControl22);
            this.panelControl21.Location = new System.Drawing.Point(644, 732);
            this.panelControl21.Name = "panelControl21";
            this.panelControl21.Size = new System.Drawing.Size(100, 47);
            this.panelControl21.TabIndex = 157;
            // 
            // checkEdit4x4
            // 
            this.checkEdit4x4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "4x4", true));
            this.checkEdit4x4.Location = new System.Drawing.Point(10, 24);
            this.checkEdit4x4.MenuManager = this.barManager1;
            this.checkEdit4x4.Name = "checkEdit4x4";
            this.checkEdit4x4.Properties.Caption = "(Calculated)";
            this.checkEdit4x4.Properties.ReadOnly = true;
            this.checkEdit4x4.Properties.ValueChecked = 1;
            this.checkEdit4x4.Properties.ValueUnchecked = 0;
            this.checkEdit4x4.Size = new System.Drawing.Size(79, 19);
            this.checkEdit4x4.TabIndex = 0;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(6, 4);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(24, 13);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "4 x 4";
            // 
            // panelControl20
            // 
            this.panelControl20.Controls.Add(this.TipperCheckEdit);
            this.panelControl20.Controls.Add(this.labelControl21);
            this.panelControl20.Location = new System.Drawing.Point(427, 732);
            this.panelControl20.Name = "panelControl20";
            this.panelControl20.Size = new System.Drawing.Size(100, 47);
            this.panelControl20.TabIndex = 156;
            // 
            // TipperCheckEdit
            // 
            this.TipperCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Tipper", true));
            this.TipperCheckEdit.Location = new System.Drawing.Point(11, 23);
            this.TipperCheckEdit.MenuManager = this.barManager1;
            this.TipperCheckEdit.Name = "TipperCheckEdit";
            this.TipperCheckEdit.Properties.Caption = "(Calculated)";
            this.TipperCheckEdit.Properties.ReadOnly = true;
            this.TipperCheckEdit.Properties.ValueChecked = 1;
            this.TipperCheckEdit.Properties.ValueUnchecked = 0;
            this.TipperCheckEdit.Size = new System.Drawing.Size(79, 19);
            this.TipperCheckEdit.TabIndex = 0;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(6, 4);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(30, 13);
            this.labelControl21.TabIndex = 2;
            this.labelControl21.Text = "Tipper";
            // 
            // panelControl19
            // 
            this.panelControl19.Controls.Add(this.ChipperCheckEdit);
            this.panelControl19.Controls.Add(this.labelControl20);
            this.panelControl19.Location = new System.Drawing.Point(323, 732);
            this.panelControl19.Name = "panelControl19";
            this.panelControl19.Size = new System.Drawing.Size(100, 47);
            this.panelControl19.TabIndex = 155;
            // 
            // ChipperCheckEdit
            // 
            this.ChipperCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Chipper", true));
            this.ChipperCheckEdit.Location = new System.Drawing.Point(11, 23);
            this.ChipperCheckEdit.MenuManager = this.barManager1;
            this.ChipperCheckEdit.Name = "ChipperCheckEdit";
            this.ChipperCheckEdit.Properties.Caption = "(Calculated)";
            this.ChipperCheckEdit.Properties.ReadOnly = true;
            this.ChipperCheckEdit.Properties.ValueChecked = 1;
            this.ChipperCheckEdit.Properties.ValueUnchecked = 0;
            this.ChipperCheckEdit.Size = new System.Drawing.Size(80, 19);
            this.ChipperCheckEdit.TabIndex = 0;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(6, 4);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(37, 13);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "Chipper";
            // 
            // panelControl18
            // 
            this.panelControl18.Controls.Add(this.MewpCheckEdit);
            this.panelControl18.Controls.Add(this.labelControl19);
            this.panelControl18.Location = new System.Drawing.Point(219, 732);
            this.panelControl18.Name = "panelControl18";
            this.panelControl18.Size = new System.Drawing.Size(100, 47);
            this.panelControl18.TabIndex = 154;
            // 
            // MewpCheckEdit
            // 
            this.MewpCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Mewp", true));
            this.MewpCheckEdit.Location = new System.Drawing.Point(11, 23);
            this.MewpCheckEdit.MenuManager = this.barManager1;
            this.MewpCheckEdit.Name = "MewpCheckEdit";
            this.MewpCheckEdit.Properties.Caption = "(Calculated)";
            this.MewpCheckEdit.Properties.ReadOnly = true;
            this.MewpCheckEdit.Properties.ValueChecked = 1;
            this.MewpCheckEdit.Properties.ValueUnchecked = 0;
            this.MewpCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.MewpCheckEdit.TabIndex = 0;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(6, 4);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(28, 13);
            this.labelControl19.TabIndex = 0;
            this.labelControl19.Text = "Mewp";
            // 
            // panelControl17
            // 
            this.panelControl17.Controls.Add(this.labelControl18);
            this.panelControl17.Controls.Add(this.HotGloveAccessAvailableCheckEdit);
            this.panelControl17.Location = new System.Drawing.Point(748, 732);
            this.panelControl17.Name = "panelControl17";
            this.panelControl17.Size = new System.Drawing.Size(133, 47);
            this.panelControl17.TabIndex = 153;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(6, 4);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(123, 13);
            this.labelControl18.TabIndex = 78;
            this.labelControl18.Text = "Hotglove access available";
            // 
            // HotGloveAccessAvailableCheckEdit
            // 
            this.HotGloveAccessAvailableCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "HotGloveAccessAvailable", true));
            this.HotGloveAccessAvailableCheckEdit.Location = new System.Drawing.Point(5, 23);
            this.HotGloveAccessAvailableCheckEdit.MenuManager = this.barManager1;
            this.HotGloveAccessAvailableCheckEdit.Name = "HotGloveAccessAvailableCheckEdit";
            this.HotGloveAccessAvailableCheckEdit.Properties.Caption = "";
            this.HotGloveAccessAvailableCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HotGloveAccessAvailableCheckEdit.Properties.ValueChecked = 1;
            this.HotGloveAccessAvailableCheckEdit.Properties.ValueUnchecked = 0;
            this.HotGloveAccessAvailableCheckEdit.Size = new System.Drawing.Size(123, 19);
            this.HotGloveAccessAvailableCheckEdit.TabIndex = 0;
            // 
            // panelControl16
            // 
            this.panelControl16.Controls.Add(this.EquipmentMemoExEdit);
            this.panelControl16.Controls.Add(this.labelControl17);
            this.panelControl16.Location = new System.Drawing.Point(11, 732);
            this.panelControl16.Name = "panelControl16";
            this.panelControl16.Size = new System.Drawing.Size(204, 47);
            this.panelControl16.TabIndex = 152;
            // 
            // EquipmentMemoExEdit
            // 
            this.EquipmentMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Equipment", true));
            this.EquipmentMemoExEdit.Location = new System.Drawing.Point(4, 22);
            this.EquipmentMemoExEdit.MenuManager = this.barManager1;
            this.EquipmentMemoExEdit.Name = "EquipmentMemoExEdit";
            this.EquipmentMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EquipmentMemoExEdit.Properties.MaxLength = 32000;
            this.EquipmentMemoExEdit.Properties.ReadOnly = true;
            this.EquipmentMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.EquipmentMemoExEdit.Properties.ShowIcon = false;
            this.EquipmentMemoExEdit.Size = new System.Drawing.Size(195, 20);
            this.EquipmentMemoExEdit.TabIndex = 0;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(6, 4);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(111, 13);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "Equipment (Calculated)";
            // 
            // panelControl15
            // 
            this.panelControl15.Controls.Add(this.labelControl16);
            this.panelControl15.Controls.Add(this.TrafficManagementCheckEdit);
            this.panelControl15.Location = new System.Drawing.Point(531, 732);
            this.panelControl15.Name = "panelControl15";
            this.panelControl15.Size = new System.Drawing.Size(109, 47);
            this.panelControl15.TabIndex = 151;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(6, 4);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(96, 13);
            this.labelControl16.TabIndex = 77;
            this.labelControl16.Text = "Traffic management";
            // 
            // TrafficManagementCheckEdit
            // 
            this.TrafficManagementCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TrafficManagement", true));
            this.TrafficManagementCheckEdit.Location = new System.Drawing.Point(15, 23);
            this.TrafficManagementCheckEdit.MenuManager = this.barManager1;
            this.TrafficManagementCheckEdit.Name = "TrafficManagementCheckEdit";
            this.TrafficManagementCheckEdit.Properties.Caption = "(Calculated)";
            this.TrafficManagementCheckEdit.Properties.ReadOnly = true;
            this.TrafficManagementCheckEdit.Properties.ValueChecked = 1;
            this.TrafficManagementCheckEdit.Properties.ValueUnchecked = 0;
            this.TrafficManagementCheckEdit.Size = new System.Drawing.Size(79, 19);
            this.TrafficManagementCheckEdit.TabIndex = 0;
            // 
            // panelControl14
            // 
            this.panelControl14.Controls.Add(this.labelControl15);
            this.panelControl14.Controls.Add(this.SpecialInstructionsMemoExEdit);
            this.panelControl14.Location = new System.Drawing.Point(458, 906);
            this.panelControl14.Name = "panelControl14";
            this.panelControl14.Size = new System.Drawing.Size(347, 41);
            this.panelControl14.TabIndex = 150;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(6, 3);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(91, 13);
            this.labelControl15.TabIndex = 88;
            this.labelControl15.Text = "Special instructions";
            // 
            // SpecialInstructionsMemoExEdit
            // 
            this.SpecialInstructionsMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SpecialInstructions", true));
            this.SpecialInstructionsMemoExEdit.Location = new System.Drawing.Point(5, 17);
            this.SpecialInstructionsMemoExEdit.MenuManager = this.barManager1;
            this.SpecialInstructionsMemoExEdit.Name = "SpecialInstructionsMemoExEdit";
            this.SpecialInstructionsMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpecialInstructionsMemoExEdit.Properties.MaxLength = 32000;
            this.SpecialInstructionsMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SpecialInstructionsMemoExEdit.Properties.ShowIcon = false;
            this.SpecialInstructionsMemoExEdit.Size = new System.Drawing.Size(337, 20);
            this.SpecialInstructionsMemoExEdit.TabIndex = 0;
            // 
            // panelControl13
            // 
            this.panelControl13.Controls.Add(this.labelControl14);
            this.panelControl13.Controls.Add(this.ShutdownLOACheckEdit);
            this.panelControl13.Location = new System.Drawing.Point(354, 906);
            this.panelControl13.Name = "panelControl13";
            this.panelControl13.Size = new System.Drawing.Size(100, 41);
            this.panelControl13.TabIndex = 149;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(11, 3);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(78, 13);
            this.labelControl14.TabIndex = 89;
            this.labelControl14.Text = "Shutdown \\ LOA";
            // 
            // ShutdownLOACheckEdit
            // 
            this.ShutdownLOACheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ShutdownLOA", true));
            this.ShutdownLOACheckEdit.Location = new System.Drawing.Point(12, 20);
            this.ShutdownLOACheckEdit.MenuManager = this.barManager1;
            this.ShutdownLOACheckEdit.Name = "ShutdownLOACheckEdit";
            this.ShutdownLOACheckEdit.Properties.Caption = "(Calculated)";
            this.ShutdownLOACheckEdit.Properties.ReadOnly = true;
            this.ShutdownLOACheckEdit.Properties.ValueChecked = 1;
            this.ShutdownLOACheckEdit.Properties.ValueUnchecked = 0;
            this.ShutdownLOACheckEdit.Size = new System.Drawing.Size(79, 19);
            this.ShutdownLOACheckEdit.TabIndex = 0;
            // 
            // panelControl12
            // 
            this.panelControl12.Controls.Add(this.labelControl13);
            this.panelControl12.Controls.Add(this.WildlifeDesignationCheckEdit);
            this.panelControl12.Location = new System.Drawing.Point(250, 906);
            this.panelControl12.Name = "panelControl12";
            this.panelControl12.Size = new System.Drawing.Size(100, 41);
            this.panelControl12.TabIndex = 148;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(6, 3);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(92, 13);
            this.labelControl13.TabIndex = 87;
            this.labelControl13.Text = "Wildlife designation";
            // 
            // WildlifeDesignationCheckEdit
            // 
            this.WildlifeDesignationCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "WildlifeDesignation", true));
            this.WildlifeDesignationCheckEdit.Location = new System.Drawing.Point(5, 20);
            this.WildlifeDesignationCheckEdit.MenuManager = this.barManager1;
            this.WildlifeDesignationCheckEdit.Name = "WildlifeDesignationCheckEdit";
            this.WildlifeDesignationCheckEdit.Properties.Caption = "";
            this.WildlifeDesignationCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.WildlifeDesignationCheckEdit.Properties.ValueChecked = 1;
            this.WildlifeDesignationCheckEdit.Properties.ValueUnchecked = 0;
            this.WildlifeDesignationCheckEdit.Size = new System.Drawing.Size(92, 19);
            this.WildlifeDesignationCheckEdit.TabIndex = 0;
            // 
            // panelControl11
            // 
            this.panelControl11.Controls.Add(this.labelControl12);
            this.panelControl11.Controls.Add(this.PlanningConservationCheckEdit);
            this.panelControl11.Location = new System.Drawing.Point(146, 906);
            this.panelControl11.Name = "panelControl11";
            this.panelControl11.Size = new System.Drawing.Size(100, 41);
            this.panelControl11.TabIndex = 147;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(6, 3);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(91, 13);
            this.labelControl12.TabIndex = 86;
            this.labelControl12.Text = "Planning restriction";
            // 
            // PlanningConservationCheckEdit
            // 
            this.PlanningConservationCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PlanningConservation", true));
            this.PlanningConservationCheckEdit.Location = new System.Drawing.Point(5, 20);
            this.PlanningConservationCheckEdit.MenuManager = this.barManager1;
            this.PlanningConservationCheckEdit.Name = "PlanningConservationCheckEdit";
            this.PlanningConservationCheckEdit.Properties.Caption = "";
            this.PlanningConservationCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PlanningConservationCheckEdit.Properties.ValueChecked = 1;
            this.PlanningConservationCheckEdit.Properties.ValueUnchecked = 0;
            this.PlanningConservationCheckEdit.Size = new System.Drawing.Size(92, 19);
            this.PlanningConservationCheckEdit.TabIndex = 0;
            // 
            // panelControl10
            // 
            this.panelControl10.Controls.Add(this.labelControl11);
            this.panelControl10.Controls.Add(this.TPOTreeCheckEdit);
            this.panelControl10.Location = new System.Drawing.Point(11, 906);
            this.panelControl10.Name = "panelControl10";
            this.panelControl10.Size = new System.Drawing.Size(131, 41);
            this.panelControl10.TabIndex = 146;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(5, 4);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(119, 13);
            this.labelControl11.TabIndex = 85;
            this.labelControl11.Text = "TPO \\ Conservation area";
            // 
            // TPOTreeCheckEdit
            // 
            this.TPOTreeCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "TPOTree", true));
            this.TPOTreeCheckEdit.Location = new System.Drawing.Point(5, 20);
            this.TPOTreeCheckEdit.MenuManager = this.barManager1;
            this.TPOTreeCheckEdit.Name = "TPOTreeCheckEdit";
            this.TPOTreeCheckEdit.Properties.Caption = "";
            this.TPOTreeCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TPOTreeCheckEdit.Properties.ValueChecked = 1;
            this.TPOTreeCheckEdit.Properties.ValueUnchecked = 0;
            this.TPOTreeCheckEdit.Size = new System.Drawing.Size(119, 19);
            this.TPOTreeCheckEdit.TabIndex = 0;
            // 
            // panelControl9
            // 
            this.panelControl9.Controls.Add(this.labelControl10);
            this.panelControl9.Controls.Add(this.EstimatedRevisitDateDateEdit);
            this.panelControl9.Location = new System.Drawing.Point(219, 859);
            this.panelControl9.Name = "panelControl9";
            this.panelControl9.Size = new System.Drawing.Size(204, 33);
            this.panelControl9.TabIndex = 145;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(6, 4);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(54, 26);
            this.labelControl10.TabIndex = 84;
            this.labelControl10.Text = "Estimated \r\nrevisit date";
            // 
            // EstimatedRevisitDateDateEdit
            // 
            this.EstimatedRevisitDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EstimatedRevisitDate", true));
            this.EstimatedRevisitDateDateEdit.EditValue = null;
            this.EstimatedRevisitDateDateEdit.Location = new System.Drawing.Point(71, 7);
            this.EstimatedRevisitDateDateEdit.MenuManager = this.barManager1;
            this.EstimatedRevisitDateDateEdit.Name = "EstimatedRevisitDateDateEdit";
            this.EstimatedRevisitDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EstimatedRevisitDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EstimatedRevisitDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EstimatedRevisitDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EstimatedRevisitDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EstimatedRevisitDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EstimatedRevisitDateDateEdit.Size = new System.Drawing.Size(126, 20);
            this.EstimatedRevisitDateDateEdit.TabIndex = 0;
            // 
            // panelControl8
            // 
            this.panelControl8.Controls.Add(this.labelControl9);
            this.panelControl8.Controls.Add(this.LandOwnerRestrictedCutCheckEdit);
            this.panelControl8.Location = new System.Drawing.Point(11, 859);
            this.panelControl8.Name = "panelControl8";
            this.panelControl8.Size = new System.Drawing.Size(204, 33);
            this.panelControl8.TabIndex = 144;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(5, 2);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(109, 26);
            this.labelControl9.TabIndex = 83;
            this.labelControl9.Text = "Has the owner insisted\r\non a restricted cut?";
            // 
            // LandOwnerRestrictedCutCheckEdit
            // 
            this.LandOwnerRestrictedCutCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "LandownerRestrictedCut", true));
            this.LandOwnerRestrictedCutCheckEdit.Location = new System.Drawing.Point(124, 7);
            this.LandOwnerRestrictedCutCheckEdit.MenuManager = this.barManager1;
            this.LandOwnerRestrictedCutCheckEdit.Name = "LandOwnerRestrictedCutCheckEdit";
            this.LandOwnerRestrictedCutCheckEdit.Properties.Caption = "";
            this.LandOwnerRestrictedCutCheckEdit.Properties.ValueChecked = 1;
            this.LandOwnerRestrictedCutCheckEdit.Properties.ValueUnchecked = 0;
            this.LandOwnerRestrictedCutCheckEdit.Size = new System.Drawing.Size(65, 19);
            this.LandOwnerRestrictedCutCheckEdit.TabIndex = 0;
            // 
            // panelControl7
            // 
            this.panelControl7.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl7.Appearance.Options.UseBackColor = true;
            this.panelControl7.Controls.Add(this.ArisingsOtherTextEdit);
            this.panelControl7.Controls.Add(this.labelControl8);
            this.panelControl7.Location = new System.Drawing.Point(635, 793);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(169, 52);
            this.panelControl7.TabIndex = 143;
            // 
            // ArisingsOtherTextEdit
            // 
            this.ArisingsOtherTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsOther", true));
            this.ArisingsOtherTextEdit.Location = new System.Drawing.Point(6, 30);
            this.ArisingsOtherTextEdit.MenuManager = this.barManager1;
            this.ArisingsOtherTextEdit.Name = "ArisingsOtherTextEdit";
            this.ArisingsOtherTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ArisingsOtherTextEdit, true);
            this.ArisingsOtherTextEdit.Size = new System.Drawing.Size(158, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ArisingsOtherTextEdit, optionsSpelling4);
            this.ArisingsOtherTextEdit.TabIndex = 0;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(17, 3);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 13);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "Other";
            // 
            // panelControl6
            // 
            this.panelControl6.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl6.Appearance.Options.UseBackColor = true;
            this.panelControl6.Controls.Add(this.ArisingsStackOnSiteCheckEdit);
            this.panelControl6.Controls.Add(this.labelControl7);
            this.panelControl6.Location = new System.Drawing.Point(531, 793);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(100, 52);
            this.panelControl6.TabIndex = 142;
            // 
            // ArisingsStackOnSiteCheckEdit
            // 
            this.ArisingsStackOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsStackOnSite", true));
            this.ArisingsStackOnSiteCheckEdit.Location = new System.Drawing.Point(13, 31);
            this.ArisingsStackOnSiteCheckEdit.MenuManager = this.barManager1;
            this.ArisingsStackOnSiteCheckEdit.Name = "ArisingsStackOnSiteCheckEdit";
            this.ArisingsStackOnSiteCheckEdit.Properties.Caption = "";
            this.ArisingsStackOnSiteCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ArisingsStackOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsStackOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsStackOnSiteCheckEdit.Size = new System.Drawing.Size(75, 19);
            this.ArisingsStackOnSiteCheckEdit.TabIndex = 0;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(22, 3);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(59, 26);
            this.labelControl7.TabIndex = 0;
            this.labelControl7.Text = "Stack neatly\r\non site";
            // 
            // panelControl5
            // 
            this.panelControl5.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl5.Appearance.Options.UseBackColor = true;
            this.panelControl5.Controls.Add(this.ArisingsChipOnSiteCheckEdit);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(427, 793);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(100, 52);
            this.panelControl5.TabIndex = 141;
            // 
            // ArisingsChipOnSiteCheckEdit
            // 
            this.ArisingsChipOnSiteCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsChipOnSite", true));
            this.ArisingsChipOnSiteCheckEdit.Location = new System.Drawing.Point(13, 31);
            this.ArisingsChipOnSiteCheckEdit.MenuManager = this.barManager1;
            this.ArisingsChipOnSiteCheckEdit.Name = "ArisingsChipOnSiteCheckEdit";
            this.ArisingsChipOnSiteCheckEdit.Properties.Caption = "";
            this.ArisingsChipOnSiteCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ArisingsChipOnSiteCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsChipOnSiteCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsChipOnSiteCheckEdit.Size = new System.Drawing.Size(75, 19);
            this.ArisingsChipOnSiteCheckEdit.TabIndex = 0;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(20, 4);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(67, 26);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "Chip on site in\r\nagreed area";
            // 
            // panelControl4
            // 
            this.panelControl4.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl4.Appearance.Options.UseBackColor = true;
            this.panelControl4.Controls.Add(this.ArisingsChipRemoveCheckEdit);
            this.panelControl4.Controls.Add(this.labelControl5);
            this.panelControl4.Location = new System.Drawing.Point(323, 793);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(100, 52);
            this.panelControl4.TabIndex = 140;
            // 
            // ArisingsChipRemoveCheckEdit
            // 
            this.ArisingsChipRemoveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ArisingsChipRemove", true));
            this.ArisingsChipRemoveCheckEdit.Location = new System.Drawing.Point(13, 29);
            this.ArisingsChipRemoveCheckEdit.MenuManager = this.barManager1;
            this.ArisingsChipRemoveCheckEdit.Name = "ArisingsChipRemoveCheckEdit";
            this.ArisingsChipRemoveCheckEdit.Properties.Caption = "";
            this.ArisingsChipRemoveCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ArisingsChipRemoveCheckEdit.Properties.ValueChecked = 1;
            this.ArisingsChipRemoveCheckEdit.Properties.ValueUnchecked = 0;
            this.ArisingsChipRemoveCheckEdit.Size = new System.Drawing.Size(75, 19);
            this.ArisingsChipRemoveCheckEdit.TabIndex = 0;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(14, 4);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(70, 26);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Chip && remove \r\narisings";
            // 
            // panelControl3
            // 
            this.panelControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl3.Appearance.Options.UseBackColor = true;
            this.panelControl3.Controls.Add(this.labelControl4);
            this.panelControl3.Controls.Add(this.RoadsideAccessOnlyCheckEdit);
            this.panelControl3.Location = new System.Drawing.Point(219, 793);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(100, 52);
            this.panelControl3.TabIndex = 139;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(10, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(79, 26);
            this.labelControl4.TabIndex = 0;
            this.labelControl4.Text = "Roadside access\r\nonly";
            // 
            // RoadsideAccessOnlyCheckEdit
            // 
            this.RoadsideAccessOnlyCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "RoadsideAccessOnly", true));
            this.RoadsideAccessOnlyCheckEdit.Location = new System.Drawing.Point(8, 31);
            this.RoadsideAccessOnlyCheckEdit.MenuManager = this.barManager1;
            this.RoadsideAccessOnlyCheckEdit.Name = "RoadsideAccessOnlyCheckEdit";
            this.RoadsideAccessOnlyCheckEdit.Properties.Caption = "";
            this.RoadsideAccessOnlyCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RoadsideAccessOnlyCheckEdit.Properties.ValueChecked = 1;
            this.RoadsideAccessOnlyCheckEdit.Properties.ValueUnchecked = 0;
            this.RoadsideAccessOnlyCheckEdit.Size = new System.Drawing.Size(84, 19);
            this.RoadsideAccessOnlyCheckEdit.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.AccessDetailsOnMapCheckEdit);
            this.panelControl2.Location = new System.Drawing.Point(115, 793);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(100, 52);
            this.panelControl2.TabIndex = 138;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 4);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(67, 26);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Access details\r\non map";
            // 
            // AccessDetailsOnMapCheckEdit
            // 
            this.AccessDetailsOnMapCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "AccessDetailsOnMap", true));
            this.AccessDetailsOnMapCheckEdit.Location = new System.Drawing.Point(10, 31);
            this.AccessDetailsOnMapCheckEdit.MenuManager = this.barManager1;
            this.AccessDetailsOnMapCheckEdit.Name = "AccessDetailsOnMapCheckEdit";
            this.AccessDetailsOnMapCheckEdit.Properties.Caption = "";
            this.AccessDetailsOnMapCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AccessDetailsOnMapCheckEdit.Properties.ValueChecked = 1;
            this.AccessDetailsOnMapCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessDetailsOnMapCheckEdit.Size = new System.Drawing.Size(80, 19);
            this.AccessDetailsOnMapCheckEdit.TabIndex = 0;
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.AccessAgreedWithLandOwnerCheckEdit);
            this.panelControl1.Location = new System.Drawing.Point(11, 793);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(100, 52);
            this.panelControl1.TabIndex = 137;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 4);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(73, 26);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Access agreed\r\nwith landowner";
            // 
            // AccessAgreedWithLandOwnerCheckEdit
            // 
            this.AccessAgreedWithLandOwnerCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "AccessAgreedWithLandOwner", true));
            this.AccessAgreedWithLandOwnerCheckEdit.Location = new System.Drawing.Point(13, 31);
            this.AccessAgreedWithLandOwnerCheckEdit.MenuManager = this.barManager1;
            this.AccessAgreedWithLandOwnerCheckEdit.Name = "AccessAgreedWithLandOwnerCheckEdit";
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Appearance.Options.UseBackColor = true;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.Caption = "";
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.ValueChecked = 1;
            this.AccessAgreedWithLandOwnerCheckEdit.Properties.ValueUnchecked = 0;
            this.AccessAgreedWithLandOwnerCheckEdit.Size = new System.Drawing.Size(73, 19);
            this.AccessAgreedWithLandOwnerCheckEdit.TabIndex = 0;
            // 
            // InfoLabel1
            // 
            this.InfoLabel1.Location = new System.Drawing.Point(11, 396);
            this.InfoLabel1.Name = "InfoLabel1";
            this.InfoLabel1.Size = new System.Drawing.Size(665, 26);
            this.InfoLabel1.StyleController = this.dataLayoutControl1;
            this.InfoLabel1.TabIndex = 136;
            this.InfoLabel1.Text = "I agree to the work(s) detailed below and on any additional referenced sheet(s) t" +
    "o be carried out on behalf of Western Power Distribution.\r\nI understand that thi" +
    "s work will be at no cost to me.";
            // 
            // EmergencyAccessMemoExEdit
            // 
            this.EmergencyAccessMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmergencyAccess", true));
            this.EmergencyAccessMemoExEdit.Location = new System.Drawing.Point(563, 300);
            this.EmergencyAccessMemoExEdit.MenuManager = this.barManager1;
            this.EmergencyAccessMemoExEdit.Name = "EmergencyAccessMemoExEdit";
            this.EmergencyAccessMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmergencyAccessMemoExEdit.Properties.MaxLength = 32000;
            this.EmergencyAccessMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.EmergencyAccessMemoExEdit.Properties.ShowIcon = false;
            this.EmergencyAccessMemoExEdit.Size = new System.Drawing.Size(366, 20);
            this.EmergencyAccessMemoExEdit.StyleController = this.dataLayoutControl1;
            this.EmergencyAccessMemoExEdit.TabIndex = 14;
            this.EmergencyAccessMemoExEdit.Enter += new System.EventHandler(this.EmergencyAccessMemoExEdit_Enter);
            // 
            // OwnerAddressMemoExEdit
            // 
            this.OwnerAddressMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerAddress", true));
            this.OwnerAddressMemoExEdit.Location = new System.Drawing.Point(147, 252);
            this.OwnerAddressMemoExEdit.MenuManager = this.barManager1;
            this.OwnerAddressMemoExEdit.Name = "OwnerAddressMemoExEdit";
            this.OwnerAddressMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OwnerAddressMemoExEdit.Properties.MaxLength = 1000;
            this.OwnerAddressMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.OwnerAddressMemoExEdit.Properties.ShowIcon = false;
            this.OwnerAddressMemoExEdit.Size = new System.Drawing.Size(276, 20);
            this.OwnerAddressMemoExEdit.StyleController = this.dataLayoutControl1;
            this.OwnerAddressMemoExEdit.TabIndex = 6;
            // 
            // SiteAddressMemoExEdit
            // 
            this.SiteAddressMemoExEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SiteAddress", true));
            this.SiteAddressMemoExEdit.Location = new System.Drawing.Point(563, 204);
            this.SiteAddressMemoExEdit.MenuManager = this.barManager1;
            this.SiteAddressMemoExEdit.Name = "SiteAddressMemoExEdit";
            this.SiteAddressMemoExEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SiteAddressMemoExEdit.Properties.MaxLength = 1000;
            this.SiteAddressMemoExEdit.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SiteAddressMemoExEdit.Properties.ShowIcon = false;
            this.SiteAddressMemoExEdit.Size = new System.Drawing.Size(366, 20);
            this.SiteAddressMemoExEdit.StyleController = this.dataLayoutControl1;
            this.SiteAddressMemoExEdit.TabIndex = 10;
            // 
            // OwnerTypeIDGridLookUpEdit
            // 
            this.OwnerTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerTypeID", true));
            this.OwnerTypeIDGridLookUpEdit.Location = new System.Drawing.Point(563, 252);
            this.OwnerTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.OwnerTypeIDGridLookUpEdit.Name = "OwnerTypeIDGridLookUpEdit";
            this.OwnerTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OwnerTypeIDGridLookUpEdit.Properties.DataSource = this.sp07170UTLandOwnerTypesWithBlankBindingSource;
            this.OwnerTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.OwnerTypeIDGridLookUpEdit.Properties.NullText = "";
            this.OwnerTypeIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.OwnerTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.OwnerTypeIDGridLookUpEdit.Size = new System.Drawing.Size(366, 20);
            this.OwnerTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.OwnerTypeIDGridLookUpEdit.TabIndex = 12;
            // 
            // sp07170UTLandOwnerTypesWithBlankBindingSource
            // 
            this.sp07170UTLandOwnerTypesWithBlankBindingSource.DataMember = "sp07170_UT_Land_Owner_Types_With_Blank";
            this.sp07170UTLandOwnerTypesWithBlankBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Owner Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // SignatureFileButtonEdit
            // 
            this.SignatureFileButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureFile", true));
            this.SignatureFileButtonEdit.Location = new System.Drawing.Point(563, 228);
            this.SignatureFileButtonEdit.MenuManager = this.barManager1;
            this.SignatureFileButtonEdit.Name = "SignatureFileButtonEdit";
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Capture - information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to open the Signature Capture screen.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "View - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to view any saved Signature";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.SignatureFileButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Capture", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "capture", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "View", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "view", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SignatureFileButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SignatureFileButtonEdit.Size = new System.Drawing.Size(366, 20);
            this.SignatureFileButtonEdit.StyleController = this.dataLayoutControl1;
            this.SignatureFileButtonEdit.TabIndex = 11;
            this.SignatureFileButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SignatureFileButtonEdit_ButtonClick);
            // 
            // OwnerEmailTextEdit
            // 
            this.OwnerEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerEmail", true));
            this.OwnerEmailTextEdit.Location = new System.Drawing.Point(147, 324);
            this.OwnerEmailTextEdit.MenuManager = this.barManager1;
            this.OwnerEmailTextEdit.Name = "OwnerEmailTextEdit";
            this.OwnerEmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerEmailTextEdit, true);
            this.OwnerEmailTextEdit.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerEmailTextEdit, optionsSpelling5);
            this.OwnerEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerEmailTextEdit.TabIndex = 9;
            // 
            // OwnerTelephoneTextEdit
            // 
            this.OwnerTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerTelephone", true));
            this.OwnerTelephoneTextEdit.Location = new System.Drawing.Point(147, 300);
            this.OwnerTelephoneTextEdit.MenuManager = this.barManager1;
            this.OwnerTelephoneTextEdit.Name = "OwnerTelephoneTextEdit";
            this.OwnerTelephoneTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerTelephoneTextEdit, true);
            this.OwnerTelephoneTextEdit.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerTelephoneTextEdit, optionsSpelling6);
            this.OwnerTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerTelephoneTextEdit.TabIndex = 8;
            // 
            // OwnerPostcodeTextEdit
            // 
            this.OwnerPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerPostcode", true));
            this.OwnerPostcodeTextEdit.Location = new System.Drawing.Point(147, 276);
            this.OwnerPostcodeTextEdit.MenuManager = this.barManager1;
            this.OwnerPostcodeTextEdit.Name = "OwnerPostcodeTextEdit";
            this.OwnerPostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerPostcodeTextEdit, true);
            this.OwnerPostcodeTextEdit.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerPostcodeTextEdit, optionsSpelling7);
            this.OwnerPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerPostcodeTextEdit.TabIndex = 7;
            // 
            // OwnerSalutationTextEdit
            // 
            this.OwnerSalutationTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerSalutation", true));
            this.OwnerSalutationTextEdit.Location = new System.Drawing.Point(147, 228);
            this.OwnerSalutationTextEdit.MenuManager = this.barManager1;
            this.OwnerSalutationTextEdit.Name = "OwnerSalutationTextEdit";
            this.OwnerSalutationTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.OwnerSalutationTextEdit, true);
            this.OwnerSalutationTextEdit.Size = new System.Drawing.Size(276, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.OwnerSalutationTextEdit, optionsSpelling8);
            this.OwnerSalutationTextEdit.StyleController = this.dataLayoutControl1;
            this.OwnerSalutationTextEdit.TabIndex = 5;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(483, 163);
            this.textEdit4.MenuManager = this.barManager1;
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit4, true);
            this.textEdit4.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit4, optionsSpelling9);
            this.textEdit4.StyleController = this.dataLayoutControl1;
            this.textEdit4.TabIndex = 3;
            // 
            // ContractorTextEdit
            // 
            this.ContractorTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Contractor", true));
            this.ContractorTextEdit.EditValue = "";
            this.ContractorTextEdit.Location = new System.Drawing.Point(483, 115);
            this.ContractorTextEdit.MenuManager = this.barManager1;
            this.ContractorTextEdit.Name = "ContractorTextEdit";
            this.ContractorTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractorTextEdit, true);
            this.ContractorTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractorTextEdit, optionsSpelling10);
            this.ContractorTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractorTextEdit.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(347, 71);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(582, 16);
            this.labelControl1.StyleController = this.dataLayoutControl1;
            this.labelControl1.TabIndex = 116;
            this.labelControl1.Text = "Vegetation Management Survey Report Form";
            // 
            // ReferenceNumberTextEdit
            // 
            this.ReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberTextEdit.Location = new System.Drawing.Point(483, 91);
            this.ReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ReferenceNumberTextEdit.Name = "ReferenceNumberTextEdit";
            this.ReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReferenceNumberTextEdit, true);
            this.ReferenceNumberTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReferenceNumberTextEdit, optionsSpelling11);
            this.ReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberTextEdit.TabIndex = 0;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.Location = new System.Drawing.Point(11, 71);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Size = new System.Drawing.Size(332, 109);
            this.pictureEdit1.StyleController = this.dataLayoutControl1;
            this.pictureEdit1.TabIndex = 114;
            // 
            // GetDetailsFromLinkedWork
            // 
            this.GetDetailsFromLinkedWork.Location = new System.Drawing.Point(773, 1055);
            this.GetDetailsFromLinkedWork.Name = "GetDetailsFromLinkedWork";
            this.GetDetailsFromLinkedWork.Size = new System.Drawing.Size(156, 22);
            this.GetDetailsFromLinkedWork.StyleController = this.dataLayoutControl1;
            this.GetDetailsFromLinkedWork.TabIndex = 113;
            this.GetDetailsFromLinkedWork.Text = "Populate from Linked Work";
            this.GetDetailsFromLinkedWork.Click += new System.EventHandler(this.GetDetailsFromLinkedWork_Click);
            // 
            // SentByStaffNameTextEdit
            // 
            this.SentByStaffNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByStaffName", true));
            this.SentByStaffNameTextEdit.Location = new System.Drawing.Point(159, 266);
            this.SentByStaffNameTextEdit.MenuManager = this.barManager1;
            this.SentByStaffNameTextEdit.Name = "SentByStaffNameTextEdit";
            this.SentByStaffNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SentByStaffNameTextEdit, true);
            this.SentByStaffNameTextEdit.Size = new System.Drawing.Size(362, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SentByStaffNameTextEdit, optionsSpelling12);
            this.SentByStaffNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SentByStaffNameTextEdit.TabIndex = 111;
            // 
            // SentByPostCheckEdit
            // 
            this.SentByPostCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SentByPost", true));
            this.SentByPostCheckEdit.Location = new System.Drawing.Point(159, 219);
            this.SentByPostCheckEdit.MenuManager = this.barManager1;
            this.SentByPostCheckEdit.Name = "SentByPostCheckEdit";
            this.SentByPostCheckEdit.Properties.Caption = "(Calculated)";
            this.SentByPostCheckEdit.Properties.ReadOnly = true;
            this.SentByPostCheckEdit.Properties.ValueChecked = 1;
            this.SentByPostCheckEdit.Properties.ValueUnchecked = 0;
            this.SentByPostCheckEdit.Size = new System.Drawing.Size(362, 19);
            this.SentByPostCheckEdit.StyleController = this.dataLayoutControl1;
            this.SentByPostCheckEdit.TabIndex = 109;
            // 
            // PostageRequiredCheckEdit
            // 
            this.PostageRequiredCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PostageRequired", true));
            this.PostageRequiredCheckEdit.Location = new System.Drawing.Point(159, 196);
            this.PostageRequiredCheckEdit.MenuManager = this.barManager1;
            this.PostageRequiredCheckEdit.Name = "PostageRequiredCheckEdit";
            this.PostageRequiredCheckEdit.Properties.Caption = "";
            this.PostageRequiredCheckEdit.Properties.ValueChecked = 1;
            this.PostageRequiredCheckEdit.Properties.ValueUnchecked = 0;
            this.PostageRequiredCheckEdit.Size = new System.Drawing.Size(362, 19);
            this.PostageRequiredCheckEdit.StyleController = this.dataLayoutControl1;
            this.PostageRequiredCheckEdit.TabIndex = 108;
            this.PostageRequiredCheckEdit.CheckedChanged += new System.EventHandler(this.PostageRequiredCheckEdit_CheckedChanged);
            // 
            // PermissionEmailedCheckEdit
            // 
            this.PermissionEmailedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PermissionEmailed", true));
            this.PermissionEmailedCheckEdit.Location = new System.Drawing.Point(159, 173);
            this.PermissionEmailedCheckEdit.MenuManager = this.barManager1;
            this.PermissionEmailedCheckEdit.Name = "PermissionEmailedCheckEdit";
            this.PermissionEmailedCheckEdit.Properties.Caption = "(Calculated)";
            this.PermissionEmailedCheckEdit.Properties.ReadOnly = true;
            this.PermissionEmailedCheckEdit.Properties.ValueChecked = 1;
            this.PermissionEmailedCheckEdit.Properties.ValueUnchecked = 0;
            this.PermissionEmailedCheckEdit.Size = new System.Drawing.Size(362, 19);
            this.PermissionEmailedCheckEdit.StyleController = this.dataLayoutControl1;
            this.PermissionEmailedCheckEdit.TabIndex = 107;
            // 
            // SiteGridReferenceTextEdit
            // 
            this.SiteGridReferenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SiteGridReference", true));
            this.SiteGridReferenceTextEdit.Location = new System.Drawing.Point(643, 1129);
            this.SiteGridReferenceTextEdit.MenuManager = this.barManager1;
            this.SiteGridReferenceTextEdit.Name = "SiteGridReferenceTextEdit";
            this.SiteGridReferenceTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteGridReferenceTextEdit, true);
            this.SiteGridReferenceTextEdit.Size = new System.Drawing.Size(286, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteGridReferenceTextEdit, optionsSpelling13);
            this.SiteGridReferenceTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteGridReferenceTextEdit.TabIndex = 29;
            // 
            // LandscapeImpactNumberTextEdit
            // 
            this.LandscapeImpactNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "LandscapeImpactNumber", true));
            this.LandscapeImpactNumberTextEdit.Location = new System.Drawing.Point(397, 1129);
            this.LandscapeImpactNumberTextEdit.MenuManager = this.barManager1;
            this.LandscapeImpactNumberTextEdit.Name = "LandscapeImpactNumberTextEdit";
            this.LandscapeImpactNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LandscapeImpactNumberTextEdit, true);
            this.LandscapeImpactNumberTextEdit.Size = new System.Drawing.Size(106, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LandscapeImpactNumberTextEdit, optionsSpelling14);
            this.LandscapeImpactNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.LandscapeImpactNumberTextEdit.TabIndex = 28;
            // 
            // EnvironmentalRANumberTextEdit
            // 
            this.EnvironmentalRANumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EnvironmentalRANumber", true));
            this.EnvironmentalRANumberTextEdit.Location = new System.Drawing.Point(147, 1129);
            this.EnvironmentalRANumberTextEdit.MenuManager = this.barManager1;
            this.EnvironmentalRANumberTextEdit.Name = "EnvironmentalRANumberTextEdit";
            this.EnvironmentalRANumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EnvironmentalRANumberTextEdit, true);
            this.EnvironmentalRANumberTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EnvironmentalRANumberTextEdit, optionsSpelling15);
            this.EnvironmentalRANumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EnvironmentalRANumberTextEdit.TabIndex = 27;
            // 
            // ManagedUnitNumberTextEdit
            // 
            this.ManagedUnitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "ManagedUnitNumber", true));
            this.ManagedUnitNumberTextEdit.Location = new System.Drawing.Point(643, 1105);
            this.ManagedUnitNumberTextEdit.MenuManager = this.barManager1;
            this.ManagedUnitNumberTextEdit.Name = "ManagedUnitNumberTextEdit";
            this.ManagedUnitNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ManagedUnitNumberTextEdit, true);
            this.ManagedUnitNumberTextEdit.Size = new System.Drawing.Size(286, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ManagedUnitNumberTextEdit, optionsSpelling16);
            this.ManagedUnitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ManagedUnitNumberTextEdit.TabIndex = 26;
            // 
            // LVSubNumberTextEdit
            // 
            this.LVSubNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "LVSubNumber", true));
            this.LVSubNumberTextEdit.Location = new System.Drawing.Point(397, 1105);
            this.LVSubNumberTextEdit.MenuManager = this.barManager1;
            this.LVSubNumberTextEdit.Name = "LVSubNumberTextEdit";
            this.LVSubNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LVSubNumberTextEdit, true);
            this.LVSubNumberTextEdit.Size = new System.Drawing.Size(106, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LVSubNumberTextEdit, optionsSpelling17);
            this.LVSubNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.LVSubNumberTextEdit.TabIndex = 25;
            // 
            // LVSubNameTextEdit
            // 
            this.LVSubNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "LVSubName", true));
            this.LVSubNameTextEdit.Location = new System.Drawing.Point(147, 1105);
            this.LVSubNameTextEdit.MenuManager = this.barManager1;
            this.LVSubNameTextEdit.Name = "LVSubNameTextEdit";
            this.LVSubNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LVSubNameTextEdit, true);
            this.LVSubNameTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LVSubNameTextEdit, optionsSpelling18);
            this.LVSubNameTextEdit.StyleController = this.dataLayoutControl1;
            this.LVSubNameTextEdit.TabIndex = 24;
            // 
            // LineNumberTextEdit
            // 
            this.LineNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "LineNumber", true));
            this.LineNumberTextEdit.Location = new System.Drawing.Point(643, 1081);
            this.LineNumberTextEdit.MenuManager = this.barManager1;
            this.LineNumberTextEdit.Name = "LineNumberTextEdit";
            this.LineNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LineNumberTextEdit, true);
            this.LineNumberTextEdit.Size = new System.Drawing.Size(286, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LineNumberTextEdit, optionsSpelling19);
            this.LineNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.LineNumberTextEdit.TabIndex = 23;
            // 
            // FeederNumberTextEdit
            // 
            this.FeederNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "FeederNumber", true));
            this.FeederNumberTextEdit.Location = new System.Drawing.Point(397, 1081);
            this.FeederNumberTextEdit.MenuManager = this.barManager1;
            this.FeederNumberTextEdit.Name = "FeederNumberTextEdit";
            this.FeederNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederNumberTextEdit, true);
            this.FeederNumberTextEdit.Size = new System.Drawing.Size(106, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederNumberTextEdit, optionsSpelling20);
            this.FeederNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederNumberTextEdit.TabIndex = 22;
            // 
            // FeederNameTextEdit
            // 
            this.FeederNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "FeederName", true));
            this.FeederNameTextEdit.Location = new System.Drawing.Point(147, 1081);
            this.FeederNameTextEdit.MenuManager = this.barManager1;
            this.FeederNameTextEdit.Name = "FeederNameTextEdit";
            this.FeederNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederNameTextEdit, true);
            this.FeederNameTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederNameTextEdit, optionsSpelling21);
            this.FeederNameTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederNameTextEdit.TabIndex = 21;
            // 
            // PrimarySubNumberTextEdit
            // 
            this.PrimarySubNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PrimarySubNumber", true));
            this.PrimarySubNumberTextEdit.Location = new System.Drawing.Point(397, 1055);
            this.PrimarySubNumberTextEdit.MenuManager = this.barManager1;
            this.PrimarySubNumberTextEdit.Name = "PrimarySubNumberTextEdit";
            this.PrimarySubNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PrimarySubNumberTextEdit, true);
            this.PrimarySubNumberTextEdit.Size = new System.Drawing.Size(106, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PrimarySubNumberTextEdit, optionsSpelling22);
            this.PrimarySubNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.PrimarySubNumberTextEdit.TabIndex = 20;
            // 
            // PrimarySubNameTextEdit
            // 
            this.PrimarySubNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PrimarySubName", true));
            this.PrimarySubNameTextEdit.Location = new System.Drawing.Point(147, 1055);
            this.PrimarySubNameTextEdit.MenuManager = this.barManager1;
            this.PrimarySubNameTextEdit.Name = "PrimarySubNameTextEdit";
            this.PrimarySubNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PrimarySubNameTextEdit, true);
            this.PrimarySubNameTextEdit.Size = new System.Drawing.Size(110, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PrimarySubNameTextEdit, optionsSpelling23);
            this.PrimarySubNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PrimarySubNameTextEdit.TabIndex = 19;
            // 
            // NearestAandETextEdit
            // 
            this.NearestAandETextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "NearestAandE", true));
            this.NearestAandETextEdit.Location = new System.Drawing.Point(563, 372);
            this.NearestAandETextEdit.MenuManager = this.barManager1;
            this.NearestAandETextEdit.Name = "NearestAandETextEdit";
            this.NearestAandETextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestAandETextEdit, true);
            this.NearestAandETextEdit.Size = new System.Drawing.Size(366, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestAandETextEdit, optionsSpelling24);
            this.NearestAandETextEdit.StyleController = this.dataLayoutControl1;
            this.NearestAandETextEdit.TabIndex = 17;
            // 
            // GridReferenceTextEdit
            // 
            this.GridReferenceTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "GridReference", true));
            this.GridReferenceTextEdit.Location = new System.Drawing.Point(563, 348);
            this.GridReferenceTextEdit.MenuManager = this.barManager1;
            this.GridReferenceTextEdit.Name = "GridReferenceTextEdit";
            this.GridReferenceTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.GridReferenceTextEdit, true);
            this.GridReferenceTextEdit.Size = new System.Drawing.Size(366, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.GridReferenceTextEdit, optionsSpelling25);
            this.GridReferenceTextEdit.StyleController = this.dataLayoutControl1;
            this.GridReferenceTextEdit.TabIndex = 16;
            this.GridReferenceTextEdit.EditValueChanged += new System.EventHandler(this.GridReferenceTextEdit_EditValueChanged);
            this.GridReferenceTextEdit.Validated += new System.EventHandler(this.GridReferenceTextEdit_Validated);
            // 
            // NearestTelephonePointTextEdit
            // 
            this.NearestTelephonePointTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "NearestTelephonePoint", true));
            this.NearestTelephonePointTextEdit.Location = new System.Drawing.Point(563, 324);
            this.NearestTelephonePointTextEdit.MenuManager = this.barManager1;
            this.NearestTelephonePointTextEdit.Name = "NearestTelephonePointTextEdit";
            this.NearestTelephonePointTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NearestTelephonePointTextEdit, true);
            this.NearestTelephonePointTextEdit.Size = new System.Drawing.Size(366, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NearestTelephonePointTextEdit, optionsSpelling26);
            this.NearestTelephonePointTextEdit.StyleController = this.dataLayoutControl1;
            this.NearestTelephonePointTextEdit.TabIndex = 15;
            // 
            // SignatureDateDateEdit
            // 
            this.SignatureDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureDate", true));
            this.SignatureDateDateEdit.EditValue = null;
            this.SignatureDateDateEdit.Location = new System.Drawing.Point(759, 69);
            this.SignatureDateDateEdit.MenuManager = this.barManager1;
            this.SignatureDateDateEdit.Name = "SignatureDateDateEdit";
            this.SignatureDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SignatureDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SignatureDateDateEdit.Properties.Mask.EditMask = "g";
            this.SignatureDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SignatureDateDateEdit.Properties.ReadOnly = true;
            this.SignatureDateDateEdit.Size = new System.Drawing.Size(158, 20);
            this.SignatureDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SignatureDateDateEdit.TabIndex = 69;
            // 
            // EmailedToClientDateDateEdit
            // 
            this.EmailedToClientDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmailedToClientDate", true));
            this.EmailedToClientDateDateEdit.EditValue = null;
            this.EmailedToClientDateDateEdit.Location = new System.Drawing.Point(691, 173);
            this.EmailedToClientDateDateEdit.MenuManager = this.barManager1;
            this.EmailedToClientDateDateEdit.Name = "EmailedToClientDateDateEdit";
            this.EmailedToClientDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EmailedToClientDateDateEdit.Properties.Mask.EditMask = "g";
            this.EmailedToClientDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EmailedToClientDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EmailedToClientDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EmailedToClientDateDateEdit.Properties.ReadOnly = true;
            this.EmailedToClientDateDateEdit.Size = new System.Drawing.Size(226, 20);
            this.EmailedToClientDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EmailedToClientDateDateEdit.TabIndex = 66;
            // 
            // EmailedToCustomerDateDateEdit
            // 
            this.EmailedToCustomerDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "EmailedToCustomerDate", true));
            this.EmailedToCustomerDateDateEdit.EditValue = null;
            this.EmailedToCustomerDateDateEdit.Location = new System.Drawing.Point(159, 242);
            this.EmailedToCustomerDateDateEdit.MenuManager = this.barManager1;
            this.EmailedToCustomerDateDateEdit.Name = "EmailedToCustomerDateDateEdit";
            this.EmailedToCustomerDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EmailedToCustomerDateDateEdit.Properties.Mask.EditMask = "g";
            this.EmailedToCustomerDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EmailedToCustomerDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EmailedToCustomerDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EmailedToCustomerDateDateEdit.Properties.ReadOnly = true;
            this.EmailedToCustomerDateDateEdit.Size = new System.Drawing.Size(362, 20);
            this.EmailedToCustomerDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EmailedToCustomerDateDateEdit.TabIndex = 65;
            // 
            // PDFFileHyperLinkEdit
            // 
            this.PDFFileHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "PDFFile", true));
            this.PDFFileHyperLinkEdit.Location = new System.Drawing.Point(159, 93);
            this.PDFFileHyperLinkEdit.MenuManager = this.barManager1;
            this.PDFFileHyperLinkEdit.Name = "PDFFileHyperLinkEdit";
            this.PDFFileHyperLinkEdit.Properties.ReadOnly = true;
            this.PDFFileHyperLinkEdit.Size = new System.Drawing.Size(758, 20);
            this.PDFFileHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.PDFFileHyperLinkEdit.TabIndex = 64;
            this.PDFFileHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.PDFFileHyperLinkEdit_OpenLink);
            // 
            // SignatureFileHyperLinkEdit
            // 
            this.SignatureFileHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "SignatureFile", true));
            this.SignatureFileHyperLinkEdit.Location = new System.Drawing.Point(159, 69);
            this.SignatureFileHyperLinkEdit.MenuManager = this.barManager1;
            this.SignatureFileHyperLinkEdit.Name = "SignatureFileHyperLinkEdit";
            this.SignatureFileHyperLinkEdit.Properties.ReadOnly = true;
            this.SignatureFileHyperLinkEdit.Size = new System.Drawing.Size(460, 20);
            this.SignatureFileHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.SignatureFileHyperLinkEdit.TabIndex = 63;
            this.SignatureFileHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.SignatureFileHyperLinkEdit_OpenLink);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(11, 35);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(99, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 61;
            this.btnSave.Text = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // RaisedByIDGridLookUpEdit
            // 
            this.RaisedByIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "RaisedByID", true));
            this.RaisedByIDGridLookUpEdit.Location = new System.Drawing.Point(483, 139);
            this.RaisedByIDGridLookUpEdit.MenuManager = this.barManager1;
            this.RaisedByIDGridLookUpEdit.Name = "RaisedByIDGridLookUpEdit";
            this.RaisedByIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RaisedByIDGridLookUpEdit.Properties.DataSource = this.sp00226StaffListWithBlankBindingSource;
            this.RaisedByIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.RaisedByIDGridLookUpEdit.Properties.MaxLength = 50;
            this.RaisedByIDGridLookUpEdit.Properties.NullText = "";
            this.RaisedByIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.RaisedByIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.RaisedByIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.RaisedByIDGridLookUpEdit.Size = new System.Drawing.Size(446, 20);
            this.RaisedByIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RaisedByIDGridLookUpEdit.TabIndex = 2;
            // 
            // sp00226StaffListWithBlankBindingSource
            // 
            this.sp00226StaffListWithBlankBindingSource.DataMember = "sp00226_Staff_List_With_Blank";
            this.sp00226StaffListWithBlankBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Active";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn14.FieldName = "Active";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Surname: Forename";
            this.gridColumn15.FieldName = "DisplayName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 225;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Email";
            this.gridColumn16.FieldName = "Email";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 192;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Forename";
            this.gridColumn17.FieldName = "Forename";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 178;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Network ID";
            this.gridColumn18.FieldName = "NetworkID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 180;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Staff ID";
            this.gridColumn19.FieldName = "StaffID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 59;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Staff Name";
            this.gridColumn20.FieldName = "StaffName";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 231;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Surname";
            this.gridColumn21.FieldName = "Surname";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 191;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Type";
            this.gridColumn22.FieldName = "UserType";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 223;
            // 
            // DateRaisedDateEdit
            // 
            this.DateRaisedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "DateRaised", true));
            this.DateRaisedDateEdit.EditValue = null;
            this.DateRaisedDateEdit.Location = new System.Drawing.Point(563, 276);
            this.DateRaisedDateEdit.MenuManager = this.barManager1;
            this.DateRaisedDateEdit.Name = "DateRaisedDateEdit";
            this.DateRaisedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateRaisedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateRaisedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateRaisedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Size = new System.Drawing.Size(366, 20);
            this.DateRaisedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateRaisedDateEdit.TabIndex = 13;
            this.DateRaisedDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DateRaisedDateEdit_Validating);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07392UTPDLinkedWorkBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Work to Permission", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Work", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Work Permission", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(11, 446);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditNumeric2DP,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemGridLookUpEditStatus,
            this.repositoryItemButtonEditWork,
            this.repositoryItemTextEditMeters});
            this.gridControl1.Size = new System.Drawing.Size(918, 272);
            this.gridControl1.TabIndex = 18;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07392UTPDLinkedWorkBindingSource
            // 
            this.sp07392UTPDLinkedWorkBindingSource.DataMember = "sp07392_UT_PD_Linked_Work";
            this.sp07392UTPDLinkedWorkBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionPermissionID,
            this.colPermissionedPoleID,
            this.colActionID,
            this.colPermissionStatusID,
            this.colPermissionDocumentID,
            this.colRemarks,
            this.colLandOwnerName,
            this.colPermissionDocumentDateRaised,
            this.colPoleNumber,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colJobDescription,
            this.colTreeReferenceNumber,
            this.colSurveyID,
            this.colTreeID,
            this.colPoleID,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colESQCRCategory,
            this.colG55Category,
            this.colAchievableClearance,
            this.colLinkedSpecies,
            this.colLandOwnerRestrictedCut,
            this.colShutdownRequired,
            this.colJobTypeID,
            this.gridColumn1,
            this.colRefusalReason,
            this.colRefusalReasonID,
            this.colRefusalRemarks,
            this.colMapID});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colActionPermissionID
            // 
            this.colActionPermissionID.Caption = "Action Permission ID";
            this.colActionPermissionID.FieldName = "ActionPermissionID";
            this.colActionPermissionID.Name = "colActionPermissionID";
            this.colActionPermissionID.OptionsColumn.AllowEdit = false;
            this.colActionPermissionID.OptionsColumn.AllowFocus = false;
            this.colActionPermissionID.OptionsColumn.ReadOnly = true;
            this.colActionPermissionID.Width = 108;
            // 
            // colPermissionedPoleID
            // 
            this.colPermissionedPoleID.Caption = "Permissioned Pole ID";
            this.colPermissionedPoleID.FieldName = "PermissionedPoleID";
            this.colPermissionedPoleID.Name = "colPermissionedPoleID";
            this.colPermissionedPoleID.OptionsColumn.AllowEdit = false;
            this.colPermissionedPoleID.OptionsColumn.AllowFocus = false;
            this.colPermissionedPoleID.OptionsColumn.ReadOnly = true;
            this.colPermissionedPoleID.Width = 110;
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colPermissionStatusID
            // 
            this.colPermissionStatusID.Caption = "Permission Status";
            this.colPermissionStatusID.ColumnEdit = this.repositoryItemGridLookUpEditStatus;
            this.colPermissionStatusID.FieldName = "PermissionStatusID";
            this.colPermissionStatusID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colPermissionStatusID.Name = "colPermissionStatusID";
            this.colPermissionStatusID.Visible = true;
            this.colPermissionStatusID.VisibleIndex = 8;
            this.colPermissionStatusID.Width = 118;
            // 
            // repositoryItemGridLookUpEditStatus
            // 
            this.repositoryItemGridLookUpEditStatus.AutoHeight = false;
            this.repositoryItemGridLookUpEditStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditStatus.DataSource = this.sp07197UTPermissionStatusesListBindingSource;
            this.repositoryItemGridLookUpEditStatus.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditStatus.Name = "repositoryItemGridLookUpEditStatus";
            this.repositoryItemGridLookUpEditStatus.NullText = "";
            this.repositoryItemGridLookUpEditStatus.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditStatus.ValueMember = "ID";
            this.repositoryItemGridLookUpEditStatus.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.repositoryItemGridLookUpEditStatus_EditValueChanging);
            // 
            // sp07197UTPermissionStatusesListBindingSource
            // 
            this.sp07197UTPermissionStatusesListBindingSource.DataMember = "sp07197_UT_Permission_Statuses_List";
            this.sp07197UTPermissionStatusesListBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Permission Status";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 169;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 45;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Width = 63;
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 126;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            this.colRemarks.Width = 152;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLandOwnerName
            // 
            this.colLandOwnerName.Caption = "Land Owner Name";
            this.colLandOwnerName.FieldName = "LandOwnerName";
            this.colLandOwnerName.Name = "colLandOwnerName";
            this.colLandOwnerName.OptionsColumn.AllowEdit = false;
            this.colLandOwnerName.OptionsColumn.AllowFocus = false;
            this.colLandOwnerName.OptionsColumn.ReadOnly = true;
            this.colLandOwnerName.Width = 144;
            // 
            // colPermissionDocumentDateRaised
            // 
            this.colPermissionDocumentDateRaised.Caption = "Permission Document Date Raised";
            this.colPermissionDocumentDateRaised.ColumnEdit = this.repositoryItemTextEditDate;
            this.colPermissionDocumentDateRaised.FieldName = "PermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.Name = "colPermissionDocumentDateRaised";
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentDateRaised.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentDateRaised.Width = 173;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "g";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 2;
            this.colPoleNumber.Width = 94;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 167;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Width = 92;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Work Agreed";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 4;
            this.colJobDescription.Width = 225;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 0;
            this.colTreeReferenceNumber.Width = 103;
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colTreeID
            // 
            this.colTreeID.Caption = "Tree ID";
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Width = 129;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plug";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Width = 144;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Width = 128;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Width = 146;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Width = 133;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Width = 157;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Width = 137;
            // 
            // colESQCRCategory
            // 
            this.colESQCRCategory.Caption = "ESQCR Cat";
            this.colESQCRCategory.FieldName = "ESQCRCategory";
            this.colESQCRCategory.Name = "colESQCRCategory";
            this.colESQCRCategory.OptionsColumn.AllowEdit = false;
            this.colESQCRCategory.OptionsColumn.AllowFocus = false;
            this.colESQCRCategory.OptionsColumn.ReadOnly = true;
            this.colESQCRCategory.Visible = true;
            this.colESQCRCategory.VisibleIndex = 6;
            this.colESQCRCategory.Width = 70;
            // 
            // colG55Category
            // 
            this.colG55Category.Caption = "G55/2 Cat";
            this.colG55Category.FieldName = "G55Category";
            this.colG55Category.Name = "colG55Category";
            this.colG55Category.OptionsColumn.AllowEdit = false;
            this.colG55Category.OptionsColumn.AllowFocus = false;
            this.colG55Category.OptionsColumn.ReadOnly = true;
            this.colG55Category.Visible = true;
            this.colG55Category.VisibleIndex = 1;
            this.colG55Category.Width = 63;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 5;
            this.colAchievableClearance.Width = 62;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "###0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colLinkedSpecies
            // 
            this.colLinkedSpecies.Caption = "Tree Species";
            this.colLinkedSpecies.FieldName = "LinkedSpecies";
            this.colLinkedSpecies.Name = "colLinkedSpecies";
            this.colLinkedSpecies.OptionsColumn.AllowEdit = false;
            this.colLinkedSpecies.OptionsColumn.AllowFocus = false;
            this.colLinkedSpecies.OptionsColumn.ReadOnly = true;
            this.colLinkedSpecies.Visible = true;
            this.colLinkedSpecies.VisibleIndex = 3;
            this.colLinkedSpecies.Width = 144;
            // 
            // colLandOwnerRestrictedCut
            // 
            this.colLandOwnerRestrictedCut.Caption = "Restricted Cut";
            this.colLandOwnerRestrictedCut.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLandOwnerRestrictedCut.FieldName = "LandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.Name = "colLandOwnerRestrictedCut";
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowEdit = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.AllowFocus = false;
            this.colLandOwnerRestrictedCut.OptionsColumn.ReadOnly = true;
            this.colLandOwnerRestrictedCut.Width = 80;
            // 
            // colShutdownRequired
            // 
            this.colShutdownRequired.Caption = "Shutdown Required";
            this.colShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colShutdownRequired.FieldName = "ShutdownRequired";
            this.colShutdownRequired.Name = "colShutdownRequired";
            this.colShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colShutdownRequired.Width = 105;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Edit";
            this.gridColumn1.ColumnEdit = this.repositoryItemButtonEditWork;
            this.gridColumn1.CustomizationCaption = "Edit Button";
            this.gridColumn1.FieldName = "gridColumn1";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowSize = false;
            this.gridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn1.OptionsColumn.FixedWidth = true;
            this.gridColumn1.OptionsColumn.ShowCaption = false;
            this.gridColumn1.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumn1.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn1.OptionsFilter.AllowFilter = false;
            this.gridColumn1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 9;
            this.gridColumn1.Width = 60;
            // 
            // repositoryItemButtonEditWork
            // 
            this.repositoryItemButtonEditWork.AutoHeight = false;
            this.repositoryItemButtonEditWork.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit Job", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click me to edit the job [and optionally any linked, equipment and materials]", "edit", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditWork.Name = "repositoryItemButtonEditWork";
            this.repositoryItemButtonEditWork.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditWork.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditWork_ButtonClick);
            // 
            // colRefusalReason
            // 
            this.colRefusalReason.Caption = "Refusal Reason";
            this.colRefusalReason.FieldName = "RefusalReason";
            this.colRefusalReason.Name = "colRefusalReason";
            this.colRefusalReason.OptionsColumn.AllowEdit = false;
            this.colRefusalReason.OptionsColumn.AllowFocus = false;
            this.colRefusalReason.OptionsColumn.ReadOnly = true;
            this.colRefusalReason.Width = 86;
            // 
            // colRefusalReasonID
            // 
            this.colRefusalReasonID.Caption = "Refusal Reason ID";
            this.colRefusalReasonID.FieldName = "RefusalReasonID";
            this.colRefusalReasonID.Name = "colRefusalReasonID";
            this.colRefusalReasonID.OptionsColumn.AllowEdit = false;
            this.colRefusalReasonID.OptionsColumn.AllowFocus = false;
            this.colRefusalReasonID.OptionsColumn.ReadOnly = true;
            this.colRefusalReasonID.Width = 100;
            // 
            // colRefusalRemarks
            // 
            this.colRefusalRemarks.Caption = "Refusal Remarks";
            this.colRefusalRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRefusalRemarks.FieldName = "RefusalRemarks";
            this.colRefusalRemarks.Name = "colRefusalRemarks";
            this.colRefusalRemarks.OptionsColumn.ReadOnly = true;
            this.colRefusalRemarks.Width = 91;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07389UTPDItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(719, 35);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(210, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(11, 35);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(918, 1134);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling27);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            this.strRemarksMemoEdit.Enter += new System.EventHandler(this.strRemarksMemoEdit_Enter);
            // 
            // OwnerNameButtonEdit
            // 
            this.OwnerNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07389UTPDItemBindingSource, "OwnerName", true));
            this.OwnerNameButtonEdit.EditValue = "";
            this.OwnerNameButtonEdit.Location = new System.Drawing.Point(147, 204);
            this.OwnerNameButtonEdit.MenuManager = this.barManager1;
            this.OwnerNameButtonEdit.Name = "OwnerNameButtonEdit";
            this.OwnerNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Unknown", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to Record Unknown against name", "unknown", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.OwnerNameButtonEdit.Properties.MaxLength = 50;
            this.OwnerNameButtonEdit.Size = new System.Drawing.Size(276, 20);
            this.OwnerNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.OwnerNameButtonEdit.TabIndex = 4;
            this.OwnerNameButtonEdit.TabStop = false;
            this.OwnerNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LandOwnerNameButtonEdit_ButtonClick);
            this.OwnerNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LandOwnerNameButtonEdit_Validating);
            // 
            // ItemForSentByStaffID
            // 
            this.ItemForSentByStaffID.Control = this.SentByStaffIDTextEdit;
            this.ItemForSentByStaffID.CustomizationFormText = "Sent By Staff ID:";
            this.ItemForSentByStaffID.Location = new System.Drawing.Point(0, 182);
            this.ItemForSentByStaffID.Name = "ItemForSentByStaffID";
            this.ItemForSentByStaffID.Size = new System.Drawing.Size(336, 24);
            this.ItemForSentByStaffID.Text = "Sent By Staff ID:";
            this.ItemForSentByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPermissionDocumentID
            // 
            this.ItemForPermissionDocumentID.Control = this.PermissionDocumentIDTextEdit;
            this.ItemForPermissionDocumentID.CustomizationFormText = "Permission Document ID:";
            this.ItemForPermissionDocumentID.Location = new System.Drawing.Point(0, 182);
            this.ItemForPermissionDocumentID.Name = "ItemForPermissionDocumentID";
            this.ItemForPermissionDocumentID.Size = new System.Drawing.Size(700, 24);
            this.ItemForPermissionDocumentID.Text = "Permission Document ID:";
            this.ItemForPermissionDocumentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(416, 824);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(253, 37);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup1.Size = new System.Drawing.Size(940, 1180);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroup2.SelectedTabPage = this.autoGeneratedGroup0;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(938, 1178);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.autoGeneratedGroup0,
            this.layoutControlGroup3,
            this.layoutControlGroup5,
            this.layoutControlGroup2,
            this.layoutControlGroup6});
            // 
            // autoGeneratedGroup0
            // 
            this.autoGeneratedGroup0.AllowDrawBackground = false;
            this.autoGeneratedGroup0.AppearanceGroup.BackColor = System.Drawing.Color.White;
            this.autoGeneratedGroup0.AppearanceGroup.Options.UseBackColor = true;
            this.autoGeneratedGroup0.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.White;
            this.autoGeneratedGroup0.AppearanceTabPage.PageClient.Options.UseBackColor = true;
            this.autoGeneratedGroup0.CustomizationFormText = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.GroupBordersVisible = false;
            this.autoGeneratedGroup0.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForOwnerName,
            this.layoutControlItem3,
            this.layoutControlItem7,
            this.ItemForReferenceNumber,
            this.layoutControlItem9,
            this.ItemForContractor,
            this.layoutControlItem12,
            this.ItemForRaisedByID,
            this.ItemForOwnerSalutation,
            this.emptySpaceItem17,
            this.ItemForOwnerPostcode,
            this.ItemForDateRaised,
            this.layoutControlItem11,
            this.ItemForOwnerTypeID,
            this.ItemForSiteAddress,
            this.ItemForOwnerAddress,
            this.ItemForOwnerTelephone,
            this.ItemForNearestTelephonePoint,
            this.ItemForEmergencyAccess,
            this.ItemForOwnerEmail,
            this.ItemForGridReference,
            this.ItemForNearestAandE,
            this.ItemForInfoLabel1,
            this.emptySpaceItem14,
            this.layoutControlItem2,
            this.emptySpaceItem4,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.ItemForArisingsStackOnSite,
            this.ItemForArisingsOther,
            this.emptySpaceItem18,
            this.ItemForLandOwnerRestrictedCut,
            this.ItemForEstimatedRevisitDate,
            this.emptySpaceItem19,
            this.emptySpaceItem20,
            this.ItemForTPOTree,
            this.emptySpaceItem21,
            this.ItemForPlanningConservation,
            this.ItemForWildlifeDesignation,
            this.ItemForShutdownLOA,
            this.ItemForSpecialInstructions,
            this.ItemForEquipment,
            this.ItemForTrafficManagement,
            this.ItemForHotGloveAccessAvailable,
            this.emptySpaceItem22,
            this.ItemForMewp,
            this.ItemForChipper,
            this.ItemForTipper,
            this.ItemFor4x4,
            this.emptySpaceItem23,
            this.emptySpaceItem24,
            this.emptySpaceItem25,
            this.ItemForTreeReplacement,
            this.emptySpaceItem27,
            this.ItemForStumpReplacement,
            this.emptySpaceItem28,
            this.emptySpaceItem26,
            this.emptySpaceItem6,
            this.ItemForPrimarySubName,
            this.ItemForFeederNumber,
            this.ItemForLVSubName,
            this.ItemForLVSubNumber,
            this.ItemForEnvironmentalRANumber,
            this.ItemForLandscapeImpactNumber,
            this.emptySpaceItem5,
            this.emptySpaceItem29,
            this.layoutControlItem6,
            this.ItemForLineNumber,
            this.emptySpaceItem30,
            this.ItemForManagedUnitNumber,
            this.ItemForSiteGridReference,
            this.ItemForPrimarySubNumber,
            this.ItemForFeederName,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.ItemForWarningLabel});
            this.autoGeneratedGroup0.Location = new System.Drawing.Point(0, 0);
            this.autoGeneratedGroup0.Name = "autoGeneratedGroup0";
            this.autoGeneratedGroup0.Size = new System.Drawing.Size(922, 1138);
            this.autoGeneratedGroup0.Text = "Data Entry Form";
            // 
            // ItemForOwnerName
            // 
            this.ItemForOwnerName.AllowHide = false;
            this.ItemForOwnerName.Control = this.OwnerNameButtonEdit;
            this.ItemForOwnerName.CustomizationFormText = "Land Owner Name:";
            this.ItemForOwnerName.Location = new System.Drawing.Point(0, 169);
            this.ItemForOwnerName.Name = "ItemForOwnerName";
            this.ItemForOwnerName.Size = new System.Drawing.Size(416, 24);
            this.ItemForOwnerName.Text = "Name:";
            this.ItemForOwnerName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "Save Button:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(103, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Save Button:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.pictureEdit1;
            this.layoutControlItem7.CustomizationFormText = "Client Logo:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 36);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(336, 113);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(336, 113);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(336, 113);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Client Logo:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.Control = this.ReferenceNumberTextEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Job Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(336, 56);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(586, 24);
            this.ItemForReferenceNumber.Text = "Job Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl1;
            this.layoutControlItem9.CustomizationFormText = "Header 1:";
            this.layoutControlItem9.Location = new System.Drawing.Point(336, 36);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 20);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(298, 20);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(586, 20);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "Header 1:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // ItemForContractor
            // 
            this.ItemForContractor.Control = this.ContractorTextEdit;
            this.ItemForContractor.CustomizationFormText = "Contractor:";
            this.ItemForContractor.Location = new System.Drawing.Point(336, 80);
            this.ItemForContractor.Name = "ItemForContractor";
            this.ItemForContractor.Size = new System.Drawing.Size(586, 24);
            this.ItemForContractor.Text = "Contractor:";
            this.ItemForContractor.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit4;
            this.layoutControlItem12.CustomizationFormText = "Contact Details:";
            this.layoutControlItem12.Location = new System.Drawing.Point(336, 128);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(586, 31);
            this.layoutControlItem12.Text = "Contact Details:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForRaisedByID
            // 
            this.ItemForRaisedByID.Control = this.RaisedByIDGridLookUpEdit;
            this.ItemForRaisedByID.CustomizationFormText = "Raised By:";
            this.ItemForRaisedByID.Location = new System.Drawing.Point(336, 104);
            this.ItemForRaisedByID.Name = "ItemForRaisedByID";
            this.ItemForRaisedByID.Size = new System.Drawing.Size(586, 24);
            this.ItemForRaisedByID.Text = "Surveyors Name:";
            this.ItemForRaisedByID.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForOwnerSalutation
            // 
            this.ItemForOwnerSalutation.Control = this.OwnerSalutationTextEdit;
            this.ItemForOwnerSalutation.CustomizationFormText = "Salutation:";
            this.ItemForOwnerSalutation.Location = new System.Drawing.Point(0, 193);
            this.ItemForOwnerSalutation.Name = "ItemForOwnerSalutation";
            this.ItemForOwnerSalutation.Size = new System.Drawing.Size(416, 24);
            this.ItemForOwnerSalutation.Text = "Salutation:";
            this.ItemForOwnerSalutation.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.CustomizationFormText = "emptySpaceItem17";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 391);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForOwnerPostcode
            // 
            this.ItemForOwnerPostcode.Control = this.OwnerPostcodeTextEdit;
            this.ItemForOwnerPostcode.CustomizationFormText = "Postcode:";
            this.ItemForOwnerPostcode.Location = new System.Drawing.Point(0, 241);
            this.ItemForOwnerPostcode.Name = "ItemForOwnerPostcode";
            this.ItemForOwnerPostcode.Size = new System.Drawing.Size(416, 24);
            this.ItemForOwnerPostcode.Text = "Postcode:";
            this.ItemForOwnerPostcode.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForDateRaised
            // 
            this.ItemForDateRaised.Control = this.DateRaisedDateEdit;
            this.ItemForDateRaised.CustomizationFormText = "Date Raised:";
            this.ItemForDateRaised.Location = new System.Drawing.Point(416, 241);
            this.ItemForDateRaised.Name = "ItemForDateRaised";
            this.ItemForDateRaised.Size = new System.Drawing.Size(506, 24);
            this.ItemForDateRaised.Text = "Date Raised:";
            this.ItemForDateRaised.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.SignatureFileButtonEdit;
            this.layoutControlItem11.CustomizationFormText = "Signed:";
            this.layoutControlItem11.Location = new System.Drawing.Point(416, 193);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(506, 24);
            this.layoutControlItem11.Text = "Signed:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForOwnerTypeID
            // 
            this.ItemForOwnerTypeID.Control = this.OwnerTypeIDGridLookUpEdit;
            this.ItemForOwnerTypeID.CustomizationFormText = "Owner Type:";
            this.ItemForOwnerTypeID.Location = new System.Drawing.Point(416, 217);
            this.ItemForOwnerTypeID.Name = "ItemForOwnerTypeID";
            this.ItemForOwnerTypeID.Size = new System.Drawing.Size(506, 24);
            this.ItemForOwnerTypeID.Text = "Owner Type:";
            this.ItemForOwnerTypeID.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSiteAddress
            // 
            this.ItemForSiteAddress.Control = this.SiteAddressMemoExEdit;
            this.ItemForSiteAddress.CustomizationFormText = "Site Address:";
            this.ItemForSiteAddress.Location = new System.Drawing.Point(416, 169);
            this.ItemForSiteAddress.Name = "ItemForSiteAddress";
            this.ItemForSiteAddress.Size = new System.Drawing.Size(506, 24);
            this.ItemForSiteAddress.Text = "Site Address:";
            this.ItemForSiteAddress.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForOwnerAddress
            // 
            this.ItemForOwnerAddress.Control = this.OwnerAddressMemoExEdit;
            this.ItemForOwnerAddress.CustomizationFormText = "Address:";
            this.ItemForOwnerAddress.Location = new System.Drawing.Point(0, 217);
            this.ItemForOwnerAddress.Name = "ItemForOwnerAddress";
            this.ItemForOwnerAddress.Size = new System.Drawing.Size(416, 24);
            this.ItemForOwnerAddress.Text = "Address:";
            this.ItemForOwnerAddress.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForOwnerTelephone
            // 
            this.ItemForOwnerTelephone.Control = this.OwnerTelephoneTextEdit;
            this.ItemForOwnerTelephone.CustomizationFormText = "Telephone:";
            this.ItemForOwnerTelephone.Location = new System.Drawing.Point(0, 265);
            this.ItemForOwnerTelephone.Name = "ItemForOwnerTelephone";
            this.ItemForOwnerTelephone.Size = new System.Drawing.Size(416, 24);
            this.ItemForOwnerTelephone.Text = "Telephone:";
            this.ItemForOwnerTelephone.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForNearestTelephonePoint
            // 
            this.ItemForNearestTelephonePoint.Control = this.NearestTelephonePointTextEdit;
            this.ItemForNearestTelephonePoint.CustomizationFormText = "Nearest Telephone Point:";
            this.ItemForNearestTelephonePoint.Location = new System.Drawing.Point(416, 289);
            this.ItemForNearestTelephonePoint.Name = "ItemForNearestTelephonePoint";
            this.ItemForNearestTelephonePoint.Size = new System.Drawing.Size(506, 24);
            this.ItemForNearestTelephonePoint.Text = "Nearest Telephone Point:";
            this.ItemForNearestTelephonePoint.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForEmergencyAccess
            // 
            this.ItemForEmergencyAccess.Control = this.EmergencyAccessMemoExEdit;
            this.ItemForEmergencyAccess.CustomizationFormText = "Emergency Access:";
            this.ItemForEmergencyAccess.Location = new System.Drawing.Point(416, 265);
            this.ItemForEmergencyAccess.Name = "ItemForEmergencyAccess";
            this.ItemForEmergencyAccess.Size = new System.Drawing.Size(506, 24);
            this.ItemForEmergencyAccess.Text = "Emergency Access:";
            this.ItemForEmergencyAccess.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForOwnerEmail
            // 
            this.ItemForOwnerEmail.Control = this.OwnerEmailTextEdit;
            this.ItemForOwnerEmail.CustomizationFormText = "Email:";
            this.ItemForOwnerEmail.Location = new System.Drawing.Point(0, 289);
            this.ItemForOwnerEmail.Name = "ItemForOwnerEmail";
            this.ItemForOwnerEmail.Size = new System.Drawing.Size(416, 72);
            this.ItemForOwnerEmail.Text = "Email:";
            this.ItemForOwnerEmail.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForGridReference
            // 
            this.ItemForGridReference.Control = this.GridReferenceTextEdit;
            this.ItemForGridReference.CustomizationFormText = "Six Figure Grid Reference:";
            this.ItemForGridReference.Location = new System.Drawing.Point(416, 313);
            this.ItemForGridReference.Name = "ItemForGridReference";
            this.ItemForGridReference.Size = new System.Drawing.Size(506, 24);
            this.ItemForGridReference.Text = "Six Figure Grid Reference:";
            this.ItemForGridReference.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForNearestAandE
            // 
            this.ItemForNearestAandE.Control = this.NearestAandETextEdit;
            this.ItemForNearestAandE.CustomizationFormText = "Nearest A and E:";
            this.ItemForNearestAandE.Location = new System.Drawing.Point(416, 337);
            this.ItemForNearestAandE.Name = "ItemForNearestAandE";
            this.ItemForNearestAandE.Size = new System.Drawing.Size(506, 24);
            this.ItemForNearestAandE.Text = "Nearest A and E:";
            this.ItemForNearestAandE.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForInfoLabel1
            // 
            this.ItemForInfoLabel1.Control = this.InfoLabel1;
            this.ItemForInfoLabel1.CustomizationFormText = "Info Label 1:";
            this.ItemForInfoLabel1.Location = new System.Drawing.Point(0, 361);
            this.ItemForInfoLabel1.MaxSize = new System.Drawing.Size(669, 30);
            this.ItemForInfoLabel1.MinSize = new System.Drawing.Size(669, 30);
            this.ItemForInfoLabel1.Name = "ItemForInfoLabel1";
            this.ItemForInfoLabel1.Size = new System.Drawing.Size(922, 30);
            this.ItemForInfoLabel1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInfoLabel1.Text = "Info Label 1:";
            this.ItemForInfoLabel1.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForInfoLabel1.TextVisible = false;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 401);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Jobs";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 411);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 276);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 276);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(922, 276);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Linked Jobs:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 687);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.panelControl1;
            this.layoutControlItem8.CustomizationFormText = "Access agreed with landowner:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 758);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(104, 56);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Access agreed with landowner:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.panelControl2;
            this.layoutControlItem10.CustomizationFormText = "Access details on map:";
            this.layoutControlItem10.Location = new System.Drawing.Point(104, 758);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(104, 56);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "Access details on map:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.panelControl3;
            this.layoutControlItem13.CustomizationFormText = "Roadside Access Only:";
            this.layoutControlItem13.Location = new System.Drawing.Point(208, 758);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(104, 56);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "Roadside Access Only:";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.panelControl4;
            this.layoutControlItem14.CustomizationFormText = "Chip && remove arisings:";
            this.layoutControlItem14.Location = new System.Drawing.Point(312, 758);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(104, 56);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "Chip && remove arisings:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.panelControl5;
            this.layoutControlItem15.CustomizationFormText = "Chip On Site:";
            this.layoutControlItem15.Location = new System.Drawing.Point(416, 758);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(104, 56);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(104, 56);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "Chip On Site:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // ItemForArisingsStackOnSite
            // 
            this.ItemForArisingsStackOnSite.Control = this.panelControl6;
            this.ItemForArisingsStackOnSite.CustomizationFormText = "Stack On Site:";
            this.ItemForArisingsStackOnSite.Location = new System.Drawing.Point(520, 758);
            this.ItemForArisingsStackOnSite.MaxSize = new System.Drawing.Size(104, 56);
            this.ItemForArisingsStackOnSite.MinSize = new System.Drawing.Size(104, 56);
            this.ItemForArisingsStackOnSite.Name = "ItemForArisingsStackOnSite";
            this.ItemForArisingsStackOnSite.Size = new System.Drawing.Size(104, 56);
            this.ItemForArisingsStackOnSite.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForArisingsStackOnSite.Text = "Stack On Site:";
            this.ItemForArisingsStackOnSite.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForArisingsStackOnSite.TextVisible = false;
            // 
            // ItemForArisingsOther
            // 
            this.ItemForArisingsOther.Control = this.panelControl7;
            this.ItemForArisingsOther.CustomizationFormText = "Arisings Other:";
            this.ItemForArisingsOther.Location = new System.Drawing.Point(624, 758);
            this.ItemForArisingsOther.MaxSize = new System.Drawing.Size(173, 56);
            this.ItemForArisingsOther.MinSize = new System.Drawing.Size(173, 56);
            this.ItemForArisingsOther.Name = "ItemForArisingsOther";
            this.ItemForArisingsOther.Size = new System.Drawing.Size(173, 56);
            this.ItemForArisingsOther.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForArisingsOther.Text = "Arisings Other:";
            this.ItemForArisingsOther.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForArisingsOther.TextVisible = false;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.CustomizationFormText = "Footer 1";
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 916);
            this.emptySpaceItem18.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem18.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLandOwnerRestrictedCut
            // 
            this.ItemForLandOwnerRestrictedCut.Control = this.panelControl8;
            this.ItemForLandOwnerRestrictedCut.CustomizationFormText = "Land Owner Restricted Cut:";
            this.ItemForLandOwnerRestrictedCut.Location = new System.Drawing.Point(0, 824);
            this.ItemForLandOwnerRestrictedCut.MaxSize = new System.Drawing.Size(208, 37);
            this.ItemForLandOwnerRestrictedCut.MinSize = new System.Drawing.Size(208, 37);
            this.ItemForLandOwnerRestrictedCut.Name = "ItemForLandOwnerRestrictedCut";
            this.ItemForLandOwnerRestrictedCut.Size = new System.Drawing.Size(208, 37);
            this.ItemForLandOwnerRestrictedCut.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLandOwnerRestrictedCut.Text = "Land Owner Restricted Cut:";
            this.ItemForLandOwnerRestrictedCut.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLandOwnerRestrictedCut.TextVisible = false;
            // 
            // ItemForEstimatedRevisitDate
            // 
            this.ItemForEstimatedRevisitDate.Control = this.panelControl9;
            this.ItemForEstimatedRevisitDate.CustomizationFormText = "Estimated Revisit Date:";
            this.ItemForEstimatedRevisitDate.Location = new System.Drawing.Point(208, 824);
            this.ItemForEstimatedRevisitDate.MaxSize = new System.Drawing.Size(208, 37);
            this.ItemForEstimatedRevisitDate.MinSize = new System.Drawing.Size(208, 37);
            this.ItemForEstimatedRevisitDate.Name = "ItemForEstimatedRevisitDate";
            this.ItemForEstimatedRevisitDate.Size = new System.Drawing.Size(208, 37);
            this.ItemForEstimatedRevisitDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEstimatedRevisitDate.Text = "Estimated Revisit Date:";
            this.ItemForEstimatedRevisitDate.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForEstimatedRevisitDate.TextVisible = false;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.CustomizationFormText = "emptySpaceItem19";
            this.emptySpaceItem19.Location = new System.Drawing.Point(416, 824);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(506, 37);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.CustomizationFormText = "emptySpaceItem20";
            this.emptySpaceItem20.Location = new System.Drawing.Point(797, 758);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(125, 56);
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTPOTree
            // 
            this.ItemForTPOTree.Control = this.panelControl10;
            this.ItemForTPOTree.CustomizationFormText = "TPO \\ Conservation Area:";
            this.ItemForTPOTree.Location = new System.Drawing.Point(0, 871);
            this.ItemForTPOTree.MaxSize = new System.Drawing.Size(135, 45);
            this.ItemForTPOTree.MinSize = new System.Drawing.Size(135, 45);
            this.ItemForTPOTree.Name = "ItemForTPOTree";
            this.ItemForTPOTree.Size = new System.Drawing.Size(135, 45);
            this.ItemForTPOTree.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTPOTree.Text = "TPO \\ Conservation Area:";
            this.ItemForTPOTree.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTPOTree.TextVisible = false;
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.CustomizationFormText = "emptySpaceItem21";
            this.emptySpaceItem21.Location = new System.Drawing.Point(798, 871);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(124, 45);
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPlanningConservation
            // 
            this.ItemForPlanningConservation.Control = this.panelControl11;
            this.ItemForPlanningConservation.CustomizationFormText = "Planning Restriction:";
            this.ItemForPlanningConservation.Location = new System.Drawing.Point(135, 871);
            this.ItemForPlanningConservation.MaxSize = new System.Drawing.Size(104, 45);
            this.ItemForPlanningConservation.MinSize = new System.Drawing.Size(104, 45);
            this.ItemForPlanningConservation.Name = "ItemForPlanningConservation";
            this.ItemForPlanningConservation.Size = new System.Drawing.Size(104, 45);
            this.ItemForPlanningConservation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForPlanningConservation.Text = "Planning Restriction:";
            this.ItemForPlanningConservation.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForPlanningConservation.TextVisible = false;
            // 
            // ItemForWildlifeDesignation
            // 
            this.ItemForWildlifeDesignation.Control = this.panelControl12;
            this.ItemForWildlifeDesignation.CustomizationFormText = "Wildlife Designation:";
            this.ItemForWildlifeDesignation.Location = new System.Drawing.Point(239, 871);
            this.ItemForWildlifeDesignation.MaxSize = new System.Drawing.Size(104, 45);
            this.ItemForWildlifeDesignation.MinSize = new System.Drawing.Size(104, 45);
            this.ItemForWildlifeDesignation.Name = "ItemForWildlifeDesignation";
            this.ItemForWildlifeDesignation.Size = new System.Drawing.Size(104, 45);
            this.ItemForWildlifeDesignation.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWildlifeDesignation.Text = "Wildlife Designation:";
            this.ItemForWildlifeDesignation.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWildlifeDesignation.TextVisible = false;
            // 
            // ItemForShutdownLOA
            // 
            this.ItemForShutdownLOA.Control = this.panelControl13;
            this.ItemForShutdownLOA.CustomizationFormText = "Shutdown \\ LOA:";
            this.ItemForShutdownLOA.Location = new System.Drawing.Point(343, 871);
            this.ItemForShutdownLOA.MaxSize = new System.Drawing.Size(104, 45);
            this.ItemForShutdownLOA.MinSize = new System.Drawing.Size(104, 45);
            this.ItemForShutdownLOA.Name = "ItemForShutdownLOA";
            this.ItemForShutdownLOA.Size = new System.Drawing.Size(104, 45);
            this.ItemForShutdownLOA.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForShutdownLOA.Text = "Shutdown \\ LOA:";
            this.ItemForShutdownLOA.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForShutdownLOA.TextVisible = false;
            // 
            // ItemForSpecialInstructions
            // 
            this.ItemForSpecialInstructions.Control = this.panelControl14;
            this.ItemForSpecialInstructions.CustomizationFormText = "Special Instructions:";
            this.ItemForSpecialInstructions.Location = new System.Drawing.Point(447, 871);
            this.ItemForSpecialInstructions.MaxSize = new System.Drawing.Size(351, 45);
            this.ItemForSpecialInstructions.MinSize = new System.Drawing.Size(351, 45);
            this.ItemForSpecialInstructions.Name = "ItemForSpecialInstructions";
            this.ItemForSpecialInstructions.Size = new System.Drawing.Size(351, 45);
            this.ItemForSpecialInstructions.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSpecialInstructions.Text = "Special Instructions:";
            this.ItemForSpecialInstructions.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForSpecialInstructions.TextVisible = false;
            // 
            // ItemForEquipment
            // 
            this.ItemForEquipment.Control = this.panelControl16;
            this.ItemForEquipment.CustomizationFormText = "Equipment:";
            this.ItemForEquipment.Location = new System.Drawing.Point(0, 697);
            this.ItemForEquipment.MaxSize = new System.Drawing.Size(208, 51);
            this.ItemForEquipment.MinSize = new System.Drawing.Size(208, 51);
            this.ItemForEquipment.Name = "ItemForEquipment";
            this.ItemForEquipment.Size = new System.Drawing.Size(208, 51);
            this.ItemForEquipment.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEquipment.Text = "Equipment:";
            this.ItemForEquipment.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForEquipment.TextVisible = false;
            // 
            // ItemForTrafficManagement
            // 
            this.ItemForTrafficManagement.Control = this.panelControl15;
            this.ItemForTrafficManagement.CustomizationFormText = "Traffic Management:";
            this.ItemForTrafficManagement.Location = new System.Drawing.Point(520, 697);
            this.ItemForTrafficManagement.MaxSize = new System.Drawing.Size(113, 51);
            this.ItemForTrafficManagement.MinSize = new System.Drawing.Size(113, 51);
            this.ItemForTrafficManagement.Name = "ItemForTrafficManagement";
            this.ItemForTrafficManagement.Size = new System.Drawing.Size(113, 51);
            this.ItemForTrafficManagement.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTrafficManagement.Text = "Traffic Management:";
            this.ItemForTrafficManagement.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTrafficManagement.TextVisible = false;
            // 
            // ItemForHotGloveAccessAvailable
            // 
            this.ItemForHotGloveAccessAvailable.Control = this.panelControl17;
            this.ItemForHotGloveAccessAvailable.CustomizationFormText = "Hot Glove Access Available:";
            this.ItemForHotGloveAccessAvailable.Location = new System.Drawing.Point(737, 697);
            this.ItemForHotGloveAccessAvailable.MaxSize = new System.Drawing.Size(137, 51);
            this.ItemForHotGloveAccessAvailable.MinSize = new System.Drawing.Size(137, 51);
            this.ItemForHotGloveAccessAvailable.Name = "ItemForHotGloveAccessAvailable";
            this.ItemForHotGloveAccessAvailable.Size = new System.Drawing.Size(137, 51);
            this.ItemForHotGloveAccessAvailable.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHotGloveAccessAvailable.Text = "Hot Glove Access Available:";
            this.ItemForHotGloveAccessAvailable.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForHotGloveAccessAvailable.TextVisible = false;
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.CustomizationFormText = "emptySpaceItem22";
            this.emptySpaceItem22.Location = new System.Drawing.Point(874, 697);
            this.emptySpaceItem22.MaxSize = new System.Drawing.Size(0, 51);
            this.emptySpaceItem22.MinSize = new System.Drawing.Size(10, 51);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(48, 51);
            this.emptySpaceItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForMewp
            // 
            this.ItemForMewp.Control = this.panelControl18;
            this.ItemForMewp.CustomizationFormText = "Mewp:";
            this.ItemForMewp.Location = new System.Drawing.Point(208, 697);
            this.ItemForMewp.MaxSize = new System.Drawing.Size(104, 51);
            this.ItemForMewp.MinSize = new System.Drawing.Size(104, 51);
            this.ItemForMewp.Name = "ItemForMewp";
            this.ItemForMewp.Size = new System.Drawing.Size(104, 51);
            this.ItemForMewp.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForMewp.Text = "Mewp:";
            this.ItemForMewp.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForMewp.TextVisible = false;
            // 
            // ItemForChipper
            // 
            this.ItemForChipper.Control = this.panelControl19;
            this.ItemForChipper.CustomizationFormText = "Chipper:";
            this.ItemForChipper.Location = new System.Drawing.Point(312, 697);
            this.ItemForChipper.MaxSize = new System.Drawing.Size(104, 51);
            this.ItemForChipper.MinSize = new System.Drawing.Size(104, 51);
            this.ItemForChipper.Name = "ItemForChipper";
            this.ItemForChipper.Size = new System.Drawing.Size(104, 51);
            this.ItemForChipper.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForChipper.Text = "Chipper:";
            this.ItemForChipper.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForChipper.TextVisible = false;
            // 
            // ItemForTipper
            // 
            this.ItemForTipper.Control = this.panelControl20;
            this.ItemForTipper.CustomizationFormText = "Tipper:";
            this.ItemForTipper.Location = new System.Drawing.Point(416, 697);
            this.ItemForTipper.MaxSize = new System.Drawing.Size(104, 51);
            this.ItemForTipper.MinSize = new System.Drawing.Size(104, 51);
            this.ItemForTipper.Name = "ItemForTipper";
            this.ItemForTipper.Size = new System.Drawing.Size(104, 51);
            this.ItemForTipper.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTipper.Text = "Tipper:";
            this.ItemForTipper.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTipper.TextVisible = false;
            // 
            // ItemFor4x4
            // 
            this.ItemFor4x4.Control = this.panelControl21;
            this.ItemFor4x4.CustomizationFormText = "4x4:";
            this.ItemFor4x4.Location = new System.Drawing.Point(633, 697);
            this.ItemFor4x4.MaxSize = new System.Drawing.Size(104, 51);
            this.ItemFor4x4.MinSize = new System.Drawing.Size(104, 51);
            this.ItemFor4x4.Name = "ItemFor4x4";
            this.ItemFor4x4.Size = new System.Drawing.Size(104, 51);
            this.ItemFor4x4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFor4x4.Text = "4x4:";
            this.ItemFor4x4.TextSize = new System.Drawing.Size(0, 0);
            this.ItemFor4x4.TextVisible = false;
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.CustomizationFormText = "emptySpaceItem23";
            this.emptySpaceItem23.Location = new System.Drawing.Point(0, 748);
            this.emptySpaceItem23.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem23.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.CustomizationFormText = "emptySpaceItem24";
            this.emptySpaceItem24.Location = new System.Drawing.Point(0, 814);
            this.emptySpaceItem24.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem24.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.CustomizationFormText = "emptySpaceItem25";
            this.emptySpaceItem25.Location = new System.Drawing.Point(0, 861);
            this.emptySpaceItem25.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem25.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTreeReplacement
            // 
            this.ItemForTreeReplacement.Control = this.panelControl22;
            this.ItemForTreeReplacement.CustomizationFormText = "Tree Replacement:";
            this.ItemForTreeReplacement.Location = new System.Drawing.Point(0, 926);
            this.ItemForTreeReplacement.MaxSize = new System.Drawing.Size(512, 37);
            this.ItemForTreeReplacement.MinSize = new System.Drawing.Size(512, 37);
            this.ItemForTreeReplacement.Name = "ItemForTreeReplacement";
            this.ItemForTreeReplacement.Size = new System.Drawing.Size(512, 37);
            this.ItemForTreeReplacement.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTreeReplacement.Text = "Tree Replacement:";
            this.ItemForTreeReplacement.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForTreeReplacement.TextVisible = false;
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.CustomizationFormText = "emptySpaceItem27";
            this.emptySpaceItem27.Location = new System.Drawing.Point(512, 926);
            this.emptySpaceItem27.MaxSize = new System.Drawing.Size(0, 37);
            this.emptySpaceItem27.MinSize = new System.Drawing.Size(10, 37);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(410, 37);
            this.emptySpaceItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStumpReplacement
            // 
            this.ItemForStumpReplacement.Control = this.panelControl23;
            this.ItemForStumpReplacement.CustomizationFormText = "Stump Replacement:";
            this.ItemForStumpReplacement.Location = new System.Drawing.Point(0, 973);
            this.ItemForStumpReplacement.MaxSize = new System.Drawing.Size(648, 37);
            this.ItemForStumpReplacement.MinSize = new System.Drawing.Size(648, 37);
            this.ItemForStumpReplacement.Name = "ItemForStumpReplacement";
            this.ItemForStumpReplacement.Size = new System.Drawing.Size(648, 37);
            this.ItemForStumpReplacement.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStumpReplacement.Text = "Stump Replacement:";
            this.ItemForStumpReplacement.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForStumpReplacement.TextVisible = false;
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.CustomizationFormText = "emptySpaceItem28";
            this.emptySpaceItem28.Location = new System.Drawing.Point(0, 963);
            this.emptySpaceItem28.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem28.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.CustomizationFormText = "emptySpaceItem26";
            this.emptySpaceItem26.Location = new System.Drawing.Point(648, 973);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(274, 37);
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 1010);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPrimarySubName
            // 
            this.ItemForPrimarySubName.Control = this.PrimarySubNameTextEdit;
            this.ItemForPrimarySubName.CustomizationFormText = "Primary Sub Name:";
            this.ItemForPrimarySubName.Location = new System.Drawing.Point(0, 1020);
            this.ItemForPrimarySubName.Name = "ItemForPrimarySubName";
            this.ItemForPrimarySubName.Size = new System.Drawing.Size(250, 26);
            this.ItemForPrimarySubName.Text = "Primary Sub Name:";
            this.ItemForPrimarySubName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForFeederNumber
            // 
            this.ItemForFeederNumber.Control = this.FeederNumberTextEdit;
            this.ItemForFeederNumber.CustomizationFormText = "Feeder Number:";
            this.ItemForFeederNumber.Location = new System.Drawing.Point(250, 1046);
            this.ItemForFeederNumber.Name = "ItemForFeederNumber";
            this.ItemForFeederNumber.Size = new System.Drawing.Size(246, 24);
            this.ItemForFeederNumber.Text = "Feeder Number:";
            this.ItemForFeederNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForLVSubName
            // 
            this.ItemForLVSubName.Control = this.LVSubNameTextEdit;
            this.ItemForLVSubName.CustomizationFormText = "LV Sub Name:";
            this.ItemForLVSubName.Location = new System.Drawing.Point(0, 1070);
            this.ItemForLVSubName.Name = "ItemForLVSubName";
            this.ItemForLVSubName.Size = new System.Drawing.Size(250, 24);
            this.ItemForLVSubName.Text = "LV Sub Name:";
            this.ItemForLVSubName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForLVSubNumber
            // 
            this.ItemForLVSubNumber.Control = this.LVSubNumberTextEdit;
            this.ItemForLVSubNumber.CustomizationFormText = "LV Sub Number:";
            this.ItemForLVSubNumber.Location = new System.Drawing.Point(250, 1070);
            this.ItemForLVSubNumber.Name = "ItemForLVSubNumber";
            this.ItemForLVSubNumber.Size = new System.Drawing.Size(246, 24);
            this.ItemForLVSubNumber.Text = "LV Sub Number:";
            this.ItemForLVSubNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForEnvironmentalRANumber
            // 
            this.ItemForEnvironmentalRANumber.Control = this.EnvironmentalRANumberTextEdit;
            this.ItemForEnvironmentalRANumber.CustomizationFormText = "Environmental R.A Number:";
            this.ItemForEnvironmentalRANumber.Location = new System.Drawing.Point(0, 1094);
            this.ItemForEnvironmentalRANumber.Name = "ItemForEnvironmentalRANumber";
            this.ItemForEnvironmentalRANumber.Size = new System.Drawing.Size(250, 24);
            this.ItemForEnvironmentalRANumber.Text = "Environmental R.A Number:";
            this.ItemForEnvironmentalRANumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForLandscapeImpactNumber
            // 
            this.ItemForLandscapeImpactNumber.Control = this.LandscapeImpactNumberTextEdit;
            this.ItemForLandscapeImpactNumber.CustomizationFormText = "Landscape Impact Number:";
            this.ItemForLandscapeImpactNumber.Location = new System.Drawing.Point(250, 1094);
            this.ItemForLandscapeImpactNumber.Name = "ItemForLandscapeImpactNumber";
            this.ItemForLandscapeImpactNumber.Size = new System.Drawing.Size(246, 24);
            this.ItemForLandscapeImpactNumber.Text = "Landscape Impact Number:";
            this.ItemForLandscapeImpactNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 1118);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.CustomizationFormText = "emptySpaceItem29";
            this.emptySpaceItem29.Location = new System.Drawing.Point(0, 1128);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.GetDetailsFromLinkedWork;
            this.layoutControlItem6.CustomizationFormText = "Populate from linked Work Button";
            this.layoutControlItem6.Location = new System.Drawing.Point(762, 1020);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(160, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(160, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(160, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Populate from linked Work Button";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // ItemForLineNumber
            // 
            this.ItemForLineNumber.Control = this.LineNumberTextEdit;
            this.ItemForLineNumber.CustomizationFormText = "Line Number:";
            this.ItemForLineNumber.Location = new System.Drawing.Point(496, 1046);
            this.ItemForLineNumber.Name = "ItemForLineNumber";
            this.ItemForLineNumber.Size = new System.Drawing.Size(426, 24);
            this.ItemForLineNumber.Text = "Line Number:";
            this.ItemForLineNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.CustomizationFormText = "emptySpaceItem30";
            this.emptySpaceItem30.Location = new System.Drawing.Point(496, 1020);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(266, 26);
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForManagedUnitNumber
            // 
            this.ItemForManagedUnitNumber.Control = this.ManagedUnitNumberTextEdit;
            this.ItemForManagedUnitNumber.CustomizationFormText = "Managed Unit Number:";
            this.ItemForManagedUnitNumber.Location = new System.Drawing.Point(496, 1070);
            this.ItemForManagedUnitNumber.Name = "ItemForManagedUnitNumber";
            this.ItemForManagedUnitNumber.Size = new System.Drawing.Size(426, 24);
            this.ItemForManagedUnitNumber.Text = "Managed Unit Number:";
            this.ItemForManagedUnitNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSiteGridReference
            // 
            this.ItemForSiteGridReference.Control = this.SiteGridReferenceTextEdit;
            this.ItemForSiteGridReference.CustomizationFormText = "Site Grid Reference:";
            this.ItemForSiteGridReference.Location = new System.Drawing.Point(496, 1094);
            this.ItemForSiteGridReference.Name = "ItemForSiteGridReference";
            this.ItemForSiteGridReference.Size = new System.Drawing.Size(426, 24);
            this.ItemForSiteGridReference.Text = "Site Grid Reference:";
            this.ItemForSiteGridReference.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForPrimarySubNumber
            // 
            this.ItemForPrimarySubNumber.Control = this.PrimarySubNumberTextEdit;
            this.ItemForPrimarySubNumber.CustomizationFormText = "Primary Sub Number:";
            this.ItemForPrimarySubNumber.Location = new System.Drawing.Point(250, 1020);
            this.ItemForPrimarySubNumber.Name = "ItemForPrimarySubNumber";
            this.ItemForPrimarySubNumber.Size = new System.Drawing.Size(246, 26);
            this.ItemForPrimarySubNumber.Text = "Primary Sub Number:";
            this.ItemForPrimarySubNumber.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForFeederName
            // 
            this.ItemForFeederName.Control = this.FeederNameTextEdit;
            this.ItemForFeederName.CustomizationFormText = "Feeder Name:";
            this.ItemForFeederName.Location = new System.Drawing.Point(0, 1046);
            this.ItemForFeederName.Name = "ItemForFeederName";
            this.ItemForFeederName.Size = new System.Drawing.Size(250, 24);
            this.ItemForFeederName.Text = "Feeder Name:";
            this.ItemForFeederName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 149);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(336, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 26);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(708, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(214, 23);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(214, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(214, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(690, 0);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(18, 26);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 159);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForWarningLabel
            // 
            this.ItemForWarningLabel.Control = this.labelControl30;
            this.ItemForWarningLabel.CustomizationFormText = "Warning Label:";
            this.ItemForWarningLabel.Location = new System.Drawing.Point(103, 0);
            this.ItemForWarningLabel.MaxSize = new System.Drawing.Size(587, 26);
            this.ItemForWarningLabel.MinSize = new System.Drawing.Size(587, 26);
            this.ItemForWarningLabel.Name = "ItemForWarningLabel";
            this.ItemForWarningLabel.Size = new System.Drawing.Size(587, 26);
            this.ItemForWarningLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForWarningLabel.Text = "Warning Label:";
            this.ItemForWarningLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForWarningLabel.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup3.CaptionImageOptions.Image")));
            this.layoutControlGroup3.CustomizationFormText = "Work Maps";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8,
            this.layoutControlItem4});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(922, 1138);
            this.layoutControlGroup3.Text = "Work Maps";
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(922, 828);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl6;
            this.layoutControlItem4.CustomizationFormText = "Linked Work Maps:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 310);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(240, 310);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(922, 310);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Linked Work Maps:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Linked Documents";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.emptySpaceItem9});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(922, 1138);
            this.layoutControlGroup5.Text = "Linked Documents";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl4;
            this.layoutControlItem5.CustomizationFormText = "Linked Documents:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 310);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(240, 310);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(922, 310);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "Linked Documents:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(922, 828);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Other Info";
            this.layoutControlGroup2.ExpandButtonVisible = true;
            this.layoutControlGroup2.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup20,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.splitterItem1,
            this.emptySpaceItem7,
            this.emptySpaceItem12});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(922, 1138);
            this.layoutControlGroup2.Text = "Other Info";
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "Saved Permission Document";
            this.layoutControlGroup20.ExpandButtonVisible = true;
            this.layoutControlGroup20.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSignatureFile,
            this.ItemForSignatureDate,
            this.ItemForPDFFile});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Size = new System.Drawing.Size(922, 94);
            this.layoutControlGroup20.Text = "Saved Permission Document";
            // 
            // ItemForSignatureFile
            // 
            this.ItemForSignatureFile.Control = this.SignatureFileHyperLinkEdit;
            this.ItemForSignatureFile.CustomizationFormText = "Saved Signature File:";
            this.ItemForSignatureFile.Location = new System.Drawing.Point(0, 0);
            this.ItemForSignatureFile.Name = "ItemForSignatureFile";
            this.ItemForSignatureFile.Size = new System.Drawing.Size(600, 24);
            this.ItemForSignatureFile.Text = "Saved Signature File:";
            this.ItemForSignatureFile.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSignatureDate
            // 
            this.ItemForSignatureDate.Control = this.SignatureDateDateEdit;
            this.ItemForSignatureDate.CustomizationFormText = "Signature Date:";
            this.ItemForSignatureDate.Location = new System.Drawing.Point(600, 0);
            this.ItemForSignatureDate.Name = "ItemForSignatureDate";
            this.ItemForSignatureDate.Size = new System.Drawing.Size(298, 24);
            this.ItemForSignatureDate.Text = "Signature Date:";
            this.ItemForSignatureDate.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForPDFFile
            // 
            this.ItemForPDFFile.Control = this.PDFFileHyperLinkEdit;
            this.ItemForPDFFile.CustomizationFormText = "Saved Permission Document File:";
            this.ItemForPDFFile.Location = new System.Drawing.Point(0, 24);
            this.ItemForPDFFile.Name = "ItemForPDFFile";
            this.ItemForPDFFile.Size = new System.Drawing.Size(898, 24);
            this.ItemForPDFFile.Text = "Saved PD File:";
            this.ItemForPDFFile.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Customer";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPostageRequired,
            this.ItemForSentByPost,
            this.ItemForSentByStaffName,
            this.ItemForPermissionEmailed,
            this.ItemForEmailedToCustomerDate});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 104);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(526, 163);
            this.layoutControlGroup13.Text = "Customer";
            // 
            // ItemForPostageRequired
            // 
            this.ItemForPostageRequired.Control = this.PostageRequiredCheckEdit;
            this.ItemForPostageRequired.CustomizationFormText = "Postage Required:";
            this.ItemForPostageRequired.Location = new System.Drawing.Point(0, 23);
            this.ItemForPostageRequired.Name = "ItemForPostageRequired";
            this.ItemForPostageRequired.Size = new System.Drawing.Size(502, 23);
            this.ItemForPostageRequired.Text = "Postage Required:";
            this.ItemForPostageRequired.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSentByPost
            // 
            this.ItemForSentByPost.Control = this.SentByPostCheckEdit;
            this.ItemForSentByPost.CustomizationFormText = "Sent By Post:";
            this.ItemForSentByPost.Location = new System.Drawing.Point(0, 46);
            this.ItemForSentByPost.Name = "ItemForSentByPost";
            this.ItemForSentByPost.Size = new System.Drawing.Size(502, 23);
            this.ItemForSentByPost.Text = "Sent By Post:";
            this.ItemForSentByPost.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForSentByStaffName
            // 
            this.ItemForSentByStaffName.Control = this.SentByStaffNameTextEdit;
            this.ItemForSentByStaffName.CustomizationFormText = "Sent By Staff:";
            this.ItemForSentByStaffName.Location = new System.Drawing.Point(0, 93);
            this.ItemForSentByStaffName.Name = "ItemForSentByStaffName";
            this.ItemForSentByStaffName.Size = new System.Drawing.Size(502, 24);
            this.ItemForSentByStaffName.Text = "Sent By Staff:";
            this.ItemForSentByStaffName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForPermissionEmailed
            // 
            this.ItemForPermissionEmailed.Control = this.PermissionEmailedCheckEdit;
            this.ItemForPermissionEmailed.CustomizationFormText = "Permission Emailed:";
            this.ItemForPermissionEmailed.Location = new System.Drawing.Point(0, 0);
            this.ItemForPermissionEmailed.Name = "ItemForPermissionEmailed";
            this.ItemForPermissionEmailed.Size = new System.Drawing.Size(502, 23);
            this.ItemForPermissionEmailed.Text = "Permission Emailed:";
            this.ItemForPermissionEmailed.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForEmailedToCustomerDate
            // 
            this.ItemForEmailedToCustomerDate.Control = this.EmailedToCustomerDateDateEdit;
            this.ItemForEmailedToCustomerDate.CustomizationFormText = "Emailed \\ Sent To Customer:";
            this.ItemForEmailedToCustomerDate.Location = new System.Drawing.Point(0, 69);
            this.ItemForEmailedToCustomerDate.Name = "ItemForEmailedToCustomerDate";
            this.ItemForEmailedToCustomerDate.Size = new System.Drawing.Size(502, 24);
            this.ItemForEmailedToCustomerDate.Text = "Emailed \\ Sent:";
            this.ItemForEmailedToCustomerDate.TextSize = new System.Drawing.Size(133, 13);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Client";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmailedToClientDate});
            this.layoutControlGroup14.Location = new System.Drawing.Point(532, 104);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(390, 163);
            this.layoutControlGroup14.Text = "Client";
            // 
            // ItemForEmailedToClientDate
            // 
            this.ItemForEmailedToClientDate.Control = this.EmailedToClientDateDateEdit;
            this.ItemForEmailedToClientDate.CustomizationFormText = "Emailed To Client Date:";
            this.ItemForEmailedToClientDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmailedToClientDate.Name = "ItemForEmailedToClientDate";
            this.ItemForEmailedToClientDate.Size = new System.Drawing.Size(366, 117);
            this.ItemForEmailedToClientDate.Text = "Emailed To Client Date:";
            this.ItemForEmailedToClientDate.TextSize = new System.Drawing.Size(133, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(526, 104);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 163);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 94);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(922, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 267);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(922, 871);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup6.CaptionImageOptions.Image")));
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(922, 1138);
            this.layoutControlGroup6.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(922, 1138);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07060UTTreeSpeciesLinkedToTreeBindingSource
            // 
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataMember = "sp07060_UT_Tree_Species_Linked_To_Tree";
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp00226_Staff_List_With_BlankTableAdapter
            // 
            this.sp00226_Staff_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07197_UT_Permission_Statuses_ListTableAdapter
            // 
            this.sp07197_UT_Permission_Statuses_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp07389_UT_PD_ItemTableAdapter
            // 
            this.sp07389_UT_PD_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07392_UT_PD_Linked_WorkTableAdapter
            // 
            this.sp07392_UT_PD_Linked_WorkTableAdapter.ClearBeforeFill = true;
            // 
            // sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter
            // 
            this.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl6;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl4;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07170_UT_Land_Owner_Types_With_BlankTableAdapter
            // 
            this.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Permission_Doc_Edit_WPD
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(957, 683);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Permission_Doc_Edit_WPD";
            this.Text = "Edit Permission Document - WPD Layout";
            this.Activated += new System.EventHandler(this.frm_UT_Permission_Doc_Edit_WPD_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Permission_Doc_Edit_WPD_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Permission_Doc_Edit_WPD_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07389UTPDItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionDocumentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl23)).EndInit();
            this.panelControl23.ResumeLayout(false);
            this.panelControl23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentNoneCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentEcoPlugsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentSprayingSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StumpTreatmentPaintSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl22)).EndInit();
            this.panelControl22.ResumeLayout(false);
            this.panelControl22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementNoneCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementWhipsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeReplacementStandardsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl21)).EndInit();
            this.panelControl21.ResumeLayout(false);
            this.panelControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4x4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl20)).EndInit();
            this.panelControl20.ResumeLayout(false);
            this.panelControl20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TipperCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl19)).EndInit();
            this.panelControl19.ResumeLayout(false);
            this.panelControl19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChipperCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl18)).EndInit();
            this.panelControl18.ResumeLayout(false);
            this.panelControl18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MewpCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl17)).EndInit();
            this.panelControl17.ResumeLayout(false);
            this.panelControl17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HotGloveAccessAvailableCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl16)).EndInit();
            this.panelControl16.ResumeLayout(false);
            this.panelControl16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EquipmentMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl15)).EndInit();
            this.panelControl15.ResumeLayout(false);
            this.panelControl15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrafficManagementCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl14)).EndInit();
            this.panelControl14.ResumeLayout(false);
            this.panelControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpecialInstructionsMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl13)).EndInit();
            this.panelControl13.ResumeLayout(false);
            this.panelControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownLOACheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl12)).EndInit();
            this.panelControl12.ResumeLayout(false);
            this.panelControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WildlifeDesignationCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl11)).EndInit();
            this.panelControl11.ResumeLayout(false);
            this.panelControl11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PlanningConservationCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl10)).EndInit();
            this.panelControl10.ResumeLayout(false);
            this.panelControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TPOTreeCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl9)).EndInit();
            this.panelControl9.ResumeLayout(false);
            this.panelControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedRevisitDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstimatedRevisitDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl8)).EndInit();
            this.panelControl8.ResumeLayout(false);
            this.panelControl8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LandOwnerRestrictedCutCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.panelControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsOtherTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsStackOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipOnSiteCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ArisingsChipRemoveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.panelControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoadsideAccessOnlyCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccessDetailsOnMapCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccessAgreedWithLandOwnerCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmergencyAccessMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerAddressMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteAddressMemoExEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07170UTLandOwnerTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerSalutationTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByStaffNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SentByPostCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostageRequiredCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PermissionEmailedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteGridReferenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LandscapeImpactNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnvironmentalRANumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ManagedUnitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LVSubNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LVSubNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LineNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimarySubNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimarySubNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestAandETextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReferenceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NearestTelephonePointTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToClientDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailedToCustomerDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDFFileHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SignatureFileHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00226StaffListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07392UTPDLinkedWorkBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07197UTPermissionStatusesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditWork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OwnerNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionDocumentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoGeneratedGroup0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerSalutation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestTelephonePoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmergencyAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOwnerEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGridReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNearestAandE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInfoLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArisingsStackOnSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArisingsOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandOwnerRestrictedCut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEstimatedRevisitDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTPOTree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPlanningConservation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWildlifeDesignation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownLOA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpecialInstructions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEquipment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTrafficManagement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHotGloveAccessAvailable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMewp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChipper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTipper)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFor4x4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTreeReplacement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStumpReplacement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimarySubName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLVSubName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLVSubNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnvironmentalRANumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLandscapeImpactNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLineNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForManagedUnitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteGridReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimarySubNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWarningLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSignatureDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDFFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostageRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSentByStaffName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPermissionEmailed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToCustomerDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmailedToClientDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit PermissionDocumentIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermissionDocumentID;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit OwnerNameButtonEdit;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp07060UTTreeSpeciesLinkedToTreeBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter;
        private DevExpress.XtraEditors.DateEdit DateRaisedDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit RaisedByIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp00226StaffListWithBlankBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00226_Staff_List_With_BlankTableAdapter sp00226_Staff_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.HyperLinkEdit SignatureFileHyperLinkEdit;
        private DevExpress.XtraEditors.HyperLinkEdit PDFFileHyperLinkEdit;
        private DevExpress.XtraEditors.DateEdit EmailedToClientDateDateEdit;
        private DevExpress.XtraEditors.DateEdit EmailedToCustomerDateDateEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPermissionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionStatusID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditStatus;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private System.Windows.Forms.BindingSource sp07197UTPermissionStatusesListBindingSource;
        private DataSet_UT_EditTableAdapters.sp07197_UT_Permission_Statuses_ListTableAdapter sp07197_UT_Permission_Statuses_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraEditors.DateEdit SignatureDateDateEdit;
        private DevExpress.XtraEditors.TextEdit NearestTelephonePointTextEdit;
        private DevExpress.XtraEditors.TextEdit NearestAandETextEdit;
        private DevExpress.XtraEditors.TextEdit GridReferenceTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraEditors.CheckEdit AccessDetailsOnMapCheckEdit;
        private DevExpress.XtraEditors.CheckEdit AccessAgreedWithLandOwnerCheckEdit;
        private DevExpress.XtraEditors.CheckEdit HotGloveAccessAvailableCheckEdit;
        private DevExpress.XtraEditors.CheckEdit TrafficManagementCheckEdit;
        private DevExpress.XtraEditors.CheckEdit RoadsideAccessOnlyCheckEdit;
        private DevExpress.XtraEditors.CheckEdit LandOwnerRestrictedCutCheckEdit;
        private DevExpress.XtraEditors.DateEdit EstimatedRevisitDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit TPOTreeCheckEdit;
        private DevExpress.XtraEditors.CheckEdit PlanningConservationCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WildlifeDesignationCheckEdit;
        private DevExpress.XtraEditors.MemoExEdit SpecialInstructionsMemoExEdit;
        private DevExpress.XtraEditors.CheckEdit ShutdownLOACheckEdit;
        private DevExpress.XtraEditors.CheckEdit TreeReplacementNoneCheckEdit;
        private DevExpress.XtraEditors.SpinEdit TreeReplacementStandardsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit TreeReplacementWhipsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StumpTreatmentEcoPlugsSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StumpTreatmentPaintSpinEdit;
        private DevExpress.XtraEditors.SpinEdit StumpTreatmentSprayingSpinEdit;
        private DevExpress.XtraEditors.CheckEdit StumpTreatmentNoneCheckEdit;
        private DevExpress.XtraEditors.TextEdit PrimarySubNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit PrimarySubNameTextEdit;
        private DevExpress.XtraEditors.TextEdit LineNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit FeederNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit FeederNameTextEdit;
        private DevExpress.XtraEditors.TextEdit LVSubNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit LVSubNameTextEdit;
        private DevExpress.XtraEditors.TextEdit EnvironmentalRANumberTextEdit;
        private DevExpress.XtraEditors.TextEdit ManagedUnitNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit SiteGridReferenceTextEdit;
        private DevExpress.XtraEditors.TextEdit LandscapeImpactNumberTextEdit;
        private DevExpress.XtraEditors.CheckEdit PermissionEmailedCheckEdit;
        private DevExpress.XtraEditors.CheckEdit PostageRequiredCheckEdit;
        private DevExpress.XtraEditors.CheckEdit SentByPostCheckEdit;
        private DevExpress.XtraEditors.TextEdit SentByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByStaffID;
        private DevExpress.XtraEditors.TextEdit SentByStaffNameTextEdit;
        private DevExpress.XtraEditors.SimpleButton GetDetailsFromLinkedWork;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit ContractorTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit ReferenceNumberTextEdit;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.BindingSource sp07389UTPDItemBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07389_UT_PD_ItemTableAdapter sp07389_UT_PD_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit OwnerSalutationTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerPostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerEmailTextEdit;
        private DevExpress.XtraEditors.TextEdit OwnerTelephoneTextEdit;
        private DevExpress.XtraEditors.ButtonEdit SignatureFileButtonEdit;
        private DevExpress.XtraEditors.GridLookUpEdit OwnerTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.MemoExEdit SiteAddressMemoExEdit;
        private DevExpress.XtraEditors.MemoExEdit OwnerAddressMemoExEdit;
        private DevExpress.XtraEditors.MemoExEdit EmergencyAccessMemoExEdit;
        private DevExpress.XtraEditors.LabelControl InfoLabel1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.CheckEdit ArisingsChipOnSiteCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.CheckEdit ArisingsChipRemoveCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.CheckEdit ArisingsStackOnSiteCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.TextEdit ArisingsOtherTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private System.Windows.Forms.BindingSource sp07392UTPDLinkedWorkBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07392_UT_PD_Linked_WorkTableAdapter sp07392_UT_PD_Linked_WorkTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditWork;
        private DevExpress.XtraGrid.Columns.GridColumn colESQCRCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colG55Category;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSpecies;
        private DevExpress.XtraGrid.Columns.GridColumn colLandOwnerRestrictedCut;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownRequired;
        private DevExpress.XtraEditors.PanelControl panelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl11;
        private DevExpress.XtraEditors.PanelControl panelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.PanelControl panelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.PanelControl panelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.PanelControl panelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.PanelControl panelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.PanelControl panelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.MemoExEdit EquipmentMemoExEdit;
        private DevExpress.XtraEditors.PanelControl panelControl18;
        private DevExpress.XtraEditors.CheckEdit MewpCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.PanelControl panelControl19;
        private DevExpress.XtraEditors.CheckEdit ChipperCheckEdit;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.PanelControl panelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.CheckEdit TipperCheckEdit;
        private DevExpress.XtraEditors.PanelControl panelControl21;
        private DevExpress.XtraEditors.CheckEdit checkEdit4x4;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.PanelControl panelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.PanelControl panelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup autoGeneratedGroup0;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerName;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractor;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRaisedByID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerSalutation;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRaised;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerAddress;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestTelephonePoint;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmergencyAccess;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOwnerEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGridReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNearestAandE;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInfoLabel1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostageRequired;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByPost;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSentByStaffName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPermissionEmailed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailedToCustomerDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmailedToClientDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSignatureFile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSignatureDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDFFile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArisingsStackOnSite;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArisingsOther;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLandOwnerRestrictedCut;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEstimatedRevisitDate;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTPOTree;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPlanningConservation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWildlifeDesignation;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShutdownLOA;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpecialInstructions;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEquipment;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTrafficManagement;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHotGloveAccessAvailable;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMewp;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChipper;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTipper;
        private DevExpress.XtraLayout.LayoutControlItem ItemFor4x4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTreeReplacement;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStumpReplacement;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimarySubName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLVSubName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLVSubNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEnvironmentalRANumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLandscapeImpactNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLineNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraLayout.LayoutControlItem ItemForManagedUnitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteGridReference;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimarySubNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAddWorkToPermission;
        private DevExpress.XtraBars.BarButtonItem bbiCreatePermissionDocument;
        private DevExpress.XtraBars.BarButtonItem bbiCreateMap;
        private DevExpress.XtraBars.BarButtonItem bbiEmailToCustomer;
        private DevExpress.XtraBars.BarButtonItem bbiPostedToCustomer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCreated;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDescription;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource sp07202UTMappingSnapshotsLinkedSnapshotsBindingSource;
        private DataSet_UT_Mapping dataSet_UT_Mapping;
        private DataSet_UT_MappingTableAdapters.sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07170UTLandOwnerTypesWithBlankBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07170_UT_Land_Owner_Types_With_BlankTableAdapter sp07170_UT_Land_Owner_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalReason;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalReasonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRefusalRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWarningLabel;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
    }
}

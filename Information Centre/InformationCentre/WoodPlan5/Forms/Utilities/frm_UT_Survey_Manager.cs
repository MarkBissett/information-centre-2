using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // For Path Combine //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Survey_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInSurveyIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        private string i_str_selected_client_ids = "";
        private string i_str_selected_client_names = "";
        private string i_str_selected_status_ids = "";
        private string i_str_selected_statuses = "";
        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultMapPath = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_UT_Survey_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Survey_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10001;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp07001_UT_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07001_UT_Client_FilterTableAdapter.Fill(dataSet_UT.sp07001_UT_Client_Filter);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            
            sp07087_UT_Survey_Status_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07087_UT_Survey_Status_FilterTableAdapter.Fill(dataSet_UT.sp07087_UT_Survey_Status_Filter);
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            popupContainerControlClients.Size = new System.Drawing.Size(312, 423);
            popupContainerControlRegions.Size = new System.Drawing.Size(312, 423);
            
            sp07086_UT_Survey_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SurveyID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedDocumentID");

            sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "SurveyedPoleID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
                if (!strDefaultMapPath.EndsWith("\\")) strDefaultMapPath += "\\";
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (strPassedInSurveyIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit1.EditValue = "Custom Filter";
                popupContainerEdit2.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInSurveyIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_UT_Survey_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Survey_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInSurveyIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "StatusFilter", i_str_selected_status_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDateFilter", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDateFilter", dateEditToDate.DateTime.ToString());                    
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Client Filter //
                int intFoundRow = 0;
                string strDistrictFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strDistrictFilter))
                {
                    Array arrayDistricts = strDistrictFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewClients = (GridView)gridControl2.MainView;
                    viewClients.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayDistricts)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewClients.LocateByValue(0, viewClients.Columns["ClientID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewClients.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewClients.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewClients.EndUpdate();
                    popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                }

                // Status Filter //
                string strRegionFilter = default_screen_settings.RetrieveSetting("StatusFilter");
                if (!string.IsNullOrEmpty(strRegionFilter))
                {
                    Array arrayRegions = strRegionFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewRegions = (GridView)gridControl4.MainView;
                    viewRegions.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRegions)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewRegions.LocateByValue(0, viewRegions.Columns["StatusID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewRegions.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewRegions.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewRegions.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit2_Get_Selected();
                }

                // Date Range Filter //
                string strFromDateFilter = default_screen_settings.RetrieveSetting("FromDateFilter");
                if (!(string.IsNullOrEmpty(strFromDateFilter) || Convert.ToDateTime(strFromDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditFromDate.DateTime = Convert.ToDateTime(strFromDateFilter);
                }
                string strToDateFilter = default_screen_settings.RetrieveSetting("ToDateFilter");
                if (!(string.IsNullOrEmpty(strToDateFilter) || Convert.ToDateTime(strToDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditToDate.DateTime = Convert.ToDateTime(strToDateFilter);
                }
                i_str_date_range = PopupContainerEditDateRange_Get_Selected();
                popupContainerDateRange.EditValue = i_str_date_range;

                bbiRefresh.PerformClick();
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs5)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            else
            {
                view = (GridView)gridControl3.MainView;
            }
            int[] intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Surveys //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerEdit1.EditValue.ToString() == "Custom Filter" && strPassedInSurveyIDs != "")  // Load passed in Records //
            {
                sp07086_UT_Survey_ManagerTableAdapter.Fill(this.dataSet_UT.sp07086_UT_Survey_Manager, "", "", null, null, strPassedInSurveyIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                if (i_dt_FromDate == Convert.ToDateTime("01/01/0001")) i_dt_FromDate = new DateTime(1900, 1, 1);
                if (i_dt_ToDate == Convert.ToDateTime("01/01/0001")) i_dt_ToDate = new DateTime(2900, 1, 1);
                sp07086_UT_Survey_ManagerTableAdapter.Fill(this.dataSet_UT.sp07086_UT_Survey_Manager, i_str_selected_client_ids, i_str_selected_status_ids, i_dt_FromDate, i_dt_ToDate, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyID"])) + ',';
            }

            // Populate Linked Documents //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 52, strDefaultPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            // Populate Surveyed Poles //
            gridControl5.MainView.BeginUpdate();
            this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07102_UT_Survey_Manager_Linked_Surveyed_Poles.Clear();
            }
            else
            {
                sp07102_UT_Survey_Manager_Linked_Surveyed_PolesTableAdapter.Fill(dataSet_UT.sp07102_UT_Survey_Manager_Linked_Surveyed_Poles, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl5.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs5 != "")
            {
                strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl5.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs5 = "";
            }
           
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    if (!iBool_AllowAdd) return;

                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_UT_Survey_Edit fChildForm = new frm_UT_Survey_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    ParentView = (GridView)gridControl1.MainView;
                    intRowHandles = ParentView.GetSelectedRows();
                    if (intRowHandles.Length == 1)
                    {
                        fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                        fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" :ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                case 3:     // Linked Documents //
                    if (!iBool_AllowAdd) return;

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    view = (GridView)gridControl5.MainView;
                    view.PostEditor();
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = "";
                    fChildForm2.strFormMode = "add";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = 0;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 52;  // Survey //

                    ParentView = (GridView)gridControl1.MainView;
                    intRowHandles = ParentView.GetSelectedRows();
                    if (intRowHandles.Length == 1)
                    {
                        fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SurveyID"));
                        fChildForm2.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "ClientName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "SurveyDate")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "Surveyor"));
                    }

                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Related Documents Link //
                    if (!iBool_AllowAdd) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "blockadd";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 52;  // Survey //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Edit fChildForm = new frm_UT_Survey_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 52;  // Survey //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_UT_Survey_Edit fChildForm = new frm_UT_Survey_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                case 3:     // Linked Documents //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl3.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                    fChildForm2.MdiParent = this.MdiParent;
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.strRecordIDs = strRecordIDs;
                    fChildForm2.strFormMode = "edit";
                    fChildForm2.strCaller = this.Name;
                    fChildForm2.intRecordCount = intCount;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2.intRecordTypeID = 52;  // Survey //
                    fChildForm2.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Circuit //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();

                    // Check user created record or is a super user - if not de-select row //
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                    }
                    intRowHandles = view.GetSelectedRows();

                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Surveys to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Survey" : Convert.ToString(intRowHandles.Length) + " Surveys") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Survey" : "these Surveys") + " will no longer be available for selection and any related records will also be deleted!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyID")) + ",";
                        }
 
                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("survey", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Data();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 3:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl3.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }
 
                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        LoadLinkedRecords();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                case 5:  // Surveyed Poles //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl5.MainView;
                    intRowHandles = view.GetSelectedRows();

                    // Check user created record or is a super user - if not de-select row //
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                    }

                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Surveyed Poles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Surveyed Pole" : Convert.ToString(intRowHandles.Length) + " Surveyed Poles") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Surveyed Pole" : "these Surveyed Poles") + " will no longer be available for selection!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyedPoleID")) + ",";
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("surveyed_pole", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        LoadLinkedRecords();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyID")) + ',';
                    }
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                    this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                    frm_UT_Survey_Edit fChildForm = new frm_UT_Survey_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "view";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
        }


        #region Client Filter Panel

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_client_ids = "";    // Reset any prior values first //
            i_str_selected_client_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_client_ids += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_client_names = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_client_names += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_client_names) ? "No Client Filter" : i_str_selected_client_names);
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        #endregion


        #region Status Filter Panel

        private void btnRegionFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_status_ids = "";    // Reset any prior values first //
            i_str_selected_statuses = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_status_ids = "";
                return "No Status Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_status_ids = "";
                return "No Status Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_status_ids += Convert.ToString(view.GetRowCellValue(i, "StatusID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_statuses = Convert.ToString(view.GetRowCellValue(i, "StatusDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_statuses += ", " + Convert.ToString(view.GetRowCellValue(i, "StatusDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_statuses) ? "No Status Filter" : i_str_selected_statuses);
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl2.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Surveys - Click Load Surveys button";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Surveys to view Linked Documents";
                    break;
                case "gridView5":
                    message = "No Surveyed Poles Available - Select one or more Surveys to view Linked Poles";
                    break;
                case "gridView4":
                    message = "No Statuses";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "TargetSurveyCompletionDate")
            {
                DateTime dtDate = (view.GetRowCellValue(e.RowHandle, "TargetSurveyCompletionDate").ToString() == "" ? DateTime.MinValue : Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "TargetSurveyCompletionDate")));
                int intStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SurveyStatusID"));
                if (dtDate != DateTime.MinValue && !(intStatus == 30 || intStatus == 50 || intStatus == 60))
                {
                    if (dtDate < DateTime.Today)  // Overdue //
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    else if (dtDate.AddDays(-7) <= DateTime.Today)  // Due within 7 Days //
                    {
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
            else if (e.Column.FieldName == "TargetPermissionCompletionDate")
            {
                DateTime dtDate = (view.GetRowCellValue(e.RowHandle, "TargetPermissionCompletionDate").ToString() == "" ? DateTime.MinValue : Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "TargetPermissionCompletionDate")));
                int intStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SurveyStatusID"));
                if (dtDate != DateTime.MinValue && !(intStatus == 50 || intStatus == 60))
                {
                    if (dtDate < DateTime.Today)  // Overdue //
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    else if (dtDate.AddDays(-7) <= DateTime.Today)  // Due within 7 Days //
                    {
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
            else if (e.Column.FieldName == "TargetWorkCompletionDate")
            {
                DateTime dtDate = (view.GetRowCellValue(e.RowHandle, "TargetWorkCompletionDate").ToString() == "" ? DateTime.MinValue : Convert.ToDateTime(view.GetRowCellValue(e.RowHandle, "TargetWorkCompletionDate")));
                int intStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "SurveyStatusID"));
                if (dtDate != DateTime.MinValue && intStatus != 60)
                {
                    if (dtDate < DateTime.Today)  // Overdue //
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    else if (dtDate.AddDays(-7) <= DateTime.Today)  // Due within 7 Days //
                    {
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    if (view.SelectedRowsCount == 1)
                    {
                        GridView gvChild = (GridView)gridControl5.MainView;
                        bbiShowMap.Enabled = gvChild.DataRowCount > 0;
                    }
                    else
                    {
                        bbiShowMap.Enabled = false;
                    }
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            LoadLinkedRecords();
            GridView view = (GridView)gridControl3.MainView;
            view.ExpandAllGroups();
            view = (GridView)gridControl5.MainView;
            view.ExpandAllGroups();

            SetMenuStatus();
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "SurveyID").ToString() + ",";
            switch (view.FocusedColumn.Name)
            {
                case "colLinkedPoleCount":
                    {
                        DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetSetting.sp07183_UT_Survey_Manager_Get_Linked_Poles(strParentIDs).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_pole");
                    }
                    break;
                case "colRevisitCount1":
                    {
                        DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetSetting.sp07348_UT_Survey_Manager_Get_Revisit_Surveyed_Poles(strParentIDs).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
                    }
                    break;
                case "colShutdownCount":
                    {
                        DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetSetting.sp07358_UT_Survey_Manager_Get_Shutdown_Surveyed_Poles(strParentIDs).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
                    }
                    break;
                case "colTrafficManagementCount":
                    {
                        DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetSetting.sp07359_UT_Survey_Manager_Get_Traffic_Management_Surveyed_Poles(strParentIDs).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_surveyed_pole");
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPoleCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RevisitCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "ShutdownCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ShutdownCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "TrafficManagementCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "TrafficManagementCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPoleCount")) == 0) e.Cancel = true;
                    break;
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RevisitCount")) == 0) e.Cancel = true;
                    break;
                case "ShutdownCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ShutdownCount")) == 0) e.Cancel = true;
                    break;
                case "TrafficManagementCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("TrafficManagementCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        Load_Data();
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.SelectedRowsCount != 1) return;
            
            GridView gvChild = (GridView)gridControl5.MainView;
            if (gvChild.DataRowCount <= 0) return;
            int[] intRowHandles = view.GetSelectedRows();
            int[] intRowHandlesChild = gvChild.GetSelectedRows();

            System.Reflection.MethodInfo method = null;
            int intSurveyID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "SurveyID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyID")));
            int intClientID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID")));
 
            int intPoleID = 0;
            string strPoleNumber = "";
            if (intRowHandlesChild.Length > 0)
            {
                intPoleID = (string.IsNullOrEmpty(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleID").ToString()) ? 0 : Convert.ToInt32(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleNumber").ToString()) ? "" : gvChild.GetRowCellValue(intRowHandlesChild[0], "PoleNumber").ToString());
            }
            else
            {
                intPoleID = (string.IsNullOrEmpty(gvChild.GetRowCellValue(0, "PoleID").ToString()) ? 0 : Convert.ToInt32(gvChild.GetRowCellValue(0, "PoleID")));
                strPoleNumber = (string.IsNullOrEmpty(gvChild.GetRowCellValue(0, "PoleNumber").ToString()) ? "" : gvChild.GetRowCellValue(0, "PoleNumber").ToString());
            }
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Activate();
                        frm_UT_Mapping frmMapping = (frm_UT_Mapping)frmChild;
                        frmMapping._CurrentPoleID = intPoleID;
                        frmMapping._SelectedSurveyID = intSurveyID;
                        frmMapping._SurveyMode = "PlotTrees"; 
                        frmMapping.LoadSurveyIntoMap();
                        frmMapping.SetModeToSurvey(strPoleNumber);
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            // Get the Workspace ID from the Region on the Survey //
            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(intSurveyID).ToString();
                char[] delimiters = new char[] { '|' };
                string[] strArray = null;
                strArray = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray[0]);
                    strWorkspaceName = strArray[1].ToString();
                    intRegionID = Convert.ToInt32(strArray[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to plot tree via Mapping - the current Survey is linked to a region which has no Mapping Workspace ID.\n\nUpdate the Linked Region with a Mapping Workspace [via the Region Manager] then try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            string strSurveyDescription = "";

            // Get Surveyor Name from grid lookup Edit //
            string strSurveyor = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "Surveyor").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "Surveyor").ToString());

            if (string.IsNullOrEmpty(strSurveyor)) strSurveyor = "Surveyor Not Specified";

            strSurveyDescription = (String.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "Unknown Client" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " +
                (String.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "SurveyDate").ToString()) ? "Unknown Date" : Convert.ToDateTime(view.GetRowCellValue(intRowHandles[0], "SurveyDate")).ToString("dd/MM/yyyy")) + " \\ " +
                    strSurveyor;

            // Open Map and show Survey Panel //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSurveyID;
            frmInstance._SelectedSurveyClientID = intClientID;
            frmInstance._SelectedSurveyDescription = strSurveyDescription;
            frmInstance._PassedInClientIDs = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ClientID"))) ? "" : Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ClientID")) + ",");
            frmInstance._PassedInRegionIDs = (String.IsNullOrEmpty(intRegionID.ToString()) ? "" : intRegionID.ToString() + ",");
            frmInstance._SurveyMode = "PlotTrees";
            frmInstance._CurrentPoleID = intPoleID;
            frmInstance.SetModeToSurvey(strPoleNumber);
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }

            method = typeof(frm_UT_Mapping).GetMethod("SetToolToSelectPoint");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
        }


        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("email".Equals(e.Button.Tag))
                    {
                        Email_Linked_Documents();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            string strExtension = view.GetRowCellValue(view.FocusedRowHandle, "DocumentExtension").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                if (strExtension.ToLower() != "pdf")
                {
                    System.Diagnostics.Process.Start(strFile);
                }
                else
                {
                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFile;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Email_Linked_Documents()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Linked Document record to email before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the file attachments to email //
            List<string> listFilesToEmail = new List<string>();
            foreach (int intRowHandle in intRowHandles)
            {
                listFilesToEmail.Add(Convert.ToString(view.GetRowCellValue(intRowHandle, "DocumentPath")));
            }

            // Get the senders email address //
            string strFromEmailAddress = "";
            try
            {
                WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strFromEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(GlobalSettings.UserID).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain your email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strFromEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You have no email address recorded against your username within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Senders Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get the person to send the email to //
            string strToEmailAddress = "";
            view = (GridView)gridControl1.MainView;
            view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the parent survey before proceeding.", "Email Linked Document(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intSurveyorID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyorID"));
            string strSurveyRemarks = view.GetRowCellValue(intRowHandles[0], "ReactiveRemarks").ToString();
            try
            {
                WoodPlanDataSetTableAdapters.QueriesTableAdapter GetSetting = new WoodPlanDataSetTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strToEmailAddress = GetSetting.sp00240_Get_Staff_Email_Address(intSurveyorID).ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Surveyors email address from the Staff table.\n\nError: " + ex.Message + "\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrWhiteSpace(strToEmailAddress))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There is no email address recorded against the Surveyor within the Staff table.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Surveyors Email Address", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            List<string> _listEmailAddresses = new List<string>();
            _listEmailAddresses.Add(strToEmailAddress);

            // Create the subject line //
            string strSubjectLine = "New ";
            string strExtractedRemarks = "";
            if (!string.IsNullOrWhiteSpace(strSurveyRemarks) && strSurveyRemarks.Contains("-"))
            {
                strExtractedRemarks = strSurveyRemarks.Substring(0, strSurveyRemarks.IndexOf('-') + 8);
            }
            strSubjectLine += (!string.IsNullOrWhiteSpace(strExtractedRemarks) ? strExtractedRemarks : "Reactive Survey") + " please sync your tablet to view this job";

            // Create the message body //
            string strMessageBody = "";

            var frm_child = new frm_Core_Linked_Document_Email();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._SubjectLine = strSubjectLine;
            frm_child._MessageBody = strMessageBody;
            frm_child._FilesToEmail = listFilesToEmail;
            frm_child._EmailAddresses = _listEmailAddresses;
            frm_child._FromEmailAddress = strFromEmailAddress;
            frm_child.ShowDialog();      

        }

        #endregion


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("reload".Equals(e.Button.Tag))
                    {
                        this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RevisitCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "TrafficMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "AccessMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }

        }

        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "RevisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RevisitCount")) == 0) e.Cancel = true;
                    break;
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("TrafficMapPath").ToString())) e.Cancel = true;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("AccessMapPath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }

        }

        private void repositoryItemHyperLinkEditRevisitCount_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.Name)
            {
                case "RevisitCount":
                    {
                        string strParentIDs = view.GetRowCellValue(view.FocusedRowHandle, "SurveyedPoleID").ToString() + ",";

                        DataSet_UTTableAdapters.QueriesTableAdapter GetSetting = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        GetSetting.ChangeConnectionString(strConnectionString);
                        string strRecordIDs = GetSetting.sp07347_UT_Survey_Manager_Get_Revisit_Actions(strParentIDs).ToString();
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "ut_action_revisits");
                    }
                    break;
                case "colTrafficMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "TrafficMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
                case "colAccessMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "AccessMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
            }
        }
        
        #endregion




 

    }
}


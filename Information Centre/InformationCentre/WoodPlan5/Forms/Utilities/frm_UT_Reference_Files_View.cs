using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars.Ribbon;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Reference_Files_View : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public int intSelectedID = 0;

        public int intRecordType = 0;
        public int intPassedInParentID = 0;
        GridHitInfo downHitInfo = null;
        private string strDefaultPath = "";
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        bool isRunning = false;
 
        #endregion

        public frm_UT_Reference_Files_View()
        {
            InitializeComponent();
        }

        private void frm_UT_Reference_Files_View_Load(object sender, EventArgs e)
        {
            this.FormID = 500056;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp07234_UT_Linked_Reference_FilesTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedReferenceFiles").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Reference Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Reference Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            emptyEditor = new RepositoryItem();

            switch (intRecordType)
            {
                case 1:
                    splitContainerControl1.Panel1.Text = "Master Species";
                    break;
                case 2:
                    splitContainerControl1.Panel1.Text = "Master Tree Defects";
                    break;
                case 3:
                    splitContainerControl1.Panel1.Text = "Master Jobs";
                    break;
                default:
                    break;
            }

            try
            {
                sp07240_UT_Reference_Files_ParentsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07240_UT_Reference_Files_ParentsTableAdapter.Fill(dataSet_UT.sp07240_UT_Reference_Files_Parents, intRecordType);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the parents list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInParentID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["RecordID"], intPassedInParentID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.ClearSelection();
                    view.SelectRow(intFoundRow);
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
 
        }

 
        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available");
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            LoadLinkedRecords();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        #endregion


        private void LoadLinkedRecords()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RecordID"])) + ',';
            }

            if (intCount == 0)
            {
                this.dataSet_UT.sp07016_UT_Region_Select.Clear();
            }
            else
            {
                try
                {
                    sp07234_UT_Linked_Reference_FilesTableAdapter.Fill(dataSet_UT.sp07234_UT_Linked_Reference_Files, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intRecordType, strDefaultPath);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the linked reference files.\n\nTry selecting a parent record again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }

            try
            {
                galleryControl1.Gallery.Groups.Clear();
                string strLastGroup = "";
                string strThisGroup = "";
                GalleryItemGroup group1 = null;
                foreach (DataRow dr in dataSet_UT.sp07234_UT_Linked_Reference_Files.Rows)
                {
                    strThisGroup = dr["LinkedRecordDescription"].ToString();
                    if (strThisGroup != strLastGroup && !string.IsNullOrEmpty(strThisGroup))
                    {
                        group1 = new GalleryItemGroup();
                        group1.Caption = strThisGroup;
                        galleryControl1.Gallery.Groups.Add(group1);
                        strLastGroup = strThisGroup;
                    }
                    Image im1 = null;
                    string strFileExtension = dr["DocumentExtension"].ToString().ToLower();
                    switch (strFileExtension)
                    {
                        case "png":
                        case "gif":
                        case "jpg":
                        case "jpeg":
                        case "bmp":
                        case "tiff":
                            im1 = Image.FromFile(dr["DocumentPath"].ToString());
                            break;
                        case "xml":
                        case "htm":
                        case "html":
                            im1 = imageCollection1.Images[0];
                            break;
                        case "xls":
                        case "xlxs":
                            im1 = imageCollection1.Images[1];
                            break;
                        case "avi":
                        case "mp4":
                            im1 = imageCollection1.Images[2];
                            break;
                        case "wav":
                        case "mp3":
                            im1 = imageCollection1.Images[3];
                            break;
                        case "pdf":
                            im1 = imageCollection1.Images[4];
                            break;
                        case "ppt":
                            im1 = imageCollection1.Images[5];
                            break;
                        case "txt":
                            im1 = imageCollection1.Images[6];
                            break;
                        case "doc":
                        case "docx":
                            im1 = imageCollection1.Images[7];
                            break;
                        default:
                            im1 = imageCollection1.Images[6];
                            break;
                    }

                    //im1 = Image.FromFile(dr["DocumentPath"].ToString());
                    GalleryItem gi = new GalleryItem();
                    gi.Image = im1;
                    gi.Caption = dr["Description"].ToString();
                    gi.Tag = dr["DocumentPath"].ToString();

                    SuperToolTip sTooltip2 = new SuperToolTip();
                    SuperToolTipSetupArgs args = new SuperToolTipSetupArgs();  // Create an object to initialize the SuperToolTip. //
                    args.Title.Text = "Reference File - Remarks";
                    args.Title.Image = imageCollection2.Images[0];
                    args.Contents.Text = (string.IsNullOrEmpty(dr["DocumentRemarks"].ToString()) ? "No Remarks" : dr["DocumentRemarks"].ToString());
                    sTooltip2.Setup(args);

                    gi.SuperTip = sTooltip2;
                    group1.Items.Add(gi);
                }
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while loading the linked reference files.\n\nTry selecting a parent record again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedValue))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
           /* GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) + " \\ " +
                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName"))) ? "Unknown Region" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName")));
                strSelectedClientName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                strSelectedRegionName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName"))) ? "Unknown Region" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName")));
                intSelectedClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                intSelectedRegionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RegionID"));

                strSelectedWorkspace = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "WorkspaceName"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "WorkspaceName")));
                intSelectedWorkspaceID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkspaceID"));
            }*/
        }

        private void galleryControl1_Gallery_ItemClick(object sender, GalleryItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Item.Tag.ToString())) return;
            string strFile = e.Item.Tag.ToString();

            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }




    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_UT_Surveyed_Tree_Work_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToSurveyedTreeID = 0;
        public string strLinkedToSurveyedTree = "";
        public DateTime dtSurveyDate = DateTime.MinValue;
        public string strDefaultPath = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        bool iBool_AllowDelete = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowAdd = true;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //

        private string i_str_AddedRecordIDs1 = "";

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";

        bool boolPicturesListUpdated = false;
        bool SkipPictureCountCheck = false;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        #endregion

        public frm_UT_Surveyed_Tree_Work_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Surveyed_Tree_Work_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500025;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "SurveyPictureID");

            sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView3, "ActionEquipmentID");
            
            sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView5, "ActionMaterialID");

            sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07138_UT_Unit_Descriptors_With_Blank);

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            // Populate Main Dataset //          
            sp07128_UT_Surveyed_Tree_Work_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["SurveyedTreeID"] = intLinkedToSurveyedTreeID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToSurveyedTree;
                        drNewRow["ReferenceNumber"] = _strLastUsedReferencePrefix;
                        drNewRow["DateRaised"] = DateTime.Now;
                        drNewRow["JobTypeID"] = 0;
                        drNewRow["Approved"] = 0;
                        drNewRow["ApprovedByStaffID"] = 0;
                        drNewRow["EstimatedLabourCost"] = 0.00;
                        drNewRow["ActualLabourCost"] = 0.00;
                        drNewRow["EstimatedMaterialsCost"] = 0.00;
                        drNewRow["ActualMaterialsCost"] = 0.00;
                        drNewRow["EstimatedEquipmentCost"] = 0.00;
                        drNewRow["ActualEquipmentCost"] = 0.00;
                        drNewRow["EstimatedTotalCost"] = 0.00;
                        drNewRow["ActualTotalCost"] = 0.00;

                        drNewRow["EstimatedLabourSell"] = 0.00;
                        drNewRow["ActualLabourSell"] = 0.00;
                        drNewRow["EstimatedMaterialsSell"] = 0.00;
                        drNewRow["ActualMaterialsSell"] = 0.00;
                        drNewRow["EstimatedEquipmentSell"] = 0.00;
                        drNewRow["ActualEquipmentSell"] = 0.00;
                        drNewRow["EstimatedTotalSell"] = 0.00;
                        drNewRow["ActualTotalSell"] = 0.00;

                        drNewRow["WorkOrderID"] = 0;
                        drNewRow["SelfBillingInvoiceID"] = 0;
                        drNewRow["FinanceSystemBillingID"] = 0;
                        drNewRow["PossibleFlail"] = 0;
                        drNewRow["AchievableClearance"] = 0.00;

                        drNewRow["StumpTreatmentNone"] = 1;
                        drNewRow["StumpTreatmentEcoPlugs"] = 0;
                        drNewRow["StumpTreatmentSpraying"] = 0;
                        drNewRow["StumpTreatmentPaint"] = 0;
                        drNewRow["TreeReplacementNone"] = 1;
                        drNewRow["TreeReplacementWhips"] = 0;
                        drNewRow["TreeReplacementStandards"] = 0;

                        drNewRow["EstimatedHours"] = 0.00;
                        drNewRow["ActualHours"] = 0.00;
                        drNewRow["PossibleLiveWork"] = 0.00;

                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["SurveyDate"] = dtSurveyDate;
                        this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows.Add(drNewRow);
                        if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix))  // Add Suffix number to Reference Number //
                        {
                            Get_Suffix_Number();  // Get next value in sequence //
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedRecordDescription"] = "N\\A - Block Editing";
                        drNewRow["ReferenceNumber"] = "";
                        this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows.Add(drNewRow);
                        this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07128_UT_Surveyed_Tree_Work_EditTableAdapter.Fill(this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
 
                    Load_Linked_Pictures();

                    Load_Linked_Equipment();

                    Load_Linked_Material();

                    if (strFormMode == "view")
                    {
                        iBool_AllowDelete = false;
                        iBool_AllowEdit = false;
                        iBool_AllowAdd = false;
                    }
                    break;

            }
            dataLayoutControl1.EndUpdate();

            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;

            m_customArrowLocusEffect1 = new ArrowLocusEffect();
            m_customArrowLocusEffect1.Name = "CustomeArrow1";
            m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
            m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
            m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
            m_customArrowLocusEffect1.MovementCycles = 20;
            m_customArrowLocusEffect1.MovementAmplitude = 200;
            m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
            m_customArrowLocusEffect1.LeadInTime = 0; //msec
            m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
            m_customArrowLocusEffect1.AnimationTime = 2000; //msec
            locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
         }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Surveyed Tree Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        JobDescriptionButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    btnSave.Enabled = false;
                    gridControl1.Enabled = true;
                    gridControl2.Enabled = true;
                    gridControl3.Enabled = true;
 
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        JobDescriptionButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    btnSave.Enabled = false;
                    gridControl1.Enabled = false;
                    gridControl2.Enabled = false;
                    gridControl3.Enabled = false;
                    
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        JobDescriptionButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    btnSave.Enabled = false;
                    gridControl1.Enabled = true;
                    gridControl2.Enabled = true;
                    gridControl3.Enabled = true;
                    
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        JobDescriptionButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    btnSave.Enabled = false;
                    gridControl1.Enabled = false;
                    gridControl2.Enabled = false;
                    gridControl3.Enabled = false;

                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;

                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        btnSave.Enabled = false;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl3.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            ibool_FormStillLoading = false;

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
           /* PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9151", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9151:  // Pole Types Picklist //    
                        {
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            } */
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null || boolPicturesListUpdated)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
                gridControl2.Enabled = false;
                gridControl3.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intActionID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow != null)
            {
                intActionID = (currentRow["ActionID"] == null ? 0 : Convert.ToInt32(currentRow["ActionID"]));
            }

            if (i_int_FocusedGrid == 1)  // Linked Work Pictures //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && strFormMode != "blockedit")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Linked Equipment //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Material //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
 
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView2 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }


        private void frm_UT_Surveyed_Tree_Work_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_UT_Surveyed_Tree_Work_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix))
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ReferenceNumberPrefix", _strLastUsedReferencePrefix);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
            
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("ReferenceNumberPrefix");
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SkipPictureCountCheck = false;
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            if (!SkipPictureCountCheck)
            {
                int intRowCount = view.DataRowCount;
                if (intRowCount <= 0)
                {
                    XtraMessageBox.Show("Unable to save changes - at least one picture must be taken before proceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                    // Use locus effect to highlight the Species List for the user to click add on //
                    try
                    {
                        Control c = gridControl1;
                        dataLayoutControl1.GetItemByControl(c);
                        DevExpress.XtraLayout.LayoutControlItem item = dataLayoutControl1.GetItemByControl(c);
                        if (item != null)
                        {
                            dataLayoutControl1.FocusHelper.PlaceItemIntoView(item);
                            dataLayoutControl1.FocusHelper.FocusElement(item, true);
                            gridControl1.Focus();  // Set active control first //
                            Rectangle rect = Rectangle.Empty;
                            if (dataLayoutControl1.ActiveControl.Parent is BaseEdit)
                            {
                                rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Parent.Bounds);
                            }
                            else
                            {
                                rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Bounds);
                            }
                            if (rect.Width > 0 && rect.Height > 0)
                            {
                                System.Drawing.Point point = new System.Drawing.Point(rect.X + 5, (rect.Height / 2) + rect.Y);
                                locusEffectsProvider1.ShowLocusEffect(this, point, m_customArrowLocusEffect1.Name);
                            }
                        }
                     }
                    catch (Exception)
                    {
                    }                   
                    return "Error";
                }
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
            try
            {
                this.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            view = (GridView)gridControl2.MainView;
            view.PostEditor();
            view = (GridView)gridControl3.MainView;
            view.PostEditor();
 
            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["ActionID"]) + ";";

                strRecordIDs = Convert.ToInt32(currentRow["ActionID"]) + ",";  // Needed so any linked records subsequently added can be retrieved //

                // Switch mode to Edit so that any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
                
                // Pass new ID to any records in the linked Jobs grid //
                int intActionID = Convert.ToInt32(currentRow["ActionID"]);
                view = (GridView)gridControl2.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.Rows)
                {
                    dr["ActionID"] = intActionID;
                }
                this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();
 
                view = (GridView)gridControl3.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.Rows)
                {
                    dr["ActionID"] = intActionID;
                }
                this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
            }
            if (this.strFormMode.ToLower() == "add" || this.strFormMode.ToLower() == "edit")
            {
                try  // Attempt to save any changes to Linked Linked Equipment... //
                {
                    sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
                }
                catch (System.Exception ex)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                    XtraMessageBox.Show("An error occurred while saving the linked Equipment changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "Error";
                }

                try  // Attempt to save any changes to Linked Linked Material... //
                {
                    sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
                }
                catch (System.Exception ex)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                    XtraMessageBox.Show("An error occurred while saving the linked Materials changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return "Error";
                }
            }
            // Notify any open instances of parent screens they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    switch (frmChild.Name)
                    {
                        case "frm_UT_Survey_Pole2":
                            {
                                var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                                fParentForm.UpdateFormRefreshStatus(6, "", "", "", strNewIDs, "", "", "", "", "", "", "", "", "", "", "", "", "");
                            }
                            break;
                        case "frm_UT_WorkOrder_Edit":
                            {
                                var fParentForm = (frm_UT_WorkOrder_Edit)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                            }
                            break;
                        case "frm_UT_Action_Manager":
                            {
                                var fParentForm = (frm_UT_Action_Manager)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                            }
                            break;
                        case "frm_UT_Permission_Doc_Edit_WPD":
                            {
                                var fParentForm = (frm_UT_Permission_Doc_Edit_WPD)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "");
                            }
                            break;
                        case "frm_UT_Permission_Doc_Edit_UKPN":
                            {
                                var fParentForm = (frm_UT_Permission_Doc_Edit_UKPN)frmChild;
                                fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "");
                            }
                            break;
                        default:
                            break;
                    }
                }    
            }

            // Check if any new default linked equipment and materials have been added as a result of saving the action //
            Load_Linked_Equipment();
            Load_Linked_Material();
            FilterGrids();
            boolPicturesListUpdated = false;

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intLinkedPicturesNew = 0;
            int intLinkedPicturesModified = 0;
            int intLinkedPicturesDeleted = 0;
            this.sp07130UTSurveyedPoleWorkPicturesListBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07130_UT_Surveyed_Pole_Work_Pictures_List.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07130_UT_Surveyed_Pole_Work_Pictures_List.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedPicturesNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedPicturesModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedPicturesDeleted++;
                        break;
                }
            }

            int intLinkedEquipmentNew = 0;
            int intLinkedEquipmentModified = 0;
            int intLinkedEquipmentDeleted = 0;
            this.sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedEquipmentNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedEquipmentModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedEquipmentDeleted++;
                        break;
                }
            }

            int intLinkedMaterialsNew = 0;
            int intLinkedMaterialsModified = 0;
            int intLinkedMaterialsDeleted = 0;
            this.sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedMaterialsNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedMaterialsModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedMaterialsDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedPicturesNew > 0 || intLinkedPicturesModified > 0 || intLinkedPicturesDeleted > 0 || intLinkedEquipmentNew > 0 || intLinkedEquipmentModified > 0 || intLinkedEquipmentDeleted > 0 || intLinkedMaterialsNew > 0 || intLinkedMaterialsModified > 0 || intLinkedMaterialsDeleted > 0 || boolPicturesListUpdated)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                if (intLinkedPicturesNew > 0) strMessage += Convert.ToString(intLinkedPicturesNew) + " New linked Work Picture record(s)\n";
                if (intLinkedPicturesModified > 0) strMessage += Convert.ToString(intLinkedPicturesModified) + " Updated linked Work Picture record(s)\n";
                if (intLinkedPicturesDeleted > 0) strMessage += Convert.ToString(intLinkedPicturesDeleted) + " Deleted linked Work Picture record(s)\n";
                if (intLinkedEquipmentNew > 0) strMessage += Convert.ToString(intLinkedEquipmentNew) + " New linked Equipment record(s)\n";
                if (intLinkedEquipmentModified > 0) strMessage += Convert.ToString(intLinkedEquipmentModified) + " Updated linked Equipment record(s)\n";
                if (intLinkedEquipmentDeleted > 0) strMessage += Convert.ToString(intLinkedEquipmentDeleted) + " Deleted linked Equipment record(s)\n";
                if (intLinkedMaterialsNew > 0) strMessage += Convert.ToString(intLinkedMaterialsNew) + " New linked Material record(s)\n";
                if (intLinkedMaterialsModified > 0) strMessage += Convert.ToString(intLinkedMaterialsModified) + " Updated linked Material record(s)\n";
                if (intLinkedMaterialsDeleted > 0) strMessage += Convert.ToString(intLinkedMaterialsDeleted) + " Deleted linked Material record(s)\n";
                if (boolPicturesListUpdated) strMessage += "Updated\\Deleted linked Work Picture record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void Load_Linked_Pictures()
        {
            gridControl1.BeginUpdate();
            this.RefreshGridViewState1.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07130_UT_Surveyed_Pole_Work_Pictures_List, strRecordIDs, strDefaultPath);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

        }

        private void Load_Linked_Equipment()
        {
            gridControl2.BeginUpdate();
            try
            {
                sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter.Fill(dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment, strRecordIDs, strFormMode);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();
        }

        private void Load_Linked_Material()
        {
            gridControl3.BeginUpdate();
            try
            {
                sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter.Fill(dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials, strRecordIDs, strFormMode);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl2.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void JobDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                if (strFormMode == "view") return;
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                if (currentRow == null) return;
                int intJobTypeID = (string.IsNullOrEmpty(currentRow["JobTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobTypeID"]));
                frm_UT_Select_Job_Type fChildForm = new frm_UT_Select_Job_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalJobID = intJobTypeID;
                 if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["JobTypeID"] = fChildForm.intSelectedJobID;
                    currentRow["JobDescription"] = fChildForm.strSelectedJobDescription;
                    sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "reference_files")
            {
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                int intCurrentRecordID = 0;
                if (currentRow != null)
                {

                    intCurrentRecordID = (string.IsNullOrEmpty(currentRow["JobTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["JobTypeID"]));
                }
                frm_UT_Reference_Files_View fChildForm = new frm_UT_Reference_Files_View();
                fChildForm.intPassedInParentID = intCurrentRecordID;
                fChildForm.intRecordType = 3;  // Master Job Type //
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                }
            }

        }
        private void JobDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(JobDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(JobDescriptionButtonEdit, "");
            }
        }

        private void ReferenceNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "UT_Action";
                fChildForm.PassedInFieldName = "ReferenceNumber";
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    ReferenceNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                Get_Suffix_Number();  // Get next value in sequence //  
            }
        }
        private void ReferenceNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ReferenceNumberButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ReferenceNumberButtonEdit, "");
            }
        }
        private void Get_Suffix_Number()
        {
            string strSequence = ReferenceNumberButtonEdit.EditValue.ToString() ?? "";

            SequenceNumberGetNext sequence = new SequenceNumberGetNext();
            string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, "ReferenceNumber", "UT_Action", strSequence);  // Calculate Next in Sequence //

            if (strNextNumber == "")
            {
                return;
            }
            if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

            int intRequiredLength = strNextNumber.Length;
            int intNextNumber = Convert.ToInt32(strNextNumber);
            int intIncrement = 0;
            string strTempNumber = "";


            // Check if value is already present within dataset //
            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            string strCurrentActionID = "";
            if (!string.IsNullOrEmpty(currentRow["ActionID"].ToString())) strCurrentActionID = currentRow["ActionID"].ToString();

            bool boolUniqueValueFound = false;
            bool boolDuplicateFound = false;
            do
            {
                boolDuplicateFound = false;
                strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                }
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07128_UT_Surveyed_Tree_Work_Edit.Rows)
                {
                    if (dr["ReferenceNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                    {
                        intIncrement++;
                        boolDuplicateFound = true;
                        break;
                    }
                }
                if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
            } while (!boolUniqueValueFound);
            ReferenceNumberButtonEdit.EditValue = strSequence + strTempNumber;
        }

        private void AchievableClearanceSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(se.EditValue.ToString()) || Convert.ToInt32(se.EditValue) == 0))
            {
                dxErrorProvider1.SetError(AchievableClearanceSpinEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AchievableClearanceSpinEdit, "");
            }
        }

        private void EstimatedLabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedLabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }

        private void ActualLabourCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualLabourCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }

        private void EstimatedMaterialsCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedMaterialsCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }

        private void ActualMaterialsCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualMaterialsCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }

        private void EstimatedEquipmentCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedEquipmentCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }

        private void ActualEquipmentCostSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualEquipmentCostSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Costs(se.Name, decValue);
        }


        private void EstimatedLabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedLabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void ActualLabourSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualLabourSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void EstimatedEquipmentSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedEquipmentSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void ActualEquipmentSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualEquipmentSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void EstimatedMaterialsSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void EstimatedMaterialsSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void ActualMaterialsSellSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ActualMaterialsSellSpinEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Total_Sell(se.Name, decValue);
        }

        private void EstimatedHoursSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(se.EditValue.ToString()) || Convert.ToDecimal(EstimatedHoursSpinEdit.EditValue) == (decimal)0.00))
            {
                dxErrorProvider1.SetError(EstimatedHoursSpinEdit, "Enter a value");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EstimatedHoursSpinEdit, "");
            }

        }

        private void StumpTreatmentNoneCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                StumpTreatmentEcoPlugsSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentEcoPlugsSpinEdit.EditValue = 0;
                StumpTreatmentSprayingSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentSprayingSpinEdit.EditValue = 0;
                StumpTreatmentPaintSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentPaintSpinEdit.EditValue = 0;
                
                StumpTreatmentEcoPlugsSpinEdit.Text = "(Disabled - Untick None to Enable)";
                StumpTreatmentSprayingSpinEdit.Text = "(Disabled - Untick None to Enable)";
                StumpTreatmentPaintSpinEdit.Text = "(Disabled - Untick None to Enable)";
            }
            else
            {
                StumpTreatmentEcoPlugsSpinEdit.Properties.ReadOnly = false;
                StumpTreatmentSprayingSpinEdit.Properties.ReadOnly = false;
                StumpTreatmentPaintSpinEdit.Properties.ReadOnly = false;

                StumpTreatmentEcoPlugsSpinEdit.Text = "(Tick if Yes)";
                StumpTreatmentSprayingSpinEdit.Text = "(Tick if Yes)";
                StumpTreatmentPaintSpinEdit.Text = "(Tick if Yes)";

            }
        }

        private void TreeReplacementNoneCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                if (currentRow == null) return;
                currentRow["TreeReplacementWhips"] = 0;
                currentRow["TreeReplacementStandards"] = 0;
                sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();

                TreeReplacementWhipsSpinEdit.Properties.ReadOnly = true;
                TreeReplacementStandardsSpinEdit.Properties.ReadOnly = true;

                TreeReplacementWhipsSpinEdit.ToolTip = "(Disabled - Untick None to Enable)";
                TreeReplacementStandardsSpinEdit.ToolTip = "(Disabled - Untick None to Enable)";
            }
            else
            {
                TreeReplacementWhipsSpinEdit.Properties.ReadOnly = false;
                TreeReplacementStandardsSpinEdit.Properties.ReadOnly = false;

                TreeReplacementWhipsSpinEdit.ToolTip = "";
                TreeReplacementStandardsSpinEdit.ToolTip = "";
            }
        }

        private void RevisitDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void RevisitDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            bool boolError = false;
            DateTime dtSurveyDate = DateTime.MinValue; 
            if (this.strFormMode != "blockedit")
            {
                DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
                if (currentRow == null) return;               
                try
                {
                    dtSurveyDate = Convert.ToDateTime(currentRow["SurveyDate"]);
                    if ((string.IsNullOrEmpty(de.EditValue.ToString()) || de.DateTime == DateTime.MinValue))
                    {
                        boolError = true;
                    }
                    else if (de.DateTime.Year - dtSurveyDate.Year < 1)
                    {
                        boolError = true;
                    }
                }
                catch (Exception) { }
            }

            if (boolError)
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "Enter a Date at least a year later than the Survey Date [" + dtSurveyDate.ToString("dd/MM/yyyy") + "].");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "");
            }
        }


        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Work Pictures NOT Shown When Block Adding and Block Editing" : "No Linked Work Pictures Available - Click the Add button to Create Linked Work Pictures");
                    break;
                case "gridView3":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Equipment NOT Shown When Block Adding and Block Editing" : "No Linked Equipment Available - Click the Add button to Create Linked Equipment");
                    break;
                case "gridView5":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Materials NOT Shown When Block Adding and Block Editing" : "No Linked Materials Available - Click the Add button to Create Linked Materials");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridControl1

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridControl2

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditEquipment_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                GridView view = (GridView)gridControl2.MainView;
                int intFocusedRow = view.FocusedRowHandle;
                if (intFocusedRow == GridControl.InvalidRowHandle) return;
                DataRowView currentRow = (DataRowView)sp07131UTSurveyedTreeWorkLinkedEquipmentBindingSource.Current;
                if (currentRow == null) return;

                int intCurrentID = (string.IsNullOrEmpty(currentRow["EquipmentID"].ToString()) ? 0 : Convert.ToInt32(currentRow["EquipmentID"]));
                frm_UT_Select_Master_Equipment fChildForm = new frm_UT_Select_Master_Equipment();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedID = intCurrentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    gridControl2.BeginUpdate();
                    currentRow["EquipmentID"] = fChildForm.intSelectedID;
                    currentRow["EquipmentDescription"] = fChildForm.strSelectedDescription;
                    currentRow["CostPerUnit"] = fChildForm.decCostPerHour;
                    currentRow["SellPerUnit"] = fChildForm.decSellPerHour;
                    sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.EndEdit();
                    Calculate_Equipment_Line_Total(view, intFocusedRow, "", (decimal)0.00);
                    gridControl2.Refresh();
                    gridControl2.EndUpdate();
                }
            }
        }

        private void repositoryItemSpinEditHours_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void repositoryItemSpinEditHours_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridView view = (GridView)gridControl2.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Equipment_Line_Total(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
        }

        private void repositoryItemSpinEditCost_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void repositoryItemSpinEditCost_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridView view = (GridView)gridControl2.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Equipment_Line_Total(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
        }


        #endregion


        #region GridControl3

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridView5_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditMaterialDescription_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                GridView view = (GridView)gridControl3.MainView;
                int intFocusedRow = view.FocusedRowHandle;
                if (intFocusedRow == GridControl.InvalidRowHandle) return;
                DataRowView currentRow = (DataRowView)sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.Current;
                if (currentRow == null) return;

                int intCurrentID = (string.IsNullOrEmpty(currentRow["MaterialID"].ToString()) ? 0 : Convert.ToInt32(currentRow["MaterialID"]));
                frm_UT_Select_Master_Material fChildForm = new frm_UT_Select_Master_Material();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedID = intCurrentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    gridControl3.BeginUpdate();
                    currentRow["MaterialID"] = fChildForm.intSelectedID;
                    currentRow["MaterialDescription"] = fChildForm.strSelectedDescription;
                    currentRow["CostPerUnit"] = fChildForm.decCostPerUnit;
                    currentRow["SellPerUnit"] = fChildForm.decSellPerUnit;
                    currentRow["UnitsDescriptorID"] = fChildForm.intUnitDescriptorID;
                    Calculate_Material_Line_Total(view, intFocusedRow, "", (decimal)0.00);
                    sp07132UTSurveyedTreeWorkLinkedMaterialsBindingSource.EndEdit();
                    gridControl3.Refresh();
                    gridControl3.EndUpdate();
                }
            }
        }

        private void repositoryItemSpinEdit2DP_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void repositoryItemSpinEdit2DP_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridView view = (GridView)gridControl3.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Material_Line_Total(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
        }

        private void repositoryItemSpinEditCurrency_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void repositoryItemSpinEditCurrency_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridView view = (GridView)gridControl3.MainView;
            SpinEdit se = (SpinEdit)sender;
            decimal decValue = Convert.ToDecimal(se.Value);
            Calculate_Material_Line_Total(view, view.FocusedRowHandle, view.FocusedColumn.FieldName, decValue);  // This is also done later for rows after this one //
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }



        private void Add_Record()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            int intActionID = 0;
            if (!string.IsNullOrEmpty(currentRow["ActionID"].ToString())) intActionID = Convert.ToInt32(currentRow["ActionID"]);
            if (intActionID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the Tree Work record first before adding linked records.", "Add Linked Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                
                // Use locus effect to highlight the correct button for the user to click //
                try
                {
                    Control c = btnSave;
                    dataLayoutControl1.GetItemByControl(c);
                    DevExpress.XtraLayout.LayoutControlItem item = dataLayoutControl1.GetItemByControl(c);
                    if (item != null)
                    {
                        dataLayoutControl1.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutControl1.FocusHelper.FocusElement(item, true);
                        btnSave.Focus();  // Set active control first //
                        Rectangle rect = Rectangle.Empty;
                        if (dataLayoutControl1.ActiveControl.Parent is BaseEdit)
                        {
                            rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Parent.Bounds);
                        }
                        else
                        {
                            rect = dataLayoutControl1.RectangleToScreen(dataLayoutControl1.ActiveControl.Bounds);
                        }
                        if (rect.Width > 0 && rect.Height > 0)
                        {
                            System.Drawing.Point point = new System.Drawing.Point(rect.X + 5, (rect.Height / 2) + rect.Y);
                            locusEffectsProvider1.ShowLocusEffect(this, point, m_customArrowLocusEffect1.Name);
                        }
                    }
                }
                catch (Exception)
                {
                }                   

             
                return;
            }
            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Work Pictures //
                    {
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "add";
                        fChildForm.intAddToRecordID = intActionID;
                        fChildForm.intAddToRecordTypeID = 2;  // 1 = Surveyed Tree Work //
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.ShowDialog();
                        int intNewPictureID = fChildForm._AddedPolePictureID;
                        if (intNewPictureID != 0)  // At least 1 picture added to refresh picture grid //
                        {
                            i_str_AddedRecordIDs1 = intNewPictureID.ToString() + ";";
                            this.RefreshGridViewState1.SaveViewInfo();
                            Load_Linked_Pictures();
                            boolPicturesListUpdated = true;
                            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //
                        }
                    }
                    break;
                case 2:  // Linked Equipment //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["ActionID"] = intActionID;
                            drNewRow["EquipmentID"] = 0;
                            drNewRow["EquipmentDescription"] = "";
                            drNewRow["CreatedFromDefaultRequiredEquipmentID"] = 0;
                            drNewRow["CostPerUnit"] = 0.00;
                            drNewRow["SellPerUnit"] = 0.00;
                            drNewRow["EstimatedHours"] = 0.00;
                            drNewRow["ActualHours"] = 0.00;
                            drNewRow["EstimatedCost"] = 0.00;
                            drNewRow["ActualCost"] = 0.00;
                            drNewRow["EstimatedSell"] = 0.00;
                            drNewRow["ActualSell"] = 0.00;

                            this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new Equipment record - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Linked Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case 3:  // Linked Material //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["ActionID"] = intActionID;
                            drNewRow["MaterialID"] = 0;
                            drNewRow["MaterialDescription"] = "";
                            drNewRow["UnitsDescriptorID"] = 0;
                            drNewRow["CostPerUnit"] = 0.00;
                            drNewRow["SellPerUnit"] = 0.00;
                            drNewRow["EstimatedUnits"] = 0.00;
                            drNewRow["ActualUnits"] = 0.00;
                            drNewRow["EstimatedCost"] = 0.00;
                            drNewRow["ActualCost"] = 0.00;
                            drNewRow["EstimatedSell"] = 0.00;
                            drNewRow["ActualSell"] = 0.00;
                            drNewRow["UnitsDescriptorID"] = 0; 

                            this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new Material record - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Linked Material", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    break;
            }
 
         }

        private void Block_Add_Record()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            int intActionID = 0;
            if (!string.IsNullOrEmpty(currentRow["ActionID"].ToString())) intActionID = Convert.ToInt32(currentRow["ActionID"]);
            if (intActionID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Save the Tree Work record first before adding linked records.", "Block Add Linked Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            switch (i_int_FocusedGrid)
            {
                case 2:  // Linked Equipment //
                    {
                        frm_UT_Surveyed_Tree_Work_Block_Add_Equipment fChildForm = new frm_UT_Surveyed_Tree_Work_Block_Add_Equipment();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        string strSelectedIDs = "";
                        int intSelectedCount = 0;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        strSelectedIDs = fChildForm.strSelectedIDs;
                        intSelectedCount = fChildForm.intSelectedCount;

                        GridView Parentview = (GridView)fChildForm.gridControl1.MainView;
                        if (Parentview.DataRowCount <= 0) return;
                        try
                        {
                            for (int i = 0; i < Parentview.DataRowCount; i++)
                            {
                                if (Convert.ToBoolean(Parentview.GetRowCellValue(i, "CheckMarkSelection")))
                                {
                                    DataRow drNewRow;
                                    drNewRow = this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["ActionID"] = intActionID;
                                    drNewRow["EquipmentID"] = Convert.ToInt32(Parentview.GetRowCellValue(i, "EquipmentID"));
                                    drNewRow["EquipmentDescription"] = Convert.ToString(Parentview.GetRowCellValue(i, "Description"));
                                    drNewRow["CostPerUnit"] = Convert.ToDecimal(Parentview.GetRowCellValue(i, "CostPerHour"));
                                    drNewRow["SellPerUnit"] = Convert.ToDecimal(Parentview.GetRowCellValue(i, "SellPerHour"));
                                    drNewRow["EstimatedHours"] = 0.00;
                                    drNewRow["ActualHours"] = 0.00;
                                    drNewRow["EstimatedCost"] = 0.00;
                                    drNewRow["ActualCost"] = 0.00;
                                    drNewRow["EstimatedSell"] = 0.00;
                                    drNewRow["ActualSell"] = 0.00;
                                    this.dataSet_UT_Edit.sp07131_UT_Surveyed_Tree_Work_Linked_Equipment.Rows.Add(drNewRow);
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new equipment - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            break;
                        }
                    }
                    break;
                case 3:  // Linked Materials //
                    {
                        frm_UT_Surveyed_Tree_Work_Block_Add_Material fChildForm = new frm_UT_Surveyed_Tree_Work_Block_Add_Material();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        string strSelectedIDs = "";
                        int intSelectedCount = 0;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;
                        strSelectedIDs = fChildForm.strSelectedIDs;
                        intSelectedCount = fChildForm.intSelectedCount;

                        GridView Parentview = (GridView)fChildForm.gridControl1.MainView;
                        if (Parentview.DataRowCount <= 0) return;
                        try
                        {
                            for (int i = 0; i < Parentview.DataRowCount; i++)
                            {
                                if (Convert.ToBoolean(Parentview.GetRowCellValue(i, "CheckMarkSelection")))
                                {
                                    DataRow drNewRow;
                                    drNewRow = this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.NewRow();
                                    drNewRow["strMode"] = "add";
                                    drNewRow["ActionID"] = intActionID;
                                    drNewRow["MaterialID"] = Convert.ToInt32(Parentview.GetRowCellValue(i, "MaterialID"));
                                    drNewRow["MaterialDescription"] = Convert.ToString(Parentview.GetRowCellValue(i, "Description"));
                                    drNewRow["CostPerUnit"] = Convert.ToDecimal(Parentview.GetRowCellValue(i, "CostPerUnit"));
                                    drNewRow["SellPerUnit"] = Convert.ToDecimal(Parentview.GetRowCellValue(i, "SellPerUnit"));
                                    drNewRow["EstimatedUnits"] = 0.00;
                                    drNewRow["ActualUnits"] = 0.00;
                                    drNewRow["EstimatedCost"] = 0.00;
                                    drNewRow["ActualCost"] = 0.00;
                                    drNewRow["EstimatedSell"] = 0.00;
                                    drNewRow["ActualSell"] = 0.00;
                                    drNewRow["UnitsDescriptorID"] = Convert.ToInt32(Parentview.GetRowCellValue(i, "UnitDescriptorID")); 
                                    this.dataSet_UT_Edit.sp07132_UT_Surveyed_Tree_Work_Linked_Materials.Rows.Add(drNewRow);
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new equipment - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Equipment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            break;
                        }
                    }
                    break;
                 default:
                    break;
            }
            SetChangesPendingLabel();
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Picture //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.ShowDialog();
                        this.RefreshGridViewState1.SaveViewInfo();
                        Load_Linked_Pictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Tree Work Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Work Picture" : Convert.ToString(intRowHandles.Length) + " Tree Work Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Work Picture" : "these Tree Work Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Tree Work Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("survey_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Pictures();
                            FilterGrids();
                            boolPicturesListUpdated = true;
                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:  // Linked Equipment //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pieces of Equipment to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Piece of Equipment" : Convert.ToString(intRowHandles.Length) + " Linked Pieces of Equipment") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Piece of Equipment" : "these Linked Pieces of Equipment") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            Calculate_Equipment_Total(view);
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Linked Piece(s) of Equipment deleted.\n\nImportant Note: Deleted Linked Pieces of Equipment are only physically removed from the work record on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
                case 3:  // Linked Material //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Materials to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Material" : Convert.ToString(intRowHandles.Length) + " Linked Materials") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Linked Material" : "these Linked Materials") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            Calculate_Material_Total(view);
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Linked Material(s) deleted.\n\nImportant Note: Deleted Linked Materials are only physically removed from the work record on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
            }
        }

        private void FilterGrids()
        {
            string strActionID = "";
            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow != null)
            {
                strActionID = (currentRow["ActionID"] == null ? "0" : currentRow["ActionID"].ToString());
            }
            if (strActionID == "-1") return;

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[LinkedToRecordID] = " + Convert.ToString(strActionID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[ActionID] = " + Convert.ToString(strActionID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[ActionID] = " + Convert.ToString(strActionID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SkipPictureCountCheck = true;
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
            FilterGrids();  // Required just in case this is a save for the first time (ID of record will change) //
        }


        private void Calculate_Equipment_Line_Total(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            decimal decEstimatedCost = (decimal)0.00;
            decimal decActualCost = (decimal)0.00;
            decimal decEstimatedSell = (decimal)0.00;
            decimal decActualSell = (decimal)0.00;
            decEstimatedCost = (strCurrentColumn == "EstimatedHours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "EstimatedHours"))) *
                        (strCurrentColumn == "CostPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnit")));
            decActualCost = (strCurrentColumn == "ActualHours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "ActualHours"))) *
                        (strCurrentColumn == "CostPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnit")));        
            decEstimatedSell = (strCurrentColumn == "EstimatedHours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "EstimatedHours"))) *
                         (strCurrentColumn == "SellPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnit")));
            decActualSell = (strCurrentColumn == "ActualHours" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "ActualHours"))) *
                        (strCurrentColumn == "SellPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnit")));
            view.SetRowCellValue(intRow, "EstimatedCost", decEstimatedCost);
            view.SetRowCellValue(intRow, "ActualCost", decActualCost);
            view.SetRowCellValue(intRow, "EstimatedSell", decEstimatedSell);
            view.SetRowCellValue(intRow, "ActualSell", decActualSell);
            Calculate_Equipment_Total(view);
        }
        private void Calculate_Equipment_Total(GridView view)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            decimal decEstimatedTotalCost = (decimal)0.00;
            decimal decActualTotalCost = (decimal)0.00;
            decimal decEstimatedTotalSell = (decimal)0.00;
            decimal decActualTotalSell = (decimal)0.00;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                decEstimatedTotalCost += Convert.ToDecimal(view.GetRowCellValue(i, "EstimatedCost"));
                decActualTotalCost += Convert.ToDecimal(view.GetRowCellValue(i, "ActualCost"));
                decEstimatedTotalSell += Convert.ToDecimal(view.GetRowCellValue(i, "EstimatedSell"));
                decActualTotalSell += Convert.ToDecimal(view.GetRowCellValue(i, "ActualSell"));
            }
            currentRow["EstimatedEquipmentCost"] = decEstimatedTotalCost;
            currentRow["ActualEquipmentCost"] = decActualTotalCost;
            currentRow["EstimatedEquipmentSell"] = decEstimatedTotalSell;
            currentRow["ActualEquipmentSell"] = decActualTotalSell;
            sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
            Calculate_Total_Costs("", (decimal)0.00);
            Calculate_Total_Sell("", (decimal)0.00);
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void Calculate_Material_Line_Total(GridView view, int intRow, string strCurrentColumn, decimal decValue)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            decimal decEstimatedCost = (decimal)0.00;
            decimal decActualCost = (decimal)0.00;
            decimal decEstimatedSell = (decimal)0.00;
            decimal decActualSell = (decimal)0.00;
            decEstimatedCost = (strCurrentColumn == "EstimatedUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "EstimatedUnits"))) *
                        (strCurrentColumn == "CostPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnit")));
            decActualCost = (strCurrentColumn == "ActualUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "ActualUnits"))) *
                        (strCurrentColumn == "CostPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "CostPerUnit")));
            decEstimatedSell = (strCurrentColumn == "EstimatedUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "EstimatedUnits"))) *
                        (strCurrentColumn == "SellPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnit")));
            decActualSell = (strCurrentColumn == "ActualUnits" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "ActualUnits"))) *
                        (strCurrentColumn == "SellPerUnit" ? decValue : Convert.ToDecimal(view.GetRowCellValue(intRow, "SellPerUnit")));
            view.SetRowCellValue(intRow, "EstimatedCost", decEstimatedCost);
            view.SetRowCellValue(intRow, "ActualCost", decActualCost);
            view.SetRowCellValue(intRow, "EstimatedSell", decEstimatedSell);
            view.SetRowCellValue(intRow, "ActualSell", decActualSell);
            Calculate_Material_Total(view);
        }
        private void Calculate_Material_Total(GridView view)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            decimal decEstimatedTotalCost = (decimal)0.00;
            decimal decActualTotalCost = (decimal)0.00;
            decimal decEstimatedTotalSell = (decimal)0.00;
            decimal decActualTotalSell = (decimal)0.00;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                decEstimatedTotalCost += Convert.ToDecimal(view.GetRowCellValue(i, "EstimatedCost"));
                decActualTotalCost += Convert.ToDecimal(view.GetRowCellValue(i, "ActualCost"));
                decEstimatedTotalSell += Convert.ToDecimal(view.GetRowCellValue(i, "EstimatedSell"));
                decActualTotalSell += Convert.ToDecimal(view.GetRowCellValue(i, "ActualSell"));
            }
            currentRow["EstimatedMaterialsCost"] = decEstimatedTotalCost;
            currentRow["ActualMaterialsCost"] = decActualTotalCost;
            currentRow["EstimatedMaterialsSell"] = decEstimatedTotalSell;
            currentRow["ActualMaterialsSell"] = decActualTotalSell;
            sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
            Calculate_Total_Costs("", (decimal)0.00);
            Calculate_Total_Sell("", (decimal)0.00);
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void Calculate_Total_Costs(string strCurrentColumn, decimal decValue)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            decimal decEstimatedCost = (decimal)0.00;
            decimal decActualCost = (decimal)0.00;

            decEstimatedCost = (strCurrentColumn == "EstimatedLabourCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedLabourCost"])) +
                        (strCurrentColumn == "EstimatedMaterialsCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedMaterialsCost"])) +
                        (strCurrentColumn == "EstimatedEquipmentCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedEquipmentCost"]));
            decActualCost = (strCurrentColumn == "ActualLabourCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualLabourCost"])) +
                        (strCurrentColumn == "ActualMaterialsCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualMaterialsCost"])) +
                        (strCurrentColumn == "ActualEquipmentCostSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualEquipmentCost"]));
            currentRow["EstimatedTotalCost"] = decEstimatedCost;
            currentRow["ActualTotalCost"] = decActualCost;
                       
            sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }
        private void Calculate_Total_Sell(string strCurrentColumn, decimal decValue)
        {
            if (strFormMode == "view" || strFormMode == "blockedit") return;

            DataRowView currentRow = (DataRowView)sp07128UTSurveyedTreeWorkEditBindingSource.Current;
            if (currentRow == null) return;
            decimal decEstimatedSell = (decimal)0.00;
            decimal decActualSell = (decimal)0.00;

            decEstimatedSell = (strCurrentColumn == "EstimatedLabourSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedLabourSell"])) +
                        (strCurrentColumn == "EstimatedMaterialsSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedMaterialsSell"])) +
                        (strCurrentColumn == "EstimatedEquipmentSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["EstimatedEquipmentSell"]));
            decActualSell = (strCurrentColumn == "ActualLabourSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualLabourSell"])) +
                        (strCurrentColumn == "ActualMaterialsSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualMaterialsSell"])) +
                        (strCurrentColumn == "ActualEquipmentSellSpinEdit" ? decValue : Convert.ToDecimal(currentRow["ActualEquipmentSell"]));
            currentRow["EstimatedTotalSell"] = decEstimatedSell;
            currentRow["ActualTotalSell"] = decActualSell;

            sp07128UTSurveyedTreeWorkEditBindingSource.EndEdit();
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }



   



    }
}


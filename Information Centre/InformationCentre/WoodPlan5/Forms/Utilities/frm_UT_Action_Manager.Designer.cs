namespace WoodPlan5
{
    partial class frm_UT_Action_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Action_Manager));
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.popupContainerControlClients = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07001UTClientFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnClientFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07338UTActionManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCost = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualLabourCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualTotalSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualLabourSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualEquipmentSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualMaterialsSell = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAchievableClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMeters = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPossibleFlail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.popupContainerControlCircuits = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnCircuitFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlFeeders = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnFeederFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlPrimarys = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnPrimaryFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlSubAreas = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnSubAreaFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlRegions = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07036UTRegionPopupFilteredByClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRegionID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnRegionFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp07001_UT_Client_FilterTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerEdit1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit5 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.popupContainerEdit6 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefreshPoles = new DevExpress.XtraBars.BarButtonItem();
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter();
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter();
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter();
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter();
            this.sp07338_UT_Action_ManagerTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07338_UT_Action_ManagerTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).BeginInit();
            this.popupContainerControlClients.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07338UTActionManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).BeginInit();
            this.popupContainerControlCircuits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).BeginInit();
            this.popupContainerControlFeeders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).BeginInit();
            this.popupContainerControlPrimarys.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).BeginInit();
            this.popupContainerControlSubAreas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).BeginInit();
            this.popupContainerControlRegions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1051, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1051, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1051, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.popupContainerEdit1,
            this.popupContainerEdit2,
            this.popupContainerEdit3,
            this.popupContainerEdit4,
            this.popupContainerEdit5,
            this.popupContainerEdit6,
            this.bbiRefreshPoles});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit1,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemPopupContainerEdit3,
            this.repositoryItemPopupContainerEdit4,
            this.repositoryItemPopupContainerEdit5,
            this.repositoryItemTextEdit1,
            this.repositoryItemPopupContainerEdit6});
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // popupContainerControlClients
            // 
            this.popupContainerControlClients.Controls.Add(this.gridControl2);
            this.popupContainerControlClients.Controls.Add(this.btnClientFilterOK);
            this.popupContainerControlClients.Location = new System.Drawing.Point(12, 171);
            this.popupContainerControlClients.Name = "popupContainerControlClients";
            this.popupContainerControlClients.Size = new System.Drawing.Size(119, 152);
            this.popupContainerControlClients.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07001UTClientFilterBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 1);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(113, 121);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07001UTClientFilterBindingSource
            // 
            this.sp07001UTClientFilterBindingSource.DataMember = "sp07001_UT_Client_Filter";
            this.sp07001UTClientFilterBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 209;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 1;
            this.colClientTypeDescription.Width = 111;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 102;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // btnClientFilterOK
            // 
            this.btnClientFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClientFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnClientFilterOK.Name = "btnClientFilterOK";
            this.btnClientFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnClientFilterOK.TabIndex = 2;
            this.btnClientFilterOK.Text = "OK";
            this.btnClientFilterOK.Click += new System.EventHandler(this.btnClientFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07338UTActionManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 34);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemTextEditMeters,
            this.repositoryItemTextEditCost,
            this.repositoryItemTextEditNumericHours,
            this.repositoryItemTextEditShortDate});
            this.gridControl1.Size = new System.Drawing.Size(1051, 508);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07338UTActionManagerBindingSource
            // 
            this.sp07338UTActionManagerBindingSource.DataMember = "sp07338_UT_Action_Manager";
            this.sp07338UTActionManagerBindingSource.DataSource = this.dataSet_UT;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber1,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colEstimatedLabourCost,
            this.colActualLabourCost,
            this.colEstimatedMaterialsCost,
            this.colActualMaterialsCost,
            this.colEstimatedEquipmentCost,
            this.colActualEquipmentCost,
            this.colEstimatedTotalCost,
            this.colActualTotalCost,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks1,
            this.colGUID3,
            this.colJobDescription,
            this.colEstimatedTotalSell,
            this.colActualTotalSell,
            this.colEstimatedLabourSell,
            this.colActualLabourSell,
            this.colEstimatedEquipmentSell,
            this.colActualEquipmentSell,
            this.colEstimatedMaterialsSell,
            this.colActualMaterialsSell,
            this.colAchievableClearance,
            this.colPossibleFlail,
            this.colClientName1,
            this.colRegionName,
            this.colRegionNumber1,
            this.colSubAreaName,
            this.colSubAreaNumber1,
            this.colPrimaryName,
            this.colPrimaryNumber1,
            this.colFeederName,
            this.colFeederNumber1,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colPoleNumber,
            this.colTreeReference,
            this.colVoltage,
            this.colActionStatus,
            this.colActionStatusID,
            this.colRevisitDate,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementWhips,
            this.colTreeReplacementStandards,
            this.colActualHours,
            this.colEstimatedHours,
            this.colPossibleLiveWork,
            this.colCreatedByStaffID,
            this.colTeamOnHold});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTreeReference, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber1
            // 
            this.colReferenceNumber1.Caption = "Job Reference #";
            this.colReferenceNumber1.FieldName = "ReferenceNumber";
            this.colReferenceNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber1.Name = "colReferenceNumber1";
            this.colReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceNumber1.Visible = true;
            this.colReferenceNumber1.VisibleIndex = 1;
            this.colReferenceNumber1.Width = 102;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 9;
            this.colDateRaised.Width = 96;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "g";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 10;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 11;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 14;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colEstimatedLabourCost
            // 
            this.colEstimatedLabourCost.Caption = "Est. Labour Cost";
            this.colEstimatedLabourCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedLabourCost.FieldName = "EstimatedLabourCost";
            this.colEstimatedLabourCost.Name = "colEstimatedLabourCost";
            this.colEstimatedLabourCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourCost.Visible = true;
            this.colEstimatedLabourCost.VisibleIndex = 24;
            this.colEstimatedLabourCost.Width = 101;
            // 
            // repositoryItemTextEditCost
            // 
            this.repositoryItemTextEditCost.AutoHeight = false;
            this.repositoryItemTextEditCost.Mask.EditMask = "c";
            this.repositoryItemTextEditCost.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCost.Name = "repositoryItemTextEditCost";
            // 
            // colActualLabourCost
            // 
            this.colActualLabourCost.Caption = "Act. Labour Cost";
            this.colActualLabourCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualLabourCost.FieldName = "ActualLabourCost";
            this.colActualLabourCost.Name = "colActualLabourCost";
            this.colActualLabourCost.OptionsColumn.AllowEdit = false;
            this.colActualLabourCost.OptionsColumn.AllowFocus = false;
            this.colActualLabourCost.OptionsColumn.ReadOnly = true;
            this.colActualLabourCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourCost.Visible = true;
            this.colActualLabourCost.VisibleIndex = 25;
            this.colActualLabourCost.Width = 102;
            // 
            // colEstimatedMaterialsCost
            // 
            this.colEstimatedMaterialsCost.Caption = "Est. Material Cost";
            this.colEstimatedMaterialsCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedMaterialsCost.FieldName = "EstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.Name = "colEstimatedMaterialsCost";
            this.colEstimatedMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsCost.Visible = true;
            this.colEstimatedMaterialsCost.VisibleIndex = 32;
            this.colEstimatedMaterialsCost.Width = 106;
            // 
            // colActualMaterialsCost
            // 
            this.colActualMaterialsCost.Caption = "Act. Material Cost";
            this.colActualMaterialsCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualMaterialsCost.FieldName = "ActualMaterialsCost";
            this.colActualMaterialsCost.Name = "colActualMaterialsCost";
            this.colActualMaterialsCost.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsCost.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsCost.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsCost.Visible = true;
            this.colActualMaterialsCost.VisibleIndex = 33;
            this.colActualMaterialsCost.Width = 107;
            // 
            // colEstimatedEquipmentCost
            // 
            this.colEstimatedEquipmentCost.Caption = "Est. Equip. Cost";
            this.colEstimatedEquipmentCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedEquipmentCost.FieldName = "EstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.Name = "colEstimatedEquipmentCost";
            this.colEstimatedEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentCost.Visible = true;
            this.colEstimatedEquipmentCost.VisibleIndex = 28;
            this.colEstimatedEquipmentCost.Width = 98;
            // 
            // colActualEquipmentCost
            // 
            this.colActualEquipmentCost.Caption = "Act. Equip. Cost";
            this.colActualEquipmentCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualEquipmentCost.FieldName = "ActualEquipmentCost";
            this.colActualEquipmentCost.Name = "colActualEquipmentCost";
            this.colActualEquipmentCost.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentCost.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentCost.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentCost.Visible = true;
            this.colActualEquipmentCost.VisibleIndex = 29;
            this.colActualEquipmentCost.Width = 99;
            // 
            // colEstimatedTotalCost
            // 
            this.colEstimatedTotalCost.Caption = "Est. Total Cost";
            this.colEstimatedTotalCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedTotalCost.FieldName = "EstimatedTotalCost";
            this.colEstimatedTotalCost.Name = "colEstimatedTotalCost";
            this.colEstimatedTotalCost.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalCost.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalCost.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalCost.Visible = true;
            this.colEstimatedTotalCost.VisibleIndex = 20;
            this.colEstimatedTotalCost.Width = 92;
            // 
            // colActualTotalCost
            // 
            this.colActualTotalCost.Caption = "Act. Total Cost";
            this.colActualTotalCost.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualTotalCost.FieldName = "ActualTotalCost";
            this.colActualTotalCost.Name = "colActualTotalCost";
            this.colActualTotalCost.OptionsColumn.AllowEdit = false;
            this.colActualTotalCost.OptionsColumn.AllowFocus = false;
            this.colActualTotalCost.OptionsColumn.ReadOnly = true;
            this.colActualTotalCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalCost.Visible = true;
            this.colActualTotalCost.VisibleIndex = 21;
            this.colActualTotalCost.Width = 93;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkOrderID.Visible = true;
            this.colWorkOrderID.VisibleIndex = 37;
            this.colWorkOrderID.Width = 91;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Mask.EditMask = "0000000";
            this.repositoryItemHyperLinkEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemHyperLinkEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSelfBillingInvoiceID.Visible = true;
            this.colSelfBillingInvoiceID.VisibleIndex = 38;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFinanceSystemBillingID.Visible = true;
            this.colFinanceSystemBillingID.VisibleIndex = 39;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 36;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // colJobDescription
            // 
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 0;
            this.colJobDescription.Width = 141;
            // 
            // colEstimatedTotalSell
            // 
            this.colEstimatedTotalSell.Caption = "Est. Total Sell";
            this.colEstimatedTotalSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedTotalSell.FieldName = "EstimatedTotalSell";
            this.colEstimatedTotalSell.Name = "colEstimatedTotalSell";
            this.colEstimatedTotalSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedTotalSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedTotalSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedTotalSell.Visible = true;
            this.colEstimatedTotalSell.VisibleIndex = 22;
            this.colEstimatedTotalSell.Width = 86;
            // 
            // colActualTotalSell
            // 
            this.colActualTotalSell.Caption = "Act. Total Sell";
            this.colActualTotalSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualTotalSell.FieldName = "ActualTotalSell";
            this.colActualTotalSell.Name = "colActualTotalSell";
            this.colActualTotalSell.OptionsColumn.AllowEdit = false;
            this.colActualTotalSell.OptionsColumn.AllowFocus = false;
            this.colActualTotalSell.OptionsColumn.ReadOnly = true;
            this.colActualTotalSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualTotalSell.Visible = true;
            this.colActualTotalSell.VisibleIndex = 23;
            this.colActualTotalSell.Width = 87;
            // 
            // colEstimatedLabourSell
            // 
            this.colEstimatedLabourSell.Caption = "Est. Labour Sell";
            this.colEstimatedLabourSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedLabourSell.FieldName = "EstimatedLabourSell";
            this.colEstimatedLabourSell.Name = "colEstimatedLabourSell";
            this.colEstimatedLabourSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedLabourSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedLabourSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedLabourSell.Visible = true;
            this.colEstimatedLabourSell.VisibleIndex = 26;
            this.colEstimatedLabourSell.Width = 95;
            // 
            // colActualLabourSell
            // 
            this.colActualLabourSell.Caption = "Act. Labour Sell";
            this.colActualLabourSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualLabourSell.FieldName = "ActualLabourSell";
            this.colActualLabourSell.Name = "colActualLabourSell";
            this.colActualLabourSell.OptionsColumn.AllowEdit = false;
            this.colActualLabourSell.OptionsColumn.AllowFocus = false;
            this.colActualLabourSell.OptionsColumn.ReadOnly = true;
            this.colActualLabourSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualLabourSell.Visible = true;
            this.colActualLabourSell.VisibleIndex = 27;
            this.colActualLabourSell.Width = 96;
            // 
            // colEstimatedEquipmentSell
            // 
            this.colEstimatedEquipmentSell.Caption = "Est. Equip. Sell";
            this.colEstimatedEquipmentSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedEquipmentSell.FieldName = "EstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.Name = "colEstimatedEquipmentSell";
            this.colEstimatedEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedEquipmentSell.Visible = true;
            this.colEstimatedEquipmentSell.VisibleIndex = 30;
            this.colEstimatedEquipmentSell.Width = 92;
            // 
            // colActualEquipmentSell
            // 
            this.colActualEquipmentSell.Caption = "Act. Equip. Sell";
            this.colActualEquipmentSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualEquipmentSell.FieldName = "ActualEquipmentSell";
            this.colActualEquipmentSell.Name = "colActualEquipmentSell";
            this.colActualEquipmentSell.OptionsColumn.AllowEdit = false;
            this.colActualEquipmentSell.OptionsColumn.AllowFocus = false;
            this.colActualEquipmentSell.OptionsColumn.ReadOnly = true;
            this.colActualEquipmentSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualEquipmentSell.Visible = true;
            this.colActualEquipmentSell.VisibleIndex = 31;
            this.colActualEquipmentSell.Width = 93;
            // 
            // colEstimatedMaterialsSell
            // 
            this.colEstimatedMaterialsSell.Caption = "Est. Material Sell";
            this.colEstimatedMaterialsSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colEstimatedMaterialsSell.FieldName = "EstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.Name = "colEstimatedMaterialsSell";
            this.colEstimatedMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colEstimatedMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colEstimatedMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colEstimatedMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedMaterialsSell.Visible = true;
            this.colEstimatedMaterialsSell.VisibleIndex = 34;
            this.colEstimatedMaterialsSell.Width = 100;
            // 
            // colActualMaterialsSell
            // 
            this.colActualMaterialsSell.Caption = "Act. Material Sell";
            this.colActualMaterialsSell.ColumnEdit = this.repositoryItemTextEditCost;
            this.colActualMaterialsSell.FieldName = "ActualMaterialsSell";
            this.colActualMaterialsSell.Name = "colActualMaterialsSell";
            this.colActualMaterialsSell.OptionsColumn.AllowEdit = false;
            this.colActualMaterialsSell.OptionsColumn.AllowFocus = false;
            this.colActualMaterialsSell.OptionsColumn.ReadOnly = true;
            this.colActualMaterialsSell.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualMaterialsSell.Visible = true;
            this.colActualMaterialsSell.VisibleIndex = 35;
            this.colActualMaterialsSell.Width = 101;
            // 
            // colAchievableClearance
            // 
            this.colAchievableClearance.Caption = "Achievable Clearance";
            this.colAchievableClearance.ColumnEdit = this.repositoryItemTextEditMeters;
            this.colAchievableClearance.FieldName = "AchievableClearance";
            this.colAchievableClearance.Name = "colAchievableClearance";
            this.colAchievableClearance.OptionsColumn.AllowEdit = false;
            this.colAchievableClearance.OptionsColumn.AllowFocus = false;
            this.colAchievableClearance.OptionsColumn.ReadOnly = true;
            this.colAchievableClearance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAchievableClearance.Visible = true;
            this.colAchievableClearance.VisibleIndex = 16;
            this.colAchievableClearance.Width = 124;
            // 
            // repositoryItemTextEditMeters
            // 
            this.repositoryItemTextEditMeters.AutoHeight = false;
            this.repositoryItemTextEditMeters.Mask.EditMask = "######0.00 M";
            this.repositoryItemTextEditMeters.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMeters.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMeters.Name = "repositoryItemTextEditMeters";
            // 
            // colPossibleFlail
            // 
            this.colPossibleFlail.Caption = "Possible Flail";
            this.colPossibleFlail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleFlail.FieldName = "PossibleFlail";
            this.colPossibleFlail.Name = "colPossibleFlail";
            this.colPossibleFlail.OptionsColumn.AllowEdit = false;
            this.colPossibleFlail.OptionsColumn.AllowFocus = false;
            this.colPossibleFlail.OptionsColumn.ReadOnly = true;
            this.colPossibleFlail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPossibleFlail.Visible = true;
            this.colPossibleFlail.VisibleIndex = 15;
            this.colPossibleFlail.Width = 80;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 38;
            this.colClientName1.Width = 128;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 2;
            this.colRegionName.Width = 110;
            // 
            // colRegionNumber1
            // 
            this.colRegionNumber1.Caption = "Region Number";
            this.colRegionNumber1.FieldName = "RegionNumber";
            this.colRegionNumber1.Name = "colRegionNumber1";
            this.colRegionNumber1.OptionsColumn.AllowEdit = false;
            this.colRegionNumber1.OptionsColumn.AllowFocus = false;
            this.colRegionNumber1.OptionsColumn.ReadOnly = true;
            this.colRegionNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionNumber1.Width = 94;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 3;
            this.colSubAreaName.Width = 109;
            // 
            // colSubAreaNumber1
            // 
            this.colSubAreaNumber1.Caption = "Sub-Area Number";
            this.colSubAreaNumber1.FieldName = "SubAreaNumber";
            this.colSubAreaNumber1.Name = "colSubAreaNumber1";
            this.colSubAreaNumber1.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber1.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber1.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaNumber1.Width = 106;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 4;
            this.colPrimaryName.Width = 101;
            // 
            // colPrimaryNumber1
            // 
            this.colPrimaryNumber1.Caption = "Primary Number";
            this.colPrimaryNumber1.FieldName = "PrimaryNumber";
            this.colPrimaryNumber1.Name = "colPrimaryNumber1";
            this.colPrimaryNumber1.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber1.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber1.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryNumber1.Width = 97;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 5;
            this.colFeederName.Width = 93;
            // 
            // colFeederNumber1
            // 
            this.colFeederNumber1.Caption = "Feeder Number";
            this.colFeederNumber1.FieldName = "FeederNumber";
            this.colFeederNumber1.Name = "colFeederNumber1";
            this.colFeederNumber1.OptionsColumn.AllowEdit = false;
            this.colFeederNumber1.OptionsColumn.AllowFocus = false;
            this.colFeederNumber1.OptionsColumn.ReadOnly = true;
            this.colFeederNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederNumber1.Width = 95;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 6;
            this.colCircuitName.Width = 117;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Width = 91;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 7;
            this.colPoleNumber.Width = 94;
            // 
            // colTreeReference
            // 
            this.colTreeReference.Caption = "Tree Reference";
            this.colTreeReference.FieldName = "TreeReference";
            this.colTreeReference.Name = "colTreeReference";
            this.colTreeReference.OptionsColumn.AllowEdit = false;
            this.colTreeReference.OptionsColumn.AllowFocus = false;
            this.colTreeReference.OptionsColumn.ReadOnly = true;
            this.colTreeReference.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReference.Visible = true;
            this.colTreeReference.VisibleIndex = 8;
            this.colTreeReference.Width = 109;
            // 
            // colVoltage
            // 
            this.colVoltage.Caption = "Voltage";
            this.colVoltage.FieldName = "Voltage";
            this.colVoltage.Name = "colVoltage";
            this.colVoltage.OptionsColumn.AllowEdit = false;
            this.colVoltage.OptionsColumn.AllowFocus = false;
            this.colVoltage.OptionsColumn.ReadOnly = true;
            this.colVoltage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colActionStatus
            // 
            this.colActionStatus.Caption = "Action Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 12;
            this.colActionStatus.Width = 85;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.Caption = "Action Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            this.colActionStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActionStatusID.Width = 99;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 40;
            this.colRevisitDate.Width = 79;
            // 
            // repositoryItemTextEditShortDate
            // 
            this.repositoryItemTextEditShortDate.AutoHeight = false;
            this.repositoryItemTextEditShortDate.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate.Name = "repositoryItemTextEditShortDate";
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 41;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 42;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 43;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 44;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 45;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 46;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 47;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditNumericHours;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 19;
            this.colActualHours.Width = 82;
            // 
            // repositoryItemTextEditNumericHours
            // 
            this.repositoryItemTextEditNumericHours.AutoHeight = false;
            this.repositoryItemTextEditNumericHours.Mask.EditMask = "######0.00 Hours";
            this.repositoryItemTextEditNumericHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericHours.Name = "repositoryItemTextEditNumericHours";
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditNumericHours;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 18;
            this.colEstimatedHours.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 17;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.OptionsColumn.AllowEdit = false;
            this.colTeamOnHold.OptionsColumn.AllowFocus = false;
            this.colTeamOnHold.OptionsColumn.ReadOnly = true;
            this.colTeamOnHold.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTeamOnHold.Visible = true;
            this.colTeamOnHold.VisibleIndex = 13;
            this.colTeamOnHold.Width = 87;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // popupContainerControlCircuits
            // 
            this.popupContainerControlCircuits.Controls.Add(this.gridControl8);
            this.popupContainerControlCircuits.Controls.Add(this.btnCircuitFilterOK);
            this.popupContainerControlCircuits.Location = new System.Drawing.Point(636, 170);
            this.popupContainerControlCircuits.Name = "popupContainerControlCircuits";
            this.popupContainerControlCircuits.Size = new System.Drawing.Size(118, 152);
            this.popupContainerControlCircuits.TabIndex = 6;
            // 
            // gridControl8
            // 
            this.gridControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl8.DataSource = this.sp07040UTCircuitPopupFilteredByVariousBindingSource;
            this.gridControl8.Location = new System.Drawing.Point(3, 1);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.Size = new System.Drawing.Size(112, 121);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp07040UTCircuitPopupFilteredByVariousBindingSource
            // 
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataMember = "sp07040_UT_Circuit_Popup_Filtered_By_Various";
            this.sp07040UTCircuitPopupFilteredByVariousBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 1;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsFind.AlwaysVisible = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn45, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn49, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView8_CustomDrawEmptyForeground);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Circuit ID";
            this.gridColumn43.FieldName = "CircuitID";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Client ID";
            this.gridColumn44.FieldName = "ClientID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Client Name";
            this.gridColumn45.FieldName = "ClientName";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 0;
            this.gridColumn45.Width = 240;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Client Code";
            this.gridColumn46.FieldName = "ClientCode";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn46.Width = 76;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Status ID";
            this.gridColumn47.FieldName = "StatusID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Circuit Name";
            this.gridColumn48.FieldName = "CircuitName";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 0;
            this.gridColumn48.Width = 212;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Circuit Number";
            this.gridColumn49.FieldName = "CircuitNumber";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 1;
            this.gridColumn49.Width = 144;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Voltage ID";
            this.gridColumn50.FieldName = "VoltageID";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Voltage Type";
            this.gridColumn51.FieldName = "VoltageType";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 2;
            this.gridColumn51.Width = 127;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Feeder";
            this.gridColumn52.FieldName = "FeederName";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.AllowEdit = false;
            this.gridColumn52.OptionsColumn.AllowFocus = false;
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 6;
            this.gridColumn52.Width = 136;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Coordinate Pairs";
            this.gridColumn53.FieldName = "CoordinatePairs";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Width = 203;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Lat\\Long Pairs";
            this.gridColumn54.FieldName = "LatLongPairs";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.Width = 200;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "Remarks";
            this.gridColumn55.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.gridColumn55.FieldName = "Remarks";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.OptionsColumn.ReadOnly = true;
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 8;
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "Status";
            this.gridColumn57.FieldName = "Status";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.OptionsColumn.AllowEdit = false;
            this.gridColumn57.OptionsColumn.AllowFocus = false;
            this.gridColumn57.OptionsColumn.ReadOnly = true;
            this.gridColumn57.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 7;
            this.gridColumn57.Width = 62;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "Primary";
            this.gridColumn58.FieldName = "PrimaryName";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.OptionsColumn.AllowEdit = false;
            this.gridColumn58.OptionsColumn.AllowFocus = false;
            this.gridColumn58.OptionsColumn.ReadOnly = true;
            this.gridColumn58.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 5;
            this.gridColumn58.Width = 129;
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "Region";
            this.gridColumn59.FieldName = "RegionName";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsColumn.AllowEdit = false;
            this.gridColumn59.OptionsColumn.AllowFocus = false;
            this.gridColumn59.OptionsColumn.ReadOnly = true;
            this.gridColumn59.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 3;
            this.gridColumn59.Width = 122;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "Sub Area";
            this.gridColumn60.FieldName = "SubAreaName";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.OptionsColumn.AllowEdit = false;
            this.gridColumn60.OptionsColumn.AllowFocus = false;
            this.gridColumn60.OptionsColumn.ReadOnly = true;
            this.gridColumn60.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 4;
            this.gridColumn60.Width = 124;
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "Feeder ID";
            this.gridColumn61.FieldName = "FeederID";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.OptionsColumn.AllowEdit = false;
            this.gridColumn61.OptionsColumn.AllowFocus = false;
            this.gridColumn61.OptionsColumn.ReadOnly = true;
            this.gridColumn61.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Primary ID";
            this.gridColumn62.FieldName = "PrimaryID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            this.gridColumn62.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn63
            // 
            this.gridColumn63.Caption = "Region ID";
            this.gridColumn63.FieldName = "RegionID";
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.AllowEdit = false;
            this.gridColumn63.OptionsColumn.AllowFocus = false;
            this.gridColumn63.OptionsColumn.ReadOnly = true;
            this.gridColumn63.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Sub-Area ID";
            this.gridColumn64.FieldName = "SubAreaID";
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.OptionsColumn.AllowEdit = false;
            this.gridColumn64.OptionsColumn.AllowFocus = false;
            this.gridColumn64.OptionsColumn.ReadOnly = true;
            this.gridColumn64.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // btnCircuitFilterOK
            // 
            this.btnCircuitFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCircuitFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnCircuitFilterOK.Name = "btnCircuitFilterOK";
            this.btnCircuitFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnCircuitFilterOK.TabIndex = 2;
            this.btnCircuitFilterOK.Text = "OK";
            this.btnCircuitFilterOK.Click += new System.EventHandler(this.btnCircuitFilterOK_Click);
            // 
            // popupContainerControlFeeders
            // 
            this.popupContainerControlFeeders.Controls.Add(this.gridControl7);
            this.popupContainerControlFeeders.Controls.Add(this.btnFeederFilterOK);
            this.popupContainerControlFeeders.Location = new System.Drawing.Point(513, 171);
            this.popupContainerControlFeeders.Name = "popupContainerControlFeeders";
            this.popupContainerControlFeeders.Size = new System.Drawing.Size(117, 149);
            this.popupContainerControlFeeders.TabIndex = 5;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp07039UTFeederPopupFilteredByPrimaryBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 1);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7});
            this.gridControl7.Size = new System.Drawing.Size(111, 118);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07039UTFeederPopupFilteredByPrimaryBindingSource
            // 
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataMember = "sp07039_UT_Feeder_Popup_Filtered_By_Primary";
            this.sp07039UTFeederPopupFilteredByPrimaryBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.colFeederID1,
            this.colFeederName1,
            this.colFeederNumber});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 4;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn29, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn35, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn38, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn41, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView7_CustomDrawEmptyForeground);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Client ID";
            this.gridColumn28.FieldName = "ClientID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Client Name";
            this.gridColumn29.FieldName = "ClientName";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 0;
            this.gridColumn29.Width = 209;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Client Code";
            this.gridColumn30.FieldName = "ClientCode";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Client Type ID";
            this.gridColumn31.FieldName = "ClientTypeID";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Client Type";
            this.gridColumn32.FieldName = "ClientTypeDescription";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn32.Width = 111;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Remarks";
            this.gridColumn33.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn33.FieldName = "Remarks";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 2;
            this.gridColumn33.Width = 102;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Region ID";
            this.gridColumn34.FieldName = "RegionID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Region Name";
            this.gridColumn35.FieldName = "RegionName";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            this.gridColumn35.Width = 206;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Region Number";
            this.gridColumn36.FieldName = "RegionNumber";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn36.Width = 161;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Sub-Area ID";
            this.gridColumn37.FieldName = "SubAreaID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn37.Width = 80;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Sub-Area Name";
            this.gridColumn38.FieldName = "SubAreaName";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            this.gridColumn38.Width = 199;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Sub-Area Number ";
            this.gridColumn39.FieldName = "SubAreaNumber";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn39.Width = 121;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Primary ID";
            this.gridColumn40.FieldName = "PrimaryID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Primary Name";
            this.gridColumn41.FieldName = "PrimaryName";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 0;
            this.gridColumn41.Width = 196;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Primary Number";
            this.gridColumn42.FieldName = "PrimaryNumber";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn42.Width = 117;
            // 
            // colFeederID1
            // 
            this.colFeederID1.Caption = "Feeder ID";
            this.colFeederID1.FieldName = "FeederID";
            this.colFeederID1.Name = "colFeederID1";
            this.colFeederID1.OptionsColumn.AllowEdit = false;
            this.colFeederID1.OptionsColumn.AllowFocus = false;
            this.colFeederID1.OptionsColumn.ReadOnly = true;
            this.colFeederID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFeederName1
            // 
            this.colFeederName1.Caption = "Feeder Name";
            this.colFeederName1.FieldName = "FeederName";
            this.colFeederName1.Name = "colFeederName1";
            this.colFeederName1.OptionsColumn.AllowEdit = false;
            this.colFeederName1.OptionsColumn.AllowFocus = false;
            this.colFeederName1.OptionsColumn.ReadOnly = true;
            this.colFeederName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName1.Visible = true;
            this.colFeederName1.VisibleIndex = 0;
            this.colFeederName1.Width = 205;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder Number";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 1;
            this.colFeederNumber.Width = 123;
            // 
            // btnFeederFilterOK
            // 
            this.btnFeederFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFeederFilterOK.Location = new System.Drawing.Point(3, 123);
            this.btnFeederFilterOK.Name = "btnFeederFilterOK";
            this.btnFeederFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnFeederFilterOK.TabIndex = 2;
            this.btnFeederFilterOK.Text = "OK";
            this.btnFeederFilterOK.Click += new System.EventHandler(this.btnFeederFilterOK_Click);
            // 
            // popupContainerControlPrimarys
            // 
            this.popupContainerControlPrimarys.Controls.Add(this.gridControl6);
            this.popupContainerControlPrimarys.Controls.Add(this.btnPrimaryFilterOK);
            this.popupContainerControlPrimarys.Location = new System.Drawing.Point(388, 171);
            this.popupContainerControlPrimarys.Name = "popupContainerControlPrimarys";
            this.popupContainerControlPrimarys.Size = new System.Drawing.Size(119, 150);
            this.popupContainerControlPrimarys.TabIndex = 4;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
            this.gridControl6.Location = new System.Drawing.Point(3, 1);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.gridControl6.Size = new System.Drawing.Size(113, 119);
            this.gridControl6.TabIndex = 3;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07038UTPrimaryPopupFilteredBySubAreaBindingSource
            // 
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataMember = "sp07038_UT_Primary_Popup_Filtered_By_SubArea";
            this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.colPrimaryID1,
            this.colPrimaryName1,
            this.colPrimaryNumber});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 3;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn17, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn26, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView6_CustomDrawEmptyForeground);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Client ID";
            this.gridColumn16.FieldName = "ClientID";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Client Name";
            this.gridColumn17.FieldName = "ClientName";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 0;
            this.gridColumn17.Width = 209;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Client Code";
            this.gridColumn18.FieldName = "ClientCode";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Type ID";
            this.gridColumn19.FieldName = "ClientTypeID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Client Type";
            this.gridColumn20.FieldName = "ClientTypeDescription";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn20.Width = 111;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Remarks";
            this.gridColumn21.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.gridColumn21.FieldName = "Remarks";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 2;
            this.gridColumn21.Width = 102;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Region ID";
            this.gridColumn22.FieldName = "RegionID";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Region Name";
            this.gridColumn23.FieldName = "RegionName";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 0;
            this.gridColumn23.Width = 206;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Region Number";
            this.gridColumn24.FieldName = "RegionNumber";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn24.Width = 161;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Sub-Area ID";
            this.gridColumn25.FieldName = "SubAreaID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn25.Width = 80;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Sub-Area Name";
            this.gridColumn26.FieldName = "SubAreaName";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            this.gridColumn26.Width = 199;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Sub-Area Number ";
            this.gridColumn27.FieldName = "SubAreaNumber";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn27.Width = 121;
            // 
            // colPrimaryID1
            // 
            this.colPrimaryID1.Caption = "Primary ID";
            this.colPrimaryID1.FieldName = "PrimaryID";
            this.colPrimaryID1.Name = "colPrimaryID1";
            this.colPrimaryID1.OptionsColumn.AllowEdit = false;
            this.colPrimaryID1.OptionsColumn.AllowFocus = false;
            this.colPrimaryID1.OptionsColumn.ReadOnly = true;
            this.colPrimaryID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPrimaryName1
            // 
            this.colPrimaryName1.Caption = "Primary Name";
            this.colPrimaryName1.FieldName = "PrimaryName";
            this.colPrimaryName1.Name = "colPrimaryName1";
            this.colPrimaryName1.OptionsColumn.AllowEdit = false;
            this.colPrimaryName1.OptionsColumn.AllowFocus = false;
            this.colPrimaryName1.OptionsColumn.ReadOnly = true;
            this.colPrimaryName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName1.Visible = true;
            this.colPrimaryName1.VisibleIndex = 0;
            this.colPrimaryName1.Width = 196;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary Number";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 1;
            this.colPrimaryNumber.Width = 117;
            // 
            // btnPrimaryFilterOK
            // 
            this.btnPrimaryFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPrimaryFilterOK.Location = new System.Drawing.Point(3, 124);
            this.btnPrimaryFilterOK.Name = "btnPrimaryFilterOK";
            this.btnPrimaryFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnPrimaryFilterOK.TabIndex = 2;
            this.btnPrimaryFilterOK.Text = "OK";
            this.btnPrimaryFilterOK.Click += new System.EventHandler(this.btnPrimaryFilterOK_Click);
            // 
            // popupContainerControlSubAreas
            // 
            this.popupContainerControlSubAreas.Controls.Add(this.gridControl5);
            this.popupContainerControlSubAreas.Controls.Add(this.btnSubAreaFilterOK);
            this.popupContainerControlSubAreas.Location = new System.Drawing.Point(263, 171);
            this.popupContainerControlSubAreas.Name = "popupContainerControlSubAreas";
            this.popupContainerControlSubAreas.Size = new System.Drawing.Size(119, 151);
            this.popupContainerControlSubAreas.TabIndex = 3;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.sp07037UTSubAreaPopupFilteredByRegionBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 1);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5});
            this.gridControl5.Size = new System.Drawing.Size(113, 120);
            this.gridControl5.TabIndex = 3;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07037UTSubAreaPopupFilteredByRegionBindingSource
            // 
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataMember = "sp07037_UT_SubArea_Popup_Filtered_By_Region";
            this.sp07037UTSubAreaPopupFilteredByRegionBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colSubAreaID1,
            this.colSubAreaName1,
            this.colSubAreaNumber});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView5_CustomDrawEmptyForeground);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Client ID";
            this.gridColumn7.FieldName = "ClientID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Client Name";
            this.gridColumn8.FieldName = "ClientName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 209;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Client Code";
            this.gridColumn9.FieldName = "ClientCode";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Client Type ID";
            this.gridColumn10.FieldName = "ClientTypeID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Client Type";
            this.gridColumn11.FieldName = "ClientTypeDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Width = 111;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Remarks";
            this.gridColumn12.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn12.FieldName = "Remarks";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 102;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Region ID";
            this.gridColumn13.FieldName = "RegionID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Region Name";
            this.gridColumn14.FieldName = "RegionName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 206;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Region Number";
            this.gridColumn15.FieldName = "RegionNumber";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn15.Width = 161;
            // 
            // colSubAreaID1
            // 
            this.colSubAreaID1.Caption = "Sub-Area ID";
            this.colSubAreaID1.FieldName = "SubAreaID";
            this.colSubAreaID1.Name = "colSubAreaID1";
            this.colSubAreaID1.OptionsColumn.AllowEdit = false;
            this.colSubAreaID1.OptionsColumn.AllowFocus = false;
            this.colSubAreaID1.OptionsColumn.ReadOnly = true;
            this.colSubAreaID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaID1.Width = 80;
            // 
            // colSubAreaName1
            // 
            this.colSubAreaName1.Caption = "Sub-Area Name";
            this.colSubAreaName1.FieldName = "SubAreaName";
            this.colSubAreaName1.Name = "colSubAreaName1";
            this.colSubAreaName1.OptionsColumn.AllowEdit = false;
            this.colSubAreaName1.OptionsColumn.AllowFocus = false;
            this.colSubAreaName1.OptionsColumn.ReadOnly = true;
            this.colSubAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName1.Visible = true;
            this.colSubAreaName1.VisibleIndex = 0;
            this.colSubAreaName1.Width = 199;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area Number ";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 1;
            this.colSubAreaNumber.Width = 121;
            // 
            // btnSubAreaFilterOK
            // 
            this.btnSubAreaFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSubAreaFilterOK.Location = new System.Drawing.Point(3, 125);
            this.btnSubAreaFilterOK.Name = "btnSubAreaFilterOK";
            this.btnSubAreaFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnSubAreaFilterOK.TabIndex = 2;
            this.btnSubAreaFilterOK.Text = "OK";
            this.btnSubAreaFilterOK.Click += new System.EventHandler(this.btnSubAreaFilterOK_Click);
            // 
            // popupContainerControlRegions
            // 
            this.popupContainerControlRegions.Controls.Add(this.gridControl4);
            this.popupContainerControlRegions.Controls.Add(this.btnRegionFilterOK);
            this.popupContainerControlRegions.Location = new System.Drawing.Point(137, 171);
            this.popupContainerControlRegions.Name = "popupContainerControlRegions";
            this.popupContainerControlRegions.Size = new System.Drawing.Size(120, 152);
            this.popupContainerControlRegions.TabIndex = 2;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp07036UTRegionPopupFilteredByClientBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(3, 1);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(114, 121);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07036UTRegionPopupFilteredByClientBindingSource
            // 
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataMember = "sp07036_UT_Region_Popup_Filtered_By_Client";
            this.sp07036UTRegionPopupFilteredByClientBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colRegionID1,
            this.colRegionName1,
            this.colRegionNumber});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView4_CustomDrawEmptyForeground);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Name";
            this.gridColumn2.FieldName = "ClientName";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 209;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Client Code";
            this.gridColumn3.FieldName = "ClientCode";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Client Type ID";
            this.gridColumn4.FieldName = "ClientTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client Type";
            this.gridColumn5.FieldName = "ClientTypeDescription";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Width = 111;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Remarks";
            this.gridColumn6.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn6.FieldName = "Remarks";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            this.gridColumn6.Width = 102;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRegionID1
            // 
            this.colRegionID1.Caption = "Region ID";
            this.colRegionID1.FieldName = "RegionID";
            this.colRegionID1.Name = "colRegionID1";
            this.colRegionID1.OptionsColumn.AllowEdit = false;
            this.colRegionID1.OptionsColumn.AllowFocus = false;
            this.colRegionID1.OptionsColumn.ReadOnly = true;
            this.colRegionID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region Name";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 0;
            this.colRegionName1.Width = 206;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region Number";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 1;
            this.colRegionNumber.Width = 94;
            // 
            // btnRegionFilterOK
            // 
            this.btnRegionFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRegionFilterOK.Location = new System.Drawing.Point(3, 126);
            this.btnRegionFilterOK.Name = "btnRegionFilterOK";
            this.btnRegionFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnRegionFilterOK.TabIndex = 2;
            this.btnRegionFilterOK.Text = "OK";
            this.btnRegionFilterOK.Click += new System.EventHandler(this.btnRegionFilterOK_Click);
            // 
            // sp07001_UT_Client_FilterTableAdapter
            // 
            this.sp07001_UT_Client_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(559, 183);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit2),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit3),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit4),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit5),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit6, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshPoles)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerEdit1
            // 
            this.popupContainerEdit1.Caption = "Client Filter";
            this.popupContainerEdit1.Edit = this.repositoryItemPopupContainerEdit1;
            this.popupContainerEdit1.EditValue = "No Client Filter";
            this.popupContainerEdit1.EditWidth = 110;
            this.popupContainerEdit1.Id = 28;
            this.popupContainerEdit1.Name = "popupContainerEdit1";
            // 
            // repositoryItemPopupContainerEdit1
            // 
            this.repositoryItemPopupContainerEdit1.AutoHeight = false;
            this.repositoryItemPopupContainerEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit1.Name = "repositoryItemPopupContainerEdit1";
            this.repositoryItemPopupContainerEdit1.PopupControl = this.popupContainerControlClients;
            this.repositoryItemPopupContainerEdit1.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit1_QueryResultValue);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.Caption = "Region Filter";
            this.popupContainerEdit2.Edit = this.repositoryItemPopupContainerEdit2;
            this.popupContainerEdit2.EditValue = "No Region Filter";
            this.popupContainerEdit2.EditWidth = 110;
            this.popupContainerEdit2.Id = 29;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            this.repositoryItemPopupContainerEdit2.PopupControl = this.popupContainerControlRegions;
            this.repositoryItemPopupContainerEdit2.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit2_QueryResultValue);
            // 
            // popupContainerEdit3
            // 
            this.popupContainerEdit3.Caption = "Sub-Area Filter";
            this.popupContainerEdit3.Edit = this.repositoryItemPopupContainerEdit3;
            this.popupContainerEdit3.EditValue = "No Sub-Area Filter";
            this.popupContainerEdit3.EditWidth = 110;
            this.popupContainerEdit3.Id = 30;
            this.popupContainerEdit3.Name = "popupContainerEdit3";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControlSubAreas;
            this.repositoryItemPopupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit3_QueryResultValue);
            // 
            // popupContainerEdit4
            // 
            this.popupContainerEdit4.Caption = "Primary Filter";
            this.popupContainerEdit4.Edit = this.repositoryItemPopupContainerEdit4;
            this.popupContainerEdit4.EditValue = "No Primary Filter";
            this.popupContainerEdit4.EditWidth = 110;
            this.popupContainerEdit4.Id = 31;
            this.popupContainerEdit4.Name = "popupContainerEdit4";
            // 
            // repositoryItemPopupContainerEdit4
            // 
            this.repositoryItemPopupContainerEdit4.AutoHeight = false;
            this.repositoryItemPopupContainerEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit4.Name = "repositoryItemPopupContainerEdit4";
            this.repositoryItemPopupContainerEdit4.PopupControl = this.popupContainerControlPrimarys;
            this.repositoryItemPopupContainerEdit4.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit4_QueryResultValue);
            // 
            // popupContainerEdit5
            // 
            this.popupContainerEdit5.Caption = "Feeder Filter";
            this.popupContainerEdit5.Edit = this.repositoryItemPopupContainerEdit5;
            this.popupContainerEdit5.EditValue = "No Feeder Filter";
            this.popupContainerEdit5.EditWidth = 110;
            this.popupContainerEdit5.Id = 32;
            this.popupContainerEdit5.Name = "popupContainerEdit5";
            // 
            // repositoryItemPopupContainerEdit5
            // 
            this.repositoryItemPopupContainerEdit5.AutoHeight = false;
            this.repositoryItemPopupContainerEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit5.Name = "repositoryItemPopupContainerEdit5";
            this.repositoryItemPopupContainerEdit5.PopupControl = this.popupContainerControlFeeders;
            this.repositoryItemPopupContainerEdit5.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit5_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Circuits";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // popupContainerEdit6
            // 
            this.popupContainerEdit6.Caption = "Circuit Filter";
            this.popupContainerEdit6.Edit = this.repositoryItemPopupContainerEdit6;
            this.popupContainerEdit6.EditValue = "No Circuit Filter";
            this.popupContainerEdit6.EditWidth = 110;
            this.popupContainerEdit6.Id = 34;
            this.popupContainerEdit6.Name = "popupContainerEdit6";
            // 
            // repositoryItemPopupContainerEdit6
            // 
            this.repositoryItemPopupContainerEdit6.AutoHeight = false;
            this.repositoryItemPopupContainerEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit6.Name = "repositoryItemPopupContainerEdit6";
            this.repositoryItemPopupContainerEdit6.PopupControl = this.popupContainerControlCircuits;
            this.repositoryItemPopupContainerEdit6.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit6_QueryResultValue);
            // 
            // bbiRefreshPoles
            // 
            this.bbiRefreshPoles.Caption = "Refresh Actions";
            this.bbiRefreshPoles.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshPoles.Glyph")));
            this.bbiRefreshPoles.Id = 35;
            this.bbiRefreshPoles.Name = "bbiRefreshPoles";
            this.bbiRefreshPoles.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshPoles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshPoles_ItemClick);
            // 
            // sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter
            // 
            this.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter
            // 
            this.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.ClearBeforeFill = true;
            // 
            // sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter
            // 
            this.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.ClearBeforeFill = true;
            // 
            // sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter
            // 
            this.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter
            // 
            this.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.ClearBeforeFill = true;
            // 
            // sp07338_UT_Action_ManagerTableAdapter
            // 
            this.sp07338_UT_Action_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Action_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1051, 542);
            this.Controls.Add(this.popupContainerControlCircuits);
            this.Controls.Add(this.popupContainerControlFeeders);
            this.Controls.Add(this.popupContainerControlClients);
            this.Controls.Add(this.popupContainerControlPrimarys);
            this.Controls.Add(this.popupContainerControlRegions);
            this.Controls.Add(this.popupContainerControlSubAreas);
            this.Controls.Add(this.gridControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Action_Manager";
            this.Text = "Utilities - Action Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Action_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Action_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Action_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControlSubAreas, 0);
            this.Controls.SetChildIndex(this.popupContainerControlRegions, 0);
            this.Controls.SetChildIndex(this.popupContainerControlPrimarys, 0);
            this.Controls.SetChildIndex(this.popupContainerControlClients, 0);
            this.Controls.SetChildIndex(this.popupContainerControlFeeders, 0);
            this.Controls.SetChildIndex(this.popupContainerControlCircuits, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlClients)).EndInit();
            this.popupContainerControlClients.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07001UTClientFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07338UTActionManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlCircuits)).EndInit();
            this.popupContainerControlCircuits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07040UTCircuitPopupFilteredByVariousBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFeeders)).EndInit();
            this.popupContainerControlFeeders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07039UTFeederPopupFilteredByPrimaryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlPrimarys)).EndInit();
            this.popupContainerControlPrimarys.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07038UTPrimaryPopupFilteredBySubAreaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSubAreas)).EndInit();
            this.popupContainerControlSubAreas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07037UTSubAreaPopupFilteredByRegionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlRegions)).EndInit();
            this.popupContainerControlRegions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07036UTRegionPopupFilteredByClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlClients;
        private DevExpress.XtraEditors.SimpleButton btnClientFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07001UTClientFilterBindingSource;
        private DataSet_UTTableAdapters.sp07001_UT_Client_FilterTableAdapter sp07001_UT_Client_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit1;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit4;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit5;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlRegions;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnRegionFilterOK;
        private System.Windows.Forms.BindingSource sp07036UTRegionPopupFilteredByClientBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DataSet_UTTableAdapters.sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSubAreas;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraEditors.SimpleButton btnSubAreaFilterOK;
        private System.Windows.Forms.BindingSource sp07037UTSubAreaPopupFilteredByRegionBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DataSet_UTTableAdapters.sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlPrimarys;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraEditors.SimpleButton btnPrimaryFilterOK;
        private System.Windows.Forms.BindingSource sp07038UTPrimaryPopupFilteredBySubAreaBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DataSet_UTTableAdapters.sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlFeeders;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private System.Windows.Forms.BindingSource sp07039UTFeederPopupFilteredByPrimaryBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraEditors.SimpleButton btnFeederFilterOK;
        private DataSet_UTTableAdapters.sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshPoles;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlCircuits;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.SimpleButton btnCircuitFilterOK;
        private System.Windows.Forms.BindingSource sp07040UTCircuitPopupFilteredByVariousBindingSource;
        private DataSet_UTTableAdapters.sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMeters;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericHours;
        private System.Windows.Forms.BindingSource sp07338UTActionManagerBindingSource;
        private DataSet_UTTableAdapters.sp07338_UT_Action_ManagerTableAdapter sp07338_UT_Action_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentCost;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualTotalSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualLabourSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualEquipmentSell;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colActualMaterialsSell;
        private DevExpress.XtraGrid.Columns.GridColumn colAchievableClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleFlail;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReference;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltage;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
    }
}

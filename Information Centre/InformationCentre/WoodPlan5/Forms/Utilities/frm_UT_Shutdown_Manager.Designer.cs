namespace WoodPlan5
{
    partial class frm_UT_Shutdown_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Shutdown_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07372UTShutdownManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colShutdownID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEngineerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlSurveyors = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07329UTSurveyorListNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnSurveyorOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateMask = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditRevisitCount = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDoneDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDueDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsiblePerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerEdit2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter();
            this.sp07372_UT_Shutdown_ManagerTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07372_UT_Shutdown_ManagerTableAdapter();
            this.sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsiblePerson1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07372UTShutdownManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyors)).BeginInit();
            this.popupContainerControlSurveyors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditRevisitCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(933, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(933, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 508);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(933, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 508);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.barEditItem1,
            this.popupContainerDateRange,
            this.popupContainerEdit2});
            this.barManager1.MaxItemId = 36;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemDateEdit1,
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEdit3});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 52;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07372UTShutdownManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(903, 262);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07372UTShutdownManagerBindingSource
            // 
            this.sp07372UTShutdownManagerBindingSource.DataMember = "sp07372_UT_Shutdown_Manager";
            this.sp07372UTShutdownManagerBindingSource.DataSource = this.dataSet_UT;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(6, "sort2_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colShutdownID,
            this.colReferenceNumber,
            this.colDateCreated,
            this.colCreatedByStaffID,
            this.colDueDate,
            this.colDoneDate,
            this.colStatusID,
            this.colRemarks,
            this.colStatusDescription,
            this.colCreatedByStaff,
            this.colSurveyedPoleCount,
            this.colContractorID,
            this.colContractorName,
            this.colEngineerName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDueDate, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colShutdownID
            // 
            this.colShutdownID.Caption = "Shutdown ID";
            this.colShutdownID.FieldName = "ShutdownID";
            this.colShutdownID.Name = "colShutdownID";
            this.colShutdownID.OptionsColumn.AllowEdit = false;
            this.colShutdownID.OptionsColumn.AllowFocus = false;
            this.colShutdownID.OptionsColumn.ReadOnly = true;
            this.colShutdownID.Width = 83;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 0;
            this.colReferenceNumber.Width = 157;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 1;
            this.colDateCreated.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colDueDate
            // 
            this.colDueDate.Caption = "Due Date";
            this.colDueDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDueDate.FieldName = "DueDate";
            this.colDueDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDueDate.Name = "colDueDate";
            this.colDueDate.OptionsColumn.AllowEdit = false;
            this.colDueDate.OptionsColumn.AllowFocus = false;
            this.colDueDate.OptionsColumn.ReadOnly = true;
            this.colDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDueDate.Visible = true;
            this.colDueDate.VisibleIndex = 3;
            this.colDueDate.Width = 100;
            // 
            // colDoneDate
            // 
            this.colDoneDate.Caption = "Done Date";
            this.colDoneDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDoneDate.FieldName = "DoneDate";
            this.colDoneDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDoneDate.Name = "colDoneDate";
            this.colDoneDate.OptionsColumn.AllowEdit = false;
            this.colDoneDate.OptionsColumn.AllowFocus = false;
            this.colDoneDate.OptionsColumn.ReadOnly = true;
            this.colDoneDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDoneDate.Visible = true;
            this.colDoneDate.VisibleIndex = 4;
            this.colDoneDate.Width = 100;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 9;
            this.colRemarks.Width = 103;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 5;
            this.colStatusDescription.Width = 91;
            // 
            // colCreatedByStaff
            // 
            this.colCreatedByStaff.Caption = "Created By";
            this.colCreatedByStaff.FieldName = "CreatedByStaff";
            this.colCreatedByStaff.Name = "colCreatedByStaff";
            this.colCreatedByStaff.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaff.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaff.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaff.Visible = true;
            this.colCreatedByStaff.VisibleIndex = 2;
            this.colCreatedByStaff.Width = 138;
            // 
            // colSurveyedPoleCount
            // 
            this.colSurveyedPoleCount.Caption = "Linked Surveyed Poles";
            this.colSurveyedPoleCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colSurveyedPoleCount.FieldName = "SurveyedPoleCount";
            this.colSurveyedPoleCount.Name = "colSurveyedPoleCount";
            this.colSurveyedPoleCount.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleCount.Visible = true;
            this.colSurveyedPoleCount.VisibleIndex = 8;
            this.colSurveyedPoleCount.Width = 128;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.Width = 85;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 7;
            this.colContractorName.Width = 129;
            // 
            // colEngineerName
            // 
            this.colEngineerName.Caption = "Engineer Name";
            this.colEngineerName.FieldName = "EngineerName";
            this.colEngineerName.Name = "colEngineerName";
            this.colEngineerName.OptionsColumn.AllowEdit = false;
            this.colEngineerName.OptionsColumn.AllowFocus = false;
            this.colEngineerName.Visible = true;
            this.colEngineerName.VisibleIndex = 6;
            this.colEngineerName.Width = 123;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlSurveyors);
            this.splitContainerControl2.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Help-Ups";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl5);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Linked Surveyed Spans";
            this.splitContainerControl2.Size = new System.Drawing.Size(928, 484);
            this.splitContainerControl2.SplitterPosition = 213;
            this.splitContainerControl2.TabIndex = 5;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // popupContainerControlSurveyors
            // 
            this.popupContainerControlSurveyors.Controls.Add(this.gridControl7);
            this.popupContainerControlSurveyors.Controls.Add(this.btnSurveyorOK);
            this.popupContainerControlSurveyors.Location = new System.Drawing.Point(232, 56);
            this.popupContainerControlSurveyors.Name = "popupContainerControlSurveyors";
            this.popupContainerControlSurveyors.Size = new System.Drawing.Size(200, 178);
            this.popupContainerControlSurveyors.TabIndex = 4;
            // 
            // gridControl7
            // 
            this.gridControl7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl7.DataSource = this.sp07329UTSurveyorListNoBlankBindingSource;
            this.gridControl7.Location = new System.Drawing.Point(3, 1);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemCheckEdit2});
            this.gridControl7.Size = new System.Drawing.Size(194, 147);
            this.gridControl7.TabIndex = 3;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07329UTSurveyorListNoBlankBindingSource
            // 
            this.sp07329UTSurveyorListNoBlankBindingSource.DataMember = "sp07329_UT_Surveyor_List_No_Blank";
            this.sp07329UTSurveyorListNoBlankBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDisplayName,
            this.colStaffID,
            this.colForename,
            this.colSurname,
            this.colStaffName,
            this.colActive});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 183;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Surveyor ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 80;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 97;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 109;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 76;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // btnSurveyorOK
            // 
            this.btnSurveyorOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSurveyorOK.Location = new System.Drawing.Point(3, 152);
            this.btnSurveyorOK.Name = "btnSurveyorOK";
            this.btnSurveyorOK.Size = new System.Drawing.Size(33, 23);
            this.btnSurveyorOK.TabIndex = 2;
            this.btnSurveyorOK.Text = "OK";
            this.btnSurveyorOK.Click += new System.EventHandler(this.btnSurveyorFilterOK_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(16, 125);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 3;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(34, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditDateMask,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemHyperLinkEditRevisitCount,
            this.repositoryItemTextEditShortDate});
            this.gridControl5.Size = new System.Drawing.Size(903, 210);
            this.gridControl5.TabIndex = 2;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource
            // 
            this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource.DataMember = "sp07374_UT_Shutdown_Manager_Linked_Surveyed_Poles";
            this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID1,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks2,
            this.colGUID1,
            this.colClientName2,
            this.colSurveyDate1,
            this.colSurveyDescription,
            this.colSurveyor1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colExchequerNumber,
            this.colContractValue,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.colCircuitNumber,
            this.colCircuitName,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID1,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptior,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colIsBaseClimbable,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colDateCreated1,
            this.colDoneDate1,
            this.colDueDate1,
            this.colReferenceNumber1,
            this.colClearanceDistanceUnder,
            this.colHeldUpType,
            this.colHeldUpTypeID,
            this.colRegionName,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colPrimaryName,
            this.colResponsiblePerson,
            this.colSubAreaName,
            this.colShutdownHours});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Date Surveyed";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 12;
            this.colSurveyedDate.Width = 100;
            // 
            // repositoryItemTextEditDateMask
            // 
            this.repositoryItemTextEditDateMask.AutoHeight = false;
            this.repositoryItemTextEditDateMask.Mask.EditMask = "g";
            this.repositoryItemTextEditDateMask.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateMask.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateMask.Name = "repositoryItemTextEditDateMask";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 14;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 15;
            this.colInfestationRate.Width = 100;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 16;
            this.colIsShutdownRequired.Width = 115;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 18;
            this.colHotGloveRequired.Width = 68;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 19;
            this.colLinesmanPossible.Width = 65;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance (Side)";
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 23;
            this.colClearanceDistance.Width = 144;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 25;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 26;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 27;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 3M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 28;
            this.colTreeWithin3Meters.Width = 93;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 29;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 31;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 224;
            // 
            // colSurveyDate1
            // 
            this.colSurveyDate1.Caption = "Survey - Date";
            this.colSurveyDate1.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colSurveyDate1.FieldName = "SurveyDate";
            this.colSurveyDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate1.Name = "colSurveyDate1";
            this.colSurveyDate1.OptionsColumn.AllowEdit = false;
            this.colSurveyDate1.OptionsColumn.AllowFocus = false;
            this.colSurveyDate1.OptionsColumn.ReadOnly = true;
            this.colSurveyDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate1.Width = 100;
            // 
            // colSurveyDescription
            // 
            this.colSurveyDescription.Caption = "Survey - Description";
            this.colSurveyDescription.FieldName = "SurveyDescription";
            this.colSurveyDescription.Name = "colSurveyDescription";
            this.colSurveyDescription.OptionsColumn.AllowEdit = false;
            this.colSurveyDescription.OptionsColumn.AllowFocus = false;
            this.colSurveyDescription.OptionsColumn.ReadOnly = true;
            this.colSurveyDescription.Width = 268;
            // 
            // colSurveyor1
            // 
            this.colSurveyor1.Caption = "Surveyor";
            this.colSurveyor1.FieldName = "Surveyor";
            this.colSurveyor1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colSurveyor1.Name = "colSurveyor1";
            this.colSurveyor1.OptionsColumn.AllowEdit = false;
            this.colSurveyor1.OptionsColumn.AllowFocus = false;
            this.colSurveyor1.OptionsColumn.ReadOnly = true;
            this.colSurveyor1.Visible = true;
            this.colSurveyor1.VisibleIndex = 42;
            this.colSurveyor1.Width = 120;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Width = 110;
            // 
            // colExchequerNumber
            // 
            this.colExchequerNumber.Caption = "Exchequer Number";
            this.colExchequerNumber.FieldName = "ExchequerNumber";
            this.colExchequerNumber.Name = "colExchequerNumber";
            this.colExchequerNumber.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber.Width = 112;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Span Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Width = 74;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 0;
            this.colPoleNumber.Width = 105;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Pole Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Visible = true;
            this.colPoleLastInspectionDate.VisibleIndex = 9;
            this.colPoleLastInspectionDate.Width = 117;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Pole Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateMask;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Visible = true;
            this.colPoleNextInspectionDate.VisibleIndex = 10;
            this.colPoleNextInspectionDate.Width = 120;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Tranformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 32;
            this.colIsTransformer.Width = 87;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 5;
            this.colCircuitNumber.Width = 111;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 81;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 13;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 11;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID1
            // 
            this.colSurveyStatusID1.Caption = "Survey Status ID";
            this.colSurveyStatusID1.FieldName = "SurveyStatusID";
            this.colSurveyStatusID1.Name = "colSurveyStatusID1";
            this.colSurveyStatusID1.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID1.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID1.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID1.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 20;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 33;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 35;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 34;
            this.colDeferredUnits.Width = 91;
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 30;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 38;
            this.colRevisitDate.Width = 79;
            // 
            // repositoryItemTextEditShortDate
            // 
            this.repositoryItemTextEditShortDate.AutoHeight = false;
            this.repositoryItemTextEditShortDate.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate.Name = "repositoryItemTextEditShortDate";
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.Visible = true;
            this.colSpanInvoiced.VisibleIndex = 39;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 36;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 37;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 21;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 22;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.ColumnEdit = this.repositoryItemHyperLinkEditRevisitCount;
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.Visible = true;
            this.colAccessMapPath.VisibleIndex = 40;
            this.colAccessMapPath.Width = 100;
            // 
            // repositoryItemHyperLinkEditRevisitCount
            // 
            this.repositoryItemHyperLinkEditRevisitCount.AutoHeight = false;
            this.repositoryItemHyperLinkEditRevisitCount.Name = "repositoryItemHyperLinkEditRevisitCount";
            this.repositoryItemHyperLinkEditRevisitCount.SingleClick = true;
            this.repositoryItemHyperLinkEditRevisitCount.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditRevisitCount_OpenLink);
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.ColumnEdit = this.repositoryItemHyperLinkEditRevisitCount;
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.Visible = true;
            this.colTrafficMapPath.VisibleIndex = 41;
            this.colTrafficMapPath.Width = 100;
            // 
            // colDateCreated1
            // 
            this.colDateCreated1.Caption = "Shutdown Created Date";
            this.colDateCreated1.FieldName = "DateCreated";
            this.colDateCreated1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated1.Name = "colDateCreated1";
            this.colDateCreated1.OptionsColumn.AllowEdit = false;
            this.colDateCreated1.OptionsColumn.AllowFocus = false;
            this.colDateCreated1.OptionsColumn.ReadOnly = true;
            this.colDateCreated1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCreated1.Width = 137;
            // 
            // colDoneDate1
            // 
            this.colDoneDate1.Caption = "Shutdown Done Date";
            this.colDoneDate1.FieldName = "DoneDate";
            this.colDoneDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDoneDate1.Name = "colDoneDate1";
            this.colDoneDate1.OptionsColumn.AllowEdit = false;
            this.colDoneDate1.OptionsColumn.AllowFocus = false;
            this.colDoneDate1.OptionsColumn.ReadOnly = true;
            this.colDoneDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDoneDate1.Width = 123;
            // 
            // colDueDate1
            // 
            this.colDueDate1.Caption = "Shutdown Due Date";
            this.colDueDate1.FieldName = "DueDate";
            this.colDueDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDueDate1.Name = "colDueDate1";
            this.colDueDate1.OptionsColumn.AllowEdit = false;
            this.colDueDate1.OptionsColumn.AllowFocus = false;
            this.colDueDate1.OptionsColumn.ReadOnly = true;
            this.colDueDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDueDate1.Width = 117;
            // 
            // colReferenceNumber1
            // 
            this.colReferenceNumber1.Caption = "Shutdown Reference #";
            this.colReferenceNumber1.FieldName = "ReferenceNumber";
            this.colReferenceNumber1.Name = "colReferenceNumber1";
            this.colReferenceNumber1.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber1.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber1.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber1.Visible = true;
            this.colReferenceNumber1.VisibleIndex = 42;
            this.colReferenceNumber1.Width = 133;
            // 
            // colClearanceDistanceUnder
            // 
            this.colClearanceDistanceUnder.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder.Name = "colClearanceDistanceUnder";
            this.colClearanceDistanceUnder.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder.Visible = true;
            this.colClearanceDistanceUnder.VisibleIndex = 24;
            this.colClearanceDistanceUnder.Width = 153;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type ";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 8;
            this.colHeldUpType.Width = 103;
            // 
            // colHeldUpTypeID
            // 
            this.colHeldUpTypeID.Caption = "Held-Up Type ID";
            this.colHeldUpTypeID.FieldName = "HeldUpTypeID";
            this.colHeldUpTypeID.Name = "colHeldUpTypeID";
            this.colHeldUpTypeID.OptionsColumn.AllowEdit = false;
            this.colHeldUpTypeID.OptionsColumn.AllowFocus = false;
            this.colHeldUpTypeID.OptionsColumn.ReadOnly = true;
            this.colHeldUpTypeID.Width = 98;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 111;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.Width = 69;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 6;
            this.colVoltageType.Width = 94;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 4;
            this.colFeederName.Width = 111;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 3;
            this.colPrimaryName.Width = 111;
            // 
            // colResponsiblePerson
            // 
            this.colResponsiblePerson.Caption = "Responsible Person";
            this.colResponsiblePerson.FieldName = "ResponsiblePerson";
            this.colResponsiblePerson.Name = "colResponsiblePerson";
            this.colResponsiblePerson.OptionsColumn.AllowEdit = false;
            this.colResponsiblePerson.OptionsColumn.AllowFocus = false;
            this.colResponsiblePerson.OptionsColumn.ReadOnly = true;
            this.colResponsiblePerson.Visible = true;
            this.colResponsiblePerson.VisibleIndex = 7;
            this.colResponsiblePerson.Width = 112;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 2;
            this.colSubAreaName.Width = 111;
            // 
            // colShutdownHours
            // 
            this.colShutdownHours.Caption = "Shutdown Hours";
            this.colShutdownHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colShutdownHours.FieldName = "ShutdownHours";
            this.colShutdownHours.Name = "colShutdownHours";
            this.colShutdownHours.OptionsColumn.AllowEdit = false;
            this.colShutdownHours.OptionsColumn.AllowFocus = false;
            this.colShutdownHours.OptionsColumn.ReadOnly = true;
            this.colShutdownHours.Visible = true;
            this.colShutdownHours.VisibleIndex = 17;
            this.colShutdownHours.Width = 98;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerEdit2),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerDateRange
            // 
            this.popupContainerDateRange.Caption = "Date Filter";
            this.popupContainerDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.popupContainerDateRange.EditValue = "No Date Filter";
            this.popupContainerDateRange.EditWidth = 140;
            this.popupContainerDateRange.Id = 34;
            this.popupContainerDateRange.Name = "popupContainerDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // popupContainerEdit2
            // 
            this.popupContainerEdit2.Caption = "Surveyor Filter";
            this.popupContainerEdit2.Edit = this.repositoryItemPopupContainerEdit3;
            this.popupContainerEdit2.EditValue = "No Surveyor Filter";
            this.popupContainerEdit2.EditWidth = 250;
            this.popupContainerEdit2.Id = 35;
            this.popupContainerEdit2.Name = "popupContainerEdit2";
            // 
            // repositoryItemPopupContainerEdit3
            // 
            this.repositoryItemPopupContainerEdit3.AutoHeight = false;
            this.repositoryItemPopupContainerEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit3.Name = "repositoryItemPopupContainerEdit3";
            this.repositoryItemPopupContainerEdit3.PopupControl = this.popupContainerControlSurveyors;
            this.repositoryItemPopupContainerEdit3.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEdit3.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEdit3_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Load Data";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Date Filter";
            this.barEditItem1.Edit = this.repositoryItemDateEdit1;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp07329_UT_Surveyor_List_No_BlankTableAdapter
            // 
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07372_UT_Shutdown_ManagerTableAdapter
            // 
            this.sp07372_UT_Shutdown_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter
            // 
            this.sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit2,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEdit3});
            this.gridControl2.Size = new System.Drawing.Size(928, 484);
            this.gridControl2.TabIndex = 3;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource
            // 
            this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource.DataMember = "sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_Poles";
            this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.colClearanceDistanceUnder1,
            this.colHeldUpTypeID1,
            this.colHeldUpType1,
            this.colFeederName1,
            this.colPrimaryName1,
            this.colRegionName1,
            this.colResponsiblePerson1,
            this.colSubAreaName1,
            this.colVoltageID1,
            this.colVoltageType1,
            this.colShutdownHours1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 5;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn31, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn27, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView2_CustomRowCellEdit);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView2_ShowingEditor);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Surveyed Pole ID";
            this.gridColumn1.FieldName = "SurveyedPoleID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 104;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Survey ID";
            this.gridColumn2.FieldName = "SurveyID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Pole ID";
            this.gridColumn3.FieldName = "PoleID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Feeder Contract ID";
            this.gridColumn4.FieldName = "FeederContractID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 110;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Date Surveyed";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn5.FieldName = "SurveyedDate";
            this.gridColumn5.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 6;
            this.gridColumn5.Width = 100;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Span Clear";
            this.gridColumn6.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn6.FieldName = "IsSpanClear";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 9;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Infestation Rate";
            this.gridColumn7.FieldName = "InfestationRate";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 10;
            this.gridColumn7.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Shutdown Required";
            this.gridColumn8.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn8.FieldName = "IsShutdownRequired";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 11;
            this.gridColumn8.Width = 115;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Hot Glove";
            this.gridColumn9.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn9.FieldName = "HotGloveRequired";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 13;
            this.gridColumn9.Width = 68;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Linesman";
            this.gridColumn10.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn10.FieldName = "LinesmanPossible";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 14;
            this.gridColumn10.Width = 65;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Clearance Distance (Side)";
            this.gridColumn11.FieldName = "ClearanceDistance";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 18;
            this.gridColumn11.Width = 144;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Traffic Management Req.";
            this.gridColumn12.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn12.FieldName = "TrafficManagementRequired";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 20;
            this.gridColumn12.Width = 143;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Traffic Management Resolved";
            this.gridColumn13.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn13.FieldName = "TrafficManagementResolved";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 21;
            this.gridColumn13.Width = 164;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "5 Year Clearance Achieved";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn14.FieldName = "FiveYearClearanceAchieved";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 22;
            this.gridColumn14.Width = 150;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Tree Within 3M";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn15.FieldName = "TreeWithin3Meters";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 23;
            this.gridColumn15.Width = 93;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Tree Climbable";
            this.gridColumn16.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn16.FieldName = "TreeClimbable";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 24;
            this.gridColumn16.Width = 91;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Remarks";
            this.gridColumn17.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn17.FieldName = "Remarks";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 26;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "GUID";
            this.gridColumn18.FieldName = "GUID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Name";
            this.gridColumn19.FieldName = "ClientName";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 224;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Survey - Date";
            this.gridColumn20.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn20.FieldName = "SurveyDate";
            this.gridColumn20.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn20.Width = 88;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Survey - Description";
            this.gridColumn21.FieldName = "SurveyDescription";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 268;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Surveyor";
            this.gridColumn22.FieldName = "Surveyor";
            this.gridColumn22.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 37;
            this.gridColumn22.Width = 132;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Contract Start Date";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn23.FieldName = "ContractStartDate";
            this.gridColumn23.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn23.Width = 116;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Contract End Date";
            this.gridColumn24.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn24.FieldName = "ContractEndDate";
            this.gridColumn24.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn24.Width = 110;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Exchequer Number";
            this.gridColumn25.FieldName = "ExchequerNumber";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 112;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Span Value";
            this.gridColumn26.ColumnEdit = this.repositoryItemTextEdit2;
            this.gridColumn26.FieldName = "ContractValue";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 74;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Pole Number";
            this.gridColumn27.FieldName = "PoleNumber";
            this.gridColumn27.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 0;
            this.gridColumn27.Width = 201;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Pole Last Inspection";
            this.gridColumn28.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn28.FieldName = "PoleLastInspectionDate";
            this.gridColumn28.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 4;
            this.gridColumn28.Width = 117;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Pole Next Inspection";
            this.gridColumn29.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn29.FieldName = "PoleNextInspectionDate";
            this.gridColumn29.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 5;
            this.gridColumn29.Width = 120;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Is Tranformer";
            this.gridColumn30.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn30.FieldName = "IsTransformer";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 27;
            this.gridColumn30.Width = 87;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Circuit Number";
            this.gridColumn31.FieldName = "CircuitNumber";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 20;
            this.gridColumn31.Width = 111;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Circuit Name";
            this.gridColumn32.FieldName = "CircuitName";
            this.gridColumn32.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Width = 81;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "No Work Required";
            this.gridColumn33.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn33.FieldName = "NoWorkRequired";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 8;
            this.gridColumn33.Width = 108;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Survey Status";
            this.gridColumn34.FieldName = "SurveyStatus";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 7;
            this.gridColumn34.Width = 96;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Survey Status ID";
            this.gridColumn35.FieldName = "SurveyStatusID";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 103;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "G55 Category ID";
            this.gridColumn36.FieldName = "G55CategoryID";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Width = 102;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "G55 Category";
            this.gridColumn37.FieldName = "G55CategoryDescription";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 15;
            this.gridColumn37.Width = 88;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Deferred";
            this.gridColumn38.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn38.FieldName = "Deferred";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 28;
            this.gridColumn38.Width = 64;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Deferred Unit Descriptor";
            this.gridColumn39.FieldName = "DeferredUnitDescriptior";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 30;
            this.gridColumn39.Width = 138;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Deferred Unit Descriptor ID";
            this.gridColumn40.FieldName = "DeferredUnitDescriptiorID";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Width = 152;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Deferred Units";
            this.gridColumn41.FieldName = "DeferredUnits";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 29;
            this.gridColumn41.Width = 91;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Is Base Climbable";
            this.gridColumn42.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn42.FieldName = "IsBaseClimbable";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 25;
            this.gridColumn42.Width = 104;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Revisit Date";
            this.gridColumn43.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn43.FieldName = "RevisitDate";
            this.gridColumn43.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 33;
            this.gridColumn43.Width = 79;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "d";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Map ID";
            this.gridColumn44.FieldName = "MapID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Invoiced";
            this.gridColumn45.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn45.FieldName = "SpanInvoiced";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 34;
            this.gridColumn45.Width = 62;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Invoice #";
            this.gridColumn46.FieldName = "InvoiceNumber";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Date Invoiced";
            this.gridColumn47.ColumnEdit = this.repositoryItemTextEdit3;
            this.gridColumn47.FieldName = "DateInvoiced";
            this.gridColumn47.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn47.Width = 88;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Deferral Reason";
            this.gridColumn48.FieldName = "DeferralReason";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 31;
            this.gridColumn48.Width = 99;
            // 
            // gridColumn49
            // 
            this.gridColumn49.Caption = "Deferral Remarks";
            this.gridColumn49.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn49.FieldName = "DeferralRemarks";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.Visible = true;
            this.gridColumn49.VisibleIndex = 32;
            this.gridColumn49.Width = 104;
            // 
            // gridColumn50
            // 
            this.gridColumn50.Caption = "Estimated Hours";
            this.gridColumn50.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn50.FieldName = "EstimatedHours";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.Visible = true;
            this.gridColumn50.VisibleIndex = 16;
            this.gridColumn50.Width = 99;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "n2";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Actual Hours";
            this.gridColumn51.ColumnEdit = this.repositoryItemTextEdit4;
            this.gridColumn51.FieldName = "ActualHours";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.AllowEdit = false;
            this.gridColumn51.OptionsColumn.AllowFocus = false;
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 17;
            this.gridColumn51.Width = 82;
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "Access Map";
            this.gridColumn52.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn52.FieldName = "AccessMapPath";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.OptionsColumn.ReadOnly = true;
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 35;
            this.gridColumn52.Width = 100;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Traffic Map";
            this.gridColumn53.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.gridColumn53.FieldName = "TrafficMapPath";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 36;
            this.gridColumn53.Width = 100;
            // 
            // colClearanceDistanceUnder1
            // 
            this.colClearanceDistanceUnder1.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder1.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder1.Name = "colClearanceDistanceUnder1";
            this.colClearanceDistanceUnder1.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder1.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder1.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder1.Visible = true;
            this.colClearanceDistanceUnder1.VisibleIndex = 19;
            this.colClearanceDistanceUnder1.Width = 153;
            // 
            // colHeldUpTypeID1
            // 
            this.colHeldUpTypeID1.Caption = "Held-Up Type ID";
            this.colHeldUpTypeID1.FieldName = "HeldUpTypeID";
            this.colHeldUpTypeID1.Name = "colHeldUpTypeID1";
            this.colHeldUpTypeID1.OptionsColumn.AllowEdit = false;
            this.colHeldUpTypeID1.OptionsColumn.AllowFocus = false;
            this.colHeldUpTypeID1.OptionsColumn.ReadOnly = true;
            this.colHeldUpTypeID1.Width = 98;
            // 
            // colHeldUpType1
            // 
            this.colHeldUpType1.Caption = "Held-Up Type";
            this.colHeldUpType1.FieldName = "HeldUpType";
            this.colHeldUpType1.Name = "colHeldUpType1";
            this.colHeldUpType1.OptionsColumn.AllowEdit = false;
            this.colHeldUpType1.OptionsColumn.AllowFocus = false;
            this.colHeldUpType1.OptionsColumn.ReadOnly = true;
            this.colHeldUpType1.Visible = true;
            this.colHeldUpType1.VisibleIndex = 3;
            this.colHeldUpType1.Width = 111;
            // 
            // colFeederName1
            // 
            this.colFeederName1.Caption = "Feeder Name";
            this.colFeederName1.FieldName = "FeederName";
            this.colFeederName1.Name = "colFeederName1";
            this.colFeederName1.OptionsColumn.AllowEdit = false;
            this.colFeederName1.OptionsColumn.AllowFocus = false;
            this.colFeederName1.OptionsColumn.ReadOnly = true;
            this.colFeederName1.Visible = true;
            this.colFeederName1.VisibleIndex = 26;
            this.colFeederName1.Width = 111;
            // 
            // colPrimaryName1
            // 
            this.colPrimaryName1.Caption = "Primary Name";
            this.colPrimaryName1.FieldName = "PrimaryName";
            this.colPrimaryName1.Name = "colPrimaryName1";
            this.colPrimaryName1.OptionsColumn.AllowEdit = false;
            this.colPrimaryName1.OptionsColumn.AllowFocus = false;
            this.colPrimaryName1.OptionsColumn.ReadOnly = true;
            this.colPrimaryName1.Visible = true;
            this.colPrimaryName1.VisibleIndex = 1;
            this.colPrimaryName1.Width = 111;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region Name";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 9;
            this.colRegionName1.Width = 111;
            // 
            // colResponsiblePerson1
            // 
            this.colResponsiblePerson1.Caption = "Responsible Person";
            this.colResponsiblePerson1.FieldName = "ResponsiblePerson";
            this.colResponsiblePerson1.Name = "colResponsiblePerson1";
            this.colResponsiblePerson1.OptionsColumn.AllowEdit = false;
            this.colResponsiblePerson1.OptionsColumn.AllowFocus = false;
            this.colResponsiblePerson1.OptionsColumn.ReadOnly = true;
            this.colResponsiblePerson1.Visible = true;
            this.colResponsiblePerson1.VisibleIndex = 1;
            this.colResponsiblePerson1.Width = 111;
            // 
            // colSubAreaName1
            // 
            this.colSubAreaName1.Caption = "Sub-Area Name";
            this.colSubAreaName1.FieldName = "SubAreaName";
            this.colSubAreaName1.Name = "colSubAreaName1";
            this.colSubAreaName1.OptionsColumn.AllowEdit = false;
            this.colSubAreaName1.OptionsColumn.AllowFocus = false;
            this.colSubAreaName1.OptionsColumn.ReadOnly = true;
            this.colSubAreaName1.Visible = true;
            this.colSubAreaName1.VisibleIndex = 16;
            this.colSubAreaName1.Width = 111;
            // 
            // colVoltageID1
            // 
            this.colVoltageID1.Caption = "Voltage ID";
            this.colVoltageID1.FieldName = "VoltageID";
            this.colVoltageID1.Name = "colVoltageID1";
            this.colVoltageID1.OptionsColumn.AllowEdit = false;
            this.colVoltageID1.OptionsColumn.AllowFocus = false;
            this.colVoltageID1.OptionsColumn.ReadOnly = true;
            this.colVoltageID1.Width = 69;
            // 
            // colVoltageType1
            // 
            this.colVoltageType1.Caption = "Voltage Type";
            this.colVoltageType1.FieldName = "VoltageType";
            this.colVoltageType1.Name = "colVoltageType1";
            this.colVoltageType1.OptionsColumn.AllowEdit = false;
            this.colVoltageType1.OptionsColumn.AllowFocus = false;
            this.colVoltageType1.OptionsColumn.ReadOnly = true;
            this.colVoltageType1.Visible = true;
            this.colVoltageType1.VisibleIndex = 2;
            this.colVoltageType1.Width = 111;
            // 
            // colShutdownHours1
            // 
            this.colShutdownHours1.Caption = "Shutdown Hours";
            this.colShutdownHours1.ColumnEdit = this.repositoryItemTextEdit4;
            this.colShutdownHours1.FieldName = "ShutdownHours";
            this.colShutdownHours1.Name = "colShutdownHours1";
            this.colShutdownHours1.OptionsColumn.AllowEdit = false;
            this.colShutdownHours1.OptionsColumn.AllowFocus = false;
            this.colShutdownHours1.OptionsColumn.ReadOnly = true;
            this.colShutdownHours1.Visible = true;
            this.colShutdownHours1.VisibleIndex = 12;
            this.colShutdownHours1.Width = 98;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 32);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(933, 510);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(928, 484);
            this.xtraTabPage1.Text = "Held-Ups";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(928, 484);
            this.xtraTabPage2.Text = "Held-Up Poles Not On Held-Up";
            // 
            // sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter
            // 
            this.sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // frm_UT_Shutdown_Manager
            // 
            this.ClientSize = new System.Drawing.Size(933, 542);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Shutdown_Manager";
            this.Text = "Held-Up Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Shutdown_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Shutdown_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Shutdown_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07372UTShutdownManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyors)).EndInit();
            this.popupContainerControlSurveyors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditRevisitCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraBars.BarEditItem popupContainerDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateMask;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraBars.BarEditItem popupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSurveyors;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.SimpleButton btnSurveyorOK;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private System.Windows.Forms.BindingSource sp07329UTSurveyorListNoBlankBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter sp07329_UT_Surveyor_List_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private System.Windows.Forms.BindingSource sp07372UTShutdownManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleCount;
        private DataSet_UTTableAdapters.sp07372_UT_Shutdown_ManagerTableAdapter sp07372_UT_Shutdown_ManagerTableAdapter;
        private System.Windows.Forms.BindingSource sp07374UTShutdownManagerLinkedSurveyedPolesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditRevisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated1;
        private DevExpress.XtraGrid.Columns.GridColumn colDoneDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colDueDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber1;
        private DataSet_UTTableAdapters.sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter sp07374_UT_Shutdown_Manager_Linked_Surveyed_PolesTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private System.Windows.Forms.BindingSource sp07404UTShutdownManagerUnLinkedSurveyedPolesBindingSource;
        private DataSet_UTTableAdapters.sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter sp07404_UT_Shutdown_Manager_UnLinked_Surveyed_PolesTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colEngineerName;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsiblePerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType1;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsiblePerson1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType1;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours1;
    }
}

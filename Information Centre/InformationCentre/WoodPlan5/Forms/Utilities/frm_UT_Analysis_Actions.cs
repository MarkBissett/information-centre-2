using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
//using System.Data.SqlClient;  // Used by Declaring Binding Source through code //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Wizard;
using DevExpress.XtraCharts.Native;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_UT_Analysis_Actions : BaseObjects.frmBase
    {

        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        private ExtendedPivotGridMenu epgmMenu;  // Used to extend Pivot Grid Field Right-click Menu //
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        public string i_str_SavedDirectoryName = "";

        private int i_int_FocusedGrid = 1;

        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string _FilteredClientIDs = "";
        private string _FilteredRegionIDs = "";
        private string _FilteredSubAreaIDs = "";
        private string _FilteredPrimaryIDs = "";
        private string _FilteredFeederIDs = "";
        private string _FilteredCircuitIDs = "";
        private string _FilteredCircuitNames = "";

        private string i_str_selected_records = "";
        
        BaseObjects.GridCheckMarksSelection selection1;

        private string i_str_selected_job_ids = "";
        private string i_str_selected_job_descriptions = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        #endregion

        public frm_UT_Analysis_Actions()
        {
            InitializeComponent();
        }

        private void frm_UT_Analysis_Actions_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 910002;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);

            ProcessPermissionsForForm();

            i_dtStart = DateTime.Today.AddYears(-1);
            i_dtEnd = DateTime.Today.AddDays(1).AddMilliseconds(-1);
            deFromDate.DateTime = i_dtStart;
            deToDate.DateTime = i_dtEnd;
            barEditItemDateFilter.EditValue = PopupContainerEditDateFilter_GetDateRange();

            sp07322_UT_Master_Job_TypesTableAdapter.Connection.ConnectionString = strConnectionString; 
            sp07322_UT_Master_Job_TypesTableAdapter.Fill(dataSet_UT_Reporting.sp07322_UT_Master_Job_Types);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter.Connection.ConnectionString = strConnectionString;
            
            sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter.Connection.ConnectionString = strConnectionString;

            LinkChartsToPivotGrids();

            LinkChartToRibbonBar(); // Link Chart to Chart Gallery on Ribbon Bar //
        }

        private void Chart_Preserve_User_Interaction(ChartControl chart)
        {
            // Ensure end-user rotation working if 3D chart //
            try
            {
                SimpleDiagram3D diagram = (SimpleDiagram3D)chart.Diagram;
                if (diagram != null)
                {
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeScrolling = true;
                    diagram.RuntimeZooming = true;
                }
            }
            catch
            {
            }
        }

        private void LinkChartsToPivotGrids()
        {
            chartControl1.DataSource = pivotGridControl1;
            chartControl1.SeriesDataMember = "Series";
            chartControl1.SeriesTemplate.ArgumentDataMember = "Arguments";
            chartControl1.SeriesTemplate.ValueDataMembers.AddRange(new string[] { "Values" });
            Chart_Preserve_User_Interaction(chartControl1);
        }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 450;

            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            this.Refresh();
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            LinkChartsToPivotGrids();
        }

        private void frm_UT_Analysis_Actions_Activated(object sender, EventArgs e)
        {
            gridControl1.Focus(); // Make sure the screen focuses the grid Control to ensure the SetMenuStatus fires properly //
            SetMenuStatus();
            LinkChartToRibbonBar();  // Ensure Charting items on Ribbon Bar are set correctly //
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //
                        break;
                    case 1:  // Edit Report Layout //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "rgbiChartTypes", "bbiChartPalette", "rgbiChartAppearance", "bbiRotateAxis", "bbiChartWizard", "bbiLayoutFlip", "bbiLayoutRotate", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            // Code in Grid's MouseDown event to handle enabling/disabling Dataset Menu items //
            if (i_int_FocusedGrid == 99)
            {

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                    iBool_AllowDelete = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                    iBool_AllowDelete = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
        }


        #region Popup Date Filter

        private void repositoryItemPopupContainerEditDateFilter_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateFilter_GetDateRange();
        }

        private string PopupContainerEditDateFilter_GetDateRange()
        {
            string strValue = "";
            i_dtStart = deFromDate.DateTime;
            i_dtEnd = deToDate.DateTime;
            strValue = String.Format("{0:dd/mm/yyyy HH:mm} - {1:dd/mm/yyyy HH:mm}", i_dtStart, i_dtEnd);
            return strValue;
        }

        private void btnDateFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        #endregion


        #region Popup Action Filter

        private void ActionFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditActionFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = popupContainerControlActionFilter_Get_Selected();
        }

        private string popupContainerControlActionFilter_Get_Selected()
        {
            i_str_selected_job_ids = "";    // Reset any prior values first //
            i_str_selected_job_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_job_ids = "";
                return "No Action Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_job_ids = "";
                return "No Action Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_job_ids += Convert.ToString(view.GetRowCellValue(i, "JobID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_job_descriptions = Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_job_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "JobDescription"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_job_descriptions) ? "No Action Filter" : i_str_selected_job_descriptions);
        }

        #endregion


        #region GridControl1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Actions Available - Adjust any filter and click Reload");
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            GridView view = sender as GridView;

            labelControlSelectedCount.Text = "       " + view.SelectedRowsCount.ToString() + (view.SelectedRowsCount == 1 ? " Action" : " Actions") + " Selected for Analysis";
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region PivotGridControl1

        private void pivotGridControl1_MouseUp(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            if (e.Button != MouseButtons.Right) return;
            if (pivotGridControl1.CalcHitInfo(pt).HitTest != PivotGridHitTest.Cell) return;
            // Shows the context menu.
            popupMenu1.ShowPopup(barManager1, pivotGridControl1.PointToScreen(pt));
        }

        private void pivotGridControl1_PopupMenuShowing(object sender, DevExpress.XtraPivotGrid.PopupMenuShowingEventArgs e)
        {
            PivotGridControl pivotGrid = (PivotGridControl)sender;
            epgmMenu = new ExtendedPivotGridMenu(pivotGrid);
            epgmMenu.ShowPivotGridMenu(sender, e);
        }

        private void bbiToggleAvailableColumnsVisibility_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pivotGridControl1.OptionsView.ShowFilterHeaders = !pivotGridControl1.OptionsView.ShowFilterHeaders;
        }

        #endregion


        private void repositoryItemButtonEditFilterCircuits_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_UT_Mapping_Pole_Filter fChildForm = new frm_UT_Mapping_Pole_Filter();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInClientIDs = _FilteredClientIDs;
                fChildForm._PassedInRegionIDs = _FilteredRegionIDs;
                fChildForm._PassedInSubAreaIDs = _FilteredSubAreaIDs;
                fChildForm._PassedInPrimaryIDs = _FilteredPrimaryIDs;
                fChildForm._PassedInFeederIDs = _FilteredFeederIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    _FilteredClientIDs = fChildForm.i_str_selected_client_ids;
                    _FilteredRegionIDs = fChildForm.i_str_selected_region_ids;
                    _FilteredSubAreaIDs = fChildForm.i_str_selected_subarea_ids;
                    _FilteredPrimaryIDs = fChildForm.i_str_selected_primary_ids;
                    _FilteredFeederIDs = fChildForm.i_str_selected_feeder_ids;
                    _FilteredCircuitIDs = fChildForm.i_str_selected_circuit_ids;
                    _FilteredCircuitNames = fChildForm.i_str_selected_circuit_names;
                    buttonEditFilterCircuits.EditValue = _FilteredCircuitNames;
                }
            }
        }

        private void bbiReloadData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager = splashScreenManager1;
            splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07323_UT_Analysis_Actions_Available_ActionsTableAdapter.Fill(this.dataSet_UT_Reporting.sp07323_UT_Analysis_Actions_Available_Actions, _FilteredCircuitIDs, i_dtStart, i_dtEnd, i_str_selected_job_ids);
            view.EndUpdate();

            splashScreenManager.CloseWaitForm();
        }

        private void btnAnalyse_Click(object sender, EventArgs e)
        {
            i_str_selected_records = "";
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            i_str_selected_records = "";
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to analyse before proceeding!", "Analyse Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager = splashScreenManager1;
            splashScreenManager.ShowWaitForm();

            foreach (int intRowHandle in intRowHandles)
            {
                i_str_selected_records += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ActionID"])) + ',';
            }

            sp07324_UT_Analysis_Actions_For_AnalysisTableAdapter.Fill(this.dataSet_UT_Reporting.sp07324_UT_Analysis_Actions_For_Analysis, i_str_selected_records);

            if (chartControl1.DataSource == null) LinkChartsToPivotGrids();

            splashScreenManager.CloseWaitForm();
        }




        private void LinkChartToRibbonBar()
        {
            ChartControl chart = chartControl1;
            PivotGridControl pivotGrid =  pivotGridControl1;

            // Link Chart to Chart Gallery on Ribbon Bar //
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Set_Chart(chart, pivotGrid);
                MainForm.InitChartPaletteGallery(MainForm.gddChartPalette);  // Analysis Drop Down list of Colour schemes for charting //
                MainForm.InitChartAppearanceGallery(MainForm.rgbiChartAppearance, chart.PaletteName);
                MainForm.ViewType = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(chart.Series[0].View);
            }
            catch { }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Copy to Clipboard popup menu item - fired from pivotGridControl's MouseUp event //
            PivotGridControl PivotGrid = pivotGridControl1;
            PivotGrid.Cells.CopySelectionToClipboard();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Action Filter //
                int intFoundRow = 0;
                string strDistrictFilter = default_screen_settings.RetrieveSetting("ActionFilter");
                if (!string.IsNullOrEmpty(strDistrictFilter))
                {
                    Array arrayActions = strDistrictFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewActions = (GridView)gridControl2.MainView;
                    viewActions.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayActions)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewActions.LocateByValue(0, viewActions.Columns["JobID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewActions.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewActions.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewActions.EndUpdate();
                    barEditItemAction.EditValue = popupContainerControlActionFilter_Get_Selected();
                }
            }
        }


        private void frm_UT_Analysis_Actions_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActionFilter", i_str_selected_job_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }

        }

        private void frm_UT_Analysis_Actions_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                // Update Main Forms Ribbon (Chart Styling) - Remove link from current chart //
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.Kill_Chart_Styling();
            }
            catch (Exception)
            {
            }
        }

        public override void OnLayoutRotateEvent(object sender, EventArgs e)
        {
            splitContainerControl1.Horizontal = !splitContainerControl1.Horizontal;
        }

        public override void OnLayoutFlipEvent(object sender, EventArgs e)
        {
            splitContainerControl1.Visible = false;
            Control parent1 = (Control)pivotGridControl1.Parent;
            Control parent2 = (Control)chartControl1.Parent;
            Control child1 = (Control)pivotGridControl1;
            Control child2 = (Control)chartControl1;
            parent1.Controls.Add(child2);
            parent2.Controls.Add(child1);
            splitContainerControl1.Visible = true;
         }

        private void chartControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                pmChart.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiChartWizard_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RunChartWizard();
            }
            catch { }
        }

        private void bbiRotateAxis_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                frmMain2 MainForm = (frmMain2)this.ParentForm;
                MainForm.RotateChartAxis();
            }
            catch { }
        }

 
   


    }
}

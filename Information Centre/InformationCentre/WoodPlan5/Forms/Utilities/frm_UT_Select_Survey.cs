using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Select_Survey : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public DateTime? dtSelectedSurveyDate = null;
        public int intSelectedSurveySurveyor = 0;
        public string strSelectedClientName = "";
        public int intSelectedSurveyID = 0;
        public int intSelectedClientID = 0;
        GridHitInfo downHitInfo = null;
        public int intPassedInClientID = 0;
        public int intPassedInSurveyID = 0;

        public int intLoadJustThisClientID = 0;  // Controls if only the passed in ClientID is to be shown in the grid (Only used by the Pole Manager screen when adding poles to an existing survey //
        #endregion

        public frm_UT_Select_Survey()
        {
            InitializeComponent();
        }

        private void frm_UT_Select_Survey_Load(object sender, EventArgs e)
        {
            this.FormID = 500016;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp07088_UT_Select_SurveyTableAdapter.Connection.ConnectionString = strConnectionString;
            
            try
            {
                sp07474_UT_Select_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07474_UT_Select_Client_ListTableAdapter.Fill(dataSet_UT.sp07474_UT_Select_Client_List, intLoadJustThisClientID);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            dateEditFromDate.DateTime = DateTime.Today.AddMonths(-1);
            dateEditToDate.DateTime = DateTime.Today.AddDays(1);

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInClientID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], intPassedInClientID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInSurveyID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["SurveyID"], intPassedInSurveyID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;
        private int GetTopDataRowHandle(int ARowHandle, GridView view)
        {
            if (view.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = view.GetVisibleRowHandle(view.GetVisibleIndex(ARowHandle) - 1);
                if (view.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle, view);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle, view);
                }
            }
            else return -1;
        }

        private int GetBottomDataRowHandle(int ARowHandle, GridView view)
        {
            int rowHandle = ARowHandle;
            if (view.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = view.GetChildRowHandle(rowHandle, view.GetChildRowCount(rowHandle) - 1);
                } while (view.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Surveys Available For Selection - Select a Client or adjust Date Filter to see Related Surveys";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Surveys //
            DateTime dtFromDate = new DateTime(1900, 1, 1);
            DateTime dtToDate = new DateTime(1900, 1, 1);
            if (Convert.ToDateTime(dateEditFromDate.DateTime) != Convert.ToDateTime("01/01/0001")) dtFromDate = dateEditFromDate.DateTime;
            if (Convert.ToDateTime(dateEditToDate.DateTime) != Convert.ToDateTime("01/01/0001")) dtToDate = dateEditToDate.DateTime;
 
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT.sp07088_UT_Select_Survey.Clear();
            }
            else
            {
                try
                {
                    sp07088_UT_Select_SurveyTableAdapter.Fill(dataSet_UT.sp07088_UT_Select_Survey, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), dtFromDate, dtToDate);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related surveys.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) + " \\ " +
                    (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SurveyDate"))) ? "Unknown Date" : Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "SurveyDate")).ToString("dd/MM/yyyy")) + " \\ " +
                    (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Surveyor"))) ? "Unknown Surveyor" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Surveyor")));
                intSelectedSurveyID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyID"));
                intSelectedClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                strSelectedClientName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                dtSelectedSurveyDate = Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "SurveyDate"));
                intSelectedSurveySurveyor = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SurveyorID"))) ? 0 : Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyorID")));
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadLinkedData1();
        }

 

    }
}


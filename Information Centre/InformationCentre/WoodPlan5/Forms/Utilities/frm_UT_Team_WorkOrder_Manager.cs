using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_UT_Team_WorkOrder_Manager : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState22;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState23;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        public string strPassedInWorkOrderIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs22 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs23 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultMapPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";
        private string i_str_selected_team_ids = "";
        private string i_str_selected_team_names = "";

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private string strDefaultWorkOrderDocumentPath = "";  // Path for the Picture files //
        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_UT_Team_WorkOrder_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Team_WorkOrder_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10018;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07289_UT_Team_Work_Order_Team_Filter_List, GlobalSettings.CurrentTeamID);
            gridControl6.ForceInitialize();

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl6.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07290_UT_Team_Work_Order_Manager_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "WorkOrderID");

            emptyEditor = new RepositoryItem();
            
            sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "ActionID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedDocumentID");

            sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "WorkOrderTeamID");

            sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "MapID");

            sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState22 = new RefreshGridState(gridView22, "RiskAssessmentID");

            sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState23 = new RefreshGridState(gridView23, "RiskQuestionID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultWorkOrderDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Work Order Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Work Order Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            if (strPassedInWorkOrderIDs != "")  // Opened in drill-down mode //
            {
                popupContainerDateRange.EditValue = "Custom Filter"; 
                Load_Data();  // Load records //
            }
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInWorkOrderIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
            this.Text = "Utilities - Team Work Order Manager  [" + (string.IsNullOrEmpty(GlobalSettings.CurrentTeamName) ? "No Team Filter" : GlobalSettings.CurrentTeamName) + "]";
        }

        private void frm_UT_Team_WorkOrder_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
                else if (UpdateRefreshStatus == 22)
                {
                    LoadLinkedRiskAssessments();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Team_WorkOrder_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInWorkOrderIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDateFilter", dateEditFromDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDateFilter", dateEditToDate.DateTime.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TeamFilter", i_str_selected_team_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Date Range Filter //
                string strFromDateFilter = default_screen_settings.RetrieveSetting("FromDateFilter");
                if (!(string.IsNullOrEmpty(strFromDateFilter) || Convert.ToDateTime(strFromDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditFromDate.DateTime = Convert.ToDateTime(strFromDateFilter);
                }
                string strToDateFilter = default_screen_settings.RetrieveSetting("ToDateFilter");
                if (!(string.IsNullOrEmpty(strToDateFilter) || Convert.ToDateTime(strToDateFilter) == Convert.ToDateTime("01/01/0001")))
                {
                    dateEditToDate.DateTime = Convert.ToDateTime(strToDateFilter);
                }
                i_str_date_range = PopupContainerEditDateRange_Get_Selected();
                popupContainerDateRange.EditValue = i_str_date_range;

                // Team Filter //
                int intFoundRow = 0;
                string strTeamFilter = default_screen_settings.RetrieveSetting("TeamFilter");
                if (!string.IsNullOrEmpty(strTeamFilter))
                {
                    Array arrayTeams = strTeamFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewTeams = (GridView)gridControl6.MainView;
                    viewTeams.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayTeams)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewTeams.LocateByValue(0, viewTeams.Columns["TeamID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewTeams.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewTeams.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewTeams.EndUpdate();
                    popupTeamFilter.EditValue = PopupContainerEdit2_Get_Selected();
                }
                bbiRefresh.PerformClick();
            }
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3, string strNewIDs4, string strNewIDs5, string strNewIDs22, string strNewIDs23)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs4 != "") i_str_AddedRecordIDs4 = strNewIDs4;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
            if (strNewIDs22 != "") i_str_AddedRecordIDs22 = strNewIDs22;
            if (strNewIDs23 != "") i_str_AddedRecordIDs23 = strNewIDs23;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = true;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Work Orders //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                //if (iBool_AllowAdd)
                //{
                //    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                //    bsiAdd.Enabled = true;
                //    bbiSingleAdd.Enabled = true;
                //}
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                //if (iBool_AllowDelete && intRowHandles.Length >= 1)
                //{
                //    alItems.Add("iDelete");
                //    bbiDelete.Enabled = true;
                //}
            }
            else if (i_int_FocusedGrid == 2)  // Linked Actions //
            {
                // No Data Entry for this list //
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 22)  // Linked Risk Assessments //
            {
                view = (GridView)gridControl22.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    GridView viewParent = (GridView)gridControl2.MainView;  // Action grid is parent here //
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                bbiBlockEdit.Enabled = false;
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length >= 1 ? true : false);

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length >= 1 ? true : false);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView22 navigator custom buttons //
            view = (GridView)gridControl22.MainView;
            intRowHandles = view.GetSelectedRows();
            GridView viewParent2 = (GridView)gridControl2.MainView;
            int[] intRowHandlesParent2;
            intRowHandlesParent2 = viewParent2.GetSelectedRows();
            if (iBool_AllowAdd && intRowHandlesParent2.Length > 0)
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 ? true : false);
            }
            else
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl22.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            gridControl22.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length == 1 ? true : false);
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            switch (i_int_FocusedGrid)
            {
                case 1:     // Work Orders //
                    {
                        if (!iBool_AllowAdd) return;
 
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Team_WorkOrder_Edit fChildForm = new frm_UT_Team_WorkOrder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                /*case 2:     // Actions //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frm_AT_Inspection_Manager";
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "InspectionID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "InspectionReference"));
                            fChildForm.intParentOwnershipID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "TreeOwnershipID"));
                            fChildForm.strParentNearestHouse = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "TreeHouseName"));
                        }
                    
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;*/
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 54;  // Work Orders //

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "WorkOrderID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PaddedWorkOrderID"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case 22:     // Risk Assessment //
                    {
                        ParentView = (GridView)gridControl2.MainView;  // Action grid //
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length < 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Get list of Pole IDs from selected actions //
                        strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(ParentView.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        
                        // Get the Risk Assessment Type //
                        int intType = 0;
                        int intSurveyedPoleID = 0;
                        frm_UT_Team_WorkOrder_Select_Surveyed_Pole_For_Risk fChildForm1 = new frm_UT_Team_WorkOrder_Select_Surveyed_Pole_For_Risk();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strPassedInPoleIDs = strRecordIDs;
                        switch (fChildForm1.ShowDialog())
                        {
                            case DialogResult.OK:
                                intType = 0;
                                intSurveyedPoleID = fChildForm1.intSelectedID;
                                break;
                            case DialogResult.Yes:
                                intType = 1;
                                intSurveyedPoleID = fChildForm1.intSelectedID;
                                break;
                            case DialogResult.Cancel:
                                return;
                        }

                        // Add New Risk Assessment (Site Specific) //
                        int intNewRiskAssessmentID = 0;
                        try
                        {
                            DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                            AddRecord.ChangeConnectionString(strConnectionString);
                            intNewRiskAssessmentID = Convert.ToInt32(AddRecord.sp07260_UT_Risk_Assessment_Create(intSurveyedPoleID, 0, GlobalSettings.UserID, intType));
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to add the Risk Assessment [" + ex.Message + "]. Try again. If the problem persists contact technical support.", "Add Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Inform this screen to reload the list once the screen re-activates after editing the record //
                        UpdateRefreshStatus = 22;
                        i_str_AddedRecordIDs22 = intNewRiskAssessmentID.ToString() + ";";

                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit fChildForm = new frm_UT_Risk_Assessment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = intNewRiskAssessmentID.ToString() + ",";
                        fChildForm.strFormMode = "edit";  // This should be Edit since we already added the record, so now we edit it //
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }

        private void Block_Add_Record()
        {
            /* GridView view = null;
             frmProgress fProgress = null;
             System.Reflection.MethodInfo method = null;
             string strRecordIDs = "";
             int[] intRowHandles;
             int intCount = 0;
             switch (i_int_FocusedGrid)
             {
                 case 3:     // Linked Documents //
                     if (!iBool_AllowAdd) return;
                     view = (GridView)gridControl1.MainView;
                     view.PostEditor();
                     intRowHandles = view.GetSelectedRows();
                     intCount = intRowHandles.Length;
                     if (intCount <= 1)
                     {
                         DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                         return;
                     }

                     foreach (int intRowHandle in intRowHandles)
                     {
                         strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "InspectionID")) + ',';
                     }
                     var fChildForm = new frm_AT_Inspection_Edit();
                     fChildForm.MdiParent = this.MdiParent;
                     fChildForm.GlobalSettings = this.GlobalSettings;
                     fChildForm.strRecordIDs = strRecordIDs;
                     fChildForm.strFormMode = "blockedit";
                     fChildForm.strCaller = "frm_UT_Team_WorkOrder_Manager";
                     fChildForm.intRecordCount = intCount;
                     fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                     fChildForm.Show();

                     method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                     if (method != null) method.Invoke(fChildForm, new object[] { null });
                     break;
                 default:
                     break;
             }*/
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Work Orders //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkOrderID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Team_WorkOrder_Edit fChildForm = new frm_UT_Team_WorkOrder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 54;  // Work Orders //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Work Orders //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
 
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkOrderID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Team_WorkOrder_Edit fChildForm = new frm_UT_Team_WorkOrder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        var fChildForm = new frm_AT_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 54;  // Work Orders //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 22:     // Risk Assessment //
                    {
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Work Orders //
                    {
                        /*if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
 
                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Work Orders to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Work Order" : Convert.ToString(intRowHandles.Length) + " Work Orders") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Work Order" : "these Work Orders") + " will no longer be available for selection and any linked Actions will be marked as not linked to a Work Order!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WorkOrderID")) + ",";
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("work_order", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }*/
                    }
                    break;
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                            }

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //

                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 22:  // Risk Assessment //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Risk Assessments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Risk Assessment" : Convert.ToString(intRowHandles.Length) + " Risk Assessments") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Risk Assessment" : "these Risk Assessments") + " will no longer be available for selection and any associated records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "RiskAssessmentID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("risk_assessment", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRiskAssessments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Work Orders //
                    {
                         view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkOrderID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Team_WorkOrder_Edit fChildForm = new frm_UT_Team_WorkOrder_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 2:     // Actions //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 22:     // Risk Assessment //
                    {
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Work Orders - Click Load Work Orders button";
                    break;
                case "gridView2":
                    message = "No Linked Actions Available - Select one or more Work Orders to view Related Actions";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Work Orders to view Linked Documents";
                    break;
                case "gridView4":
                    message = "No Linked Sub-Contractors Available - Select one or more Work Orders to view Related Sub-Contractors";
                    break;
                case "gridView5":
                    message = "No Linked Maps Available - Select one or more Work Orders to view Related Maps";
                    break;
                case "gridView22":
                    message = "No Linked Risk Assessments";
                    break;
                case "gridView23":
                    message = "No Linked Risk Assessment Questions - Select one or more Risk Assessments to see Questions";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiPrintWorkOrder.Enabled = true;
                bbiPrintWorkOrder.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            if (splitContainerControl1.PanelVisibility == SplitPanelVisibility.Both)
            {
                LoadLinkedRecords();
                GridView view = (GridView)gridControl2.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl3.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl4.MainView;
                view.ExpandAllGroups();
                view = (GridView)gridControl5.MainView;
                view.ExpandAllGroups();

                LoadLinkedRiskAssessments();
                view = (GridView)gridControl22.MainView;
                view.ExpandAllGroups();
            }
            SetMenuStatus();
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "WorkOrderPDF":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "WorkOrderPDF").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "TeamOnHoldCount")
            {
                int intValue = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "TeamOnHoldCount"));

                if (intValue > 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "WorkOrderPDF":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("WorkOrderPDF").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderPDF").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Work Order Document Linked - unable to proceed.\n\nTry Editing the Work Order and click the Create Work Order Document button.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strFilePath = strDefaultWorkOrderDocumentPath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                System.Diagnostics.Process.Start(strFilePath);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Work Order Document: " + strFile + ".\n\nThe Work Order may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiPrintWorkOrder.Enabled = false;
                bbiPrintWorkOrder.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiPrintWorkOrder.Enabled = false;
                bbiPrintWorkOrder.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DocumentPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "DocumentPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DocumentPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("DocumentPath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiPrintWorkOrder.Enabled = false;
                bbiPrintWorkOrder.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiPrintWorkOrder.Enabled = false;
                bbiPrintWorkOrder.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "Map":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "Map").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Map":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("Map").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditMap_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "Map").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Map Linked - unable to proceed.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view map: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView22

        private void gridView22_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView22_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 22;
            SetMenuStatus();
        }

        private void gridView22_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView22_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            LoadLinkedRiskAssessmentQuestions();
            GridView view = (GridView)gridControl23.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl22_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 22;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    {
                        if ("add".Equals(e.Button.Tag))
                        {
                            Add_Record();
                        }
                        else if ("edit".Equals(e.Button.Tag))
                        {
                            Edit_Record();
                        }
                        else if ("delete".Equals(e.Button.Tag))
                        {
                            Delete_Record();
                        }
                        else if ("view".Equals(e.Button.Tag))
                        {
                            View_Record();
                        }
                        else if ("refresh".Equals(e.Button.Tag))
                        {
                            LoadLinkedRiskAssessments();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView23

        private void gridView23_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 23;
            SetMenuStatus();
        }

        private void gridView23_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView23_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl23_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 22;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedRiskAssessmentQuestions();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView23_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "Answer":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText1":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText2":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText3":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView23_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Answer":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) == 0) e.Cancel = true;
                    break;
                case "AnswerText1":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                case "AnswerText2":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                case "AnswerText3":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            string strTeamIDs = (GlobalSettings.CurrentTeamID != 0 ? GlobalSettings.CurrentTeamID.ToString() + "," : i_str_selected_team_ids);

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerDateRange.EditValue.ToString() == "Custom Filter" && strPassedInWorkOrderIDs != "")  // Load passed in Records //
            {
                sp07290_UT_Team_Work_Order_Manager_ListTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07290_UT_Team_Work_Order_Manager_List, null, null, strTeamIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                if (i_dt_FromDate == Convert.ToDateTime("01/01/0001")) i_dt_FromDate = new DateTime(1900, 1, 1);
                if (i_dt_ToDate == Convert.ToDateTime("01/01/0001")) i_dt_ToDate = new DateTime(2900, 1, 1);
                sp07290_UT_Team_Work_Order_Manager_ListTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07290_UT_Team_Work_Order_Manager_List, i_dt_FromDate, i_dt_ToDate, strTeamIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WorkOrderID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();

        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["WorkOrderID"])) + ',';
            }

            // Populate Linked Actions //
            gridControl2.MainView.BeginUpdate();
            this.RefreshGridViewState2.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07245_UT_Work_Order_Manager_Linked_Actions.Clear();
            }
            else
            {
                sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter.Fill(dataSet_UT_WorkOrder.sp07245_UT_Work_Order_Manager_Linked_Actions, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl2.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs2 != "")
                {
                    strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl3.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs2 = "";
                }
            }

            // Populate Linked Documents //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 54, strDefaultPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs3 != "")
                {
                    strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl3.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs3 = "";
                }
            }

            // Populate Linked Teams //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState4.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07269_UT_Work_Order_Manager_Linked_SubContractors.Clear();
            }
            else
            {
                sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter.Fill(dataSet_UT_WorkOrder.sp07269_UT_Work_Order_Manager_Linked_SubContractors, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs4 != "")
                {
                    strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl4.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["WorkOrderTeamID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs4 = "";
                }
            }

            // Populate Linked Maps //
            gridControl5.MainView.BeginUpdate();
            this.RefreshGridViewState5.SaveViewInfo();
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07270_UT_Work_Order_Manager_Linked_Maps.Clear();
            }
            else
            {
                sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter.Fill(dataSet_UT_WorkOrder.sp07270_UT_Work_Order_Manager_Linked_Maps, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultMapPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl5.MainView.EndUpdate();

            if (intCount > 0)
            {
                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDs5 != "")
                {
                    strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControl5.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["MapID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDs5 = "";
                }
            }

        }

        private void LoadLinkedRiskAssessments()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["WorkOrderID"])) + ',';
            }
            gridControl22.MainView.BeginUpdate();
            this.RefreshGridViewState22.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07291_UT_Team_Work_Order_Risk_Assessment_List, strSelectedIDs);
                this.RefreshGridViewState22.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl22.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs22 != "")
            {
                strArray = i_str_AddedRecordIDs22.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl22.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskAssessmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs22 = "";
            }
        }

        private void LoadLinkedRiskAssessmentQuestions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl22.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RiskAssessmentID"])) + ',';
            }
            gridControl23.MainView.BeginUpdate();
            this.RefreshGridViewState23.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_List.Clear();
            }
            else
            {
                try
                {
                    sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState23.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl23.MainView.EndUpdate();
        }


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void bbiPrintWorkOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*string strSelectedWorkOrderID = "";
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length == 1)
            {
                strSelectedWorkOrderID = view.GetRowCellValue(intRowHandles[0], "WorkOrderID").ToString() + ";";
            }
            // Attempt to activate Print Work Orders screen if it is already open otherwise open it //
            frm_AT_WorkOrder_Print frmWorkOrderPrint = null;
            try
            {
                foreach (frmBase frmChild in this.MdiParent.MdiChildren)
                {
                    if (frmChild.FormID == 92003)
                    {
                        frmChild.Activate();
                        frmWorkOrderPrint = (frm_AT_WorkOrder_Print)frmChild;
                        frmWorkOrderPrint.LoadWorkOrdersList();  // Make sure list of Work Orders on screen is up-to-date //
                        frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strSelectedWorkOrderID);
                        return;
                    }
                }
            }
            catch (Exception)
            {
            }
            // If we are here then the form was NOT already open, so open it //
            frmWorkOrderPrint = new frm_AT_WorkOrder_Print();
            frmProgress fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();
            frmWorkOrderPrint.fProgress = fProgress;
            frmWorkOrderPrint.MdiParent = this.MdiParent;
            frmWorkOrderPrint.GlobalSettings = this.GlobalSettings;
            frmWorkOrderPrint.Show();

            frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strSelectedWorkOrderID);
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmWorkOrderPrint, new object[] { null });
            */
        }


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl2.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        #region Team Filter Panel

        private void btnTeamFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditTeam_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_team_ids = "";    // Reset any prior values first //
            i_str_selected_team_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl6.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_team_ids = "";
                return "No Team Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_team_ids = "";
                return "No Team Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_team_ids += Convert.ToString(view.GetRowCellValue(i, "TeamID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_team_names = Convert.ToString(view.GetRowCellValue(i, "TeamName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_team_names += ", " + Convert.ToString(view.GetRowCellValue(i, "TeamName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_team_names;
        }

        #endregion





    }
}


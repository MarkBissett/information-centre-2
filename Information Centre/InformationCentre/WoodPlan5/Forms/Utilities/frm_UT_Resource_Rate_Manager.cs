﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraSplashScreen;

namespace WoodPlan5
{
    public partial class frm_UT_Resource_Rate_Manager : BaseObjects.frmBase
    {
        public frm_UT_Resource_Rate_Manager()
        {
            InitializeComponent();
        }

        #region Instance Variables
        private bool isRunning = false;

        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateResourceRate; 
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public int numOfSelectedRows = 0;
        private int i_int_FocusedGrid = 0;

        GridHitInfo downHitInfo = null;
        private uint _PropertyName;
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        private bool bool_FormLoading = true;

        public bool resourceRateChanged = false;
        public enum FormMode { add, edit, view, delete, block_add, block_edit };
        public enum SentenceCase { Upper, Lower, Title, AsIs }

        private string strMessage1, strMessage2;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //               
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //


        #endregion

        #region Events

        private void managerGridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void resourceRateGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void resourceRateGridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Resource Rate Details Available");
        }

        private void resourceRateGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = sender as GridView;
            numOfSelectedRows = view.SelectedRowsCount;
            isRunning = false;
            
            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }
            int[] resourceRateIDs = new int[tempNumOfSelectedRows];

            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;

                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    {
                        case 0://Resource Rate
                            resourceRateIDs[countRows] = Convert.ToInt32((((DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow)(dr)).ResourceRateID));
                            countRows += 1;
                            break;         
                    }
                }               
                    strRecordIDs = stringRecords(resourceRateIDs);               
            }
            else
            {
                strRecordIDs = "";
            }
            SetMenuStatus();  
        }

        private void frm_UT_Resource_Rate_Manager_Load(object sender, EventArgs e)
        {
            this.FormID = 10026;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            RefreshGridViewStateResourceRate = new RefreshGridState(managerGridView, "ResourceRateID");
            PostOpen(null);
        }

        private void resourceRateGridControl_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = managerGridView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                  
                    break;
                default:
                    break;
            }
        }

     
        private void frm_UT_Resource_Rate_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus();
        }
        #endregion

        #region Method

        private string stringRecords(int[] IDs)
        {
            string strIDs = "";
            foreach (int rec in IDs)
            {
                strIDs += Convert.ToString(rec) + ',';
            }
            return strIDs;
        }
              
        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            managerGridControl.BeginUpdate();
            //.
            sp07438_UT_Resource_Rate_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List, "", strFormMode);
            this.RefreshGridViewStateResourceRate.LoadViewInfo();  // Reload any expanded groups and selected rows //
            managerGridControl.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)managerGridControl.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ResourceRateID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://ResourceRate
                    gridControl = managerGridControl;
                    strMessage1 = "1 Resource Rate record";
                    strMessage2 = " Resource Rate records";
                    break;               
                default:
                    gridControl = managerGridControl;
                    strMessage1 = "1 Resource Rate record";
                    strMessage2 = " Resource Rate records";
                    break;
            }
        }


        private void storeGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateResourceRate.SaveViewInfo();
        }
     
        private void Delete_Record()
        {
            if (!iBool_AllowDelete)
                return;
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 0:     // Resource Rate
                    if (!iBool_AllowDelete)
                        return;
                    break;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }
            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to delete.", "No Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strRecordsToLoad = "";
            foreach (int intRowHandle in intRowHandles)
            {
                DataRow dr = view.GetDataRow(intRowHandle);
                switch (i_int_FocusedGrid)
                {
                    case 0:     // Resource Rate
                        strRecordsToLoad += (((WoodPlan5.DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow)(dr)).ResourceRateID) + ',';
                        break;
                }
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) +
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") +
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateResourceRate.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateResourceRate.SaveViewInfo();  // Store Grid View State //
                try
                {
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Resource Rate record
                            sp07438_UT_Resource_Rate_ListTableAdapter.Delete("delete", strRecordsToLoad);
                            break;                        
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Edit_Record()
        {
            if (!iBool_AllowEdit)
                return;
            OpenEditForm(FormMode.edit, "frm_UT_Resource_Rate_Manager");
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd)
                return;
            OpenEditForm(FormMode.add, "frm_UT_Resource_Rate_Manager");
        }

        private void OpenEditForm(FormMode mode, string frmCaller)
        {
            try
            {
                GridView view = (GridView)managerGridControl.MainView; ;
                frmProgress fProgress = null;
                System.Reflection.MethodInfo method = null;
                string strRecordsToLoad = "";
               
                int intCount = 0;

                int[] intRowHandles = view.GetSelectedRows();

                intCount = intRowHandles.Length;
                if (mode == FormMode.edit || mode == FormMode.block_edit)
                {
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Resource Rate record(s) to edit before proceeding.", "Edit Resource Rate Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    //foreach (int intRowHandle in intRowHandles)
                    //{
                    //    //DataRow dr = view.GetDataRow(intRowHandle);
                    //    DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow dr = (DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow)view.GetDataRow(intRowHandle);
                    //    strRecordsToLoad += dr.ResourceRateID + ',';
                    //    //strRecordsToLoad += (((DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow)(dr)).ResourceRateID)+ ',';
                    //}
                    strRecordsToLoad = getFocusedRowID(view);
                }

                storeGridViewState();
                //Check if open already
                bool alreadyOpen ;
                if (Application.OpenForms["frm_UT_Resource_Rate_Edit"] != null)
                {
                    alreadyOpen = true;
                }
                else
                {
                    alreadyOpen = false;
                }

                if (alreadyOpen)
                {
                    XtraMessageBox.Show("Another instance of the Resource Rate Edit form is already open, please finish operation before you start another.", "Edit Form Open", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {

                    frm_UT_Resource_Rate_Edit sChildForm = new frm_UT_Resource_Rate_Edit();
                    sChildForm.MdiParent = this.MdiParent;
                    sChildForm.GlobalSettings = this.GlobalSettings;
                    sChildForm.strRecordIDs = strRecordsToLoad;
                    sChildForm.strFormMode = mode.ToString();
                    sChildForm.formMode = (frm_UT_Resource_Rate_Edit.FormMode)mode;
                    sChildForm.strCaller = frmCaller;
                    sChildForm.intRecordCount = 0;
                    sChildForm.FormPermissions = this.FormPermissions;
                    SplashScreenManager splashScreenManager1 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    sChildForm.splashScreenManager = splashScreenManager1;
                    sChildForm.splashScreenManager.ShowWaitForm();
                    sChildForm.Show();
                    //sChildForm.ShowDialog();

                    //method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    //if (method != null) method.Invoke(sChildForm, new object[] { null });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                    

        }

        private static string getFocusedRowID(GridView view)
        {
            int[] intRowHandles = view.GetSelectedRows();
                foreach (int intRowHandle in intRowHandles)
                {
                    DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow dr = (DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow)view.GetDataRow(intRowHandle);
                    return dr.ResourceRateID.ToString() + ',';
                }
                return "";
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                resourceRateChanged = true;
            }
        }
     
        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            if (strFormMode.ToLower() == "add")
            {
                // add
            }
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            this.Validate();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List);//Resource Rate Check
            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (resourceRateChanged)
                {
                    this.sp07438_UT_Resource_Rate_ListTableAdapter.Update(dataSet_UT_Quote);
                    resourceRateChanged = false;
                }
               
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //// If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            //string strNewIDs = "";
            ////if (this.strFormMode.ToLower() == "add")
            ////{
            //DataRowView currentRow = (DataRowView)sp_AS_11002_Equipment_ItemBindingSource.Current;
            //if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["EquipmentID"]) + ";";

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void EndEdit()
        {
            sp07438UTResourceRateListBindingSource.EndEdit();
        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessageResourceRate = CheckTablePendingSave(this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List, " on the Resource Rate Form ");//Resource Rate Check

            if (strMessageResourceRate != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessageResourceRate != "") strMessage += strMessageResourceRate;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)" + formArea + "\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)" + formArea + "\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)" + formArea + "\n";
            }
            return strMessage;
        }
                
        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;

            bbiSave.Enabled = false;
            bbiCancel.Enabled = true;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems, intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) 
                frmParent.PermissionsHandler(alItems);

            // Set enabled status of equipmentView navigator custom buttons //
            setCustomNavigatorAccess(managerGridControl, view);
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        private void setCustomNavigatorAccess(GridControl xGridControl, GridView view)
        {
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Resource Rate

                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bbiBlockAdd.Enabled = false;
                    }

                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
            }
        }
        
        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Resource Rate
                    view = (GridView)managerGridControl.MainView;
                    break;
                default:
                    view = (GridView)managerGridControl.MainView;
                    break;
            }

        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp07438_UT_Resource_Rate_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp07438_UT_Resource_Rate_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List, "", strFormMode);
        }

        public override void PostOpen(object objParameter)
        {
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            bool_FormLoading = false;
          
        }

        #endregion

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void resourceRateGridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void managerGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void managerGridControl_Click(object sender, EventArgs e)
        {

        }


    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Pole_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInPoleIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState9;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState10;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState14;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState11;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState12;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState13;  // Used by Grid View State Facilities //

        private string i_str_selected_client_ids = "";
        private string i_str_selected_client_names = "";
        private string i_str_selected_region_ids = "";
        private string i_str_selected_region_names = "";
        private string i_str_selected_subarea_ids = "";
        private string i_str_selected_subarea_names = "";
        private string i_str_selected_primary_ids = "";
        private string i_str_selected_primary_names = "";
        private string i_str_selected_feeder_ids = "";
        private string i_str_selected_feeder_names = "";
        private string i_str_selected_circuit_ids = "";
        private string i_str_selected_circuit_names = "";

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs9 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs10 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs14 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs11 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs12 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs13 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;
        BaseObjects.GridCheckMarksSelection selection5;
        BaseObjects.GridCheckMarksSelection selection6;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_UT_Pole_Manager()
        {
            InitializeComponent();
        }

        private void frm_UT_Pole_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 10009;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp07001_UT_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07001_UT_Client_FilterTableAdapter.Fill(dataSet_UT.sp07001_UT_Client_Filter);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl5.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl6.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl7.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl8.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection6 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection6.CheckMarkColumn.VisibleIndex = 0;
            selection6.CheckMarkColumn.Width = 30;

            popupContainerControlClients.Size = new System.Drawing.Size(312, 423);
            popupContainerControlRegions.Size = new System.Drawing.Size(312, 423);
            popupContainerControlSubAreas.Size = new System.Drawing.Size(312, 423);
            popupContainerControlPrimarys.Size = new System.Drawing.Size(312, 423);
            popupContainerControlFeeders.Size = new System.Drawing.Size(312, 423);
            popupContainerControlCircuits.Size = new System.Drawing.Size(512, 523);
            
            sp07041_UT_Pole_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "PoleID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedDocumentID");

            sp07064_UT_Pole_Manager_Linked_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState9 = new RefreshGridState(gridView9, "TreeID");

            sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState10 = new RefreshGridState(gridView10, "TreeSpeciesID");


            sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState14 = new RefreshGridState(gridView14, "PoleAccessIssueID");

            sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState11 = new RefreshGridState(gridView11, "PoleEnvironmentalIssueID");

            sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState12 = new RefreshGridState(gridView12, "PoleSiteHazardID");

            sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState13 = new RefreshGridState(gridView13, "PoleElectricalHazardID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (strPassedInPoleIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit1.EditValue = "Custom Filter";
                popupContainerEdit2.EditValue = "Custom Filter";
                popupContainerEdit3.EditValue = "Custom Filter";
                popupContainerEdit4.EditValue = "Custom Filter";
                popupContainerEdit5.EditValue = "Custom Filter";
                popupContainerEdit6.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInPoleIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_UT_Pole_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
                else if (UpdateRefreshStatus == 2)
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Pole_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInPoleIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientFilter", i_str_selected_client_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "RegionFilter", i_str_selected_region_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "SubAreaFilter", i_str_selected_subarea_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PrimaryFilter", i_str_selected_primary_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FeederFilter", i_str_selected_feeder_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CircuitFilter", i_str_selected_circuit_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Client Filter //
                int intFoundRow = 0;
                string strDistrictFilter = default_screen_settings.RetrieveSetting("ClientFilter");
                if (!string.IsNullOrEmpty(strDistrictFilter))
                {
                    Array arrayDistricts = strDistrictFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewClients = (GridView)gridControl2.MainView;
                    viewClients.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayDistricts)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewClients.LocateByValue(0, viewClients.Columns["ClientID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewClients.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewClients.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewClients.EndUpdate();
                    popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                    Load_Region_Filter();
                }

                // Region Filter //
                string strRegionFilter = default_screen_settings.RetrieveSetting("RegionFilter");
                if (!string.IsNullOrEmpty(strRegionFilter))
                {
                    Array arrayRegions = strRegionFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewRegions = (GridView)gridControl4.MainView;
                    viewRegions.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRegions)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewRegions.LocateByValue(0, viewRegions.Columns["RegionID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewRegions.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewRegions.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewRegions.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit2_Get_Selected();
                    Load_SubArea_Filter();
                }

                // SubArea Filter //
                string strSubAreaFilter = default_screen_settings.RetrieveSetting("SubAreaFilter");
                if (!string.IsNullOrEmpty(strSubAreaFilter))
                {
                    Array arraySubAreas = strSubAreaFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewSubAreas = (GridView)gridControl5.MainView;
                    viewSubAreas.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arraySubAreas)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewSubAreas.LocateByValue(0, viewSubAreas.Columns["SubAreaID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewSubAreas.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewSubAreas.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewSubAreas.EndUpdate();
                    popupContainerEdit3.EditValue = PopupContainerEdit3_Get_Selected();
                    Load_Primary_Filter();
                }

                // Primary Filter //
                string strPrimaryFilter = default_screen_settings.RetrieveSetting("PrimaryFilter");
                if (!string.IsNullOrEmpty(strPrimaryFilter))
                {
                    Array arrayPrimarys = strPrimaryFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewPrimarys = (GridView)gridControl6.MainView;
                    viewPrimarys.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayPrimarys)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewPrimarys.LocateByValue(0, viewPrimarys.Columns["PrimaryID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewPrimarys.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewPrimarys.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewPrimarys.EndUpdate();
                    popupContainerEdit4.EditValue = PopupContainerEdit4_Get_Selected();
                    Load_Feeder_Filter();
                }

                // Feeder Filter //
                string strFeederFilter = default_screen_settings.RetrieveSetting("FeederFilter");
                if (!string.IsNullOrEmpty(strFeederFilter))
                {
                    Array arrayFeeders = strFeederFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFeeders = (GridView)gridControl7.MainView;
                    viewFeeders.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayFeeders)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFeeders.LocateByValue(0, viewFeeders.Columns["FeederID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFeeders.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFeeders.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFeeders.EndUpdate();
                    popupContainerEdit5.EditValue = PopupContainerEdit5_Get_Selected();
                }
                bbiRefresh.PerformClick();

                // Feeder Filter //
                string strCircuitFilter = default_screen_settings.RetrieveSetting("CircuitFilter");
                if (!string.IsNullOrEmpty(strCircuitFilter))
                {
                    Array arrayCircuits = strCircuitFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewCircuits = (GridView)gridControl8.MainView;
                    viewCircuits.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayCircuits)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewCircuits.LocateByValue(0, viewCircuits.Columns["CircuitID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewCircuits.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewCircuits.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewCircuits.EndUpdate();
                    popupContainerEdit6.EditValue = PopupContainerEdit6_Get_Selected();
                }
                bbiRefreshPoles.PerformClick();

            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs3, string strNewIDs9, string strNewIDs10, string strNewIDs11, string strNewIDs12, string strNewIDs13, string strNewIDs14)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs9 != "") i_str_AddedRecordIDs9 = strNewIDs9;
            if (strNewIDs10 != "") i_str_AddedRecordIDs10 = strNewIDs10;
            if (strNewIDs11 != "") i_str_AddedRecordIDs11 = strNewIDs11;
            if (strNewIDs12 != "") i_str_AddedRecordIDs12 = strNewIDs12;
            if (strNewIDs13 != "") i_str_AddedRecordIDs13 = strNewIDs13;
            if (strNewIDs14 != "") i_str_AddedRecordIDs14 = strNewIDs14;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
 
            if (i_int_FocusedGrid == 1)  // Poles //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    bbiAddPolesToNewSurvey.Enabled = (intRowHandles.Length >= 1);
                    bbiAddPolesToExistingSurvey.Enabled = (intRowHandles.Length >= 1);
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Documents //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 14)  // Access Issues //
            {
                view = (GridView)gridControl14.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 11)  // Environmental Issues //
            {
                view = (GridView)gridControl11.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 12)  // Site Hazards //
            {
                view = (GridView)gridControl12.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 13)  // Electrical Hazards //
            {
                view = (GridView)gridControl13.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                    if (intRowHandlesParent.Length >= 2)
                    {
                        alItems.Add("iBlockAdd");
                        bbiBlockAdd.Enabled = true;
                    }
                }
                //bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            else if (i_int_FocusedGrid == 9)  // Trees //
            {
                view = (GridView)gridControl9.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 10)  // Trees //
            {
                view = (GridView)gridControl10.MainView;
                intRowHandles = view.GetSelectedRows();
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
                bbiBlockAdd.Enabled = false;
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = false;
                bbiBlockEdit.Enabled = false;
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView14 navigator custom buttons //
            view = (GridView)gridControl14.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl14.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView11 navigator custom buttons //
            view = (GridView)gridControl11.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl11.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView12 navigator custom buttons //
            view = (GridView)gridControl12.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView13 navigator custom buttons //
            view = (GridView)gridControl13.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl13.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView9 navigator custom buttons //
            view = (GridView)gridControl9.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd)
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl9.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView10 navigator custom buttons //
            view = (GridView)gridControl10.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowDelete)
            {
                gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }

        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_circuit_ids == null) i_str_selected_circuit_ids = "";
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            if (popupContainerEdit6.EditValue.ToString() == "Custom Filter" && strPassedInPoleIDs != "")  // Load passed in Trees //
            {
                sp07041_UT_Pole_ManagerTableAdapter.Fill(this.dataSet_UT.sp07041_UT_Pole_Manager, "", strPassedInPoleIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp07041_UT_Pole_ManagerTableAdapter.Fill(this.dataSet_UT.sp07041_UT_Pole_Manager, i_str_selected_circuit_ids, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["PoleID"])) + ',';
            }
            //Populate Linked Documents //
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_AT.sp00220_Linked_Documents_List.Clear();
            }
            else
            {
                sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 51, strDefaultPath);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

            // Populate Linked Trees //
            gridControl9.MainView.BeginUpdate();
            this.RefreshGridViewState9.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07064_UT_Pole_Manager_Linked_Trees.Clear();
            }
            else
            {
                sp07064_UT_Pole_Manager_Linked_TreesTableAdapter.Fill(dataSet_UT.sp07064_UT_Pole_Manager_Linked_Trees, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl9.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs9 != "")
            {
                strArray = i_str_AddedRecordIDs9.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl9.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["TreeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs9 = "";
            }

            // Populate Linked Access Issues //
            gridControl14.MainView.BeginUpdate();
            this.RefreshGridViewState14.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07070_UT_Pole_Manager_Linked_Access_Issues.Clear();
            }
            else
            {
                sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.Fill(dataSet_UT.sp07070_UT_Pole_Manager_Linked_Access_Issues, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState14.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl14.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs14 != "")
            {
                strArray = i_str_AddedRecordIDs14.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl14.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleAccessIssueID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs14 = "";
            }

            // Populate Linked Environmental Issues //
            gridControl11.MainView.BeginUpdate();
            this.RefreshGridViewState11.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07075_UT_Pole_Manager_Linked_Environmental_Issues.Clear();
            }
            else
            {
                sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.Fill(dataSet_UT.sp07075_UT_Pole_Manager_Linked_Environmental_Issues, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState11.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl11.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs11 != "")
            {
                strArray = i_str_AddedRecordIDs11.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl11.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleEnvironmentalIssueID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs11 = "";
            }

            // Populate Linked Site Hazards //
            gridControl12.MainView.BeginUpdate();
            this.RefreshGridViewState12.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07080_UT_Pole_Manager_Linked_Site_Hazards.Clear();
            }
            else
            {
                sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.Fill(dataSet_UT.sp07080_UT_Pole_Manager_Linked_Site_Hazards, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState12.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl12.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs12 != "")
            {
                strArray = i_str_AddedRecordIDs12.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl12.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleSiteHazardID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs12 = "";
            }

            // Populate Linked Electrical Hazards //
            gridControl13.MainView.BeginUpdate();
            this.RefreshGridViewState13.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07085_UT_Pole_Manager_Linked_Electrical_Hazards.Clear();
            }
            else
            {
                sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.Fill(dataSet_UT.sp07085_UT_Pole_Manager_Linked_Electrical_Hazards, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState13.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl13.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs13 != "")
            {
                strArray = i_str_AddedRecordIDs13.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl13.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleElectricalHazardID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs13 = "";
            }

        }

        private void LoadLinkedSpeciesRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl9.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
            }

            // Populate Linked Species //
            gridControl10.MainView.BeginUpdate();
            this.RefreshGridViewState10.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07065_UT_Pole_Manager_Species_Linked_To_Tree.Clear();
            }
            else
            {
                sp07065_UT_Pole_Manager_Species_Linked_To_TreeTableAdapter.Fill(dataSet_UT.sp07065_UT_Pole_Manager_Species_Linked_To_Tree, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState10.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl10.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs10 != "")
            {
                strArray = i_str_AddedRecordIDs10.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl10.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["TreeSpeciesID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs10 = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Poles //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                            fChildForm.intLinkedToCircuitID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                            fChildForm.strLinkedToCircuitName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " + (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "RegionName").ToString());
                            fChildForm.strLinkedToCircuitNumber = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitNumber").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SubAreaName").ToString());
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm2.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID"));
                            fChildForm2.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber"));
                        }

                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 9:     // Linked Trees //
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControl9.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                            fChildForm.intLinkedToCircuitID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                            fChildForm.strLinkedToPoleName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " + (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) + " \\ " + (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString());
                            fChildForm.strLinkedToCircuitNumber = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitNumber").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SubAreaName").ToString());
                            fChildForm.intLinkedToPoleID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "PoleID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID")));
                            fChildForm.strLinkedToPoleNumber = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString());
                            fChildForm.intObjectType = 0;  // 0 = Tree, 1 = Woodland, 2 = Hedge //
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 14:     // Linked Access Issues //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
 
                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber"));
                            fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.intLinkedToRecordID3 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 11:     // Linked Environmental Issues //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl11.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber"));
                            fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.intLinkedToRecordID3 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 12:     // Linked Site Hazards //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl12.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber"));
                            fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.intLinkedToRecordID3 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 13:     // Linked Electrical Hazards //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl13.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.intLinkedToRecordID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "PoleID"));
                            fChildForm.strLinkedToRecordDesc = Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "CircuitName")) + " \\ " + Convert.ToString(ParentView.GetRowCellValue(intRowHandles[0], "PoleNumber"));
                            fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            fChildForm.intLinkedToRecordID3 = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "RegionID")));
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Related Documents Link //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockadd";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 14:     // Related Access Issues //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 11:     // Related Environmental Issues //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 12:     // Related Site Hazards //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 13:     // Related Electrical Hazards //
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Circuits //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "blockedit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 9:     // Trees //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl9.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 14:     // Linked Access Issues //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleAccessIssueID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 11:     // Linked Environmental Issues //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl11.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleEnvironmentalIssueID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 12:     // Linked Site Hazards //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl12.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleSiteHazardID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 13:     // Linked Electrical Hazards //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl13.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleElectricalHazardID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Poles //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                case 9:     // Trees //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl9.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 14:     // Linked Access Issues //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl14.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleAccessIssueID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 11:     // Linked Environmental Issues //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl11.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleEnvironmentalIssueID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 12:     // Linked Site Hazards //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl12.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleSiteHazardID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 13:     // Linked Electrical Hazards //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl13.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleElectricalHazardID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Pole //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();
                        
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Poles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole" : Convert.ToString(intRowHandles.Length) + " Poles") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole" : "these Poles") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Linked Documents //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 9:  // Tree //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl9.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Trees to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree" : Convert.ToString(intRowHandles.Length) + " Trees") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree" : "these Trees") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "TreeID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("tree", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 10:  // Tree Species //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl10.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Tree Species to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Species" : Convert.ToString(intRowHandles.Length) + " Tree Species") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Species" : "these Tree Species") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "TreeSpeciesID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("tree_species", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedSpeciesRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 14:  // Linked Access Issues //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl14.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Access Issues to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Access Issue" : Convert.ToString(intRowHandles.Length) + " Pole Access Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Access Issue" : "these Pole Access Issues") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleAccessIssueID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_access_issue", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 11:  // Linked Environmental Issues //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Environmental Issues to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Environmental Issue" : Convert.ToString(intRowHandles.Length) + " Pole Environmental Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Environmental Issue" : "these Pole Environmental Issues") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleEnvironmentalIssueID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_environmental_issue", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 12:  // Linked Site Hazards //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Site Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Site Hazard" : Convert.ToString(intRowHandles.Length) + " Pole Site Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Site Hazard" : "these Pole Site Hazards") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleSiteHazardID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_site_hazard", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 13:  // Linked Electrical Hazards //
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl13.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Electrical Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Electrical Hazard" : Convert.ToString(intRowHandles.Length) + " Pole Electrical Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Electrical Hazard" : "these Pole Electrical Hazards") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleElectricalHazardID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState14.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState13.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_electrical_hazard", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Poles //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                case 9:     // Trees //
                    {
                        view = (GridView)gridControl9.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                        break;
                    }
                default:
                    break;
            }
        }


        #region Client Filter Panel

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalClientIDs = i_str_selected_client_ids;
            e.Value = PopupContainerEdit1_Get_Selected();
            if (strOriginalClientIDs != i_str_selected_client_ids)  // Only load if a the parent level selection changed //
            {
                Load_Region_Filter();
                Load_SubArea_Filter();
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_client_ids = "";    // Reset any prior values first //
            i_str_selected_client_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_client_ids += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_client_names = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_client_names += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_client_names) ? "No Client Filter" : i_str_selected_client_names);
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            if (i_str_selected_region_ids == null) i_str_selected_region_ids = "";
            if (i_str_selected_subarea_ids == null) i_str_selected_subarea_ids = "";
            if (i_str_selected_primary_ids == null) i_str_selected_primary_ids = "";
            if (i_str_selected_feeder_ids == null) i_str_selected_feeder_ids = "";
            GridView view = (GridView)gridControl8.MainView;
            view.BeginUpdate();
            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Fill(this.dataSet_UT.sp07040_UT_Circuit_Popup_Filtered_By_Various, i_str_selected_client_ids, i_str_selected_region_ids, i_str_selected_subarea_ids, i_str_selected_primary_ids, i_str_selected_feeder_ids);
            view.EndUpdate();
            Clear_Circuit_Filter();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        #endregion


        #region Region Filter Panel

        private void btnRegionFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Regions - Select one or more Clients from the Client Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalRegionIDs = i_str_selected_region_ids;
            e.Value = PopupContainerEdit2_Get_Selected();
            if (strOriginalRegionIDs != i_str_selected_region_ids)
            {
                Load_SubArea_Filter();  // Only load if a the parent level selection changed //
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_region_ids = "";    // Reset any prior values first //
            i_str_selected_region_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_region_ids += Convert.ToString(view.GetRowCellValue(i, "RegionID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_region_names = Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_region_names += ", " + Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_region_names) ? "No Region Filter" : i_str_selected_region_names);
        }

        #endregion


        #region Sub-Area Filter Panel

        private void btnSubAreaFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Sub-Areas - Select one or more Regions from the Region Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalSubAreaIDs = i_str_selected_subarea_ids;
            e.Value = PopupContainerEdit3_Get_Selected();
            if (strOriginalSubAreaIDs != i_str_selected_subarea_ids)
            {
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_subarea_ids = "";    // Reset any prior values first //
            i_str_selected_subarea_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_subarea_ids += Convert.ToString(view.GetRowCellValue(i, "SubAreaID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_subarea_names = Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_subarea_names += ", " + Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_subarea_names) ? "No Sub-Area Filter" : i_str_selected_subarea_names);
        }

        #endregion


        #region Primary Filter Panel

        private void btnPrimaryFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView6_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Primarys - Select one or more Sub-Areas from the Sub-Area Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit4_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalPrimaryIDs = i_str_selected_primary_ids;
            e.Value = PopupContainerEdit4_Get_Selected();
            if (strOriginalPrimaryIDs != i_str_selected_primary_ids)
            {
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit4_Get_Selected()
        {
            i_str_selected_primary_ids = "";    // Reset any prior values first //
            i_str_selected_primary_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl6.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_primary_ids += Convert.ToString(view.GetRowCellValue(i, "PrimaryID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_primary_names = Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_primary_names += ", " + Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_primary_names) ? "No Primary Filter" : i_str_selected_primary_names);
        }

        #endregion


        #region Feeder Filter Panel

        private void btnFeederFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView7_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Feeders - Select one or more Primarys from the Primary Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit5_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit5_Get_Selected();
        }

        private string PopupContainerEdit5_Get_Selected()
        {
            i_str_selected_feeder_ids = "";    // Reset any prior values first //
            i_str_selected_feeder_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_feeder_ids += Convert.ToString(view.GetRowCellValue(i, "FeederID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_feeder_names = Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_feeder_names += ", " + Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_feeder_names) ? "No Feeder Filter" : i_str_selected_feeder_names);
        }

        #endregion


        #region Circuit Filter Panel

        private void btnCircuitFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView8_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Circuits - Select any pre-filters from the other filter lists first then click Refresh Circuits", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit6_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit6_Get_Selected();
        }

        private string PopupContainerEdit6_Get_Selected()
        {
            i_str_selected_circuit_ids = "";    // Reset any prior values first //
            i_str_selected_circuit_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";

            }
            else if (selection6.SelectedCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_circuit_ids += Convert.ToString(view.GetRowCellValue(i, "CircuitID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_circuit_names = Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_circuit_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_circuit_names) ? "No Circuit Filter" : i_str_selected_circuit_names);
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Poles - Click Refresh Poles button";
                    break;
                case "gridView3":
                    message = "No Linked Documents Available - Select one or more Poles to view Linked Documents";
                    break;
                case "gridView9":
                    message = "No Linked Trees Available - Select one or more Poles to view Linked Trees";
                    break;
                case "gridView10":
                    message = "No Linked Species Available - Select one or more Trees to view Linked Species";
                    break;
                case "gridView14":
                    message = "No Linked Access Issues Available - Select one or more Poles to view Linked Access Issues";
                    break;
                case "gridView11":
                    message = "No Linked Environmental Issues Available - Select one or more Poles to view Linked Environmental Issues";
                    break;
                case "gridView12":
                    message = "No Linked Site Hazards Available - Select one or more Poles to view Linked Site Hazards";
                    break;
                case "gridView13":
                    message = "No Linked Electrical Hazards Available - Select one or more Poles to view Linked Electrical Hazards";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedRecords();
                    view = (GridView)gridControl3.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl9.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl14.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl11.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl12.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControl13.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridView9":
                    LoadLinkedSpeciesRecords();
                    view = (GridView)gridControl10.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                if (i_int_FocusedGrid == 1)
                {
                    //bsiDataset.Enabled = true;
                    //bbiDatasetManager.Enabled = true;
                    bbiShowMap.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPoleCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedPoleCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedPoleCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
           /* GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            string strSelectedIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CircuitID")) + ',';
            }
            try
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strSelectedIDs, "locality");
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }*/
        }

        #endregion


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView9

        private void gridView9_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 9;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl9_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView10

        private void gridView10_DoubleClick(object sender, EventArgs e)
        {
            /*DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }*/
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 10;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView14

        private void gridView14_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView14_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 14;
            SetMenuStatus();
        }

        private void gridView14_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl14_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView11

        private void gridView11_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView11_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 11;
            SetMenuStatus();
        }

        private void gridView11_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl11_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView12

        private void gridView12_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView12_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 12;
            SetMenuStatus();
        }

        private void gridView12_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl12_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView13

        private void gridView13_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView13_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 13;
            SetMenuStatus();
        }

        private void gridView13_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl13_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion



        private void Load_Region_Filter()
        {
            popupContainerEdit2.EditValue = "No Region Filter";
            gridControl4.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_client_ids))
            {
                dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client.Clear();
                selection2.ClearSelection();
             }
            else
            {
                sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Fill(dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client, i_str_selected_client_ids);
            }
            gridControl4.EndUpdate();
        }

        private void Load_SubArea_Filter()
        {
            popupContainerEdit3.EditValue = "No Sub-Area Filter";
            gridControl5.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_region_ids))
            {
                dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region.Clear();
                selection3.ClearSelection();
            }
            else
            {
                sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Fill(dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region, i_str_selected_region_ids);
            }
            gridControl5.EndUpdate();
        }

        private void Load_Primary_Filter()
        {
            popupContainerEdit4.EditValue = "No Primary Filter";
            gridControl6.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_subarea_ids))
            {
                dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea.Clear();
                selection4.ClearSelection();
            }
            else
            {
                sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Fill(dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea, i_str_selected_subarea_ids);
            }
            gridControl6.EndUpdate();
        }

        private void Load_Feeder_Filter()
        {
            popupContainerEdit5.EditValue = "No Feeder Filter";
            gridControl7.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_primary_ids))
            {
                dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary.Clear();
                selection5.ClearSelection();
            }
            else
            {
                sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Fill(dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary, i_str_selected_primary_ids);
            }
            gridControl7.EndUpdate();
        }

        private void Clear_Circuit_Filter()
        {
            popupContainerEdit6.EditValue = "No Circuit Filter";
            i_str_selected_circuit_ids = "";
            selection6.ClearSelection();
         }

        private void bbiRefreshPoles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        private void bbiAddPolesToNewSurvey_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (i_int_FocusedGrid != 1 || !iBool_AllowAdd) return;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Poles to Add to a New Survey before proceeding.", "Add poles to New Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check that all poles are for same client and region //
            int intLastClientID = 0;
            bool boolDifferentClients = false;
            int intLastRegionID = 0;
            string strRegionIDs = "";
            bool boolDifferentRegions = false;
            string strSelectedPoleIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                if (intLastClientID == 0)
                {
                    intLastClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                }
                else if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID")) != intLastClientID)
                {
                    boolDifferentClients = true;
                }
                if (intLastRegionID == 0)
                {
                    intLastRegionID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "RegionID"));
                }
                else if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "RegionID")) != intLastRegionID)
                {
                    boolDifferentRegions = true;
                }
                strRegionIDs += view.GetRowCellValue(intRowHandle, "RegionID").ToString() + ",";
                strSelectedPoleIDs += view.GetRowCellValue(intRowHandle, "PoleID").ToString() + ",";
            }
            intRowHandles = view.GetSelectedRows();

            if (boolDifferentClients)
            {
                XtraMessageBox.Show("You have selected poles from different clients. All selected poles must be owned by the same client.\n\nTry selecting different poles before proceeding.", "Add poles to New Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("<b>Warning...</b> You are about to create a <b>new</b> Survey and add the Selected Poles to the Survey - Are you sure you wish to proceed?", "Add poles to New Survey", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            if (boolDifferentRegions || intLastRegionID == 0)
            {
                var fChildForm = new frm_UT_Select_Region_For_Survey();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInRegionIDs = strRegionIDs;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    intLastRegionID = fChildForm.intSelectedRegionID;
                }
                else
                {
                    return;
                }
            }
            // Checks passed so create survey and add surveyed poles //
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Creating Survey and Adding Poles...");
            
            int intSurveyID = 0;
            try
            {
                DataSet_UT_EditTableAdapters.QueriesTableAdapter AddSurvey = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                AddSurvey.ChangeConnectionString(strConnectionString);
                intSurveyID = Convert.ToInt32(AddSurvey.sp07473_UT_Survey_Create(intLastClientID, DateTime.Today, 0, GlobalSettings.UserID, 0, 0, intLastRegionID, DateTime.Today.Year, GlobalSettings.UserID, 1));
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create new survey - an error occurred [ " + ex.Message + "].\nPlease try again. If the problems persists contact Technical Support.", "Add poles to New Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Add poles to survey //
            try
            {
                DataSet_UT_MappingTableAdapters.QueriesTableAdapter AddPolesToSurvey = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                AddPolesToSurvey.ChangeConnectionString(strConnectionString);
                AddPolesToSurvey.sp07110_UT_Survey_Add_Poles_To_Survey(strSelectedPoleIDs, intSurveyID, GlobalSettings.UserID, GlobalSettings.UserID);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add selected poles to new survey - an error occurred [ " + ex.Message + "].\n\nThe Survey was created but the poles were not added. You should now select the Add Poles to Existing Survey menu item and select this new survey to proceed.", "Add Poles to New Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Success so refresh parent Manager if it is open and open survey  //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Manager")
                    {
                        var fParentForm = (frm_UT_Survey_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, intSurveyID.ToString() + ";", "", "", "");
                    }
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            var fChildForm2 = new frm_UT_Survey_Edit();
            fChildForm2.MdiParent = this.MdiParent;
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.strRecordIDs = intSurveyID.ToString() + ",";
            fChildForm2.strFormMode = "edit";
            fChildForm2.strCaller = this.Name;
            fChildForm2.intRecordCount = 1;
            fChildForm2.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm2.splashScreenManager = splashScreenManager2;
            fChildForm2.splashScreenManager.ShowWaitForm();
            fChildForm2.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm2, new object[] { null });
        }

        private void bbiAddPolesToExistingSurvey_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (i_int_FocusedGrid != 1 || !iBool_AllowAdd) return;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Poles to Add to an Existing Survey before proceeding.", "Add poles to Existing Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check that all poles are for same client and region //
            int intLastClientID = 0;
            bool boolDifferentClients = false;
            string strSelectedPoleIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                if (intLastClientID == 0)
                {
                    intLastClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                }
                else if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID")) != intLastClientID)
                {
                    boolDifferentClients = true;
                }
                strSelectedPoleIDs += view.GetRowCellValue(intRowHandle, "PoleID").ToString() + ",";
            }
            intRowHandles = view.GetSelectedRows();

            if (boolDifferentClients)
            {
                XtraMessageBox.Show("You have selected poles from different clients. All selected poles must be owned by the same client.\n\nTry selecting different poles before proceeding.", "Add poles to Existing Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("<b>Warning...</b> You are about to add the Selected Poles to an <b>Existing</b> Survey - Are you sure you wish to proceed?", "Add poles to Existing Survey", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            int intSurveyID = 0;
            var fChildForm = new frm_UT_Select_Survey();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intSelectedClientID = intLastClientID;
            fChildForm.intLoadJustThisClientID = intLastClientID;  // Prevent the Client from being changed in the top grid of the select screen //
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                intSurveyID = fChildForm.intSelectedSurveyID;
            }
            else
            {
                return;
            }

            // Check the selected Survey does not already have the selected pole attached to it - if it does we need to de-select the pole //
            string strExistingPoleIDsOnSurvey = "";
            try
            {
                DataSet_UT_EditTableAdapters.QueriesTableAdapter GetExistingPolesOnSurvey = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                GetExistingPolesOnSurvey.ChangeConnectionString(strConnectionString);
                strExistingPoleIDsOnSurvey = GetExistingPolesOnSurvey.sp07475_UT_Get_Comma_Separated_PoleIDs_From_SurveyID(intSurveyID.ToString() + ",").ToString() + ",";
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add selected poles to an existing survey - an error occurred while checking if the selected poles are already on the survey [ " + ex.Message + "].\n\nPlease try again. If the problem persist contact Technical Support.", "Add Poles to Existing Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            string strPoleID = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strPoleID = view.GetRowCellValue(intRowHandle, "PoleID").ToString();
                if (strExistingPoleIDsOnSurvey.Contains(", " + strPoleID + ",")) view.UnselectRow(intRowHandle);
            }

            intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Poles to Add to an Existing Survey before proceeding.\n\nNote: This process will de-select any selected pole if it is already attached to the survey selected.", "Add poles to Existing Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Checks passed so add surveyed poles to selected survey //
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Adding Poles to Survey...");

            try
            {
                DataSet_UT_MappingTableAdapters.QueriesTableAdapter AddPolesToSurvey = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                AddPolesToSurvey.ChangeConnectionString(strConnectionString);
                AddPolesToSurvey.sp07110_UT_Survey_Add_Poles_To_Survey(strSelectedPoleIDs, intSurveyID, GlobalSettings.UserID, GlobalSettings.UserID);
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add selected poles to an existing survey - an error occurred [ " + ex.Message + "].\n\nPlease try again. If the problem persist contact Technical Support.", "Add Poles to Existing Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            // Success so refresh parent Manager if it is open and open survey  //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Manager")
                    {
                        var fParentForm = (frm_UT_Survey_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "", "", "");
                    }
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            var fChildForm2 = new frm_UT_Survey_Edit();
            fChildForm2.MdiParent = this.MdiParent;
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.strRecordIDs = intSurveyID.ToString() + ",";
            fChildForm2.strFormMode = "edit";
            fChildForm2.strCaller = this.Name;
            fChildForm2.intRecordCount = 1;
            fChildForm2.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm2.splashScreenManager = splashScreenManager2;
            fChildForm2.splashScreenManager.ShowWaitForm();
            fChildForm2.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm2, new object[] { null });
        }



 

    }
}


namespace WoodPlan5
{
    partial class frm_UT_Circuit_Contract_Clone
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Circuit_Contract_Clone));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditAmount = new DevExpress.XtraEditors.SpinEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditApplyRateUplift = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditClone = new DevExpress.XtraEditors.CheckEdit();
            this.groupControlClonedValues = new DevExpress.XtraEditors.GroupControl();
            this.labelControlActiveContract = new DevExpress.XtraEditors.LabelControl();
            this.checkEditActive = new DevExpress.XtraEditors.CheckEdit();
            this.labelControlContractDurationTo = new DevExpress.XtraEditors.LabelControl();
            this.ToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControlContractDurationFrom = new DevExpress.XtraEditors.LabelControl();
            this.FromDate = new DevExpress.XtraEditors.DateEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyRateUplift.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlClonedValues)).BeginInit();
            this.groupControlClonedValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(649, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 206);
            this.barDockControlBottom.Size = new System.Drawing.Size(649, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 206);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(649, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 206);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.checkEdit2);
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(7, 91);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(299, 74);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "Select Method of Update:";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit2.Location = new System.Drawing.Point(6, 49);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Fixed Amount";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 2;
            this.checkEdit2.Size = new System.Drawing.Size(288, 19);
            this.checkEdit2.TabIndex = 1;
            this.checkEdit2.TabStop = false;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Percentage";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 2;
            this.checkEdit1.Size = new System.Drawing.Size(288, 19);
            this.checkEdit1.TabIndex = 0;
            this.checkEdit1.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.spinEditAmount);
            this.groupControl2.Location = new System.Drawing.Point(309, 91);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(332, 74);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "Enter Amount to Increase \\ Decrease Rate By:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(8, 31);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Amount:";
            // 
            // spinEditAmount
            // 
            this.spinEditAmount.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditAmount.Location = new System.Drawing.Point(70, 28);
            this.spinEditAmount.MenuManager = this.barManager1;
            this.spinEditAmount.Name = "spinEditAmount";
            this.spinEditAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditAmount.Properties.Mask.EditMask = "P";
            this.spinEditAmount.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditAmount.Size = new System.Drawing.Size(100, 20);
            this.spinEditAmount.TabIndex = 0;
            this.spinEditAmount.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditAmount_Validating);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(451, 174);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "Clone Contracts";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(549, 174);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(92, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 1;
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Records Selected For Update: 0";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 0;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(649, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 206);
            this.barDockControl2.Size = new System.Drawing.Size(649, 26);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 206);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(649, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 206);
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.checkEditApplyRateUplift);
            this.groupControl3.Controls.Add(this.checkEditClone);
            this.groupControl3.Location = new System.Drawing.Point(7, 8);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(299, 74);
            this.groupControl3.TabIndex = 13;
            this.groupControl3.Text = "Select Action to be performed:";
            // 
            // checkEditApplyRateUplift
            // 
            this.checkEditApplyRateUplift.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditApplyRateUplift.Location = new System.Drawing.Point(6, 49);
            this.checkEditApplyRateUplift.MenuManager = this.barManager1;
            this.checkEditApplyRateUplift.Name = "checkEditApplyRateUplift";
            this.checkEditApplyRateUplift.Properties.Caption = "Apply Rate Uplift";
            this.checkEditApplyRateUplift.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditApplyRateUplift.Properties.RadioGroupIndex = 1;
            this.checkEditApplyRateUplift.Size = new System.Drawing.Size(288, 19);
            this.checkEditApplyRateUplift.TabIndex = 1;
            this.checkEditApplyRateUplift.TabStop = false;
            this.checkEditApplyRateUplift.CheckedChanged += new System.EventHandler(this.checkEditApplyRateUplift_CheckedChanged);
            // 
            // checkEditClone
            // 
            this.checkEditClone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditClone.EditValue = true;
            this.checkEditClone.Location = new System.Drawing.Point(6, 26);
            this.checkEditClone.MenuManager = this.barManager1;
            this.checkEditClone.Name = "checkEditClone";
            this.checkEditClone.Properties.Caption = "Clone Feeder Contract and Optionally Apply Rate Uplift";
            this.checkEditClone.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditClone.Properties.RadioGroupIndex = 1;
            this.checkEditClone.Size = new System.Drawing.Size(288, 19);
            this.checkEditClone.TabIndex = 0;
            this.checkEditClone.CheckedChanged += new System.EventHandler(this.checkEditClone_CheckedChanged);
            // 
            // groupControlClonedValues
            // 
            this.groupControlClonedValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlClonedValues.Controls.Add(this.labelControlActiveContract);
            this.groupControlClonedValues.Controls.Add(this.checkEditActive);
            this.groupControlClonedValues.Controls.Add(this.labelControlContractDurationTo);
            this.groupControlClonedValues.Controls.Add(this.ToDate);
            this.groupControlClonedValues.Controls.Add(this.labelControlContractDurationFrom);
            this.groupControlClonedValues.Controls.Add(this.FromDate);
            this.groupControlClonedValues.Location = new System.Drawing.Point(309, 8);
            this.groupControlClonedValues.Name = "groupControlClonedValues";
            this.groupControlClonedValues.Size = new System.Drawing.Size(332, 74);
            this.groupControlClonedValues.TabIndex = 14;
            this.groupControlClonedValues.Text = "Cloned Contract Details";
            // 
            // labelControlActiveContract
            // 
            this.labelControlActiveContract.Location = new System.Drawing.Point(6, 53);
            this.labelControlActiveContract.Name = "labelControlActiveContract";
            this.labelControlActiveContract.Size = new System.Drawing.Size(79, 13);
            this.labelControlActiveContract.TabIndex = 5;
            this.labelControlActiveContract.Text = "Active Contract:";
            // 
            // checkEditActive
            // 
            this.checkEditActive.Location = new System.Drawing.Point(100, 51);
            this.checkEditActive.MenuManager = this.barManager1;
            this.checkEditActive.Name = "checkEditActive";
            this.checkEditActive.Properties.Caption = "(Tick if Yes)";
            this.checkEditActive.Size = new System.Drawing.Size(75, 19);
            this.checkEditActive.TabIndex = 4;
            // 
            // labelControlContractDurationTo
            // 
            this.labelControlContractDurationTo.Location = new System.Drawing.Point(205, 29);
            this.labelControlContractDurationTo.Name = "labelControlContractDurationTo";
            this.labelControlContractDurationTo.Size = new System.Drawing.Size(16, 13);
            this.labelControlContractDurationTo.TabIndex = 3;
            this.labelControlContractDurationTo.Text = "To:";
            // 
            // ToDate
            // 
            this.ToDate.EditValue = null;
            this.ToDate.Location = new System.Drawing.Point(225, 24);
            this.ToDate.MenuManager = this.barManager1;
            this.ToDate.Name = "ToDate";
            this.ToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.ToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.ToDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.ToDate.Size = new System.Drawing.Size(100, 20);
            this.ToDate.TabIndex = 2;
            this.ToDate.Validating += new System.ComponentModel.CancelEventHandler(this.ToDate_Validating);
            // 
            // labelControlContractDurationFrom
            // 
            this.labelControlContractDurationFrom.Location = new System.Drawing.Point(6, 28);
            this.labelControlContractDurationFrom.Name = "labelControlContractDurationFrom";
            this.labelControlContractDurationFrom.Size = new System.Drawing.Size(90, 13);
            this.labelControlContractDurationFrom.TabIndex = 1;
            this.labelControlContractDurationFrom.Text = "Contract Duration:";
            // 
            // FromDate
            // 
            this.FromDate.EditValue = null;
            this.FromDate.Location = new System.Drawing.Point(102, 25);
            this.FromDate.MenuManager = this.barManager1;
            this.FromDate.Name = "FromDate";
            this.FromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.FromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.FromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.FromDate.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.FromDate.Size = new System.Drawing.Size(100, 20);
            this.FromDate.TabIndex = 0;
            this.FromDate.Validating += new System.ComponentModel.CancelEventHandler(this.FromDate_Validating);
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // frm_UT_Circuit_Contract_Clone
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(649, 232);
            this.Controls.Add(this.groupControlClonedValues);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_UT_Circuit_Contract_Clone";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Feeder Contract - Clone \\ Apply Rate Uplift";
            this.Load += new System.EventHandler(this.frm_UT_Circuit_Contract_Clone_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.groupControl2, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.groupControl3, 0);
            this.Controls.SetChildIndex(this.groupControlClonedValues, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditApplyRateUplift.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditClone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlClonedValues)).EndInit();
            this.groupControlClonedValues.ResumeLayout(false);
            this.groupControlClonedValues.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditActive.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinEditAmount;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarButtonItem barStaticItemInformation;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditApplyRateUplift;
        private DevExpress.XtraEditors.CheckEdit checkEditClone;
        private DevExpress.XtraEditors.GroupControl groupControlClonedValues;
        private DevExpress.XtraEditors.LabelControl labelControlContractDurationTo;
        private DevExpress.XtraEditors.DateEdit ToDate;
        private DevExpress.XtraEditors.LabelControl labelControlContractDurationFrom;
        private DevExpress.XtraEditors.DateEdit FromDate;
        private DevExpress.XtraEditors.LabelControl labelControlActiveContract;
        private DevExpress.XtraEditors.CheckEdit checkEditActive;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
    }
}

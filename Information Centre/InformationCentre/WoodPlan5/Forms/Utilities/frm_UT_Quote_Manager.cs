using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using System.Collections.Specialized;
using DevExpress.XtraSplashScreen;

namespace WoodPlan5
{
    public partial class frm_UT_Quote_Manager : BaseObjects.frmBase
    {

        private void frm_AS_Billing_Centre_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 1103;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //

            RefreshGridViewStateBillingCentre = new RefreshGridState(quoteGridView, "BillingCentreCodeID");

            RefreshGridViewStateCompany = new RefreshGridState(companyGridView, "CompanyID");

            RefreshGridViewStateDepartment = new RefreshGridState(departmentGridView, "DepartmentID");

            RefreshGridViewStateCostCentre = new RefreshGridState(costCentreGridView, "CostCentreID");

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //


            if (strPassedInRecordDs != "")  // Opened in drill-down mode //
            {
                //popupContainerEdit1.Text = "Custom Filter";
                Load_Data();  // Load records //
            }

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();

        }
        
        #region Instance Variables...

        private Boolean iboolDistinctMulitpleChildSelected = false;
        private int iDistinctSelectedType = 0;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        public string strBillingCentreCodeRecordIDs = "";
        public string strCompanyRecordIDs = "";
        public string strDepartmentRecordIDs = "";
        public string strCostCentreRecordIDs = "";

        public string strRecordsToLoad = "";
        public int numOfSelectedRows = 0;
        public int mainViewRowCount = 0;
        private string gcRef = "";
        private string tempMake = "";
        private string tempModel = "";
        private string tempManufacturer = "";
        private string tempRegistration = "";
        private string strMessage1, strMessage2, strMessage3;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;

        private bool iBool_AllowAddCompany = false;
        private bool iBool_AllowEditCompany = false;
        private bool iBool_AllowDeleteCompany = false;

        private bool iBool_AllowAddDepartment = false;
        private bool iBool_AllowEditDepartment = false;
        private bool iBool_AllowDeleteDepartment = false;

        private bool iBool_AllowAddCostCentre = false;
        private bool iBool_AllowEditCostCentre = false;
        private bool iBool_AllowDeleteCostCentre = false;
             
        int i_int_FocusedGrid = 0;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        
        
        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateBillingCentre; 
        public RefreshGridState RefreshGridViewStateCompany;  
        public RefreshGridState RefreshGridViewStateDepartment;  
        public RefreshGridState RefreshGridViewStateCostCentre;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //


        private enum FormType { child, parent };
        private enum GridType {child, parent };
        private enum FormMode { add, edit, view, delete, blockadd, blockedit };
        private enum BillingCodeType { BillingCentreCode, Company = 1, Department = 2, CostCentre = 3}

        #endregion

        #region Constructor and compulsory implementation

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems,intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of Billing Centre CodeView navigator custom buttons //
            getCRUDButtonPermissions(quoteGridControl, iBool_AllowAdd, iBool_AllowEdit, iBool_AllowDelete);

            // Set enabled status of department GridControl navigator custom buttons //
            getCRUDButtonPermissions(departmentGridControl, iBool_AllowAddDepartment, iBool_AllowEditDepartment, iBool_AllowDeleteDepartment);

            // Set enabled status of company GridControl navigator custom buttons //
            getCRUDButtonPermissions(companyGridControl, iBool_AllowAddCompany, iBool_AllowEditCompany, iBool_AllowDeleteCompany);

            // Set enabled status of costCentre GridControl navigator custom buttons //
            getCRUDButtonPermissions(costCentreGridControl, iBool_AllowAddCostCentre, iBool_AllowEditCostCentre, iBool_AllowDeleteCostCentre);       

        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Billing_Centre_Code
                                   
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    bbiBlockAdd.Enabled = false;
                }
                
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    bbiBlockEdit.Enabled = false;                  
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                break;

                case 1://company
                getChildMouseMenuPermissions(alItems,intRowHandles, iBool_AllowAddCompany, iBool_AllowEditCompany, iBool_AllowDeleteCompany);
                break;
                case 2://department
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddDepartment, iBool_AllowEditDepartment, iBool_AllowDeleteDepartment);
                break;
                case 3://costCentre
                getChildMouseMenuPermissions(alItems, intRowHandles, iBool_AllowAddCostCentre, iBool_AllowEditCostCentre, iBool_AllowDeleteCostCentre);
                break;
            }
        }

        private void getCRUDButtonPermissions(GridControl GridControlX,bool iBool_AllowAddX, bool iBool_AllowEditX, bool iBool_AllowDeleteX)
        {
            GridControl xGridControl = null;
            xGridControl = GridControlX;
            GridView view = null;
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            if (iBool_AllowAddX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEditX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDeleteX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        private void getChildMouseMenuPermissions(ArrayList alItems, int[] intRowHandles, bool iBool_AllowAddChild, bool iBool_AllowEditChild, bool iBool_AllowDeleteChild)
        {
            if (iBool_AllowAddChild)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
              
            }
            if (iBool_AllowEditChild && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                bbiBlockEdit.Enabled = false;
            }
            if (iBool_AllowDeleteChild && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
        }

        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://billingCentreCode
                    view = (GridView)quoteGridControl.MainView;
                    break;
                case 1://company
                    view = (GridView)companyGridControl.MainView;
                    break;
                case 2://department
                    view = (GridView)departmentGridControl.MainView;
                    break;
                case 3://cost centre
                    view = (GridView)costCentreGridControl.MainView;
                    break;

                default://billingCentreCode
                    view = (GridView)quoteGridControl.MainView;
                    break;
            }
     
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Company //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddCompany = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditCompany = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteCompany = true;
                        }
                        break;
                    case 2:  // department //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddDepartment = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditDepartment = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteDepartment = true;
                        }
                        break;
                    case 3:  // costCentre //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddCostCentre = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditCostCentre= true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteCostCentre = true;
                        }
                        break;
                }
            }
        }

        public frm_UT_Quote_Manager()
        {
            InitializeComponent();
        }

        #endregion

        #region Form_Events

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "billingCentreCodeGridView":
                    message = "No Billing Centre Code Information Available";
                    break;
                case "companyGridView":
                    message = "No Company Information Available";
                    break;
                case "departmentGridView":
                    message = "No Department Information Available";
                    break;
                case "costCentreGridView":
                    message = "No Cost Centre Information Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "billingCentreCodeView":
                    LoadLinkedRecords();
                    expandChildGridView();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
            updateSelectedBillingCentreCodeIDs();
        }

        #endregion


        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    View_Record();
            }
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "billingCentreCodeGridView":
                    i_int_FocusedGrid = 0;
                    break;

                case "companyGridView":
                    i_int_FocusedGrid = 1;
                    break;

                case "departmentGridView":
                    i_int_FocusedGrid = 2;
                    break;

                case "costCentreGridView":
                    i_int_FocusedGrid = 3;
                    break;
            }

            SetMenuStatus();
        }

        private void commonGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("blockadd".Equals(e.Button.Tag))
                    {
                        Block_Add();
                    }
                    else if ("edit".Equals(e.Button.Tag) && i_int_FocusedGrid != 0)
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void commonGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);

            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item, "", "view");
            sp_AS_11057_Company_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11057_Company_Item, "", "view");
            sp_AS_11060_Department_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11060_Department_Item , "", "view");
            sp_AS_11063_Cost_Centre_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_Item, "", "view");
        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11057_Company_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11060_Department_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_AS_11063_Cost_Centre_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
        }

        private void frm_AS_Billing_Centre_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }
        
        #endregion

        #region Form_Functions

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus(); 
        }

        private void updateSelectedBillingCentreCodeIDs()
        {

            GridControl gridControl = null;
            switch(i_int_FocusedGrid)
            {
                case 0:
                    gridControl = quoteGridControl;
                    break;
                case 1:
                    gridControl = companyGridControl;
                    break;
                case 2:
                    gridControl = departmentGridControl;
                    break;
                case 3:
                    gridControl = costCentreGridControl;
                    break;
                default:
                    gridControl = quoteGridControl;
                    break;
            }
            
            GridView view = (GridView)gridControl.MainView;
            numOfSelectedRows = view.SelectedRowsCount;

            if (i_int_FocusedGrid == 0)
            {
                mainViewRowCount = view.SelectedRowsCount;
            }
            else
            {
                mainViewRowCount = 0;
            }

            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }
            int[] BillingCentreCodeIDs = new int[tempNumOfSelectedRows];
            int[] companyIDs = new int[tempNumOfSelectedRows];
            int[] departmentIDs = new int[tempNumOfSelectedRows];
            int[] costCentreIDs = new int[tempNumOfSelectedRows];

            bool updateBillingCodeString = false;
            bool updateCompanyString = false;
            bool updateDepartmentString = false;
            bool updateCostCentreString = false;

            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;

                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    { 
                        case 0://BillingCentreCode
                            BillingCentreCodeIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_ItemRow)(dr)).BillingCentreCodeID);
                            companyIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_ItemRow)(dr)).CompanyID);
                            departmentIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_ItemRow)(dr)).DepartmentID);
                            costCentreIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_ItemRow)(dr)).CostCentreID);
                            countRows += 1;
                            updateBillingCodeString = true;
                            updateCompanyString = true;
                            updateDepartmentString = true;
                            updateCostCentreString = true;
                        break;
                        case 1://Company
                            companyIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11057_Company_ItemRow)(dr)).CompanyID);
                            countRows += 1;
                            updateCompanyString = true;
                        break;
                        case 2://Department
                            departmentIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11060_Department_ItemRow)(dr)).DepartmentID);
                            countRows += 1;
                            updateDepartmentString = true;
                        break;
                        case 3://CostCentre
                            costCentreIDs[countRows] = (((DataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_ItemRow)(dr)).CostCentreID);
                            countRows += 1;
                            updateCostCentreString = true;
                        break;
                    }
                    
                }
                if (updateBillingCodeString)
                {
                    strBillingCentreCodeRecordIDs = stringRecords(BillingCentreCodeIDs);
                }
                if (updateCompanyString)
                {
                    strCompanyRecordIDs = stringRecords(companyIDs);
                }
                if (updateDepartmentString)
                {
                    strDepartmentRecordIDs = stringRecords(departmentIDs);
                }
                if (updateCostCentreString)
                {
                    strCostCentreRecordIDs = stringRecords(costCentreIDs);
                }
            }
            else
            {
                strBillingCentreCodeRecordIDs = "";
                strCompanyRecordIDs = "";
                strDepartmentRecordIDs = "";
                strCostCentreRecordIDs = "";
            }
        }

        private void LoadLinkedRecords()
        {
            //companyTabPage
            companyGridControl.MainView.BeginUpdate();
            this.RefreshGridViewStateCompany.SaveViewInfo();  // Store expanded groups and selected rows //
            if (mainViewRowCount == 0)
            {
                this.dataSet_AS_DataEntry.sp_AS_11057_Company_Item.Clear();
            }
            else
            {
                this.sp_AS_11057_Company_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11057_Company_Item, (string.IsNullOrEmpty(strCompanyRecordIDs) ? "" : strCompanyRecordIDs), "view");    
                this.RefreshGridViewStateCompany.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            companyGridControl.MainView.EndUpdate();

            //departmentTabPage
            departmentGridControl.MainView.BeginUpdate();
            this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store expanded groups and selected rows //
            if (mainViewRowCount == 0)
            {
                this.dataSet_AS_DataEntry.sp_AS_11060_Department_Item.Clear();
            }
            else
            {
                this.sp_AS_11060_Department_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11060_Department_Item, (string.IsNullOrEmpty(strDepartmentRecordIDs) ? "" : strDepartmentRecordIDs), "view");
                this.RefreshGridViewStateDepartment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            departmentGridControl.MainView.EndUpdate();

            //costCentreTabPage
            costCentreGridControl.MainView.BeginUpdate();
            this.RefreshGridViewStateCostCentre.SaveViewInfo();  // Store expanded groups and selected rows //
            if (mainViewRowCount == 0)
            {
                this.dataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_Item.Clear();
            }
            else
            {
                this.sp_AS_11063_Cost_Centre_ItemTableAdapter.Fill(this.dataSet_AS_DataEntry.sp_AS_11063_Cost_Centre_Item, (string.IsNullOrEmpty(strCostCentreRecordIDs) ? "" : strCostCentreRecordIDs), "view");  
                this.RefreshGridViewStateCompany.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            costCentreGridControl.MainView.EndUpdate();      
        }

        private void customFilterDraw(GridView view, CustomFilterDialogEventArgs e)
        {
            if (e.Column.ColumnType != typeof(DateTime))
            {
                GridColumnCollection cols = new GridColumnCollection(view);
                GridColumn column = cols.AddField(e.Column.FieldName);
                RepositoryItemComboBox comboBox = new RepositoryItemComboBox();
                object[] values = view.DataController.FilterHelper.GetUniqueColumnValues(view.DataController.Columns[column.ColumnHandle], view.OptionsFilter.ColumnFilterPopupMaxRecordsCount, false, true, null);
                if (values != null)
                {
                    comboBox.Items.AddRange(values);
                    column.ColumnEdit = comboBox;
                }
                DevExpress.XtraGrid.Filter.FilterCustomDialog dlg = new DevExpress.XtraGrid.Filter.FilterCustomDialog(column, false);
                dlg.ShowDialog();
                e.FilterInfo = null;
                e.Handled = true;
                switch (view.Name)
                {
                    case "companyGridView":
                        companyGridControl.Refresh();
                        break;
                    case "departmentGridView":
                        departmentGridControl.Refresh();
                        break;
                    case "costCentreGridView":
                        costCentreGridControl.Refresh();
                        break;
                    default:
                        quoteGridControl.Refresh();
                        break;
                }
                
            }
        }

        private void OpenEditForm(FormMode mode, string frmCaller, GridControl gridControl,BillingCodeType billingCentreCodeType)
        {

            if (mode == FormMode.blockadd || mode == FormMode.blockedit)
            {
                XtraMessageBox.Show("Block edit/add is not available.", "Block Edit/Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = null;
            //frmProgress fProgress = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            saveGridViewState();
            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (mode == FormMode.edit)
            {
                if (intCount <= 0)
                {
                    XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            updateSelectedBillingCentreCodeIDs();
                    
            switch (i_int_FocusedGrid)
            {
                case 0:     // Billing Centre Code
                    //frm_AS_Billing_Codes_Edit fChildForm = new frm_AS_Billing_Codes_Edit();
                    frm_AS_Billing_Centre_Edit fChildForm = new frm_AS_Billing_Centre_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strBillingCentreCodeRecordIDs;
                    fChildForm.formMode = (frm_AS_Billing_Centre_Edit.FormMode)mode;
                    fChildForm.strFormMode = (mode.ToString()).ToLower();
                    //if (mode == FormMode.add)
                    //{
                    //    fChildForm.MaximumSize = new Size(771, 184);
                    //}
                    fChildForm.strCaller = frmCaller;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager1 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();
                    break;
                case 1:     // company
                    frm_AS_Company_Edit cChildForm = new frm_AS_Company_Edit();
                    //cChildForm.MdiParent = this.MdiParent;
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strRecordIDs = strCompanyRecordIDs;
                    cChildForm.strFormMode = (mode.ToString()).ToLower();
                    cChildForm.formMode = (frm_AS_Company_Edit.FormMode)mode;
                    cChildForm.strCaller = frmCaller;
                    cChildForm.intRecordCount = intCount;
                    cChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager2 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager2;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.ShowDialog();
                    break;
                case 2:     // department 
                    frm_AS_Department_Edit dChildForm = new frm_AS_Department_Edit();
                    dChildForm.GlobalSettings = this.GlobalSettings;
                    dChildForm.strRecordIDs = strDepartmentRecordIDs;
                    dChildForm.strFormMode = (mode.ToString()).ToLower();
                    dChildForm.formMode = (frm_AS_Department_Edit.FormMode)mode;
                    dChildForm.strCaller = frmCaller;
                    dChildForm.intRecordCount = intCount;
                    dChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager3 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    dChildForm.splashScreenManager = splashScreenManager3;
                    dChildForm.splashScreenManager.ShowWaitForm();
                    dChildForm.ShowDialog();
                    break;
                case 3:     // costCentre 
                    frm_AS_Cost_Centre_Edit eChildForm = new frm_AS_Cost_Centre_Edit();
                    eChildForm.GlobalSettings = this.GlobalSettings;
                    eChildForm.strRecordIDs = strCostCentreRecordIDs;
                    eChildForm.strFormMode = (mode.ToString()).ToLower();
                    eChildForm.formMode = (frm_AS_Cost_Centre_Edit.FormMode)mode;
                    eChildForm.strCaller = frmCaller;
                    eChildForm.intRecordCount = intCount;
                    eChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager4 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    eChildForm.splashScreenManager = splashScreenManager4;
                    eChildForm.splashScreenManager.ShowWaitForm();
                    eChildForm.ShowDialog();
                    break;
                    
                default:
                    MessageBox.Show("Failed to load edit form", "Error Loading Edit form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void saveGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateBillingCentre.SaveViewInfo();  
            this.RefreshGridViewStateCompany.SaveViewInfo(); 
            this.RefreshGridViewStateDepartment.SaveViewInfo();
            this.RefreshGridViewStateCostCentre.SaveViewInfo();
        }

        private void loadGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStateBillingCentre.LoadViewInfo();
            this.RefreshGridViewStateCompany.LoadViewInfo();
            this.RefreshGridViewStateDepartment.LoadViewInfo();
            this.RefreshGridViewStateCostCentre.LoadViewInfo();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
     
        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                //LoadLastSavedUserScreenSettings();
            }
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            quoteGridControl.BeginUpdate();
          //  LoadAdapters();
            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item, "", "view");
           // this.RefreshGridViewStateBillingCentre.LoadViewInfo();  // Reload any expanded groups and selected rows //
            loadGridViewState();
            quoteGridControl.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)quoteGridControl.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BillingCentreCodeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.MakeRowVisible(intRowHandle, false);
                        view.SelectRow(intRowHandle);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private string stringRecords(int[] IDs)
        {
            string strIDs = "";
            foreach (int rec in IDs)
            {
                strIDs += Convert.ToString(rec) + ',';
            }
            return strIDs;
        }

        private void expandChildGridView()
        {
            GridView view = (GridView)companyGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)departmentGridControl.MainView;
            view.ExpandAllGroups();

            view = (GridView)costCentreGridControl.MainView;
            view.ExpandAllGroups();

        }

        #endregion
        
        
        #region CRUD

        #region CRUD Methods
           
        private void Add_Record()
        {
           
            switch (i_int_FocusedGrid)
            {
                case 0:     // Billing Centre Code //
                    if (!iBool_AllowAdd)
                        return;
                    OpenEditForm(FormMode.add, "frm_UT_Quote_Manager", quoteGridControl,BillingCodeType.BillingCentreCode);
                    break;
                case 1:     // company //
                    if (!iBool_AllowAddCompany) 
                        return;
                    OpenEditForm(FormMode.add, "frm_UT_Quote_Manager", companyGridControl,BillingCodeType.Company);                  
                    break;
                case 2:     // department //
                    if (!iBool_AllowAddDepartment) 
                        return;
                    OpenEditForm(FormMode.add, "frm_UT_Quote_Manager", departmentGridControl, BillingCodeType.Department );
                    break;
                case 3:     // costCentre //
                    if (!iBool_AllowAddCostCentre)
                        return;
                    OpenEditForm(FormMode.add, "frm_UT_Quote_Manager", costCentreGridControl,BillingCodeType.CostCentre);
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            XtraMessageBox.Show("Block adding is currently restricted the function is under construction.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void Block_Edit()
        {
            XtraMessageBox.Show("Block adding is restricted.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void Edit_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Billing Centre Code //
                    if (!iBool_AllowEdit)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", quoteGridControl, BillingCodeType.BillingCentreCode);
                    break;
                case 1:     // company //
                    if (!iBool_AllowEditCompany)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", companyGridControl, BillingCodeType.Company);
                    break;
                case 2:     // department //
                    if (!iBool_AllowEditDepartment)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", departmentGridControl, BillingCodeType.Department);
                    break;
                case 3:     // costCentre //
                    if (!iBool_AllowEditCostCentre)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", costCentreGridControl, BillingCodeType.CostCentre);
                    break;
                default:
                    break;
            }
            
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";


            switch (i_int_FocusedGrid)
            {
                case 0:     // Billing Centre Code
                    if (!iBool_AllowDelete)
                        return;
                    break;
                case 1:     // company
                    if (!iBool_AllowDeleteCompany)
                        return;
                    break;
                case 2:     // department 
                    if (!iBool_AllowDeleteDepartment)
                        return;
                    break;
                case 3:     // costCentre 
                    if (!iBool_AllowDeleteCostCentre)
                        return;
                    break;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }

            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2, out strMessage3);

            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
                                        
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to delete.", "No Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) + 
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") + 
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateBillingCentre.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStateBillingCentre.SaveViewInfo();  // Store Grid View State //
                try
                {
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Billing Centre Code
                            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Delete("delete", strBillingCentreCodeRecordIDs);
                            break;
                        case 1:     // company
                            sp_AS_11057_Company_ItemTableAdapter.Delete("delete", strCompanyRecordIDs);
                            break;
                        case 2:     // department 
                            sp_AS_11060_Department_ItemTableAdapter.Delete("delete", strDepartmentRecordIDs);
                            break;
                        case 3:     // costCentre 
                            sp_AS_11063_Cost_Centre_ItemTableAdapter.Delete("delete", strCostCentreRecordIDs);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);           
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2, out string strMessage3)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Billing Centre Code
                    gridControl = quoteGridControl;                    
                    strMessage1 = "1 Billing Centre Code record";
                    strMessage2 = " Billing Centre Code records";
                    strMessage3 = "";
                    break;
                case 1://company
                    gridControl = companyGridControl;                  
                    strMessage1 = "1 Company record";
                    strMessage2 = " Company records";
                    strMessage3 = "";
                    break;
                case 2://department
                    gridControl = departmentGridControl;
                    
                    strMessage1 = "1 Department record";
                    strMessage2 = " Department records";
                    strMessage3 = "";
                    break;
                case 3://costCentre
                    gridControl = costCentreGridControl;
                    
                    strMessage1 = "1 CostCentre record";
                    strMessage2 = " CostCentre records";
                    strMessage3 = "";
                    break;
                default:
                    gridControl = quoteGridControl;
                    strMessage1 = "1 Billing Centre Code record";
                    strMessage2 = " Billing Centre Code records";
                    strMessage3 = "";
                    break;
            }
        }

        private void getCurrentGridControl(out GridControl gridControl)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Billing Centre Code
                    gridControl = quoteGridControl;
                    break;
                case 1://company
                    gridControl = companyGridControl;
                    break;
                case 2://department
                    gridControl = departmentGridControl;
                    break;
                case 3://costCentre
                    gridControl = costCentreGridControl;
                    break;
                default:
                    gridControl = quoteGridControl;
                    break;
            }
        }

        private void View_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Billing Centre Code //
                    OpenEditForm(FormMode.view, "frm_UT_Quote_Manager", quoteGridControl, BillingCodeType.BillingCentreCode);
                    break;
                case 1:     // company //
                    if (!iBool_AllowEditCompany)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", companyGridControl, BillingCodeType.Company);
                    break;
                case 2:     // department //
                    if (!iBool_AllowEditDepartment)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", departmentGridControl, BillingCodeType.Department);
                    break;
                case 3:     // costCentre //
                    if (!iBool_AllowEditCostCentre)
                        return;
                    OpenEditForm(FormMode.edit, "frm_UT_Quote_Manager", costCentreGridControl, BillingCodeType.CostCentre);
                    break;
            }
            
        }

        #endregion

        #region CRUD Events


        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        #endregion

        #endregion




    }
}


using System;
using System.Collections;
using System.Collections.Generic;  // Required for List call //
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;
using MapInfo.Tools;
using MapInfo.Windows.Controls;
using MapInfo.Windows.Dialogs;
using MapInfo.Styles;
using MapInfo.Data;
using MapInfo.Mapping.Thematics;
using MapInfo.Mapping.Legends;

using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;  // Required for GraphicsPath call on Drawing Legend //
using System.Drawing.Imaging;  // Required for ImageMap Call on Drawing Legend //

using System.Reflection;  // Required by GridViewFiltering //

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;  // Used for adding menu items on-the-fly for StoredGridViews //
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.XtraBars.Docking;

using WoodPlan5.Properties;
using Utilities;
using BaseObjects;
using GPS;
using LocusEffects;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intLoadedWorkspaceID = 0;
        public int intLoadedWorkspaceOwner = 0;
        public string strLoadedWorkspaceName = "";

        public string strCircuitIDs = "";  // Used to filter list of available map objects //
        public string strCircuitNames = "";
        public string strSiteIDs = "";  // Used to filter list of available map objects //
        public string strAssetTypeIDs = "";  // Used to filter list of available map objects //
        public string strVisibleIDs = "";  // Used to control which MapObjects are intially visible on the map after loading //
        public string strVisibleTreesIDs = "";  // Used to control which MapObjects are intially visible on the map after loading //

        public string i_str_selected_workorders = "";  // Passed through to WorkOrder Mapping if this screen is called from WorkOrders //
        public string strHighlightedIDs = "";  // Used to control which MapObjects are initailly highlighted on the map after loading //
        public string strHighlightedAssetIDs = "";  // Used to control which MapObjects are initailly highlighted on the map after loading //
        public int intInitialHighlightColour = -10185235;  // [Default CornflowerBlue] Used to control the Initial Highlighter colour on the map after loading //
        public int intPassedMapX = 0;  // Passed in from Mapping_Functions when no tres have been selected for highlighting so the map can centre on the passed in X //
        public int intPassedMapY = 0;  // Passed in from Mapping_Functions when no tres have been selected for highlighting so the map can centre on the passed in Y //
        private bool PreventDefaultMapScaleUse = false;  // This is set to true temporarily if highlighting and map scaling from passed parameters is used //
        public bool NoLocusEffectOnStart = false;

        private string strMappingFilesPath = "";
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selectionTrees;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        string i_str_tooltip_structure;
        string i_str_label_structure;
        bool i_bool_FullExtentFired = false;  // Used to stop popupContainerEdit2 firing QueryResultValue when it's Full Extent button clicked //
        RepositoryItem emptyEditor;  // Used to conditionally hide the Label Seperator Text editor if New Line Checkbox is ticked //
        private int intMapObjectSizing = 8;  // Value 6 to 20 or -1 for dynamic sizing based on Crown Diameter //
        public static GPSHandler GPS;

        private string i_str_Polygon_Area_Measurement_Unit = "M�";
        private MapInfo.Geometry.AreaUnit i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.SquareMeter;
        private int i_int_GPS_CrossHair_Colour = -65536;  // Red //
        private int i_int_GPS_Polygon_Line_Plot_Colour = -16776961;  // Blue //
        private int i_int_GPS_Polygon_Plot_Line_Style = 2;  // Solid //
        private int i_str_GPS_Polygon_Plot_Line_Width = 1;
        private Timer GPSTimer = null;
        private bool boolLogGPS = false;

        MouseHook mouseHook = new MouseHook();  // Hooks for Mouse Events //
        KeyboardHook keyboardHook = new KeyboardHook();  // Hooks for Keyboard Events //

        double dMeasuredDistance = (double)0.00;
        double dCurrentMapScale = (double)0.00;  // Used as a Kludge as the MapViewChanged Event can't tell between Map Panned and Map Zoomed //

        SqlDataAdapter sdaDefaultStyles = new SqlDataAdapter();
        DataSet dsDefaultStyles = new DataSet("NewDataSet");
        SqlDataAdapter sdaThematicStyleItems = new SqlDataAdapter();
        DataSet dsThematicStyleItems = new DataSet("NewDataSet");
        SqlDataAdapter sdaThematicStyleSets = new SqlDataAdapter();
        DataSet dsThematicStyleSets = new DataSet("NewDataSet");

        //SqlDataAdapter sdaFontSettings = new SqlDataAdapter();
        //DataSet dsFontSettings = new DataSet("NewDataSet");
        //SqlDataAdapter sdaLabelStructure = new SqlDataAdapter();
        //DataSet dsLabelStructure = new DataSet("NewDataSet");
        public int _Transparency = 0;
        public int _LineColour = 0;
        public int _LineWidth = 1;
        public string _LabelFontName = "Tahoma";
        public int _LabelFontSize = 8;
        public int _LabelFontColour = 0;
        public int _LabelAngle = 0;
        public string _LabelPosition = "CentreRight";
        public int _LabelOffset = 6;
        public int _LabelVisibleRangeFrom = 0;
        public int _LabelVisibleRangeTo = 10000;
        public int _LabelOverlap = 1;
        public int _LabelDuplicates = 1;

        int intThematicStylingFieldHeaderID = 0;
        int intThematicStylingFieldHeaderType = 0;  // 0 = System Picklist, 1 = Saved Thematic Style Set //
        int intLoadedStyleSetOwnerID = 0;
        int intThematicStylingBasePicklistID = 0;
        int intUseThematicStyling = 0;
        string strThematicStylingFieldName = "";  // Used by the Legend //
        int intMapObjectTransparency = 0;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private BeaconLocusEffect m_customBeaconLocusEffect1 = null;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        AutoScrollHelper autoScrollHelper;

        private bool boolQueryToolInUse = false;  // Used to get round MapInfo bug which calls the custom query tool twice //

        private DataSet_Selection DS_Selection1;

        private int intStrPolygonXYMaxLength = 32000; //255;

        private decimal decCurrentMouseX = 0;  // Used when setting district \ locality centre point //
        private decimal decCurrentMouseY = 0;  // Used when setting district \ locality centre point //
        private bool boolTrackXandY = true;  // Used when setting district \ locality centre point //

        private List<Mapping_View> listStoredMapView = new List<Mapping_View>();
        private bool boolStoreViewChanges = true;
        private bool boolStoreViewChangesTempDisable = false;  // used to temporarily stop the map view being logeed when the user selects a stored view otherwise it would be logged again //
        private int intCurrentViewID = 0;

        int intTreeRefStructureType = 0;
        string strTreeRefStructureStripPattern = "";
        int intTreeRefStructureStripNumber = 0;

        private MapInfo.Styles.BitmapPointStyle bitmapPointStyle1;

        private int intGazetteerSearchCircleColour = -65536;  // Red //
        private int intGazetteerSearchCircleTransparency = 75;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr WindowFromPoint(System.Drawing.Point pnt); // Used by mouseHook_MouseUp event to get current control under Mouse Pointer //

        private string strDefaultMappingProjection = "";

        private decimal decDefaultHedgeWidth = (decimal)0.00;

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        bool iBoolDontFireGridGotFocusOnDoubleClick = false;
        private int i_int_FocusedGrid = 1;
        int i_intMapObjectsSelected = 0;  // Used to switch on and off Add buttons in Linked Inspections and Actions grid - set in FeatureSelected Event //
        public int UpdateRefreshStatus = 0; // Controls if grids needs to refresh themselves on activate when a child screen has updated their data //
        string i_str_AddedRecordIDs1 = "";
        string i_str_AddedRecordIDs2 = "";
        string i_str_AddedRecordIDs3 = "";
        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;

        public int i_intCallingModule = 0;
        int i_intSelectedObjectType = 0;

        // Survey Vars //
        public int _SelectedSurveyID = 0;
        public int _SelectedSurveyClientID = 0;
        public string _SelectedSurveyDescription = "";
        public string _PassedInRegionIDs = "";  // Passed in from Calling screen //
        public string _PassedInClientIDs = "";  // Passed in from Calling screen //
        public string _SurveyMode = "";
        public int _CurrentPoleID = 0;  // Used for plotting trees from surveys //
        public int _PoleThematicTypeID = 0;  // 0 = Survey StatusID, 1 = Shutdown required, 2 = Traffic Management Required //
        public int _TreeThematicTypeID = 0;  // -1 = None, 0 = Permissioned //

        private Color colourPoleLegend1 = Color.DimGray;
        private Color colourPoleLegend2 = Color.FromArgb(255, 128, 0);
        private Color colourPoleLegend3 = Color.Yellow;
        private Color colourPoleLegend4 = Color.DodgerBlue;
        private Color colourPoleLegend5 = Color.Red;
        private Color colourPoleLegend6 = Color.LimeGreen;
        private Color colourPoleLegend7 = Color.DarkOrchid;

        private Color colourTreeLegend1 = Color.DimGray;
        private Color colourTreeLegend2 = Color.LimeGreen;
        private Color colourTreeLegend3 = Color.Red;
        private Color colourTreeLegend4 = Color.FromArgb(255, 128, 0);
        private Color colourTreeLegend5 = Color.DodgerBlue;

        // Permission Document Map Vars //
        public string _PassedInTreeIDs = "";
        public string _PassedInPoleIDs = "";
        public int _PassedInPermissionDocumentID = 0;
        public string _PassedInPermissionDocumentDescription = "";
        public int _PassedInRecordType = 0;  // 1 = Permission Document, 2 = Work Order //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //

        public bool boolFormOpening = true;  // Prevents certain code firing while form is loading //
        #endregion

        public frm_UT_Mapping(int WorkSpaceID, int LoadedWorkspaceOwner, string LoadedWorkspaceName, int CallingModule)
        {
            InitializeComponent();

            intLoadedWorkspaceID = WorkSpaceID;
            intLoadedWorkspaceOwner = LoadedWorkspaceOwner;
            strLoadedWorkspaceName = LoadedWorkspaceName;
            i_intCallingModule = CallingModule;

            // Assign the Pan tool to the middle mouse button
            mapControl1.Tools.MiddleButtonTool = "Pan";

            // Listen to map events //
            mapControl1.Map.ViewChangedEvent += new ViewChangedEventHandler(Map_ViewChanged);
            mapControl1.Map.Layers.Added += new CollectionEventHandler(Layers_CountChanged);
            mapControl1.Map.Layers.Removed += new CollectionEventHandler(Layers_CountChanged);

            // Listen to tool events //
            mapControl1.Tools.FeatureAdding += new FeatureAddingEventHandler(FeatureAdding);
            //mapControl1.Tools.FeatureAdded += new FeatureAddedEventHandler(FeatureAdded);
            mapControl1.Tools.FeatureChanging += new FeatureChangingEventHandler(FeatureChanging);
            mapControl1.Tools.FeatureChanged += new MapInfo.Tools.FeatureChangedEventHandler(FeatureChanged);
            //mapControl1.Tools.FeatureSelecting += new FeatureSelectingEventHandler(FeatureSelecting);
            mapControl1.Tools.FeatureSelected += new FeatureSelectedEventHandler(FeatureSelected);
            mapControl1.Tools.NodeChanging += new NodeChangingEventHandler(Tools_NodeChanging);
            mapControl1.Tools.NodeChanged += new NodeChangedEventHandler(Tools_NodeChanged);

            mapControl1.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseWheel);
            mapControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseMove);

            mapControl1.GotFocus +=new EventHandler(mapControl1_GotFocus);

            // *** Enable translucency and anti-aliasing (anti-aliasing is optional) *** //
            mapControl1.Map.DrawingAttributes.EnableTranslucency = true;
            //mapControl1.Map.DrawingAttributes.SmoothingMode = MapInfo.Mapping.SmoothingMode.AntiAlias;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            //*** Register Custom Measuring Tool ***//
            MapInfo.Tools.IMouseToolProperties mp = mapControl1.Tools.MouseToolProperties;
            mp.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            mp.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");  // Holding Down Shift puts in right angles or controlled percentages of an angle //
            mp.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            WoodPlan5.PSGDistanceTool psgDistTool = new WoodPlan5.PSGDistanceTool(true, true, true, mapControl1.Viewer, mapControl1.Handle.ToInt32(), mapControl1.Tools, mp, mapControl1.Tools.MapToolProperties);
            // Following stops subsequent Query Tool from setting this tool to it's cursor //
            psgDistTool.UseDefaultCursor = false;
            psgDistTool.UseDefaultControlCursor = false;
            psgDistTool.UseDefaultShiftCursor = false;
            psgDistTool.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            psgDistTool.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            psgDistTool.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Measurer.cur");
            // End of... Following stops subsequent Query Tool from setting this tool to it's cursor //
            mapControl1.Tools.Add("Distance", psgDistTool);  // Add the tool to the tools collection "Distance" is the same as the ToolId for the mapToolBarButtonDistance //
            psgDistTool.PSGDistanceToolPointAdded += new WoodPlan5.PSGDistanceToolPointAddedEventHandler(psgDistTool_PSGDistanceToolPointAdded);  // Register events for measuring tool //
            psgDistTool.PSGDistanceToolMove += new WoodPlan5.PSGDistanceToolPointAddedEventHandler(psgDistTool_PSGDistanceToolMove);  // Register events for measuring tool //

            // *** Register Custom Query Tool *** //
            MapInfo.Tools.IMouseToolProperties mp2 = mapControl1.Tools.MouseToolProperties;
            mp2.Cursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");
            mp2.ShiftCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");  // Holding Down Shift puts in right angles or controlled percentages of an angle //
            mp2.ControlCursor = new System.Windows.Forms.Cursor(Application.StartupPath + "\\Map_Query.cur");
            MapInfo.Tools.MapTool ptMapTool = new MapInfo.Tools.CustomPointMapTool(false, this.mapControl1.Tools.FeatureViewer, this.mapControl1.Handle.ToInt32(), this.mapControl1.Tools, mp2, this.mapControl1.Tools.MapToolProperties);
            ptMapTool.UseDefaultCursor = false;
            this.mapControl1.Tools.Add("InfoTool", ptMapTool);

            this.mapControl1.Tools.Used += new MapInfo.Tools.ToolUsedEventHandler(Tools_Used);

            GPS = new GPSHandler(this); //Initialize GPS handler
            GPS.TimeOut = 5; //Set timeout to 5 seconds
            GPS.NewGPSFix += new GPSHandler.NewGPSFixHandler(this.GPSEventHandler); //Hook up GPS data events to a handler

            GPSTimer = new Timer();
            GPSTimer.Interval = 5000;
            GPSTimer.Tick += new EventHandler(GPSTimer_Tick);

            // Define MapMarker Point Icon (bitmap) //
            bitmapPointStyle1 = new MapInfo.Styles.BitmapPointStyle("PushPin_Red_16.bmp");
            MapInfo.Styles.StyleRepository sr = MapInfo.Engine.Session.Current.StyleRepository;
            String path = Application.StartupPath + "\\";
            MapInfo.Styles.BitmapPointStyleRepository bpsr = sr.BitmapPointStyleRepository;
            bpsr.Reload(path);

            //loading.Close();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        MapInfo.Geometry.DPoint ptMouseDown;
        private void Tools_Used(object sender, MapInfo.Tools.ToolUsedEventArgs e)
        {
            switch (e.ToolName.ToString())
            {
                case "Measurer":
                    switch (e.ToolStatus)
                    {
                        case MapInfo.Tools.ToolStatus.Start:
                            ptMouseDown = e.MapCoordinate;
                            break;
                        case MapInfo.Tools.ToolStatus.End:
                            MapInfo.Geometry.DRect Rect = new MapInfo.Geometry.DRect(ptMouseDown, e.MapCoordinate);
                            break;
                    }
                    break;
                case "InfoTool":
                    CustomQueryInfo(e);
                    break;
            }
        }

        private void frm_UT_Mapping_Load(object sender, EventArgs e)
        {
            this.FormID = 10003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;

            frmMain2 fMain = (frmMain2)this.MdiParent;
            if (fMain != null)
            {
               DevExpress.XtraBars.Docking2010.DocumentManager dm = (DevExpress.XtraBars.Docking2010.DocumentManager)fMain.documentManager1;
               dm.View.DocumentProperties.AllowFloat = false;
            }

            dockManager1.ActivePanel = dockPanelLayers;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "2004", GlobalSettings.ViewedPeriodID);

            sp07049_UT_Tree_Picker_PolesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "PoleID");

            sp07053_UT_Tree_Picker_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridViewTrees, "TreeID");

            if (this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count > 0)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.blRead = true;
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["CreateAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[0]["DeleteAccess"]);
                iBool_AllowAdd = sfpPermissions.blCreate;
                iBool_AllowEdit = sfpPermissions.blUpdate;
                iBool_AllowDelete = sfpPermissions.blDelete;
                ArrayList alPermissions = new ArrayList();
                alPermissions.Add(sfpPermissions);
                this.FormPermissions = alPermissions;

                bbiAddPoint.Enabled = iBool_AllowAdd;
                bbiAddPolygon.Enabled = iBool_AllowAdd;
                bbiAddPolyLine.Enabled = iBool_AllowAdd;
                btnGPS_Start.Enabled = iBool_AllowAdd;
            }

            // Get default Polygon Area Measurement Unit from System_Settings table //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            i_str_Polygon_Area_Measurement_Unit = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Polygon_Area"));
            switch (i_str_Polygon_Area_Measurement_Unit)
            {
                case "M�":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.SquareMeter;
                    break;
                case "Hectares":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.Hectare;
                    i_str_Polygon_Area_Measurement_Unit = "Ha";
                    break;
                case "Acres":
                    i_AreaUnit_Polygon_Area_Measurement_Unit = MapInfo.Geometry.AreaUnit.Acre;
                    break;
                default:
                    break;
            }

            strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";

            string strDefaultHedgeWidth = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Default_Hedge_Width").ToString();
            decDefaultHedgeWidth = (string.IsNullOrEmpty(strDefaultHedgeWidth) ? (decimal)1.00 : Convert.ToDecimal(strDefaultHedgeWidth));

            colorEditHighlight.EditValue = intInitialHighlightColour;

            // Get default GPS CrossHair Colour from System_Settings table //
            i_int_GPS_CrossHair_Colour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_CrossHair_Colour"));

            // Get default GPS Polygon Plot Line Colour from System_Settings table //
            i_int_GPS_Polygon_Line_Plot_Colour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Line_Plot_Colour"));

            // Get default GPS Polygon Plot Line Style from System_Settings table //
            i_int_GPS_Polygon_Plot_Line_Style = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Style"));

            // Get default GPS Polygon Plot Line Width from System_Settings table //
            i_str_GPS_Polygon_Plot_Line_Width = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_GPS_Polygon_Plot_Line_Width"));

            // Add record visibility checkboxes to pole object grid //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 70; // Make wider to allow for caption in header //
            selection1.CheckMarkColumn.Caption = "     Visible";  // Spaces at start so room for checkbox in front //
            selection1.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selection1.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selection1.CheckMarkColumn.ToolTip = "Controls which Map Objects are Loaded into the map.";
            selection1.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selection1.editColumnHeader.Caption = "";
            selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;

            emptyEditor = new RepositoryItem();  // Used to conditionally hide the Label Seperator Text editor if New Line Checkbox is ticked //
            gridControl2.RepositoryItems.Add(emptyEditor);

            gridControl2.ForceInitialize();

            // Add record visibility checkboxes to tree object grid //
            selectionTrees = new BaseObjects.GridCheckMarksSelection((GridView)gridControl6.MainView);
            selectionTrees.CheckMarkColumn.VisibleIndex = 0;
            selectionTrees.CheckMarkColumn.Width = 70; // Make wider to allow for caption in header //
            selectionTrees.CheckMarkColumn.Caption = "     Visible";  // Spaces at start so room for checkbox in front //
            selectionTrees.CheckMarkColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far;
            selectionTrees.CheckMarkColumn.OptionsColumn.ShowCaption = true;
            selectionTrees.CheckMarkColumn.ToolTip = "Controls which Map Objects are Loaded into the map.";
            selectionTrees.editColumnHeader.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            selectionTrees.editColumnHeader.Caption = "";
            selectionTrees.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            selectionTrees.CheckMarkColumn.VisibleIndex = 0;

            if (string.IsNullOrEmpty(_PassedInTreeIDs))  // Permission Documents or Work Orders //
            {
                Load_Map_Objects_Grid();  // Populate Map Objects //
            }
            tabbedControlGroup1.SelectedTabPageIndex = 0;

            // Populate list of available plottable object types on the toolbar - Check which object types to work with from System_Settings table //
            sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07048_UT_Tree_Picker_Plotting_Object_Types_With_BlankTableAdapter.Fill(dataSet_UT_Mapping.sp07048_UT_Tree_Picker_Plotting_Object_Types_With_Blank, 1, 1);
            beiPlotObjectType.EditValue = -1;  // Poles //

            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01254_AT_Tree_Picker_scale_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01254_AT_Tree_Picker_scale_list);
            gridControl4.ForceInitialize();

            // *** Gazetteer *** //
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp01311_AT_Tree_Picker_Gazetteer_Search_TypesTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types);
            if (this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows.Count > 1)
            {
                lookUpEditGazetteerSearchType.EditValue = this.dataSet_AT_TreePicker.sp01311_AT_Tree_Picker_Gazetteer_Search_Types.Rows[1]["Description"].ToString();
            }
            buttonEditGazetteerFindValue.EditValue = "";
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl3.MainView = gridView3;

            // Get default value for Gazetteer Search Radius on Menu //
            string strGazetteerSearchRadius = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius").ToString();
            beiGazetteerLoadObjectsWithinRange.EditValue = (string.IsNullOrEmpty(strGazetteerSearchRadius) ? (decimal)50.00 : Convert.ToDecimal(strGazetteerSearchRadius));
            // Get back colour for Gazetteer Search Area //
            string strGazetteerSearchCircleColour = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Colour").ToString();
            intGazetteerSearchCircleColour = (string.IsNullOrEmpty(strGazetteerSearchCircleColour) ? (int)-65536 : Convert.ToInt32(strGazetteerSearchCircleColour));
            // Get transparency level for Gazetteer Search Area //
            string strGazetterSearchCircleTransparency = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTrees_TreePicker_Gazetteer_Search_Radius_Transparency").ToString();
            intGazetteerSearchCircleTransparency = (string.IsNullOrEmpty(strGazetterSearchCircleTransparency) ? (int)-65536 : Convert.ToInt32(strGazetterSearchCircleTransparency));
            // *** End of Gazetteer *** //

            // Get Mapping Background Files Path Location //
            //strMappingFilesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
            strMappingFilesPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();
            if (strMappingFilesPath == "")
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Load Map - No Mapping Background Folder Location has been specified in the System Settings table.", "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.UnlockThisWindow();  // ***** Locked in Load event ***** //
                Close();
            }
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Connection.ConnectionString = strConnectionString;  // Holds the Mapping layers to load //
            gridControl5.ForceInitialize();
            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Connection.ConnectionString = strConnectionString;  // Holds the Mapping Workspaces properties //

            #region Load Default Styles and Thematics
            int intPickListID = -999;  // Set to -999 so nothing is initially retrieved //
            int intPickListType = 0;
            int intDefaultSymbol = 0;
            int DefaultSymbolColour = 0;
            int DefaultSymbolSize = 0;
            int DefaultPolygonBoundaryColour = 0;
            int DefaultPolygonBoundaryWidth = 0;
            int DefaultPolygonFillColour = 0;
            int DefaultPolygonFillPattern = 0;
            int DefaultPolygonFillPatternColour = 0;
            int DefaultLineSymbol = 0;
            int DefaultLineColour = 0;
            int DefaultLineWidth = 0;
            int intDefaultSymbol2 = 0;
            int DefaultSymbol2Colour = 0;
            int DefaultSymbol2Size = 0;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp07054_UT_Tree_Picker_Thematic_Mapping_Default_Dummy", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", DefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", DefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", DefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", DefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", DefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", DefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", DefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", DefaultLineSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", DefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", DefaultLineWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol2", intDefaultSymbol2));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol2Colour", DefaultSymbol2Colour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol2Size", DefaultSymbol2Size));
            sdaDefaultStyles = new SqlDataAdapter(cmd);
            sdaDefaultStyles.Fill(dsDefaultStyles, "Table");

            int intCurrentUser = this.GlobalSettings.UserID;
            int intCurrentUserType = this.GlobalSettings.PersonType;
            cmd = null;
            cmd = new SqlCommand("sp01256_AT_Tree_Picker_Thematic_Mapping_Available_Sets", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@intUserID", intCurrentUser));
            cmd.Parameters.Add(new SqlParameter("@intUserType", intCurrentUserType));

            sdaThematicStyleSets = new SqlDataAdapter(cmd);
            sdaThematicStyleSets.Fill(dsThematicStyleSets, "Table");

            cmd = null;
            cmd = new SqlCommand("sp01255_AT_Tree_Picker_Thematic_Mapping_Default_Items", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PickListID", intPickListID));
            cmd.Parameters.Add(new SqlParameter("@PicklistType", intPickListType));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", DefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", DefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", DefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", DefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", DefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", DefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", DefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", DefaultLineSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", DefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", DefaultLineWidth));
            sdaThematicStyleItems = new SqlDataAdapter(cmd);
            sdaThematicStyleItems.Fill(dsThematicStyleItems, "Table");
            dsThematicStyleItems.Clear();  // Remove any values as the user will need to choose thematic criteria from the child Thematic Styling screen first //

            SQlConn.Close();
            SQlConn.Dispose();
            #endregion

            // Prepare LocusEffects and add custom effect //
            locusEffectsProvider1 = new LocusEffectsProvider();
            locusEffectsProvider1.Initialize();
            locusEffectsProvider1.FramesPerSecond = 30;
            m_customBeaconLocusEffect1 = new BeaconLocusEffect();
            m_customBeaconLocusEffect1.Name = "CustomBeacon1";
            m_customBeaconLocusEffect1.InitialSize = new Size(150, 150);
            m_customBeaconLocusEffect1.AnimationTime = 1500;
            m_customBeaconLocusEffect1.LeadInTime = 0;
            m_customBeaconLocusEffect1.LeadOutTime = 0;
            m_customBeaconLocusEffect1.AnimationStartColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationEndColor = Color.Red;
            m_customBeaconLocusEffect1.AnimationOuterColor = Color.Salmon;
            m_customBeaconLocusEffect1.RingWidth = 6; // 6;
            m_customBeaconLocusEffect1.OuterRingWidth = 3;// 3;
            m_customBeaconLocusEffect1.BodyFadeOut = true;
            m_customBeaconLocusEffect1.Style = BeaconEffectStyles.Shrink;//.HeartBeat;
            locusEffectsProvider1.AddLocusEffect(m_customBeaconLocusEffect1);

            m_customArrowLocusEffect1 = new ArrowLocusEffect();
            m_customArrowLocusEffect1.Name = "CustomeArrow1";
            m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
            m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
            m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
            m_customArrowLocusEffect1.MovementCycles = 20;
            m_customArrowLocusEffect1.MovementAmplitude = 200;
            m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
            m_customArrowLocusEffect1.LeadInTime = 0; //msec
            m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
            m_customArrowLocusEffect1.AnimationTime = 2000; //msec
            locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

            Load_Layer_Manager();

            LoadGPSComPortList();

            // *** Hoook into Mouse and Keyboard Events *** //
            mouseHook.MouseMove += new MouseEventHandler(mouseHook_MouseMove);
            mouseHook.MouseDown += new MouseEventHandler(mouseHook_MouseDown);
            mouseHook.MouseUp += new MouseEventHandler(mouseHook_MouseUp);
            mouseHook.MouseWheel += new MouseEventHandler(mouseHook_MouseWheel);
            keyboardHook.KeyDown += new KeyEventHandler(keyboardHook_KeyDown);
            keyboardHook.KeyUp += new KeyEventHandler(keyboardHook_KeyUp);
            keyboardHook.KeyPress += new KeyPressEventHandler(keyboardHook_KeyPress);
            // Mouse and Keyboard Hooks started in mapControl1_Enter event //

            mapControl1.Visible = true;
            autoScrollHelper = new AutoScrollHelper(gridView5);  // Add Autoscroll functionality when dragging to adjust record order //
            gridControl5.AllowDrop = true;

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView2, strConnectionString);  // Trees List //

        }

        public override void PostOpen(object objParameter)
        {
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

            // Check if we are in survey mode - if yes then configure map accordingly //
            if (_SelectedSurveyID > 0)
            {
                buttonEditSurvey.Text = _SelectedSurveyDescription;
                panelContainer1.ActiveChild = dockPanelMapObjects;
 
                GridView view = (GridView)gridControl2.MainView;
                int intLength = view.DataRowCount;
                if (intLength > 0 && !(_SurveyMode == "PermissionDocumentMap" || _SurveyMode == "WorkOrderMap")) // Poles loaded //
                {
                    // Tick Visible for any loaded pole objects //
                    view.BeginUpdate();
                    selection1.SelectAll();
                    view.ExpandAllGroups();
                    view.EndUpdate();

                    // Load Linked Trees into Tree Object grid and tick visible //
                    gridControl6.ForceInitialize();
                    Load_Tree_Objects_Grid();  // BeginUpdate and EndUpdate for view6 are done inside this event //
                    view = (GridView)gridControl6.MainView;
                    intLength = view.DataRowCount;
                    if (intLength > 0)  // Trees loaded //
                    {
                        view.BeginUpdate();
                        selectionTrees.SelectAll();
                        view.ExpandAllGroups();
                        view.EndUpdate();
                    }

                    // Load Poles and Trees into Map //
                    UpdateMapObjectsDisplayed(true);

                    // Centre Map on the pole which was last added to the survey (get highest SurveyedPoleID from DB) //
                    string strLastMapID = "";
                    DataSet_UT_MappingTableAdapters.QueriesTableAdapter getLastMapID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                    getLastMapID.ChangeConnectionString(strConnectionString);
                    try
                    {
                        strLastMapID = getLastMapID.sp07112_UT_Mapping_Survey_Get_Last_Added_Pole(_SelectedSurveyID).ToString();
                    }
                    catch (Exception)
                    {
                    }
                    if (!string.IsNullOrEmpty(strLastMapID) && _SurveyMode == "AddPolesToSurvey")
                    {
                        System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                        double MinX = (double)9999999.00;  // Used for Zooming to Scale [Not used here but required for function] //
                        double MinY = (double)9999999.00;
                        double MaxX = (double)-9999999.00;
                        double MaxY = (double)-9999999.00;
                        FindAndHighlight(strLastMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                }
                else if (!string.IsNullOrEmpty(_PassedInTreeIDs))  // Permission Documents or Work Orders for Map Snapshot //
                {
                    panelContainer1.ActiveChild = dockPanelMapObjects;

                    // Attempt to focus Tree Object Page //
                    layoutControl1.GetItemByControl(btnRefreshTrees);
                    DevExpress.XtraLayout.LayoutControlItem item = layoutControl1.GetItemByControl(btnRefreshTrees);
                    if (item != null)
                    {
                        layoutControl1.FocusHelper.PlaceItemIntoView(item);
                        layoutControl1.FocusHelper.FocusElement(item, true);
                    }
                    _TreeThematicTypeID = -1;  // No Tree Thematics //
                    string strLastMapID = "";
                    // Load Linked Trees into Tree Object grid and tick visible //
                    gridControl6.ForceInitialize();
                    Load_Tree_Objects_Grid();  // BeginUpdate and EndUpdate for view6 are done inside this event //
                    view = (GridView)gridControl6.MainView;
                    intLength = view.DataRowCount;
                    if (intLength > 0)  // Trees loaded //
                    {
                        view.BeginUpdate();
                        selectionTrees.SelectAll();
                        view.ExpandAllGroups();
                        view.EndUpdate();
                        strLastMapID = view.GetRowCellValue(view.DataRowCount - 1, "MapID").ToString();
                    }

                    if (!string.IsNullOrEmpty(_PassedInPoleIDs))
                    {
                        Load_Map_Objects_Grid();
                        view = (GridView)gridControl2.MainView;
                        intLength = view.DataRowCount;
                        if (intLength > 0)  // Trees loaded //
                        {
                            view.BeginUpdate();
                            selection1.SelectAll();
                            view.ExpandAllGroups();
                            view.EndUpdate();
                        }
                    }

                    UpdateMapObjectsDisplayed(true);  // Load Trees into Map //

                    // Centre Map on the tree which was last added to the survey (get highest SurveyedPoleID from DB) //
                    if (!string.IsNullOrEmpty(strLastMapID))
                    {
                        System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                        double MinX = (double)9999999.00;  // Used for Zooming to Scale [Not used here but required for function] //
                        double MinY = (double)9999999.00;
                        double MaxX = (double)-9999999.00;
                        double MaxY = (double)-9999999.00;
                        FindAndHighlight(strLastMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                }

                // If screen fired from WorkOrders or Permission Documents - Highlight the Map Linking button on the toolbar //
                if (!string.IsNullOrEmpty(i_str_selected_workorders) || _PassedInPermissionDocumentID != 0)
                {
                    if (!NoLocusEffectOnStart)
                    {
                        // Get BarButtons Location //
                        System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                        foreach (BarItemLink link in bar4.ItemLinks)
                        {
                            if (link.Item.Name == "bbiWorkOrderMap")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available) //
                            {
                                rect = GetLinksScreenRect(link);
                                break;
                            }
                        }
                        if (rect.X > 0 && rect.Y > 0)
                        {
                            // Draw Locus Effect on object so user can see it //
                            System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                            locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                        }
                    }
                    else
                    {
                        NoLocusEffectOnStart = false;
                    }
                }

                if (_SurveyMode == "AddPolesToSurvey")
                {
                    bbiSelectRectangle.Checked = true;
                    mapControl1.Tools.LeftButtonTool = "SelectRect";
                    Open_Choose_Circuits_Screen();
                    bbiLegend.Checked = true;
                    bciSurvey.Checked = true;  // Show Survey Panel //
                }
                else if (_SurveyMode == "PlotTrees")
                {
                    bbiAddPoint.Checked = true;
                    mapControl1.Tools.LeftButtonTool = "AddPoint";
                    beiPlotObjectType.EditValue = 2;  // Trees //
                    bbiLegend.Checked = true;
                    bciSurvey.Checked = true;  // Show Survey Panel //

                    if (_CurrentPoleID <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select the pole to add the mapped trees to by clicking on it in either the Map or the Pole Map Object List.", "Map Trees", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        // Get Map ID of pole so we can highlight it on the map //
                        string strMapID = "";
                        view = (GridView)gridControl2.MainView;
                        int intRowHandle = view.LocateByValue(0, view.Columns["PoleID"], _CurrentPoleID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            strMapID = view.GetRowCellValue(intRowHandle, "MapID").ToString();
                        }
                        if (!string.IsNullOrEmpty(strMapID))
                        {
                            // Centre map on Pole and show Locus Effect //
                            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                            double MinX = (double)9999999.00;  // Used for Zooming to Scale [Not used here but required for function] //
                            double MinY = (double)9999999.00;
                            double MaxX = (double)-9999999.00;
                            double MaxY = (double)-9999999.00;
                            FindAndHighlight(strMapID.ToString(), true, true, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                        }
                    }
                    
                }
                else if (_SurveyMode == "WorkOrderMap")
                {
                    // Highlight any passed in objects //
                    int intHighlightedCount = 0;
                    if (!string.IsNullOrEmpty(strHighlightedIDs))
                    {
                        string strHighlightIDs = "," + strHighlightedIDs;
                        view = (GridView)gridControl2.MainView;
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (strHighlightIDs.Contains("," + view.GetRowCellValue(i, "PoleID").ToString() + ","))
                            {
                                view.SetRowCellValue(i, "Highlight", 1);
                                intHighlightedCount++;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(strHighlightedAssetIDs))
                    {
                        string strHighlightIDs = "," + strHighlightedAssetIDs;
                        view = (GridView)gridControl6.MainView;
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (strHighlightIDs.Contains("," + view.GetRowCellValue(i, "TreeID").ToString() + ","))
                            {
                                view.SetRowCellValue(i, "Highlight", 1);
                                intHighlightedCount++;
                            }
                        }
                    }
                    if (intHighlightedCount > 0)
                    {
                        Highlight_Clicked(true, intHighlightedCount);
                    }
                }
            }

            Clear_Stored_Map_Views();  // Make sure no Stored Map Views are left //
            boolFormOpening = false;
        }

        public void SetModeToTreePlotting(string strPoleNumber)
        {
            _SurveyMode = "PlotTrees";
            bbiAddPoint.Checked = true;
            mapControl1.Tools.LeftButtonTool = "AddPoint";
            textEditPlotTreesAgainstPole.Text = strPoleNumber;
            beiPlotObjectType.EditValue = 2;  // Trees //
            if (_CurrentPoleID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the pole to add the mapped trees to by clicking on it in either the Map or the Pole Map Object List.", "Map Trees", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                // Get Map ID of pole so we can highlight it on the map //
                string strMapID = "";
                GridView view = (GridView)gridControl2.MainView;
                int intRowHandle = view.LocateByValue(0, view.Columns["PoleID"], _CurrentPoleID);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    strMapID = view.GetRowCellValue(intRowHandle, "MapID").ToString();
                }
                if (!string.IsNullOrEmpty(strMapID))
                {
                    // Centre map on Pole and show Locus Effect //
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale [Not used here but required for function] //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID.ToString(), true, true, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }
        }
        public void SetPlottedTreeDescription(string strPoleNumber)
        {
            // Only fired when Called by Survey Pole screen - Adding Tree and Mapping not already open //
            textEditPlotTreesAgainstPole.Text = strPoleNumber;
        }
        public void LoadSurveyIntoMap()
        {
            // Called by UT_Survey_Edit screen //
            strCircuitIDs = "";
            strCircuitNames = "";
            Load_Map_Objects_Grid();
            GridView view = (GridView)gridControl2.MainView;
            int intMax = view.DataRowCount;
            view.BeginUpdate();
            for (int i = 0; i < intMax; i++)
            {
                view.SetRowCellValue(i, "CheckMarkSelection", 1);
            }
            view.EndUpdate();
            UpdateMapObjectsDisplayed(true);
        }
        public void SetModeToSurvey(string strPoleNumber)
        {
            // Called by UT_Survey_Edit screen //
            _SurveyMode = "PlotTrees";
            SetToolToSelectPoint(null);
            textEditPlotTreesAgainstPole.Text = strPoleNumber;
            beiPlotObjectType.EditValue = 2;  // Trees //
            if (_CurrentPoleID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the pole to add the mapped trees to by clicking on it in either the Map or the Pole Map Object List.", "Map Trees", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                // Get Map ID of pole so we can highlight it on the map //
                string strMapID = "";
                GridView view = (GridView)gridControl2.MainView;
                int intRowHandle = view.LocateByValue(0, view.Columns["PoleID"], _CurrentPoleID);
                if (intRowHandle != GridControl.InvalidRowHandle)
                {
                    strMapID = view.GetRowCellValue(intRowHandle, "MapID").ToString();
                }
                if (!string.IsNullOrEmpty(strMapID))
                {
                    // Centre map on Pole and show Locus Effect //
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale [Not used here but required for function] //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID.ToString(), true, true, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }

        }
        public void SetToolToSelectPoint(object objParameter)
        {
            bbiSelect.Checked = true;
            mapControl1.Tools.LeftButtonTool = "Select";
        }

        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        private void frm_UT_Mapping_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                Process_Selected_Map_Objects();  // Refresh Linked Inspections and Actions grids //
            }
            SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_UT_Mapping_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
            //MouseSimulator.MouseDown(MouseButton.Left); // This line is commented out as it can cause ghosting when switching between page tabs and any opened child form needs a second click to focus it... //
            KeyboardSimulator.KeyPress(Keys.Escape);
        }

        private void frm_UT_Mapping_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void frm_UT_Mapping_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (btnGPS_Start.Text == "Stop GPS")
            {
                btnGPS_Start.PerformClick();  // Stop GPS by simulating Click on GPS Button //
            }

            mapControl1.Map.ViewChangedEvent -= new ViewChangedEventHandler(Map_ViewChanged); // Unhook event so it doesn't fire on shut down //
            mapControl1.Map.Clear();
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }

            t = conn.Catalog.GetTable("TempMapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }
            conn.Close();

            if (GPSTimer != null)
            {
                GPSTimer.Stop();
                GPSTimer = null;
            }

            frmMain2 fMain = (frmMain2)this.MdiParent;
            if (fMain != null)
            {
                DevExpress.XtraBars.Docking2010.DocumentManager dm = (DevExpress.XtraBars.Docking2010.DocumentManager)fMain.documentManager1;
                dm.View.DocumentProperties.AllowFloat = true;
            }

            // Remove Mouse and Keyboard Hooks //
            mouseHook.Stop();
            keyboardHook.Stop();

            GC.GetTotalMemory(true);
        }

        private void dockManager1_ActivePanelChanged(object sender, DevExpress.XtraBars.Docking.ActivePanelChangedEventArgs e)
        {
        }

        private void dockManager1_ClosedPanel(object sender, DevExpress.XtraBars.Docking.DockPanelEventArgs e)
        {
        }


        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }


        public override void PostViewClick(object sender, EventArgs e, int row)
        {
            GridView view = (GridView)sender;
            if (view.GridControl.MainView.Name != "gridView2") return;  // Only fire for appropriate view //
            if (row == GridControl.InvalidRowHandle) return;

            if (Control.ModifierKeys != Keys.Shift) return;

            if (row == -99999999)  // User clicked on Viewed
            {
                view.BeginUpdate();
                view.BeginDataUpdate();
                // Process just child rows of the current group //
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    view.SetRowCellValue((i), "Highlight", Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")));
                }
                view.EndDataUpdate();
                view.EndUpdate();
            }
            else if (row < 0)  // Viewed Column checkbox on a Group or the Grid Column Header clicked so copy Visible setting to Highlighted //
            {
                view.BeginUpdate();
                view.BeginDataUpdate();
                // Process just child rows of the current group //
                ArrayList childRows = new ArrayList();
                GetChildDataRows(view, row, childRows);
                for (int i = 0; i < childRows.Count; i++)
                {
                    view.SetRowCellValue(Convert.ToInt32(childRows[i]), "Highlight", Convert.ToInt32(view.GetRowCellValue(Convert.ToInt32(childRows[i]), "CheckMarkSelection")));
                }
                view.EndDataUpdate();
                view.EndUpdate();
            }
            /*else if (row != 0)  // Indivdual row checkbox clicked //
            {
                view.BeginUpdate();
                if (Control.ModifierKeys == Keys.Shift)
                {
                    view.SetRowCellValue(row, "Highlight", Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")));
                }
                else
                {
                    if (Convert.ToInt32(view.GetRowCellValue(row, "CheckMarkSelection")) == 0) view.SetRowCellValue(row, "Highlight", 0);
                }
                view.EndUpdate();
            }*/
        }

        void GetChildDataRows(GridView view, int rowHandle, ArrayList r)
        {
            int childCount = view.GetChildRowCount(rowHandle);
            for (int i = 0; i < childCount; i++)
            {
                int childRowHandle = view.GetChildRowHandle(rowHandle, i);
                if (childRowHandle < 0)
                {
                    GetChildDataRows(view, childRowHandle, r);
                }
                else
                {
                    r.Add(childRowHandle);
                }
            }
        }


        public void Load_Map_Objects_Grid()
        {
            this.RefreshGridViewState1.SaveViewInfo(); // Store any expanded groups and selected rows //
            // Store visible Map Objects first //
            int intSelectionCount = selection1.SelectedCount;
            int intCount = 0;
            List<int> list = new List<int>();
            GridView view = (GridView)gridControl2.MainView;
            int intMax = view.DataRowCount;
            if (intSelectionCount > 0)
            {
                for (int i = 0; i < intMax; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        list.Add(Convert.ToInt32(view.GetRowCellValue(i, "PoleID")));
                        intCount++;
                        if (intCount >= intSelectionCount) break;
                    }
                }
            }

            // Populate list of searchable Map Objects //
            view.BeginUpdate();
            this.sp07049_UT_Tree_Picker_PolesTableAdapter.Fill(this.dataSet_UT_Mapping.sp07049_UT_Tree_Picker_Poles, strCircuitIDs, _SelectedSurveyID, _PassedInPoleIDs, _PoleThematicTypeID);

            // Put Visible objects back //
            if (intSelectionCount > 0)
            {
                GridColumn column = view.Columns["PoleID"];
                int intFoundRow = 0;
                foreach (int item in list)
                {
                    intFoundRow = view.LocateByValue(0, column, item);
                    if (intFoundRow != GridControl.InvalidRowHandle) view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                }
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();
        }


 
        private void CreateFeatureLayerModifier(MapInfo.Mapping.IMapLayer l, DataRow dr)
        {
            int intTempPointSize = (intMapObjectSizing <= 0 ? 8 : intMapObjectSizing);

            if (l.Type == LayerType.Normal)  // Vector Layer //
            {
                // Create a feature override style modifier if PreserveDefaultStyling switched off //
                if (Convert.ToInt32(dr["PreserveDefaultStyling"]) == 0)  // Allow styling according to criteria //
                {
                    CompositeStyle style = new CompositeStyle();
                    //((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * Convert.ToInt32(dr["Transparency"])), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(dr["LineWidth"]), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"]))));

                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.White);
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));


                    FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                    modifier.Style = style;
                    modifier.Name = "FeatureStyleModifier1";
                    FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
                    layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //
                }
            }
            else if (l.Type == LayerType.Raster)  // Raster Layer //
            {
                // Create a feature override style modifier //
                CompositeStyle style = new CompositeStyle();
                style.RasterStyle.Grayscale = (Convert.ToInt32(dr["GreyScale"]) == 1 ? true : false);
                //style.RasterStyle.Transparent = true;    // *** This line commented out otherwise monocrome image layers don't display! *** //
                style.RasterStyle.Alpha = (int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100)));  // Controls Transparency //

                FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                modifier.Style = style;
                modifier.Name = "FeatureStyleModifier1";
                FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
                layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //

            }
        }

        private int GetLayerManagerMapObjectsLayerRow()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return -1;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjects") return i;
            }
            return -1;  // Not found //
        }

        private string Get_ThematicField_ColumnName(int ThematicStylingBasePicklistID)
        {
            // Returns the field name (column) from which the value needs to be extracted from the MapObject Grid so it can be used for a match against the thematic styles //
            switch (ThematicStylingBasePicklistID)
            {
                case -1:  // Localities //
                    return "intLocalityID";
                case 2:  // VoltageType //
                    return "VoltageID";
                case 6:  // Ownerships //
                    return "intOwnershipID";
                case 101:  // Status //
                    return "intStatus";
                case 103:  // Risk Categories //
                    return "intSafetyPriority";
                case 112:  // Accessibility //
                    return "intAccess";
                case 113:  // Visibility //
                    return "intVisibility";
                case 114:  // Legal Status //
                    return "intLegalStatus";
                case 115:  // Ground Conditions //
                    return "intGroundType";
                case 116:  // Protection Types //
                    return "intProtectionType";
                case 118:  // Site Hazard Classes //
                    return "intSiteHazardClass";
                case 122:  // Sizes //
                    return "intSize";
                case 123:  // Age Classes //
                    return "intAgeClass";
                case 135:  // Height Bands //
                    return "intHeightRange";
                case 136:  // DBH Bands //
                    return "intDBHRange";
                case 90000:  // DBH //
                    return "DBH";
                case 90001:  // Height //
                    return "Height";
                case 90002:  // Risk Factor //
                    return "RiskFactor";
                case 90003:  // Area M2 [Value is actual M2 but stored in field labeled Ha for Historical Reasons] //
                    return "AreaHa";
                case 90004:  // Number of days since last inspection [stored at Tree level (LastInspectionDate)] //
                    return "LastInspectionElapsedDays";
                default:
                    return "";
            }
        }

        private double Get_Thematic_Value(MapInfo.Data.Feature f)
        {
            double dblReturn = (double)0.00;
            if (Convert.ToInt32(f["ModuleID"]) == 2)  // 2 = Tree //
            {
                if (Convert.ToInt32(f["intHighlighted"]) == 1)
                {
                    dblReturn = Convert.ToDouble(999995);  // Highlight //
                }
                else
                {
                    dblReturn = Convert.ToDouble(f["ThematicValue"]);
                }
            }
            else  // 1 = Pole //
            {
                if (Convert.ToInt32(f["intHighlighted"]) == 1)
                {
                    dblReturn = Convert.ToDouble(999995);  // Highlight //
                }
                else if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR POLES //
                {
                    dblReturn = Convert.ToDouble(f["ThematicValue"]);
                }
            }
            return dblReturn;
        }

        private void Clear_MapInfo_Themes()
        {
            Map map = mapControl1.Map;
            FeatureLayer lyr = map.Layers["MapObjects"] as MapInfo.Mapping.FeatureLayer;
            if (lyr == null) return;

            lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
            map.Adornments.Clear();  // Clear any prior map adornments //
            if (intMapObjectTransparency <= 0) return;
        }

        private void Create_MapInfo_Theme()
        {
            Map map = mapControl1.Map;
            FeatureLayer lyr = map.Layers["MapObjects"] as MapInfo.Mapping.FeatureLayer;
            if (lyr == null) return;
            int i = 0;

            // Get number of bins in theme to create //
            if (intUseThematicStyling != 1 || intThematicStylingBasePicklistID <= 0)  // No Thematics so create a single catch all so transparency effect can be applied //
            {
                MapInfo.Mapping.Thematics.IndividualValueTheme thm = new MapInfo.Mapping.Thematics.IndividualValueTheme(lyr, "Thematics", "Thematics", 11);  // LAST NUMBER IS NUMBER OF BINS TO CREATE //
                thm.ApplyStylePart = StylePart.Color;
                // Thematic Tree //
                CompositeStyle defaultStyle = Get_Default_Style();
                thm.Bins[0].Style = defaultStyle;
                thm.Bins[0].Value = Convert.ToDouble(9999998);

                // Thematic Pole //
                CompositeStyle defaultStylePole = Get_Default_Style_Pole();  // ***** IMPORTANT: Make sure to create a seperate style for each bin otherwise the stying gets overwritten by the last one. ***** //
                thm.Bins[1].Style = defaultStylePole;
                thm.Bins[1].Value = Convert.ToDouble(9999997);

                // Create Highlighting Bin //
                thm.Bins[2].Style = Get_Highlight_Style();
                thm.Bins[2].Value = Convert.ToDouble(999995);

                // Create Surveying Themes Bins //
                CompositeStyle defaultStyle2 = Get_Default_Style();
                defaultStyle2.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend1);
                thm.Bins[3].Style = defaultStyle2;
                thm.Bins[3].Value = Convert.ToDouble(-1);

                CompositeStyle defaultStyle3 = Get_Default_Style();
                defaultStyle3.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend2);
                thm.Bins[4].Style = defaultStyle3;
                thm.Bins[4].Value = Convert.ToDouble(0);

                CompositeStyle defaultStyle4 = Get_Default_Style();
                defaultStyle4.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend3);
                thm.Bins[5].Style = defaultStyle4;
                thm.Bins[5].Value = Convert.ToDouble(1);

                CompositeStyle defaultStyle5 = Get_Default_Style();
                defaultStyle5.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend5);
                thm.Bins[6].Style = defaultStyle5;
                thm.Bins[6].Value = Convert.ToDouble(2);

                CompositeStyle defaultStyle6 = Get_Default_Style();
                defaultStyle6.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend4);
                thm.Bins[7].Style = defaultStyle6; //5;
                thm.Bins[7].Value = Convert.ToDouble(3);

                CompositeStyle defaultStyle7 = Get_Default_Style();
                defaultStyle7.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend6);
                thm.Bins[8].Style = defaultStyle7;
                thm.Bins[8].Value = Convert.ToDouble(4);

                CompositeStyle defaultStyle8 = Get_Default_Style();
                defaultStyle7.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), colourPoleLegend7);
                thm.Bins[9].Style = defaultStyle8;
                thm.Bins[9].Value = Convert.ToDouble(5);

                // Non-Thematic Tree //
                thm.Bins[10].Style = defaultStyle6;
                thm.Bins[10].Value = Convert.ToDouble(0);

                // Everything Else (Non-Thematic Poles) //
                thm.AllOthersBin.Style = defaultStylePole;
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);

            }
            else if (intThematicStylingBasePicklistID >= 90000)  // Range Theme //
            {
                DataSet dataSetFiltered = new DataSet();
                dataSetFiltered.Merge(dsThematicStyleItems.Tables[0].Select("ShowInLegend = 1"));

                int intMaxItems = dataSetFiltered.Tables[0].Rows.Count;
                if (intMaxItems < 0) return;

                // Create Theme //
                MapInfo.Mapping.Thematics.RangedTheme thm = new MapInfo.Mapping.Thematics.RangedTheme(lyr, "Thematics", "Thematics", intMaxItems + 1, MapInfo.Mapping.Thematics.DistributionMethod.CustomRanges);
                thm.ApplyStylePart = StylePart.Color;
                thm.SpreadBy = SpreadByPart.None;

                // Create Legend //
                MapInfo.Mapping.Legends.LegendFactory lgdFactory = this.mapControl1.Map.Legends;
                MapInfo.Mapping.Legends.Legend lgd = lgdFactory.CreateLegend(new Size(5, 5));
                MapInfo.Mapping.Legends.CustomLegendFrameRow[] rows = new MapInfo.Mapping.Legends.CustomLegendFrameRow[intMaxItems + 1];
                //MapInfo.Mapping.Legends.AllOthersLegendFrameRow[] rowAllOthers = new AllOthersLegendFrameRow[1];
                lgd.Border = true;
                lgd.BorderPen = new System.Drawing.Pen(System.Drawing.Color.Black, 1f);

                // Populate Theme //
                int intDefaultSymbol = 0;
                int intDefaultSymbolColour = 0;
                int intDefaultSymbolSize = 0;
                int intDefaultPolygonBoundaryColour = 0;
                int intDefaultPolygonBoundaryWidth = 0;
                int intDefaultPolygonFillColour = 0;
                int intDefaultPolygonFillPattern = 0;
                int intDefaultPolygonFillPatternColour = 0;
                int intDefaultLineStyle = 0;
                int intDefaultLineColour = 0;
                int intDefaultLineWidth = 0;

                CompositeStyle style = null;
                foreach (DataRow dr in dataSetFiltered.Tables[0].Rows)
                {
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    style = new CompositeStyle();
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    }
                    ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                    ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                    ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                    style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //

                    if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
                    {
                        thm.Bins[i].Min = Convert.ToDouble(dr["StartBand"]);
                        thm.Bins[i].Max = Convert.ToDouble(dr["EndBand"]); ;
                    }
                    else
                    {
                        thm.Bins[i].Min = Convert.ToDouble(dr["PicklistItemID"]);
                        thm.Bins[i].Max = Convert.ToDouble(dr["PicklistItemID"]);
                    }
                    thm.Bins[i].Style = style;
                    i++;  // Increment counter //
                }
                // Create Highlighting Bin //
                thm.Bins[i].Style = Get_Highlight_Style();
                thm.Bins[i].Min = Convert.ToDouble(999990);
                thm.Bins[i].Max = Convert.ToDouble(999999);
                thm.Recompute();  // Recompute Bins and Styles since theybhave changed //

                thm.AllOthersBin.Style = Get_Default_Style();
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);
                /*
                // Populate Legend //
                i = 0;
                for (i = 0; i < thm.Bins.Count - 1; i++)
                {
                    DataRow dr = dataSetFiltered.Tables[0].Rows[i];
                    rows[i] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[i].Style.AreaStyle, thm.Bins[i].Style.LineStyle, null, thm.Bins[i].Style.SymbolStyle), dr["ItemDescription"].ToString());
                }
                rows[intMaxItems] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[intMaxItems].Style.AreaStyle, thm.Bins[intMaxItems].Style.LineStyle, null, thm.Bins[intMaxItems].Style.SymbolStyle), "Highlighted");

                MapInfo.Mapping.Legends.LegendFrame thmFrame = MapInfo.Mapping.Legends.LegendFrameFactory.CreateCustomLegendFrame("Legend", "Legend", rows);
                lgd.Frames.Append(thmFrame);
                lgd.BackgroundBrush = new System.Drawing.SolidBrush(Color.White);
                thmFrame.TitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.SubTitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.RowTextStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.Title = "Legend: " + strThematicStylingFieldName;
                //thmFrame.TitleStyle.FontWeight = FontWeight.Bold;
                map.Adornments.Append(lgd);
                LegendExport lgdExporter = new LegendExport(mapControl1.Map, lgd);
                lgdExporter.Format = ExportFormat.Png;
                pictureBoxLegend.Image = lgdExporter.Export();
                */
            }
            else   // Individual Theme //
            {
                DataSet dataSetFiltered = new DataSet();
                dataSetFiltered.Merge(dsThematicStyleItems.Tables[0].Select("ShowInLegend = 1"));

                int intMaxItems = dataSetFiltered.Tables[0].Rows.Count;
                if (intMaxItems < 0) return;
                //else if (intMaxItems > 30) intMaxItems = 30;  // Max 50, so 49 themes plus 1 for Highlight theme. //

                // Create Theme //
                MapInfo.Mapping.Thematics.IndividualValueTheme thm = new MapInfo.Mapping.Thematics.IndividualValueTheme(lyr, "Thematics", "Thematics", intMaxItems + 1);
                thm.ApplyStylePart = StylePart.Color;

                // Create Legend //
                MapInfo.Mapping.Legends.LegendFactory lgdFactory = this.mapControl1.Map.Legends;
                MapInfo.Mapping.Legends.Legend lgd = lgdFactory.CreateLegend(new Size(5, 5));
                MapInfo.Mapping.Legends.CustomLegendFrameRow[] rows = new MapInfo.Mapping.Legends.CustomLegendFrameRow[intMaxItems + 1];
                //MapInfo.Mapping.Legends.AllOthersLegendFrameRow[] rowAllOthers = new AllOthersLegendFrameRow[1];
                lgd.Border = true;
                lgd.BorderPen = new System.Drawing.Pen(System.Drawing.Color.Black, 1f);

                // Populate Theme //
                int intDefaultSymbol = 0;
                int intDefaultSymbolColour = 0;
                int intDefaultSymbolSize = 0;
                int intDefaultPolygonBoundaryColour = 0;
                int intDefaultPolygonBoundaryWidth = 0;
                int intDefaultPolygonFillColour = 0;
                int intDefaultPolygonFillPattern = 0;
                int intDefaultPolygonFillPatternColour = 0;
                int intDefaultLineStyle = 0;
                int intDefaultLineColour = 0;
                int intDefaultLineWidth = 0;

                CompositeStyle style = null;
                foreach (DataRow dr in dataSetFiltered.Tables[0].Rows)
                {
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    style = new CompositeStyle();
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                        ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    }
                    ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                    ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                    ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                    style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //

                    thm.Bins[i].Value = Convert.ToDouble(dr["PicklistItemID"]);
                    thm.Bins[i].Style = style;
                    i++;  // Increment counter //
                }
                // Create Highlighting Bin //
                thm.Bins[i].Style = Get_Highlight_Style();
                thm.Bins[i].Value = Convert.ToDouble(999995);

                thm.AllOthersBin.Style = Get_Default_Style();
                lyr.Modifiers.Clear();  // Clear any prior themes on the Map Objects layer //
                lyr.Modifiers.Append(thm);
                /*
                // Populate Legend //
                i = 0;
                for (i = 0; i < thm.Bins.Count - 1; i++)
                {
                    DataRow dr = dataSetFiltered.Tables[0].Rows[i];
                    rows[i] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[i].Style.AreaStyle, thm.Bins[i].Style.LineStyle, null, thm.Bins[i].Style.SymbolStyle), dr["ItemDescription"].ToString());
                }
                rows[intMaxItems] = new CustomLegendFrameRow(new CompositeStyle(thm.Bins[intMaxItems].Style.AreaStyle, thm.Bins[intMaxItems].Style.LineStyle, null, thm.Bins[intMaxItems].Style.SymbolStyle), "Highlighted");
                
                MapInfo.Mapping.Legends.LegendFrame thmFrame = MapInfo.Mapping.Legends.LegendFrameFactory.CreateCustomLegendFrame("Legend", "Legend", rows);
                lgd.Frames.Append(thmFrame);
                lgd.BackgroundBrush = new System.Drawing.SolidBrush(Color.White);
                thmFrame.TitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.SubTitleStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.RowTextStyle = new MapInfo.Styles.Font("Arial", 9);
                thmFrame.Title = "Legend: " + strThematicStylingFieldName;
                //thmFrame.TitleStyle.FontWeight = FontWeight.Bold;
                map.Adornments.Append(lgd);
                LegendExport lgdExporter = new LegendExport(mapControl1.Map, lgd);
                lgdExporter.Format = ExportFormat.Png;
                pictureBoxLegend.Image = lgdExporter.Export();
                */
            }
        }

        private MapInfo.Styles.CompositeStyle Get_Default_Style()
        {
            DataRow drStyle = dsDefaultStyles.Tables[0].Rows[0];
            int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
            int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
            int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
            int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
            int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
            int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
            int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
            int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
            int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
            int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
            int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);
            //int intDefaultSymbol2 = Convert.ToInt32(drStyle["Symbol2"]);
            //int intDefaultSymbol2Colour = Convert.ToInt32(drStyle["Symbol2Colour"]);
            //int intDefaultSymbol2Size = Convert.ToInt32(drStyle["Size2"]);

            CompositeStyle style = new CompositeStyle();
            if (intDefaultPolygonFillPattern == 2) // Solid //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            else if (intDefaultPolygonFillPattern == 1)  // Tranparent //  The 50 on the lines below = transparent level [hardcoded for now]
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }

            else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
            ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
            ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
            ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

            style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
            style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //

            return style;
        }
        private MapInfo.Styles.CompositeStyle Get_Default_Style_Pole()
        {
            DataRow drStyle = dsDefaultStyles.Tables[0].Rows[0];
            int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
            int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
            int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
            int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
            int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
            int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
            int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
            int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
            int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
            int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
            int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);
            int intDefaultSymbol2 = Convert.ToInt32(drStyle["Symbol2"]);
            int intDefaultSymbol2Colour = Convert.ToInt32(drStyle["Symbol2Colour"]);
            int intDefaultSymbol2Size = Convert.ToInt32(drStyle["Size2"]);

            CompositeStyle style = new CompositeStyle();
            if (intDefaultPolygonFillPattern == 2) // Solid //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            else if (intDefaultPolygonFillPattern == 1)  // Tranparent //  The 50 on the lines below = transparent level [hardcoded for now]
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(75) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }

            else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                //((SimpleInterior)style.AreaStyle.Interior).Transparent = true;
            }
            ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
            ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
            ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
            ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

            style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
            style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol2), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbol2Colour))), intDefaultSymbol2Size); // Symbol 32 = Square //
            return style;
        }

        /*private MapInfo.Styles.CompositeStyle Get_Thematic_Style(int intPicklistValue, decimal decFieldValue)
        {
            if (intThematicStylingBasePicklistID == 0) return Get_Default_Style();
            CompositeStyle style = null;
            DataRow[] drFiltered;
            if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("StartBand <= " + decFieldValue.ToString() + " and EndBand >= " + decFieldValue.ToString());
            }
            else
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("PicklistItemID = " + intPicklistValue.ToString());
            }
            if (drFiltered.Length != 0)
            {
                style = new CompositeStyle();
                // Set Style here //
                DataRow drStyle = drFiltered[0];
                int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
                int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
                int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
                int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
                int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
                int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
                int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
                int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
                int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
                int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);

                if (intDefaultPolygonFillPattern == 2) // Solid //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                }
                else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                }
                ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

                style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbolColour))), intDefaultSymbolSize); // Symbol 34 = circle //
            }
            return (style == null ? Get_Default_Style() : style);
        }*/
        /*private MapInfo.Styles.CompositeStyle Get_Thematic_Style_Pole(int intPicklistValue, decimal decFieldValue)
        {
            if (intThematicStylingBasePicklistID == 0) return Get_Default_Style();
            CompositeStyle style = null;
            DataRow[] drFiltered;
            if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("StartBand <= " + decFieldValue.ToString() + " and EndBand >= " + decFieldValue.ToString());
            }
            else
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("PicklistItemID = " + intPicklistValue.ToString());
            }
            if (drFiltered.Length != 0)
            {
                style = new CompositeStyle();
                // Set Style here //
                DataRow drStyle = drFiltered[0];
                int intDefaultSymbol = Convert.ToInt32(drStyle["Symbol"]);
                int intDefaultSymbolColour = Convert.ToInt32(drStyle["SymbolColour"]);
                int intDefaultSymbolSize = Convert.ToInt32(drStyle["Size"]);
                int intDefaultPolygonBoundaryColour = Convert.ToInt32(drStyle["PolygonLineColour"]);
                int intDefaultPolygonBoundaryWidth = Convert.ToInt32(drStyle["PolygonLineWidth"]);
                int intDefaultPolygonFillColour = Convert.ToInt32(drStyle["PolygonFillColour"]);
                int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                int intDefaultPolygonFillPatternColour = Convert.ToInt32(drStyle["PolygonFillPatternColour"]);
                int intDefaultLineStyle = Convert.ToInt32(drStyle["PolylineStyle"]);
                int intDefaultLineColour = Convert.ToInt32(drStyle["PolylineColour"]);
                int intDefaultLineWidth = Convert.ToInt32(drStyle["PolylineWidth"]);
                int intDefaultSymbol2 = Convert.ToInt32(drStyle["Symbol2"]);
                int intDefaultSymbol2Colour = Convert.ToInt32(drStyle["Symbol2Colour"]);
                int intDefaultSymbol2Size = Convert.ToInt32(drStyle["Size2"]);

                if (intDefaultPolygonFillPattern == 2) // Solid //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                }
                else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillColour)));
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonFillPatternColour)));
                }
                ((SimpleInterior)style.AreaStyle.Interior).Pattern = intDefaultPolygonFillPattern;
                ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultPolygonBoundaryColour)));
                ((SimpleLineStyle)style.AreaStyle.Border).Pattern = 2;  // 2 = solid line //
                ((SimpleLineStyle)style.AreaStyle.Border).Width = new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultPolygonBoundaryWidth), MapInfo.Styles.LineWidthUnit.Pixel);

                style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intDefaultLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), intDefaultLineStyle, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultLineColour))));
                style.SymbolStyle = new SimpleVectorPointStyle(Convert.ToInt16(intDefaultSymbol2), Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intDefaultSymbol2Colour))), intDefaultSymbol2Size); // Symbol 32 = Square //
            }
            return (style == null ? Get_Default_Style() : style);
        }*/
        private MapInfo.Styles.CompositeStyle Get_Thematic_Style_Tree(int intObjectType, int intThematicValue)
        {
            // Get a Thematic Style - base it on the default style then adjust the colours to match that of the calculated thematic colour //
            System.Drawing.Color ThematicColour = (intObjectType == 1 ? Get_Pole_Colour(intThematicValue) : Get_Tree_Colour(intThematicValue));
            CompositeStyle style = Get_Default_Style();
            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
            {
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), ThematicColour);
            }
            else
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), ThematicColour);
            }
            style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), ThematicColour);
            ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), ThematicColour);
            return style;
        }

        private MapInfo.Styles.CompositeStyle Get_Highlight_Style()
        {
            // Get a Highlight Style - base it on the default style then adjust the colours to match that on the highlight colour drop down //
            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
            CompositeStyle style = Get_Default_Style();
            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
            {
                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
            else
            {
                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
            style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            return style;
        }

        private Color Get_Tree_Colour(int intStatus)
        {
            if (_TreeThematicTypeID == -1) return Color.LimeGreen;

            switch (intStatus)
            {
                //case -1:
                //    return colourTreeLegend1;
                case 0:
                    return colourTreeLegend1;
                case 1:
                    return colourTreeLegend2;
                case 2:
                    return colourTreeLegend3;
                case 3:
                    return colourTreeLegend4;
                case 4:
                    return colourTreeLegend5;
                default:
                    return Color.LimeGreen;
            }
        }
        private Color Get_Pole_Colour(int intStatus)
        {
            switch (intStatus)
            {
                case -1:
                    return colourPoleLegend1;
                case 0:
                    return colourPoleLegend2;
                case 1:
                    return colourPoleLegend3;
                case 2:
                    return colourPoleLegend5;
                case 3:
                    return colourPoleLegend4;
                case 4:
                    return colourPoleLegend6;
                case 5:
                    return colourPoleLegend7;
                default:
                    return Color.HotPink;
            }
        }

        public void Update_Map_Object_Grid_Visible_And_Highlighted()  // Public so it can be called by the Mapping_Functions Class //
        {
            char[] delimiters = new char[] { ',' };
            string[] strArray = strVisibleIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 0)
            {
                GridView view = view = (GridView)gridControl2.MainView;
                GridColumn column = view.Columns["MapID"];

                int intFoundRow = 0;
                view.BeginUpdate();
                int intLastFoundRow = GridControl.InvalidRowHandle;
                for (int i = 0; i < strArray.Length; i++)
                {
                    intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        intLastFoundRow = intFoundRow;
                    }
                }
                if (selection1.SelectedCount > 0 || selectionTrees.SelectedCount > 0)
                {
                    UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
                }
                view.EndUpdate();
                // Move map to last found row only if no passed IDs for highlighting otherwise moving / highlighting / scaling done in next process //
                if (strHighlightedIDs == "")
                {
                    string strMapID = view.GetRowCellValue(intLastFoundRow, "MapID").ToString();
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }
            strArray = strVisibleTreesIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length > 0)
            {
                GridView view = view = (GridView)gridControl6.MainView;
                GridColumn column = view.Columns["AssetID"];

                int intFoundRow = 0;
                view.BeginUpdate();
                int intLastFoundRow = GridControl.InvalidRowHandle;
                for (int i = 0; i < strArray.Length; i++)
                {
                    intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                        intLastFoundRow = intFoundRow;
                    }
                }
                if (selection1.SelectedCount > 0 || selectionTrees.SelectedCount > 0)
                {
                    UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
                }
                view.EndUpdate();
                // Move map to last found row only if no passed IDs for highlighting otherwise moving / highlighting / scaling done in next process //
                if (strHighlightedAssetIDs == "")
                {
                    string strMapID = view.GetRowCellValue(intLastFoundRow, "MapID").ToString();
                    System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;
                    double MinX = (double)9999999.00;  // Used for Zooming to Scale //
                    double MinY = (double)9999999.00;
                    double MaxX = (double)-9999999.00;
                    double MaxY = (double)-9999999.00;
                    FindAndHighlight(strMapID, true, false, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }

            int intHighlightedCount = 0;
            if (strHighlightedIDs != "")
            {
                strArray = strHighlightedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 0)
                {
                    GridView view = (GridView)gridControl2.MainView;
                    GridColumn column = view.Columns["MapID"];
                    int intFoundRow = 0;
                    view.BeginUpdate();
                    int intLastFoundRow = GridControl.InvalidRowHandle;
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "Highlight", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            intLastFoundRow = intFoundRow;
                            intHighlightedCount++;
                        }
                    }
                    view.EndUpdate();
                }
            }

            if (strHighlightedAssetIDs != "")
            {
                strArray = strHighlightedAssetIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray.Length > 0)
                {
                    GridView view = (GridView)gridControl6.MainView;
                    GridColumn column = view.Columns["AssetID"];
                    int intFoundRow = 0;
                    view.BeginUpdate();
                    int intLastFoundRow = GridControl.InvalidRowHandle;
                    for (int i = 0; i < strArray.Length; i++)
                    {
                        intFoundRow = view.LocateByValue(0, column, Convert.ToInt32(strArray[i]));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "Highlight", 1);
                            view.MakeRowVisible(intFoundRow, false);
                            intLastFoundRow = intFoundRow;
                            intHighlightedCount++;
                        }
                    }
                    view.EndUpdate();
                }
            }
            if (selection1.SelectedCount > 0 || selectionTrees.SelectedCount > 0)
            {
                UpdateMapObjectsDisplayed(false);  // false for no progress screen as we already have a progress screen open //
            }
            if (intHighlightedCount > 0)
            {
                //if (i_str_selected_workorders != "")  // Work Order Passed In //
                //{
                ceCentreMap.Checked = false;
                csScaleMapForHighlighted.Checked = true;
                //}
                Highlight_Clicked(false, intHighlightedCount);
                PreventDefaultMapScaleUse = true;  // Stop default Scale and centre point from being set in Load_Map_Layers() event //
            }


        }

        private void LoadMapObjects()
        {
            CompositeStyle style = Get_Default_Style();

            //*** Create Temporary Tab file to hold points, polygons and polylines etc ***** //
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }
            TableInfoMemTable tiTemp = new TableInfoMemTable("MapObjects");
            tiTemp.Temporary = true;
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedStringColumn("id", 50));  // Holds records MapID //
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("MapID"));  // Holds either PoleID or Tree ID //
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ObjectReference", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("RegionName", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SubAreaName", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("VoltageType", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateDoubleColumn("X"));
            tiTemp.Columns.Add(ColumnFactory.CreateDoubleColumn("Y"));
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("intObjectType"));  // 0 = Point, 1 = Polygon, 2 = Polyline //
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("intHighlighted"));  // 0 = No, 1 = Yes //
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("CrownDiameter"));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("CalculatedSize", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("CalculatedPerimeter", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateDateTimeColumn("LastInspectionDate"));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ShortTreeRef", 50));

            // New Columns //
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedIntColumn("ModuleID"));  // 1 = Poles, 2 Trees //
            //tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("AssetID"));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetNumber", 50));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("PartNumber", 50));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ModelNumber", 50));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SerialNumber", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ClientName", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("ClientCode", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SurveyStatus", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("SurveyDone"));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SiteName", 100));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("SiteCode", 50));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetType", 100));
            //tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("AssetSubType", 100));
            tiTemp.Columns.Add(ColumnFactory.CreateDoubleColumn("Thematics"));
            tiTemp.Columns.Add(ColumnFactory.CreateIntColumn("ThematicValue"));

            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Tooltip", 1000));

            tiTemp.Columns.Add(ColumnFactory.CreateStyleColumn());

            CoordSysFactory factory = Session.Current.CoordSysFactory;
            CoordSys BritishGrid = factory.CreateFromMapBasicString(strDefaultMappingProjection);
            //CoordSys BritishGrid = factory.CreateFromMapBasicString("CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000");
            tiTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(BritishGrid));

            // Note: No need to add a column of type Key. Every table automatically contains a column named "MI_Key". //
            Table table = Session.Current.Catalog.CreateTable(tiTemp);  // This layer not yet loaded into map - done later in process //

            // Check if any map objects need to be pre-loaded, if yes then load them... //
            Update_Map_Object_Grid_Visible_And_Highlighted();

            conn.Close();
            conn.Dispose();

            mapControl1.Map.Load(new MapTableLoader(table)); // Load Object Layer now it has been populated //
        }  // ***** Underlying Table Structure ***** //

        private void CreateScaleBar()
        {
            // NO LONGER USED - MANUALLY GENERATED USING GDI+ //
            /*
            if (ceScaleBar.Checked)
            {
                // Create a scalebar 
                ScaleBarAdornment sba = new ScaleBarAdornment(mapControl1.Map);

                // Position the scalebar at the lower right corner of map
                int x = mapControl1.Map.Size.Width - sba.Size.Width;
                int y = mapControl1.Map.Size.Height - sba.Size.Height;
                sba.Location = new System.Drawing.Point(x, y);
                
                // Add the control to the map
                ScaleBarAdornmentControl sbac = new ScaleBarAdornmentControl(sba, mapControl1.Map);
                
                mapControl1.AddAdornment(sba, sbac);
            }
            else
            {
                // Remove Scale Bar if present //

            }
            */
        }

        private void OpenShapeFile()
        {

            // Open Shape Files //
            TableInfoShapefile ti = new TableInfoShapefile("Clarence_OS_polyline [EsriShape]");
            ti.TablePath = @"C:\Map\TreePicker\ESRI\Clarence_OS_polyline.shp";
            MapInfo.Geometry.CoordSysFactory CSysFactory = Session.Current.CoordSysFactory;


            MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:" + strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)");
            //MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000 Bounds (0,0) (5000000,5000000)");
            /*MapInfo.Geometry.CoordSysFactory miCF = new MapInfo.Geometry.CoordSysFactory();
            MapInfo.Data.SpatialSchemaXY miSpS = new MapInfo.Data.SpatialSchemaXY();
            miSpS.XColumn = "intX";  // *** HOW DO I Get polygons to link? *** //
            miSpS.YColumn = "intY";
            string mbCoordSys = strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)";
            miSpS.CoordSys = miCF.CreateFromMapBasicString(mbCoordSys);
            ti.SpatialSchema = miSpS;
            */


            ti.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(coordSys));
            //ti.Columns.Add(ColumnFactory.CreateStringColumn("State", 2));
            //ti.Columns.Add(ColumnFactory.CreateStringColumn("State_name", 20));

            //ti.Columns.Add(ColumnFactory.CreateStyleColumn()); 
            ti.DefaultStyle = new MapInfo.Styles.AreaStyle(

            new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(2.0, MapInfo.Styles.LineWidthUnit.Pixel), (int)MapInfo.Styles.PatternStyle.Solid, Color.Red),

            new MapInfo.Styles.SimpleInterior((int)MapInfo.Styles.PatternStyle.Cross, Color.Red, Color.White));

            Table table = Session.Current.Catalog.OpenTable(ti);

            MapTableLoader tl = new MapTableLoader(table);
            mapControl1.Map.Load(tl);



        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            int[] intRowHandles;

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView5":
                    message = "No map layers available";
                    break;
                case "gridView2":
                    message = "No objects available";
                    break;
                case "gridViewAssetObjects":
                    message = "No objects available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView2":
                    if (bsiSyncSelection.Caption == "Sync Map Selection: Off")
                    {
                        if (view.SelectedRowsCount > 0)
                        {
                            int[] intRowHandles = view.GetSelectedRows();
                            int intLastSelectedRow = intRowHandles.Length - 1;

                            textEditPlotTreesAgainstPole.Text = view.GetRowCellValue(intRowHandles[intLastSelectedRow], "PoleNumber").ToString();
                            _CurrentPoleID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[intLastSelectedRow], "PoleID"));
                        }
                    }
                    break;
                case "gridView4":
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region Mouse and Keyboard Hook Events

        void keyboardHook_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (Form.ActiveForm != null)  // This line prevents code from firing the event from other running applications. [Excluding the next IF will allow any form regardless of which is active to fire the code below]. //
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            if (mapControl1.Tools.CurrentTool == null) return;
            if (mapControl1.Tools.CurrentTool.ToString() == "WoodPlan5.PSGDistanceTool")
            {
                if (e.KeyChar.ToString() == "d")  // Show Total measured distance //
                {
                    XtraMessageBox.Show("Total Distance Measured = " + dMeasuredDistance.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(MapInfo.Geometry.DistanceUnit.Meter) + ".", "Measure", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (e.KeyChar.ToString() == "p")  // Plot Point //
                {
                    System.Drawing.Point ptCursor = new System.Drawing.Point(Cursor.Position.X, Cursor.Position.Y);
                    System.Drawing.Point ptMap = mapControl1.PointToClient(ptCursor);
                    MapInfo.Geometry.DPoint newPoint = new DPoint(0, 0);
                    MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
                    converter.FromDisplay(new PointF(ptMap.X, ptMap.Y), out newPoint);
                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), newPoint.x, newPoint.y);
                    int intSuccess = AddObjectToMap(g);  // Add object to map //
                }
                else if (e.KeyChar.ToString() == "s")  // Snap //
                {
                    ToggleSnap();  // toggle snap mode if user presses the 'S' key //
                }
            }
        }

        void keyboardHook_KeyUp(object sender, KeyEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.KeyCode.ToString();
        }

        void keyboardHook_KeyDown(object sender, KeyEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.KeyCode.ToString();
        }

        void mouseHook_MouseWheel(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();
        }

        void mouseHook_MouseUp(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();

            if (e.Button == MouseButtons.Right)
            {
                IntPtr hWnd = WindowFromPoint(Control.MousePosition);
                if (hWnd != IntPtr.Zero)
                {
                    Control ctl = Control.FromHandle(hWnd);
                    if (ctl != null)
                    {
                        if (ctl.Name == "mapControl1")
                        {
                            if (mapControl1.Tools.LeftButtonTool != null)
                            {
                                string strCurrentTool = mapControl1.Tools.LeftButtonTool;
                                if (strCurrentTool == "Distance" || strCurrentTool == "AddPoint" || strCurrentTool == "AddPolygon" || strCurrentTool == "AddPolyline")
                                {
                                    // The Current tool won't disengage to allow the menu to process the mouse over [highlighting current menu item], so following takes care of it //
                                    // Switch tool to the Pan tool while menu is shown then switch back again afterwards //
                                    mapControl1.Tools.LeftButtonTool = "Pan";
                                    this.ActiveControl = panelContainer1;
                                    ShowMapControl1Menu(e);
                                    mapControl1.Tools.LeftButtonTool = strCurrentTool;
                                    return;

                                }
                                else
                                {
                                    ShowMapControl1Menu(e);
                                }
                            }
                            else
                            {
                                ShowMapControl1Menu(e);
                            }
                        }
                    }
                }
            }
        }

        void mouseHook_MouseDown(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            e.Delta.ToString();

        }

        void mouseHook_MouseMove(object sender, MouseEventArgs e)
        {
            Form frmMain = this.MdiParent;
            if (frmMain == null) return;
            if (frmMain.ActiveMdiChild == null) return;
            if (frmMain.ActiveMdiChild.Text.ToString() != "Utilities - Mapping") return;  // This line prevents code from firing the event if this form is not currently active //
            double doubleX = e.X;
            double doubleY = e.Y;

        }

        private void ShowMapControl1Menu(MouseEventArgs e)
        {
            bbiGazetteerShowMatch.Enabled = true;

            beiGazetteerLoadObjectsWithinRange.Enabled = true;
            beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Always;  // Show Range Spinner menu item //

            bbiGazetteerLoadMapObjectsInRange.Enabled = true;
            bbiGazetteerLoadMapObjectsInRange.Caption = "Load  Map Objects in Range";

            bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Always;


            if (e != null) popupMenu_MapControl1.ShowPopup(new System.Drawing.Point(e.X, e.Y));  // May be null if triggered from barSubItemMapEdit_Popup //
        }

        private void popupMenu_MapControl1_Popup(object sender, EventArgs e)
        {
            // Used to prevent tracking of current mouse position on map when right click menu of Map displayed [used on Set Locality \ District Centre Point] //
            boolTrackXandY = false;

            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            int intMapObjectPoleCount = 0;
            int intMapObjectTreeCount = 0;
            int intTempMapObjectCount = 0;
            int intNotOnSurvey = 0;
            int intOnSurvey = 0;
            int intSurveyedCount = 0;
            int intNotSurveyedCount = 0;
            int intPoleCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole //
                        {
                            intMapObjectPoleCount++;
                            intPoleCount++;
                            if (Convert.ToInt32(f["SurveyDone"]) == -1)
                            {
                                intNotOnSurvey++;  // Not on survey so add to count //
                            }
                            else
                            {
                                intOnSurvey++;  // On survey so add to count //
                            }

                            if (Convert.ToInt32(f["SurveyDone"]) == 0)
                            {
                                intNotSurveyedCount++;  // Not yet surveyed so add to count //
                            }
                            else
                            {
                                intSurveyedCount++;  // Surveyed so add to count //
                            }
                        }
                        else if (Convert.ToInt32(f["ModuleID"]) == 2)  // Tree //
                        {
                            intMapObjectTreeCount++;                           
                        }
                    }
                }
                else if (irfc.BaseTable.Alias == "TempMapObjects")  // Ignore any selectable layers except TempMapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        intTempMapObjectCount++;
                    }
                }
            }
            bsiEditMapObject.Enabled = (intMapObjectPoleCount > 0 && iBool_AllowEdit ? true : false);
            bbiEditSelectedMapObjects.Enabled = (intMapObjectPoleCount > 0 ? true : false);
            bbiBlockEditselectedMapObjects.Enabled = (intMapObjectPoleCount > 1 && iBool_AllowEdit ? true : false);
            bbiDeleteMapObjects.Enabled = (intMapObjectPoleCount > 0 && iBool_AllowDelete ? true : false);
            bsiSurvey.Enabled = ((intMapObjectTreeCount > 0 || intMapObjectPoleCount > 0) && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiTransferPolesToSurvey.Enabled = (intNotOnSurvey > 0 && _SelectedSurveyID > 0 && iBool_AllowAdd ? true : false);
            bbiRemovePolesFromSurvey.Enabled = (intOnSurvey > 0 && _SelectedSurveyID > 0 && iBool_AllowDelete ? true : false);
            bbiSurveyPole.Enabled = (intOnSurvey == 1 && intNotOnSurvey == 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiSurveyPoleSetClear.Enabled = (intOnSurvey >= 1 && intNotSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);

            bbiShutdownRequired.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiShutdownClear.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiTrafficManagmentRequired.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiTrafficMap.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiTrafficManagmentClear.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiDeferredSet.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiClearDeferred.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiCreateAccessMap.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiCopyJobs.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiPasteJobs.Enabled = (intOnSurvey >= 1 && intSurveyedCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit && !string.IsNullOrEmpty(GlobalSettings.CopiedUtilityJobIDs) ? true : false);
            bbiPermission.Enabled = (intMapObjectTreeCount >= 1 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiPermissionView.Enabled = (intMapObjectTreeCount >= 1 && _SelectedSurveyID > 0 ? true : false);
            bbiSetPoleHistorical.Enabled = (intMapObjectPoleCount > 0 && iBool_AllowEdit ? true : false);
            bbiAddWorkToWorkOrder.Enabled = (intMapObjectPoleCount > 0 && _SelectedSurveyID > 0 && iBool_AllowEdit ? true : false);
            bbiViewPoleOwnership.Enabled = intMapObjectPoleCount > 0;
            bbiViewPictures.Enabled = (intMapObjectPoleCount > 0 && intMapObjectTreeCount == 0) || (intMapObjectPoleCount == 0 && intMapObjectTreeCount > 0);
            
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'MapMarker'");
            MapInfo.Data.IResultSetFeatureCollection irfc2 = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            conn.Close();
            conn.Dispose();
            //bbiDeleteAllMapMarkers.Enabled = (irfc2.Count <= 0 ? false : true);
        }

        private void popupMenu_MapControl1_CloseUp(object sender, EventArgs e)
        {
            // Used to prevent tracking of current mouse position on map when right click menu of Map displayed [used on Set Locality \ District Centre Point] //
            boolTrackXandY = true;
        }

        #endregion


        #region Distance Measurer

        private void psgDistTool_PSGDistanceToolPointAdded(object sender, WoodPlan5.PSGDistanceToolEventArgs e)
        {
            MapInfo.Geometry.DistanceUnit unit = MapInfo.Geometry.DistanceUnit.Meter;
            Double dblDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetCurrentLineLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            bsiDistanceMeasured.Caption = "Measured Distance: " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit);
            dMeasuredDistance = dblDist;
        }

        private void psgDistTool_PSGDistanceToolMove(object sender, WoodPlan5.PSGDistanceToolEventArgs e)
        {
            MapInfo.Geometry.DistanceUnit unit = MapInfo.Geometry.DistanceUnit.Meter;
            Double dblLastDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetLastSegmentLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            Double dblDist = System.Convert.ToDouble(String.Format("{0:N2}", e.GetCurrentLineLength(unit, MapInfo.Geometry.DistanceType.Spherical)));
            bsiDistanceMeasured.Caption = "Measured Distance: " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit) + " - Last Segment: " + dblLastDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit);
            dMeasuredDistance = dblDist;

            if (e.IsFinished) XtraMessageBox.Show("Total Distance Measured = " + dblDist.ToString() + " " + MapInfo.Geometry.CoordSys.DistanceUnitAbbreviation(unit) + ".", "Measure", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        #region Layer Manager

        public void Load_Layer_Manager()
        {
            beWorkspace.EditValue = strLoadedWorkspaceName;
            beWorkspace.Properties.Buttons[1].Enabled = (intLoadedWorkspaceOwner == GlobalSettings.UserID || GlobalSettings.PersonType == 0 ? true : false);
            this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list, intLoadedWorkspaceID, strMappingFilesPath);

            // Get any default thematic style set specified for the layer //
            int intMapObjectLayerRowID = GetLayerManagerMapObjectsLayerRow();
            if (intMapObjectLayerRowID >= 0)
            {
                GridView view = (GridView)gridControl5.MainView;
                intThematicStylingFieldHeaderID = Convert.ToInt32(view.GetRowCellValue(intMapObjectLayerRowID, "ThematicStyleSet"));
                intUseThematicStyling = Convert.ToInt32(view.GetRowCellValue(intMapObjectLayerRowID, "UseThematicStyling"));

                if (intThematicStylingFieldHeaderID > 0)
                {
                    intThematicStylingFieldHeaderType = 1;  // 0 = System Picklist, 1 = Saved Thematic Style Set //

                    // Populate Thematic Styling - Get Defaults to pass through to SP first then trigger event to load data //
                    int intDefaultSymbol = 0;
                    int intDefaultSymbolColour = 0;
                    int intDefaultSymbolSize = 0;
                    int intDefaultPolygonBoundaryColour = 0;
                    int intDefaultPolygonBoundaryWidth = 0;
                    int intDefaultPolygonFillColour = 0;
                    int intDefaultPolygonFillPattern = 0;
                    int intDefaultPolygonFillPatternColour = 0;
                    int intDefaultLineStyle = 0;
                    int intDefaultLineColour = 0;
                    int intDefaultLineWidth = 0;
                    int intDefaultSymbol2 = 0;
                    int intDefaultSymbol2Colour = 0;
                    int intDefaultSymbol2Size = 0;
                    DataRow dr = dsDefaultStyles.Tables[0].Rows[0];
                    intDefaultSymbol = Convert.ToInt32(dr["Symbol"]);
                    intDefaultSymbolColour = Convert.ToInt32(dr["SymbolColour"]);
                    intDefaultSymbolSize = Convert.ToInt32(dr["Size"]);
                    intDefaultPolygonBoundaryColour = Convert.ToInt32(dr["PolygonLineColour"]);
                    intDefaultPolygonBoundaryWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                    intDefaultPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                    intDefaultPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                    intDefaultPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                    intDefaultLineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                    intDefaultLineColour = Convert.ToInt32(dr["PolylineColour"]);
                    intDefaultLineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                    intDefaultSymbol2 = Convert.ToInt32(dr["Symbol2"]);
                    intDefaultSymbol2Colour = Convert.ToInt32(dr["Symbol2Colour"]);
                    intDefaultSymbol2Size = Convert.ToInt32(dr["Size2"]);
                    ChildForm_RefreshThematicItemDataSet(intThematicStylingFieldHeaderID, intThematicStylingFieldHeaderType, intDefaultSymbol, intDefaultSymbolColour, intDefaultSymbolSize, intDefaultPolygonBoundaryColour, intDefaultPolygonBoundaryWidth, intDefaultPolygonFillColour, intDefaultPolygonFillPattern, intDefaultPolygonFillPatternColour, intDefaultLineStyle, intDefaultLineColour, intDefaultLineWidth);

                    // Get Owner of SavedStyleSet if it is a SavedStyleSet //
                    if (intThematicStylingFieldHeaderID <= 0 || intThematicStylingFieldHeaderType == 0)  // StyleSet is a dynamic one [not Saved] //
                    {
                        intLoadedStyleSetOwnerID = 0;
                    }
                    else
                    {
                        DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetOwner = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                        GetOwner.ChangeConnectionString(strConnectionString);
                        try
                        {
                            string strIDandOwner = "";
                            strIDandOwner = GetOwner.sp01291_AT_Tree_Picker_Style_Get_owner(intThematicStylingFieldHeaderID).ToString();
                            intLoadedStyleSetOwnerID = Convert.ToInt32(strIDandOwner.Substring(0, strIDandOwner.IndexOf('|')));
                            strThematicStylingFieldName = strIDandOwner.Substring(strIDandOwner.IndexOf('|') + 1);

                            // Get Transparency Level //
                            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetTransparency = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                            GetTransparency.ChangeConnectionString(strConnectionString);
                            intMapObjectTransparency = Convert.ToInt32(GetTransparency.sp03077_AT_Tree_Picker_Get_Transparency(intThematicStylingFieldHeaderID));
                        }
                        catch (Exception)
                        {
                            intLoadedStyleSetOwnerID = 0;
                            strThematicStylingFieldName = "";
                        }
                    }
                }
                else  // Clear Thematics //
                {
                    dsThematicStyleItems.Clear();
                }
            }

            this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit, intLoadedWorkspaceID);

            Load_Map_Layers();

            // Update Legend //
            if (dockPanel5.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden)
            {
                if (intUseThematicStyling != 0)
                {
                    Create_Legend();
                }
                else  // No Thematics on so hide legend and uncheck Lengend button on toolbar //
                {
                    bbiLegend.Checked = false;
                }
            }
        }

        private void Load_Map_Layers()
        {
            mapControl1.Map.Clear();

            GridView view = (GridView)gridControl5.MainView;
            string strFileName = "";
            for (int i = view.DataRowCount - 1; i >= 0; i--)  // Load Map in reverse Order so Labels and Points are on top //
            {
                DataRow dr = view.GetDataRow(i);
                //if (Convert.ToInt32(dr["LayerType"]) != 2 && Convert.ToString(dr["LayerName"]) != "Map Objects")  // Background Layer //
                if (Convert.ToString(dr["LayerName"]) != "Map Object Labels" && Convert.ToString(dr["LayerName"]) != "Map Objects" && Convert.ToString(dr["LayerName"]) != "Temporary Map Objects")  // Background Layer //
                {
                    // Load Background Files //
                    strFileName = dr["LayerPath"].ToString();
                    if (strFileName.Substring(strFileName.Length - 3, 3).ToUpper() == "TAB")
                    {
                        MapTableLoader tl = new MapTableLoader(@strFileName);
                        try
                        {
                            tl.AutoPosition = false; // Set table loader options //
                            tl.StartPosition = 0;
                            mapControl1.Map.Load(tl);

                            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[0];
                            l.Name = strFileName;
                            l.Enabled = (Convert.ToInt32(view.GetRowCellValue(i, "LayerVisible")) == 0 ? false : true);  // Controls visibility //
                            MapInfo.Mapping.LayerHelper.SetSelectable(l, (Convert.ToInt32(view.GetRowCellValue(i, "LayerHitable")) == 0 ? false : true));
                            MapInfo.Mapping.LayerHelper.SetEditable(l, false);
                            MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

                            CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    else if (strFileName.Substring(strFileName.Length - 3, 3).ToUpper() == "SHP")
                    {
                        /*TableInfoShapefile ti = new TableInfoShapefile("EsriShape");
                        ti.TablePath = @strFileName;
                        MapInfo.Geometry.CoordSysFactory CSysFactory = Session.Current.CoordSysFactory;
                        MapInfo.Geometry.CoordSys coordSys = CSysFactory.CreateCoordSys("mapinfo:" + strDefaultMappingProjection + " Bounds (0,0) (5000000,5000000)");

                        ti.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(coordSys));
                        ti.Columns.Add(ColumnFactory.CreateDecimalColumn("AREA", 12, 3));
                        ti.Columns.Add(ColumnFactory.CreateDecimalColumn("PERIMETER", 12, 3));
                        ti.Columns.Add(ColumnFactory.CreateStringColumn("ID", 25));
                        ti.Columns.Add(ColumnFactory.CreateStringColumn("CAPTION", 35));
                        ti.Columns.Add(ColumnFactory.CreateIntColumn("STYLE"));

                        ti.DefaultStyle = new MapInfo.Styles.AreaStyle(

                        new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(2.0, MapInfo.Styles.LineWidthUnit.Pixel), (int)MapInfo.Styles.PatternStyle.Solid, Color.Red),

                        new MapInfo.Styles.SimpleInterior((int)MapInfo.Styles.PatternStyle.Cross, Color.Red, Color.White));

                        Table tableSHP = Session.Current.Catalog.OpenTable(ti);
                        MapTableLoader tl = new MapTableLoader(tableSHP);

                        try
                        {
                            tl.AutoPosition = true; // Set table loader options //
                            //tl.StartPosition = i;
                            //tl.EnableLayers = EnableLayers.Enable;
                            mapControl1.Map.Load(tl);

                            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[mapControl1.Map.Layers.Count - 1];
                            l.Name = strFileName;
                       }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }*/
                    }
                }
                else if (Convert.ToInt32(dr["LayerType"]) == 1 && Convert.ToString(dr["LayerName"]) == "Map Objects")
                {
                    intMapObjectSizing = (view.GetRowCellValue(i, "PointSize").ToString() == "Dynamic" ? -1 : 8);
                    LoadMapObjects();  // Load Map Objects Layer from Database //

                    foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                    {
                        if (l.Alias == "MapObjects")
                        {
                            MapInfo.Mapping.LayerHelper.SetSelectable(l, (Convert.ToInt32(view.GetRowCellValue(i, "LayerHitable")) == 0 ? false : true));
                            MapInfo.Mapping.LayerHelper.SetEditable(l, true);
                            MapInfo.Mapping.LayerHelper.SetInsertable(l, true);
                            //CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                            break;
                        }
                    }
                    //Load_Map_Label_Settings();
                    Update_Map_Object_Labels();
                }
                else if (Convert.ToInt32(dr["LayerType"]) == 1 && Convert.ToString(dr["LayerName"]) == "Temporary Map Objects")
                {
                    Create_Temp_Features_Layer();
                }
            }

            if (PreventDefaultMapScaleUse)  // Only set to true LoadMapObjects() event when screen has highlighted items passed in on load and map auto-scales to show them //
            {
                PreventDefaultMapScaleUse = false;
            }
            else
            {
                mapControl1.Map.Scale = (this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"].ToString() == null ? 2500 : Convert.ToInt32(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"]));  // Set Scale //
                // Attempt to Centre the Map //
                if (intPassedMapX == 0 || intPassedMapY == 0)
                {
                    if (String.IsNullOrEmpty(strVisibleIDs) && String.IsNullOrEmpty(strVisibleTreesIDs) && !String.IsNullOrEmpty(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"].ToString()) && !String.IsNullOrEmpty(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"].ToString()))
                    {
                        MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(Convert.ToDouble(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"]), Convert.ToDouble(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreY"]));
                        mapControl1.Map.Center = dpt1;
                    }
                }
                else
                {
                    MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(Convert.ToDouble(intPassedMapX), Convert.ToDouble(intPassedMapY));
                    mapControl1.Map.Center = dpt1;
                    intPassedMapX = 0;
                    intPassedMapY = 0;
                }
            }
            Clear_Stored_Map_Views();  // Make sure no Stored Map Views are left //
        }

        private bool Load_TAB_File_Layer(int intLayerPosition, string strFileName, DataRow dr)
        {
            MapTableLoader tl = new MapTableLoader(@strFileName);
            try
            {
                tl.AutoPosition = false; // Set table loader options //
                tl.StartPosition = intLayerPosition;
                mapControl1.Map.Load(tl);

                MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[intLayerPosition];
                l.Name = strFileName;

                MapInfo.Mapping.LayerHelper.SetSelectable(l, false);
                MapInfo.Mapping.LayerHelper.SetEditable(l, false);
                MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

                CreateFeatureLayerModifier(l, dr);  // Create Style Modifier for Layer to enable end-user customisation //
                l.Enabled = true;  // Make Layer visible //
                return true;
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map background [" + strFileName + "].\n\nError: " + ex.Message, "Load Map Layers", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        private bool Load_SHP_File_Layer(int intLayerPosition, string strFileName, DataRow dr)
        {
            return false;
        }

        /*
        private void Load_Map_Label_Settings()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjectLabels")
                {
                    int intMapObjectLabelLayerID = Convert.ToInt32(view.GetRowCellValue(i, "LayerID"));
                    
                    // *** Label Styling *** //
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = new SqlCommand("sp01271_AT_Tree_Picker_Workspace_label_font_settings", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@LayerID", intMapObjectLabelLayerID));
                    dsFontSettings.Clear();  // Remove old values first //
                    sdaFontSettings = new SqlDataAdapter(cmd);
                    sdaFontSettings.Fill(dsFontSettings, "Table");

                    SqlCommand UpdateCmd2 = new SqlCommand("sp01278_AT_Tree_Picker_Workspace_Label_Layer_Font_Save", SQlConn);
                    UpdateCmd2.CommandType = CommandType.StoredProcedure;
                    UpdateCmd2.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontName", SqlDbType.VarChar, 100, ParameterDirection.Input, 0, 0, "FontName", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontSize", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontSize", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontColour", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontColour", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@FontAngle", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FontAngle", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@Position", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Position", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@Offset", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "Offset", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@VisibleFromScale", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "VisibleFromScale", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@VisibleToScale", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "VisibleToScale", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@ShowOverlapping", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ShowOverlapping", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd2.Parameters.Add(new SqlParameter("@ShowDuplicates", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "ShowDuplicates", DataRowVersion.Current, false, null, "", "", ""));
                    sdaFontSettings.UpdateCommand = UpdateCmd2;
                    // *** End of Label Styling *** //


                    // *** Label and Tooltip Structure *** //
                    SQlConn = new SqlConnection(strConnectionString);
                    cmd = new SqlCommand("sp01268_AT_Tree_Picker_Workspace_layer_labelling", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@LayerID", intMapObjectLabelLayerID));
                    dsLabelStructure.Clear();  // Remove old values first //
                    sdaLabelStructure = new SqlDataAdapter(cmd);
                    sdaLabelStructure.Fill(dsLabelStructure, "Table");

                    SqlCommand DeleteCmd = new SqlCommand("dbo.sp01281_AT_Tree_Picker_Workspace_Layer_Labelling_Delete", SQlConn);
                    DeleteCmd.CommandType = CommandType.StoredProcedure;
                    DeleteCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    DeleteCmd.Parameters.Add(new SqlParameter("@LabelStructureID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LabelStructureID", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.DeleteCommand = DeleteCmd;

                    SqlCommand InsertCmd = new SqlCommand("dbo.sp01280_AT_Tree_Picker_Workspace_Layer_Labelling_insert", SQlConn);
                    InsertCmd.CommandType = CommandType.StoredProcedure;
                    InsertCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@StructureType", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "StructureType", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Prefix", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@ColumnName", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "ColumnName", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Suffix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Suffix", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@NewLine", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "NewLine", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@Seperator", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Seperator", DataRowVersion.Current, false, null, "", "", ""));
                    InsertCmd.Parameters.Add(new SqlParameter("@FieldOrder", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FieldOrder", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.InsertCommand = InsertCmd;

                    SqlCommand UpdateCmd = new SqlCommand("sp01279_AT_Tree_Picker_Workspace_Layer_Labelling_Save", SQlConn);
                    UpdateCmd.CommandType = CommandType.StoredProcedure;
                    UpdateCmd.Parameters.Add(new SqlParameter("@RETURN_VALUE", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 10, 0, null, DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@LabelStructureID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LabelStructureID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@LayerID", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "LayerID", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@StructureType", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "StructureType", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Prefix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Prefix", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@ColumnName", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "ColumnName", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Suffix", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Suffix", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@NewLine", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "NewLine", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@Seperator", SqlDbType.VarChar, 50, ParameterDirection.Input, 0, 0, "Seperator", DataRowVersion.Current, false, null, "", "", ""));
                    UpdateCmd.Parameters.Add(new SqlParameter("@FieldOrder", SqlDbType.Int, 4, ParameterDirection.Input, 10, 0, "FieldOrder", DataRowVersion.Current, false, null, "", "", ""));
                    sdaLabelStructure.UpdateCommand = UpdateCmd;
                    // *** End of Label and Tooltip Structure *** //

                    SQlConn.Close();
                    SQlConn.Dispose();
                    break;
                }
            }
        }
        */
        private void beWorkspace_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            frm_AT_Mapping_Tree_Picker_Save_Settings frmSave = null;
            switch (e.Button.Tag.ToString())
            {
                case "Select":
                    frm_AT_Mapping_Workspace_Select fm_AT_Mapping_Workspace_Select = new frm_AT_Mapping_Workspace_Select();
                    fm_AT_Mapping_Workspace_Select.GlobalSettings = GlobalSettings;
                    fm_AT_Mapping_Workspace_Select.intCurrentlyLoadedWorkspaceID = intLoadedWorkspaceID;
                    if (fm_AT_Mapping_Workspace_Select.ShowDialog() == DialogResult.OK)
                    {
                        intLoadedWorkspaceID = fm_AT_Mapping_Workspace_Select.intWorkspaceID;
                        intLoadedWorkspaceOwner = fm_AT_Mapping_Workspace_Select.intWorkspaceOwner;
                        strLoadedWorkspaceName = fm_AT_Mapping_Workspace_Select.strWorkspaceName;
                        Load_Layer_Manager();
                    }
                    break;
                case "Save":
                    if (intLoadedWorkspaceOwner != this.GlobalSettings.UserID)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save current workspace. Only the owner of a workspace can save changes to it.\n\nTip: If you need to save the changes, click the Save As button to create a new workspace with the current settings.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    frmSave = new frm_AT_Mapping_Tree_Picker_Save_Settings();
                    frmSave.GlobalSettings = GlobalSettings;
                    frmSave.strFormMode = "Save";
                    frmSave.boolSaveLoadedLayers = true;
                    frmSave.boolSaveLabelStructure = true;
                    frmSave.boolSaveThematicStyling = (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID ? true : false);
                    frmSave.boolSaveXY = false;
                    frmSave.boolSaveMapScale = false;

                    if (frmSave.ShowDialog() == DialogResult.OK)
                    {
                        this.fProgress = new frmProgress(20);  // Show Progress Window //
                        this.AddOwnedForm(fProgress);
                        fProgress.UpdateCaption("Saving Mapping Workspace, Please Wait...");
                        fProgress.Show();
                        Application.DoEvents();

                        if (Save_Workspace_Front_End_Changes_To_Database(frmSave.boolSaveLoadedLayers, frmSave.boolSaveLabelStructure, frmSave.boolSaveXY, frmSave.boolSaveMapScale, frmSave.boolSaveThematicStyling) == -1) return;

                        fProgress.UpdateProgress(20);
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                        if (this.GlobalSettings.ShowConfirmations == 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Workspace Changes Saved.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case "SaveAs":
                    frmSave = new frm_AT_Mapping_Tree_Picker_Save_Settings();
                    frmSave.GlobalSettings = GlobalSettings;
                    frmSave.strFormMode = "SaveAs";
                    frmSave.boolSaveLoadedLayers = true;
                    frmSave.boolSaveLabelStructure = true;
                    frmSave.boolSaveThematicStyling = (intLoadedStyleSetOwnerID == this.GlobalSettings.UserID ? true : false);
                    frmSave.boolSaveXY = false;
                    frmSave.boolSaveMapScale = false;

                    if (frmSave.ShowDialog() == DialogResult.OK)
                    {
                        this.fProgress = new frmProgress(10);  // Show Progress Window //
                        this.AddOwnedForm(fProgress);
                        fProgress.UpdateCaption("Saving Mapping Workspace, Please Wait...");
                        fProgress.Show();
                        Application.DoEvents();

                        int intNewWorkspaceID = 0;
                        // First generate Workspace based on current workspace //
                        DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter CreateWorkspace = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                        CreateWorkspace.ChangeConnectionString(strConnectionString);
                        try
                        {
                            intNewWorkspaceID = Convert.ToInt32(CreateWorkspace.sp01292_AT_Tree_Picker_Workspace_SaveAs(this.GlobalSettings.UserID, frmSave.strWorkspaceName, frmSave.strRemarks, frmSave.intDefaultWorkspace, frmSave.intShareType, frmSave.strShareWithGroupIDs, intLoadedWorkspaceID));
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close(); // Close Progress Window //
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the new Mapping Workspace [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress.UpdateProgress(10);

                        // Second, iterate through all the mapping workpace related datasets in memory and switch IDs from old value to new value //
                        // Workspace Layers //
                        int intOldMapObjectLabel_LayerID = 0;
                        int intNewMapObjectLabel_LayerID = 0;
                        //int intFoundRow = GridControl.InvalidRowHandle;
                        SqlDataAdapter sda = new SqlDataAdapter();
                        DataSet ds = new DataSet("NewDataSet");
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = new SqlCommand("sp01293_AT_Tree_Picker_Workspace_SaveAs_GetNewLayerIDs", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@WorkspaceID", intNewWorkspaceID));
                        ds.Clear();  // Remove old values first //
                        sda = new SqlDataAdapter(cmd);
                        try
                        {
                            sda.Fill(ds, "Table");
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close(); // Close Progress Window //
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved layers [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        bool boolToggleDelete = false;
                        foreach (DataRow drTarget in this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.Rows)
                        {
                            DataRow[] drFoundRows;
                            if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                            {
                                boolToggleDelete = true;
                                drTarget.RejectChanges();
                            }
                            else if (drTarget.RowState == DataRowState.Added)  // No LayerID //
                            {
                                drTarget["WorkspaceID"] = intNewWorkspaceID;
                                continue;  // Abort rest of loop //
                            }
                            drFoundRows = ds.Tables[0].Select("CreatedFromLayerID = " + drTarget["LayerID", DataRowVersion.Original].ToString());
                            if (drFoundRows.Length == 1)
                            {
                                DataRow drFoundRow = drFoundRows[0];
                                drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                if (drTarget["LayerName", DataRowVersion.Original].ToString() == "Map Object Labels")
                                {
                                    intOldMapObjectLabel_LayerID = Convert.ToInt32(drTarget["LayerID"]);  // Store this for later [need for Label structure and font] //
                                    intNewMapObjectLabel_LayerID = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                            }
                            drTarget["WorkspaceID"] = intNewWorkspaceID;

                            if (boolToggleDelete)
                            {
                                boolToggleDelete = false;
                                drTarget.AcceptChanges();
                                drTarget.Delete();
                            }
                        }
                        fProgress.UpdateProgress(10);

                        if (intOldMapObjectLabel_LayerID > 0 && intNewMapObjectLabel_LayerID > 0)
                        {
                            // Font Settings //
                            cmd = new SqlCommand("sp01294_AT_Tree_Picker_Workspace_SaveAs_GetNewFontSettingsID", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@LayerID", intNewMapObjectLabel_LayerID));
                            ds.Clear();  // Remove old values first //
                            sda = new SqlDataAdapter(cmd);
                            try
                            {
                                sda.Fill(ds, "Table");
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close(); // Close Progress Window //
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved font settings [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }
                            /*
                            foreach (DataRow drTarget in dsFontSettings.Tables[0].Rows)
                            {
                                DataRow[] drFoundRows;
                                if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                                {
                                    boolToggleDelete = true;
                                    drTarget.RejectChanges();
                                }
                                drFoundRows = ds.Tables[0].Select("CreatedFromFontID = " + drTarget["FontID"].ToString());
                                if (drFoundRows.Length == 1)
                                {
                                    DataRow drFoundRow = drFoundRows[0];
                                    drTarget["FontID"] = Convert.ToInt32(drFoundRow["FontID"]);
                                    drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                                if (boolToggleDelete)
                                {
                                    boolToggleDelete = false;
                                    drTarget.AcceptChanges();
                                    drTarget.Delete();
                                }
                            }
                            */
                            // Layer Labelling Structure //
                            cmd = new SqlCommand("sp01295_AT_Tree_Picker_Workspace_SaveAS_GetNewLayerLabellingIDs", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@LayerID", intNewMapObjectLabel_LayerID));
                            ds.Clear();  // Remove old values first //
                            sda = new SqlDataAdapter(cmd);
                            try
                            {
                                sda.Fill(ds, "Table");
                            }
                            catch (Exception ex)
                            {
                                if (fProgress != null)
                                {
                                    fProgress.Close(); // Close Progress Window //
                                    fProgress = null;
                                }
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while updating the On-screen Layer Manager with the saved Label\tooltip structure settings [" + ex.Message + "]!\n\nTry doing a Save As again - if the problem persists, contact Technical Support.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }


                            // Update the remaining rows //
                            /*foreach (DataRow drTarget in dsLabelStructure.Tables[0].Rows)
                            {
                                DataRow[] drFoundRows;
                                if (drTarget.RowState == DataRowState.Deleted)  // Set flag then reject changes to ghet deleted row back so we can change it's values then accept values to commit new IDs to DB then re-delete row at very end. //
                                {
                                    boolToggleDelete = true;
                                    drTarget.RejectChanges();
                                }
                                else if (drTarget.RowState == DataRowState.Added)  // No LabelStructureID //
                                {
                                    drTarget["LayerID"] = intNewMapObjectLabel_LayerID;
                                    continue;  // Abort rest of loop //
                                }
                                drFoundRows = ds.Tables[0].Select("CreatedFromLabelStructureID = " + drTarget["LabelStructureID"].ToString());
                                if (drFoundRows.Length == 1)
                                {
                                    DataRow drFoundRow = drFoundRows[0];
                                    drTarget["LabelStructureID"] = Convert.ToInt32(drFoundRow["LabelStructureID"]);
                                    drTarget["LayerID"] = Convert.ToInt32(drFoundRow["LayerID"]);
                                }
                                if (boolToggleDelete)
                                {
                                    boolToggleDelete = false;
                                    drTarget.AcceptChanges();
                                    drTarget.Delete();
                                }
                            }*/
                        }
                        fProgress.UpdateProgress(10);

                        // Third, Switch the loaded workspace to the new one so when scale and X and Y are saved, the new workspace is updated. //
                        this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit, intNewWorkspaceID);

                        // Forth, fire the normal Save code to write the pending Mapping Workspace front-end changes to the database //
                        if (Save_Workspace_Front_End_Changes_To_Database(frmSave.boolSaveLoadedLayers, frmSave.boolSaveLabelStructure, frmSave.boolSaveXY, frmSave.boolSaveMapScale, frmSave.boolSaveThematicStyling) == -1) return;

                        // Lastly, switch the currently loaded workspace to this new workspace //
                        intLoadedWorkspaceID = intNewWorkspaceID;
                        intLoadedWorkspaceOwner = this.GlobalSettings.UserID;
                        strLoadedWorkspaceName = frmSave.strWorkspaceName;
                        beWorkspace.EditValue = strLoadedWorkspaceName;

                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                        if (this.GlobalSettings.ShowConfirmations == 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Workspace Changes Saved as New Workspace.", "Save Workspace As", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private int Save_Workspace_Front_End_Changes_To_Database(bool boolSaveLoadedLayers, bool boolSaveLabelStructure, bool boolSaveXY, bool boolSaveMapScale, bool boolSaveThematicStyling)
        {
            if (boolSaveLoadedLayers)  // Save Layers //
            {
                this.sp01288ATTreePickerWorkspacelayerslistBindingSource.EndEdit();
                try
                {
                    this.sp01288_AT_Tree_Picker_Workspace_layers_listTableAdapter.Update(dataSet_AT_TreePicker);  // Update, Delete queries defined in Table Adapter //
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Layer Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveLabelStructure)  // Save Label\Tooltip Structure and Font Settings //
            {
                try  // Label\Tooltip Structure //
                {
                    /*sdaLabelStructure.DeleteCommand.Connection.ConnectionString = strConnectionString;  // Need to reapply connection string as it seems to get lost after creating in Load_Map_Label_Settings Event //
                    sdaLabelStructure.InsertCommand.Connection.ConnectionString = strConnectionString;
                    sdaLabelStructure.UpdateCommand.Connection.ConnectionString = strConnectionString;

                    this.sdaLabelStructure.Update(dsLabelStructure);  // Insert, Update and Delete queries defined in code in Load_Label_Settings Method //
                     * */
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while saving the Object Labelling Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
                try  // Font Settings //
                {
                    /*
                    sdaFontSettings.UpdateCommand.Connection.ConnectionString = strConnectionString;  // Need to reapply connection string as it seems to get lost after creating in Load_Map_Label_Settings Event //
                    this.sdaFontSettings.Update(dsFontSettings);  // Update query defined in code in Load_Label_Settings Method //
                    */
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Object Labelling Font Settings [" + ex.Message + "]!\n\nTry saving again again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveXY || boolSaveMapScale) // Save XY and/or Scale //
            {
                if (boolSaveXY)
                {
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreX"] = Convert.ToDouble(mapControl1.Map.Center.x);
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultCentreY"] = Convert.ToDouble(mapControl1.Map.Center.y);
                }
                if (boolSaveMapScale)
                {
                    this.dataSet_AT_TreePicker.sp01274_AT_Tree_Picker_Workspace_Edit.Rows[0]["DefaultScale"] = Convert.ToInt32(mapControl1.Map.Scale);
                }
                try
                {
                    this.sp01274_AT_Tree_Picker_Workspace_EditTableAdapter.Update(dataSet_AT_TreePicker);
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    XtraMessageBox.Show("An error occurred while saving the Workspace Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            fProgress.UpdateProgress(20);

            if (boolSaveThematicStyling) // Save Thematics //
            {
                // Clear the Styling Items for the Style Set header first [they will be recreated in SaveStyleSetItemInfo method call] //
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter ClearStyleItems = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                ClearStyleItems.ChangeConnectionString(strConnectionString);
                try
                {
                    ClearStyleItems.sp01261_AT_Tree_Picker_Style_Set_Delete_Style_Items(intThematicStylingFieldHeaderID);
                }
                catch (Exception ex)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while saving the Thematic Styling Settings (Clearing Original Values) [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
                if (SaveStyleSetItemInfo(intThematicStylingFieldHeaderID) == -1)
                {
                    if (fProgress != null)
                    {
                        fProgress.Close(); // Close Progress Window //
                        fProgress = null;
                    }
                    return -1;
                }
            }
            return 1;
        }

        private int SaveStyleSetItemInfo(int HeaderID)
        {
            // Save Style Set Items...// 
            DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter SaveItem = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
            SaveItem.ChangeConnectionString(strConnectionString);

            int intThematicItemID = 0;
            int intThematicSetID = 0;
            string strItemDescription = "";
            int intPicklistItemID = 0;
            int intPointSymbol = 0;
            int intPointSize = 0;
            int intPointColour = 0;
            int intPolygonFillColour = 0;
            int intPolygonFillPattern = 0;
            int intPolygonFillPatternColour = 0;
            int intPolygonLineWidth = 0;
            int intPolygonLineColour = 0;
            int intPolylineStyle = 0;
            int intPolylineColour = 0;
            int intPolylineWidth = 0;
            int intShowInLegend = 0;
            int intItemOrder = 0;
            decimal decBandStart = (decimal)0.00;
            decimal decBandEnd = (decimal)0.00;

            foreach (DataRow dr in dsThematicStyleItems.Tables[0].Rows)
            {
                intThematicItemID = Convert.ToInt32(dr["ItemID"]);
                intThematicSetID = HeaderID;
                strItemDescription = Convert.ToString(dr["ItemDescription"]);
                intPicklistItemID = Convert.ToInt32(dr["PicklistItemID"]);
                intPointSymbol = Convert.ToInt32(dr["Symbol"]);
                intPointSize = Convert.ToInt32(dr["Size"]);
                intPointColour = Convert.ToInt32(dr["SymbolColour"]);
                intPolygonFillColour = Convert.ToInt32(dr["PolygonFillColour"]);
                intPolygonFillPattern = Convert.ToInt32(dr["PolygonFillPattern"]);
                intPolygonFillPatternColour = Convert.ToInt32(dr["PolygonFillPatternColour"]);
                intPolygonLineWidth = Convert.ToInt32(dr["PolygonLineWidth"]);
                intPolygonLineColour = Convert.ToInt32(dr["PolygonLineColour"]);
                intPolylineStyle = Convert.ToInt32(dr["PolylineStyle"]);
                intPolylineColour = Convert.ToInt32(dr["PolylineColour"]);
                intPolylineWidth = Convert.ToInt32(dr["PolylineWidth"]);
                intShowInLegend = Convert.ToInt32(dr["ShowInLegend"]);
                intItemOrder = Convert.ToInt32(dr["Order"]);
                decBandStart = Convert.ToDecimal(dr["StartBand"]);
                decBandEnd = Convert.ToDecimal(dr["EndBand"]);
                try
                {
                    SaveItem.sp01260_AT_Tree_Picker_Style_Set_Save_Item("insert", 0, intThematicSetID, strItemDescription, intPicklistItemID, intPointSymbol, intPointSize, intPointColour, intPolygonFillColour, intPolygonFillPattern, intPolygonFillPatternColour, intPolygonLineWidth, intPolygonLineColour, intPolylineStyle, intPolylineColour, intPolylineWidth, intShowInLegend, intItemOrder, decBandStart, decBandEnd, intMapObjectTransparency);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while saving the Thematic Styling Settings [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Workspace", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return -1;
                }
            }
            return 1;
        }

        private void bbiLayerManagerProperties_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the layer to view the properties for before proceeding.", "View Layer Properties", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strLayerName = view.GetFocusedRowCellValue("LayerName").ToString();
            switch (strLayerName)
            {
                case "Map Objects":
                    // Create clones of DataSets used so if changes are made then cancel is clicked, we can roll back the changes //
                    DataSet dsDefaultStyles_backup = (DataSet)dsDefaultStyles.Copy();
                    DataSet dsThematicStyleItems_backup = (DataSet)dsThematicStyleItems.Copy();
                    DataSet dsThematicStyleSets_backup = (DataSet)dsThematicStyleSets.Copy();
                    int intUseThematicStyling_backup = intUseThematicStyling;
                    int intLoadedStyleSetOwnerID_backup = intLoadedStyleSetOwnerID;

                    frm_UT_Mapping_Styles frm_styles = new frm_UT_Mapping_Styles();
                    //frm_styles.MdiParent = this.MdiParent;
                    frm_styles.GlobalSettings = this.GlobalSettings;
                    frm_styles.dsDefaultStyle = this.dsDefaultStyles;
                    frm_styles.dsThematicItems = this.dsThematicStyleItems;
                    frm_styles.dsThematicSets = this.dsThematicStyleSets;
                    frm_styles.intThematicStylingFieldHeaderID = this.intThematicStylingFieldHeaderID;
                    frm_styles.intThematicStylingFieldHeaderType = this.intThematicStylingFieldHeaderType;
                    frm_styles.intLoadedStyleSetOwnerID = this.intLoadedStyleSetOwnerID;
                    frm_styles.intUseThematicStyling = Convert.ToInt32(view.GetFocusedRowCellValue("UseThematicStyling"));
                    frm_styles.strPointSize = Convert.ToString(view.GetFocusedRowCellValue("PointSize"));
                    frm_styles.strThematicStylingFieldName = this.strThematicStylingFieldName;
                    frm_styles.intTransparency = this.intMapObjectTransparency;
                    frm_styles.frmCallingForm = this;
                    if (frm_styles.ShowDialog() == DialogResult.OK)
                    {
                        if (frm_styles.intLoadedStyleSetID != 0) view.SetFocusedRowCellValue("ThematicStyleSet", frm_styles.intLoadedStyleSetID);  // Ignore if 0 as these are not saved sets [just temporary] //
                        view.SetFocusedRowCellValue("UseThematicStyling", frm_styles.intUseThematicStyling);
                        view.SetFocusedRowCellValue("PointSize", frm_styles.strPointSize);
                        intUseThematicStyling = frm_styles.intUseThematicStyling;
                        intLoadedStyleSetOwnerID = frm_styles.intLoadedStyleSetOwnerID;
                        strThematicStylingFieldName = frm_styles.strThematicStylingFieldName;  // Shown on Legend //
                        intMapObjectTransparency = frm_styles.intTransparency;

                        if (view.GetFocusedRowCellValue("PointSize").ToString().StartsWith("Fix"))
                        {
                            string strSize = BaseObjects.ExtensionFunctions.ExtractNumbersFromString(view.GetFocusedRowCellValue("PointSize").ToString());
                            if (string.IsNullOrEmpty(strSize))
                            {
                                intMapObjectSizing = 8;
                            }
                            else
                            {
                                intMapObjectSizing = Convert.ToInt32(strSize);
                            }
                        }
                        else
                        {
                            intMapObjectSizing = -1; // Dynamic sizing based on Crown Diameter //
                        }
                        UpdateMapObjectsDisplayed(true);

                        // Update Legend //
                        if (dockPanel5.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                        {
                            if (intUseThematicStyling != 0)
                            {
                                Create_Legend();
                            }
                            else  // No Thematics on so hide legend and uncheck Lengend button on toolbar //
                            {
                                bbiLegend.Checked = false;
                            }
                        }
                    }
                    else // Use Cloned copies to roll back the changes //
                    {
                        dsDefaultStyles = (DataSet)dsDefaultStyles_backup.Copy();
                        dsThematicStyleItems = (DataSet)dsThematicStyleItems_backup.Copy();
                        dsThematicStyleSets = (DataSet)dsThematicStyleSets_backup.Copy();
                        intUseThematicStyling = intUseThematicStyling_backup;
                        intLoadedStyleSetOwnerID = intLoadedStyleSetOwnerID_backup;
                    }
                    break;
                case "Map Object Labels":
                    frm_UT_Mapping_Layer_Label_Properties fChildForm2 = new frm_UT_Mapping_Layer_Label_Properties();
                    fChildForm2.GlobalSettings = this.GlobalSettings;
                    fChildForm2.FormPermissions = this.FormPermissions;
                    fChildForm2.fProgress = fProgress;
                    fChildForm2._Transparency = this._Transparency;
                    fChildForm2._LineColour = this._LineColour;
                    fChildForm2._LineWidth = this._LineWidth;
                    fChildForm2._LabelFontName = this._LabelFontName;
                    fChildForm2._LabelFontSize = this._LabelFontSize;
                    fChildForm2._LabelFontColour = this._LabelFontColour;
                    fChildForm2._LabelAngle = this._LabelAngle;
                    fChildForm2._LabelPosition = this._LabelPosition;
                    fChildForm2._LabelOffset = this._LabelOffset;
                    fChildForm2._LabelVisibleRangeFrom = this._LabelVisibleRangeFrom;
                    fChildForm2._LabelVisibleRangeTo = this._LabelVisibleRangeTo;
                    fChildForm2._LabelOverlap = this._LabelOverlap;
                    fChildForm2._LabelDuplicates = this._LabelDuplicates;
                    if (fChildForm2.ShowDialog() == DialogResult.OK)
                    {
                        // OK clicked so update current row //
                        view.SetFocusedRowCellValue("Transparency", fChildForm2._Transparency);
                        view.SetFocusedRowCellValue("LineColour", fChildForm2._LineColour);
                        view.SetFocusedRowCellValue("LineWidth", fChildForm2._LineWidth);
                        this._Transparency = fChildForm2._Transparency;
                        this._LineColour = fChildForm2._LineColour;
                        this._LineWidth = fChildForm2._LineWidth;
                        this._LabelFontName = fChildForm2._LabelFontName;
                        this._LabelFontSize = fChildForm2._LabelFontSize;
                        this._LabelFontColour = fChildForm2._LabelFontColour;
                        this._LabelAngle = fChildForm2._LabelAngle;
                        this._LabelPosition = fChildForm2._LabelPosition;
                        this._LabelOffset = fChildForm2._LabelOffset;
                        this._LabelVisibleRangeFrom = fChildForm2._LabelVisibleRangeFrom;
                        this._LabelVisibleRangeTo = fChildForm2._LabelVisibleRangeTo;
                        this._LabelOverlap = fChildForm2._LabelOverlap;
                        this._LabelDuplicates = fChildForm2._LabelDuplicates;
                        // Update Map //                       
                        Update_Map_Object_Labels();
                    }
                    break;
                case "Temporary Map Object Labels":
                    return;

                default:  // Map backgrounds (type = 0: Raster or 1: Vector //
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LayerType")) == 0)  // Raster //
                    {
                        frm_AT_Mapping_Properties_Layer_Raster fChildForm3 = new frm_AT_Mapping_Properties_Layer_Raster();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intGreyScale = Convert.ToInt32(view.GetFocusedRowCellValue("GreyScale"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("GreyScale", fChildForm3.intGreyScale);

                            // Update Map //
                            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == dr["LayerPath"].ToString())
                                {
                                    Update_Layer_Setup(l, dr);
                                    break;
                                }
                            }
                        }
                    }
                    else  // Vector //
                    {
                        frm_AT_Mapping_Properties_Layer_Vector fChildForm3 = new frm_AT_Mapping_Properties_Layer_Vector();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.FormPermissions = this.FormPermissions;
                        fChildForm3.fProgress = fProgress;
                        fChildForm3.intPreserveDefaultStyling = Convert.ToInt32(view.GetFocusedRowCellValue("PreserveDefaultStyling"));
                        fChildForm3.intTransparency = Convert.ToInt32(view.GetFocusedRowCellValue("Transparency"));
                        fChildForm3.intLineColour = Convert.ToInt32(view.GetFocusedRowCellValue("LineColour"));
                        fChildForm3.intLineWidth = Convert.ToInt32(view.GetFocusedRowCellValue("LineWidth"));

                        if (fChildForm3.ShowDialog() == DialogResult.OK)
                        {
                            // OK clicked so update current row //
                            view.SetFocusedRowCellValue("PreserveDefaultStyling", fChildForm3.intPreserveDefaultStyling);
                            view.SetFocusedRowCellValue("Transparency", fChildForm3.intTransparency);
                            view.SetFocusedRowCellValue("LineColour", fChildForm3.intLineColour);
                            view.SetFocusedRowCellValue("LineWidth", fChildForm3.intLineWidth);

                            // Update Map //
                            DataRow dr = view.GetDataRow(view.FocusedRowHandle);
                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == dr["LayerPath"].ToString())
                                {
                                    Update_Layer_Setup(l, dr);
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private void bbiAddLayer_ItemClick(object sender, ItemClickEventArgs e)
        {
            AddRecord();
        }

        private void bbiRemoveLayer_ItemClick(object sender, ItemClickEventArgs e)
        {
            DeleteRecord();
        }

        private void AddRecord()
        {
            GridView view = (GridView)gridControl5.MainView;
            // Get Default Mapping Path //
            string strDefaultPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                //strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesMapBackgroundFolder").ToString().ToLower();
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, (this.GlobalSettings.SystemDataTransferMode != "Tablet" ? "AmenityTreesMapBackgroundFolder" : "AmenityTreesMapBackgroundFolderTablet")).ToString().ToLower();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Select Mapping File(s) - No Mapping Background Folder Location has been specified in the System Settings table.", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Following will determin where to insert the new layers in the Map [if MapObjects / MapObjectLabels are at the end, then new ayers go in just before them otherwise they go on the end] //
            //bool boolAddPriorToMapObjects = false;
            //int intFoundRow1 = GetLayerPosition("MapObjects");
            //int intFoundRow2 = GetLayerPosition("MapObjectLabels");

            //if (intFoundRow1 == 0 || intFoundRow2 == 0) boolAddPriorToMapObjects = true;  // Remember, that Layer order is actually reverse of GridView order //

            // Process all selected files //
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "tab";
                //dlg.Filter = "TAB Files (*.tab)|*.tab|Shape Files (*.shp)|*.shp";
                dlg.Filter = "TAB Files (*.tab)|*.tab";
                if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                dlg.Multiselect = true;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    string strTempFileName = "";
                    int intFileType = 0;  // 0 = Raster, 1 = Vector //
                    int intMaxRowsAtStart = view.DataRowCount;
                    bool boolProceedWithLoadLayerManager = false;
                    foreach (string filename in dlg.FileNames)
                    {
                        if (strDefaultPath != "")
                            if (!filename.ToLower().StartsWith(strDefaultPath))
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Mapping - Background Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                //view.EndDataUpdate();
                                //view.EndSort();
                                //view.EndUpdate();
                                return;
                            }
                        strTempFileName = filename.Substring(strDefaultPath.Length);

                        // Determin if the file is Raster or Vector //
                        try
                        {
                            string strFileText = System.IO.File.ReadAllText(filename);
                            intFileType = (!strFileText.Contains("Type \"RASTER\"") ? 1 : 0);
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to select mapping file: " + filename + ".\n\nThe following error occurred while attempting to determin it's type - [" + Ex.Message + "].", "Select Files(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            continue;
                        }

                        // Data row defined as it must be passed through to Add Map Layer Process [Used by the LayerModifier call] However, it is not added to the grid yet! //
                        DataRow drAdd = this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.NewRow();
                        drAdd["WorkSpaceID"] = intLoadedWorkspaceID;
                        drAdd["LayerType"] = intFileType;
                        drAdd["LayerTypeDescription"] = (intFileType == 0 ? "Raster" : "Vector");
                        drAdd["LayerName"] = System.IO.Path.GetFileName(strTempFileName);
                        drAdd["LayerPath"] = strDefaultPath + strTempFileName;
                        drAdd["LayerOrder"] = 3; // Insert after Map Objects,  Map Labels and Temporary Map Objects //
                        drAdd["LayerVisible"] = 1;
                        drAdd["LayerHitable"] = 0;
                        drAdd["PreserveDefaultStyling"] = 0;
                        drAdd["Transparency"] = 0;
                        drAdd["LineColour"] = -16777216;
                        drAdd["LineWidth"] = 1;
                        drAdd["GreyScale"] = 0;
                        drAdd["ThematicStyleSet"] = 0;
                        drAdd["PointSize"] = 8;
                        drAdd["DummyLayerPathForSave"] = strTempFileName;  // This is the value stored in the DB should the workspace be saved! //

                        if (strTempFileName.Substring(strTempFileName.Length - 3, 3).ToUpper() == "TAB")
                        {
                            boolProceedWithLoadLayerManager = Load_TAB_File_Layer(3, strDefaultPath + strTempFileName, drAdd);  // Load TAB File into Map //

                        }
                        else  // Shape File //
                        {
                            boolProceedWithLoadLayerManager = Load_SHP_File_Layer(3, strDefaultPath + strTempFileName, drAdd);  // Load SHP File into Map //
                        }

                        if (boolProceedWithLoadLayerManager)  // Load of TAB\SHP File was successful so add layer into grid now //
                        {
                            view.BeginSort();
                            view.BeginDataUpdate();
                            for (int i = 3; i < view.DataRowCount; i++)  // Add 1 to existing LayerOrders //
                            {
                                view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) + 1);
                            }
                            view.EndDataUpdate();
                            view.EndSort();
                            this.dataSet_AT_TreePicker.sp01288_AT_Tree_Picker_Workspace_layers_list.Rows.Add(drAdd);
                        }
                    }
                    mapControl1.Map.Invalidate();  // Force Map to Repaint itself //
                }
            }
        }

        private void DeleteRecord()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                XtraMessageBox.Show("Select the layer to be removed by clicking on it first then try again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.GetFocusedRowCellValue("LayerName").ToString() == "Map Object Labels" || view.GetFocusedRowCellValue("LayerName").ToString() == "Map Objects" || view.GetFocusedRowCellValue("LayerName").ToString() == "Temporary Map Objects")
            {
                XtraMessageBox.Show("You cannot remove the Map Object, Map Object Label and Temporary Map Object layers. Select a different layer to be removed by clicking on it first then try again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.DataRowCount <= 4)
            {
                XtraMessageBox.Show("Unable to remove current layer... A Mapping Workspace must have at least 4 Map Layers [Map Objects, Map Object Labels, Temporary Map Objects and at least one background layer].\n\nTip: If you need to remove the current layer, add another map background layer first then try removing this layer again.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You have 1 Map Layer selected for removal!\n\nProceed?", "Remove Map Layer", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                // Remove layer from map then remove it from the grid //
                try
                {
                    mapControl1.Map.Layers.RemoveAt(GetLayerPosition(view.GetFocusedRowCellValue("LayerPath").ToString()));
                    view.DeleteRow(view.FocusedRowHandle);  // Delete record from grid //
                    view.BeginSort();
                    view.BeginDataUpdate();
                    for (int i = 0; i < view.DataRowCount; i++)  // Adjust LayerOrders //
                    {
                        view.SetRowCellValue(i, "LayerOrder", i);
                    }
                    view.EndDataUpdate();
                    view.EndSort();

                    mapControl1.Map.Invalidate();  // Force Map to Repaint itself //
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("Unable to remove selected Map Layer - the following error occurred [" + Ex.Message + "]. Try removint the layer again. If the problem persisits contact Technical Support.", "Remove Map Layer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private int GetLayerPosition(string strLayerName)
        {
            int i = 0;
            foreach (IMapLayer Layer in this.mapControl1.Map.Layers)
            {
                if (Layer.Name.ToUpper() == strLayerName.ToUpper()) break;
                i = i + 1;
            }
            return i;
        }

        private void Update_Layer_Setup(MapInfo.Mapping.IMapLayer l, DataRow dr)
        {
            // Remove any existing feature override style modifier if it exists //
            FeatureLayer layer = mapControl1.Map.Layers[mapControl1.Map.Layers.IndexOf(l)] as FeatureLayer;
            try
            {
                layer.Modifiers.RemoveAt(0);
            }
            catch (Exception)
            {
            }

            if (l.Type == LayerType.Normal)  // Vector Layer //
            {
                // Create a new feature override style modifier if PreserveDefaultStyling switched off //
                if (Convert.ToInt32(dr["PreserveDefaultStyling"]) == 0)  // Allow styling according to criteria //
                {
                    CompositeStyle style = new CompositeStyle();
                    style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(dr["LineWidth"]), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"]))));

                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.White);
                    ((SimpleLineStyle)style.AreaStyle.Border).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));
                    style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100))), Color.FromArgb(Convert.ToInt32(dr["LineColour"])));

                    FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                    modifier.Style = style;
                    modifier.Name = "FeatureStyleModifier1";
                    layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //

                }
            }
            else if (l.Type == LayerType.Raster)  // Raster Layer //
            {
                // Create a feature override style modifier //
                CompositeStyle style = new CompositeStyle();
                style.RasterStyle.Grayscale = (Convert.ToInt32(dr["GreyScale"]) == 1 ? true : false);
                //style.RasterStyle.Transparent = true;    // *** This line commented out otherwise monocrome image layers don't display! *** //
                style.RasterStyle.Alpha = (int)(255 * (1 - (Convert.ToDouble(dr["Transparency"]) / 100)));  // Controls Transparency //

                FeatureOverrideStyleModifier modifier = new FeatureOverrideStyleModifier();
                modifier.Style = style;
                modifier.Name = "FeatureStyleModifier1";
                layer.Modifiers.Append(modifier);  // *** Allow switching on and off *** //
            }

        }

        private void Update_Map_Object_Labels()
        {
            // Get Object Label Layer - Layer ID //
            // Load Label Properties from Mapping_Workspace_Layer_Label_Font passing Object Label Layer ID as a parameter //
            // Set Label styling info according to data from Mapping_Workspace_Layer_Label_Font record //
            // Load Label and Tooltip structure from Mapping_Workspace_Layer_Label_Structure table //
            // Parse Label structure to form string value //
            // Parse ToolTip structure to form string value //
            // Update map with Label and Tootip structure //

            GridView view = (GridView)gridControl5.MainView;
            if (view == null) return;

            i_str_label_structure = "ObjectReference";
            i_str_tooltip_structure = "Tooltip";

            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (view.GetRowCellValue(i, "LayerPath").ToString() == "MapObjectLabels")
                {
                    int intTransparency = Convert.ToInt32(view.GetRowCellValue(i, "Transparency"));
                    int intLineColour = Convert.ToInt32(view.GetRowCellValue(i, "LineColour"));
                    int intLineWidth = Convert.ToInt32(view.GetRowCellValue(i, "LineWidth"));

                    // Close any Existing Label Layer First - Enumerate through label layers only then reconstruct Label Layer //
                    MapLayerEnumerator mle = mapControl1.Map.Layers.GetMapLayerEnumerator(MapLayerFilterFactory.FilterByLayerType(LayerType.Label));
                    while (mle.MoveNext())
                    {
                        LabelLayer current = mle.Current as LabelLayer;
                        if (current != null) mapControl1.Map.Layers.Remove(current);
                    }

                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

                    // Create a SimpleLineStyle to be used as the callout line's style //
                    MapInfo.Styles.SimpleLineStyle lineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(intLineWidth), MapInfo.Styles.LineWidthUnit.Pixel), 2, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparency) / 100))), Color.FromArgb(Convert.ToInt32(intLineColour))));
                    // Construct a TextStyle with a SimpleLineStyle for the callout line //
                    Color FontColour = Color.FromArgb(_LabelFontColour);
                    MapInfo.Styles.TextStyle textStyle = new MapInfo.Styles.TextStyle(new MapInfo.Styles.Font(_LabelFontName, _LabelFontSize, Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparency) / 100))), FontColour), FontColour, MapInfo.Styles.FontFaceStyle.Normal, MapInfo.Styles.FontWeight.Normal, MapInfo.Styles.TextEffect.None, MapInfo.Styles.TextDecoration.None, MapInfo.Styles.TextCase.Default, false, false), lineStyle);

                    MapInfo.Mapping.LabelLayer lblLayer = new MapInfo.Mapping.LabelLayer("Labels");
                    MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(t);
                    lblSource.DefaultLabelProperties.Visibility.AllowOverlap = (_LabelOverlap == 1 ? true : false);
                    lblSource.DefaultLabelProperties.Visibility.AllowDuplicates = (_LabelDuplicates == 1 ? true : false);
                    lblSource.DefaultLabelProperties.Visibility.AllowOutOfView = true;
                    lblSource.DefaultLabelProperties.Style = textStyle;
                    lblSource.DefaultLabelProperties.Caption = i_str_label_structure;  // *** NEED TO ADD TOOLTIP *** //
                    lblSource.DefaultLabelProperties.Visibility.Enabled = true;
                    lblSource.DefaultLabelProperties.Visibility.VisibleRangeEnabled = true;
                    lblSource.DefaultLabelProperties.Visibility.VisibleRange = new VisibleRange(_LabelVisibleRangeFrom, _LabelVisibleRangeTo, MapInfo.Geometry.DistanceUnit.Meter);
                    //lblSource.Maximum = 50;
                    lblSource.DefaultLabelProperties.Layout.UseRelativeOrientation = false;
                    //lblSource.DefaultLabelProperties.Layout.RelativeOrientation = MapInfo.Text.RelativeOrientation.FollowPath;
                    lblSource.DefaultLabelProperties.Layout.Angle = _LabelAngle;
                    //lblSource.DefaultLabelProperties.Priority.Major = "Pop_1994";
                    lblSource.DefaultLabelProperties.Layout.Offset = _LabelOffset;
                    switch (_LabelPosition)
                    {
                        case "BottomCentre":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomCenter;
                            break;
                        case "BottomLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomLeft;
                            break;
                        case "BottomRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.BottomRight;
                            break;
                        case "CentreCenter":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterCenter;
                            break;
                        case "CentreLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterLeft;
                            break;
                        case "CentreRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.CenterRight;
                            break;
                        case "TopCentre":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopCenter;
                            break;
                        case "TopLeft":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopLeft;
                            break;
                        case "TopRight":
                            lblSource.DefaultLabelProperties.Layout.Alignment = MapInfo.Text.Alignment.TopRight;
                            break;
                        default:
                            break;
                    }
                    lblLayer.Sources.Append(lblSource);
                    int intLayerPosition = mapControl1.Map.Layers.Add(lblLayer);
                    if (intLayerPosition >= 0)
                    {
                        MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[intLayerPosition];
                        l.Name = "MapObjectLabels";
                    }
                    conn.Close();
                    conn.Dispose();

                    // Set Default Tooltip... //
                    MapInfo.Tools.MapTool.SetInfoTipExpression(mapControl1.Tools.MapToolProperties, mapControl1.Map.Layers["MapObjects"] as FeatureLayer, i_str_tooltip_structure);

                    break;
                }
            }
            return;
        }


        #region Drag Drop Functionality

        // AutoScrollHelper autoScrollHelper declared under instance vars //
        // autoScrollHelper linked to grid in Form Load Event //
        // gridControl5.AllowDrop = true in form Load Event //

        int dropTargetRowHandle = -1;
        int DropTargetRowHandle
        {
            get
            {
                return dropTargetRowHandle;
            }
            set
            {
                dropTargetRowHandle = value;
                gridControl5.Invalidate();
            }
        }
        const string OrderFieldName = "LayerOrder";

        private void gridControl5_DragOver(object sender, DragEventArgs e)
        {
            GridControl grid = (GridControl)sender;
            System.Drawing.Point pt = new System.Drawing.Point(e.X, e.Y);
            pt = grid.PointToClient(pt);
            GridView view = grid.GetViewAt(pt) as GridView;
            if (view == null) return;

            if (view.FocusedRowHandle == 0 || view.FocusedRowHandle == 1 || view.FocusedRowHandle == 2)  // Don't allow Map Objects or Map Objects Labels or Temporary Map Objects [may be -1 if at end of grid] //
            {
                DropTargetRowHandle = -1;
                return;
            }

            GridHitInfo hitInfo = view.CalcHitInfo(pt);
            if (hitInfo.HitTest == GridHitTest.EmptyRow)
            {
                DropTargetRowHandle = view.DataRowCount;
            }
            else
            {
                DropTargetRowHandle = hitInfo.RowHandle;
            }

            e.Effect = DragDropEffects.None;

            GridHitInfo downHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            if (downHitInfo != null)
            {
                //if (hitInfo.RowHandle != downHitInfo.RowHandle)
                if (hitInfo.RowHandle != downHitInfo.RowHandle && (hitInfo.RowHandle < 0 || hitInfo.RowHandle > 2))  // Everything after && Stops row 1, 2 and 3 being moved //
                    e.Effect = DragDropEffects.Move;
            }

            autoScrollHelper.ScrollIfNeeded(hitInfo);
        }

        private void gridControl5_DragDrop(object sender, DragEventArgs e)
        {
            GridControl grid = sender as GridControl;
            GridView view = grid.MainView as GridView;
            GridHitInfo srcHitInfo = e.Data.GetData(typeof(GridHitInfo)) as GridHitInfo;
            GridHitInfo hitInfo = view.CalcHitInfo(grid.PointToClient(new System.Drawing.Point(e.X, e.Y)));
            int sourceRow = srcHitInfo.RowHandle;
            int targetRow = (hitInfo.RowHandle == GridControl.InvalidRowHandle ? -1 : hitInfo.RowHandle);

            if (DropTargetRowHandle < 0) return;  // No Valid Place for Dropping //

            DropTargetRowHandle = -1;
            MoveRow(sourceRow, targetRow, view, true);
        }

        private void gridControl5_DragLeave(object sender, EventArgs e)
        {
            DropTargetRowHandle = -1;
        }

        private void gridControl5_Paint(object sender, PaintEventArgs e)
        {
            if (DropTargetRowHandle < 0) return;
            GridControl grid = (GridControl)sender;
            GridView view = (GridView)grid.MainView;

            bool isBottomLine = DropTargetRowHandle == view.DataRowCount;

            GridViewInfo viewInfo = view.GetViewInfo() as GridViewInfo;
            GridRowInfo rowInfo = viewInfo.GetGridRowInfo(isBottomLine ? DropTargetRowHandle - 1 : DropTargetRowHandle);

            if (rowInfo == null) return;

            System.Drawing.Point p1, p2;
            if (isBottomLine)
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Bottom - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Bottom - 1);
            }
            else
            {
                p1 = new System.Drawing.Point(rowInfo.Bounds.Left, rowInfo.Bounds.Top - 1);
                p2 = new System.Drawing.Point(rowInfo.Bounds.Right, rowInfo.Bounds.Top - 1);
            }
            e.Graphics.DrawLine(Pens.Blue, p1, p2);
        }

        private void gridView5_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            if (e.Button == MouseButtons.Left && downHitInfo != null)
            {
                Size dragSize = SystemInformation.DragSize;
                System.Drawing.Rectangle dragRect = new System.Drawing.Rectangle(new System.Drawing.Point(downHitInfo.HitPoint.X - dragSize.Width / 2, downHitInfo.HitPoint.Y - dragSize.Height / 2), dragSize);

                if (!dragRect.Contains(new System.Drawing.Point(e.X, e.Y)))
                {
                    view.GridControl.DoDragDrop(downHitInfo, DragDropEffects.All);
                    downHitInfo = null;
                }
            }
        }

        private void MoveRow(int sourceRow, int targetRow, GridView view, bool MoveMapLayer)
        {
            view.BeginDataUpdate();
            view.BeginSort();
            if (sourceRow > targetRow && targetRow != -1)
            {
                for (int i = targetRow; i < sourceRow; i++)
                {
                    view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) + 1);
                }
                DataRow dragRow = view.GetDataRow(sourceRow);
                dragRow[OrderFieldName] = targetRow;
                if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow);  // Update Map's Layer Position //
            }
            else
            {
                if (targetRow > sourceRow && targetRow != -1) // Not trying to position on end of grid //
                {
                    for (int i = targetRow - 1; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow - 1;
                    if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow - 1);  // Update Map's Layer Position //
                }
                else // Positioning on end of grid //
                {
                    targetRow = view.DataRowCount - 1;
                    for (int i = targetRow; i > sourceRow; i--)
                    {
                        view.SetRowCellValue(i, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(i, "LayerOrder")) - 1);
                    }
                    DataRow dragRow = view.GetDataRow(sourceRow);
                    dragRow[OrderFieldName] = targetRow;
                    if (MoveMapLayer) Adjust_Layer_Order(sourceRow, targetRow);  // Update Map's Layer Position //
                }
            }
            view.EndSort();
            view.EndDataUpdate();
        }

        #endregion


        private void Adjust_Layer_Order(int intOriginalPosition, int intNewPosition)
        {
            // Note: Layer order should be reversed as MapInfo draws the layers in reverse order (Last layer is on top) //
            //intOriginalPosition = mapControl1.Map.Layers.Count - 1 - intOriginalPosition;
            //intNewPosition = mapControl1.Map.Layers.Count - 1 - intNewPosition;

            mapControl1.Map.Layers.Move(intOriginalPosition, intNewPosition);
        }


        #region GridView5

        private void gridView5_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                /*if (i_intLoadedTemplateID == 0)
                {
                    bbiSaveTemplateAs.Enabled = false;
                }
                else
                {
                    bbiSaveTemplateAs.Enabled = true;
                }

                if (i_intLoadedTemplateCreatedByID != GlobalSettings.UserID && i_intLoadedTemplateID > 0 && GlobalSettings.PersonType != 0)
                {
                    bbiSaveTemplate.Enabled = false;
                }
                else
                {
                    bbiSaveTemplate.Enabled = true;
                }
                */
                int[] intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length > 0)
                {
                    bbiLayerManagerProperties.Enabled = (view.GetFocusedRowCellValue("LayerName").ToString() == "Temporary Map Objects" ? false : true);
                }
                else
                {
                    bbiLayerManagerProperties.Enabled = false;
                }
                popupMenu_LayerManager.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LayerHitable":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LayerType")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();

        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Append:
                    e.Handled = true;
                    AddRecord();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.CancelEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Edit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.EndEdit:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.NextPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.PrevPage:
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Remove:
                    e.Handled = true;
                    DeleteRecord();
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:

                    GridView view = gridView5;
                    if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 3) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "LayerOrder")) + 1);
                        Adjust_Layer_Order(intFocusedRow, intFocusedRow - 1);  // Update Map's Layer Position //
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 2 || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "LayerOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "LayerOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "LayerOrder")) - 1);
                        Adjust_Layer_Order(intFocusedRow, intFocusedRow + 1);  // Update Map's Layer Position //
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView5_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = gridView5;
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (e.FocusedRowHandle == GridControl.InvalidRowHandle || e.FocusedRowHandle <= 3 ? false : true);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (e.FocusedRowHandle == GridControl.InvalidRowHandle || e.FocusedRowHandle <= 2 || e.FocusedRowHandle >= view.DataRowCount - 1 ? false : true);
        }

        private void repositoryItemCE_LayerVisible_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            GridView view = (GridView)gridControl5.MainView;
            Set_LayerVisibility(view.FocusedRowHandle, ce.Checked);
        }

        private void repositoryItemCE_LayerHitable_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            GridView view = (GridView)gridControl5.MainView;
            string strFileName = view.GetFocusedRowCellValue("LayerPath").ToString();
            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
            {
                if (l.Name == strFileName)
                {
                    if (l.Name == "MapObjects")
                    {
                        MapInfo.Mapping.LayerHelper.SetSelectable(l, ce.Checked);
                        MapInfo.Mapping.LayerHelper.SetEditable(l, ce.Checked);
                        MapInfo.Mapping.LayerHelper.SetInsertable(l, ce.Checked);
                    }
                    else if (l.Name == "MapObjectLabels")
                    {
                        mapControl1.Tools.SelectMapToolProperties.LabelsAreEditable = ce.Checked;
                    }
                    else
                    {
                        MapInfo.Mapping.LayerHelper.SetSelectable(l, ce.Checked);
                    }
                }
            }
        }

        #endregion


        private void Set_LayerVisibility(int intGridRow, bool boolVisibility)
        {
            GridView view = (GridView)gridControl5.MainView;
            string strFileName = view.GetRowCellValue(intGridRow, "LayerPath").ToString();
            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
            {
                if (l.Name == strFileName) l.Enabled = boolVisibility;
            }
        }

        public void ChildForm_RefreshThematicItemDataSet(int intHeaderID, int intHeaderType, int intDefaultSymbol, int intDefaultSymbolColour, int intDefaultSymbolSize, int intDefaultPolygonBoundaryColour, int intDefaultPolygonBoundaryWidth, int intDefaultPolygonFillColour, int intDefaultPolygonFillPattern, int intDefaultPolygonFillPatternColour, int intDefaultLineStyle, int intDefaultLineColour, int intDefaultLineWidth)
        {
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01255_AT_Tree_Picker_Thematic_Mapping_Default_Items", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PickListID", intHeaderID));
            cmd.Parameters.Add(new SqlParameter("@PickListType", intHeaderType));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbol", intDefaultSymbol));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolColour", intDefaultSymbolColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultSymbolSize", intDefaultSymbolSize));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryColour", intDefaultPolygonBoundaryColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonBoundaryWidth", intDefaultPolygonBoundaryWidth));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillColour", intDefaultPolygonFillColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPattern", intDefaultPolygonFillPattern));
            cmd.Parameters.Add(new SqlParameter("@DefaultPolygonFillPatternColour", intDefaultPolygonFillPatternColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineSymbol", intDefaultLineStyle));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineColour", intDefaultLineColour));
            cmd.Parameters.Add(new SqlParameter("@DefaultLineWidth", intDefaultLineWidth));
            dsThematicStyleItems.Clear();  // Remove old values first //
            sdaThematicStyleItems = new SqlDataAdapter(cmd);
            sdaThematicStyleItems.Fill(dsThematicStyleItems, "Table");
            intThematicStylingFieldHeaderID = intHeaderID;
            intThematicStylingFieldHeaderType = intHeaderType;
            SQlConn.Close();
            SQlConn.Dispose();

            if (intHeaderType == 0)  // Selected Thematic set is an in-memory set as opposed to a saved set //
            {
                intThematicStylingBasePicklistID = intHeaderID;
            }
            else  // Set is saved so we need to get the controlling field ID from it.
            {
                DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter GetSetting = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                try
                {
                    intThematicStylingBasePicklistID = Convert.ToInt32(GetSetting.sp01289_AT_Tree_Picker_Style_Get_Bast_PicklistID(intHeaderID));
                }
                catch (Exception)
                {
                    intThematicStylingBasePicklistID = 0;
                }
            }
        }

        public void ChildForm_RefreshListOfAvailableStyleSets(int intNewStyleSetID, int intNewStyleSetType)
        {
            int intCurrentUser = this.GlobalSettings.UserID;
            int intCurrentUserType = this.GlobalSettings.PersonType;
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01256_AT_Tree_Picker_Thematic_Mapping_Available_Sets", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@intUserID", intCurrentUser));
            cmd.Parameters.Add(new SqlParameter("@intUserType", intCurrentUserType));
            sdaThematicStyleSets = new SqlDataAdapter(cmd);
            dsThematicStyleSets.Clear();
            sdaThematicStyleSets.Fill(dsThematicStyleSets, "Table");
            intThematicStylingFieldHeaderID = intNewStyleSetID;
            intThematicStylingFieldHeaderType = intNewStyleSetType;

            SQlConn.Close();
            SQlConn.Dispose();
        }

        #endregion


        #region grid2 [Map Objects]

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount > 0)
                {
                    bbiHighlightYes.Enabled = true;
                    bbiHighlightNo.Enabled = true;
                    bbiVisibleYes.Enabled = true;
                    bbiVisibleNo.Enabled = true;
                }
                else
                {
                    bbiHighlightYes.Enabled = false;
                    bbiHighlightNo.Enabled = false;
                    bbiVisibleYes.Enabled = false;
                    bbiVisibleNo.Enabled = false;
                }
                bbiViewOnMap.Enabled = (intCount == 1 ? true : false);
                bbiCopyHighlightFromVisible.Enabled = true;

                bool boolRowsSelected = false;
                for (int i = 0; i < view.RowCount; i++)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                    {
                        boolRowsSelected = true;
                        break;
                    }
                }
                if (boolRowsSelected)
                {
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bbiDatasetCreate.Enabled = false;
                }
                if (view.RowCount > 0)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    bbiDatasetSelection.Enabled = true;
                    bsiDataset.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                    bsiDataset.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetManager.Enabled = true;

                popupMenu1.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemCheckEditHighlight_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            GridView view = (GridView)gridControl2.MainView;
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "CheckMarkSelection", 1);
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "Highlight", 1);
        }

        private void SetButtonStatus()
        {
            /*           if (selection1.SelectedCount == 0)
                       {
                           foreach(EditorButton button in colorEditHighlight.Properties.Buttons) 
                           {
                               if (button.Tag == "Highlight") button.Enabled = false;
                           }
                       }
                       else
                       {
                           foreach(EditorButton button in colorEditHighlight.Properties.Buttons) 
                           {
                               if (button.Tag == "Highlight") button.Enabled = true;
                           }
                       }*/
        }

        private void colorEditHighlight_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "Highlight")
            {
                int intSelectedCount = 0;

                GridView view = (GridView)gridControl2.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
                }
                view = (GridView)gridControl6.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
                }

                if (intSelectedCount == 0)
                {
                    XtraMessageBox.Show("Select one or more map objects to highlight before proceeding.", "Highlight Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Highlight_Clicked(true, intSelectedCount);
            }
            else if (e.Button.Tag.ToString() == "Clear")
            {
                ClearHighlight();
            }
        }

        private void colorEditHighlight_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            if (intMapObjectTransparency > 0)
            {
                Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
                Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Info Theme to handle thematics //-
            }
        }

        private void Highlight_Clicked(bool boolShowProgress, int intSelectedCount)
        {
            GridView view = (GridView)gridControl2.MainView;
            frmProgress fProgress = new frmProgress(10);
            if (boolShowProgress)
            {
                fProgress.UpdateCaption("Highlighting Map Objects...");
                fProgress.Show();
                Application.DoEvents();
            }
            int intUpdateProgressThreshhold = intSelectedCount / 10;
            int intUpdateProgressTempCount = 0;

            string strMapID = "";
            int intProcessedCount = 0;
            int intMatchingCount = 0;
            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;

            double MinX = (double)9999999.00;  // Used for Zooming to Scale //
            double MinY = (double)9999999.00;
            double MaxX = (double)-9999999.00;
            double MaxY = (double)-9999999.00;


            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intProcessedCount++;
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    if (intProcessedCount == intSelectedCount && ceCentreMap.Checked)
                    {
                        intMatchingCount += FindAndHighlight(strMapID, true, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                    else
                    {
                        intMatchingCount += FindAndHighlight(strMapID, false, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }

                    if (boolShowProgress)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                    if (intProcessedCount >= intSelectedCount) break;
                }
            }
            // Now Process Assets Grid //
            intProcessedCount = 0;
            view = (GridView)gridControl6.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intProcessedCount++;
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    if (intProcessedCount == intSelectedCount && ceCentreMap.Checked)
                    {
                        intMatchingCount += FindAndHighlight(strMapID, true, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }
                    else
                    {
                        intMatchingCount += FindAndHighlight(strMapID, false, false, true, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
                    }

                    if (boolShowProgress)
                    {
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }
                    }
                    if (intProcessedCount >= intSelectedCount) break;
                }
            }
            if (csScaleMapForHighlighted.Checked)
            {
                // *** Scale map to show all highlighted fatures *** //
                // First Get a margin to add //
                double dMargin = (double)0.0;
                double dMaxVariance = (double)0.00;
                if ((MaxX - MinX) > (MaxY - MinY))
                {
                    dMaxVariance = MaxX - MinX;
                }
                else
                {
                    dMaxVariance = MaxY - MinY;
                }
                dMargin = (0.025 * dMaxVariance) - 0.47;

                MapInfo.Geometry.DRect rect = new MapInfo.Geometry.DRect(MinX - dMargin, MinY - dMargin, MaxX + dMargin, MaxY + dMargin);  // Create rectangle on coordinated with applied margins //
                MapInfo.Geometry.Envelope envelope = new MapInfo.Geometry.Envelope(mapControl1.Map.GetDisplayCoordSys(), rect);  // Create envelopeusing current map projection and rectangle //
                this.mapControl1.Map.Bounds = envelope.Bounds;  // Scale and centre map on rectangle //
                if (this.mapControl1.Map.Scale < 250)  // If Scale has zoomed in too far, zoom out to a sensible scale //
                {
                    this.mapControl1.Map.Scale = 250;
                }
            }

            mapControl1.ResumeLayout();  // Switch on Map Redraw //

            if (boolShowProgress)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
            }
        }

        public void Scale_Map_To_Visible_Objects(bool boolShowWaitForm)
        {
            GridView view = (GridView)gridControl2.MainView;

            string strMapID = "";

            double MinX = (double)9999999.00;  // Used for Zooming to Scale //
            double MinY = (double)9999999.00;
            double MaxX = (double)-9999999.00;
            double MaxY = (double)-9999999.00;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            if (boolShowWaitForm)
            {
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Scaling Map...");
            }

            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    FindAndHighlight(strMapID, false, false, false, Color.Black, ref MinX, ref MinY, ref MaxX, ref MaxY);
                }
            }
            // Now Process Trees Grid //
            view = (GridView)gridControl6.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                              
                    //*** Get Map Objects Coordinates ***//
                    MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
                    connection.Open();
                    MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("id = '" + strMapID + "'");
                    MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
                    connection.Close();
                    connection.Dispose();

                    foreach (Feature ftr in irfc)  // Should only be one feature in collection //
                    {
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //
                        if (ftr.Geometry.Bounds.x1 < MinX) MinX = ftr.Geometry.Bounds.x1;
                        if (ftr.Geometry.Bounds.x2 > MaxX) MaxX = ftr.Geometry.Bounds.x2;
                        if (ftr.Geometry.Bounds.y1 < MinY) MinY = ftr.Geometry.Bounds.y1;
                        if (ftr.Geometry.Bounds.y2 > MaxY) MaxY = ftr.Geometry.Bounds.y2;
                    }
                }
            }
            // *** Scale map to show all highlighted fatures *** //
            // First Get a margin to add //
            double dMargin = (double)0.0;
            double dMaxVariance = (double)0.00;
            if ((MaxX - MinX) > (MaxY - MinY))
            {
                dMaxVariance = MaxX - MinX;
            }
            else
            {
                dMaxVariance = MaxY - MinY;
            }
            dMargin = (0.025 * dMaxVariance) - 0.47;

            MapInfo.Geometry.DRect rect = new MapInfo.Geometry.DRect(MinX - dMargin, MinY - dMargin, MaxX + dMargin, MaxY + dMargin);  // Create rectangle on coordinated with applied margins //
            MapInfo.Geometry.Envelope envelope = new MapInfo.Geometry.Envelope(mapControl1.Map.GetDisplayCoordSys(), rect);  // Create envelopeusing current map projection and rectangle //
            this.mapControl1.Map.Bounds = envelope.Bounds;  // Scale and centre map on rectangle //
            if (this.mapControl1.Map.Scale < 250)  // If Scale has zoomed in too far, zoom out to a sensible scale //
            {
                this.mapControl1.Map.Scale = 250;
            }

            mapControl1.ResumeLayout();  // Switch on Map Redraw //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }


        private int FindAndHighlight(string MapID, bool CentreOnMap, bool ShowLocusEfect, bool HighlightObject, System.Drawing.Color HighlightColour, ref double MinX, ref double MinY, ref double MaxX, ref double MaxY)
        {
            CompositeStyle style = new CompositeStyle();

            //*** Get Map Object to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("id = '" + MapID + "'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();

            foreach (Feature ftr in irfc)  // Should only be one feature in collection //
            {
                if (HighlightObject)  // Change objects background colour to passed in Highlight colour //
                {
                    if (intMapObjectTransparency <= 0)
                    {
                        if (ftr.Style.ToString().Contains("MapInfo.Styles.AreaStyle"))
                        {
                            style.AreaStyle = (AreaStyle)ftr.Style;
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                            }
                            else
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                            }
                        }
                        else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                        {
                            style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                            style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                        }
                        else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleLineStyle"))
                        {
                            style.LineStyle = (SimpleLineStyle)ftr.Style;
                            ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                        }
                        ftr.Style = style;
                    }
                    ftr["intHighlighted"] = 1;
                    ftr["Thematics"] = Convert.ToDouble(999995);
                    ftr.Update();
                }
                ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //
                if (ftr.Geometry.Bounds.x1 < MinX) MinX = ftr.Geometry.Bounds.x1;
                if (ftr.Geometry.Bounds.x2 > MaxX) MaxX = ftr.Geometry.Bounds.x2;
                if (ftr.Geometry.Bounds.y1 < MinY) MinY = ftr.Geometry.Bounds.y1;
                if (ftr.Geometry.Bounds.y2 > MaxY) MaxY = ftr.Geometry.Bounds.y2;

                if (CentreOnMap)
                {
                    Double dblCurrentScale = (this.mapControl1.Map.Scale > 5000 ? 5000 : this.mapControl1.Map.Scale);
                    this.mapControl1.Map.SetView(ftr);
                    this.mapControl1.Map.Scale = dblCurrentScale;
                }

                if (ShowLocusEfect)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint;
                    MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
                    converter.ToDisplay(ftr.Geometry.Centroid, out screenPoint);

                    System.Drawing.Point MapControlPoint = new System.Drawing.Point();
                    MapControlPoint = this.mapControl1.PointToScreen(MapControlPoint);

                    screenPoint.X += MapControlPoint.X;
                    screenPoint.Y += MapControlPoint.Y;

                    //locusEffectsProvider1.ShowLocusEffect(this, ptCentreMapObject, LocusEffectsProvider.DefaultLocusEffectArrow);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);
                }
            }
            return 1;
        }

        private int ClearHighlight()
        {
            int intSelectedCount = 0;

            GridView view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
            }
            view = (GridView)gridControl6.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1) intSelectedCount++;
            }
            if (intSelectedCount == 0) return 0;

            frmProgress fProgress = new frmProgress(10);
            fProgress.UpdateCaption("Clearing Highlighted Map Objects...");
            fProgress.Show();
            Application.DoEvents();
            int intUpdateProgressThreshhold = intSelectedCount / 10;


            // Prepare Thematic styling first //
            int intPicklistValue = 0;
            decimal decPicklistValue = (decimal)0.00;
            string strThematicsGridViewColumn = "";
            System.Drawing.Color colorObjectColour = Color.Red;

            if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
            {
                strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
            }
            // End of Prepare Thematics //

            string strMapID = "";
            int intUpdateProgressTempCount = 0;
            view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    intPicklistValue = Convert.ToInt32(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    decPicklistValue = Convert.ToDecimal(view.GetRowCellValue(i, strThematicsGridViewColumn));

                    Reset_Map_Object_Back_Colour(strMapID, (intUseThematicStyling == 1 ? true : false), intPicklistValue, decPicklistValue);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            // Now Process Assets Grid //
            view = (GridView)gridControl6.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    strMapID = Convert.ToString(view.GetRowCellValue(i, "MapID"));
                    //intPicklistValue = Convert.ToInt32(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    //decPicklistValue = Convert.ToDecimal(view.GetRowCellValue(i, strThematicsGridViewColumn));
                    //Reset_Map_Object_Back_Colour(strMapID, (intUseThematicStyling == 1 ? true : false), intPicklistValue, decPicklistValue);
                    Reset_Map_Object_Back_Colour(strMapID, false, intPicklistValue, decPicklistValue);

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }

            if (fProgress != null)
            {
                fProgress.Close();
                fProgress.Dispose();
            }
            return 1;
        }

        private void JustHighlight(ref CompositeStyle style, System.Drawing.Color HighlightColour)
        {
            if (intMapObjectTransparency <= 0)
            {
                if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)
                {
                    ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                }
                else
                {
                    ((SimpleInterior)style.AreaStyle.Interior).BackColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                }
                style.SymbolStyle.Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
                ((SimpleLineStyle)style.LineStyle).Color = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), HighlightColour);
            }
        }

        private void Reset_Map_Object_Back_Colour(string MapID, bool UseThematics, int intThematicPickListValue, decimal decThematicPickListValue)
        {
            CompositeStyle style = new CompositeStyle();

            //*** Get Map Object to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("id = '" + MapID + "'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();
            foreach (Feature ftr in irfc)  // Should only be one feature in collection //
            {
                if (intMapObjectTransparency <= 0)
                {
                    if (ftr.Style.ToString().Contains("MapInfo.Styles.AreaStyle"))
                    {
                        style.AreaStyle = (AreaStyle)ftr.Style;
                        if (UseThematics)
                        {
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)  // Solid Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "PolygonForeColor");
                            }
                            else // Pattern Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "Polygon");
                            }
                        }
                        else
                        {
                            if (((SimpleInterior)style.AreaStyle.Interior).Pattern == 2)  // Solid Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Get_Default_BackColour("PolygonForeColor");
                            }
                            else  // Pattern Fill //
                            {
                                ((SimpleInterior)style.AreaStyle.Interior).BackColor = Get_Default_BackColour("Polygon");
                            }
                        }
                    }
                    else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                    {
                        string strPointType = (Convert.ToInt32(ftr["ModuleID"]) == 1 ? "Point2" : "Point");  // Work out if we need to Style a pole or Tree //
                        style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                        if (UseThematics)
                        {
                            style.SymbolStyle.Color = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, strPointType);
                        }
                        else
                        {
                            style.SymbolStyle.Color = Get_Default_BackColour(strPointType);
                        }
                    }
                    else if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleLineStyle"))
                    {
                        style.LineStyle = (SimpleLineStyle)ftr.Style;
                        if (UseThematics)
                        {
                            ((SimpleLineStyle)style.LineStyle).Color = Get_Thematic_BackColour(intThematicPickListValue, decThematicPickListValue, "Polyline");
                        }
                        else
                        {
                            ((SimpleLineStyle)style.LineStyle).Color = Get_Default_BackColour("Polyline");
                        }
                    }
                    ftr.Style = style;
                }
                ftr["intHighlighted"] = 0;
                ftr["Thematics"] = Convert.ToDouble(decThematicPickListValue);
                ftr.Update();
            }
        }

        private void Update_Map_Object_Sizes(bool ShowProgress)
        {
            // Loops through all the visible point map objects and calculates their sizes //
            // Only called when the scale of the map is changed [Map_ViewChanged Event] and Dynamic Sizing according to Crown Diameter is switched on //

            CompositeStyle style = new CompositeStyle();

            //*** Get Map Objects to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("intObjectType = 0");  // Only Points [intObjectType = 0] //
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();

            foreach (Feature ftr in irfc)
            {
                if (ftr.Style.ToString().Contains("MapInfo.Styles.SimpleVectorPointStyle"))
                {
                    style.SymbolStyle = (SimpleVectorPointStyle)ftr.Style;
                    style.SymbolStyle.PointSize = Calculate_Point_Size(ftr);

                    ftr.Style = style;
                    ftr.Update();
                }
            }
        }

        private int Calculate_Point_Size(Feature ftr)
        {
            int intCalculatedSize = 0;
            intCalculatedSize = Convert.ToInt32(ftr["CrownDiameter"]);
            if (intCalculatedSize <= 0)
            {
                intCalculatedSize = 2;
                // Do something to highlight the object as missing a value //
            }
            else if (intCalculatedSize > 240)
            {
                intCalculatedSize = 240;
                // Do something to highlight the object as a bigger value //
            }
            intCalculatedSize = Convert.ToInt32(Math.Round(Convert.ToDouble(intCalculatedSize) / (0.000209 * mapControl1.Map.Scale), 0));
            if (intCalculatedSize < 2)
            {
                intCalculatedSize = 2;
            }
            else if (intCalculatedSize > 240)
            {
                intCalculatedSize = 240;
            }
            return intCalculatedSize;
        }

        private System.Drawing.Color Get_Default_BackColour(string strObjectType)
        {
            DataRow drStyle = dsDefaultStyles.Tables[0].Rows[0];
            switch (strObjectType)
            {
                case "Polygon":
                    int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillPatternColour"])));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                    }
                case "PolygonForeColor":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                case "Polyline":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolylineColour"])));
                case "Point":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["SymbolColour"])));
                case "Point2":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["Symbol2Colour"])));
                default:
                    return Color.Red;
            }
        }

        private System.Drawing.Color Get_Thematic_BackColour(int intPicklistValue, decimal decFieldValue, string strObjectType)
        {
            if (intThematicStylingBasePicklistID == 0) return Get_Default_BackColour(strObjectType);
            DataRow[] drFiltered;
            if (intThematicStylingBasePicklistID >= 90000)  // 90000+ = Thematic Set using Continues Variables [BandStart and BandEnd] //
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("StartBand <= " + decFieldValue.ToString() + " and EndBand >= " + decFieldValue.ToString());
            }
            else
            {
                drFiltered = dsThematicStyleItems.Tables[0].Select("PicklistItemID = " + intPicklistValue.ToString());
            }

            DataRow drStyle;
            if (drFiltered.Length != 0)
            {
                drStyle = drFiltered[0];
            }
            else
            {
                drStyle = dsDefaultStyles.Tables[0].Rows[0];
            }
            switch (strObjectType)
            {
                case "Polygon":
                    int intDefaultPolygonFillPattern = Convert.ToInt32(drStyle["PolygonFillPattern"]);
                    if (intDefaultPolygonFillPattern == 2) // Solid //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillPatternColour"])));
                    }
                    else  // Patterned so swap colours round as MapInfo does it the wrong way round by default //
                    {
                        return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                    }
                case "PolygonForeColor":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolygonFillColour"])));
                case "Polyline":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["PolylineColour"])));
                case "Point":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["SymbolColour"])));
                case "Point2":
                    return Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intMapObjectTransparency) / 100))), Color.FromArgb(Convert.ToInt32(drStyle["Symbol2Colour"])));
                default:
                    return Color.Red;
            }
        }

        private void btnRefreshMapObjects_Click(object sender, EventArgs e)
        {
            UpdateMapObjectsDisplayed(true);
        }

        private void btnClearMapObjects_Click(object sender, EventArgs e)
        {
            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //

            // Clear Layer of all Map Objects //
            try
            {
                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                MICommand MapCmd = conn.CreateCommand();
                MapCmd.CommandText = "Delete From MapObjects";
                MapCmd.Prepare();
                int nchanged = MapCmd.ExecuteNonQuery();
                MapCmd.Dispose();
                t.Pack(MapInfo.Data.PackType.All);  // Pack the table //
                conn.Close();
                conn.Dispose();
            }
            catch (Exception)
            {
            }
            mapControl1.ResumeLayout();
        }

        public void UpdateMapObjectsDisplayed(bool boolShowProgress)
        {
            // Prepare Thematic styling first //
            Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
            int intPicklistValue = 0;
            string strThematicsGridViewColumn = "";
            CompositeStyle style = new CompositeStyle();
            CompositeStyle stylePole = new CompositeStyle();
            if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
            {
                strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
            }
            else // No Thematics so get default style //
            {
                //style = Get_Default_Style();
                strThematicsGridViewColumn = "VoltageID";
            }
            style = Get_Default_Style();
            stylePole = Get_Default_Style_Pole();
            // End of Prepare Thematics //

            frmProgress fProgress = null;
            if (boolShowProgress)
            {
                fProgress = new frmProgress(10);
                fProgress.UpdateCaption("Loading Map Objects...");
                fProgress.Show();
                Application.DoEvents();
            }
            int intUpdateProgressThreshhold = (selection1.SelectedCount + selectionTrees.SelectedCount) / 10;
            int intUpdateProgressTempCount = 0;
            int intProcessedCount = 0;

            string strID = "";
            string strPolygonXY = "";
            int intCrownDiameter = 0;
            string[] strArray;
            char[] delimiters = new char[] { ';' };
            int iPoint = 0;
            int nchanged = 0;

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

            mapControl1.SuspendLayout();  // Switch off Map Redraw until done //

            // Clear Layer of all Map Objects first //
            MICommand MapCmd = conn.CreateCommand();
            MapCmd.CommandText = "Delete From MapObjects";
            MapCmd.Prepare();
            nchanged = MapCmd.ExecuteNonQuery();
            MapCmd.Dispose();
            t.Pack(MapInfo.Data.PackType.All);  // Pack the table //
            // End of Clear Layer //

            t.BeginAccess(MapInfo.Data.TableAccessMode.Write);

            string strCurrentValue = "";

            // ----- Load Any Trees Selected for Loading ----- //
            GridView viewMapObject = (GridView)gridControl6.MainView;
            if (selectionTrees.SelectedCount > 0)
            {
                for (int i = 0; i < viewMapObject.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        intProcessedCount++;

                        //if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        //{
                        //    intPicklistValue = Convert.ToInt32(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn));
                        //    style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        //}
                        // ***** STYLE SET ON LINE BEFORE FOR LOOP ***** //
                        if (viewMapObject.GetRowCellValue(i, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            //intCrownDiameter = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "X")), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "Y")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            f["MapID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "TreeID"));
                            f["ObjectReference"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReferenceNumber"));
                            f["RegionName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "RegionName"));
                            f["SubAreaName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SubAreaName"));
                            f["VoltageType"] = "";
                            f["CrownDiameter"] = 0;
                            f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                            f["LastInspectionDate"] = Convert.ToDateTime(viewMapObject.GetRowCellValue(i, "LastInspectionDate"));
                            f["ModuleID"] = 2;  // 1 = Poles, 2 = Trees //
                            f["SurveyStatus"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SurveyStatus"));
                            f["SurveyDone"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "SurveyDone"));
                            f["Thematics"] = Get_Thematic_Value(f);
                            f["ThematicValue"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "ThematicValue"));

                            // Following populated so object labelling will work //
                            f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReferenceNumber"));
                            f["Tooltip"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Tooltip"));

                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }

                            if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR TREES //
                            {
                                style = Get_Thematic_Style_Tree(2, Convert.ToInt32(f["ThematicValue"]));
                            }
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            strPolygonXY = Convert.ToString(viewMapObject.GetRowCellValue(i, "XYPairs"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polyline");
                                }

                                f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                                f["MapID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "TreeID"));
                                f["ObjectReference"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReferenceNumber"));
                                f["RegionName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "RegionName"));
                                f["SubAreaName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SubAreaName"));
                                f["VoltageType"] = "";
                                f["CrownDiameter"] = 0;
                                f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                                f["LastInspectionDate"] = Convert.ToDateTime(viewMapObject.GetRowCellValue(i, "LastInspectionDate"));
                                f["ModuleID"] = 2;  // 1 = Amenity Trees, 2 = Assets //
                                f["CrownDiameter"] = 0;
                                f["SurveyStatus"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SurveyStatus"));
                                f["SurveyDone"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "SurveyDone"));
                                f["Thematics"] = Get_Thematic_Value(f);
                                f["ThematicValue"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "ThematicValue"));

                                // Following populated so object labelling will work //
                                f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReferenceNumber"));
                                f["Tooltip"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Tooltip"));

                                if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR TREES //
                                {
                                    style = Get_Thematic_Style_Tree(2, Convert.ToInt32(f["ThematicValue"]));
                                }
                                f.Style = style;
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                        if (boolShowProgress)
                        {
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (intProcessedCount >= selection1.SelectedCount + selectionTrees.SelectedCount) break;
                    }
                }
            }
            // ----- End of Load Any Trees Selected for Loading ----- //

            // Load All Pole Map Objects ticked for inclusion //
            viewMapObject = (GridView)gridControl2.MainView;
            intProcessedCount = 0;
            if (selection1.SelectedCount > 0)
            {
                for (int i = 0; i < viewMapObject.DataRowCount; i++)
                {
                    if (Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        intProcessedCount++;

                        //if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        //{
                        //    intPicklistValue = Convert.ToInt32(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn));
                        //    if (intMapObjectTransparency <= 0) stylePole = Get_Thematic_Style_Pole(intPicklistValue, Convert.ToDecimal(viewMapObject.GetRowCellValue(i, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        //}

                        if (viewMapObject.GetRowCellValue(i, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "X")), Convert.ToDouble(viewMapObject.GetRowCellValue(i, "Y")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            f["MapID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "PoleID"));
                            f["ObjectReference"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                            f["RegionName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "RegionName"));
                            f["SubAreaName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SubAreaName"));
                            f["VoltageType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "VoltageType"));
                            f["CrownDiameter"] = 0;
                            f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                            f["LastInspectionDate"] = Convert.ToDateTime(viewMapObject.GetRowCellValue(i, "LastInspectionDate"));
                            f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                            f["SurveyStatus"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SurveyStatus"));
                            f["SurveyDone"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "SurveyDone"));
                            f["Thematics"] = Get_Thematic_Value(f);
                            f["ThematicValue"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "ThematicValue"));
                            switch (intTreeRefStructureType)
                            {
                                case 0:
                                    f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                                    break;
                                case 1:
                                    strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                                    f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                    break;
                                case 2:
                                    strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));  // Note that the second commented out method also works //
                                    f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                    break;
                                default:
                                    break;
                            }
                            f["Tooltip"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Tooltip"));
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                stylePole.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }

                            if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR POLES //
                            {
                                stylePole.SymbolStyle.Color = Get_Pole_Colour(Convert.ToInt32(f["ThematicValue"]));
                            }
                            f.Style = stylePole;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                            strPolygonXY = Convert.ToString(viewMapObject.GetRowCellValue(i, "XYPairs"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    viewMapObject.SetRowCellValue(i, "ObjectType", "Polyline");
                                }
                                f.Style = stylePole;
                                f["id"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "MapID"));
                                f["MapID"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "PoleID"));
                                f["ObjectReference"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                                f["RegionName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "RegionName"));
                                f["SubAreaName"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SubAreaName"));
                                f["VoltageType"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "VoltageType"));
                                f["CrownDiameter"] = 0;
                                f["intHighlighted"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "Highlight"));
                                f["LastInspectionDate"] = Convert.ToDateTime(viewMapObject.GetRowCellValue(i, "LastInspectionDate"));
                                f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                                f["SurveyStatus"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "SurveyStatus"));
                                f["SurveyDone"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "SurveyDone"));
                                f["Thematics"] = Get_Thematic_Value(f);
                                f["ThematicValue"] = Convert.ToInt32(viewMapObject.GetRowCellValue(i, "ThematicValue"));

                                switch (intTreeRefStructureType)
                                {
                                    case 0:
                                        f["ShortTreeRef"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                                        break;
                                    case 1:
                                        strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));
                                        f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(viewMapObject.GetRowCellValue(i, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                        break;
                                    case 2:
                                        strCurrentValue = Convert.ToString(viewMapObject.GetRowCellValue(i, "PoleNumber"));  // Note that the second commented out method also works //
                                        f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                        break;
                                    default:
                                        break;
                                }
                                f["Tooltip"] = Convert.ToString(viewMapObject.GetRowCellValue(i, "Tooltip"));
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                        if (boolShowProgress)
                        {
                            intUpdateProgressTempCount++;
                            if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                            {
                                intUpdateProgressTempCount = 0;
                                fProgress.UpdateProgress(10);
                            }
                        }
                        if (intProcessedCount >= selection1.SelectedCount) break;
                    }
                }
            }

            t.EndAccess();
            conn.Close();
            conn.Dispose();

            if (intMapObjectTransparency > 0) Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Info Theme to handle thematics //

            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //

            if (boolShowProgress)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress.Dispose();
                }
            }
        }

        private void ceCentreMap_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) csScaleMapForHighlighted.Checked = false;
        }

        private void csScaleMapForHighlighted_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked) ceCentreMap.Checked = false;
        }

        private void btnSelectHighlighted_Click(object sender, EventArgs e)
        {
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();

            string strMapID = "";
            int intSelectedCount = 0;
            GridView view = (GridView)gridControl2.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intSelectedCount++;
                    strMapID += "id='" + Convert.ToString(view.GetRowCellValue(i, "MapID")) + "' or ";
                }
            }
            // Now Process Assets Grid //
            view = (GridView)gridControl6.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                {
                    intSelectedCount++;
                    strMapID += "id='" + Convert.ToString(view.GetRowCellValue(i, "MapID")) + "' or ";
                }
            }

            if (intSelectedCount == 0)
            {
                XtraMessageBox.Show("Select one or more map objects to highlight before proceeding.", "Select Highlighted Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strMapID = strMapID.Remove(strMapID.Length - 4);  // Remove last " or " statement //

            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strMapID);
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            selection.Add(irfc);  // Add the whole feature set to the default selection //
            connection.Close();
            connection.Dispose();
        }

        private void Highlight_Map_Objects_From_Grid_Selection()
        {
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();

            string strMapID = "";
            GridView view = (GridView)gridControl2.MainView;
            // Now Process Assets Grid //
            GridView view2 = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int[] intRowHandles2 = view2.GetSelectedRows();
            if (intRowHandles.Length <= 0 && intRowHandles2.Length <= 0) return;

            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //
            foreach (int intRowHandle in intRowHandles)
            {
                strMapID += "id='" + Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
            }
            foreach (int intRowHandle in intRowHandles2)
            {
                strMapID += "id='" + Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID")) + "' or ";
            }

            strMapID = strMapID.Remove(strMapID.Length - 4);  // Remove last " or " statement //

            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strMapID);
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            selection.Add(irfc);  // Add the whole feature set to the default selection //
            connection.Close();
            connection.Dispose();

            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //
        }

        #endregion


        #region Grid [Tree Map Objects]

        private void gridViewAssetObjects_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 12;
            SetMenuStatus();
        }

        private void gridViewAssetObjects_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                if (intCount > 0)
                {
                    bbiHighlightYes.Enabled = true;
                    bbiHighlightNo.Enabled = true;
                    bbiVisibleYes.Enabled = true;
                    bbiVisibleNo.Enabled = true;
                }
                else
                {
                    bbiHighlightYes.Enabled = false;
                    bbiHighlightNo.Enabled = false;
                    bbiVisibleYes.Enabled = false;
                    bbiVisibleNo.Enabled = false;
                }
                bbiViewOnMap.Enabled = (intCount == 1 ? true : false);
                bbiCopyHighlightFromVisible.Enabled = true;

                bool boolRowsSelected = false;
                for (int i = 0; i < view.RowCount; i++)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "Highlight")) == 1)
                    {
                        boolRowsSelected = true;
                        break;
                    }
                }
                if (boolRowsSelected)
                {
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bbiDatasetCreate.Enabled = false;
                }
                //if (view.RowCount > 0)  // Use RowCount and not DataRowCount as it's visible rows we are concerned with //
                //{
                //    bbiDatasetSelection.Enabled = true;
                //    bsiDataset.Enabled = true;
                //}
                //else
                //{
                bbiDatasetSelection.Enabled = false;
                bsiDataset.Enabled = false;
                //}
                //bsiDataset.Enabled = true;
                //bbiDatasetManager.Enabled = true;

                popupMenu1.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemCheckEditHighlight2_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            GridView view = (GridView)gridControl2.MainView;
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "CheckMarkSelection", 1);
            if (ce.Checked) view.SetRowCellValue(view.FocusedRowHandle, "Highlight", 1);
        }

        #endregion


        private void bbiViewOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Pole Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // Tree Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Map Object to be viewed by clicking on the row in the grid then try again.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetFocusedRowCellValue("CheckMarkSelection")) != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Ensure the Map Object to be viewed is loaded into the map first then try again.\n\nTip: To load the Map Object into the map, tick it's Visibile column in the grid then click the Refresh Map Objects button.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strMapID = view.GetFocusedRowCellValue("MapID").ToString();
            if (string.IsNullOrEmpty(strMapID))
            {
                XtraMessageBox.Show("The Map Object to be viewed does not have a Map ID - unable to perform search.\n\nYou should edit this record and assign it a Map ID before trying again.", "View Map Object", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            System.Drawing.Color HighlightColour = (System.Drawing.Color)colorEditHighlight.EditValue;

            double MinX = (double)9999999.00;  // Used for Zooming to Scale //
            double MinY = (double)9999999.00;
            double MaxX = (double)-9999999.00;
            double MaxY = (double)-9999999.00;

            FindAndHighlight(strMapID, true, true, false, HighlightColour, ref MinX, ref MinY, ref MaxX, ref MaxY);
        }

        private void bbiHighlightYes_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "Highlight", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiHighlightNo_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "Highlight", 0);
                }
                view.EndUpdate();
            }
        }

        private void bbiCopyHighlightFromVisible_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            view.BeginUpdate();
            for (int i = 0; i < view.DataRowCount; i++)
            {
                view.SetRowCellValue(i, "Highlight", Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")));
            }
            view.EndUpdate();
        }

        private void bbiVisibleYes_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Pole Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // Tree Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiVisibleNo_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 2:  // Amenity Tree Objects //
                    view = (GridView)gridControl2.MainView;
                    break;
                case 12:  // EstatePlan Objects //
                    view = (GridView)gridControl6.MainView;
                    break;
                default:
                    return;
            }
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndUpdate();
            }
        }


        #region PopupContainer2 Scale

        private void popupContainerEdit2_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            if (i_bool_FullExtentFired) // Don't fire since Full Extents button clicked //
            {
                i_bool_FullExtentFired = false;
            }
            else
            {
                e.Value = PopupContainerEdit2_Get_Selected();
            }
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            string strCurrentScale = "";
            if (Convert.ToInt32(seUserDefinedScale.Value) > 0)
            {
                strCurrentScale = seUserDefinedScale.Value.ToString();
            }
            else
            {
                GridView view = (GridView)gridControl4.MainView;
                if (view.DataRowCount <= 0)
                {
                    XtraMessageBox.Show("Select or enter a scale before proceeding.", "Select Scale", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return strCurrentScale;
                }
                else
                {
                    strCurrentScale = view.GetFocusedRowCellValue("Scale").ToString();
                }
            }
            // Zoom map to scale //
            if (Convert.ToDouble(strCurrentScale) > 0.00) mapControl1.Map.SetView(mapControl1.Map.Center, mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(strCurrentScale));
            return "Scale 1:" + strCurrentScale;
        }

        private void btnSetScale_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            if ((button.Parent as PopupContainerControl).OwnerEdit != null) (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnViewFullExtent_Click(object sender, EventArgs e)
        {
            //FeatureLayer lyr = mapControl1.Map.Layers["MapObjects"] as FeatureLayer;
            //if (lyr != null) mapControl1.Map.SetView(lyr);

            MapInfo.Mapping.IMapLayerFilter iml = MapInfo.Mapping.MapLayerFilterFactory.FilterByType(typeof(MapInfo.Mapping.FeatureLayer));
            MapInfo.Mapping.MapLayerEnumerator mle = this.mapControl1.Map.Layers.GetMapLayerEnumerator(iml);
            try
            {
                this.mapControl1.Map.SetView(mle);
            }
            catch (Exception)
            {
            }

            i_bool_FullExtentFired = true;
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                {
                    seUserDefinedScale.Value = 0;  // Clear User Defined Value since grid selection used //
                }
            }
        }

        #endregion


        #region GPS Panel

        string strGpsPort = "";
        int intGpsBaudRate = 0;
        GPS.NMEA.NMEA2OSG OSGconv = new GPS.NMEA.NMEA2OSG();  // OSGridConverter //
        MapInfo.Data.Key GpsPointKey = null;
        MapInfo.Data.Key GpsPolygonPlotterKey = null;

        private void LoadGPSComPortList()
        {
            cmbBaudRate.SelectedIndex = 4;  // List Hardcoded, item 4 (0 based) = 4800 //
            cmbPortName.Properties.Items.Clear();
            cmbPortName.Properties.Items.Add("Auto Detect");  // Add dummy first item //
            foreach (string s in System.IO.Ports.SerialPort.GetPortNames())
            {
                cmbPortName.Properties.Items.Add(s);
            }
            if (cmbPortName.Properties.Items.Count > 0)
            {
                cmbPortName.SelectedIndex = 0;
            }
            else
            {
                XtraMessageBox.Show("There are no COM Ports detected on this computer - GPS Functionality Diabled.\nPlease install a COM Port and reopen this screen if you need to use GPS.", "WoodPlan Mapping - GPS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                btnGPS_Start.Enabled = false;
            }

        }

        private int GetGpsPortSettings(string strPort, int intBaudRate)
        {
            // *** Note this may fail if another device is also connected to a another comp port in addition to the GPS device being connected to a com port! *** //
            WaitDialogForm loading = new WaitDialogForm("Searching for GPS Device...", "WoodPlan Mapping");
            loading.Show();
            string[] strPorts = System.IO.Ports.SerialPort.GetPortNames();
            if (strPort == "Auto Detect")
            {
                foreach (string port in strPorts)
                {
                    try
                    {
                        System.IO.Ports.SerialPort com = new System.IO.Ports.SerialPort(port, intBaudRate);
                        com.Open();
                        System.Threading.Thread.Sleep(500);  // Wait half a second //
                        int nBytes = com.BytesToRead;
                        if (nBytes == 0)  // Try again //
                        {
                            System.Threading.Thread.Sleep(500);  // Wait half a second //
                            nBytes = com.BytesToRead;
                            if (nBytes == 0)  // Try again //
                            {
                                System.Threading.Thread.Sleep(500);  // Wait half a second //
                                nBytes = com.BytesToRead;
                            }
                        }
                        byte[] BufBytes;
                        BufBytes = new byte[nBytes];
                        com.Read(BufBytes, 0, nBytes);
                        string strData = System.Text.Encoding.GetEncoding("ASCII").GetString(BufBytes, 0, nBytes);
                        com.Close();
                        if (strData != "")
                        {
                            strGpsPort = port;
                            intGpsBaudRate = intBaudRate;
                            loading.Close();
                            return 1;
                        }
                    }
                    catch (System.IO.IOException)
                    {
                        //Some devices (like iPAQ H4100 among others) throws an IOException for no reason
                        //Let's just ignore it and run along
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                try
                {
                    System.IO.Ports.SerialPort com = new System.IO.Ports.SerialPort(strPort, intBaudRate);
                    com.Open();
                    System.Threading.Thread.Sleep(500);  // Wait half a second //
                    int nBytes = com.BytesToRead;
                    if (nBytes == 0)  // Try again //
                    {
                        System.Threading.Thread.Sleep(500);  // Wait half a second //
                        nBytes = com.BytesToRead;
                        if (nBytes == 0)  // Try again //
                        {
                            System.Threading.Thread.Sleep(500);  // Wait half a second //
                            nBytes = com.BytesToRead;
                        }
                    }
                    byte[] BufBytes;
                    BufBytes = new byte[nBytes];
                    com.Read(BufBytes, 0, nBytes);
                    string strData = System.Text.Encoding.GetEncoding("ASCII").GetString(BufBytes, 0, nBytes);
                    com.Close();
                    if (strData != "")
                    {
                        strGpsPort = strPort;
                        //intGpsBaudRate = i;
                        intGpsBaudRate = intBaudRate;
                        loading.Close();
                        return 1;
                    }
                    else
                    {
                        loading.Close();
                        return 0;
                    }
                }
                catch (System.IO.IOException)
                {
                    //Some devices (like iPAQ H4100 among others) throws an IOException for no reason
                    //Let's just ignore it and run along
                }
                catch (System.Exception)
                {
                }
            }
            loading.Close();
            return 0;
        }

        public void btnGPS_Start_Click(object sender, EventArgs e)  // Public so this can be triggered from outwith the form if required //
        {
            Start_Stop_GPS();
        }

        public void Start_Stop_GPS()
        {
            ResetGpsPlottingControls();
            if (btnGPS_Start.Text == "Start GPS")
            {
                statusBar1.Text = "";  // Reset the Status text for GPS //
                if (!GPS.IsPortOpen)
                {
                    string strTempPort = cmbPortName.EditValue.ToString();
                    int intTempBaud = Convert.ToInt32(cmbBaudRate.EditValue);
                    int intReturn = 1;
                    if (strGpsPort == "" || intGpsBaudRate <= 0 || (strGpsPort != "" && strGpsPort != strTempPort) || (intGpsBaudRate > 0 && intGpsBaudRate != intTempBaud))
                    {
                        intReturn = GetGpsPortSettings(strTempPort, intTempBaud);
                    }
                    if (strGpsPort == "" || intGpsBaudRate == 0 || intReturn == 0)
                    {
                        XtraMessageBox.Show("Unable to start GPS... The Port or Baud Rate specified may be incorrect.\nTry again by clicking the Start GPS button. If the problem persists, try adjusting the port and/or baud rate.", "Start GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        try
                        {
                            GPS.Start(strGpsPort, intGpsBaudRate); // Open serial port //
                            btnGPS_Start.Text = "Stop GPS";
                            btnGPS.Text = "Hide GPS Position";  // Survey Panel button //
                            groupControl2.Enabled = true;

                            gridControl5.Enabled = false;
                            beWorkspace.Properties.Buttons[0].Enabled = false;  // Disable Select Workspace button //
                            beWorkspace.Properties.Buttons[1].Enabled = false;  // Disable Save Workspace button //
                            beWorkspace.Properties.Buttons[2].Enabled = false;  // Disable Save As Workspace button //
                            gridControl2.Enabled = false;
                            btnRefreshMapObjects.Enabled = false;
                            gridControl3.Enabled = false;

                            foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                            {
                                if (l.Name == "MapObjects")
                                {
                                    l.VolatilityHint = LayerVolatilityHint.Animate;
                                }
                            }

                        }
                        catch (System.Exception ex)
                        {
                            XtraMessageBox.Show("An error occured when trying to open port: " + ex.Message, "Start GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                }
            }
            else
            {
                GPS.Stop(); // Close serial port //
                btnGPS_Start.Text = "Start GPS";
                btnGPS.Text = "Show GPS Position";  // Survey Panel button //
                groupControl2.Enabled = false;
                foreach (MapInfo.Mapping.IMapLayer l in mapControl1.Map.Layers)
                {
                    if (l.Name == "MapObjects")
                    {
                        l.VolatilityHint = LayerVolatilityHint.Normal;
                    }
                }

                // Clear GPS CrossHair Object if it exists //
                if (GpsPointKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        try
                        {
                            MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                            t.DeleteFeature(GpsPointKey);  // delete feature //
                            t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                            GpsPointKey = null;
                        }
                        catch (Exception)
                        {
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                // Clear GPS Polygon Plotter Object if it exists //
                if (GpsPolygonPlotterKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        try
                        {
                            MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                            if (ftr != null)
                            {
                                t.DeleteFeature(GpsPolygonPlotterKey);  // delete feature //
                                t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                            }
                            GpsPolygonPlotterKey = null;
                        }
                        catch (Exception)
                        {
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                gridControl5.Enabled = true;
                beWorkspace.Properties.Buttons[0].Enabled = true;  // Enable Select Workspace button //
                beWorkspace.Properties.Buttons[1].Enabled = true;  // Enable Save Workspace button //
                beWorkspace.Properties.Buttons[2].Enabled = true;  // Enable Save As Workspace button //
                gridControl2.Enabled = true;
                btnRefreshMapObjects.Enabled = true;
                gridControl3.Enabled = true;
            }
        }

        private void ceGPS_PlotPoint_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotPoint");
        }

        private void ceGPS_PlotPolygon_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotPolygon");
        }

        private void ceGPS_PlotLine_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            if (ce.Checked) UpdateGPS_Main_Controls("PlotLine");
        }

        private void ceGps_PlotWithTimer_CheckedChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.CheckEdit ce = (DevExpress.XtraEditors.CheckEdit)sender;
            seGps_PlotTimerInterval.Enabled = ce.Checked;
        }

        private void btnGps_PauseTimer_Click(object sender, EventArgs e)
        {
            if (btnGps_PauseTimer.Text == "Pause Timer")
            {
                // Stop the Timer //
                GPSTimer.Stop();
                btnGps_PauseTimer.Text = "Restart Timer";
                labelPlottingUsingTimer.Text = "Plotting Timer <color=255,0,0>Paused</color>";
            }
            else
            {
                // Restart the timer //
                GPSTimer.Start();
                btnGps_PauseTimer.Text = "Pause Timer";
                labelPlottingUsingTimer.Text = "Plotting using Timer";
            }
        }

        private void seGps_PlotTimerInterval_EditValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.SpinEdit se = (DevExpress.XtraEditors.SpinEdit)sender;
            GPSTimer.Interval = Convert.ToInt32(se.EditValue) * 1000;
        }

        private void GPSTimer_Tick(object sender, EventArgs e)
        {
            if (Update_GPS_Plotter_Polygon("InsertVertice") != 1) DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS on Timer Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS using Timer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void UpdateGPS_Main_Controls(string strStatus)
        {
            if (strStatus == "PlotPoint")
            {
                btnGPS_PlotStart.Text = "Start";
                btnGPS_PlotStart.Enabled = false;
                btnGPS_Plot.Text = "Plot";
                btnGPS_Plot.Enabled = true;
                btnGPS_PlotStop.Enabled = false;
                btnGPS_PlotStop.Text = "Stop";
                ceGps_PlotWithTimer.Enabled = false;
                seGps_PlotTimerInterval.Enabled = false;
            }
            else if (strStatus == "PlotPolygon")
            {
                btnGPS_PlotStart.Text = "Start Polygon";
                btnGPS_PlotStart.Enabled = true;
                btnGPS_Plot.Text = "Insert Vertice";
                btnGPS_Plot.Enabled = false;
                btnGPS_PlotStop.Text = "End Polygon";
                btnGPS_PlotStop.Enabled = false;
                ceGps_PlotWithTimer.Enabled = true;
                seGps_PlotTimerInterval.Enabled = true;
            }
            else if (strStatus == "PlotLine")
            {
                btnGPS_PlotStart.Text = "Start Line";
                btnGPS_PlotStart.Enabled = true;
                btnGPS_Plot.Text = "Insert Vertice";
                btnGPS_Plot.Enabled = false;
                btnGPS_PlotStop.Text = "End Line";
                btnGPS_PlotStop.Enabled = false;
                ceGps_PlotWithTimer.Enabled = true;
                seGps_PlotTimerInterval.Enabled = true;
            }
            btnGps_PauseTimer.Text = "Pause Timer";
            btnGps_PauseTimer.Enabled = false;
            GPSTimer.Stop();
            labelPlottingUsingTimer.Visible = false;
            labelPlottingUsingTimer.Text = "Plotting using Timer";
        }

        private void PlotGpsPositionOnMap(Coordinate coordinate, double ellipHeight)
        {
            double doubX = 0.00;
            double doubY = 0.00;
            if (OSGconv.Transform(coordinate.Latitude, coordinate.Longitude, ellipHeight))
            {
                //display decimal values of lat and lon
                doubX = Convert.ToDouble(OSGconv.east);
                doubY = Convert.ToDouble(OSGconv.north);
                lbRMCGridRef.Text = OSGconv.ngr;  // Show Grid Reference //
            }
            else
            {
                return;  // Unable to get OS Grid From Passed Lat/Long so Abort Map Update //
            }

            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(i_int_GPS_CrossHair_Colour);
            _vectorSymbol.PointSize = (short)30;
            CompositeStyle cs = new CompositeStyle();
            cs.SymbolStyle = _vectorSymbol;

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");

            MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), doubX, doubY);
            bool boolUpdated = false;
            if (GpsPointKey != null)
            {
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                if (si != null)  // Map Object Found //
                {
                    MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                    ftr.Geometry = g;
                    ftr.Update();
                    boolUpdated = true;
                }
            }
            if (!boolUpdated)  // GPS Object not already created or unable to find it so re-create //
            {
                MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
                ftr["ID"] = "*GPS Position*";
                ftr.Geometry = g;
                ftr.Style = cs;
                GpsPointKey = t.InsertFeature(ftr);


                // *** Prevent Labelling of the GPS Point using a SelectionLabelModifier *** //
                // Remove any previous SelectionLabelModifier //
                try
                {
                    ArrayList lblmods;
                    // LOOP OVER EACH LABEL LAYER //
                    foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
                    {
                        // LOOP OVER EACH LABELS SOURCE //
                        foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                        {
                            lblmods = new ArrayList();
                            //------------------------------------------------------------------------
                            // CHECK THE MODIFIERS TO SEE IF ANY ARE A SelectionLabelModifier.
                            // IF SO ADD IT TO AN ARRAYLIST.
                            // CANNOT DELETE IT RIGHT A WAY -- ERRORS OUT
                            //------------------------------------------------------------------------
                            foreach (MapInfo.Mapping.LabelModifier lsm in lblsrc.Modifiers)
                            {
                                if (lsm is MapInfo.Mapping.SelectionLabelModifier)
                                {
                                    lblmods.Add(lsm);
                                }
                            }
                            //-----------------------------------------------------------
                            // DELETE ANY SelectionLabelModifier FOUND FOR THIS SOURCE
                            //-----------------------------------------------------------
                            foreach (MapInfo.Mapping.LabelModifier lsm in lblmods)
                            {
                                lblsrc.Modifiers.Remove(lsm);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Error Clearing Custom Labels.\n" + ex.Message, "Error", MessageBoxButtons.OK);
                }

                // Add a SelectionLabelModifier for the GPS Point Feature to customise it's Label //
                foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
                {
                    // LOOP OVER EACH LABELS SOURCE //
                    foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                    {
                        MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(t, "LabelSource");
                        MapInfo.Mapping.LabelProperties lp = new MapInfo.Mapping.LabelProperties();

                        LabelProperties properties = new LabelProperties();
                        properties.Attributes = LabelAttribute.VisibilityEnabled | LabelAttribute.Caption | LabelAttribute.CalloutLineUse;
                        properties.Visibility.Enabled = false;
                        properties.Caption = "''";
                        properties.CalloutLine.Use = false;

                        // Add the properties to a new selection label modifier and add the modifier to the label source //
                        SelectionLabelModifier modifier = new SelectionLabelModifier();
                        modifier.Properties.Add(GpsPointKey, properties);
                        lblsrc.Modifiers.Append(modifier);

                    }
                }
                // *** End of Prevent Labelling of the GPS Point using a SelectionLabelModifier *** //

            }

            if (GpsPolygonPlotterKey != null) Update_GPS_Plotter_Polygon("UpdateLastVerticePosition");  // Update the GPS Ploygon Plotter //

            int intBuffer = (50 * Convert.ToInt32(mapControl1.Map.Scale)) / 2500;
            if (g.Bounds.x1 - intBuffer <= mapControl1.Map.Bounds.x1 || g.Bounds.x2 + intBuffer >= mapControl1.Map.Bounds.x2 || g.Bounds.y1 - intBuffer <= mapControl1.Map.Bounds.y1 || g.Bounds.y2 + intBuffer >= mapControl1.Map.Bounds.y2)
            {
                this.mapControl1.Map.SetView(g);
            }
            conn.Close();
            conn.Dispose();
        }

        private void btnGPS_PlotStart_Click(object sender, EventArgs e)
        {
            if (btnGPS_PlotStart.Text == "Start Polygon" || btnGPS_PlotStart.Text == "Start Line")
            {
                int intSuccess = 0;
                if (GpsPointKey != null)
                {
                    intSuccess = Update_GPS_Plotter_Polygon("Start");
                    btnGPS_PlotStart.Text = "Cancel";
                    btnGPS_Plot.Enabled = true;  // Insert Vertice Button //
                    btnGPS_PlotStop.Enabled = true;
                    ceGps_PlotWithTimer.Enabled = false;
                    seGps_PlotTimerInterval.Enabled = false;
                    if (ceGps_PlotWithTimer.Checked)
                    {
                        btnGps_PauseTimer.Enabled = true;
                        GPSTimer.Start();
                        labelPlottingUsingTimer.Visible = true;
                    }
                }
                if (intSuccess != 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                // abort GPS generated Polygon or Line //
                ResetGpsPlottingControls();
            }
        }

        private void btnGPS_PlotStop_Click(object sender, EventArgs e)
        {
            int intSuccess = 0;
            if (btnGPS_PlotStop.Text == "End Line")
            {
                intSuccess = Update_GPS_Plotter_Polygon("CreateLineFeature");
            }
            else if (btnGPS_PlotStop.Text == "End Polygon")
            {
                intSuccess = Update_GPS_Plotter_Polygon("CreatePolygonFeature");
            }
            if (intSuccess != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        private ArrayList _GpsPolygonPlotterPoints;  // Internal list of coordinates to keep track of the points submitted by the user //
        private int Update_GPS_Plotter_Polygon(string strAction)
        {
            int intSuccess = 0;
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            switch (strAction)
            {
                case "Start":
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        _GpsPolygonPlotterPoints = new ArrayList();
                        _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));
                        _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));
                        //_linePoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x + 500, ftr.Geometry.Centroid.y + 500));
                        //_linePoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x + 750, ftr.Geometry.Centroid.y + 750));

                        MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array // 

                        //Create the feature and add it to the layer
                        MapInfo.Data.Feature ftrLine = new MapInfo.Data.Feature(t.TableInfo.Columns);
                        ftrLine["ID"] = "*GPS Plotter*";
                        ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);

                        CompositeStyle style = new CompositeStyle();
                        style.LineStyle = new MapInfo.Styles.SimpleLineStyle(new MapInfo.Styles.LineWidth(Convert.ToDouble(i_str_GPS_Polygon_Plot_Line_Width), MapInfo.Styles.LineWidthUnit.Pixel), i_int_GPS_Polygon_Plot_Line_Style, Color.FromArgb(i_int_GPS_Polygon_Line_Plot_Colour));
                        ftrLine.Style = style;
                        GpsPolygonPlotterKey = t.InsertFeature(ftrLine);
                        intSuccess = 1;
                    }
                    break;
                case "InsertVertice":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //
                            _GpsPolygonPlotterPoints.Add(new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y));  // Add new vertice //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            intSuccess = 1;
                        }
                    }
                    break;
                case "UpdateLastVerticePosition":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y);  // Adjust end of last vertice to match GPS CrossHair position //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            intSuccess = 1;
                        }
                    }
                    break;
                case "CreateLineFeature":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //
                            ftrLine.Geometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            AddObjectToMap(ftrLine.Geometry);  // Create new Line Map Object //
                            ResetGpsPlottingControls();  // Clear the TempMapPlotterPolygon and reset the controlling buttons //
                            intSuccess = 1;
                        }
                    }
                    break;
                case "CreatePolygonFeature":
                    si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Crosshairs feature //
                        ftr.Geometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature geometry to new feature geometry so the CoordSys for the feature geometry can be set to correct projection //

                        si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                        if (si != null)  // Map Object Found //
                        {
                            MapInfo.Data.Feature ftrLine = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);  // Got the GPS Plotter line - don't need to bother sorting out geometry coordsys as it will be recontructed from _GpsPolygonPlotterPoints ArrayList //

                            _GpsPolygonPlotterPoints[_GpsPolygonPlotterPoints.Count - 1] = new MapInfo.Geometry.DPoint(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y); // Adjust end of last vertice to match GPS CrossHair position //
                            _GpsPolygonPlotterPoints.Add(_GpsPolygonPlotterPoints[0]);  // Add new FINAL vertice with same coordinates as the first one to close polygon //

                            MapInfo.Geometry.DPoint[] dpPoints = (MapInfo.Geometry.DPoint[])_GpsPolygonPlotterPoints.ToArray(typeof(MapInfo.Geometry.DPoint));  // Convert ArrayList into Array //

                            ftrLine.Geometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dpPoints);
                            ftrLine.Update();
                            AddObjectToMap(ftrLine.Geometry);  // Create new Polygon Map Object //
                            ResetGpsPlottingControls();  // Clear the TempMapPlotterPolygon and reset the controlling buttons //
                            intSuccess = 1;
                        }
                    }
                    break;

                default:
                    break;
            }
            conn.Close();
            conn.Dispose();
            return intSuccess;
        }

        private void ResetGpsPlottingControls()
        {
            // *** Send Message to Abort *** //
            if (ceGPS_PlotPoint.Checked)
            {
                UpdateGPS_Main_Controls("PlotPoint");
            }
            else if (ceGPS_PlotPolygon.Checked)
            {
                UpdateGPS_Main_Controls("PlotPolygon");
            }
            else
            {
                UpdateGPS_Main_Controls("PlotLine");
            }

            // Clear GPS Polygon Plotter Object if it exists //
            if (GpsPolygonPlotterKey != null)
            {
                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPolygonPlotterKey.ToString() + "'");
                if (si != null)  // Map Object Found //
                {
                    try
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                        if (ftr != null)
                        {
                            t.DeleteFeature(GpsPolygonPlotterKey);  // delete feature //
                            t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                        }
                        GpsPolygonPlotterKey = null;
                    }
                    catch (Exception)
                    {
                    }
                }
                conn.Close();
                conn.Dispose();
            }

        }

        private void btnGPS_Plot_Click(object sender, EventArgs e)
        {
            int intSuccess = 0;
            if (ceGPS_PlotPoint.Checked)  // Plot Point according to GPS Signal //
            {
                if (GpsPointKey != null)
                {
                    MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                    conn.Open();
                    MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MI_Key = '" + GpsPointKey.ToString() + "'");
                    if (si != null)  // Map Object Found //
                    {
                        MapInfo.Data.Feature ftr = MapInfo.Engine.Session.Current.Catalog.SearchForFeature(t, si);
                        MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)ftr.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                        intSuccess = AddObjectToMap(newGeometry);  // Add object to map //
                    }
                    conn.Close();
                    conn.Dispose();
                }
            }
            else
            {
                intSuccess = Update_GPS_Plotter_Polygon("InsertVertice");
            }
            if (intSuccess != 1) DevExpress.XtraEditors.XtraMessageBox.Show("Plot Map Object from GPS Failed... Ensure the GPS is running [the GPS Crosshairs should be visible on the map] then try again.", "Plot Map Object from GPS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void tbRawLog_Properties_BeforeShowMenu(object sender, BeforeShowMenuEventArgs e)
        {
            bool boolSaveCreated = false;
            bool boolClearCreated = false;

            foreach (DevExpress.Utils.Menu.DXMenuItem item in e.Menu.Items)
            {
                if (item.Caption == "Save GPS Log...")
                {
                    boolSaveCreated = true;
                    item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                }
                else if (item.Caption == "Clear GPS Log")
                {
                    boolClearCreated = true;
                    item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);

                }
            }
            if (!boolSaveCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiGPSSaveLog", new EventHandler(bbiGPSSaveLog_ItemClick));
                item.Caption = "Save GPS Log...";
                item.Image = imageListGPSLogMenu.Images[0];
                item.BeginGroup = true;
                item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                item.BeginGroup = true;
                e.Menu.Items.Add(item);
            }
            if (!boolClearCreated)
            {
                DevExpress.Utils.Menu.DXMenuItem item = new DevExpress.Utils.Menu.DXMenuItem("bbiGPSClearLog", new EventHandler(bbiGPSClearLog_ItemClick));
                item.Caption = "Clear GPS Log";
                item.Image = imageListGPSLogMenu.Images[1];
                item.Enabled = (string.IsNullOrEmpty(tbRawLog.Text) ? false : true);
                e.Menu.Items.Add(item);
            }
        }

        private void bbiGPSSaveLog_ItemClick(object sender, EventArgs e)
        {

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Text Files(*.TXT)|*.TXT";
            string strFileName = "";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    strFileName = new System.IO.FileInfo(dlg.FileName).FullName;
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine(tbRawLog.Text);
                    sw.Close();
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Save of GPS Log Failed... [" + ex.Message + "]!\n\nTry again - if the problem persists contact Technical Support.", "Save GPS Log", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
        }

        private void bbiGPSClearLog_ItemClick(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to clear the GPS Log?", "Clear GPS Log", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                tbRawLog.Text = "";
            }
        }

        private void ceLogRawGPSData_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            boolLogGPS = ce.Checked;
        }

        private void NMEAtabs_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if (NMEAtabs.SelectedTabPage.Text == "GPGSV") DrawGSV();
        }

        /// <summary>
        /// Responds to sentence events from GPS receiver
        /// </summary>
        private void GPSEventHandler(object sender, GPSHandler.GPSEventArgs e)
        {
            if (boolLogGPS)
            {
                tbRawLog.Text += e.Sentence + "\r\n";
                if (tbRawLog.Text.Length > 20 * 1024 * 1024) //20Kb maximum - prevents crash
                {
                    tbRawLog.Text = tbRawLog.Text.Substring(10 * 1024 * 1024);
                }
                tbRawLog.ScrollToCaret(); //Scroll to bottom
            }
            switch (e.TypeOfEvent)
            {
                case GPSEventType.GPRMC:  //Recommended minimum specific GPS/Transit data
                    if (GPS.HasGPSFix) //Is a GPS fix available?
                    {
                        //lbRMCPosition.Text = GPS.GPRMC.Position.ToString("#.000000");
                        lbRMCPosition.Text = GPS.GPRMC.Position.ToString("DMS");
                        //double[] utmpos = TransformToUTM(GPS.GPRMC.Position);
                        //lbRMCPositionUTM.Text = utmpos[0].ToString("#.0N ") + utmpos[0].ToString("#.0E") + " (Zone: " + utmpos[2] + ")";
                        lbRMCCourse.Text = GPS.GPRMC.Course.ToString();
                        lbRMCSpeed.Text = GPS.GPRMC.Speed.ToString() + " mph";
                        lbRMCTimeOfFix.Text = GPS.GPRMC.TimeOfFix.ToString("F");
                        lbRMCMagneticVariation.Text = GPS.GPRMC.MagneticVariation.ToString();
                    }
                    else
                    {
                        statusBar1.Text = "No Fix";
                        lbRMCCourse.Text = "N/A";
                        lbRMCSpeed.Text = "N/A";
                        lbRMCTimeOfFix.Text = GPS.GPRMC.TimeOfFix.ToString();
                    }
                    break;
                case GPSEventType.GPGGA: //Global Positioning System Fix Data
                    if (GPS.GPGGA.Position != null)
                        lbGGAPosition.Text = GPS.GPGGA.Position.ToString("DM");
                    else
                        lbGGAPosition.Text = "";
                    lbGGATimeOfFix.Text = GPS.GPGGA.TimeOfFix.Hour.ToString() + ":" + GPS.GPGGA.TimeOfFix.Minute.ToString() + ":" + GPS.GPGGA.TimeOfFix.Second.ToString();
                    lbGGAFixQuality.Text = GPS.GPGGA.FixQuality.ToString();
                    lbGGANoOfSats.Text = GPS.GPGGA.NoOfSats.ToString();
                    lbGGAAltitude.Text = GPS.GPGGA.Altitude.ToString() + " " + GPS.GPGGA.AltitudeUnits;
                    lbGGAHDOP.Text = GPS.GPGGA.Dilution.ToString();
                    lbGGAGeoidHeight.Text = GPS.GPGGA.HeightOfGeoid.ToString();
                    lbGGADGPSupdate.Text = GPS.GPGGA.DGPSUpdate.ToString();
                    lbGGADGPSID.Text = GPS.GPGGA.DGPSStationID;
                    if (GPS.GPRMC.Position != null)
                    {
                        if (GPS.GPRMC.Position.Latitude == 0.00 && GPS.GPRMC.Position.Longitude == 0.00)
                        {
                            // Do Nothing //
                        }
                        else
                        {
                            PlotGpsPositionOnMap(GPS.GPRMC.Position, GPS.GPGGA.Altitude);
                        }
                    }
                    break;
                case GPSEventType.GPGLL: //Geographic position, Latitude and Longitude
                    lbGLLPosition.Text = GPS.GPGLL.Position.ToString();
                    lbGLLTimeOfSolution.Text = (GPS.GPGLL.TimeOfSolution.HasValue ? GPS.GPGLL.TimeOfSolution.Value.Hours.ToString() + ":" + GPS.GPGLL.TimeOfSolution.Value.Minutes.ToString() + ":" + GPS.GPGLL.TimeOfSolution.Value.Seconds.ToString() : "");
                    lbGLLDataValid.Text = GPS.GPGLL.DataValid.ToString();
                    break;
                case GPSEventType.GPGSA: //GPS DOP and active satellites
                    if (GPS.GPGSA.Mode == 'A')
                        lbGSAMode.Text = "Auto";
                    else if (GPS.GPGSA.Mode == 'M')
                        lbGSAMode.Text = "Manual";
                    else lbGSAMode.Text = "";
                    lbGSAFixMode.Text = GPS.GPGSA.FixMode.ToString();
                    lbGSAPRNs.Text = "";
                    if (GPS.GPGSA.PRNInSolution.Count > 0)
                        foreach (string prn in GPS.GPGSA.PRNInSolution)
                            lbGSAPRNs.Text += prn + " ";
                    else
                        lbGSAPRNs.Text += "none";
                    lbGSAPDOP.Text = GPS.GPGSA.PDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.PDOP) + ")";
                    lbGSAHDOP.Text = GPS.GPGSA.HDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.HDOP) + ")";
                    lbGSAVDOP.Text = GPS.GPGSA.VDOP.ToString() + " (" + DOPtoWord(GPS.GPGSA.VDOP) + ")";
                    break;
                case GPSEventType.GPGSV: //Satellites in view
                    if (NMEAtabs.SelectedTabPage.Text == "GPGSV") //Only update this tab when it is active
                        DrawGSV();
                    break;
                case GPSEventType.PGRME: //Garmin proprietary sentences.
                    //    lbRMEHorError.Text = GPS.PGRME.EstHorisontalError.ToString();
                    //    lbRMEVerError.Text = GPS.PGRME.EstVerticalError.ToString();
                    //    lbRMESphericalError.Text = GPS.PGRME.EstSphericalError.ToString();
                    break;
                case GPSEventType.TimeOut: //Serialport timeout.
                    statusBar1.Text = "Serial Port Timeout";
                    /*notification1.Caption = "GPS Serialport timeout";
                    notification1.InitialDuration = 5;
                    notification1.Text = "Check your settings and connection";
                    notification1.Critical = false;
                    notification1.Visible = true;
                     */
                    break;
            }
        }
        /*   private double[] TransformToUTM(GPS.Coordinate p)
           {
               //For fun, let's use the SharpMap transformation library and display the position in UTM
               int zone = (int)Math.Floor((p.Longitude + 183) / 6.0);
               SharpMap.CoordinateSystems.ProjectedCoordinateSystem proj = SharpMap.CoordinateSystems.ProjectedCoordinateSystem.WGS84_UTM(zone, (p.Latitude >= 0));
               SharpMap.CoordinateSystems.Transformations.ICoordinateTransformation trans =
                   new SharpMap.CoordinateSystems.Transformations.CoordinateTransformationFactory().CreateFromCoordinateSystems(proj.GeographicCoordinateSystem, proj);
               double[] result = trans.MathTransform.Transform(new double[] { p.Longitude, p.Latitude });
               return new double[] { result[0], result[1], zone };
           } */
        private string DOPtoWord(double dop)
        {
            if (dop < 1.5) return "Ideal";
            else if (dop < 3) return "Excellent";
            else if (dop < 6) return "Good";
            else if (dop < 8) return "Moderate";
            else if (dop < 20) return "Fair";
            else return "Poor";
        }
        private void DrawGSV()
        {
            System.Drawing.Color[] Colors = { Color.Blue , Color.Red , Color.Green, Color.Yellow, Color.Cyan, Color.Orange,
											  Color.Gold , Color.Violet, Color.YellowGreen, Color.Brown, Color.GreenYellow,
											  Color.Blue , Color.Red , Color.Green, Color.Yellow, Color.Aqua, Color.Orange};
            //Generate signal level readout
            int SatCount = GPS.GPGSV.SatsInView;
            Bitmap imgSignals = new Bitmap(picGSVSignals.Width, picGSVSignals.Height);
            Graphics g = Graphics.FromImage(imgSignals);
            g.Clear(Color.White);
            Pen penBlack = new Pen(Color.Black, 1);
            Pen penBlackDashed = new Pen(Color.Black, 1);
            penBlackDashed.DashPattern = new float[] { 2f, 2f };
            Pen penGray = new Pen(Color.LightGray, 1);
            int iMargin = 4; //Distance to edge of image
            int iPadding = 4; //Distance between signal bars
            g.DrawRectangle(penBlack, 0, 0, imgSignals.Width - 1, imgSignals.Height - 1);

            StringFormat sFormat = new StringFormat();
            int barWidth = 1;
            if (SatCount > 0)
                barWidth = (imgSignals.Width - 2 * iMargin - iPadding * (SatCount - 1)) / SatCount;

            //Draw horisontal lines
            for (int i = imgSignals.Height - 15; i > iMargin; i -= (imgSignals.Height - 15 - iMargin) / 5)
                g.DrawLine(penGray, 1, i, imgSignals.Width - 2, i);
            sFormat.Alignment = StringAlignment.Center;
            //Draw satellites
            //GPS.GPGSV.Satellites.Sort(); //new Comparison<GPS.NMEA.GPGSV.Satellite>(sat1, sat2) { int.Parse(sat1)
            for (int i = 0; i < GPS.GPGSV.Satellites.Count; i++)
            {
                GPS.NMEA.GPGSV.Satellite sat = GPS.GPGSV.Satellites[i];
                int startx = i * (barWidth + iPadding) + iMargin;
                int starty = imgSignals.Height - 15;
                int height = (imgSignals.Height - 15 - iMargin) / 50 * sat.SNR;
                if (GPS.GPGSA.PRNInSolution.Contains(sat.PRN))
                {
                    g.FillRectangle(new System.Drawing.SolidBrush(Colors[i]), startx, starty - height + 1, barWidth, height);
                    g.DrawRectangle(penBlack, startx, starty - height, barWidth, height);
                }
                else
                {
                    g.FillRectangle(new System.Drawing.SolidBrush(Color.FromArgb(50, Colors[i])), startx, starty - height + 1, barWidth, height);
                    g.DrawRectangle(penBlackDashed, startx, starty - height, barWidth, height);
                }

                sFormat.LineAlignment = StringAlignment.Near;
                g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 9, FontStyle.Regular), new System.Drawing.SolidBrush(Color.Black), startx + barWidth / 2, imgSignals.Height - 15, sFormat);
                sFormat.LineAlignment = StringAlignment.Far;
                g.DrawString(sat.SNR.ToString(), new System.Drawing.Font("Verdana", 9, FontStyle.Regular), new System.Drawing.SolidBrush(Color.Black), startx + barWidth / 2, starty - height, sFormat);
            }
            picGSVSignals.Image = imgSignals;

            //Generate sky view
            Bitmap imgSkyview = new Bitmap(picGSVSkyview.Width, picGSVSkyview.Height);
            g = Graphics.FromImage(imgSkyview);
            g.Clear(Color.Transparent);
            g.FillEllipse(Brushes.White, 0, 0, imgSkyview.Width - 1, imgSkyview.Height - 1);
            g.DrawEllipse(penGray, 0, 0, imgSkyview.Width - 1, imgSkyview.Height - 1);
            g.DrawEllipse(penGray, imgSkyview.Width / 4, imgSkyview.Height / 4, imgSkyview.Width / 2, imgSkyview.Height / 2);
            g.DrawLine(penGray, imgSkyview.Width / 2, 0, imgSkyview.Width / 2, imgSkyview.Height);
            g.DrawLine(penGray, 0, imgSkyview.Height / 2, imgSkyview.Width, imgSkyview.Height / 2);
            sFormat.LineAlignment = StringAlignment.Near;
            sFormat.Alignment = StringAlignment.Near;
            float radius = 6f;
            for (int i = 0; i < GPS.GPGSV.Satellites.Count; i++)
            {
                GPS.NMEA.GPGSV.Satellite sat = GPS.GPGSV.Satellites[i];
                double ang = 90.0 - sat.Azimuth;
                ang = ang / 180.0 * Math.PI;
                int x = imgSkyview.Width / 2 + (int)Math.Round((Math.Cos(ang) * ((90.0 - sat.Elevation) / 90.0) * (imgSkyview.Width / 2.0 - iMargin)));
                int y = imgSkyview.Height / 2 - (int)Math.Round((Math.Sin(ang) * ((90.0 - sat.Elevation) / 90.0) * (imgSkyview.Height / 2.0 - iMargin)));
                g.FillEllipse(new System.Drawing.SolidBrush(Colors[i]), x - radius * 0.5f, y - radius * 0.5f, radius, radius);

                if (GPS.GPGSA.PRNInSolution.Contains(sat.PRN))
                {
                    g.DrawEllipse(penBlack, x - radius * 0.5f, y - radius * 0.5f, radius, radius);
                    g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 9, FontStyle.Bold), new System.Drawing.SolidBrush(Color.Black), x, y, sFormat);
                }
                else
                    g.DrawString(sat.PRN, new System.Drawing.Font("Verdana", 8, FontStyle.Italic), new System.Drawing.SolidBrush(Color.Gray), x, y, sFormat);
            }
            picGSVSkyview.Image = imgSkyview;
        }

        #endregion


        #region Legend

        private void bbiLegend_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bbiLegend.Checked)
            {
                if (dockPanelLegend.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                {
                    dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                    if (dockPanelLegend.Dock == DockingStyle.Float)
                    {
                        // Important note: Forms StartPosition needs to be set to Manual in the Form's properties //
                        dockPanelLegend.FloatLocation = new System.Drawing.Point((PointToScreen(mapControl1.Location).X + mapControl1.Width + panelContainer1.Width) - dockPanelLegend.Width, PointToScreen(mapControl1.Location).Y + 42);
                    }
                }
            }
            else
            {
                if (dockPanelLegend.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanelLegend.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelLegend_ClosingPanel(object sender, DockPanelCancelEventArgs e)
        {
            if (bbiLegend.Checked) bbiLegend.Checked = false;
        }

        private void dockPanelLegend_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }
        }


        private void colorEditPoleLegend1_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend1 = ce.Color;
        }
        private void colorEditPoleLegend2_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend2 = ce.Color;
        }
        private void colorEditPoleLegend3_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend3 = ce.Color;
        }
        private void colorEditPoleLegend4_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend4 = ce.Color;
        }
        private void colorEditPoleLegend5_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend5 = ce.Color;
        }
        private void colorEditPoleLegend6_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend6 = ce.Color;
        }
        private void colorEditPoleLegend7_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourPoleLegend7 = ce.Color;
        }

        private void colorEditTreeLegend1_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourTreeLegend1 = ce.Color;
        }
        private void colorEditTreeLegend2_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourTreeLegend2 = ce.Color;
        }
        private void colorEditTreeLegend3_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourTreeLegend3 = ce.Color;
        }
        private void colorEditTreeLegend4_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourTreeLegend4 = ce.Color;
        }
        private void colorEditTreeLegend5_EditValueChanged(object sender, EventArgs e)
        {
            ColorEdit ce = (ColorEdit)sender;
            colourTreeLegend5 = ce.Color;
        }
       
        private void ComboBoxEditPoleStylingType_SelectedValueChanged(object sender, EventArgs e)
        {
            switch (ComboBoxEditPoleStylingType.EditValue.ToString())
            {
                case "Survey Status":
                    {
                        _PoleThematicTypeID = 0;
                        
                        colorEditPoleLegend1.Enabled = true;
                        colorEditPoleLegend1.Color = Color.Gray;
                        labelControlPoleLegend1.Text = "Not on Survey";

                        colorEditPoleLegend2.Enabled = true;
                        colorEditPoleLegend2.Color = Color.FromArgb(255, 128, 0);
                        labelControlPoleLegend2.Text = "To Be Started";

                        colorEditPoleLegend3.Enabled = true;
                        colorEditPoleLegend3.Color = Color.Yellow;
                        labelControlPoleLegend3.Text = "Started";

                        colorEditPoleLegend4.Enabled = true;
                        colorEditPoleLegend4.Color = Color.DodgerBlue;
                        labelControlPoleLegend4.Text = "Survey Completed";

                        colorEditPoleLegend5.Enabled = true;
                        colorEditPoleLegend5.Color = Color.Red;
                        labelControlPoleLegend5.Text = "On Hold";

                        colorEditPoleLegend6.Enabled = true;
                        colorEditPoleLegend6.Color = Color.LimeGreen;
                        labelControlPoleLegend6.Text = "Permissioned";

                        colorEditPoleLegend7.Enabled = true;
                        colorEditPoleLegend7.Color = Color.DarkOrchid;
                        labelControlPoleLegend7.Text = "Work Completed";
                    }
                    break;
                case "Shutdown":
                    {
                        _PoleThematicTypeID = 1;

                        colorEditPoleLegend1.Enabled = true;
                        colorEditPoleLegend1.Color = Color.Gray;
                        labelControlPoleLegend1.Text = "Not Surveyed Yet";

                        colorEditPoleLegend2.Enabled = true;
                        colorEditPoleLegend2.Color = Color.LimeGreen;
                        labelControlPoleLegend2.Text = "No Shutdown";

                        colorEditPoleLegend3.Enabled = true;
                        colorEditPoleLegend3.Color = Color.Red;
                        labelControlPoleLegend3.Text = "Yes Shutdown";

                        colorEditPoleLegend4.Enabled = false;
                        colorEditPoleLegend4.Color = Color.Transparent;
                        labelControlPoleLegend4.Text = "N\\A";

                        colorEditPoleLegend5.Enabled = false;
                        colorEditPoleLegend5.Color = Color.Transparent;
                        labelControlPoleLegend5.Text = "N\\A";

                        colorEditPoleLegend6.Enabled = false;
                        colorEditPoleLegend6.Color = Color.Transparent;
                        labelControlPoleLegend6.Text = "N\\A";

                        colorEditPoleLegend7.Enabled = false;
                        colorEditPoleLegend7.Color = Color.Transparent;
                        labelControlPoleLegend7.Text = "N\\A";
                    }
                    break;
                case "Traffic Management":
                    {
                        _PoleThematicTypeID = 2;

                        colorEditPoleLegend1.Enabled = true;
                        colorEditPoleLegend1.Color = Color.Gray;
                        labelControlPoleLegend1.Text = "Not Surveyed Yet";

                        colorEditPoleLegend2.Enabled = true;
                        colorEditPoleLegend2.Color = Color.LimeGreen;
                        labelControlPoleLegend2.Text = "No Traffic Man.";

                        colorEditPoleLegend3.Enabled = true;
                        colorEditPoleLegend3.Color = Color.Red;
                        labelControlPoleLegend3.Text = "Yes Traffic Man.";

                        colorEditPoleLegend4.Enabled = false;
                        colorEditPoleLegend4.Color = Color.Transparent;
                        labelControlPoleLegend4.Text = "N\\A";

                        colorEditPoleLegend5.Enabled = false;
                        colorEditPoleLegend5.Color = Color.Transparent;
                        labelControlPoleLegend5.Text = "N\\A";

                        colorEditPoleLegend6.Enabled = false;
                        colorEditPoleLegend6.Color = Color.Transparent;
                        labelControlPoleLegend6.Text = "N\\A";

                        colorEditPoleLegend7.Enabled = false;
                        colorEditPoleLegend7.Color = Color.Transparent;
                        labelControlPoleLegend7.Text = "N\\A";
                    }
                    break;

                default:
                    break;
            }
            // Need to reload the Pole Objects grid so then thematic column is updated //
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
            }
            if (string.IsNullOrEmpty(strCircuitIDs)) strCircuitIDs = "";
            Load_Map_Objects_Grid();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }
        private void ComboBoxEditTreeStylingType_SelectedValueChanged(object sender, EventArgs e)
        {
            switch (ComboBoxEditTreeStylingType.EditValue.ToString())
            {
                case "Permission Status":
                    {
                        _TreeThematicTypeID = 0;
     
                        colorEditTreeLegend1.Enabled = true;
                        colorEditTreeLegend1.Color = Color.Gray;
                        labelControlTreeLegend1.Text = "Not Permissioned";

                        colorEditTreeLegend2.Enabled = true;
                        colorEditTreeLegend2.Color = Color.LimeGreen;
                        labelControlTreeLegend2.Text = "Permissioned";

                        colorEditTreeLegend3.Enabled = true;
                        colorEditTreeLegend3.Color = Color.Red;
                        labelControlTreeLegend3.Text = "Not Permissioned";

                        colorEditTreeLegend4.Enabled = true;
                        colorEditTreeLegend4.Color = Color.FromArgb(255, 128, 0);
                        labelControlTreeLegend4.Text = "On Hold";
                        
                        colorEditTreeLegend5.Enabled = true;
                        colorEditTreeLegend5.Color = Color.DodgerBlue;
                        labelControlTreeLegend5.Text = "No Work";
                    }
                    break;
                case "None":
                    {
                        _TreeThematicTypeID = -1;

                        colorEditTreeLegend1.Enabled = false;
                        colorEditTreeLegend1.Color = Color.Transparent;
                        labelControlTreeLegend1.Text = "N\\A";

                        colorEditTreeLegend2.Enabled = false;
                        colorEditTreeLegend2.Color = Color.Transparent;
                        labelControlTreeLegend2.Text = "N\\A";

                        colorEditTreeLegend3.Enabled = false;
                        colorEditTreeLegend3.Color = Color.Transparent;
                        labelControlTreeLegend3.Text = "N\\A";

                        colorEditTreeLegend4.Enabled = false;
                        colorEditTreeLegend4.Color = Color.Transparent;
                        labelControlTreeLegend4.Text = "N\\A";
                        
                        colorEditTreeLegend5.Enabled = false;
                        colorEditTreeLegend5.Color = Color.Transparent;
                        labelControlTreeLegend5.Text = "N\\A";
                    }
                    break;
            }
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
            }
            Load_Tree_Objects_Grid();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void btnSurveyRefreshPoles_Click(object sender, EventArgs e)
        {
            /*if (_SelectedSurveyID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load Poles - Select the Survey to work with first then try again.", "Load Survey Poles", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }*/
            UpdateMapObjectsDisplayed(true); // Refresh Loaded Objects within Map //
        }

        public void Create_Legend()
        {
        }

        private void DrawShape(Graphics G, string strShapeName, int intStartX, int intStartY, Color BorderColor, Brush BrushStyle, int intWidth)
        {
            G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen pen = new Pen(BorderColor, intWidth);
            pen.Alignment = PenAlignment.Inset;

            switch (strShapeName)
            {
                case "Rectangle":
                    intStartX += 2;
                    intStartY += 2;
                    System.Drawing.Rectangle rec = new System.Drawing.Rectangle(intStartX, intStartY, 16, 16);
                    G.DrawRectangle(pen, rec);
                    G.FillRectangle(BrushStyle, rec);
                    break;
                case "Diamond":
                    System.Drawing.Point[] myPointArray = { new System.Drawing.Point(intStartX, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY + 20), new System.Drawing.Point(intStartX + 20, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray);
                    G.DrawPolygon(pen, myPointArray);
                    break;
                case "Star":
                    intStartX += 10;
                    intStartY += 10;
                    GraphicsPath star = Star(intStartX, intStartY, 4, 11, 5, 270);
                    G.FillPath(BrushStyle, star);
                    G.DrawPath(pen, star);
                    break;
                case "Circle":
                    intStartX += 2;
                    intStartY += 2;
                    System.Drawing.Rectangle rec2 = new System.Drawing.Rectangle(intStartX, intStartY, 16, 16);
                    G.FillEllipse(BrushStyle, rec2);
                    G.DrawEllipse(pen, rec2);
                    break;
                case "Triangle1":
                    intStartY += 5;
                    System.Drawing.Point[] myPointArray2 = { new System.Drawing.Point(intStartX, intStartY + 10), new System.Drawing.Point(intStartX + 10, intStartY), new System.Drawing.Point(intStartX + 20, intStartY + 10) };
                    G.FillPolygon(BrushStyle, myPointArray2);
                    G.DrawPolygon(pen, myPointArray2);
                    break;
                case "Triangle2":
                    intStartY += 5;
                    System.Drawing.Point[] myPointArray3 = { new System.Drawing.Point(intStartX, intStartY), new System.Drawing.Point(intStartX + 10, intStartY + 10), new System.Drawing.Point(intStartX + 20, intStartY) };
                    G.FillPolygon(BrushStyle, myPointArray3);
                    G.DrawPolygon(pen, myPointArray3);
                    break;
                case "Polygon":
                    System.Drawing.Point[] myPointArray4 = {new System.Drawing.Point(intStartX, intStartY + 7), 
                                                            new System.Drawing.Point(intStartX, intStartY + 19), 
                                                            new System.Drawing.Point(intStartX + 20, intStartY + 19), 
                                                            new System.Drawing.Point(intStartX + 20, intStartY + 11), 
                                                            new System.Drawing.Point(intStartX + 10, intStartY + 1), 
                                                            new System.Drawing.Point(intStartX, intStartY + 7) };
                    G.FillPolygon(BrushStyle, myPointArray4);
                    G.DrawPolygon(pen, myPointArray4);
                    break;
                case "Line":
                    G.DrawLine(pen, new System.Drawing.Point(intStartX, intStartY + 9), new System.Drawing.Point(intStartX + 20, intStartY + 9));
                    break;
                default:
                    break;
            }
            pen.Dispose();
        }

        private void DrawText(Graphics G, string strText, int intStartX, int intStartY, Brush BrushStyle, int intSize, FontStyle fontStyle)
        {
            G.DrawString(strText, new System.Drawing.Font("Tahoma", intSize, fontStyle), BrushStyle, (float)intStartX, (float)intStartY);
        }

        private const double DEGREE = 0.017453292519943295769236907684886;  // Number of radians in one degree //
        public static GraphicsPath Star(int c_x, int c_y, int inner_radius, int radius, int n, int rot)
        {
            rot %= 360;
            System.Drawing.Point[] points = new System.Drawing.Point[n * 2];
            byte[] types = new byte[n * 2];
            int index = 0;
            for (int a = rot; a < 360 + rot; a += 180 / n)
            {
                if (index >= points.Length) break;
                points[index] = new System.Drawing.Point(c_x + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Cos(DEGREE * a)), c_y + (int)(((index % 2 == 0) ? radius : inner_radius) * Math.Sin(DEGREE * a)));
                types[index] = (byte)PathPointType.Line;
                index++;
            }
            types[0] = (byte)PathPointType.Start;
            return new GraphicsPath(points, types);
        }

        #endregion


        #region Scale Bar

        private void bciScaleBar_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            Image image = null;
            if (bciScaleBar.Checked)
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                image = MapFunctions.Create_Scale_Bar(pictureBoxScaleBar.Size.Width, pictureBoxScaleBar.Size.Height, Convert.ToInt32(dCurrentMapScale));
                dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                pictureBoxScaleBar.Image = image;
            }
            else
            {
                dockPanel6.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                pictureBoxScaleBar.Image = image;
            }
        }

        private void dockPanel6_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciScaleBar.Checked) bciScaleBar.Checked = false;
        }

        private void dockPanel6_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }
        }

        private void dockPanel6_Resize(object sender, EventArgs e)
        {
            refresh_scale_bar();
        }

        private void pictureBoxScaleBar_Resize(object sender, EventArgs e)
        {
        }

        private void refresh_scale_bar()
        {
            if (pictureBoxScaleBar.Image != null)
            {
                Mapping_Functions MapFunctions = new Mapping_Functions();
                Image image = MapFunctions.Create_Scale_Bar(pictureBoxScaleBar.Size.Width, pictureBoxScaleBar.Size.Height, Convert.ToInt32(dCurrentMapScale));
                pictureBoxScaleBar.Image = image;
            }

        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        #endregion


        #region Gazetteer

        private void bciGazetteer_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciGazetteer.Checked)
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelGazetteer.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelGazetteer_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciGazetteer.Checked) bciGazetteer.Checked = false;
        }

        private void dockPanelGazetteer_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }
        }

        private void lookUpEditGazetteerSearchType_EditValueChanged(object sender, EventArgs e)
        {
            this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search.Rows.Clear();
            LookUpEdit lue = (LookUpEdit)sender;
            if (lue.EditValue.ToString().ToLower() == "localities")
            {
                gridControl3.MainView = gridView6;
            }
            else
            {
                gridControl3.MainView = gridView3;
            }
        }

        private void buttonEditGazetteerFindValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GazetteerFind();
        }

        private void GazetteerFind()
        {
            WaitDialogForm loading = new WaitDialogForm("Searching for Matches...", "WoodPlan Mapping");
            loading.Show();

            string strType = lookUpEditGazetteerSearchType.EditValue.ToString();
            int intPattern = Convert.ToInt32(radioGroupGazetteerMatchPattern.EditValue);
            string strValue = (intPattern == 0 ? "" : "%") + buttonEditGazetteerFindValue.EditValue.ToString() + "%";
            GridView view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            this.sp01312_AT_Tree_Picker_Gazetteer_SearchTableAdapter.Fill(this.dataSet_AT_TreePicker.sp01312_AT_Tree_Picker_Gazetteer_Search, strType, strValue);
            view.EndUpdate();

            loading.Close();
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) ShowLocality(Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate")));
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerShowMatch.Enabled = (intCount == 1 ? true : false);

                beiGazetteerLoadObjectsWithinRange.Enabled = (intCount == 1 ? true : false);
                beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Always;  // Show Range Spinner menu item //

                bbiGazetteerLoadMapObjectsInRange.Enabled = (intCount == 1 ? true : false);
                bbiGazetteerLoadMapObjectsInRange.Caption = "Load  Map Objects in Range";

                bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Always;

                popupMenu_Gazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle) ShowLocality(Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate")));
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (hitInfo.RowHandle >= 0)  // Make sure not on a Group row //
                //{
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                bbiGazetteerShowMatch.Enabled = (intCount == 1 ? true : false);

                beiGazetteerLoadObjectsWithinRange.Enabled = false;
                beiGazetteerLoadObjectsWithinRange.Visibility = BarItemVisibility.Never;  // Hide Range Spinner menu item //

                bbiGazetteerLoadMapObjectsInRange.Enabled = (intCount == 1 ? true : false);
                bbiGazetteerLoadMapObjectsInRange.Caption = "Load All Map Objects in Locality";

                bbiGazetteerClearMapSearch.Visibility = BarItemVisibility.Never;

                popupMenu_Gazetteer.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void ShowLocality(double doubleX, double doubleY)
        {
            MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(doubleX, doubleY);  // create a DPoint to center the map on //

            Double dblCurrentScale = (this.mapControl1.Map.Scale > 5000 ? 5000 : this.mapControl1.Map.Scale);
            this.mapControl1.Map.SetView(dpt1, mapControl1.Map.GetDisplayCoordSys(), dblCurrentScale);

            // Draw Locus Effect on object so user can see it //
            System.Drawing.Point screenPoint;
            MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
            converter.ToDisplay(dpt1, out screenPoint);

            System.Drawing.Point MapControlPoint = new System.Drawing.Point();
            MapControlPoint = this.mapControl1.PointToScreen(MapControlPoint);

            screenPoint.X += MapControlPoint.X;
            screenPoint.Y += MapControlPoint.Y;

            locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customBeaconLocusEffect1.Name);
        }

        private void buttonEditGazetteerFindValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) GazetteerFind();
        }

        private void bbiGazetteerShowMatch_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one Gazetteer match to locate on the map before proceeding.", "Gazetteer - Show Match", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            ShowLocality(Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "XCoordinate")), Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "YCoordinate")));
        }

        private void bbiGazetteerLoadMapObjectsInRange_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            string strType = lookUpEditGazetteerSearchType.EditValue.ToString();
            if (strType == "Localities")
            {
                int intLocalityID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RecordID"));

                Mapping_Functions MapFunctions = new Mapping_Functions();
                MapFunctions.Gazetteer_Map_Object_Search(this.GlobalSettings, this.strConnectionString, this.ParentForm, intLocalityID.ToString() + ",", "locality", "", "");
            }
            else // Address //
            {
                decimal decX = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "XCoordinate"));
                decimal decY = Convert.ToDecimal(view.GetRowCellValue(view.FocusedRowHandle, "YCoordinate"));
                decimal decRange = Convert.ToDecimal(beiGazetteerLoadObjectsWithinRange.EditValue);
                //decimal decRangeAdjusted = (decRangeSpinner * (decimal)100) / (decimal)113.5;  // Gets round round size of circle drawn (always 13.5% too big) //
                decimal decRangeAdjusted = decRange;
                string strDescription = "";
                strDescription = (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine1").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine1").ToString() + " ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine2").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine2").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine3").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine3").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine4").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine4").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AddressLine5").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AddressLine5").ToString() + ", ") + (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "Postcode").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "Postcode").ToString());
                if (string.IsNullOrEmpty(strDescription)) strDescription = "No Address Specified";
                SearchForNearbyMapObjects(decX, decY, decRange, strDescription);

            }
        }

        private void bbiGazetteerClearMapSearch_ItemClick(object sender, ItemClickEventArgs e)
        {
            Clear_Search_Results();
        }

        #endregion


        #region Survey

        private void dockPanelSurvey_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciSurvey.Checked) bciSurvey.Checked = false;
        }

        private void dockPanelSurvey_DockChanged(object sender, EventArgs e)
        {
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }

        }

        private void bciSurvey_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (bciSurvey.Checked)
            {
                if (dockPanelSurvey.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Hidden)
                {
                    dockPanelSurvey.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                    if (dockPanelSurvey.Dock == DockingStyle.Float)
                    {
                        // Important note: Forms StartPosition needs to be set to Manual in the Form's properties //
                        dockPanelSurvey.FloatLocation = new System.Drawing.Point(PointToScreen(mapControl1.Location).X + panelContainer1.Size.Width, PointToScreen(mapControl1.Location).Y + 42);
                    }
                }
            }
            else
            {
                if (dockPanelSurvey.Visibility != DevExpress.XtraBars.Docking.DockVisibility.Hidden) dockPanelSurvey.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void buttonEditSurvey_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                frm_UT_Select_Survey fChildForm = new frm_UT_Select_Survey();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (_SelectedSurveyID > 0)
                {
                    fChildForm.intPassedInSurveyID = _SelectedSurveyID;
                    fChildForm.intPassedInClientID = _SelectedSurveyClientID;
                }
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                {
                    buttonEditSurvey.Text = fChildForm.strSelectedValue;
                    _SelectedSurveyID = fChildForm.intSelectedSurveyID;
                    _SelectedSurveyClientID = fChildForm.intSelectedClientID;
                    Load_Map_Objects_Grid();  // Reload Poles Grid //
                    UpdateMapObjectsDisplayed(true); // Refresh Loaded Objects within Map //
                }
            }
            else if (e.Button.Tag.ToString() == "clear")  // Clear Button //
            {
                buttonEditSurvey.Text = "";
                _SelectedSurveyID = 0;
                _SelectedSurveyClientID = 0;
                Load_Map_Objects_Grid();  // Reload Poles Grid //
                UpdateMapObjectsDisplayed(true); // Refresh Loaded Objects within Map //
            }

        }

        private void btnGPS_Click(object sender, EventArgs e)
        {
            Start_Stop_GPS();
        }

        private void bbiTransferPolesToSurvey_ItemClick(object sender, ItemClickEventArgs e)
        {
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Adding Poles to Survey...");

            view.BeginUpdate();
            string strNewIDs = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    string strSelectedIDs = "";
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) == -1)  // Pole Object and not on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";
                            f["SurveyStatus"] = "Survey To Be Started";  // Update map object //
                            f["SurveyDone"] = 0;  // Update map object //
                            if (_PoleThematicTypeID == 0) f["ThematicValue"] = 0;  // Update map object //

                            // Change Colour //
                            CompositeStyle style = Get_Default_Style_Pole();
                            style.SymbolStyle.Color = colourPoleLegend2;
                            f.Style = style;
                            
                            f.Update();

                            intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                            if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                            {
                                view.SetRowCellValue(intRowHandle, "SurveyDone", 0);  // Update grid row //
                                view.SetRowCellValue(intRowHandle, "SurveyStatus", "Survey To Be Started");  // Update grid row //
                                if (_PoleThematicTypeID == 0) view.SetRowCellValue(intRowHandle, "ThematicValue", 0);  // Update grid row //
                            }
                        }
                    }
                    // Add poles to survey //
                    try
                    {
                        DataSet_UT_MappingTableAdapters.QueriesTableAdapter AddPolesToSurvey = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                        AddPolesToSurvey.ChangeConnectionString(strConnectionString);
                        strNewIDs = AddPolesToSurvey.sp07110_UT_Survey_Add_Poles_To_Survey(strSelectedIDs, _SelectedSurveyID, GlobalSettings.UserID, GlobalSettings.UserID).ToString();
                    }
                    catch (Exception ex)
                    {
                        view.EndUpdate();
                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add selected poles to survey - an error occurred [ " + ex.Message + "].\nYou should refresh the list of Map Objects then reselect the poles to add and try again.", "Add Poles to Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }
            view.EndUpdate();

            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.i_str_AddedRecordIDs1 = strNewIDs;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            selection.Clear();  // Clear MapInfo Selection //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiRemovePolesFromSurvey_ItemClick(object sender, ItemClickEventArgs e)
        {
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish the to remove the selected poles from the current survey?\r\nIf you proceed, the survey information for the selected poles will be deleted!", "Remove Poles from Survey", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Removing Poles from Survey...");

            view.BeginUpdate();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    string strSelectedIDs = "";
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) >= 0 && Convert.ToInt32(f["SurveyDone"]) < 3)  // Pole Object and on survey (and not completed) //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";
                            f["SurveyStatus"] = "No Survey Required";  // Update map object //
                            f["SurveyDone"] = -1;  // Update map object //
                            if (_PoleThematicTypeID == 0) f["ThematicValue"] = -1;  // Update map object //

                            // Change Colour //
                            CompositeStyle style = Get_Default_Style_Pole();
                            style.SymbolStyle.Color = colourPoleLegend1;
                            f.Style = style;

                            f.Update();

                            intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                            if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                            {
                                view.SetRowCellValue(intRowHandle, "SurveyDone", -1);  // Update grid row //
                                view.SetRowCellValue(intRowHandle, "SurveyStatus", "No Survey Required");  // Update grid row //
                                if (_PoleThematicTypeID == 0) view.SetRowCellValue(intRowHandle, "ThematicValue", -1);  // Update grid row //
                            }
                        }
                    }
                    // Remove poles from survey //
                    try
                    {
                        DataSet_UT_MappingTableAdapters.QueriesTableAdapter RemovePolesFromSurvey = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                        RemovePolesFromSurvey.ChangeConnectionString(strConnectionString);
                        RemovePolesFromSurvey.sp07113_UT_Survey_Remove_Poles_From_Survey(strSelectedIDs, _SelectedSurveyID);
                    }
                    catch (Exception ex)
                    {
                        view.EndUpdate();
                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to remove selected poles from survey - an error occurred [ " + ex.Message + "].\nYou should refresh the list of Map Objects then reselect the poles to remove and try again.", "Remove Poles from Survey", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
            }
            view.EndUpdate();

            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            selection.Clear();  // Clear MapInfo Selection //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiSurveyPole_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectedIDs = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) >= 0)  // Pole Object and on survey (-1 = not on survey) //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            textEditPlotTreesAgainstPole.Text = f["ObjectReference"].ToString();
                            _CurrentPoleID = Convert.ToInt32(f["MapID"]);
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a pole to survey before proceeding.", "Survey Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_UT_Survey_Pole2 fChildForm = new frm_UT_Survey_Pole2();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm.strRecordIDs = strSelectedIDs;
            fChildForm._SurveyID = _SelectedSurveyID;
            //fChildForm.intPassedInSurveyPoleID = intSurveyPoleID;
            fChildForm.strFormMode = "edit";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            
            System.Reflection.MethodInfo method = null;
            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiSurveyPoleSetClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) == 0)  // Pole Object and on survey but not started //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to <b>mark as clear</b> before proceeding.", "Survey Pole(s) - Set Clear", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as <b>Surveyed - Clear of Infestation</b>.\n\nAre you sure you wish to proceed?\n\nIf you proceed the system will require you to take a panoramic picture of the clear poles...";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Clear", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            string strSavedPictureFolder = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSavedPictureFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (string.IsNullOrEmpty(strSavedPictureFolder))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The default Picture Files path (from the System Configuration Screen) has not been set yet.\n\nPlease set this value in the System configuration screen before proceeding.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strSavedPictureFolder.EndsWith("\\")) strSavedPictureFolder += "\\";

            frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
            fChildForm.strCaller = this.Name;
            fChildForm.strConnectionString = strConnectionString;
            fChildForm.strFormMode = "add";
            fChildForm.intAddToRecordID = -999;
            fChildForm.intAddToRecordTypeID = 0;  // 1 = Surveyed Pole //
            fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
            fChildForm.ShowDialog();
            Image imageCaptured = fChildForm._returnedImage;
            if (imageCaptured == null) return;  // No image captured //
            string strPictureRemarks = fChildForm._returnedRemarks;

            // Process each selected pole on map and link image to pole survey then set status to Completed //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DateTime CurrentDateTime = DateTime.Now;
            string strImageName = "";
            string strSaveFileName = "";
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter SetPoleSurveyedClear = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            SetPoleSurveyedClear.ChangeConnectionString(strConnectionString);

            foreach (string strElement in strArray)
            {
                try
                {
                    strImageName = "Survey_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + strElement + ".jpg";
                    strSaveFileName = strSavedPictureFolder + strImageName;
                    imageCaptured.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    SetPoleSurveyedClear.sp07185_UT_Surveyed_Pole_Set_As_Clear(Convert.ToInt32(strElement), _SelectedSurveyID, strImageName, CurrentDateTime, strPictureRemarks, GlobalSettings.UserID);

                }
                catch (Exception ex)
                {
                }

                // Refresh map - show selected poles as Completed and clear map selection //
                Update_Pole_Survey_Status(Convert.ToInt32(strElement), 3, -999, -999);
                
                // Refresh Survey Edit screen if it is open \\
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frm_UT_Survey_Edit")
                        {
                            var fParentForm = (frm_UT_Survey_Edit)frmChild;
                            fParentForm.Load_Surveyed_Poles();
                            fParentForm.FilterGrids();
                        }
                    }
                }
            }
            selection.Clear();
        }

        public void Update_Pole_Survey_Status(int intPoleID, int intSurveyStatus, int intShutdown, int intTrafficManagement)
        {
            // Called by frm_UT_Survey_Pole2 - on Save event, and from within this Mapping screen in various places //
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];

            //*** Get Map Object to update ***//
            MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();  // create connection and open table //
            connection.Open();
            MapInfo.Data.Table t = connection.Catalog.GetTable("MapObjects");
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("MapID = " + intPoleID.ToString());
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            connection.Close();
            connection.Dispose();

            view.BeginUpdate();

            foreach (Feature f in irfc)  // Should only be one feature in collection //
            {
                if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                {
                    int intThematicValue = 0;
                    switch (_PoleThematicTypeID)
                    {
                        case 0:
                            {
                                intThematicValue = intSurveyStatus;
                            }
                            break;
                        case 1:
                            {
                                intThematicValue = intShutdown;
                            }
                            break;
                        case 2:
                            {
                                intThematicValue = intTrafficManagement;
                            }
                            break;
                        default:
                            break;
                    }
                    if (intThematicValue != -999) f["ThematicValue"] = intThematicValue;  // Update map object //

                    string strStatus = "";
                    if (intSurveyStatus != -999)  // Ignore if this value wasn't set (may be one of the other thematic criteria set instead) //
                    {
                        switch (intSurveyStatus)
                        {
                            case -1:
                                strStatus = "No Survey Required";
                                break;
                            case 0:
                                strStatus = "Survey To Be Started";
                                break;
                            case 1:
                                strStatus = "Survey Started";
                                break;
                            case 2:
                                strStatus = "Survey On-Hold";
                                break;
                            case 3:
                                strStatus = "Survey Completed";
                                break;
                            case 4:
                                strStatus = "Survey Permissioned";
                                break;
                            case 5:
                                strStatus = "Work Completed";
                                break;
                            default:
                                strStatus = "Unknown";
                                break;
                        }
                        f["SurveyDone"] = intSurveyStatus;
                    }
                    if (_SelectedSurveyID > 0)
                    {
                        // Change Colour //
                        CompositeStyle style = Get_Default_Style_Pole();
                        style.SymbolStyle.Color = Get_Pole_Colour(intThematicValue);
                        f.Style = style;
                    }
                    f.Update();

                    intRowHandle = view.LocateByValue(0, col, intPoleID);
                    if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                    {
                        if (intSurveyStatus != -999 && !string.IsNullOrWhiteSpace(strStatus)) view.SetRowCellValue(intRowHandle, "SurveyStatus", strStatus);  // Update grid row //
                        if (intThematicValue != -999) view.SetRowCellValue(intRowHandle, "ThematicValue", intThematicValue);  // Update grid row //
                    }
                }
            }
            view.EndUpdate();
        }

        private void bbiViewPoleOwnership_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectedIDs = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                         }
                    }
                }
            }
            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to view pole ownership of before proceeding.", "View Pole Ownership", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_UT_Pole_Ownership_View fChildForm = new frm_UT_Pole_Ownership_View();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._PassedInIDs = strSelectedIDs;
            fChildForm._AllowDelete = iBool_AllowDelete;
            fChildForm.ShowDialog();
        }

        private void bbiViewPictures_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectedIDs = "";
            int intType = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        //if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                        //{
                        //    strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                        //}
                        strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                        intType = (Convert.ToInt32(f["ModuleID"]) == 1  ? 0 : 1);
                    }
                }
            }
            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to view pole pictures of before proceeding.", "View Pole Pictures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_UT_Picture_Viewer fChildForm = new frm_UT_Picture_Viewer();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._SelectedValues = strSelectedIDs;
            fChildForm._SelectedTypeID = intType;  // 0 = Pole and Linked Tree pictures, 1 = Tree pictures //
            fChildForm.ShowDialog();
        }


        private void bbiShutdownRequired_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on surveyed //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to mark as Require Shutdown before proceeding.", "Survey Pole(s) - Set Require Shutdown", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as Requiring Shutdown.\n\nAre you sure you wish to proceed?";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Require Shutdown", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            Set_Shutdown_Status(strSelectedIDs, 1);
            //selection.Clear();
        }
        private void bbiShutdownClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on surveyed //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to mark as Do Not Require Shutdown before proceeding.", "Survey Pole(s) - Set Do Not Require Shutdown", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as Do Not Require Shutdown.\n\nAre you sure you wish to proceed?";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Do Not Require Shutdown", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            Set_Shutdown_Status(strSelectedIDs, 0);
            //selection.Clear();
        }
        private void Set_Shutdown_Status(string strSelectedIDs, int intStatus)
        {
            // Process each selected pole on map then set as Shutdown Status (0 = no, 1 = yes) //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter SetPoleShutdown = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            SetPoleShutdown.ChangeConnectionString(strConnectionString);

            foreach (string strElement in strArray)
            {
                try
                {
                    SetPoleShutdown.sp07360_UT_Surveyed_Pole_Set_Shutdown_Status(Convert.ToInt32(strElement), _SelectedSurveyID, intStatus);
                }
                catch (Exception ex)
                {
                }

                // Refresh map - show selected poles as Completed and clear map selection (-999 means leave existing value alone) //
                Update_Pole_Survey_Status(Convert.ToInt32(strElement), -999, intStatus, -999);
            }
            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
        }

        private void bbiTrafficManagmentRequired_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to set <b>Traffic Management</b> for before proceeding.", "Survey Pole(s) - Set Traffic Management", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to set <b>Traffic Management</b> for " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + ".\n\nAre you sure you wish to proceed?\n\nIf you proceed the system will allow you to specify any Traffic Management items required...";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Traffic Management", MessageBoxButtons.YesNo, MessageBoxIcon.Information, DefaultBoolean.True) == DialogResult.No) return;

            DataSet dsTrafficItems = new DataSet("NewDataSet");
            SqlDataAdapter sdaTrafficItems = new SqlDataAdapter();
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp07362_UT_Traffic_Management_Item", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@strRecordIDs", ""));
            cmd.Parameters.Add(new SqlParameter("@strMode", ""));
            dsTrafficItems.Clear();  // Remove old values first //
            sdaTrafficItems = new SqlDataAdapter(cmd);
            sdaTrafficItems.Fill(dsTrafficItems, "Table");
            SQlConn.Close();
            SQlConn.Dispose();

            frm_UT_Surveyed_Pole_Set_TM_Required fChildForm = new frm_UT_Surveyed_Pole_Set_TM_Required();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.dsTrafficItems = dsTrafficItems;
            fChildForm.intRecordCount = selection.Count;
            if (fChildForm.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            // Process each selected pole on map and set traffic management status //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter SetTM = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            SetTM.ChangeConnectionString(strConnectionString);

            foreach (string strElement in strArray)
            {
                try
                {
                    SetTM.sp07365_UT_Traffic_Management_Set_Required(Convert.ToInt32(strElement), _SelectedSurveyID, 1);
                    if (dsTrafficItems.Tables[0].Rows.Count > 0)  // Add all Traffic Management Items //
                    {
                        foreach (DataRow dr in dsTrafficItems.Tables[0].Rows)
                        {
                            SetTM.sp07364_UT_Traffic_Management_Item_Add(Convert.ToInt32(strElement), _SelectedSurveyID, Convert.ToInt32(dr["TrafficManagementTypeID"]), Convert.ToDecimal(dr["AmountRequired"]), dr["Remarks"].ToString());
                        } 
                    }
                }
                catch (Exception ex)
                {
                }

                // Refresh map - show selected poles as Traffic Management Required and clear map selection (-999 means leave existing value alone) //
                Update_Pole_Survey_Status(Convert.ToInt32(strElement), -999, -999, 1);
            }
            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            //selection.Clear();
        }
        private void bbiTrafficManagmentClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to <b>clear Traffic Management</b> for before proceeding.", "Survey Pole(s) - Clear Traffic Management", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to <b>clear Traffic Management</b> for " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + ".\n\nAre you sure you wish to proceed?\n\nIf you proceed any linked Traffic Management Items will be deleted.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Clear Traffic Management", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            // Process each selected pole on map and set traffic management status //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter SetTM = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            SetTM.ChangeConnectionString(strConnectionString);

            foreach (string strElement in strArray)
            {
                try
                {
                    SetTM.sp07365_UT_Traffic_Management_Set_Required(Convert.ToInt32(strElement), _SelectedSurveyID, 0);
                }
                catch (Exception ex)
                {
                }

                // Refresh map - show selected poles as Traffic Management NOT Required and clear map selection (-999 means leave existing value alone) //
                Update_Pole_Survey_Status(Convert.ToInt32(strElement), -999, -999, 0);
            }
            // Refresh Survey Edit screen if it is open \\
                        if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            //selection.Clear();
        }
        private void bbiTrafficMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to create a <b>Traffic Map</b> for before proceeding.", "Survey Pole(s) - Create Traffic Map", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to create a <b>Traffic Map</b> for " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + ".\n\nAre you sure you wish to proceed?\n\nIf you proceed the system will open the Map Snapshot screen to allow you to create your map.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Create Traffic Map", MessageBoxButtons.YesNo, MessageBoxIcon.Information, DefaultBoolean.True) == DialogResult.No) return;

            // Process each selected pole on map //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedPoleID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedPoleID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedPoleID = Convert.ToInt32(GetSurveyedPoleID.sp07371_UT_Mapping_Get_Surveyed_Pole_From_Survey_and_Pole(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedPoleID > 0) strRecordIDs += intSurveyedPoleID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Generating Map Preview...");

            Image image = null;
            this.Generate_Work_Order_Map(ref image);

            // Get current DPI from screen //
            Graphics dspGraphics = CreateGraphics();
            int intDPI = Convert.ToInt32(dspGraphics.DpiX);
            dspGraphics.Dispose();

            string strDefaultMapPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
                if (!strDefaultMapPath.EndsWith("\\")) strDefaultMapPath += "\\";
            }
            catch (Exception)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_UT_Mapping_Map_Snapshot_Map frm_child = new frm_UT_Mapping_Map_Snapshot_Map();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.strFormMode = "Add";
            frm_child.imageMap = image;
            frm_child.frmParentMappingForm = this;
            frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale); ;
            frm_child.intDPI = intDPI;
            frm_child._LinkedToRecordID = 0;
            frm_child._LinkedToRecordTypeID = 4;  // Traffic Map //
            frm_child._LinkedToRecordIDs = strRecordIDs;
            frm_child._LinkedToRecordDescription = "";
            frm_child._PassedInMapDescription = "Traffic Management Map";
            frm_child.strMapsPath = strDefaultMapPath;

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            frm_child.ShowDialog();
            string strSavedMapName = frm_child._SavedMapName;

            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                    if (frmChild.Name == "frm_UT_Survey_Pole2")
                    {
                        var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                        fParentForm.Update_Linked_Map_Path(4, strRecordIDs, strSavedMapName);
                    }
                }
            }
            //selection.Clear();
        }

        private void bbiDeferredSet_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to set as <b>Deferred</b> before proceeding.", "Survey Pole(s) - Set Deferred", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as <b>Deferred</b>.\n\nAre you sure you wish to proceed?\n\nIf you proceed the system will require you to enter a Revisit Date and a Deferred Reason...";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Deferred", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            frm_UT_Surveyed_Pole_Set_Deferred fChildForm = new frm_UT_Surveyed_Pole_Set_Deferred();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intRecordCount = selection.Count;
            if (fChildForm.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            int intUnits = fChildForm._Units;
            int intUnitDescriptorID = fChildForm._UnitDescriptorID;
            int intReasonID = fChildForm._ReasonID;
            DateTime dtRevisitDate = fChildForm._RevisitDate;
            string strRemarks = fChildForm._Remarks;

            Set_Deffered_Status(strSelectedIDs, 1, intUnits, intUnitDescriptorID, intReasonID, dtRevisitDate, strRemarks);
        }
        private void bbiClearDeferred_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to set as <b>Not Deferred</b> before proceeding.", "Survey Pole(s) - Set Not Deferred", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as <b>Not Deferred</b>.\n\nAre you sure you wish to proceed?\n\n";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Set Not Deferred", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            Set_Deffered_Status(strSelectedIDs, 0, 0, 0, 0, null, null);
        }
        private void Set_Deffered_Status(string strSelectedIDs, int intStatus, int intUnits, int intUnitDescriptorID, int intReasonID, DateTime? dtRevisitDate, string strRemarks)
        {
            // Process each selected pole on map and set deferred status //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter SetDeferredPole = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            SetDeferredPole.ChangeConnectionString(strConnectionString);

            foreach (string strElement in strArray)
            {
                try
                {
                    SetDeferredPole.sp07361_UT_Surveyed_Pole_Set_Deferred(Convert.ToInt32(strElement), _SelectedSurveyID, intStatus, intUnits, intUnitDescriptorID, dtRevisitDate, intReasonID, strRemarks);
                }
                catch (Exception ex)
                {
                }

                // Refresh map - show selected poles as Completed and clear map selection //
                //Update_Pole_Survey_Status(Convert.ToInt32(strElement), 3);
            }
            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            //selection.Clear();
        }

        private void bbiCreateAccessMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to create an <b>Access Map</b> for before proceeding.", "Survey Pole(s) - Create Access Map", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to create an <b>Access Map</b> for " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + ".\n\nAre you sure you wish to proceed?\n\nIf you proceed the system will open the Map Snapshot screen to allow you to create your map.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Survey Pole(s) - Create Access Map", MessageBoxButtons.YesNo, MessageBoxIcon.Information, DefaultBoolean.True) == DialogResult.No) return;

            // Process each selected pole on map //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedPoleID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedPoleID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedPoleID = Convert.ToInt32(GetSurveyedPoleID.sp07371_UT_Mapping_Get_Surveyed_Pole_From_Survey_and_Pole(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedPoleID > 0) strRecordIDs += intSurveyedPoleID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Generating Map Preview...");

            Image image = null;
            this.Generate_Work_Order_Map(ref image);

            // Get current DPI from screen //
            Graphics dspGraphics = CreateGraphics();
            int intDPI = Convert.ToInt32(dspGraphics.DpiX);
            dspGraphics.Dispose();

            string strDefaultMapPath = "";
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
                if (!strDefaultMapPath.EndsWith("\\")) strDefaultMapPath += "\\";
            }
            catch (Exception)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            frm_UT_Mapping_Map_Snapshot_Map frm_child = new frm_UT_Mapping_Map_Snapshot_Map();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child.strFormMode = "Add";
            frm_child.imageMap = image;
            frm_child.frmParentMappingForm = this;
            frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale); ;
            frm_child.intDPI = intDPI;
            frm_child._LinkedToRecordID = 0;
            frm_child._LinkedToRecordTypeID = 3;  // Access Map //
            frm_child._LinkedToRecordIDs = strRecordIDs;
            frm_child._LinkedToRecordDescription = "";
            frm_child._PassedInMapDescription = "Access Map";
            frm_child.strMapsPath = strDefaultMapPath;

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            frm_child.ShowDialog();
            string strSavedMapName = frm_child._SavedMapName;

            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                    if (frmChild.Name == "frm_UT_Survey_Pole2")
                    {
                        var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                        fParentForm.Update_Linked_Map_Path(3, strRecordIDs, strSavedMapName);
                    }
                }
            }
            //selection.Clear();
        }

        private void bbiCopyJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to <b>copy</b> their jobs into memory before proceeding.", "Survey Pole(s) - Copy Jobs into Memory", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            // Process each selected pole on map //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedPoleID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedPoleID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedPoleID = Convert.ToInt32(GetSurveyedPoleID.sp07371_UT_Mapping_Get_Surveyed_Pole_From_Survey_and_Pole(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedPoleID > 0) strRecordIDs += intSurveyedPoleID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            // We have a list of surveyed poles now so pass to screen and load their jobs... //
            frm_UT_Jobs_In_Memory_Copy frm_child = new frm_UT_Jobs_In_Memory_Copy();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._PassedInSurveyedPoleIDs = strRecordIDs;
            frm_child.ShowDialog();      
        }

        private void bbiPasteJobs_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1 && Convert.ToInt32(f["SurveyDone"]) > 0)  // Pole Object and on survey //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to <b>paste the jobs</b> in memory to before proceeding.", "Survey Pole(s) - Paste Jobs from Memory", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            // Process each selected pole on map //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedPoleID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedPoleID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedPoleID = Convert.ToInt32(GetSurveyedPoleID.sp07371_UT_Mapping_Get_Surveyed_Pole_From_Survey_and_Pole(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedPoleID > 0) strRecordIDs += intSurveyedPoleID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            // We have a list of surveyed poles now so pass to screen and load the jobs from memory... //
            frm_UT_Jobs_In_Memory_Paste frm_child = new frm_UT_Jobs_In_Memory_Paste();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._PassedInSurveyedPoleIDs = strRecordIDs;
            frm_child.ShowDialog();
        }

        private void bbiPermission_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            string strSelectedMapIDs = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 2 )  // Tree Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Tree ID //
                            strSelectedMapIDs += f["ID"].ToString() + ",";  // This is the Map ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=2 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Tree ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }
                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Trees before proceeding.", "Survey Pole(s) - Permission Tree Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Process each selected tree on map (pick up the Surveyed Tree ID from each) //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedTreeID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedTreeID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedTreeID = Convert.ToInt32(GetSurveyedTreeID.sp07388_UT_Mapping_Get_Surveyed_Tree_From_Survey_and_Tree(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedTreeID > 0) strRecordIDs += intSurveyedTreeID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            // We have a list of surveyed trees now so pass to screen and load their jobs... //
            frm_UT_Mapping_Permission_Work frm_child = new frm_UT_Mapping_Permission_Work();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._PassedInSurveyedTreeIDs = strRecordIDs;
            if (frm_child.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            string DoWhat = frm_child._DoWhat;
            string ActionIDs = frm_child._SelectedActionIDs;
            if (string.IsNullOrEmpty(ActionIDs)) return;  // This should never happen! //
            int PermissionDocumentID = 0;

            // Get Permission Document Layout to use //
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetLayout = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            GetLayout.ChangeConnectionString(strConnectionString);

            string strLayout = "";
            try
            {
                strLayout = GetLayout.sp07395_UT_PD_Get_Layout_From_ClientID(_SelectedSurveyClientID, 1).ToString();  // 1 = Screen Layout, 2 = Report Layout //
            }
            catch (Exception ex)
            {
            }
            if (string.IsNullOrEmpty(strLayout))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to find the Permission Document layout to use.\n\nThe Layout to use is set on the master client record. Please check and adjust before trying again.", "Survey Pole(s) - Permission Tree Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DoWhat == "CreateNew")
            {
                // Create New Permission Document //
                DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter CreatePD = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                CreatePD.ChangeConnectionString(strConnectionString);

                string strReferenceNumber = GlobalSettings.UserForename.Substring(0,1).ToUpper() + GlobalSettings.UserSurname.Substring(0,1).ToUpper() + DateTime.Now.ToString("yyyyMMddHHmmss");
                try
                {
                    PermissionDocumentID = Convert.ToInt32(CreatePD.sp07391_UT_PD_Insert(DateTime.Now, GlobalSettings.UserID, strReferenceNumber, ActionIDs, _SelectedSurveyClientID));
                    if (PermissionDocumentID <= 0) return;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Permission Document - [" + ex.Message + "].", "Survey Pole(s) - Permission Tree Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // "AddExisting" //
            {
                // Add Selected Work to Existing Permisison Document //
                frm_UT_Select_Permission_Document fChildForm = new frm_UT_Select_Permission_Document();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInClientID = _SelectedSurveyClientID;
                if (fChildForm.ShowDialog() != DialogResult.OK) return;

                PermissionDocumentID = fChildForm.intSelectedID;
                DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddWorkToPermissionDocument = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                AddWorkToPermissionDocument.ChangeConnectionString(strConnectionString);
                try
                {
                    Convert.ToInt32(AddWorkToPermissionDocument.sp07243_UT_Add_Work_To_Permission_Document(PermissionDocumentID, ActionIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add work to existing permission document - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            // Update Thematics for tree if necessary //
            if (_TreeThematicTypeID != -1) this.RefreshMapObjects(strSelectedMapIDs, 1);

            if (PermissionDocumentID <= 0) return;
            // Permission Document now created or existing one added to, so open it //

            if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
            {
                frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
            {
                frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }           
        }

        private void bbiPermissionView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            string strSelectedMapIDs = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 2)  // Tree Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Tree ID //
                            strSelectedMapIDs += f["ID"].ToString() + ",";  // This is the Map ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=2 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Tree ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }
                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Trees before proceeding.", "View Permissions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Process each selected tree on map (pick up the Surveyed Tree ID from each) //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedTreeID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedTreeID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedTreeID = Convert.ToInt32(GetSurveyedTreeID.sp07388_UT_Mapping_Get_Surveyed_Tree_From_Survey_and_Tree(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedTreeID > 0) strRecordIDs += intSurveyedTreeID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }
            frm_UT_Surveyed_Tree_Permission_Documents_View fChild = new frm_UT_Surveyed_Tree_Permission_Documents_View();
            fChild.GlobalSettings = this.GlobalSettings;
            fChild.FormPermissions = this.FormPermissions;
            fChild._PassedInSurvedTreeIDs = strRecordIDs;
            if (fChild.ShowDialog() != DialogResult.OK) return;

            string strLayout = fChild._SelectedDataEntryScreen;
            int PermissionDocumentID = fChild._SelectedPermissionDocumentID;
            if (PermissionDocumentID <= 0 || string.IsNullOrWhiteSpace(strLayout)) return;

            if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
            {
                frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "view";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
            {
                frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = PermissionDocumentID + ",";
                fChildForm.strFormMode = "view";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 1;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }           

        }

        private void bbiSetPoleHistorical_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to mark as <b>Historical</b>before proceeding.", "Pole(s) - Set As Historical", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            string strMessage = "You are about to set " + intCount.ToString() + (intCount == 1 ? " pole" : " poles") + " as <b>Historical</b>.\n\nAre you sure you wish to proceed?\n\nIf your proceed " + (intCount == 1 ? "this pole" : "these poles") + " will be removed from the current survey and marked as Historical.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Pole(s) - Set As Historical", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.No) return;

            Set_Pole_Status(2, -1);
        }

        private void Set_Pole_Status(int PoleStatusID, int SurveyedPoleStatusID)
        {
            string strStatusDescription = "";
            switch (PoleStatusID)
            {
                case 0:
                    {
                        strStatusDescription = "Planned";
                    }
                    break;
                case 1:
                    {
                        strStatusDescription = "Actual";
                    }
                    break;
                case 2:
                    {
                        strStatusDescription = "Historical";
                    }
                    break;
                default:
                    {
                        strStatusDescription = "Unknown";
                    }
                    break;
            }

            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Setting Pole(s) as " + strStatusDescription + "...");

            view.BeginUpdate();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    string strSelectedIDs = "";
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1) // Pole Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";
                            f["SurveyStatus"] = "No Survey Required";  // Update map object //
                            f["SurveyDone"] = -1;  // Update map object //
                            if (_PoleThematicTypeID == 0) f["ThematicValue"] = -1;  // Update map object //

                            // Change Colour //
                            CompositeStyle style = Get_Default_Style_Pole();
                            style.SymbolStyle.Color = colourPoleLegend1;
                            f.Style = style;

                            f.Update();

                            intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                            if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                            {
                                view.SetRowCellValue(intRowHandle, "SurveyDone", -1);  // Update grid row //
                                view.SetRowCellValue(intRowHandle, "SurveyStatus", "No Survey Required");  // Update grid row //
                                view.SetRowCellValue(intRowHandle, "Status", strStatusDescription);  // Update grid row //
                                if (_PoleThematicTypeID == 0) view.SetRowCellValue(intRowHandle, "ThematicValue", -1);  // Update grid row //
                            }
                        }
                    }
                    // Update poles with new StatusID //
                    try
                    {
                        DataSet_UT_MappingTableAdapters.QueriesTableAdapter UpdatePoleStatus = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                        UpdatePoleStatus.ChangeConnectionString(strConnectionString);
                        UpdatePoleStatus.sp07467_UT_Set_Pole_Status(strSelectedIDs, _SelectedSurveyID, PoleStatusID, SurveyedPoleStatusID);
                    }
                    catch (Exception ex)
                    {
                        view.EndUpdate();
                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to set selected pole(s) as <b>" + strStatusDescription + "</> - an error occurred [ " + ex.Message + "].\nYou should refresh the list of Map Objects then reselect the poles and try again.", "Set poles as " + strStatusDescription, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, DefaultBoolean.True);
                        return;
                    }
                }
            }
            view.EndUpdate();

            // Refresh Survey Edit screen if it is open \\
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.Load_Surveyed_Poles();
                        fParentForm.FilterGrids();
                    }
                }
            }
            selection.Clear();  // Clear MapInfo Selection //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void bbiAddWorkToWorkOrder_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            string strSelectionText = "";

            string strSelectedIDs = "";
            string strSelectedReferences = "";
            string strDeselectedIDs = "";
            string strSeselectedReferences = "";
            int intCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                        {
                            strSelectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSelectedReferences += f["ObjectReference"].ToString();
                            strSelectionText += "id='" + f["id"].ToString() + "' and ModuleID=1 or ";
                            intCount++;

                        }
                        else
                        {
                            strDeselectedIDs += f["MapID"].ToString() + ",";  // This is the Pole ID //
                            strSeselectedReferences += f["ObjectReference"].ToString();
                        }

                    }
                }
            }

            // Re-apply Selected Objects to map //
            selection.Clear();
            if (!string.IsNullOrEmpty(strSelectionText))
            {
                strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search("MapObjects", si);
                selection.Add(irfc);  // Add the whole feature set to the default selection //
            }

            if (string.IsNullOrEmpty(strSelectedIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to add their work to a <b>Work Order</b> before proceeding.", "Add Work To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            // Process each selected pole on map //
            char[] delimiters = new char[] { ',' };
            string[] strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0) return;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter GetSurveyedPoleID = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            GetSurveyedPoleID.ChangeConnectionString(strConnectionString);

            string strRecordIDs = "";
            foreach (string strElement in strArray)
            {
                try
                {
                    int intSurveyedPoleID = Convert.ToInt32(GetSurveyedPoleID.sp07371_UT_Mapping_Get_Surveyed_Pole_From_Survey_and_Pole(Convert.ToInt32(strElement), _SelectedSurveyID));
                    if (intSurveyedPoleID > 0) strRecordIDs += intSurveyedPoleID.ToString() + ",";
                }
                catch (Exception ex)
                {
                }
            }

            // We have a list of surveyed poles now so pass to screen and load their jobs... //
            frm_UT_Mapping_WorkOrder_Work frm_child = new frm_UT_Mapping_WorkOrder_Work();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._PassedInSurveyedPoleIDs = strRecordIDs;
            if (frm_child.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            string DoWhat = frm_child._DoWhat;
            string ActionIDs = frm_child._SelectedActionIDs;
            if (string.IsNullOrEmpty(ActionIDs)) return;  // This should never happen! //
            int WorkOrderID = 0;

            if (DoWhat == "CreateNew")
            {
                // Create New Work Order//
                DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter CreateWO = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                CreateWO.ChangeConnectionString(strConnectionString);

                try
                {
                    WorkOrderID = Convert.ToInt32(CreateWO.sp07470_UT_WO_Insert(DateTime.Now, GlobalSettings.UserID, 0, ActionIDs));
                    if (WorkOrderID <= 0) return;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the new Work Order - [" + ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Create Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // "AddExisting" //
            {
                // Add Selected Work to Existing Work Order //
                frm_UT_Select_Work_Order fChildForm = new frm_UT_Select_Work_Order();
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() != DialogResult.OK) return;

                WorkOrderID = fChildForm.intSelectedID;
                DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddWorkToPermissionDocument = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                AddWorkToPermissionDocument.ChangeConnectionString(strConnectionString);
                try
                {
                    AddWorkToPermissionDocument.sp07471_UT_Add_Work_To_Work_Order(WorkOrderID, ActionIDs);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while adding work to the existing Work Order - [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Add Work To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            if (WorkOrderID <= 0) return;
            // Work Order now created or existing one added to, so open it //

            frm_UT_WorkOrder_Edit fChildForm2 = new frm_UT_WorkOrder_Edit();
            fChildForm2.MdiParent = this.MdiParent;
            fChildForm2.GlobalSettings = this.GlobalSettings;
            fChildForm2.strRecordIDs = WorkOrderID + ",";
            fChildForm2.strFormMode = "edit";
            fChildForm2.strCaller = this.Name;
            fChildForm2.intRecordCount = 1;
            fChildForm2.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm2.splashScreenManager = splashScreenManager1;
            fChildForm2.splashScreenManager.ShowWaitForm();
            fChildForm2.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm2, new object[] { null });
        }

        #endregion


        #region TempFeatures

        private void Create_Temp_Features_Layer()
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t != null)
            {
                if (t.IsOpen) t.Close();
            }

            TableInfoMemTable tiTemp = new TableInfoMemTable("TempMapObjects");
            tiTemp.Temporary = true;
            tiTemp.Columns.Add(ColumnFactory.CreateIndexedStringColumn("id", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Description", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateStringColumn("Type", 50));
            tiTemp.Columns.Add(ColumnFactory.CreateDecimalColumn("decX", 18, 2));
            tiTemp.Columns.Add(ColumnFactory.CreateDecimalColumn("decY", 18, 2));

            tiTemp.Columns.Add(ColumnFactory.CreateStyleColumn());

            CoordSysFactory factory = Session.Current.CoordSysFactory;
            CoordSys BritishGrid = factory.CreateFromMapBasicString(strDefaultMappingProjection);
            //CoordSys BritishGrid = factory.CreateFromMapBasicString("CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000");
            tiTemp.Columns.Add(ColumnFactory.CreateFeatureGeometryColumn(BritishGrid));

            Table table = Session.Current.Catalog.CreateTable(tiTemp);  // Note: No need to add a column of type Key. Every table automatically contains a column named "MI_Key". //

            MapTableLoader tl = new MapTableLoader(table);
            try
            {
                tl.AutoPosition = false; // Set table loader options //
                tl.StartPosition = 0;
                mapControl1.Map.Load(tl);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load the Temporary Map Object Layer - an error occurred [ " + ex.Message + "].\nYou should Close the Mapping screen once in has finished loading then try opening the screen again.", "Load Map Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                conn.Close();
                conn.Dispose();
                return;
            }

            MapInfo.Mapping.IMapLayer l = mapControl1.Map.Layers[0];
            MapInfo.Mapping.LayerHelper.SetSelectable(l, false);
            MapInfo.Mapping.LayerHelper.SetEditable(l, true);  // Allow the map markers to be manually moved and deleted //
            MapInfo.Mapping.LayerHelper.SetInsertable(l, false);

            
            FeatureLayer fl = (FeatureLayer)mapControl1.Map.Layers["TempMapObjects"];           
            MapInfo.Tools.MapTool.SetInfoTipExpression(mapControl1.Tools.MapToolProperties, fl, "Description");

            conn.Close();
            conn.Dispose();
        }

        private void Clear_Search_Results()
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            // Remove any Prior Search First //
            MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere("Type = 'Search'");
            MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
            try
            {
                foreach (Feature f in irfc) // Should  probably be two features: Address Cross Hair and Search Circle //
                {
                    t.DeleteFeature(f);  // delete feature //
                }
                t.Pack(MapInfo.Data.PackType.All);  // pack the table //
            }
            catch (Exception)
            {
            }

            // Clear any existing Search FeatureStyleModifier //
            FeatureLayer fl = this.mapControl1.Map.Layers["TempMapObjects"] as FeatureLayer;
            Clear_FeatureStyleModifier(fl, "SearchFeatures");

            conn.Close();
            conn.Dispose();
        }

        public void Create_Gazetteer_Search_Temp_Feature(double doubX, double doubY, decimal decRadius, string strDescription, decimal decRangeAdjusted)
        {
            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
            if (t == null)
            {
                conn.Close();
                conn.Dispose();
                return;
            }
            Clear_Search_Results();  // Remove any previous search //

            // *** Create Seach Address CrossHair and Search Circle Objects and add to Map Layer *** //
            // Define Styling //
            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(0, 0, 255);
            _vectorSymbol.PointSize = (short)30;
            CompositeStyle style = new CompositeStyle();
            style.SymbolStyle = _vectorSymbol;
            ((SimpleInterior)style.AreaStyle.Interior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intGazetteerSearchCircleTransparency) / 100))), Color.FromArgb(intGazetteerSearchCircleColour));

            MapInfo.Data.Feature ftr = null;
            if (decRangeAdjusted > (decimal)0.00)  // Search Radius specified so draw it //
            {
                // Add Search Circle //
                MapInfo.Geometry.DPoint center = new MapInfo.Geometry.DPoint(Convert.ToInt32(doubX), Convert.ToInt32(doubY));
                double rad = Convert.ToDouble(decRangeAdjusted);
                MapInfo.Geometry.Ellipse circle = new MapInfo.Geometry.Ellipse(this.mapControl1.Map.GetDisplayCoordSys(), center, rad, rad, MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical);
                ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
                ftr["Description"] = "Search Radius: " + decRadius.ToString() + (decRadius != (decimal)1 ? " metres" : " metre");
                ftr["Type"] = "Search";
                ftr.Geometry = circle;
                ftr.Style = style;
                t.InsertFeature(ftr);
            }
            // Add House at centre of Search Circle //
            MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToInt32(doubX), Convert.ToInt32(doubY));
            ftr = new MapInfo.Data.Feature(t.TableInfo.Columns);
            ftr["Description"] = strDescription;
            ftr["Type"] = "Search";
            ftr.Geometry = g;
            ftr.Style = style;
            t.InsertFeature(ftr);

           // Clear any existing Search FeatureStyleModifier then re-create so search polygon area has a transparency applied to it //
            FeatureLayer fl = this.mapControl1.Map.Layers["TempMapObjects"] as FeatureLayer;
            Clear_FeatureStyleModifier(fl, "SearchFeatures");
           
            MapInfo.Data.MICommand command = conn.CreateCommand();
            command.CommandText = "Select * from TempMapObjects where Type = 'Search'";
            command.Prepare();
            MapInfo.Data.IResultSetFeatureCollection irfc = command.ExecuteFeatureCollection();

            SelectedAreaModifier sam = new SelectedAreaModifier("SearchFeatures", "SearchFeatures", irfc, intGazetteerSearchCircleColour, intGazetteerSearchCircleTransparency);  // This is declared in a new class at the end of all the code //
            fl.Modifiers.Append(sam);

            conn.Close();
            conn.Dispose();
        }

        private void Clear_FeatureStyleModifier(FeatureLayer fl, string StyleModifierName)
        {
            foreach (FeatureStyleModifier fsm in fl.Modifiers)
            {
                if (fsm.Name == StyleModifierName)
                {
                    fl.Modifiers.Remove(fsm);
                    break;
                }
            }
        }

        #endregion


        #region StoredMapViews

        private void StoreView()
        {
            Mapping_View mapView = new Mapping_View();
            mapView.DViewScale = mapControl1.Map.Scale;
            mapView.DMapCentreX = mapControl1.Map.Center.x;
            mapView.DMapCentreY = mapControl1.Map.Center.y;
            DateTime dt = DateTime.Now;
            mapView.DtDateTime = dt;
            mapView.StrViewDescription = "Scale 1:" + mapControl1.Map.Scale.ToString("#0") + "  |  X,Y: " + mapControl1.Map.Center.x.ToString("#0") + "," + mapControl1.Map.Center.y.ToString("#0") + "  |  Time: " + dt.ToString("HH:MM:ss");

            if (intCurrentViewID > 0)  // Strip out any views after the currently loaded view before the new view is added (this is how IE works when navigating web pages. //
            {
                if (intCurrentViewID == 1)
                {

                    listStoredMapView.RemoveAt(0);  // Need this otherwise crashes in loop below when intCurrentViewID = 1 //
                }
                else
                {
                    for (int i = intCurrentViewID - 1; i >= 0; i--)
                    {
                        listStoredMapView.RemoveAt(i);
                    }
                }
            }
            listStoredMapView.Insert(0, mapView);
            intCurrentViewID = 0;

            bbiLastMapView.Enabled = (intCurrentViewID == listStoredMapView.Count - 1 ? false : true);
            bbiNextMapView.Enabled = false;
        }

        private void bciStoreViewChanges_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            boolStoreViewChanges = bciStoreViewChanges.Checked;
        }

        private void bbiStoreCurrentMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            StoreView();
        }

        private void bbiClearAllMapViews_ItemClick(object sender, ItemClickEventArgs e)
        {
            Clear_Stored_Map_Views();
        }

        private void bbiStoredViewItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarCheckItem bbiItem = (BarCheckItem)e.Item;
            int intPosition = Convert.ToInt32(bbiItem.Tag);
            if (intPosition > listStoredMapView.Count)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load map view - the view is no longer valid.", "Load Map view", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            boolStoreViewChangesTempDisable = true;  // Prevent the view chnage being stored as a new view change as we are moving to an already stored view. //

            intCurrentViewID = intPosition;
            Load_Stored_Map_View();
        }

        private void bbiLastMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (intCurrentViewID >= listStoredMapView.Count - 1) return;
            intCurrentViewID++;
            Load_Stored_Map_View();
        }

        private void bbiNextMapView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (intCurrentViewID <= 0) return;
            intCurrentViewID--;
            Load_Stored_Map_View();
        }

        private void popupStoredMapViews_Popup(object sender, EventArgs e)
        {
            bsiGoToMapView.ClearLinks();
            BarCheckItem bciStoredViewItem;
            for (int i = 0; i < listStoredMapView.Count; i++)
            {
                Mapping_View mapView = (Mapping_View)listStoredMapView[i];
                bciStoredViewItem = new BarCheckItem();
                bciStoredViewItem.Tag = Convert.ToInt32(i);
                bciStoredViewItem.Caption = mapView.StrViewDescription.ToString();
                bciStoredViewItem.Name = "iStoredViewItem";
                bsiGoToMapView.AddItem(bciStoredViewItem);
                bciStoredViewItem.ItemClick += new ItemClickEventHandler(bbiStoredViewItem_ItemClick);
                if (i == intCurrentViewID) bciStoredViewItem.Checked = true;
            }

        }

        private void Load_Stored_Map_View()
        {
            boolStoreViewChangesTempDisable = true;  // Prevent the view chnage being stored as a new view change as we are moving to an already stored view. //
            Mapping_View mapView = (Mapping_View)listStoredMapView[intCurrentViewID];

            bbiLastMapView.Enabled = (intCurrentViewID == listStoredMapView.Count - 1 ? false : true);
            bbiNextMapView.Enabled = (intCurrentViewID <= 0 ? false : true);

            MapInfo.Geometry.DPoint newPoint = new DPoint(mapView.DMapCentreX, mapView.DMapCentreY);
            mapControl1.Map.SetView(newPoint, mapControl1.Map.GetDisplayCoordSys(), mapView.DViewScale);

        }

        private void Clear_Stored_Map_Views()
        {
            listStoredMapView.Clear();
            intCurrentViewID = 0;
            bbiLastMapView.Enabled = false;
            bbiNextMapView.Enabled = false;
            StoreView();  // Store current Map View //
        }
        
        #endregion


        private void CustomQueryInfo(MapInfo.Tools.ToolUsedEventArgs e)
        {
            if (boolQueryToolInUse)
            {
                boolQueryToolInUse = false;
                return;
            }
            else
            {
                boolQueryToolInUse = true;
                WaitDialogForm loading = new WaitDialogForm("Checking for Object Properties...", "WoodPlan Mapping");
                loading.Show();

                MapInfo.Data.MIConnection connection = new MapInfo.Data.MIConnection();
                connection.Open();

                // This is the point at which the user clicked on the map //
                MapInfo.Geometry.Point pt = new MapInfo.Geometry.Point(this.mapControl1.Map.GetDisplayCoordSys(), e.MapCoordinate.x, e.MapCoordinate.y);

                bool boolObjectFound = false;
                VGridControl vg = new VGridControl();

                for (int i = 0; i < this.mapControl1.Map.Layers.Count; i++)
                {
                    if (this.mapControl1.Map.Layers[i].Type != LayerType.Normal) continue;  // Only process vector [Normal type] Layers //
                    FeatureLayer fl = this.mapControl1.Map.Layers[i] as FeatureLayer;
                    if (!MapInfo.Mapping.LayerHelper.GetSelectable(fl)) continue;  // Only process selectable layers //

                    if ((fl.VisibleRangeEnabled == true & fl.VisibleRange.Within(this.mapControl1.Map.Zoom)) || (fl.VisibleRangeEnabled == false))  // if the layer is visible within the current map //             
                    {
                        MapInfo.Data.MICommand command = connection.CreateCommand();
                        command.CommandText = "Select * from " + fl.Table.Alias + " where @pt intersects Obj";

                        MapInfo.Data.GeometryColumn geoCol = null;
                        for (int j = 0; j < fl.Table.TableInfo.Columns.Count; j++)
                        {
                            Column col = fl.Table.TableInfo.Columns[j];
                            if (col.DataType == MIDbType.FeatureGeometry)
                            {
                                geoCol = (GeometryColumn)col;
                                break;
                            }
                        }
                        if (geoCol == null) continue;   // Unable to get the geometry column so abort processing this layer //

                        if (geoCol.PredominantGeometryType.ToString() == "MultiCurve" || geoCol.PredominantGeometryType.ToString() == "Curve")
                        {
                            //Checks to see if the table is a table of MultiCurves or Curves. We will use a buffer to search for these features
                            command.Parameters.Add("@pt", pt.Buffer(this.mapControl1.Map.Zoom.Value / 500, MapInfo.Geometry.DistanceUnit.Mile, 99));
                        }
                        else if (geoCol.PredominantGeometryType.ToString() == "Point" || geoCol.PredominantGeometryType.ToString() == "MultiPoint")
                        {
                            //Checks to see if the table is a tabl of Points or MultiPoints. We will use a buffer for these features as well.
                            command.Parameters.Add("@pt", pt.Buffer(this.mapControl1.Map.Zoom.Value / 200, MapInfo.Geometry.DistanceUnit.Mile, 99));
                        }
                        else
                        {
                            command.Parameters.Add("@pt", pt);
                        }

                        MapInfo.Data.IResultSetFeatureCollection irfc = command.ExecuteFeatureCollection();
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            boolObjectFound = true;
                            CategoryRow cRow = new CategoryRow("Layer: " + fl.Alias);  // Create VerticalGrid Category Row //
                            foreach (Column col in f.Columns)
                            {
                                EditorRow eRow = new EditorRow();
                                eRow.Properties.Caption = col.ToString();
                                eRow.Properties.Value = f[col.ToString()].ToString();
                                eRow.Properties.ReadOnly = true;
                                eRow.Properties.UnboundType = DevExpress.Data.UnboundColumnType.String;
                                cRow.ChildRows.Add(eRow);
                            }
                            vg.Rows.Add(cRow);
                        }
                        command.Dispose();
                        irfc.Close();
                    }
                }
                connection.Close();
                loading.Close();
                if (boolObjectFound)
                {
                    frm_AT_Mapping_Generic_Query_Tool frm_query = new frm_AT_Mapping_Generic_Query_Tool();
                    frm_query.FormID = 20049;
                    frm_query.GlobalSettings = this.GlobalSettings;
                    frm_query.FormPermissions = this.FormPermissions;
                    //frm_query.fProgress = fProgress;
                    frm_query.vg = vg;
                    frm_query.ShowDialog();
                }
            }
        }

        public void RefreshMapObjects(string strMapIDs, int IDType)
        {
            // Check if an outstanding call from a child Tree \ Inspection \ Action Edit screen has notified the form for update on this form activating. //
            // If yes, clear the flag if no new records need to be highlighted as the rest of the event will take care of refreshing the map. //
            if (UpdateRefreshStatus > 0 && i_str_AddedRecordIDs1 == "" && i_str_AddedRecordIDs2 == "" && i_str_AddedRecordIDs3 == "") UpdateRefreshStatus = 0;

            // Get Updated Pole Objects //
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp07052_UT_Mapping_Load_Updated_Objects", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@MapObjectIDs", strMapIDs));
            cmd.Parameters.Add(new SqlParameter("@IDType", IDType));
            cmd.Parameters.Add(new SqlParameter("@SurveyID", _SelectedSurveyID));
            cmd.Parameters.Add(new SqlParameter("@ThematicValueTypeID", _PoleThematicTypeID));
            SqlDataAdapter sdaUpdatedMapObjects = new SqlDataAdapter();
            DataSet dsUpdatedMapObjects = new DataSet("NewDataSet");
            dsUpdatedMapObjects.Clear();  // Remove any old values first //
            sdaUpdatedMapObjects = new SqlDataAdapter(cmd);

            // Ensure columns are of correct datatype as they have Nulls in the StoredProc so VS will map them to Int32 //
            //sdaUpdatedMapObjects.FillSchema(dsUpdatedMapObjects, SchemaType.Mapped);
            //dsUpdatedMapObjects.Tables[0].Columns["LastInspectionDate"].DataType = System.Type.GetType("System.DateTime");
            //dsUpdatedMapObjects.Tables[0].Columns["NextInspectionDate"].DataType = System.Type.GetType("System.DateTime");

            sdaUpdatedMapObjects.Fill(dsUpdatedMapObjects, "Table");
             
            // Get Updated Tree Objects //
            cmd = null;
            cmd = new SqlCommand("sp07055_UT_Mapping_Load_Updated_Tree_Objects", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@MapObjectIDs", strMapIDs));
            cmd.Parameters.Add(new SqlParameter("@IDType", IDType));
            cmd.Parameters.Add(new SqlParameter("@SurveyID", _SelectedSurveyID));
            cmd.Parameters.Add(new SqlParameter("@ThematicValueTypeID", _TreeThematicTypeID));
            SqlDataAdapter sdaUpdatedMapObjects2 = new SqlDataAdapter();
            DataSet dsUpdatedMapObjects2 = new DataSet("NewDataSet");
            dsUpdatedMapObjects2.Clear();  // Remove any old values first //
            sdaUpdatedMapObjects2 = new SqlDataAdapter(cmd);
            sdaUpdatedMapObjects2.Fill(dsUpdatedMapObjects2, "Table");

            if (dsUpdatedMapObjects.Tables[0].Rows.Count <= 0 && dsUpdatedMapObjects2.Tables[0].Rows.Count <= 0 && strMapIDs.Length <= 0) return;  // Abort if no rows and Passed IDs is blank - Note: no rows if a map delete has occurred (but the passed IDs should have the map IDs of the deleted objects) otherwise abort process. //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
            conn.Open();
            MapInfo.Data.Table t = conn.Catalog.GetTable("MapObjects");
            mapControl1.SuspendLayout();  // Switch of Map Redraw until done //

            string[] strArray;
            string strID = "";
            char[] delimiters; // Value set to correct value later //
            int intDeletedObjectCount = 0;
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            GridView view2 = (GridView)gridControl6.MainView;

            // Clear Selection //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            selection.Clear();
            string strSelectionText = "";
            string strThematicsGridViewColumn = "";
            CompositeStyle style = new CompositeStyle();
            if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
            {
                strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
            }
            else // No Thematics so get default style //
            {
                //style = Get_Default_Style();
                strThematicsGridViewColumn = "VoltageID";
            }

            if (dsUpdatedMapObjects.Tables[0].Rows.Count > 0 || dsUpdatedMapObjects2.Tables[0].Rows.Count > 0)
            {
                // Update Changed Tree Objects //
                if (dsUpdatedMapObjects2.Tables[0].Rows.Count > 0)  // Map Objects need updating on the map so delete them first then re-add them with appropriate styling //
                {
                    view2.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                    view2.ClearSelection();  // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //

                    /*
                    // Prepare Thematic Styling //
                    int intPicklistValue = 0;
                    string strThematicsGridViewColumn = "";
                    CompositeStyle style = new CompositeStyle();
                    if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)
                    {
                        strThematicsGridViewColumn = Get_ThematicField_ColumnName(intThematicStylingBasePicklistID);
                    }
                    else // No Thematics so get default style //
                    {
                        style = Get_Default_Style();
                    }
                    // End of Prepare Thematics //
                    */
                    style = Get_Default_Style();

                    delimiters = new char[] { ';' };
                    int intAssetID = 0;
                    string strPolygonXY = "";
                    int intCrownDiameter = 0;
                    int iPoint = 0;
                    bool boolHighlighted = false;
                    Color colorHighlight = (System.Drawing.Color)colorEditHighlight.EditValue;
                    int intPicklistValue = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, strThematicsGridViewColumn));
 
                    foreach (DataRow dr in dsUpdatedMapObjects2.Tables[0].Rows)
                    {
                        intAssetID = Convert.ToInt32(dr["TreeID"]);  // Get ID of record for current row //
                        // Search Map Objects grid to check if ID is present. If yes, remove it (it will be added later in this process) //
                        DevExpress.XtraGrid.Columns.GridColumn col = view2.Columns["TreeID"];
                        intRowHandle = view2.LocateByValue(0, col, intAssetID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            boolHighlighted = (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1 ? true : false);
                            strSelectionText += "id='" + Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID")) + "' and ModuleID=2 or ";
                            view2.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where MapID = " + intAssetID.ToString() + " and ModuleID=2";
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                        dataSet_UT_Mapping.sp07053_UT_Tree_Picker_Trees.Rows.Add(dr.ItemArray);  // Add the new row into the DataSet so it is shown in the grid //               

                        // Get the new row in the grid and set it's displayed checkbox to 1 //
                        intRowHandle = view2.LocateByValue(0, col, intAssetID);
                        if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                        view2.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);

                        view2.SelectRow(intRowHandle);
                        if (boolHighlighted) view2.SetRowCellValue(intRowHandle, "Highlight", 1);  // Set highlighted ticked if required //
                        view2.MakeRowVisible(intRowHandle, false);

                        // Update Map //
                        //if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        //{
                        //    style = Get_Thematic_Style(intPicklistValue, Convert.ToDecimal(view2.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        //}

                        if (view2.GetRowCellValue(intRowHandle, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "CrownDiameter"));
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(view2.GetRowCellValue(intRowHandle, "X")), Convert.ToDouble(view2.GetRowCellValue(intRowHandle, "Y")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                            f["MapID"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "TreeID"));
                            f["ObjectReference"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReferenceNumber"));
                            f["RegionName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "RegionName"));
                            f["SubAreaName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SubAreaName"));
                            f["VoltageType"] = 0;
                            f["CrownDiameter"] = 0;
                            f["intHighlighted"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight"));
                            f["LastInspectionDate"] = Convert.ToDateTime(view2.GetRowCellValue(intRowHandle, "LastInspectionDate"));
                            f["ModuleID"] = 2;  // 1 = Poles, 2 = Trees //
                            f["CrownDiameter"] = 0;
                            f["SurveyStatus"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SurveyStatus"));
                            f["SurveyDone"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "SurveyDone"));
                            f["Thematics"] = Get_Thematic_Value(f);
                            f["ThematicValue"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "ThematicValue"));

                            // Following populated so object labelling will work //
                            f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReferenceNumber"));
                            f["Tooltip"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "Tooltip"));
                           
                            if (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                            if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR TREES //
                            {
                                style = Get_Thematic_Style_Tree(2, Convert.ToInt32(f["ThematicValue"]));
                            }
                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                            strPolygonXY = Convert.ToString(view2.GetRowCellValue(intRowHandle, "XYPairs"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    view2.SetRowCellValue(intRowHandle, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    view2.SetRowCellValue(intRowHandle, "ObjectType", "Polyline");
                                }
                                f["id"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "MapID"));
                                f["MapID"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "TreeID"));
                                f["ObjectReference"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReferenceNumber"));
                                f["RegionName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "RegionName"));
                                f["SubAreaName"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SubAreaName"));
                                f["VoltageType"] = 0;
                                f["CrownDiameter"] = 0;
                                f["intHighlighted"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight"));
                                f["LastInspectionDate"] = Convert.ToDateTime(view2.GetRowCellValue(intRowHandle, "RiskFactor"));
                                f["ModuleID"] = 2;  // 1 = Poles, 2 = Trees //
                                f["CrownDiameter"] = 0;
                                f["SurveyStatus"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "SurveyStatus"));
                                f["SurveyDone"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "SurveyDone"));
                                f["Thematics"] = Get_Thematic_Value(f);
                                f["ThematicValue"] = Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "ThematicValue"));

                                // Following populated so object labelling will work //
                                f["ShortTreeRef"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "TreeReferenceNumber"));
                                f["Tooltip"] = Convert.ToString(view2.GetRowCellValue(intRowHandle, "Tooltip"));

                                if (Convert.ToInt32(view2.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                                
                                if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR TREES //
                                {
                                    style = Get_Thematic_Style_Tree(2, Convert.ToInt32(f["ThematicValue"]));
                                }
                                f.Style = style;
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                    }
                    view2.EndSelection();
                    view2.Invalidate();
                }

                // Update Changed Pole Objects //
                if (dsUpdatedMapObjects.Tables[0].Rows.Count > 0)  // Map Objects need updating on the map so delete them first then re-add them with appropriate styling //
                {
                    view.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                    view.ClearSelection();  // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //

                    // Prepare Thematic Styling //
                    Clear_MapInfo_Themes();  // Clear Any MapInfo Thematics already defined - these will be created later if required //
                    int intPicklistValue = 0;
                    style = Get_Default_Style_Pole();
                    // End of Prepare Thematics //

                    delimiters = new char[] { ';' };
                    int intTreeID = 0;
                    string strPolygonXY = "";
                    int intCrownDiameter = 0;
                    int iPoint = 0;
                    bool boolHighlighted = false;
                    Color colorHighlight = (System.Drawing.Color)colorEditHighlight.EditValue;
                    string strCurrentValue = "";
                    foreach (DataRow dr in dsUpdatedMapObjects.Tables[0].Rows)
                    {
                        intTreeID = Convert.ToInt32(dr["PoleID"]);  // Get ID of record for current row //
                        // Search Map Objects grid to check if ID is present. If yes, remove it (it will be added later in this process) //
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];
                        intRowHandle = view.LocateByValue(0, col, intTreeID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            boolHighlighted = (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1 ? true : false);
                            strSelectionText += "id='" + Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID")) + "' and ModuleID=1 or ";
                            view.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where MapID = " + intTreeID.ToString() + " and ModuleID=1";
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                        dataSet_UT_Mapping.sp07049_UT_Tree_Picker_Poles.Rows.Add(dr.ItemArray);  // Add the new row into the DataSet so it is shown in the grid //               

                        // Get the new row in the grid and set it's displayed checkbox to 1 //
                        intRowHandle = view.LocateByValue(0, col, intTreeID);
                        if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);

                        view.SelectRow(intRowHandle);

                        if (boolHighlighted) view.SetRowCellValue(intRowHandle, "Highlight", 1);  // Set highlighted ticked if required //

                        view.MakeRowVisible(intRowHandle, false);

                        // Update Map //
                        //if (intUseThematicStyling == 1 && intThematicStylingBasePicklistID != 0)  // Get Thematic Style
                        //{
                        //     if (intMapObjectTransparency <= 0) style = Get_Thematic_Style_Pole(intPicklistValue, Convert.ToDecimal(view.GetRowCellValue(intRowHandle, strThematicsGridViewColumn)));  // Pass value as both int and decimal and let function sort it out //
                        //}

                        if (view.GetRowCellValue(intRowHandle, "ObjectType").ToString() == "Point")  // Load Point //
                        {
                            intCrownDiameter = 0;
                            MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                            f.Geometry = new MapInfo.Geometry.Point(mapControl1.Map.GetDisplayCoordSys(), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "X")), Convert.ToDouble(view.GetRowCellValue(intRowHandle, "Y")));
                            f["CalculatedSize"] = "";
                            f["CalculatedPerimeter"] = "";
                            f["intObjectType"] = 0;  //0 = Point //
                            f["id"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                            f["MapID"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "PoleID"));
                            f["ObjectReference"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                            f["RegionName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "RegionName"));
                            f["SubAreaName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SubAreaName"));
                            f["VoltageType"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "VoltageType"));
                            f["CrownDiameter"] = 0;
                            f["intHighlighted"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight"));
                            f["LastInspectionDate"] = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "LastInspectionDate"));
                            f["ModuleID"] = 1;  // 1 = Poles, 2 = Trees //
                            f["SurveyStatus"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyStatus"));
                            f["SurveyDone"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyDone"));
                            f["Thematics"] = Get_Thematic_Value(f);
                            f["ThematicValue"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ThematicValue"));
                            switch (intTreeRefStructureType)
                            {
                                case 0:
                                    f["ShortTreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                                    break;
                                case 1:
                                    strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                                    f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                    break;
                                case 2:
                                    strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));  // Note that the second commented out method also works //
                                    f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                    break;
                                default:
                                    break;
                            }
                            f["Tooltip"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "Tooltip"));
                            if (intMapObjectSizing == -1)  // Dynamic Sizing according to Crown Diameter //
                            {
                                style.SymbolStyle.PointSize = Calculate_Point_Size(f);
                            }
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                            
                            if (_SelectedSurveyID > 0)  // MAP IS IN SURVEY MODE SO OVERRIDE STYLE COLOURS FOR POLES //
                            {
                                style.SymbolStyle.Color = Get_Pole_Colour(Convert.ToInt32(f["ThematicValue"]));
                            }

                            f.Style = style;
                            MapInfo.Data.Key k = t.InsertFeature(f);
                        }
                        else  // Polygon / Polyline //
                        {
                            strID = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                            strPolygonXY = Convert.ToString(view.GetRowCellValue(intRowHandle, "XYPairs"));

                            // Check there is an even number of points //
                            strArray = strPolygonXY.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length % 2 != 0)  // Modulus (%) //
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Invalid Polygon\\Polyline in database - MapID: " + strID + ".\r\nInvalid Number of Coordinates - Object ignored!", "Load Map Data", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            }
                            else
                            {
                                MapInfo.Geometry.DPoint[] dPoints = new MapInfo.Geometry.DPoint[strArray.Length / 2];
                                iPoint = 0;
                                for (int j = 0; j < strArray.Length - 1; j++)
                                {
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(strArray[j]), Convert.ToDouble(strArray[j + 1]));
                                    j++;  // Jump ahead so that we are always processing odd numbered array items //
                                    iPoint++;
                                }
                                MapInfo.Data.Feature f = new MapInfo.Data.Feature(t.TableInfo.Columns);
                                if (dPoints[0] == dPoints[dPoints.Length - 1]) // Polygon as the first and last coordinates match //
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                                    f["intObjectType"] = 1;  // 1 = Polygon //
                                    view.SetRowCellValue(intRowHandle, "ObjectType", "Polygon");
                                }
                                else
                                {
                                    MapInfo.Geometry.FeatureGeometry g = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);
                                    f.Geometry = g;
                                    f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)f.Geometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                                    f["intObjectType"] = 2;  // 2 = PolyLine //
                                    view.SetRowCellValue(intRowHandle, "ObjectType", "Polyline");
                                }
                                f["id"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "MapID"));
                                f["MapID"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "PoleID"));
                                f["ObjectReference"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                                f["RegionName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "RegionName"));
                                f["SubAreaName"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SubAreaName"));
                                f["VoltageType"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "VoltageType"));
                                f["CrownDiameter"] = 0;
                                f["intHighlighted"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight"));
                                f["LastInspectionDate"] = Convert.ToDateTime(view.GetRowCellValue(intRowHandle, "RiskFactor"));
                                f["ModuleID"] = 1;  // 1 = Amenity Trees, 2 = Assets //
                                f["SurveyStatus"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyStatus"));
                                f["SurveyDone"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SurveyDone"));
                                f["Thematics"] = Get_Thematic_Value(f);
                                f["ThematicValue"] = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ThematicValue"));
                                switch (intTreeRefStructureType)
                                {
                                    case 0:
                                        f["ShortTreeRef"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                                        break;
                                    case 1:
                                        strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));
                                        f["ShortTreeRef"] = (intTreeRefStructureStripNumber > strCurrentValue.Length ? strCurrentValue : Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeReference")).Substring(intTreeRefStructureStripNumber));
                                        break;
                                    case 2:
                                        strCurrentValue = Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleNumber"));  // Note that the second commented out method also works //
                                        f["ShortTreeRef"] = (strTreeRefStructureStripPattern.Length > strCurrentValue.Length ? strCurrentValue : strCurrentValue.Substring(strCurrentValue.IndexOf(strTreeRefStructureStripPattern) + strTreeRefStructureStripPattern.Length));
                                        break;
                                    default:
                                        break;
                                }
                                f["Tooltip"] = Convert.ToString(view.GetRowCellValue(intRowHandle, "Tooltip"));
                                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Highlight")) == 1) JustHighlight(ref style, colorHighlight);  // Pass feature by ref so it is updated with highlight colour //
                                f.Style = style;
                                MapInfo.Data.Key k = t.InsertFeature(f);
                            }
                        }
                    }
                    view.EndSelection();
                    view.Invalidate();
                }
                // Re-apply Selected Objects to map //
                if (!string.IsNullOrEmpty(strSelectionText))
                {
                    strSelectionText = strSelectionText.Remove(strSelectionText.Length - 4);  // Remove last " or " statement //
                    MapInfo.Data.SearchInfo si = MapInfo.Data.SearchInfoFactory.SearchWhere(strSelectionText);
                    MapInfo.Data.IResultSetFeatureCollection irfc = MapInfo.Engine.Session.Current.Catalog.Search(t, si);
                    selection.Add(irfc);  // Add the whole feature set to the default selection //
                }
                if (intMapObjectTransparency > 0) Create_MapInfo_Theme();  // Transparency on, so we need to create a Map Info Theme to handle thematics //
            }
            else  // No Map Objects need updating on the map (map objects have been deleted so remove them from the map // //
            {
                delimiters = new char[] { ',' };
                strArray = strMapIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i <= strArray.Length - 1; i++)
                {
                    strID = strArray[i].ToString();  // Get Map ID //
                    // Search Map Objects grid to check if ID is present. If yes, remove it //
                    DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["MapID"];
                    intRowHandle = view.LocateByValue(0, col, strID);
                    if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)  // Check Poles Object Grid First //
                    {
                        view.DeleteRow(intRowHandle);

                        // Clear object from map //
                        MICommand MapCmd = conn.CreateCommand();
                        MapCmd.CommandText = "Delete From MapObjects Where id = '" + strID + "' and ModuleID=1";
                        MapCmd.Prepare();
                        intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                        MapCmd.Dispose();
                        // End of Clear object from map //
                    }
                    else  // Object was not found so check Trees Object Grid //
                    {
                        col = view2.Columns["MapID"];
                        intRowHandle = view2.LocateByValue(0, col, strID);
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            view2.DeleteRow(intRowHandle);

                            // Clear object from map //
                            MICommand MapCmd = conn.CreateCommand();
                            MapCmd.CommandText = "Delete From MapObjects Where id = '" + strID + "' and ModuleId=2";
                            MapCmd.Prepare();
                            intDeletedObjectCount += MapCmd.ExecuteNonQuery();
                            MapCmd.Dispose();
                            // End of Clear object from map //
                        }
                    }
                }
            }
            if (intDeletedObjectCount > 0) t.Pack(MapInfo.Data.PackType.All);  // Pack the map objects table //
            conn.Close();
            conn.Dispose();

            mapControl1.Invalidate();
            mapControl1.Refresh();
            mapControl1.ResumeLayout(true);  // Switch back on Map Redrawing //
        }

        public void UpdateMapAfterChangesFromWoodplanVersion4(int intParameter)
        {
            // Get the WoodPlan V.4 passed parameter string from the Windows registry //
            string strRegistryValue = "";
            Microsoft.Win32.RegistryKey registry = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\WoodPlan");
            if (registry == null) return;
            strRegistryValue = Convert.ToString(registry.GetValue("V4toV5ParameterString"));
            registry.Close();
            if (string.IsNullOrEmpty(strRegistryValue)) return;

            switch (intParameter)
            {
                case 1:  // New point record added //
                case 2:  // New polygon record added //
                case 3:  // Point / polygon record deleted //
                    //MessageBox.Show("Message received: Add - " + strRegistryValue);
                    RefreshMapObjects(strRegistryValue, 0);
                    break;
                default:
                    break;
            }
        }

        private bool NotifyWoodPlanVersion4(string strAction, string strPassedParameterString)
        {
            // ***** NO LONGER USED ***** //                    
            // Call WoodPlan V.4 for the data entry screens //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strExePath = Convert.ToString(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesVersion4WoodPlan_Incident_Link_Exe"));
            if (string.IsNullOrEmpty(strExePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - The path to the WoodPlan V.4 Link Exe File specified in the System Settings table is missing.\n\nEnter the Folder Location of the WoodPlan V.4 Link File in the System Options screen before proceeding.", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            string strWoodPlanParameters = "";
            if (strAction.ToLower() != "workorder")  // Normal TreePicker Type Action - Adding a Tree, Hedge or Tree Group //
            {
                strExePath += "\\woodplan_incident_link.exe";
                strWoodPlanParameters = "'" + strAction + strPassedParameterString + "'";
            }
            else  // Add Action to Work Order //
            {
                strExePath += "\\woodplan_workorder_link.exe";
                strWoodPlanParameters = "'" + strPassedParameterString + "'";
            }
            if (!System.IO.File.Exists(strExePath))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - The path to the WoodPlan V.4 Link Exe File specified in the System Settings table is incorrect.\n\nCorrect the Folder Location of the WoodPlan V.4 Link File in the System Options screen before proceeding.", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            try
            {
                System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                //Proc.StartInfo.CreateNoWindow = true;  // Hide DOS Window [This line and next]//
                Proc.StartInfo.UseShellExecute = false;
                //Proc.StartInfo.RedirectStandardError = true;
                Proc.StartInfo.FileName = strExePath;
                Proc.StartInfo.Arguments = strWoodPlanParameters;

                Proc.Start();
                //Proc.WaitForExit(300000);

                //string error = Proc.StandardError.ReadToEnd();
                //Proc.WaitForExit(10000);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to run the WoodPlan V.4 Link - An error occurred [" + ex.Message + "].", "WoodPlan V.4 Link", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }

            return true;
        }

        private int AddObjectToMap_OLD(MapInfo.Geometry.FeatureGeometry PassedFeature)
        {
            // ***** NO LONGER USED ***** //
            MapInfo.Geometry.FeatureGeometry g = PassedFeature;

            CompositeStyle style = new CompositeStyle();
            style = Get_Default_Style();  // *** Get Styling *** //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();  // create connection and open table //
            conn.Open();
            MapInfo.Data.Table tab1 = conn.Catalog.GetTable("MapObjects");
            MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(tab1.TableInfo.Columns);  // make a blank feature based on the table's structure //
            ftr.Geometry = PassedFeature;  // assign the memory address of the passed geometry to the new features Geometry property //

            int intPreviousX = -99999;
            int intPreviousY = -99999;
            int intCurrentX = 0;
            int intCurrentY = 0;
            string strPassedParameterString = "";
            string strAction = "";

            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area/length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null; // Used for adjusted area / length calculation //

            switch (PassedFeature.Type)
            {
                case GeometryType.MultiPolygon:
                    MapInfo.Geometry.MultiPolygon mpoly = ftr.Geometry as MapInfo.Geometry.MultiPolygon;
                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                    {
                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                        {
                            dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                            foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                            {
                                intCurrentX = Convert.ToInt32(pt.x);
                                intCurrentY = Convert.ToInt32(pt.y);
                                if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                {
                                    strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                    intPreviousX = intCurrentX;
                                    intPreviousY = intCurrentY;
                                }
                                dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted area calculation //
                                iPoint++;  // Used for adjusted area calculation //
                            }
                        }
                        // Get Polygon Area - Need to create a new feature geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        strAction = "new gp v5;" + string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter)) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.x) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.y) + ";polygon;";

                        ftr["intObjectType"] = 1;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                        ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)ftr.Geometry;
                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                    MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                    dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                    //step through the array and print the coordinates to the debug window
                    for (int i = 0; i < dptArr.Length; i++)
                    {
                        intCurrentX = Convert.ToInt32(dptArr[i].x);
                        intCurrentY = Convert.ToInt32(dptArr[i].y);
                        if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                        {
                            strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                            intPreviousX = intCurrentX;
                            intPreviousY = intCurrentY;
                        }
                        dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(intCurrentX), Convert.ToDouble(intCurrentY));  // Used for adjusted length calculation //
                        iPoint++;  // Used for adjusted length calculation //                           
                    }
                    // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                    tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                    //strAction = "new group;";  // No Area Passed with this one //
                    strAction = "new gp v5;" + string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.x) + ";" + string.Format("{0:N2}", tempFeatureGeometry.Centroid.y) + ";polyline;";

                    ftr["intObjectType"] = 2;  // 0 = Point, 1 = Polygon, 2 = Line //
                    ftr["intHighlighted"] = 0;
                    ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                    ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    strPassedParameterString += Convert.ToInt32(ftr.Geometry.Centroid.x).ToString() + ";" + Convert.ToInt32(ftr.Geometry.Centroid.y).ToString() + ";";
                    strAction = "new;";
                    ftr["intObjectType"] = 0;  // 0 = Point, 1 = Polygon, 2 = Line //
                    ftr["intHighlighted"] = 0;
                    ftr["CalculatedSize"] = "";
                    ftr["CalculatedPerimeter"] = "";
                    break;
                default:
                    return 0;
            }

            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            bool boolSucceed = NotifyWoodPlanVersion4(strAction, strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            if (boolSucceed)
            {
                // Assign data to the feature... *** Once Data Entry screens are created, pick up the following data from them, add object into GridControl2 and get correct themtic styling if switched on. *** //             
                ftr["ObjectReference"] = "Mark Test";
                ftr["VoltageType"] = "Scots Pine";
                ftr["ID"] = "1234567890";
                ftr["CrownDiameter"] = 0;  // *** NEED TO GET THIS ONCE USER CHANGES IT in data entry screen *** //                      
                ftr["Thematics"] = Convert.ToDouble(0.00);
                ftr.Style = style;  // ***  NEED TO FIRE DYNAMIC SIZING CODE IF SWITCHED ON THEN UPDATE STYLE'S SIZE *** //

                // ***** FOLLOWING LINE NO LONGER REQUIRED AT THE ONJECT IS ADDED AFTER WOODPLAN V4 HAS FINISHED WITH IT ***** //
                //MapInfo.Data.Key ftrKey = tab1.InsertFeature(ftr);
            }
            /*
             // Folloing Code Prevents and MapLabel from being generated //
            foreach (MapInfo.Mapping.LabelLayer lbllyr in mapControl1.Map.Layers.GetMapLayerEnumerator(new MapInfo.Mapping.FilterByLayerType(MapInfo.Mapping.LayerType.Label)))
            {
                // LOOP OVER EACH LABELS SOURCE //
                foreach (MapInfo.Mapping.LabelSource lblsrc in lbllyr.Sources)
                {
                    MapInfo.Mapping.LabelSource lblSource = new MapInfo.Mapping.LabelSource(tab1, "LabelSource");
                    MapInfo.Mapping.LabelProperties lp = new MapInfo.Mapping.LabelProperties();

                    LabelProperties properties = new LabelProperties();
                    properties.Attributes = LabelAttribute.VisibilityEnabled | LabelAttribute.Caption | LabelAttribute.CalloutLineUse;
                    properties.Visibility.Enabled = false;
                    properties.Caption = "''";
                    properties.CalloutLine.Use = false;

                    // Add the properties to a new selection label modifier and add the modifier to the label source //
                    SelectionLabelModifier modifier = new SelectionLabelModifier();
                    modifier.Properties.Add(ftrKey, properties);
                    //lbllyr.Sources[0].Modifiers..Append(modifier);
                    lblsrc.Modifiers.Append(modifier);
                }
            }
            */
            conn.Close();
            conn.Dispose();
            return 1;
        }

        private int AddObjectToMap(MapInfo.Geometry.FeatureGeometry PassedFeature)
        {
            MapInfo.Geometry.FeatureGeometry g = PassedFeature;

            CompositeStyle style = new CompositeStyle();
            style = Get_Default_Style();  // *** Get Styling *** //

            MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();  // create connection and open table //
            conn.Open();
            MapInfo.Data.Table tab1 = conn.Catalog.GetTable("MapObjects");
            MapInfo.Data.Feature ftr = new MapInfo.Data.Feature(tab1.TableInfo.Columns);  // make a blank feature based on the table's structure //
            ftr.Geometry = PassedFeature;  // assign the memory address of the passed geometry to the new features Geometry property //

            //int intPreviousX = -99999;
            //int intPreviousY = -99999;
            float flCurrentX = 0;
            float flCurrentY = 0;

            int intObjectType = -1;
            float flX = 0;
            float flY = 0;
            string strPolygonXY = "";
            decimal decArea = (decimal)0.00;
            decimal decLength = (decimal)0.00;
            decimal decWidth = (decimal)0.00;

            float flPreviousX = (float)-99999;
            float flPreviousY = (float)-99999;
            float flLatLongX = (float)0.00;
            float flLatLongY = (float)0.00;
            string strLatLongPolygonXY = "";

            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area/length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null; // Used for adjusted area / length calculation //

            switch (PassedFeature.Type)
            {
                case GeometryType.MultiPolygon:
                    {
                        MapInfo.Geometry.MultiPolygon mpoly = ftr.Geometry as MapInfo.Geometry.MultiPolygon;
                        foreach (MapInfo.Geometry.Polygon poly in mpoly)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flCurrentX = (float)pt.x;
                                    flCurrentY = (float)pt.y;
                                    if (flCurrentX != flPreviousX || flCurrentY != flPreviousY)
                                    {
                                        strPolygonXY += flCurrentX.ToString() + ";" + flCurrentY.ToString() + ";";
                                        flPreviousX = flCurrentX;
                                        flPreviousY = flCurrentY;
                                    }
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(flCurrentX), Convert.ToDouble(flCurrentY));  // Used for adjusted area calculation //
                                    iPoint++;  // Used for adjusted area calculation //
                                }
                            }
                            // Get Polygon Area - Need to create a new feature geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                            tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                            intObjectType = 1;  // Woodland //
                            decArea = Convert.ToDecimal(((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter));

                            ftr["intObjectType"] = 1;  // 0 = Point, 1 = Polygon, 2 = Line //
                            ftr["intHighlighted"] = 0;
                            ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                            ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)ftr.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                        }

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polygon_Between_Projections(mpoly);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        foreach (MapInfo.Geometry.Polygon poly in (MapInfo.Geometry.MultiPolygon)OriginalLatLongFeature)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flLatLongX = (float)pt.x;
                                    flLatLongY = (float)pt.y;
                                    if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                                    {
                                        strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                        flPreviousX = flLatLongX;
                                        flPreviousY = flLatLongY;
                                    }
                                }
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    {
                        MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)ftr.Geometry;
                        MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flCurrentX = (float)dptArr[i].x;
                            flCurrentY = (float)dptArr[i].y;
                            if (flCurrentX != flPreviousX || flCurrentY != flPreviousY)
                            {
                                strPolygonXY += flCurrentX.ToString() + ";" + flCurrentY.ToString() + ";";
                                flPreviousX = flCurrentX;
                                flPreviousY = flCurrentY;
                            }
                            dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(flCurrentX), Convert.ToDouble(flCurrentY));  // Used for adjusted length calculation //
                            iPoint++;  // Used for adjusted length calculation //                           
                        }
                        // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        intObjectType = 2;  // Hedge //
                        decLength = Convert.ToDecimal(((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical));
                        decWidth = decDefaultHedgeWidth;
                        decArea = decLength * decWidth;

                        ftr["intObjectType"] = 2;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        ftr["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";


                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polyline_Between_Projections(multiCurve);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        multiCurve = (MapInfo.Geometry.MultiCurve)OriginalLatLongFeature;
                        curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flLatLongX = (float)dptArr[i].x;
                            flLatLongY = (float)dptArr[i].y;
                            if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                            {
                                strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                flPreviousX = flLatLongX;
                                flPreviousY = flLatLongY;
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    {
                        intObjectType = 0;  // Tree //
                        flX = (float)ftr.Geometry.Centroid.x;
                        flY = (float)ftr.Geometry.Centroid.y;
                        ftr["intObjectType"] = 0;  // 0 = Point, 1 = Polygon, 2 = Line //
                        ftr["intHighlighted"] = 0;
                        ftr["CalculatedSize"] = "";
                        ftr["CalculatedPerimeter"] = "";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.Point OriginalLatLongFeature = MapFunctions.Convert_Point_Between_Projections(ftr.Geometry.Centroid.x, ftr.Geometry.Centroid.y);
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                        strLatLongPolygonXY = "";
                    }
                    break;
                default:
                    return 0;
            }
            if (intObjectType == -1) return 0;  // Unknown object type so abort //

            // Get Object Type to be plotted from drop down list of object types on toolbar //
            int intPlotType = Convert.ToInt32(beiPlotObjectType.EditValue);
            if (intPlotType == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the object type to be plotted from the Plot list on the toolbar before proceeding.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }
            ftr["ModuleID"] = (intPlotType == -1 ? 1 : 2);

            if (strPolygonXY.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Add Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            Guid guid = Guid.NewGuid();
            string strMapID = guid.ToString();

            if (intPlotType == -1)  // Pole //
            {
                // Open Edit Pole screen and pass mapping details of potential new map object to it. //
                int[] intRowHandles;

                frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();

                fChildForm.boolCalledByMapping = true;
                fChildForm.flX = flX;
                fChildForm.flY = flY;
                fChildForm.MapID = strMapID;
                fChildForm.flLatitude = flLatLongX;
                fChildForm.flLongitude = flLatLongY;

                GridView view = (GridView)gridControl2.MainView;
                view.PostEditor();
                intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length == 1)
                {
                    fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID")));
                    fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                    fChildForm.intLinkedToCircuitID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "CircuitID")));
                    fChildForm.strLinkedToCircuitName = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " + (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "CircuitName").ToString());
                    fChildForm.strLinkedToCircuitNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitNumber").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "SubAreaName").ToString());
                }

                fChildForm.Show();
                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            else  // Tree //
            {
                // Open Edit Tree screen and pass mapping details of potential new map object to it. //
                int[] intRowHandles;

                frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = "";
                fChildForm.strFormMode = "add";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = 0;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();

                fChildForm.boolCalledByMapping = true;
                fChildForm.flX = flX;
                fChildForm.flY = flY;
                fChildForm.MapID = strMapID;
                fChildForm.flLatitude = flLatLongX;
                fChildForm.flLongitude = flLatLongY;
                fChildForm.strXYPairs = strPolygonXY;
                fChildForm.strLatLongPairs = strLatLongPolygonXY;
                fChildForm.decArea = decArea;
                fChildForm.decLength = decLength;
                fChildForm.intObjectType = intObjectType;
                GridView view = (GridView)gridControl2.MainView;
                view.PostEditor();

                if (_CurrentPoleID > 0)  // Current Pole is stored for adding tree against //
                {
                    int intFoundRow = 0;
                    GridColumn column = view.Columns["PoleID"];
                    intFoundRow = view.LocateByValue(0, column, _CurrentPoleID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "ClientID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intFoundRow, "ClientID")));
                        fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "ClientName").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "ClientName").ToString());
                        fChildForm.intLinkedToCircuitID = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "CircuitID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intFoundRow, "CircuitID")));
                        fChildForm.strLinkedToPoleName = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "ClientName").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "ClientName").ToString()) + " \\ " + (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "CircuitName").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "CircuitName").ToString()) + " \\ " + (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "PoleNumber").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "PoleNumber").ToString());
                        fChildForm.strLinkedToCircuitNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "CircuitNumber").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "SubAreaName").ToString());
                        fChildForm.intLinkedToPoleID = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "PoleID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intFoundRow, "PoleID")));
                        fChildForm.strLinkedToPoleNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intFoundRow, "PoleNumber").ToString()) ? "" : view.GetRowCellValue(intFoundRow, "PoleNumber").ToString());
                    }
                }
                /*intRowHandles = view.GetSelectedRows();
                if (intRowHandles.Length == 1)
                {
                    fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID")));
                    fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                    fChildForm.intLinkedToCircuitID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "RegionID")));
                    fChildForm.strLinkedToPoleName = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) + " \\ " + (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "CircuitName").ToString()) + " \\ " + (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString());
                    fChildForm.strLinkedToCircuitNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "CircuitNumber").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "SubAreaName").ToString());
                    fChildForm.intLinkedToPoleID = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PoleID").ToString()) ? 0 : Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "PoleID")));
                    fChildForm.strLinkedToPoleNumber = (string.IsNullOrEmpty(view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString()) ? "" : view.GetRowCellValue(intRowHandles[0], "PoleNumber").ToString());
                }*/
                fChildForm.intObjectType = intObjectType;  // 0 = Tree, 1 = Woodland, 2 = Hedge //
 
                fChildForm.Show();
                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

            //if (intNewTreeID > 0)   // Record added to tblTree successfully, so open it for editing
            //{
                // Assign data to the feature... *** Once Data Entry screens are created, pick up the following data from them, add object into GridControl2 and get correct themtic styling if switched on. *** //             
                ftr["ObjectReference"] = "Mark Test";
                ftr["VoltageType"] = " ";
                ftr["ID"] = "1234567890";
                ftr["CrownDiameter"] = 0;  // *** NEED TO GET THIS ONCE USER CHANGES IT in data entry screen *** //                      
                ftr["Thematics"] = Convert.ToDouble(0.00);
                ftr.Style = style;  // ***  NEED TO FIRE DYNAMIC SIZING CODE IF SWITCHED ON THEN UPDATE STYLE'S SIZE *** //

                // ***** FOLLOWING LINE NO LONGER REQUIRED AS THE OBJECT IS ADDED AFTER WOODPLAN V4 HAS FINISHED WITH IT ***** //
                //MapInfo.Data.Key ftrKey = tab1.InsertFeature(ftr);
            //}

            conn.Close();
            conn.Dispose();
            return 1;
        }


        private void FeatureAdding(object sender, FeatureAddingEventArgs e)
        {
            if (e.Feature == null) return;
            MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)e.Feature.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //

            // The Current tool won't disengage to allow the user to select a different one [due to e.Cancel = true above], so following takes care of it //
            if (e.Feature.Type == GeometryType.MultiPolygon || e.Feature.Type == GeometryType.MultiCurve)
            {
                MouseSimulator.MouseDown(MouseButton.Left);
                KeyboardSimulator.KeyPress(Keys.Escape);
            }
            e.Cancel = true;  // Cancel the MapXtreme Default Event now //
            System.Windows.Forms.SendKeys.SendWait("{ESC}");  // Simulate Escape Key Press to force Map Extreme to let go

            int intSuccess = AddObjectToMap(newGeometry);  // Add object to map //
        }


        private void FeatureAdded(object sender, FeatureAddedEventArgs e)
        {
        }

        private void FeatureSelecting(object sender, FeatureSelectingEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nFeatureSelecting:  " + (e.Cancel ? "Cancel" : e.ToString());
            //memoEdit1.Refresh();
        }

        private void FeatureSelected(object sender, FeatureSelectedEventArgs e)
        {
            /*memoEdit1.Text = memoEdit1.Text + "\r\nFeatureSelected:  " + e.ToString() + "\r\n";
            memoEdit1.Refresh();
            //DevExpress.XtraEditors.XtraMessageBox.Show("Tool: " + e.ToolName + "\nX: " + e.MapCoordinate.x.ToString() + "\nY: " + e.MapCoordinate.y.ToString() + "\nClientCoordinate: " + e.ClientCoordinate.ToString(), "Selection Fired");
            */
            Process_Selected_Map_Objects();
         }

        private void Process_Selected_Map_Objects()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            int intRowHandle = 0;
            GridView view = (GridView)gridControl2.MainView;
            GridView view2 = (GridView)gridControl6.MainView;
            DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];
            DevExpress.XtraGrid.Columns.GridColumn col2 = view2.Columns["TreeID"];
            if (bsiSyncSelection.Caption == "Sync Map Selection: On")
            {
                view.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                view2.BeginSelection();  // Prevent Grid from repainting grid while selecting records until process is complete //
                view.ClearSelection(); // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //
                view2.ClearSelection(); // Clear any selected rows (rows with blue highlighter) - Updated rows will be selected below //
            }

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();

            i_intMapObjectsSelected = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    string strSelectedIDs = "";
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        //for (int i = 0; i < f.Columns.Count; i++)
                        //{
                        //    System.Diagnostics.Debug.WriteLine(f[i].ToString());
                        //}
                        //memoEdit1.Text = memoEdit1.Text + "TreeID Selected:  " + f[0].ToString() + "\r\n";
                        i_intMapObjectsSelected++;

                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole Object //
                        {
                            if (bsiSyncSelection.Caption == "Sync Map Selection: On")
                            {
                                strSelectedIDs += f["MapID"].ToString() + ",";
                                if (bsiSyncSelection.Caption == "Sync Map Selection: On")
                                {
                                    intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                                    if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                                    view.SelectRow(intRowHandle);
                                    view.MakeRowVisible(intRowHandle, false);
                                }
                            }
                            textEditPlotTreesAgainstPole.Text = f["ObjectReference"].ToString();
                            _CurrentPoleID = Convert.ToInt32(f["MapID"]);
                        }
                        else  // Tree Object //
                        {
                            if (bsiSyncSelection.Caption == "Sync Map Selection: On")
                            {
                                intRowHandle = view2.LocateByValue(0, col2, Convert.ToInt32(f["MapID"]));
                                if (intRowHandle == DevExpress.XtraGrid.GridControl.InvalidRowHandle) return;
                                view2.SelectRow(intRowHandle);
                                view2.MakeRowVisible(intRowHandle, false);
                            }
                        }
                    }
                }
            }
            if (bsiSyncSelection.Caption == "Sync Map Selection: On")
            {
                view.EndSelection();
                view2.EndSelection();
                view.Invalidate();
                view2.Invalidate();
            }
        }

        private void FeatureChanging(object sender, FeatureChangingEventArgs e)
        {
            if (e.FeatureChangeMode == FeatureChangeMode.Delete)
            {
                // Get all selected items and iterate through them //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                int intDeletedCount = 0;

                MapInfo.Data.MIConnection conn = new MapInfo.Data.MIConnection();
                conn.Open();
                MapInfo.Data.Table t = conn.Catalog.GetTable("TempMapObjects");
                try
                {
                    while (enum1.MoveNext())
                    {
                        MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                        if (irfc.BaseTable.Alias == "TempMapObjects")  // Ignore any selectable layers except TempMapObjects //
                        {
                            foreach (MapInfo.Data.Feature f in irfc)
                            {
                                intDeletedCount++;
                                t.DeleteFeature(f);  // delete feature //
                            }
                        }
                    }
                    if (intDeletedCount > 0)
                    {
                        t.Pack(MapInfo.Data.PackType.All);  // pack the table //
                        conn.Close();
                        conn.Dispose();
                        e.Cancel = true;  // Now cancel out so that if any Map Objects [not temp ones] are selected, they are not deleted //
                    }
                }
                catch
                {
                    conn.Close();
                    conn.Dispose();
                }

                if (intDeletedCount <= 0)  // Nothing deleted, so only regular map objects selected which need to be deleted via the menu //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("To Delete map objects, select Delete from the Map Menu.", "Delete Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    e.Cancel = true;
                }
            }
        }

        private void FeatureChanged(object sender, MapInfo.Tools.FeatureChangedEventArgs e)
        {
            if (e.ToolStatus == ToolStatus.End)
            {
                //memoEdit1.Text = memoEdit1.Text + "\r\nFeatureChanged:  " + e.ToString() + "  FeatureChangeMode = " + e.FeatureChangeMode.ToString();
                //memoEdit1.Refresh();
                //UpdateObjCount();

                // Get all selected items and iterate through them then update the back-end //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            UpdateMapObjectPosition(f);
                        }
                    }
                    else if (irfc.BaseTable.Alias == "TempMapObjects")
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            if (f["Type"].ToString() == "MapMarker")
                            {
                                MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)f.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
                                int intCurrentX = 0;
                                int intCurrentY = 0;
                                intCurrentX = Convert.ToInt32(newGeometry.Centroid.x);
                                intCurrentY = Convert.ToInt32(newGeometry.Centroid.y);
                                f["decX"] = Convert.ToDecimal(intCurrentX);
                                f["decY"] = Convert.ToDecimal(intCurrentY);
                                f["Description"] = "Map Marker - X,Y: " + newGeometry.Centroid.x.ToString("#0") + "," + newGeometry.Centroid.y.ToString("#0");
                                f.Update(true);
                            }
                        }
                    }
                }
            }
        }

        private void Tools_NodeChanging(object sender, MapInfo.Tools.NodeChangingEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nNodeChanging:  " + e.NodeChangeMode.ToString();
            //memoEdit1.Refresh();
            /*
            if (e.NodeChangeMode == NodeChangeMode.Add)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to add a new node to the current map object?", "Add Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            else if (e.NodeChangeMode == NodeChangeMode.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to remove the currently selected new node from the current map object?", "Remove Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            else if (e.NodeChangeMode == NodeChangeMode.Move)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to move the currently selected node in the current map object?", "Move Polgon \\ Polyline Node", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No) e.Cancel = true;
            }
            */

            if (e.NodeChangeMode == NodeChangeMode.Delete)
            {
                // If Deleting check enough points will be left in the object and cancel the delete if the object will be deleted as a result of too few points left in it //
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            switch (f.Geometry.Type)
                            {
                                case GeometryType.MultiPolygon:
                                    MapInfo.Geometry.MultiPolygon mpoly = f.Geometry as MapInfo.Geometry.MultiPolygon;
                                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                                    {
                                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                                        {
                                            if (curve.SamplePoints().Length <= 4)
                                            {
                                                e.Cancel = true;
                                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete node - too few nodes would be left to complete the object.", "Delete Polgon Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            }
                                        }
                                    }
                                    break;
                                case GeometryType.MultiCurve:  // PolyLine //
                                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)f.Geometry;
                                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                                    if (curves.SamplePoints().Length <= 2)
                                    {
                                        e.Cancel = true;
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to delete node - too few nodes would be left to complete the object.", "Delete Polyline Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            else if (e.NodeChangeMode == NodeChangeMode.Add)
            {
                // If Adding check total length of points will not exceed 255 character limit of GBM Mobile //
                int intPreviousX = -99999;
                int intPreviousY = -99999;
                int intCurrentX = 0;
                int intCurrentY = 0;
                string strPassedParameterString = "";
                MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
                System.Collections.IEnumerator enum1 = selection.GetEnumerator();
                while (enum1.MoveNext())
                {
                    MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                    if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                    {
                        foreach (MapInfo.Data.Feature f in irfc)
                        {
                            switch (f.Geometry.Type)
                            {
                                case GeometryType.MultiPolygon:
                                    MapInfo.Geometry.MultiPolygon mpoly = f.Geometry as MapInfo.Geometry.MultiPolygon;
                                    foreach (MapInfo.Geometry.Polygon poly in mpoly)
                                    {
                                        foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                                        {
                                            foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                            {
                                                intCurrentX = Convert.ToInt32(pt.x);
                                                intCurrentY = Convert.ToInt32(pt.y);
                                                if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                                {
                                                    strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                                    intPreviousX = intCurrentX;
                                                    intPreviousY = intCurrentY;
                                                }
                                            }
                                            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)
                                            {
                                                e.Cancel = true;
                                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add node - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.", "Add Polgon Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            }
                                        }
                                    }
                                    break;
                                case GeometryType.MultiCurve:  // PolyLine //
                                    MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)f.Geometry;
                                    MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                                    MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //
                                    // Step through the array and get the coordinates //
                                    for (int i = 0; i < dptArr.Length; i++)
                                    {
                                        intCurrentX = Convert.ToInt32(dptArr[i].x);
                                        intCurrentY = Convert.ToInt32(dptArr[i].y);
                                        if (intCurrentX != intPreviousX || intCurrentY != intPreviousY)
                                        {
                                            strPassedParameterString += intCurrentX.ToString() + ";" + intCurrentY.ToString() + ";";
                                            intPreviousX = intCurrentX;
                                            intPreviousY = intCurrentY;
                                        }
                                    }
                                    if (strPassedParameterString.Length > intStrPolygonXYMaxLength)
                                    {
                                        e.Cancel = true;
                                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add node - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.", "Add Polyline Node", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

        }

        private void Tools_NodeChanged(object sender, MapInfo.Tools.NodeChangedEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nNodeChanged:  " + e.NodeChangeMode.ToString();
            //memoEdit1.Refresh();

            // Get all selected items and iterate through them then update the back-end//
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        UpdateMapObjectPosition(f);
                    }
                }
            }
        }

        private void UpdateMapObjectPosition(MapInfo.Data.Feature f)
        {
            if (f == null) return;
            MapInfo.Geometry.FeatureGeometry newGeometry = (MapInfo.Geometry.FeatureGeometry)f.Geometry.Copy(mapControl1.Map.GetDisplayCoordSys());  // Copy feature to new feature so the CoordSys for the feature can be set to correct projection //
            float flCurrentX = (float)0;
            float flCurrentY = (float)0;
            string strPassedParameterString = "";
            string strObjectType = "";

            float flPreviousX = (float)-99999;
            float flPreviousY = (float)-99999;
            float flLatLongX = (float)0.00;
            float flLatLongY = (float)0.00;
            string strLatLongPolygonXY = "";

            MapInfo.Geometry.FeatureGeometry tempFeatureGeometry = null;  // Used for adjusted area / length calculation //
            int iPoint = 0;  // Used for adjusted area / length calculation //
            MapInfo.Geometry.DPoint[] dPoints = null;  // Used for adjusted area / length calculation ///

            switch (f.Geometry.Type)
            {
                case GeometryType.MultiPolygon:
                    {
                        MapInfo.Geometry.MultiPolygon mpoly = newGeometry as MapInfo.Geometry.MultiPolygon;
                        foreach (MapInfo.Geometry.Polygon poly in mpoly)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length];  // Used for adjusted area / length calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flCurrentX = (float)pt.x;
                                    flCurrentY = (float)pt.y;
                                    if (flCurrentX != flPreviousX || flCurrentY != flPreviousY)
                                    {
                                        strPassedParameterString += flCurrentX.ToString() + ";" + flCurrentY.ToString() + ";";
                                        flPreviousX = flCurrentX;
                                        flPreviousY = flCurrentY;
                                    }
                                    dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(flCurrentX), Convert.ToDouble(flCurrentY));  // Used for adjusted area / length calculation //
                                    iPoint++;  // Used for adjusted area / length calculation //
                                }
                            }
                            // Get Polygon Area - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                            tempFeatureGeometry = new MapInfo.Geometry.MultiPolygon(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                            f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(i_AreaUnit_Polygon_Area_Measurement_Unit)) + " " + i_str_Polygon_Area_Measurement_Unit;
                            f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)f.Geometry).Perimeter(MapInfo.Geometry.DistanceUnit.Meter)) + " M";
                            strObjectType = "polygon";
                        }
                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polygon_Between_Projections(mpoly);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        foreach (MapInfo.Geometry.Polygon poly in (MapInfo.Geometry.MultiPolygon)OriginalLatLongFeature)
                        {
                            foreach (MapInfo.Geometry.CurveSegment curve in poly.Exterior)
                            {
                                dPoints = new MapInfo.Geometry.DPoint[curve.SamplePoints().Length]; // Used for adjusted area calculation //
                                foreach (MapInfo.Geometry.DPoint pt in curve.SamplePoints())
                                {
                                    flLatLongX = (float)pt.x;
                                    flLatLongY = (float)pt.y;
                                    if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                                    {
                                        strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                        flPreviousX = flLatLongX;
                                        flPreviousY = flLatLongY;
                                    }
                                }
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiCurve:  // PolyLine //
                    {
                        MapInfo.Geometry.MultiCurve multiCurve = (MapInfo.Geometry.MultiCurve)newGeometry;
                        MapInfo.Geometry.Curve curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        MapInfo.Geometry.DPoint[] dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //
                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length];  // Used for adjusted area / length calculation //
                        // Step through the array and get the coordinates //
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flCurrentX = (float)dptArr[i].x;
                            flCurrentY = (float)dptArr[i].y;
                            if (flCurrentX != flPreviousX || flCurrentY != flPreviousY)
                            {
                                strPassedParameterString += flCurrentX.ToString() + ";" + flCurrentY.ToString() + ";";
                                flPreviousX = flCurrentX;
                                flPreviousY = flCurrentY;
                            }
                            dPoints[iPoint] = new MapInfo.Geometry.DPoint(Convert.ToDouble(flCurrentX), Convert.ToDouble(flCurrentY));  // Used for adjusted area / length calculation //
                            iPoint++;  // Used for adjusted area / length calculation //
                        }
                        // Get Polyline Length - Need to create a new fetaure geometry with truncated points to calculate the area accuratly as this is how the object will be redrawn on saving and refreshing the map //
                        tempFeatureGeometry = new MapInfo.Geometry.MultiCurve(mapControl1.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, dPoints);

                        f["CalculatedSize"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        f["CalculatedPerimeter"] = string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)) + " M";
                        strObjectType = "polyline";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.FeatureGeometry OriginalLatLongFeature = MapFunctions.Convert_Polyline_Between_Projections(multiCurve);
                        flPreviousX = (float)-99999;
                        flPreviousY = (float)-99999;
                        multiCurve = (MapInfo.Geometry.MultiCurve)OriginalLatLongFeature;
                        curves = multiCurve[0];  // take the first line from the MultiCurve collection //
                        dptArr = curves.SamplePoints();  // place all the points from the curve into dptArr //

                        dPoints = new MapInfo.Geometry.DPoint[curves.SamplePoints().Length]; // Used for adjusted area / length calculation //
                        //step through the array and print the coordinates to the debug window
                        for (int i = 0; i < dptArr.Length; i++)
                        {
                            flLatLongX = (float)dptArr[i].x;
                            flLatLongY = (float)dptArr[i].y;
                            if (flLatLongX != flPreviousX || flLatLongY != flPreviousY)
                            {
                                strLatLongPolygonXY += flLatLongX.ToString() + ";" + flLatLongY.ToString() + ";";
                                flPreviousX = flLatLongX;
                                flPreviousY = flLatLongY;
                            }
                        }
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                    }
                    break;
                case GeometryType.MultiPoint:
                    break;
                case GeometryType.Point:
                    {
                        flCurrentX = (float)newGeometry.Centroid.x;
                        flCurrentY = (float)newGeometry.Centroid.y;
                        f["CalculatedSize"] = "";
                        f["CalculatedPerimeter"] = "";
                        strObjectType = "point";

                        // Pick up LatLong Version //
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapInfo.Geometry.Point OriginalLatLongFeature = MapFunctions.Convert_Point_Between_Projections(f.Geometry.Centroid.x, f.Geometry.Centroid.y);
                        flLatLongX = (float)OriginalLatLongFeature.Centroid.x;
                        flLatLongY = (float)OriginalLatLongFeature.Centroid.y;
                        strLatLongPolygonXY = "";
                    }
                    break;
                default:
                    return;
            }
            f.Update(true);  // Update CalculatedSize //

            if (strPassedParameterString.Length > intStrPolygonXYMaxLength)  // Trap anything longer that 255 characters otherwise GBM Mobile Export can't handle it //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to edit the object - the total length of the boundaries of the object exceed the WoodPlan limit of " + intStrPolygonXYMaxLength.ToString() + " characters.\n\nTry plotting the object again but ensure there are less vertices.", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Update Database with changes to the object //
            int intRowHandle = 0;
            DataSet_UT_MappingTableAdapters.QueriesTableAdapter UpdateBackEnd = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
            UpdateBackEnd.ChangeConnectionString(strConnectionString);
            if (strObjectType == "point")
            {
                try
                {
                    if (Convert.ToInt32(f["ModuleID"]) == 1)  // Poles //
                    {
                        GridView view = (GridView)gridControl2.MainView;
                        UpdateBackEnd.sp07050_UT_Tree_Picker_Update_X_and_Y(Convert.ToInt32(f["MapID"]), 1,flCurrentX, flCurrentY, flLatLongX, flLatLongY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["PoleID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "X")) != flCurrentX || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Y")) != flCurrentY)
                            {
                                view.SetRowCellValue(intRowHandle, "X", flCurrentX);
                                view.SetRowCellValue(intRowHandle, "Y", flCurrentY);
                            }
                        }
                    }
                    else  // Trees //
                    {
                        GridView view = (GridView)gridControl6.MainView;
                        UpdateBackEnd.sp07050_UT_Tree_Picker_Update_X_and_Y(Convert.ToInt32(f["MapID"]), 2, flCurrentX, flCurrentY, flLatLongX, flLatLongY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "X")) != flCurrentX || Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Y")) != flCurrentY)
                            {
                                view.SetRowCellValue(intRowHandle, "X", flCurrentX);
                                view.SetRowCellValue(intRowHandle, "Y", flCurrentY);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to update the database [" + ex.Message + "].", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else  // Polygon / Polyline //
            {
                try
                {
                    decimal decArea = (decimal)0.00;
                    string strObType = "";
                    if (strObjectType == "polygon")
                    {
                        decArea = Convert.ToDecimal(string.Format("{0:N2}", ((MapInfo.Geometry.MultiPolygon)tempFeatureGeometry).Area(AreaUnit.SquareMeter)));
                        strObType = "polygon";
                    }
                    else
                    {
                        decArea = Convert.ToDecimal(string.Format("{0:N2}", ((MapInfo.Geometry.MultiCurve)tempFeatureGeometry).Length(MapInfo.Geometry.DistanceUnit.Meter, MapInfo.Geometry.DistanceType.Spherical)));
                        strObType = "polyline";
                    }
                    if (Convert.ToInt32(f["ModuleID"]) == 1)  // Poles //
                    {
                        // ***** FOLLOWING COMMENTED OUT AS THERE SHOULD BE NO POLYGONS FOR POLES ***** //
                        /*GridView view = (GridView)gridControl2.MainView;
                        UpdateBackEnd.sp07051_UT_Tree_Picker_Update_PolygonXY(Convert.ToInt32(f["MapID"]), 1, strObType, strPassedParameterString, decAreaM2, (float)tempFeatureGeometry.Centroid.x, (float)tempFeatureGeometry.Centroid.y, flLatLongX, flLatLongY, strLatLongPolygonXY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["MapID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (view.GetRowCellValue(intRowHandle, "XYPairs").ToString() != strPassedParameterString)
                            {
                                view.SetRowCellValue(intRowHandle, "XYPairs", strPassedParameterString);
                            }
                        }*/
                    }
                    else  // Trees //
                    {
                        GridView view = (GridView)gridControl6.MainView;
                        UpdateBackEnd.sp07051_UT_Tree_Picker_Update_PolygonXY(Convert.ToInt32(f["MapID"]), 2, strObType, strPassedParameterString, decArea, (float)tempFeatureGeometry.Centroid.x, (float)tempFeatureGeometry.Centroid.y, flLatLongX, flLatLongY, strLatLongPolygonXY);
                        DevExpress.XtraGrid.Columns.GridColumn col = view.Columns["TreeID"];
                        intRowHandle = view.LocateByValue(0, col, Convert.ToInt32(f["MapID"]));
                        if (intRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                        {
                            if (view.GetRowCellValue(intRowHandle, "XYPairs").ToString() != strPassedParameterString)
                            {
                                view.SetRowCellValue(intRowHandle, "XYPairs", strPassedParameterString);
                            }
                        }
                    }
               }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to update the database [" + ex.Message + "].", "Edit Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void Map_ViewChanged(object o, ViewChangedEventArgs e)
        {
            if (Math.Round(mapControl1.Map.Scale, 0, MidpointRounding.ToEven) != Math.Round(dCurrentMapScale, 0, MidpointRounding.ToEven))  // Scale changed so check if we need to recalculate the size of all point objects on the map //
            {
                dCurrentMapScale = mapControl1.Map.Scale;
                beiPopupContainerScale.EditValue = "Scale 1:" + mapControl1.Map.Scale.ToString("#0");

                refresh_scale_bar();

                // *** If dynamic point sizing is switched on, redraw points so sizes are calculates correctly *** //
                if (intMapObjectSizing == -1) Update_Map_Object_Sizes(true);
            }
            if (boolStoreViewChanges)
            {
                if (!boolStoreViewChangesTempDisable)
                {
                    StoreView();
                }
                else  // Don't store view change as the user has gone to an exiting sored view and we don't want it stored again - so reset flag //
                {
                    boolStoreViewChangesTempDisable = false;
                }
            }
        }


        private void Layers_CountChanged(object o, CollectionEventArgs e)
        {
            bsiLayerCount.Caption = "Layer Count: " + mapControl1.Map.Layers.Count.ToString();
        }

        private void mapControl1_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //memoEdit1.Text = memoEdit1.Text + "\r\nMouseWheel:  Delta=" + e.Delta + ", X=" + e.X + ", Y=" + e.Y;
            //memoEdit1.Refresh();
        }

        private void mapControl1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 's')
                switch (e.KeyChar)
                {
                    case 's':
                        ToggleSnap();  // toggle snap mode if user presses the 'S' key //
                        break;
                }
        }

        private void mapControl1_Enter(object sender, EventArgs e)
        {
            // Start Mouse and Keyboard Hooks //
            mouseHook.Stop();
            keyboardHook.Stop();
            mouseHook.Start();  // Also Stopped in the FormClosed event //
            keyboardHook.Start();  // Also Stopped in the FormClosed event //
        }
        private void mapControl1_GotFocus(object sender, EventArgs e)
        {
            // Start Mouse and Keyboard Hooks //
            mouseHook.Stop();
            keyboardHook.Stop();
            mouseHook.Start();  // Also Stopped in the FormClosed event //
            keyboardHook.Start();  // Also Stopped in the FormClosed event //
        }

        private void bsiSnap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ToggleSnap();  // toggle snap mode //
        }
        private void ToggleSnap()
        {
            mapControl1.Tools.MapToolProperties.SnapEnabled = !mapControl1.Tools.MapToolProperties.SnapEnabled;
            bsiSnap.Caption = "Snap: " + (mapControl1.Tools.MapToolProperties.SnapEnabled ? "On" : "Off");
        }

        private void bsiSyncSelection_ItemClick(object sender, ItemClickEventArgs e)
        {
            ToggleSyncSelection();
        }
        private void ToggleSyncSelection()
        {
            if (bsiSyncSelection.Caption == "Sync Map Selection: On")
            {
                bsiSyncSelection.Caption = "Sync Map Selection: Off";
            }
            else
            {
                bsiSyncSelection.Caption = "Sync Map Selection: On";
            }
        }


        private void mapControl1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            System.Drawing.PointF DisplayPoint = new System.Drawing.PointF(e.X, e.Y);
            MapInfo.Geometry.DPoint MapPoint = new MapInfo.Geometry.DPoint();
            MapInfo.Geometry.DisplayTransform converter = this.mapControl1.Map.DisplayTransform;
            converter.FromDisplay(DisplayPoint, out MapPoint);
            bsiScreenCoords.Caption = "Location: " + String.Format("{0:0.##}", MapPoint.x) + ", " + String.Format("{0:0.##}", MapPoint.y);

            if (boolTrackXandY)  // Used to prevent tracking when right click menu of Map displayed [see popupMenu_MapControl1_Popup and popupMenu_MapControl1_Closeup events] //
            {
                decCurrentMouseX = (decimal)MapPoint.x;
                decCurrentMouseY = (decimal)MapPoint.y;
            }
        }


        private void barSubItemMapEdit_Popup(object sender, EventArgs e)
        {
            MouseEventArgs mea = null;
            ShowMapControl1Menu(mea);  // Set Menu Item status in called event - pass null as we don't want the menu to show from there as barSubItemMapEdit will open it's own version of the menu automatically here. // 
        }

        private void bbiEditSelectedMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedParameterString = "";
            string strPassedParameterString2 = "";
            int intTreeCount = 0;
            int intAssetCount = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Poles //
                        {
                            strPassedParameterString += f["MapID"].ToString() + ",";
                            intTreeCount++;
                        }
                        else // Trees //
                        {
                            strPassedParameterString2 += f["MapID"].ToString() + ",";
                            intAssetCount++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedParameterString) && string.IsNullOrEmpty(strPassedParameterString2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to edit before proceeding.", "Edit Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("selected;", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            if (!string.IsNullOrEmpty(strPassedParameterString) && !string.IsNullOrEmpty(strPassedParameterString2))
            {
                frm_UT_Mapping_Edit_Select_Type fChooseType = new frm_UT_Mapping_Edit_Select_Type();
                fChooseType.ShowDialog();
                switch (fChooseType.strReturnedValue)
                {
                    case "Cancel":
                        return;
                    case "Poles":
                        strPassedParameterString2 = "";  // Clear selected Trees so the edit screen is not opened //
                        break;
                    case "Trees":
                        strPassedParameterString = "";  // Clear selected Poles so the edit screen is not opened //
                        break;
                    case "Both":  // Do nothing so both data entry screens are opened //
                        break;
                    default:
                        break;
                }
            }

            if (!string.IsNullOrEmpty(strPassedParameterString))  // Edit Amenity Trees //
            {
                frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString;
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = intTreeCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            if (!string.IsNullOrEmpty(strPassedParameterString2))  // Edit Assets //
            {
                frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString2;
                fChildForm.strFormMode = "edit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = intAssetCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

        }

        private void bbiBlockEditselectedMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowEdit) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedParameterString = "";
            string strPassedParameterString2 = "";
            int intTreeCount = 0;
            int intAssetCount = 0;
            string strMapID = "";
            string strMapID2 = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Amenity Trees Object //
                        {
                            strPassedParameterString += f["MapID"].ToString() + ",";
                            strMapID += f["id"].ToString() + ",";  // Map IDs //
                            intTreeCount++;
                        }
                        else // Asset Management Object //
                        {
                            strPassedParameterString2 += f["AssetID"].ToString() + ",";
                            strMapID2 += f["id"].ToString() + ",";  // Map IDs //
                            intAssetCount++;
                        }
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedParameterString) && string.IsNullOrEmpty(strPassedParameterString2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to block edit before proceeding.", "Block Edit Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!string.IsNullOrEmpty(strPassedParameterString) && !string.IsNullOrEmpty(strPassedParameterString2))
            {
                frm_UT_Mapping_Edit_Select_Type fChooseType = new frm_UT_Mapping_Edit_Select_Type();
                fChooseType.ShowDialog();
                switch (fChooseType.strReturnedValue)
                {
                    case "Cancel":
                        return;
                    case "Poles":
                        strPassedParameterString2 = "";  // Clear selected Trees so the edit screen is not opened //
                        break;
                    case "Trees":
                        strPassedParameterString = "";  // Clear selected Poles so the edit screen is not opened //
                        break;
                    case "Both":  // Do nothing so both data entry screens are opened //
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(strPassedParameterString))  // Block Edit Amenity Trees //
            {
                frm_UT_pole_Edit fChildForm = new frm_UT_pole_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString;
                fChildForm.strMapIDs = strMapID;
                fChildForm.strFormMode = "blockedit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = selection.TotalCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
            if (!string.IsNullOrEmpty(strPassedParameterString2))  // Block Edit Assets //
            {
                frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strRecordIDs = strPassedParameterString2;
                fChildForm.strFormMode = "blockedit";
                fChildForm.strCaller = this.Name;
                fChildForm.intRecordCount = selection.TotalCount;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }

        private void bbiDeleteMapObjects_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!iBool_AllowDelete) return;

            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strRecordIDs1 = "";
            string strRecordIDs2 = "";
            string strMapIDs = "";
            int intCount1 = 0;
            int intCount2 = 0;
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        if (Convert.ToInt32(f["ModuleID"]) == 1)  // Pole //
                        {
                            strRecordIDs1 += f["MapID"].ToString() + ",";
                            intCount1++;
                        }
                        else  // Tree //
                        {
                            strRecordIDs2 += f["MapID"].ToString() + ",";
                            intCount2++;
                        }
                        strMapIDs += f["id"].ToString() + ",";  // MapIDs //
                    }
                }
            }
            if (string.IsNullOrEmpty(strRecordIDs1) && string.IsNullOrEmpty(strRecordIDs2))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects to delete before proceeding.", "Delete Selected Map Objects", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("delete;", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //
            //if (!iBool_AllowDelete) return;
            string strMessage = "";
            if (intCount1 > 0)
            {
                strMessage = "You have " + (intCount1 == 1 ? "1 Pole" : Convert.ToString(intCount1.ToString()) + " Poles") + " selected for delete!";
            }
            if (intCount2 > 0)
            {
                if (!string.IsNullOrEmpty(strMessage)) strMessage += "\n";
                strMessage += "You have " + (intCount2 == 1 ? "1 Tree" : Convert.ToString(intCount2.ToString()) + " Trees") + " selected for delete!";
            }
            strMessage += "\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount1+intCount2 == 1 ? "this object" : "these objects") + " will be deleted from the database and any related records will also be deleted!";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                if (!string.IsNullOrEmpty(strRecordIDs1))
                {
                    DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp07000_UT_Delete("pole", strRecordIDs1);  // Remove the records from the DB in one go //
                    if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                }
                if (!string.IsNullOrEmpty(strRecordIDs2))
                {
                    DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    RemoveRecords.sp07000_UT_Delete("tree", strRecordIDs2);  // Remove the records from the DB in one go //
                    if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                }

                RefreshMapObjects(strMapIDs, 0);  // Pass the delete object's mapIDs to the event so they can be removed from the map //

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                int intTotal = intCount1 + intCount2;
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intTotal.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void bbiSetLocalityCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("locality", decCurrentMouseX, decCurrentMouseY);
        }

        private void bbiSetDistrictCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("district", decCurrentMouseX, decCurrentMouseY);
        }

        private void bbiSetSiteCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("site", decCurrentMouseX, decCurrentMouseY);
        }

        private void bbiSetClientCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SetRecordCentre("client", decCurrentMouseX, decCurrentMouseY);
        }

        private void SetRecordCentre(string strRecordType, decimal decX, decimal decY)
        {
            frm_AT_Mapping_Set_Centre_Point frm_child = new frm_AT_Mapping_Set_Centre_Point();
            frm_child.GlobalSettings = GlobalSettings;
            frm_child.strRecordType = strRecordType;
            this.ParentForm.AddOwnedForm(frm_child);
            if (frm_child.ShowDialog() == DialogResult.OK)
            {
                int intRecordID = frm_child.intRecordID;
                System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.InvariantCulture;  // Needed for converting to Title Case on MessageBox //
                System.Globalization.TextInfo textInfo = cultureInfo.TextInfo;  // Needed for converting to Title Case on MessageBox //
                try
                {
                    // Generate Lat Long values //
                    float flLatLongX = (float)0.00;
                    float flLatLongY = (float)0.00;
                    string strLatLongPolygonXY = "";
                    Mapping_Functions MapFunctions = new Mapping_Functions();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords((float)decX, (float)decY, "");
                    if (ftr != null) MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref flLatLongX, ref flLatLongY, ref strLatLongPolygonXY);

                    // Update Database //
                    DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter UpdateRecord = new DataSet_AT_TreePickerTableAdapters.QueriesTableAdapter();
                    UpdateRecord.ChangeConnectionString(strConnectionString);
                    UpdateRecord.sp01319_AT_Tree_Picker_Set_Centre_Point(intRecordID, strRecordType, decX, decY, flLatLongX, flLatLongY);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to set the " + strRecordType + " centre point [" + ex.Message + "]!\n\nTry doing it again. If the problem persist contact Technical Support.", "Set " + textInfo.ToTitleCase(strRecordType) + " Centre Point", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(textInfo.ToTitleCase(strRecordType) + " Centre Point Updated Successfully.", "Set " + textInfo.ToTitleCase(strRecordType) + " Centre Point", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        private void bbiTransferToWorkOrder_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get all selected items and iterate through them //
            MapInfo.Data.MultiResultSetFeatureCollection selection = MapInfo.Engine.Session.Current.Selections.DefaultSelection;
            System.Collections.IEnumerator enum1 = selection.GetEnumerator();
            string strPassedTreeIDs = "";
            while (enum1.MoveNext())
            {
                MapInfo.Data.IResultSetFeatureCollection irfc = (MapInfo.Data.IResultSetFeatureCollection)enum1.Current;
                if (irfc.BaseTable.Alias == "MapObjects")  // Ignore any selectable layers except MapObjects //
                {
                    foreach (MapInfo.Data.Feature f in irfc)
                    {
                        strPassedTreeIDs += f[1].ToString() + ",";  // Tree IDs //
                    }
                }
            }
            if (string.IsNullOrEmpty(strPassedTreeIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more map objects containing actions before proceeding.", "Transfer Actions To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            //bool boolSucceed = NotifyWoodPlanVersion4("workorder", strPassedParameterString);  // Call WoodPlan V.4 for Edit screens //

            // Get number of actions from selected map objects available for adding to work order //
            DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter GetSetting = new DataSet_AT_WorkOrdersTableAdapters.QueriesTableAdapter();       
            GetSetting.ChangeConnectionString(strConnectionString);
            int intCount = Convert.ToInt32(GetSetting.sp01409_AT_Tree_Picker_WorkOrder_Add_Get_Action_Count(strPassedTreeIDs));
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The currently selected map objects have no actions available for linking to Work Orders.\n\nSelect one or more map objects containing actions before proceeding.", "Transfer Actions To Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check if only one Edit work Order screen is open - if yes, make sure the currently loaded Work Order has been saved (intWorkOrderID > 0) //
            Form frm_main;
            frm_main = this.MdiParent;
            frm_AT_WorkOrder_Edit fForm = new frm_AT_WorkOrder_Edit();
            int intFormCount = 0;
            string strWorkOrderIDs = "";
            int intWorkOrderID = 0;
            foreach (Form frmChild in frm_main.MdiChildren)
            {
                if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                {
                    fForm = (frm_AT_WorkOrder_Edit)frmChild;
                    if (fForm.Action_Adding_Allowed(ref strWorkOrderIDs)) 
                    {
                        intFormCount++;
                        intWorkOrderID = fForm.Return_ActionsID_To_Calling_Form();
                    }
                }
            }
            switch (intFormCount)
            {
                case 1:
                    {
                        // Step round the open forms to find the correct one then activate it //
                        foreach (Form frmChild in frm_main.MdiChildren)
                        {
                            if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                            {
                                fForm = (frm_AT_WorkOrder_Edit)frmChild;
                                if (fForm.Return_ActionsID_To_Calling_Form() == intWorkOrderID)
                                {
                                    break;  // We have a reference to the correct form so exit loop. //
                                }
                            }
                        }
                        fForm.Activate();
                        fForm.Add_Record(strPassedTreeIDs);
                        break;
                    }
                case 0:
                    {
                        // Open a list of incomplete Work Orders to allow the user to pick the Work Order to add to //

                        frm_AT_Work_Order_Select_On_Map_Add fSelect = new frm_AT_Work_Order_Select_On_Map_Add();
                        this.ParentForm.AddOwnedForm(fSelect);
                        fSelect.GlobalSettings = this.GlobalSettings;
                        if (fSelect.ShowDialog() == DialogResult.OK)
                        {
                            int intSelectedWorkOrderID = fSelect.intSelectedID;
                            if (intSelectedWorkOrderID <= 0) return;

                            System.Reflection.MethodInfo method = null;
                            fProgress = new frmProgress(10);
                            this.AddOwnedForm(fProgress);
                            fProgress.Show();  // ***** Closed in PostOpen event ***** //
                            Application.DoEvents();

                            frm_AT_WorkOrder_Edit fChildForm = new frm_AT_WorkOrder_Edit();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = intSelectedWorkOrderID.ToString() + ",";
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = "frm_AT_WorkOrder_Manager";
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            fChildForm.fProgress = fProgress;
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                            fChildForm.Add_Record(strPassedTreeIDs);

                        }
                        break;
                    }
                default:  // More than 1 so open screen asking the user to select which work order to add to //
                    {
                        // Open a list of incomplete Work Orders that are currently open to allow the user to pick the Work Order to add to //

                        frm_AT_Work_Order_Select_On_Map_Add fSelect = new frm_AT_Work_Order_Select_On_Map_Add();
                        this.ParentForm.AddOwnedForm(fSelect);
                        fSelect.GlobalSettings = this.GlobalSettings;
                        fSelect.strPassedInWorkOrderIDs = strWorkOrderIDs;
                        if (fSelect.ShowDialog() == DialogResult.OK)
                        {
                            int intSelectedWorkOrderID = fSelect.intSelectedID;
                            if (intSelectedWorkOrderID <= 0) return;

                            foreach (Form frmChild in frm_main.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_WorkOrder_Edit")
                                {
                                    fForm = (frm_AT_WorkOrder_Edit)frmChild;
                                    if (fForm.Return_ActionsID_To_Calling_Form() == intSelectedWorkOrderID)
                                    {
                                        break;  // We have a reference to the correct form so exit loop. //
                                    }
                                }
                            }
                            fForm.Activate();
                            fForm.Add_Record(strPassedTreeIDs);
                            break;
                        }
                        break;
                    }
            }

        }

        private void bbiNearbyObjectsFind_ItemClick(object sender, ItemClickEventArgs e)
        {
            // X and Y set in mouseHook_MouseUp //
            SearchForNearbyMapObjects(decCurrentMouseX, decCurrentMouseY, Convert.ToDecimal(beiGazetteerLoadObjectsWithinRange.EditValue), "No Address Specified");
        }

        private void SearchForNearbyMapObjects(decimal decX, decimal decY, decimal decRange, string strDescription)
        {
            WaitDialogForm loading = new WaitDialogForm("Checking For Nearby Map Objects...", "WoodPlan Mapping");
            loading.Show();

            //decimal decRangeAdjusted = (decRangeSpinner * (decimal)100) / (decimal)113.5;  // Gets round round size of circle drawn (always 13.5% too big) //
            decimal decRangeAdjusted = decRange;

            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp01320_AT_Gazetteer_Get_Map_Objects_In_Range", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@decX", decX));
            cmd.Parameters.Add(new SqlParameter("@decY", decY));
            cmd.Parameters.Add(new SqlParameter("@decRange", decRange));
            SqlDataAdapter sdaTreesToShow = new SqlDataAdapter(cmd);
            DataSet dsTreesToShow = new DataSet("NewDataSet");
            try
            {
                sdaTreesToShow.Fill(dsTreesToShow, "Table");
            }
            catch (Exception ex)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the map objects within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current address.\n\nError: " + ex.Message, "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;

            }

            SQlConn = new SqlConnection(strConnectionString);
            cmd = null;
            cmd = new SqlCommand("sp03075_AT_Gazetteer_Get_Asset_Map_Objects_In_Range", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@decX", decX));
            cmd.Parameters.Add(new SqlParameter("@decY", decY));
            cmd.Parameters.Add(new SqlParameter("@decRange", decRange));
            SqlDataAdapter sdaAssetsToShow = new SqlDataAdapter(cmd);
            DataSet dsAssetsToShow = new DataSet("NewDataSet");
            try
            {
                sdaAssetsToShow.Fill(dsAssetsToShow, "Table");
            }
            catch (Exception ex)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Asset map objects within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current address.\n\nError: " + ex.Message, "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;

            }

            if (dsTreesToShow.Tables[0].Rows.Count == 0 && dsAssetsToShow.Tables[0].Rows.Count == 0)
            {
                loading.Close();
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no Map Objects to load within " + decRange.ToString() + (decRange == (decimal)1.00 ? " metre" : " metres") + " of the current location.", "Load  Map Objects in Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string strTreeIDs = "";
            foreach (DataRow dr in dsTreesToShow.Tables[0].Rows)
            {
                strTreeIDs += dr["MapID"].ToString() + ",";
            }
            string strAssetIDs = "";
            foreach (DataRow dr in dsAssetsToShow.Tables[0].Rows)
            {
                strAssetIDs += dr["AssetID"].ToString() + ",";
            }
            // ***** GOT THIS FAR WITH UPDATE WITH ASSETS ***** //
            Mapping_Functions MapFunctions = new Mapping_Functions();
            MapFunctions.Gazetteer_Map_Object_Search(this.GlobalSettings, this.strConnectionString, this.ParentForm, strTreeIDs, "tree", strAssetIDs, "asset");

            Create_Gazetteer_Search_Temp_Feature(Convert.ToDouble(decX), Convert.ToDouble(decY), decRange, strDescription, decRangeAdjusted);
            loading.Close();

        }

        private void bbiOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "MapInfo TAB Files(*.tab)|*.tab|MapInfo Geoset Files(*.gst)|*.gst|MWS Files(*.mws)|*.mws";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mapControl1.Map.Load(new MapGeosetLoader(dlg.FileName));
                }
                catch (Exception)
                {

                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to load the specified file", "Load File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }

            }
            //mapControl1.Tools.LeftButtonTool = "Select";

        }

        private void bbiNone_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Arrow";
        }

        private void bbiPan_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Pan";
        }

        private void bbiZoomIn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "ZoomIn";
        }

        private void bbiZoomOut_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "ZoomOut";
        }

        private void bbiCentre_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Center";
        }

        private void bbiSelect_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Select";
        }

        private void bbiSelectRectangle_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRect";
        }

        private void bbiSelectRadius_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRadius";
        }

        private void bbiSelectPolygon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectPolygon";
        }

        private void bbiSelectRegion_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "SelectRegion";
        }

        private void beiPlotObjectType_EditValueChanged(object sender, EventArgs e)
        {
            BarEditItem bei = (BarEditItem)sender;
            i_intSelectedObjectType = Convert.ToInt32(bei.EditValue);
            if (i_intSelectedObjectType == 0 || !iBool_AllowEdit) // No object type selected or no Add access so disable Plotting tools on toolbar //
            {
                bbiAddPoint.Enabled = false;
                bbiAddPolygon.Enabled = false;
                bbiAddPolyLine.Enabled = false;
                if (mapControl1.Tools.LeftButtonTool == "AddPoint" || mapControl1.Tools.LeftButtonTool == "AddPolyline" || mapControl1.Tools.LeftButtonTool == "AddPolygon")
                {
                    mapControl1.Tools.LeftButtonTool = "Arrow";
                    bbiNone.Checked = true;
                }
            }
            else // object type selected and Add access so enable Plotting tools on toolbar //
            {
                if (i_intSelectedObjectType == -1)  // Poles //
                {
                    bbiAddPoint.Enabled = true;
                    bbiAddPolygon.Enabled = false;
                    bbiAddPolyLine.Enabled = false;
                    if (mapControl1.Tools.LeftButtonTool == "AddPolyline" || mapControl1.Tools.LeftButtonTool == "AddPolygon")
                    {
                        mapControl1.Tools.LeftButtonTool = "Arrow";
                        bbiNone.Checked = true;
                    }
                    ceGPS_PlotPolygon.Enabled = false;
                    ceGPS_PlotLine.Enabled = false;
                }
                else
                {
                    bbiAddPoint.Enabled = true;
                    bbiAddPolygon.Enabled = true;
                    bbiAddPolyLine.Enabled = true;
                    ceGPS_PlotPolygon.Enabled = true;
                    ceGPS_PlotLine.Enabled = true;
                }
            }
        }

        private void bbiAddPolygon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPolygon";
        }

        private void bbiAddPolyLine_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPolyline";
        }

        private void bbiAddPoint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "AddPoint";
        }

        private void bbiQueryTool_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "InfoTool";
        }

        private void bciEditingNone_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingNone.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.None;
        }

        private void bciEditingMove_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingMove.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.Objects;
        }

        private void bciEditingAdd_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingAdd.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.AddNode;
        }

        private void bciEditingEdit_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciEditingEdit.Checked) mapControl1.Tools.SelectMapToolProperties.EditMode = MapInfo.Tools.EditMode.Nodes;
        }

        private void bbiMeasureLine_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.Tools.LeftButtonTool = "Distance";  // Custom Tool //
        }

        private void bbiWorkOrderMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            frm_UT_Mapping_Map_Snapshot_Step1 frm_child = new frm_UT_Mapping_Map_Snapshot_Step1();
            frm_child.GlobalSettings = this.GlobalSettings;
            frm_child._LinkedToRecordTypeID = _PassedInRecordType;
            frm_child._LinkedToRecordID = _PassedInPermissionDocumentID;
            frm_child._LinkedToRecordDescription = _PassedInPermissionDocumentDescription;
            frm_child.frmParentMappingForm = this;
            frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale);
            this.ParentForm.AddOwnedForm(frm_child);

            // Get current DPI from screen //
            Graphics dspGraphics = CreateGraphics();
            int intDPI = Convert.ToInt32(dspGraphics.DpiX);
            dspGraphics.Dispose();
            frm_child.intDPI = intDPI;

            frm_child.ShowDialog();
        }
         
        public void Generate_Work_Order_Map(ref Image image)  // CALLED BY frm_AT_Mapping_WorkOrder_Map_Step1 FORM //
        {
            // Export a a bigger map than what is currently shown on the screen //
            MapInfo.Geometry.DPoint dpt1 = new MapInfo.Geometry.DPoint(mapControl1.Map.Center.x, mapControl1.Map.Center.y);  // create a Dpoint to center the map on //
            MapInfo.Mapping.Map map = (MapInfo.Mapping.Map)mapControl1.Map.Clone();  // clone the map window //
            using (MapInfo.Mapping.MapExport export = new MapInfo.Mapping.MapExport(map))
            {
                // set the size for the exported image //  
                // Get current DPI from screen //
                Graphics dspGraphics = CreateGraphics();
                int intDPI = Convert.ToInt32(dspGraphics.DpiX);
                dspGraphics.Dispose();
                export.ExportSize = new MapInfo.Mapping.ExportSize(3000, 3000, (short)intDPI);
                //300);
                export.Map.SetView(dpt1, mapControl1.Map.GetDisplayCoordSys(), mapControl1.Map.Scale);
                // set the scale for the exported map //
                export.Format = MapInfo.Mapping.ExportFormat.Jpeg;
                // set the image format //
                image = export.Export();
            }  // perform the export to an Image object //
            map = null;
            return;
        }

        private void frm_UT_Mapping_MouseClick(object sender, MouseEventArgs e)
        {
            string strButton = e.Button.ToString();
        }

        private void bbiPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrintMap();
        }

        private void PrintMap()
        {
            /*WaitDialogForm loading = new WaitDialogForm("Generating Map Preview...", "WoodPlan Mapping");
            loading.Show();

            Image image = null;
            this.Generate_Work_Order_Map(ref image);
            using (frm_AT_Mapping_WorkOrder_Map frm_child = new frm_AT_Mapping_WorkOrder_Map())
            {
                frm_child.GlobalSettings = this.GlobalSettings;
                frm_child.strFormMode = "View";
                frm_child.imageMap = image;
                frm_child.frmParentMappingForm = this;
                frm_child.intCurrentMapScale = Convert.ToInt32(dCurrentMapScale);
                // Get current DPI from screen //
                Graphics dspGraphics = CreateGraphics();
                int intDPI = Convert.ToInt32(dspGraphics.DpiX);
                dspGraphics.Dispose();
                frm_child.intDPI = intDPI;
                loading.Close();
                frm_child.ShowDialog();
            }
            image = null;
            GC.GetTotalMemory(true);*/
        }

        private void dockPanel2_Click(object sender, EventArgs e)
        {

        }

        private void buttonEditFilterCircuits_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                Open_Choose_Circuits_Screen();
            }
            else if (e.Button.Tag.ToString() == "refresh poles")  // Choose Button //
            {
                if (!splashScreenManager.IsSplashFormVisible)
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    splashScreenManager.ShowWaitForm();
                }
                if (string.IsNullOrEmpty(strCircuitIDs)) strCircuitIDs = "";
                Load_Map_Objects_Grid();

                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            }
        }
        private void Open_Choose_Circuits_Screen()
        {
            string strOriginalCircuitIDs = strCircuitIDs;  // Store original value so we can tell if it has changed later //

            frm_UT_Mapping_Pole_Filter fChildForm = new frm_UT_Mapping_Pole_Filter();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInClientIDs = _PassedInClientIDs;
            fChildForm._PassedInRegionIDs = _PassedInRegionIDs;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                strCircuitIDs = fChildForm.i_str_selected_circuit_ids;
                strCircuitNames = fChildForm.i_str_selected_circuit_names;
                buttonEditFilterCircuits.EditValue = strCircuitNames;
                _PassedInPoleIDs = "";  // Clear any passed in poles from Permissions Documents or Work Orders otherwise that list will override what is returned by the sp07049_UT_Tree_Picker_Poles SP //
                if (strOriginalCircuitIDs != strCircuitIDs)
                {
                    Load_Map_Objects_Grid();  // Reload Poles List //
                    
                    // Selected the newly loaded map objects //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        view.SetRowCellValue(i, "CheckMarkSelection",1);
                    }
                    view.EndUpdate();
                }
            }
        }

        private void btnRefreshTrees_Click(object sender, EventArgs e)
        {
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
            }
            Load_Tree_Objects_Grid();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        public void Load_Tree_Objects_Grid()
        {
            // Loads either trees for selected poles or trees in passed in list of trees (when called by PermissionDocuments or WorkOrders) //
            this.RefreshGridViewState2.SaveViewInfo(); // Store any expanded groups and selected rows //
            // Store visible Map Objects first //
            int intSelectionCount = selectionTrees.SelectedCount;
            int intCount = 0;
            List<int> list = new List<int>();
            GridView view = (GridView)gridControl6.MainView;
            int intMax = view.DataRowCount;
            if (intSelectionCount > 0)
            {
                for (int i = 0; i < intMax; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        list.Add(Convert.ToInt32(view.GetRowCellValue(i, "TreeID")));
                        intCount++;
                        if (intCount >= intSelectionCount) break;
                    }
                }
            }

            
            view = (GridView)gridControl2.MainView;
            int intProcessedRows = 0;
            if (selection1.SelectedCount <= 0 && string.IsNullOrEmpty(_PassedInTreeIDs))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more poles to load trees for before proceeding.", "Load Trees", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strPoleIDs = "";
            if (selection1.SelectedCount > 0)
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        strPoleIDs += Convert.ToString(view.GetRowCellValue(i, "PoleID")) + ',';
                        intProcessedRows++;
                    }
                    if (intProcessedRows >= selection1.SelectedCount) break;
                }
            }
            view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            this.sp07053_UT_Tree_Picker_TreesTableAdapter.Fill(this.dataSet_UT_Mapping.sp07053_UT_Tree_Picker_Trees, strPoleIDs, _PassedInTreeIDs, _SelectedSurveyID, _TreeThematicTypeID);
            //view.EndUpdate();

            // Put Visible objects back //
            if (intSelectionCount > 0)
            {
                GridColumn column = view.Columns["TreeID"];
                int intFoundRow = 0;
                foreach (int item in list)
                {
                    intFoundRow = view.LocateByValue(0, column, item);
                    if (intFoundRow != GridControl.InvalidRowHandle) view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                }
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

        }

        private void btnScaleToViewedObjects_Click(object sender, EventArgs e)
        {
            Scale_Map_To_Visible_Objects(true);
        }

        private void dockPanel1_DockChanged(object sender, EventArgs e)
        {
            // Events Panel //
            // Make semi transparent when undocked //
            DockPanel panel = sender as DockPanel;
            if (panel.FloatForm != null)
            {
                panel.FloatForm.AllowTransparency = true;
                panel.FloatForm.Opacity = 0.9;
            }
        }

        private void groupControl3_Paint(object sender, PaintEventArgs e)
        {

        }





 
        /*
        //Global Variables 
        MapExport _PrintExport = null;
        Image _PrintImage = null;
        System.Drawing.Rectangle _MapPrintArea;
        int _CurrentPrintImageX = 0;
        int _CurrentPrintImageY = 0;
        string _CurrentMapPrintTitle = "Test Print";

        private void Print()
        {
            try
            {
                //Initialise the Print Variables 
                this._MapPrintArea = new System.Drawing.Rectangle();

                //Initialise the Print Document 
                PrintDocument printDoc = new System.Drawing.Printing.PrintDocument();
                printDoc.PrinterSettings.PrintRange = PrintRange.AllPages;
                printDoc.PrintPage += new PrintPageEventHandler(PrintDocument_PrintPage);
                printDoc.BeginPrint += new PrintEventHandler(PrintDocument_BeginPrint);

                //Set the Print Area for the Map to the paper size taking into account the margins. 
                this._MapPrintArea.Height = printDoc.DefaultPageSettings.Bounds.Height - (printDoc.DefaultPageSettings.Margins.Top + printDoc.DefaultPageSettings.Margins.Bottom);
                this._MapPrintArea.Width = printDoc.DefaultPageSettings.Bounds.Width - (printDoc.DefaultPageSettings.Margins.Left + printDoc.DefaultPageSettings.Margins.Right);
                this._MapPrintArea.X = printDoc.DefaultPageSettings.Margins.Left;
                this._MapPrintArea.Y = printDoc.DefaultPageSettings.Margins.Top;

                //Set the Preview Dialog 
                PrintPreviewDialog previewDialog = new PrintPreviewDialog();
                previewDialog.Document = printDoc;
                previewDialog.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Print Failed");
            }
        }

        private void PrintDocument_BeginPrint(object sender, PrintEventArgs e)
        {
            try
            {
                //Reset the Print Variables 
                this._PrintExport = null;
                this._PrintImage = null;
                this._CurrentPrintImageX = 0;
                this._CurrentPrintImageY = 0;

                //Scale 1:25000 as define 0.25km per 1 cm. 
                //double scale = 0.25;
                double scale = 5;

                //Generate a new Map Export File. 
                this._PrintExport = new MapExport(this.mapControl1.Map.Clone() as Map);

                //Determine the distance that the current Map View occupies in Kms so we can work out the scale. 
                DPoint[] xPoints = new DPoint[2];
                xPoints[0] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y1);
                xPoints[1] = new DPoint(this._PrintExport.Map.Bounds.x2, this._PrintExport.Map.Bounds.y1);
                Curve curve = new Curve(this._PrintExport.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, xPoints);
                //double widthKms = curve.Length(DistanceUnit.Kilometer, DistanceType.Spherical);
                double widthKms = curve.Length(DistanceUnit.Meter, DistanceType.Spherical);

                DPoint[] yPoints = new DPoint[2];
                yPoints[0] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y1);
                yPoints[1] = new DPoint(this._PrintExport.Map.Bounds.x1, this._PrintExport.Map.Bounds.y2);
                curve = new Curve(this._PrintExport.Map.GetDisplayCoordSys(), MapInfo.Geometry.CurveSegmentType.Linear, yPoints);
                //double heightKms = curve.Length(DistanceUnit.Kilometer, DistanceType.Spherical);
                double heightKms = curve.Length(DistanceUnit.Meter, DistanceType.Spherical);

                //Use the export size feature which allows for the generated to be define in paper units. 
                //This will allow use to calculate the printed scale 
                this._PrintExport.ExportSize = new ExportSize((widthKms / scale), (heightKms / scale), MapInfo.Geometry.PaperUnit.Centimeter);

                //Export the Print Image. 
                this._PrintImage = this._PrintExport.Export();
            }
            catch (Exception)
            {
                MessageBox.Show("Failed to export and print the map at the selected Scale.  The generated file maybe too big.", "Print Failed");
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                // Add header text to each page. 
                Graphics g = e.Graphics;
                //Image logo = (Image)Properties.Resources.Logo;
                string datum = this.mapControl1.Map.GetDisplayCoordSys().Datum.ToString();
                //g.DrawImage(logo, 10, 10);
                g.DrawString(this._CurrentMapPrintTitle, new System.Drawing.Font("Arial", 12, FontStyle.Regular), Brushes.Black, 50, 20);
                g.DrawString(datum, new System.Drawing.Font("Arial", 10, FontStyle.Regular), Brushes.Black, ((e.PageBounds.Width / 2) - ((datum.Length * 10) / 2)), 20);
                g.DrawString("Printed: " + DateTime.Now.ToString("dd-MMM-yyyy hh:mm tt"), new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, (e.PageBounds.Right - 350), 20);
                g.DrawString("By: " + this.GlobalSettings.Username, new System.Drawing.Font("Arial", 8, FontStyle.Regular), Brushes.Black, (e.PageBounds.Right - 350), 35);

                //Do the custom printing for when the user has requested scaling... 
                //This code was taken from another another MapInfo forum article.  It prints the area of the export image that can fit into 
                //print area.  It then moves to the next unprinted section of the export image. 
                e.Graphics.DrawImage(this._PrintImage, this._MapPrintArea, this._CurrentPrintImageX, this._CurrentPrintImageY, this._MapPrintArea.Width, this._MapPrintArea.Height, System.Drawing.GraphicsUnit.Pixel);
                e.HasMorePages = true;
                if (this._CurrentPrintImageY < this._PrintImage.Height - this._MapPrintArea.Height) this._CurrentPrintImageY += this._MapPrintArea.Height;
                else if (this._CurrentPrintImageX < this._PrintImage.Width - this._MapPrintArea.Width)
                {
                    this._CurrentPrintImageX += this._MapPrintArea.Width;
                    this._CurrentPrintImageY = 0;
                }
                else e.HasMorePages = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Print Failed");
            }
        }

  */
    }



    internal class SelectedAreaModifier2 : MapInfo.Mapping.FeatureStyleModifier
    {
        private System.Collections.Hashtable features;
        private int intBackColour;
        private int intTransparencyLevel;

        public SelectedAreaModifier2(string name, string alias, MapInfo.Data.IResultSetFeatureCollection irfc, int intColour, int intTransparancy)
            : base(name, alias)
        {
            features = new System.Collections.Hashtable();
            string[] exp = new string[] { "MI_Key" };
            this.Expressions = exp;

            foreach (MapInfo.Data.Feature f in irfc)
            {
                features.Add(f.Key.Value, f.Key.Value);
            }
            intBackColour = intColour;
            intTransparencyLevel = intTransparancy;
        }

        protected override bool Modify(MapInfo.Styles.FeatureStyleStack styles, object[] values)
        {
            MapInfo.Styles.CompositeStyle cs = styles.Current;

            SimpleVectorPointStyle _vectorSymbol = new SimpleVectorPointStyle();
            _vectorSymbol.Code = (short)49;  // 49 = Cross //
            _vectorSymbol.Color = Color.FromArgb(0, 0, 255);
            _vectorSymbol.PointSize = (short)30;

            if (features.Contains((values[0] as MapInfo.Data.Key).Value))
            {
                // Set the style for region features because we are applying the modifier to a region layer.
                (cs.AreaStyle.Interior as MapInfo.Styles.SimpleInterior).ForeColor = Color.FromArgb((int)(255 * (1 - (Convert.ToDouble(intTransparencyLevel) / 100))), Color.FromArgb(intBackColour));
                cs.SymbolStyle = _vectorSymbol;
                return true;
            }
            return false;
        }
    }


    // Overview Map Class //
    public class MapOverviewAdornment2 : MapInfo.Mapping.IAdornment
    {
        private string _Alias;
        private string _Name;
        private System.Drawing.Size _Size;
        private System.Drawing.Point _Loc;
        private MapInfo.Mapping.Map _Map;
        private string _FilePath;

        public event ViewChangedEventHandler ViewChangedEvent;

        public void MapAdornment(string alias, string name, System.Drawing.Size size, System.Drawing.Point loc, MapInfo.Mapping.Map map, string filepath)
        {
            _Alias = alias;
            _Name = name;
            _Size = size;
            _Loc = loc;
            _Map = map;
            _FilePath = filepath;

            MapInfo.Mapping.MapExport ExportObject = new MapInfo.Mapping.MapExport(_Map.Clone() as MapInfo.Mapping.Map);
            ExportObject.ExportSize = new MapInfo.Mapping.ExportSize(150, 150);
            ExportObject.Format = MapInfo.Mapping.ExportFormat.Bmp;
            ExportObject.Export(filepath);
            ExportObject.Dispose();
        }

        string IAdornment.Alias
        {
            get
            {
                return _Alias;
            }
            set
            {
                _Alias = value;
            }
        }

        string IAdornment.Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        System.Drawing.Size IAdornment.Size
        {
            get
            {
                return _Size;
            }
            set
            {
                _Size = value;
            }
        }

        System.Drawing.Point IAdornment.Location
        {
            get
            {
                return _Loc;
            }
            set
            {
                _Loc = value;
            }
        }

        MapInfo.Mapping.Map IAdornment.Map
        {
            get
            {
                return _Map;
            }
        }

        public void Draw(System.Drawing.Graphics graphics, System.Drawing.Rectangle updateArea, System.Drawing.Point drawPnt)
        {
            Bitmap img = new Bitmap(_FilePath);
            graphics.DrawImage(img, drawPnt);
        }
    }



}
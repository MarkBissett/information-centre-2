using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using System.Data.SqlClient;  // Used by Generate Map process //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Permission_Doc_Edit_UKPN : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public string strLinkedToName = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        private string strSignaturePath = "";
        private string strPermissionDocumentPath = "";
        private string strPicturePath = "";
        private string strMapPath = "";
        private string strLinkedDocumentPath = "";
        private string strImagePath = "";

        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowDelete = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        public string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        #endregion

        public frm_UT_Permission_Doc_Edit_UKPN()
        {
            InitializeComponent();
        }

        private void frm_UT_Permission_Doc_Edit_UKPN_Load(object sender, EventArgs e)
        {
            this.FormID = 500084;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            strImagePath = Application.StartupPath;

            sp00226_Staff_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00226_Staff_List_With_BlankTableAdapter.Fill(dataSet_AT_DataEntry.sp00226_Staff_List_With_Blank);

            sp07197_UT_Permission_Statuses_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07197_UT_Permission_Statuses_ListTableAdapter.Fill(dataSet_UT_Edit.sp07197_UT_Permission_Statuses_List);

            sp07170_UT_Land_Owner_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07170_UT_Land_Owner_Types_With_BlankTableAdapter.Fill(dataSet_UT_WorkOrder.sp07170_UT_Land_Owner_Types_With_Blank);

            sp07392_UT_PD_Linked_WorkTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "ActionPermissionID");

            sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "sp07202_UT_Mapping_Snapshots_Linked_Snapshots");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "LinkedDocumentID");

            sp07402_UT_PD_Linked_Access_MapsTableAdapter.Connection.ConnectionString = strConnectionString;

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Signature Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strSignaturePath.EndsWith("\\")) strSignaturePath += "\\";

            try
            {
                strPermissionDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Permission Document Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPermissionDocumentPath.EndsWith("\\")) strPermissionDocumentPath += "\\";

            try
            {
                strPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPicturePath.EndsWith("\\")) strPicturePath += "\\";

            try
            {
                strMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strMapPath.EndsWith("\\")) strMapPath += "\\";
            try
            {
                strLinkedDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strLinkedDocumentPath.EndsWith("\\")) strLinkedDocumentPath += "\\";

            // Populate Main Dataset //
            sp07389_UT_PD_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["OwnerName"] = strLinkedToName;
                        drNewRow["DateRaised"] = DateTime.Today;
                        drNewRow["RaisedByID"] = GlobalSettings.UserID;
                        drNewRow["PermissionEmailed"] = 0;
                        drNewRow["PostageRequired"] = 0;
                        drNewRow["SentByPost"] = 0;
                        drNewRow["SentByStaffID"] = 0;
                        drNewRow["AccessAgreedWithLandOwner"] = 1;  // Default to Yes //
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["OwnerName"] = strLinkedToName;
                        drNewRow["DateRaised"] = DateTime.Today;
                        drNewRow["RaisedByID"] = GlobalSettings.UserID;
                        drNewRow["Reactive"] = 0;
                        this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows.Add(drNewRow);
                        this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                        iBool_AllowDelete = false;
                        iBool_AllowAdd = false;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07389_UT_PD_ItemTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item, strRecordIDs, strFormMode, strImagePath);
                        Load_Maps();
                    }
                    catch (Exception)
                    {
                    }
                    Load_Permissioned_Work();
                    Load_Linked_Maps();

                    if (strFormMode == "view")
                    {
                        iBool_AllowAdd = false;
                        iBool_AllowEdit = false;
                        iBool_AllowDelete = false;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is PanelControl) this.Attach_EditValueChanged_To_Children(item.Controls);  // Required for controls within Panels //
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                if (item2 is PanelControl) this.Detach_EditValuechanged_From_Children(item.Controls);  // Required for controls within Panels //
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberTextEdit.Focus();

                        OwnerNameButtonEdit.Properties.ReadOnly = false;
                        OwnerNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ReferenceNumberTextEdit.Focus();

                        OwnerNameButtonEdit.Properties.ReadOnly = false;
                        OwnerNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberTextEdit.Focus();

                        OwnerNameButtonEdit.Properties.ReadOnly = false;
                        OwnerNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ReferenceNumberTextEdit.Focus();

                        OwnerNameButtonEdit.Properties.ReadOnly = false;
                        OwnerNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberTextEdit.Focus();

                        OwnerNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            /*SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9153", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9153:  // Size Bands Picklist //    
                        {
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            SizeBandIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }*/
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            GridView view = (GridView)gridControl1.MainView;
            view.CloseEditor();
            sp07392UTPDLinkedWorkBindingSource.EndEdit();  // Force any pending save from Grid to underlying table adapter //

            DataSet dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit" || strFormMode == "view")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            else if (i_int_FocusedGrid == 6)
            {
                view = (GridView)gridControl6.MainView;
            }
            else if (i_int_FocusedGrid == 4)
            {
                view = (GridView)gridControl4.MainView;
            }
            else if (i_int_FocusedGrid == 5)
            {
                view = (GridView)gridControl5.MainView;
            }
            intRowHandles = view.GetSelectedRows();

            int intPermissionDocumentID = 0;  // Check if the current record has been saved at some point //
            string strSavedPDFFile = "";
            int intPostageRequired = 0;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow != null)
            {
                intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
                strSavedPDFFile = (currentRow["PDFFile"] == null ? "" : currentRow["PDFFile"].ToString());
                intPostageRequired = (currentRow["PostageRequired"] == null ? 0 : Convert.ToInt32(currentRow["PostageRequired"]));
            }
            bbiAddWorkToPermission.Enabled = iBool_AllowAdd && strFormMode != "blockedit" && intPermissionDocumentID > 0;
            bbiCreateMap.Enabled = iBool_AllowAdd && intPermissionDocumentID > 0 && view.RowCount > 0;  // RowCount used instead of DataRowCount as the grid may be filtered //
            bbiCreatePermissionDocument.Enabled = iBool_AllowAdd && intPermissionDocumentID > 0 && view.RowCount > 0;  // RowCount used instead of DataRowCount as the grid may be filtered //
            bbiEmailToCustomer.Enabled = iBool_AllowAdd && !string.IsNullOrEmpty(strSavedPDFFile);
            bbiPostedToCustomer.Enabled = iBool_AllowAdd && !string.IsNullOrEmpty(strSavedPDFFile) && intPostageRequired == 1;

            if (i_int_FocusedGrid == 1)  // Permissioned Work //
            {
                if (iBool_AllowAdd && strFormMode != "blockedit" && intPermissionDocumentID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && intPermissionDocumentID > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && intPermissionDocumentID > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 6)  // Linked Map //
            {
                if (iBool_AllowAdd && strFormMode == "edit" && intPermissionDocumentID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = false;
                bbiBlockEdit.Enabled = false;
                if (iBool_AllowDelete && strFormMode == "edit" && intRowHandles.Length >= 1 && intPermissionDocumentID > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 4)  // Linked Documents //
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;

                    GridView viewParent = (GridView)gridControl1.MainView;
                    int[] intRowHandlesParent;
                    intRowHandlesParent = viewParent.GetSelectedRows();
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && strFormMode == "edit" && intPermissionDocumentID > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intPermissionDocumentID > 0 && strFormMode == "edit" && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intPermissionDocumentID > 0 && strFormMode == "edit" && intRowHandles.Length > 0);

            // Set enabled status of GridView6 navigator custom buttons //
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && strFormMode == "edit" && intPermissionDocumentID > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowDelete && intPermissionDocumentID > 0 && strFormMode == "edit" && intRowHandles.Length > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intPermissionDocumentID > 0 && intRowHandles.Length == 1);

            // Set enabled status of GridView4 navigator custom buttons //
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && strFormMode == "edit" && intPermissionDocumentID > 0);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowDelete && intPermissionDocumentID > 0 && strFormMode == "edit" && intRowHandles.Length > 0);
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intPermissionDocumentID > 0 && intRowHandles.Length == 1);
        }

        private void frm_UT_Permission_Doc_Edit_UKPN_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 99)  // Refresh Top Level data //
                {
                    Load_Top_Level();
                }
                else if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Top_Level();  // Need to reload this just in case the equipment linked to jobs has changed so the PD level calculated columns for equipment refreshes //
                    Load_Permissioned_Work();
                }
                else if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs6))
                {
                    Load_Linked_Maps();
                }
                else if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDs4))
                {
                    Load_Linked_Documents();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Permission_Doc_Edit_UKPN_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs6, string strNewIDs4)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs6 != "") i_str_AddedRecordIDs6 = strNewIDs6;
            if (strNewIDs4 != "") i_str_AddedRecordIDs4 = strNewIDs4;
        }

        public void Set_All_Linked_Actions_As_Permissioned()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intCount = view.DataRowCount;
            view.BeginUpdate();
            for (int i = 0; i < intCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "PermissionStatusID")) != 2) view.SetRowCellValue(i, "PermissionStatusID", 1);  // 1 = Permissioned only if not refused //
            }
            view.EndUpdate();
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07389UTPDItemBindingSource.EndEdit();
            try
            {
                this.sp07389_UT_PD_ItemTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["PermissionDocumentID"]) + ";";
                    this.strRecordIDs = Convert.ToInt32(currentRow["PermissionDocumentID"]) + ",";
                }

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            sp07392UTPDLinkedWorkBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            try  // Attempt to save any changes to Linked Permissioned Work... //
            {
                sp07392_UT_PD_Linked_WorkTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked Permissioned Work record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            Load_Permissioned_Work();  // Reload Data //

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // Pick up Unique Map IDs from actions to pass to Mapping if open so Thematics acan be updated if neccessary //
            string strSelectedMapIDs = "";
            string strMapID = "";
            foreach (DataRow dr in dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work.Rows)
            {
                strMapID = dr["MapID"].ToString();
                if (!strSelectedMapIDs.Contains(strMapID)) strSelectedMapIDs += strMapID + ",";
            }        

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Permission_Document_Manager")
                    {
                        var fParentForm = (frm_UT_Permission_Document_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "");
                    }
                    else if (frmChild.Name == "frm_UT_Survey_Pole2" && this.strCaller == "frm_UT_Survey_Pole2")
                    {
                        var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                        fParentForm.UpdateFormRefreshStatus(27, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", strNewIDs, "");
                    }
                    else if (frmChild.Name == "frm_UT_Mapping")  // Mapping Form //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.RefreshMapObjects(strSelectedMapIDs, 1);
                    }
                    else if (frmChild.Name == "frm_UT_Team_WorkOrder_Edit")
                    {
                        var fParentForm = (frm_UT_Team_WorkOrder_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(11, "", "");
                    }
                    else if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(3, "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            gridView2.PostEditor();
            
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07389_UT_PD_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intWorkNew = 0;
            int intWorkModified = 0;
            int intWorkDeleted = 0;
            this.sp07392UTPDLinkedWorkBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intWorkNew++;
                        break;
                    case DataRowState.Modified:
                        intWorkModified++;
                        break;
                    case DataRowState.Deleted:
                        intWorkDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intWorkNew > 0 || intWorkModified > 0 || intWorkDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                if (intWorkNew > 0) strMessage += Convert.ToString(intWorkNew) + " New linked Permission Work record(s)\n";
                if (intWorkModified > 0) strMessage += Convert.ToString(intWorkModified) + " Updated linked Permission Work record(s)\n";
                if (intWorkDeleted > 0) strMessage += Convert.ToString(intWorkDeleted) + " Deleted linked Permission Work record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void Load_Company_Image()
        {
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;
            string strImagePath = currentRow["ImagePath"].ToString();
            if (string.IsNullOrEmpty(strImagePath))
            {
                pictureEdit1.Image = null;
                return;
            }
            try
            {
                pictureEdit1.Image = Image.FromFile(strImagePath);
            }
            catch (Exception ex)
            {
                pictureEdit1.Image = null;
                return;
            }
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    Load_Company_Image();

                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();

                    Set_Warning_Label_Status();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private void Set_Warning_Label_Status()
        {
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;
            string PDFFile = (string.IsNullOrEmpty(currentRow["PDFFile"].ToString()) ? "" : currentRow["PDFFile"].ToString());
            ItemForWarningLabel.Visibility = (string.IsNullOrWhiteSpace(PDFFile) ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
        }

        #endregion


        #region Editors

        private void LandOwnerNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
                if (currentRow == null) return;
                string strOwnerName = (string.IsNullOrEmpty(currentRow["OwnerName"].ToString()) ? "" : currentRow["OwnerName"].ToString());
                frm_UT_Select_Land_Owner fChildForm = new frm_UT_Select_Land_Owner();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._PassedInOwnerName = strOwnerName;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["OwnerName"] = fChildForm._SelectedName;
                    currentRow["OwnerAddress"] = fChildForm._SelectedAddress;
                    currentRow["OwnerSalutation"] = fChildForm._SelectedSalutation;
                    currentRow["OwnerPostcode"] = fChildForm._SelectedPostcode;
                    currentRow["OwnerTelephone"] = fChildForm._SelectedTelephone;
                    currentRow["OwnerEmail"] = fChildForm._SelectedEmail;
                    sp07389UTPDItemBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "unknown")  // Unknown Button //
            {
                DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
                if (currentRow == null) return;
                currentRow["OwnerName"] = "Unknown";
                sp07389UTPDItemBindingSource.EndEdit();
            }
        }
        private void LandOwnerNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(OwnerNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(OwnerNameButtonEdit, "");
            }
        }

        private void DateRaisedDateEdit_Validating(object sender, CancelEventArgs e)
        {
            // Following commented out since this field is not visible on the layout by default //
            /*DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DateRaisedDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DateRaisedDateEdit, "");
            }*/
        }

        private void SignatureFileHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strFile = SignatureFileHyperLinkEdit.Text.ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strFilePath = strSignaturePath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                System.Diagnostics.Process.Start(strFilePath);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void PDFFileHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strFile = PDFFileHyperLinkEdit.Text.ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strFilePath = strPermissionDocumentPath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                //System.Diagnostics.Process.Start(strFilePath);

                frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                fChildForm.strPDFFile = strFilePath;
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.Show();

            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void TreeReplacementNoneCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            /*CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                TreeReplacementWhipsSpinEdit.Properties.ReadOnly = true;
                TreeReplacementWhipsSpinEdit.EditValue = 0;
                TreeReplacementStandardsSpinEdit.Properties.ReadOnly = true;
                TreeReplacementStandardsSpinEdit.EditValue = 0;
            }
            else
            {
                TreeReplacementWhipsSpinEdit.Properties.ReadOnly = false;
                TreeReplacementStandardsSpinEdit.Properties.ReadOnly = false;
            }*/
        }

        private void StumpTreatmentNoneCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            /*CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                StumpTreatmentEcoPlugsSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentEcoPlugsSpinEdit.EditValue = 0;
                StumpTreatmentSprayingSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentSprayingSpinEdit.EditValue = 0;
                StumpTreatmentPaintSpinEdit.Properties.ReadOnly = true;
                StumpTreatmentPaintSpinEdit.EditValue = 0;
            }
            else
            {
                StumpTreatmentEcoPlugsSpinEdit.Properties.ReadOnly = false;
                StumpTreatmentSprayingSpinEdit.Properties.ReadOnly = false;
                StumpTreatmentPaintSpinEdit.Properties.ReadOnly = false;
            }*/
        }

        private void PostageRequiredCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;

            int intPostageRequired = 0;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow != null)
            {
                intPostageRequired = (currentRow["PostageRequired"] == null ? 0 : Convert.ToInt32(currentRow["PostageRequired"]));
            }
            bbiPostedToCustomer.Enabled = ce.Checked && intPostageRequired == 1;
        }


        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Permissioned Work NOT Shown When Block Adding and Block Editing" : "No Linked Permissioned Work Available - Click the Add button to Permissioned Work");
                    break;
                case "gridView6":
                    message = "No Linked Maps Available - Click the add button to add Linked Maps";
                    break;
                case "gridView4":
                    message = "No Linked Documents Available - Select Add to create a Linked Documents";
                    break;
                case "gridView5":
                    message = "No Linked Maps Available - Maps are created from the Surveyed Pole Screen or the Utilities Mapping screen";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                if (!iBool_AllowEdit) return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditWork_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "edit")  // Edit Job Button //
            {
                GridView view = view = (GridView)gridControl1.MainView;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                string strRecordIDs = view.GetFocusedRowCellValue("ActionID").ToString() + ',';
                Edit_Job(strRecordIDs);
            }
        }

        private void repositoryItemGridLookUpEditStatus_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (Convert.ToInt32(e.NewValue) == 2)
            {
                GridView view = (GridView)gridControl1.MainView;
                int intActionPermissionID = (view.GetFocusedRowCellValue("ActionPermissionID") == null ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("ActionPermissionID")));
                string strWorkAgreed = (view.GetFocusedRowCellValue("JobDescription") == null ? "" : view.GetFocusedRowCellValue("JobDescription").ToString());
                string strPoleNumber = (view.GetFocusedRowCellValue("PoleNumber") == null ? "" : view.GetFocusedRowCellValue("PoleNumber").ToString());

                DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
                if (currentRow == null) return;
                string ReferenceNumber = (currentRow["ReferenceNumber"] == null ? "" : currentRow["ReferenceNumber"].ToString());

                frm_UT_Permission_Doc_Refuse_Permission fChildForm = new frm_UT_Permission_Doc_Refuse_Permission();
                fChildForm._PassedInReferenceNumber = ReferenceNumber;
                fChildForm._PassedInPoleNumber = strPoleNumber;
                fChildForm._PassedInAgreedWork = strWorkAgreed;
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() != DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
                else
                {
                    view.SetFocusedRowCellValue("RefusalReasonID", fChildForm._SelectedReasonID);
                    view.SetFocusedRowCellValue("RefusalRemarks", fChildForm._SelectedRemarks);
                    view.SetFocusedRowCellValue("RefusalReason", fChildForm._SelectedReason);
                    e.Cancel = false;
                    return;
                }
            }
            else
            {
                GridView view = (GridView)gridControl1.MainView;
                int RefusalReasonID = (view.GetFocusedRowCellValue("RefusalReasonID") == null ? 0 : Convert.ToInt32(view.GetFocusedRowCellValue("RefusalReasonID")));
                string RefusalRemarks = (view.GetFocusedRowCellValue("RefusalRemarks") == null ? "" : view.GetFocusedRowCellValue("RefusalRemarks").ToString());
                string RefusalReason = (view.GetFocusedRowCellValue("RefusalReason") == null ? "" : view.GetFocusedRowCellValue("RefusalReason").ToString());
                if (RefusalReasonID > 0)
                {
                    view.SetFocusedRowCellValue("RefusalReasonID", 0);
                    view.SetFocusedRowCellValue("RefusalRemarks", "");
                    view.SetFocusedRowCellValue("RefusalReason", "");
                    e.Cancel = true;
                    return;
                }
            }
        }


        #endregion


        #region GridView6

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Linked_Maps();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControl6.MainView;
                        int[] intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one map to view before proceeding.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Linked Map available for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = System.IO.Path.Combine(strMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();

                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView5

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Maps();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void repositoryItemHyperLinkEditMapPath_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            string strMap = (string.IsNullOrWhiteSpace(view.GetFocusedRowCellValue("MapPath").ToString()) ? "" : view.GetFocusedRowCellValue("MapPath").ToString());
            if (string.IsNullOrEmpty(strMap))
            {
                XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strMap += ".gif";
            string strImagePath = System.IO.Path.Combine(strMapPath, strMap);

            frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
            frm_preview.strImage = strImagePath;
            frm_preview.ShowDialog();
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;
            int intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
            int intClientID = (currentRow["ClientID"] == null ? 0 : Convert.ToInt32(currentRow["ClientID"]));
            string strOwnerName = (currentRow["OwnerName"] == null ? "" : currentRow["OwnerName"].ToString());

            if (intPermissionDocumentID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document must be saved before adding Linked Work to be Permissioned.", "Add Linked Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Check if the screen contains pending changes - if yes, prompt user to save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before adding further Permissioned Work to this Permission Document!\n\nSave Changes?", "Add Permissioned Work to Permission Document", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (!boolProceed) return;

            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Jobs //
                    {
                        GridView view = (GridView)gridControl1.MainView; ;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        string strExcludedActionIDs = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludedActionIDs += view.GetRowCellValue(i, "ActionID").ToString() + ",";
                        }

                        frm_UT_Permission_Document_Add_Work fChildForm = new frm_UT_Permission_Document_Add_Work();
                        fChildForm._PassedInExcludedActionIDs = strExcludedActionIDs;
                        fChildForm._PassedInClientID = intClientID;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            string strActionIDs = fChildForm._SelectedActionIDs;
                            if (string.IsNullOrEmpty(strActionIDs)) return;

                            // Add the jobs to the Permisison Document then reload the whole document //
                            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddWorkToPermissionDocument = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                            AddWorkToPermissionDocument.ChangeConnectionString(strConnectionString);
                            try
                            {
                                Convert.ToInt32(AddWorkToPermissionDocument.sp07243_UT_Add_Work_To_Permission_Document(intPermissionDocumentID, strActionIDs));
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add work to permission document - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Add Work to Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return;
                            }

                            Load_Top_Level();  // Need to reload this just in case the equipment linked to jobs has changed so the PD level calculated columns for equipment refreshes //
                            Load_Permissioned_Work();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                        }
                        break;
                    }
                case 6:  // Linked Map //
                    {
                        OpenMapping();
                    }
                    break;
                case 4:  // Linked Documents //
                    {
                        if (!iBool_AllowAdd) return;

                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        GridView view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 53;  // Permission Document //

                        fChildForm.intLinkedToRecordID = intPermissionDocumentID;
                        string strDate = (currentRow["DateRaised"] == null ? "" : Convert.ToDateTime(currentRow["DateRaised"]).ToString("dd/MM/yyyy"));
                        fChildForm.strLinkedToRecordDesc = strOwnerName + " - Date: " + strDate;

                        fChildForm.Show();

                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

            }
        }

        private void Block_Edit_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Linked Documents //
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl4.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.intRecordTypeID = 53;  // Permission Document //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Tree Work //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        Edit_Job(strRecordIDs);
                    }
                    break;
                case 4:  // Linked Documents //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl4.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                    }
                    this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                    frm_AT_Linked_Document_Edit fChildForm = new frm_AT_Linked_Document_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.fProgress = fProgress;
                    fChildForm.intRecordTypeID = 53;  // Permission Document //
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
        }

        private void Edit_Job(string strRecordIDs)
        {
            // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before editing any job information.\n\nSave Changes?", "Edit Work Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (!boolProceed) return;

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
            frmInstance.MdiParent = this.MdiParent;
            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.strRecordIDs = strRecordIDs;
            frmInstance.strFormMode = "edit";
            frmInstance.strCaller = this.Name;
            frmInstance.intRecordCount = 0;
            frmInstance.FormPermissions = this.FormPermissions;
            frmInstance.strDefaultPath = strPicturePath;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();
            frmInstance.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmInstance, new object[] { null });
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Jobs //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Work Permissions to delete by clicking on them then try again.", "Delete Linked Work Permissions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Work Permission" : Convert.ToString(intRowHandles.Length) + " Linked Work Permissions") + " selected for DELETING!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Work Permission" : "these Linked Work Permissions") + " will be DELETED and will no longer be available for selection.\n\nNote: You are deleting the Work Permission, NOT the Work!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "DELETE Work Permission", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for deleting.\n\nIMPORTANT NOTE: You must save changes to this permission document before the marked work permission records are physically deleted.", "DELETE Work Permission", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 6:  // Linked Maps //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Work Maps to delete by clicking on them then try again.", "Delete Linked Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Work Map" : Convert.ToString(intRowHandles.Length) + " Linked Work Map") + " selected for DELETING!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Work Map" : "these Linked Work Map") + " will be DELETED and will no longer be available for selection.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "DELETE Work Map", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageThumbPath = "";
                            string strImagePath = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedMapID")) + ",";

                                strImageThumbPath = strMapPath + view.GetRowCellValue(intRowHandle, "DocumentPath").ToString() + "_thumb" + ".jpg";
                                strImagePath = strMapPath + view.GetRowCellValue(intRowHandle, "DocumentPath").ToString() + ".gif";

                                try  // Remove the physical map files //
                                {
                                    System.IO.File.Delete(strImageThumbPath);
                                    System.IO.File.Delete(strImagePath);
                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("map", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Maps();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 4:  // Linked Documents //
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl4.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                        }

                        DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Linked_Documents();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }
        }

        public void Load_Top_Level()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            // Refresh main data since it has been changed by the Create Permission Document screen [may have a signature file and a physical PDF document] //
            try
            {
                sp07389_UT_PD_ItemTableAdapter.Fill(dataSet_UT_WorkOrder.sp07389_UT_PD_Item, strRecordIDs, strFormMode, strImagePath);
            }
            catch (Exception)
            {
            }

            // Check if we need to adjust the status of the Email and Postage buttons //
            string strSavedPDFFile = "";
            int intPostageRequired = 0;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow != null)
            {
                strSavedPDFFile = (currentRow["PDFFile"] == null ? "" : currentRow["PDFFile"].ToString());
                intPostageRequired = (currentRow["PostageRequired"] == null ? 0 : Convert.ToInt32(currentRow["PostageRequired"]));
            }
            bbiEmailToCustomer.Enabled = iBool_AllowAdd && !string.IsNullOrEmpty(strSavedPDFFile);
            bbiPostedToCustomer.Enabled = iBool_AllowAdd && !string.IsNullOrEmpty(strSavedPDFFile) && intPostageRequired == 1;
        }

        public void Load_Permissioned_Work()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            this.RefreshGridViewState1.SaveViewInfo();  // Store expanded groups and selected rows //
            gridControl1.BeginUpdate();
            try
            {
                sp07392_UT_PD_Linked_WorkTableAdapter.Fill(dataSet_UT_WorkOrder.sp07392_UT_PD_Linked_Work, strRecordIDs);
            }
            catch (Exception)
            {
            }
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();
            FilterGrids();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionPermissionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void Load_Linked_Maps()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Maps //
            gridControl6.MainView.BeginUpdate();
            this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
            sp07202_UT_Mapping_Snapshots_Linked_SnapshotsTableAdapter.Fill(dataSet_UT_Mapping.sp07202_UT_Mapping_Snapshots_Linked_Snapshots, strRecordIDs, 1);  // 1 = Permission Doc, 2 = Work Order //
            this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl6.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs6 != "")
            {
                strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedMapID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }

        private void Load_Linked_Documents()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Maps //
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState4.SaveViewInfo();  // Store expanded groups and selected rows //
            sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, strRecordIDs, 53, strLinkedDocumentPath);
            this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl4.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs4 != "")
            {
                strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedMapID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs4 = "";
            }
        }

        private void Load_Maps()
        {
            gridControl5.BeginUpdate();
            try
            {
                sp07402_UT_PD_Linked_Access_MapsTableAdapter.Fill(dataSet_UT_WorkOrder.sp07402_UT_PD_Linked_Access_Maps, strRecordIDs);
            }
            catch (Exception)
            {
            }
            gridControl5.EndUpdate();
        }

        public void FilterGrids()
        {
            string strPermissionDocumentID = "";
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow != null) strPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? "0" : currentRow["PermissionDocumentID"].ToString());

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[PermissionDocumentID] = " + Convert.ToString(strPermissionDocumentID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionPermissionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }


        public void OpenMapping()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;
            int intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
            string strDocumentDescription = currentRow["OwnerName"].ToString() + " - Date: " + Convert.ToDateTime(currentRow["DateRaised"]).ToString("dd/MM/yyyy");
            if (intPermissionDocumentID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document must be saved before adding Linked Work to be Permissioned.", "Add Linked Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = (GridView)gridControl1.MainView;
            int intRowCount = view.RowCount;  // Not using DataRowCount as we have a filtered grid and we don't want any other permission document's work //
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You must add at least one piece of work before proceeding.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get Unique list of Survey IDs //
            string strSurveyIDs = ",";
            string strCurrentID = "";
            string strTreeIDs = ",";
            string strTreeID = "";
            string strPoleIDs = ",";
            string strPoleID = "";
            for (int i = 0; i < view.RowCount; i++)
            {
                strCurrentID = view.GetRowCellValue(i, "SurveyID").ToString();
                if (!(string.IsNullOrEmpty(strCurrentID) || strCurrentID == "0"))
                {
                    if (!strSurveyIDs.Contains("," + strCurrentID + ",")) strSurveyIDs += strCurrentID + ",";
                }
                strTreeID = view.GetRowCellValue(i, "TreeID").ToString();
                if (!(string.IsNullOrEmpty(strTreeID) || strTreeID == "0"))
                {
                    if (!strTreeIDs.Contains("," + strTreeID + ",")) strTreeIDs += strTreeID + ",";
                }
                strPoleID = view.GetRowCellValue(i, "PoleID").ToString();
                if (!(string.IsNullOrEmpty(strPoleID) || strPoleID == "0"))
                {
                    if (!strPoleIDs.Contains("," + strPoleID + ",")) strPoleIDs += strPoleID + ",";
                }
            }
            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strSurveyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Survey - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strTreeIDs = strTreeIDs.TrimStart(',');
            if (strTreeIDs.Length < 2)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Tree - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intSelectedSurveyID = Convert.ToInt32(strArray[0]);

            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(Convert.ToInt32(strArray[0])).ToString();
                delimiters = new char[] { '|' };
                string[] strArray2 = null;
                strArray2 = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray2.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray2[0]);
                    strWorkspaceName = strArray2[1].ToString();
                    intRegionID = Convert.ToInt32(strArray2[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is linked to a Survey, but the survey is not linked to a region with a Mapping Workspace - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Close any open instance of the mapping //
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Close();
                    }
                }
            }
            catch (Exception)
            {
            }

            // Open Map and set mode to Map Snapshot and load in trees with work on this Permission Document //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSelectedSurveyID;
            frmInstance._SurveyMode = "PermissionDocumentMap";
            frmInstance._PassedInRecordType = 1;  // 1 = Permision Document, 2 = Work Order //
            frmInstance._PassedInTreeIDs = strTreeIDs;
            frmInstance._PassedInPoleIDs = strPoleIDs;
            frmInstance._PassedInPermissionDocumentID = intPermissionDocumentID;
            frmInstance._PassedInPermissionDocumentDescription = strDocumentDescription;
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
            frmInstance.Scale_Map_To_Visible_Objects(false);
        }

        private void EmergencyAccessMemoExEdit_Enter(object sender, EventArgs e)
        {
            this.scSpellChecker.Check(EmergencyAccessMemoExEdit);

        }
        private void strRemarksMemoEdit_Enter(object sender, EventArgs e)
        {
            //this.scSpellChecker.ParentContainer = strRemarksMemoEdit;
            this.scSpellChecker.Check(strRemarksMemoEdit);
        }

        private void bbiAddWorkToPermission_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            i_int_FocusedGrid = 1;
            Add_Record();
        }

        private void bbiCreatePermissionDocument_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;
            int intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
            if (intPermissionDocumentID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document must be saved before creating a Permission Document File.", "Create Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // See if this is already open - if yes activate it otherwise open it //

            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Permission_Doc_Create_Document")
                    {
                        var fParentForm = (frm_UT_Permission_Doc_Create_Document)frmChild;
                        if (fParentForm._intPermissionDocumentID == intPermissionDocumentID)
                        {
                            fParentForm.Activate();
                            return;
                        }
                    }
                }
            }

            // Form wasn't open, so open it //
            string strPDFFile = (currentRow["PDFFile"] == null ? "" : currentRow["PDFFile"].ToString());

            // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before creating the Permission Document File!\n\nSave Changes?", "Create Permission Document", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (boolProceed)
            {
                frm_UT_Permission_Doc_Create_Document fChildForm = new frm_UT_Permission_Doc_Create_Document();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.strSignaturePath = strSignaturePath;
                fChildForm.strPermissionDocumentPath = strPermissionDocumentPath;
                fChildForm.strPicturePath = strPicturePath;
                fChildForm.strMapPath = strMapPath;
                fChildForm._strExistingPDFFileName = strPDFFile;

                fChildForm._intPermissionDocumentID = intPermissionDocumentID;
                fChildForm.frmCallingEditPermissionDocFormUKPN = this;  // Pass this form as a reference to the Print screen so we can get back to it easily or activate it's Create Maps functionality //
                fChildForm._CallingFormType = 2;  // 1 = WPD, 2 = UKPN //
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }
        }

        private void bbiCreateMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenMapping();
        }

        private void bbiEmailToCustomer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;

            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }

            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;

            string strPermissionDocumentRef = currentRow["ReferenceNumber"].ToString();
            if (String.IsNullOrWhiteSpace(strPermissionDocumentRef)) strPermissionDocumentRef = "";

            int intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
            if (intPermissionDocumentID == 0)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document must be saved and the PermissionDocument File must be created before emailing to the customer.", "Email Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strPDFFile = (currentRow["PDFFile"] == null ? "" : currentRow["PDFFile"].ToString());

            if (string.IsNullOrEmpty(strPDFFile))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document file must be created before emailing to the customer.", "Email Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strPDFFile = strPermissionDocumentPath + strPDFFile;
            string strEmailAddresses = (currentRow["OwnerEmail"] == null ? "" : currentRow["OwnerEmail"].ToString());
            if (string.IsNullOrWhiteSpace(strEmailAddresses))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to email Permission Document, no email address stored for recipient.", "Email Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Get DB Settings for generated Emails //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp07216_UT_Get_Email_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            int intParameter = 0;  // 0 = Customer, 1 = Client //
            cmd.Parameters.Add(new SqlParameter("@RecipientTypeID", intParameter));
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Email Settings (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Email Settings (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strEmailBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (Email Layout File, From Email Address and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //

                msg.Subject = strEmailSubjectLine + (string.IsNullOrWhiteSpace(strPermissionDocumentRef) ? "" : " - " + strPermissionDocumentRef);
                if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                msg.Priority = System.Net.Mail.MailPriority.High;
                msg.IsBodyHtml = true;

                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                // Create a new attachment //
                System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFFile);  // create the attachment //
                msg.Attachments.Add(mailAttachment);

                //create the LinkedResource (embedded image)
                System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                logo.ContentId = "companylogo";
                //add the LinkedResource to the appropriate view
                htmlView.LinkedResources.Add(logo);

                //create the LinkedResource (embedded image)
                System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                logo2.ContentId = "companyfooter";
                //add the LinkedResource to the appropriate view
                htmlView.LinkedResources.Add(logo2);

                msg.AlternateViews.Add(plainView);
                msg.AlternateViews.Add(htmlView);

                object userState = msg;
                System.Net.Mail.SmtpClient emailClient = null;
                if (intSMTPMailServerPort != 0)
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                }
                else
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                }
                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                {
                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = basicCredential;
                }
                emailClient.SendAsync(msg, userState);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Email Permission Document - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Email Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Update DB (store sent date against permission document) //
            DateTime dtNow = DateTime.Now;
            DataSet_UT_EditTableAdapters.QueriesTableAdapter UpdatePermissionDocumentRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            UpdatePermissionDocumentRecord.ChangeConnectionString(strConnectionString);
            try
            {
                UpdatePermissionDocumentRecord.sp07209_UT_Permission_Document_Update_Path(2, intPermissionDocumentID, "", dtNow, GlobalSettings.UserID);

                // Update screen with date also //
                currentRow["EmailedToCustomerDate"] = dtNow;
                currentRow["PermissionEmailed"] = 1;
                currentRow["SentByStaffID"] = GlobalSettings.UserID;
                currentRow["SentByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
                sp07389UTPDItemBindingSource.EndEdit();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Permission Document Emailed Date - an error occurred while updating the Permission Document Record [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Update Permission Document Emailed Date", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            DevExpress.XtraEditors.XtraMessageBox.Show("Permission Document emailed successfully.", "Email Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bbiPostedToCustomer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;

            DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
            if (currentRow == null) return;

            string strPDFFile = (currentRow["PDFFile"] == null ? "" : currentRow["PDFFile"].ToString());
            if (string.IsNullOrEmpty(strPDFFile))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document file must be created before posting to the customer.", "Post Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            currentRow["EmailedToCustomerDate"] = DateTime.Now;
            currentRow["SentByPost"] = 1;
            currentRow["SentByStaffID"] = GlobalSettings.UserID;
            currentRow["SentByStaffName"] = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
            sp07389UTPDItemBindingSource.EndEdit();
        }

        private void SignatureFileButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "capture")  // Capture Button //
            {
                DataRowView currentRow = (DataRowView)sp07389UTPDItemBindingSource.Current;
                if (currentRow == null) return;

                int intPermissionDocumentID = (currentRow["PermissionDocumentID"] == null ? 0 : Convert.ToInt32(currentRow["PermissionDocumentID"]));
                if (intPermissionDocumentID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("The Permission Document must be saved before capturing a signature.", "Capture Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                string strOwnerName = (string.IsNullOrEmpty(currentRow["OwnerName"].ToString()) ? "" : currentRow["OwnerName"].ToString());
                frm_Core_Signature_Capture fChildForm = new frm_Core_Signature_Capture();
                fChildForm._PersonsNameRequired = false;
                fChildForm._PersonsNameVisible = true;
                fChildForm._PassedInName = strOwnerName;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User clicked OK on child form //
                {
                    Image returnedSignatureImage = fChildForm._returnedImage;
                    string returnedPersonsName = fChildForm._returnedPersonsName;
                    if (!string.IsNullOrWhiteSpace(returnedPersonsName))
                    {
                        currentRow["OwnerName"] = returnedPersonsName;
                    }
                    // Save image and update permission record //
                    try
                    {
                        string strSaveFileName = strSignaturePath;
                        if (!strSaveFileName.EndsWith("\\")) strSaveFileName += "\\";
                        strSaveFileName += "Signature_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + intPermissionDocumentID.ToString() + ".jpg";
                        returnedSignatureImage.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

                        strSaveFileName = System.IO.Path.GetFileName(strSaveFileName);  // Change to filename without path //
                        currentRow["SignatureFile"] = strSaveFileName;
                        currentRow["SignatureDate"] = DateTime.Now;
                        //DataSet_UT_EditTableAdapters.QueriesTableAdapter SavePicture = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                        //SavePicture.ChangeConnectionString(strConnectionString);
                        //SavePicture.sp07209_UT_Permission_Document_Update_Path(1, intPermissionDocumentID, strSaveFileName, DateTime.Now, GlobalSettings.UserID);
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while saving the captured signature - error [" + ex.Message + "].\n\nPlease try again - if the problems persists, contact Technical Support", "Capture Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    sp07389UTPDItemBindingSource.EndEdit();

                }
            }
            if (e.Button.Tag.ToString() == "view")  // View Button //
            {
                string strFile = SignatureFileButtonEdit.Text.ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strSignaturePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    System.Diagnostics.Process.Start(strFilePath);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }


        }





    }
}


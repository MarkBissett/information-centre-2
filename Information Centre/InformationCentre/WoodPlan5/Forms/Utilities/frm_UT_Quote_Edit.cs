﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Xml;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraCharts;
using DevExpress.XtraGauges.Core.Model;
using DevExpress.XtraGauges.Core.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Quote_Edit: BaseObjects.frmBase
    {
        public frm_UT_Quote_Edit()
        {
            InitializeComponent();            
        }

        private void frm_UT_Quote_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 1000201;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            // connect adapters //
            sp07418_UT_Quote_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07421_UT_Quote_Item_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07424_UT_Quote_Category_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07424_UT_Quote_Category_Item_AddModeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07427_UT_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07431_UT_Client_Contract_Type_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07435_UT_Item_Units_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07438_UT_Resource_Rate_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07446_UT_Picklist_By_Header_IDTableAdapter.Connection.ConnectionString = strConnectionString;
            //Populate picklist Data
            PopulatePickList();

            editFormDataLayoutControlGroup.BeginUpdate();
            switch (strFormMode.ToLower())
            {
                case "add":
                    lcgMain.Text = "Create Quote";
                    qstage = QuoteStage.Create;
                   // addOnlyControlVisible(true);
                    lueBillingCentreCodeID.Enabled = false;
                    cmbQuoteCategoryID.Enabled = false;
                    cmbResourceRateID.Enabled = false;
                    bbiSave.Enabled = false;
                    bbiFormAddAnother.Enabled = false;
                    lueBillingCentreCodeID.Focus();
                    addNewRow(FormMode.add);
                    bbiSaveReview.Enabled = true;
                    break;
                case "blockadd":
                    addNewRow(FormMode.blockadd);
                    break;
                case "blockedit":
                    addNewRow(FormMode.blockedit);
                    this.dataSet_UT_Quote.sp07418_UT_Quote_List.Rows[0].AcceptChanges();
                    break;
                case "edit":
                case "view":

                    lcgMain.Text = "Review Quote";
                    qstage = QuoteStage.Review;

                    bbiSaveReview.Enabled = false;
                   // addOnlyControlVisible(false);
                    try
                    {
                        loadQuote();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
            }
            lockReadonlyFields();
            editFormDataLayoutControlGroup.EndUpdate();
            lcgMain.Selected = true;


            if (strFormMode == "view")  // Disable all controls //
            {
               editFormDataLayoutControlGroup.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            PostOpen(null);
        }

        #region Instance Variables

        bool ibool_FormEditingCancelled = false;
        bool blnBulkAdding = false;
        public enum QuoteStage { Create, Review }
        private QuoteStage qstage;
        bool forceFormClose = false;
        public FormMode formMode;
        string strFilterList;
        public int intQuoteID = 0;
        private string strClientContractFilter = "";
        private string strResourceRateFilter = "";
        public int intWorkOrderNum = 0;
        public string strPaddedWorkOrderNum;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        bool bool_FormLoading = true;
        public bool editorDataChanged = false;
        public bool quoteItemChanged = false;
        public enum FormMode { add, edit, view, delete, blockadd, blockedit };
        public enum SentenceCase { Upper, Lower, Title, AsIs }
        int intTempQuoteItem = -1;

        #endregion
        
        #region Validate Method

        private bool validateTextBox(TextEdit txtBox, SentenceCase SentenceCase)
        {
            if (bool_FormLoading)
                return true;
            if (forceFormClose)
                return true;

            bool valid = true;
            string errorMessage = "Please enter a valid value.";

            /*
            string originalText = txtBox.Text;
            string parsedText = Regex.Replace(txtBox.Text, "[^a-z_A-Z0-9]", "");
            //check non alphanumeric characters
            if (originalText != parsedText || txtBox.Text == "")
            {
                dxErrorProvider.SetError(txtBox, "Please enter a valid text, avoid entering non alphanumeric characters, invalid characters have been removed");
                return valid;
            }
            else
            {
                valid = true;
            }
            */
            if (txtBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        txtBox.Text = txtBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        txtBox.Text = txtBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        txtBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtBox.Text);
                        break;
                }
            }

            //check empty text box after PO has been created
            if (txtBox.Text == "" && ceTransferredExchequer.CheckState == CheckState.Checked)
            {
                dxErrorProvider.SetError(txtBox, errorMessage);
                valid = false;
                return valid;
            }

            if (valid)
            {
                dxErrorProvider.SetError(txtBox, "");
            }
            return valid;
        }

        private bool validateSpinEdit(SpinEdit spnEdit)
        {
            if (bool_FormLoading)
                return true;
            if (forceFormClose)
                return true;

            bool valid = true;
            string ErrorText = "Please Enter Value.";

            if (formMode == FormMode.blockedit && spnEdit.EditValue == null)
            {
                dxErrorProvider.SetError(spnEdit, "");
                return true;
            }

            if ((spnEdit.EditValue == null || spnEdit.EditValue.ToString() == "") && formMode != FormMode.blockedit)
            {
                dxErrorProvider.SetError(spnEdit, ErrorText);
                return valid = false;  // Show stop icon as field is invalid //
            }
            else
            {
                dxErrorProvider.SetError(spnEdit, "");
            }

            return valid;
        }

        private bool validateDateEdit(DateEdit dateEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;

            if (formMode == FormMode.blockedit)
            {
                dxErrorProvider.SetError(dateEdit, "");
                return true;
            }

            string ErrorText = "Please Enter a Valid Date.";

            if (dateEdit.Tag == null ? false : true)
            {
                ErrorText = "Please Enter a valid " + dateEdit.Tag.ToString() + ".";
            }


            if (dateEdit.DateTime == DateTime.Parse("01/01/0001"))
            {
                valid = false;
            }

            if (valid)
            {
                dxErrorProvider.SetError(dateEdit, "");
            }
            else
            {
                dxErrorProvider.SetError(dateEdit, ErrorText);
            }
            return valid;
        }

        private bool validateSearchLookupEdit(SearchLookUpEdit searchLookUpEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;

            string errorText = "Please Select Value.";

            if (searchLookUpEdit.Tag == null ? false : true)
            {
                errorText = String.Format("Please Select {0}.", searchLookUpEdit.Tag);
            }

            if (formMode == FormMode.blockedit || searchLookUpEdit.Visible == false)
            {
                dxErrorProvider.SetError(searchLookUpEdit, "");
                return true;
            }

            if ( Convert.ToInt32(searchLookUpEdit.EditValue) == 0 || searchLookUpEdit.EditValue == null || searchLookUpEdit.EditValue.ToString() == "")
            {
                dxErrorProvider.SetError(searchLookUpEdit, errorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }

            if (valid)
            {
                dxErrorProvider.SetError(searchLookUpEdit, "");
            }
            else
            {
                dxErrorProvider.SetError(searchLookUpEdit, errorText);
            }
            return valid;
        }

        private bool validateLookupEdit(LookUpEdit lookupEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;

            string errorText = "Please Select Value.";

            if (lookupEdit.Tag == null ? false : true)
            {
                errorText = String.Format("Please Select {0}.", lookupEdit.Tag);
            }

            if (formMode == FormMode.blockedit || lookupEdit.Visible == false)
            {
                dxErrorProvider.SetError(lookupEdit, "");
                return true;
            }

            if (lookupEdit.EditValue == null || lookupEdit.EditValue.ToString() == "")
            {
                dxErrorProvider.SetError(lookupEdit, errorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }

            if (valid)
            {
                dxErrorProvider.SetError(lookupEdit, "");
            }
            else
            {
                dxErrorProvider.SetError(lookupEdit, errorText);
            }
            return valid;
        }

        private bool validateMemEdit(MemoEdit memBox, SentenceCase SentenceCase)
        {
            if (bool_FormLoading)
                return true;

            bool valid = false;
            if (memBox.Text != "")
            {
                switch (SentenceCase)
                {
                    case SentenceCase.Upper:
                        memBox.Text = memBox.Text.ToUpper();
                        break;
                    case SentenceCase.Lower:
                        memBox.Text = memBox.Text.ToLower();
                        break;
                    case SentenceCase.Title:
                        memBox.Text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(memBox.Text);
                        break;
                }
            }

            if (memBox.Visible == false)
            {
                return true;
            }

            string originalText = memBox.Text;
            //check empty field
            if (originalText == "")
            {
                dxErrorProvider.SetError(memBox, "Please enter a valid text.");
                return valid;
            }
            else
            {
                valid = true;
            }

            if (valid)
            {
                dxErrorProvider.SetError(memBox, "");
            }
            return valid;
        }

        private bool validateCheckedComboEdit(CheckedComboBoxEdit checkedComboBoxEdit)
        {
            bool valid = true;
            if (bool_FormLoading)
                return valid;
            if (forceFormClose)
                return valid;

            string errorText = "Please Select Value.";

            if (checkedComboBoxEdit.Tag == null ? false : true)
            {
                errorText = "Please Select " + checkedComboBoxEdit.Tag.ToString() + ".";
            }

            if (formMode == FormMode.blockedit || checkedComboBoxEdit.Visible == false)
            {
                dxErrorProvider.SetError(checkedComboBoxEdit, "");
                return true;
            }

            if (!chkComboChecked(checkedComboBoxEdit))
            {
                dxErrorProvider.SetError(checkedComboBoxEdit, errorText);
                valid = false;  // Show stop icon as field is invalid //
                return valid;
            }

            if (valid)
            {
                dxErrorProvider.SetError(checkedComboBoxEdit, "");
            }
            else
            {
                dxErrorProvider.SetError(checkedComboBoxEdit, errorText);
            }
            return valid;
        }

        #endregion

        #region Editor Events

        private void quoteItemDataNavigator_PositionChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            categoryValueChanged();
        }

        private void lueRating_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            sellRateChanged();
            buyRateChanged();
        }

        private void lueQuoteCategoryID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            filterItemUnits();
            categoryValueChanged();
        }

        private void categoryValueChanged()
        {
            if (lueQuoteCategoryID.Properties.GetDisplayText(lueQuoteCategoryID.EditValue) != "Other")
            {
                lockBuyFields(false);
                lockSellFields(false);
                hideMarkUPnTextRate(false);
            }
            else
            {
                lockBuyFields(true);
                lockSellFields(true);
                hideMarkUPnTextRate(true);
            }
        }
                
        private void lueBillingCentreCodeID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (formMode == FormMode.add)
            {
                cmbQuoteCategoryID.Enabled = true;
                cmbQuoteCategoryID.Focus();
            }
        }

        private void searchLookup_Validating(object sender, CancelEventArgs e)
        {
            if (validateSearchLookupEdit((SearchLookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lueItemUnitsID_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            try
            {
                if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
                {
                    if ("add".Equals(e.Button.Tag))
                    {
                        Open_Child_Forms("frm_UT_Utility_Units_Edit", FormMode.add, "frm_UT_Quote_Edit");
                        loadItemUnits(); 
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        loadItemUnits();
                        filterItemUnits();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Refreshing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void lueBillingCentreCodeID_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_UT_Billing_Centre_Edit", FormMode.add, "frm_UT_Quote_Edit");
                    RefreshBillingCentreCode();
                }
                else if ("refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshBillingCentreCode();
                }
            }
        }

        private void lueClientContractTypeID_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("add".Equals(e.Button.Tag)) // Add Make Button 
                {
                    Open_Child_Forms("frm_UT_Client_Contract_Edit", FormMode.add, "frm_UT_Quote_Edit");
                    RefreshResourceClientContractTypeList();
                }
                else if ("refresh".Equals(e.Button.Tag)) //Refresh Make List
                {
                    RefreshResourceClientContractTypeList();
                }
            }
        }
     
        private void cmbResourceRateID_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            try
            {
                if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
                {
                    if ("add".Equals(e.Button.Tag)) // Add Make Button 
                    {
                        Open_Child_Forms("frm_UT_Resource_Rate_Edit", FormMode.add, "frm_UT_Quote_Edit");
                        RefreshResourceRateList();
                        FilterResourceRate();
                        uncheckcmbResourceRateID();
                    }
                    else if ("refresh".Equals(e.Button.Tag)) //Refresh Resource Rate List
                    {
                        RefreshResourceRateList();
                        FilterResourceRate();
                        uncheckcmbResourceRateID();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Refreshing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void bbiSaveReview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EndEdit();
            this.Validate();
            this.ValidateChildren();

            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
            {
                //string strErrors = GetInvalidDataEntryValues(dxErrorProvider, dlcAddQuote);
                //XtraMessageBox.Show("Quote has not been created.\nOne or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                //"Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Quote Not Created", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            strFilterList = (!chkComboChecked(cmbResourceRateID) ? "0" : cmbResourceRateID.EditValue.ToString());
            if (strFilterList == "0" || lueBillingCentreCodeID.Properties.GetDisplayText(lueBillingCentreCodeID.EditValue) == "" || lueClientContractTypeID.Properties.GetDisplayText(lueClientContractTypeID.EditValue) == "")
            {
                XtraMessageBox.Show("Quote has not been created.\nOne or more missing\\incorrect values are contained within the current record!\n\n" +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Quote Not Created", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                // List with duplicate elements.
                List<int> list = new List<int>();

                string[] t = splitStrRecords(strFilterList);
                string strFix = "";
                int intCount = 0;
                foreach (string tr in t)
                {
                    list.Add(Convert.ToInt32(tr));
                }
                List<int> distinct = list.Distinct().ToList();
                foreach (int intValue in distinct)
                {
                    intCount++;
                    if (distinct.Count == intCount)
                    {
                        strFix += intValue;
                    }
                    else
                    {
                        strFix += intValue + ",";
                    }
                }
                strFilterList = strFix;

                string strFilter = String.Format("ResourceRateID in ({0})", strFilterList);

                DataRow[] drQuoteItemList = this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List.Select(strFilter);

                foreach (DataSet_UT_Quote.sp07438_UT_Resource_Rate_ListRow drQuoteItem in drQuoteItemList)
                {
                    int intQuantity = 1;
                    int intRating = getTaxRating("S - Standard");
                    int intAnalysisCode = getAnalysisCode("U-CUT1");
                    int intResourceRateID = drQuoteItem.ResourceRateID;
                    decimal dcMarkUp = Convert.ToDecimal(1.2);
                    decimal dcFreeTextRate = 0;
                    decimal dcBuyRate = (drQuoteItem.BuyRate == 0 ? 0 : Convert.ToDecimal(drQuoteItem.BuyRate));
                    decimal dcBuyRateVat = (dcBuyRate * getVatRate());
                    decimal dcBuyRateTotal = dcBuyRateVat + dcBuyRate;

                    decimal dcSellRate = (drQuoteItem.SellRate == 0 ? 0 : Convert.ToDecimal(drQuoteItem.SellRate));
                    decimal dcSellRateVat = (dcSellRate * getVatRate());
                    decimal dcSellRateTotal = dcSellRateVat + dcSellRate;

                    decimal dcNetCost = dcBuyRate * intQuantity;
                    decimal dcNetPrice = dcSellRate * intQuantity;

                    switch (drQuoteItem.QuoteCategoryID)
                    {
                        case 4://traffic management
                              dcMarkUp = Convert.ToDecimal(1);
                        //    dcFreeTextRate = (dcFreeTextRate == 0 ? dcBuyRateTotal : dcFreeTextRate);
                        //    dcBuyRate = 0;
                        //    dcBuyRateVat = 0;
                        //    dcBuyRateTotal = 0;
                        //    dcNetCost = dcFreeTextRate;
                            break;

                        case 5://other
                            dcNetCost = dcFreeTextRate;
                            dcNetPrice = dcFreeTextRate * dcMarkUp;
                            break;
                    }
                    AddNewChildRow(intTempQuoteItem, drQuoteItem.QuoteCategoryID, drQuoteItem.Description, dcSellRate, dcSellRateVat, dcSellRateTotal, dcBuyRate, dcBuyRateVat, dcBuyRateTotal, dcFreeTextRate, dcNetPrice, dcNetCost, dcMarkUp, intQuantity, drQuoteItem.ItemUnitsID, intRating, intAnalysisCode, intResourceRateID);
                    intTempQuoteItem--;
                }

                if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations), false)))
                {
                    XtraMessageBox.Show("Quote has been successfully created.", "Quote Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //load Quote
                    sp07418_UT_Quote_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07418_UT_Quote_List, intQuoteID.ToString(), "edit");
                    //load Quote Items
                    sp07421_UT_Quote_Item_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List, intQuoteID.ToString(), "edit");
                    PopulatePickList();
                    addOnlyControlVisible(false);
                }
                else
                {
                    XtraMessageBox.Show("Quote has not been created.\nOne or more missing\\incorrect values are contained within the current record!\n\n" +
                    "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Quote Not Created", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Quote has not been created.\nOne or more missing\\incorrect values are contained within the current record!\n\n" +
               "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Quote Not Created", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }
        
        private void spinEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateSpinEdit((SpinEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as fie ld is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void spnFreeTextRate_EditValueChanged(object sender, EventArgs e)
        {
            decimal dcBuyRATE = (spnBuyRate.EditValue == null ? 0 : Convert.ToDecimal(spnBuyRate.EditValue));
            decimal dcSellRATE = (spnSellRate.EditValue == null ? 0 : Convert.ToDecimal(spnSellRate.EditValue));
            getNetCost(dcBuyRATE);
            getNetPrice(dcSellRATE);
        }

        private void spnQuantity_EditValueChanged(object sender, EventArgs e)
        {
            sellRateChanged();
            buyRateChanged();
        }

        private void memDescription_Validating(object sender, CancelEventArgs e)
        {
            if (validateMemEdit((MemoEdit)sender, SentenceCase.AsIs))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void textEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateTextBox((TextEdit)sender, SentenceCase.Upper))
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void checkedComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (validateCheckedComboEdit((CheckedComboBoxEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void lookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (validateLookupEdit((LookUpEdit)sender))
            {
                e.Cancel = false;// Remove stop icon as field is valid //
            }
            else
            {
                e.Cancel = true;// Show stop icon as field is invalid //
            }
        }

        private void cmbQuoteCategoryID_Validated(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;

            if (chkComboChecked(cmbQuoteCategoryID))
            {
                FilterResourceRate();
                cmbResourceRateID.Enabled = true;
            }
            else
            {
                cmbResourceRateID.Enabled = false;
                uncheckcmbResourceRateID();
            }
        }

        private void cmbResourceRateID_Enter(object sender, EventArgs e)
        {
            RefreshResourceRateList();
            FilterResourceRate();
            uncheckcmbResourceRateID();
        }
        
        private void spnSellRateVAT_EditValueChanged(object sender, EventArgs e)
        {
            //if (bool_FormLoading)
            //    return;

            //sellRateChanged(false);
        }

        private void spnSellRate_EditValueChanged(object sender, EventArgs e)
        {
            sellRateChanged();
        }

        private void spnBuyRateVAT_EditValueChanged(object sender, EventArgs e)
        {
            //if (bool_FormLoading)
            //    return;

            //buyRateChanged(false);
        }

        private void spnBuyRate_EditValueChanged(object sender, EventArgs e)
        {
            buyRateChanged();
        }

        private void lueClientContractTypeID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (formMode == FormMode.add)
            {
                FilterResourceRate();
                uncheckcmbResourceRateID();
                lueBillingCentreCodeID.Enabled = true;
                lueBillingCentreCodeID.Focus();
            }
        }

        private void lueClientContractTypeID_Enter(object sender, EventArgs e)
        {
            if (formMode == FormMode.add)
            {
                if (strClientContractFilter == "" || strClientContractFilter == "ClientID = 0")
                {
                    FliterContractType();
                }
            }
        }

        private void cmbQuoteCategoryID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (formMode == FormMode.add)
            {
                uncheckcmbResourceRateID();
                cmbResourceRateID.Enabled = true;
                cmbResourceRateID.Focus();
            }
        }

        private void lueClientID_EditValueChanged(object sender, EventArgs e)
        {
            if (bool_FormLoading)
                return;
            if (formMode != FormMode.add)
                return;
            FliterContractType();
        }


        #endregion

        #region Unique Form Functions

        private void loadItemUnits()
        {
            sp07435_UT_Item_Units_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07435_UT_Item_Units_List, "", 0, "");
        }

        private void filterItemUnits()
        {
            string strItemunitsFilter;
            if (lueQuoteCategoryID.EditValue != null && lueQuoteCategoryID.Properties.GetDisplayText(lueQuoteCategoryID.EditValue) != "")
                strItemunitsFilter = String.Format("QuoteCategoryID = {0}", Convert.ToInt32(lueQuoteCategoryID.EditValue));
            else
                strItemunitsFilter = "QuoteCategoryID = 0";
            sp07435UTItemUnitsListBindingSource.Filter = strItemunitsFilter;
            
        }

        private void lockBuyFields(bool blnSuggestedLock)
        {
            bool blnRateLock ;
            if (!blnSuggestedLock)
                blnRateLock = getBuyRateLockStatus();
            else
                blnRateLock = blnSuggestedLock;

            spnBuyRate.Properties.ReadOnly = blnRateLock;
            spnBuyRateVAT.Properties.ReadOnly = blnRateLock;
        }

        private bool getSellRateLockStatus()
        {

           // bool? blnValue;
            bool blnReturnValue = false;

            var blnAllowEdit =
                                from o in dataSet_UT_Quote.sp07421_UT_Quote_Item_List.AsEnumerable()
                                join t in dataSet_UT_Quote.sp07438_UT_Resource_Rate_List.AsEnumerable()
                                on o.ResourceRateID equals t.ResourceRateID
                                where o.QuoteItemID == getCurrentQuoteItemID()
                                select new
                                {
                                    //AllowEditSellRate = Boolean.Parse(t.AllowEditSellRate.ToString())
                                    AllowEditSellRate = t == null ? true : t.AllowEditSellRate
                                };
            foreach (var row in blnAllowEdit)
            {
                blnReturnValue = row.AllowEditSellRate;

                //blnValue = NullableTryParseBool(row.AllowEditSellRate.ToString());

                //if (blnValue == null)
                //{
                //    blnReturnValue = false;
                //}
                //else
                //{
                //    blnReturnValue = (bool)blnValue;
                //}
            }

            //blnValue = NullableTryParseBool(blnAllowEdit.ToString());

            //if (blnValue == null)
            //{
            //    blnReturnValue = false;
            //}
            //else
            //{
            //    blnReturnValue = (bool)blnValue;
            //}
            return blnReturnValue;
        }

        private bool getBuyRateLockStatus()
        {
            //bool? blnValue;
            bool blnReturnValue = false;

            var blnAllowEdit = 
                                from o in dataSet_UT_Quote.sp07421_UT_Quote_Item_List.AsEnumerable()
                                join t in dataSet_UT_Quote.sp07438_UT_Resource_Rate_List.AsEnumerable() 
                                on o.ResourceRateID equals t.ResourceRateID
                                where o.QuoteItemID == getCurrentQuoteItemID()
                                select new 
                                            {
                                             //   AllowEditBuyRate = Boolean.Parse(t.AllowEditBuyRate.ToString())
                                                 AllowEditBuyRate = t == null ? true : t.AllowEditBuyRate
                                            };
            foreach (var row in blnAllowEdit)
            {
                blnReturnValue = row.AllowEditBuyRate;

                //blnValue = NullableTryParseBool(row.AllowEditBuyRate.ToString()); 
                
                //if (blnValue == null)
                //{
                //    blnReturnValue = false;
                //}
                //else
                //{
                //    blnReturnValue = (bool)blnValue;
                //}
            }
           // blnValue = NullableTryParseBool(blnAllowEdit.ToString());

            //if (blnValue == null)
            //{
            //    blnReturnValue = false;
            //}
            //else
            //{
            //    blnReturnValue = (bool)blnValue;
            //}
            return blnReturnValue;
        }

        public static bool? NullableTryParseBool(string text)
        {
            bool value;
            return Boolean.TryParse(text, out value) ? (Boolean?)value : null;
        }


        private int getCurrentQuoteItemID()
        {
            int intCurrentID;
            DataRowView currentRow = (DataRowView)sp07421_UT_Quote_Item_ListBindingSource.Current;
            if (currentRow != null)
                intCurrentID = Convert.ToInt32(currentRow["QuoteItemID"]);
            else
                intCurrentID = 0;
            return intCurrentID;
        }

        private void hideMarkUPnTextRate(bool blnHide)
        {
            if (blnHide)
            {
                lciMarkUp.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lciFreeTextRate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                lciMarkUp.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                lciFreeTextRate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void lockSellFields(bool blnSuggestedLock)
        {
            bool blnRateLock;
            if (!blnSuggestedLock)
                blnRateLock = getSellRateLockStatus();
            else
                blnRateLock = blnSuggestedLock;
            spnSellRate.Properties.ReadOnly = blnRateLock;
            spnSellRateVAT.Properties.ReadOnly = blnRateLock;
        }
        
        private void lockReadonlyFields()
        {
            txtPONumber.Properties.ReadOnly = true;
            spnBuyRateTotal.Properties.ReadOnly = true;
            spnNetCost.Properties.ReadOnly = true;
            spnNetPrice.Properties.ReadOnly = true;
            spnSellRateTotal.Properties.ReadOnly = true;
            spnMarkUp.Properties.ReadOnly = true;
        }

        private void RefreshResourceRateList()
        {
            sp07438_UT_Resource_Rate_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07438_UT_Resource_Rate_List, "", ""); //resources rate           
        }

        private void RefreshResourceClientContractTypeList()
        {
            sp07431_UT_Client_Contract_Type_ListTableAdapter.Fill(dataSet_UT_Quote.sp07431_UT_Client_Contract_Type_List, "", "view", 0); //client contract type
            FliterContractType();
        }

        private void RefreshBillingCentreCode()
        {
            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item, "", "view");
        }

        private void Open_Child_Forms(string formName, FormMode formMode, string frmCaller)
        {
            switch (formName)
            { 
                case "frm_UT_Client_Contract_Edit":
                    frm_UT_Client_Contract_Edit aChildForm1 = new frm_UT_Client_Contract_Edit();
                    aChildForm1.GlobalSettings = this.GlobalSettings;
                    aChildForm1.strRecordIDs = "";
                    aChildForm1.strFormMode = formMode.ToString();
                    aChildForm1.strCaller = frmCaller;
                    aChildForm1.intRecordCount = 0;
                    aChildForm1.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager3 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    aChildForm1.splashScreenManager = splashScreenManager3;
                    aChildForm1.splashScreenManager.ShowWaitForm();
                    aChildForm1.ShowDialog();
                    break;
                case "frm_UT_Resource_Rate_Edit":
                    frm_UT_Resource_Rate_Edit aChildForm = new frm_UT_Resource_Rate_Edit();
                    aChildForm.GlobalSettings = this.GlobalSettings;
                    aChildForm.strRecordIDs = "";
                    aChildForm.strFormMode = formMode.ToString();
                    aChildForm.strCaller = frmCaller;
                    aChildForm.intClientContractType = (lueClientContractTypeID.Properties.GetDisplayText(lueClientContractTypeID.EditValue) == "" ? 0 : Convert.ToInt32(lueClientContractTypeID.EditValue));
                    aChildForm.intRecordCount = 0;
                    aChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    aChildForm.splashScreenManager = splashScreenManager2;
                    aChildForm.splashScreenManager.ShowWaitForm();
                    aChildForm.ShowDialog();
                    break;
                case "frm_UT_Billing_Centre_Edit":
                    frm_UT_Billing_Centre_Edit bChildForm = new frm_UT_Billing_Centre_Edit();
                    bChildForm.GlobalSettings = this.GlobalSettings;
                    bChildForm.strRecordIDs = "";
                    bChildForm.formMode = (frm_UT_Billing_Centre_Edit.FormMode)formMode;
                    bChildForm.strFormMode = (formMode.ToString()).ToLower();
                    bChildForm.strCaller = frmCaller;
                    bChildForm.intRecordCount = 0;
                    bChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager4 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    bChildForm.splashScreenManager = splashScreenManager4;
                    bChildForm.splashScreenManager.ShowWaitForm();
                    bChildForm.ShowDialog();
                    break;
                case "frm_UT_Utility_Units_Edit":
                    frm_UT_Utility_Units_Edit cChildForm = new frm_UT_Utility_Units_Edit();
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strRecordIDs = "";
                    cChildForm.strFormMode = formMode.ToString();
                    cChildForm.strCaller = frmCaller;
                    cChildForm.intCategoryID = (lueQuoteCategoryID.Properties.GetDisplayText(lueQuoteCategoryID.EditValue) == "" ? 0 : Convert.ToInt32(lueQuoteCategoryID.EditValue));
                    cChildForm.intRecordCount = 0;
                    cChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager5 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager5;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.ShowDialog();
                    break;
                default:
                    MessageBox.Show("Failed to load the form.", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;

            }
        }
        
        private void addOnlyControlVisible(bool blnVisible)
        {
            if (blnVisible)
            {
                lcgMain.Text = "Create Quote"; 
                lcgQuoteItemBlockAdd.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lciReference.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                lcgQuoteItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                lcgQuotePerformance.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                bbiFormAddAnother.Enabled = false;
            }
            else
            {
                lcgQuoteItemBlockAdd.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                lcgQuotePerformance.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lcgQuoteItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lciReference.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lcgMain.Text = "Review Quote";
                bbiFormAddAnother.Enabled = true;
                qstage = QuoteStage.Review;
                bbiSaveReview.Enabled = false;
            }
        }

        private decimal getVatRate()
        {
            if (lueRating.Properties.GetDisplayText(lueRating.EditValue) == "S - Standard")
            {
                DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetVatRate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
                GetVatRate.ChangeConnectionString(strConnectionString);
                lciBuyRateTotal.Text = "Buy Rate Total(incl. VAT):";
                lciSellRateTotal.Text = "Sell Rate Total(incl. VAT):";
                return Convert.ToDecimal(GetVatRate.sp00043_RetrieveSingleSystemSetting(11, "VATRate"));
            }
            else
            {
                lciBuyRateTotal.Text = "Buy Rate Total:";
                lciSellRateTotal.Text = "Sell Rate Total:";
                return 0;
            }
        }


        private void buyRateChanged()
        {
            if (bool_FormLoading)
                return;

            decimal dcBuyRATE = (spnBuyRate.EditValue == null ? 0 : Convert.ToDecimal(spnBuyRate.EditValue));
            decimal dcBuyVAT = 0;
            //if (blnAutoUpdateBuyVat)
            //{
                dcBuyVAT = dcBuyRATE * getVatRate();
                spnBuyRateVAT.EditValue = dcBuyVAT;
            //}
            //else 
            //{
            //    dcBuyVAT = (spnBuyRateVAT.EditValue == null ? 0 : Convert.ToDecimal(spnBuyRateVAT.EditValue));
            //}
            updateAutoBuyTotal(dcBuyRATE, dcBuyVAT);

            getNetCost(dcBuyRATE);           
        }

        private void sellRateChanged()
        {
            if (bool_FormLoading)
                return;

            decimal dcSellRATE = (spnSellRate.EditValue == null ? 0 : Convert.ToDecimal(spnSellRate.EditValue));
            decimal dcSellVAT = 0;
            //if (blnAutoUpdateSellVat)
            //{
                dcSellVAT = dcSellRATE * getVatRate();
                spnSellRateVAT.EditValue = dcSellVAT;
            //}
            //else
            //{
            //    dcSellVAT = (spnSellRateVAT.EditValue == null ? 0 : Convert.ToDecimal(spnSellRateVAT.EditValue));
            //}
            updateAutoSellTotal(dcSellRATE, dcSellVAT);

            getNetPrice(dcSellRATE);
        }

        private void getNetCost(decimal dcNewBuyRate)
        {
            if (bool_FormLoading)
                return;

            decimal dcNetCost = dcNewBuyRate * Convert.ToInt32(spnQuantity.EditValue);
            spnNetCost.EditValue = dcNetCost;

            if (lueQuoteCategoryID.Properties.GetDisplayText(lueQuoteCategoryID.EditValue) != "")
            {
                switch (Convert.ToInt32(lueQuoteCategoryID.EditValue))
                {
                    case 4://traffic management
                        if (spnFreeTextRate.Properties.GetDisplayText(spnFreeTextRate.EditValue) != "")
                        {
                            dcNetCost = Convert.ToDecimal(spnFreeTextRate.EditValue);
                            spnNetCost.EditValue = dcNetCost;
                        }
                        break;

                    case 5://other
                        if (spnFreeTextRate.Properties.GetDisplayText(spnFreeTextRate.EditValue) != "")
                        {
                            dcNetCost = Convert.ToDecimal(spnFreeTextRate.EditValue);
                            spnNetCost.EditValue = dcNetCost;
                        }
                        break;
                }
            }
        }

        private void getNetPrice(decimal dcNewSellBuyRate)
        {
            if (bool_FormLoading)
                return;

            //decimal dcSellRATE = (spnSellRate.EditValue == null ? 0 : Convert.ToDecimal(spnSellRate.EditValue));
            decimal dcNetPrice = dcNewSellBuyRate * Convert.ToInt32(spnQuantity.EditValue);
            spnNetPrice.EditValue = dcNetPrice;

            if (lueQuoteCategoryID.Properties.GetDisplayText(lueQuoteCategoryID.EditValue) != "")
            {
                switch (Convert.ToInt32(lueQuoteCategoryID.EditValue))
                {
                    case 5://other
                        if (spnFreeTextRate.Properties.GetDisplayText(spnFreeTextRate.EditValue) != "")
                        {
                            decimal dcMarkUp = (spnMarkUp.EditValue == null ? Convert.ToDecimal(1.2) : Convert.ToDecimal(spnMarkUp.EditValue));
                            dcNetPrice = Convert.ToDecimal(spnFreeTextRate.EditValue);
                            spnNetPrice.EditValue = dcNetPrice * dcMarkUp * Convert.ToInt32(spnQuantity.EditValue);
                        }
                        break;
                }
            }
        }

        private void updateAutoBuyTotal(decimal dcNewBuyRate, decimal dcNewBuyVat)
        {
            spnBuyRateTotal.EditValue = dcNewBuyRate + dcNewBuyVat;
        }

        private void updateAutoSellTotal(decimal dcNewSellRate, decimal dcNewSellRateVat)
        {
            //decimal dcSellRATE = spnSellRate.EditValue == null ? 0 : Convert.ToDecimal(spnSellRate.EditValue);
            //decimal AutoVAT = dcSellRATE * getVatRate();
            spnSellRateTotal.EditValue = dcNewSellRate + dcNewSellRateVat;
        }

        private void FliterContractType()
        {
            strClientContractFilter = "ClientID = 0";
            if (lueClientID.EditValue != null && lueClientID.Properties.GetDisplayText(lueClientID.EditValue) != "")
            {
                strClientContractFilter = String.Format("ClientID = {0}", Convert.ToInt32(lueClientID.EditValue));
            }
            sp07431UTClientContractTypeListBindingSource.Filter = strClientContractFilter;
            try
            {
                switch (strFormMode.ToLower())
                {
                    case "add":
                        if (blnBulkAdding)
                        {
                            lueClientContractTypeID.EditValue = null;
                        }
                        break;
                    case "edit":
                    case "view":
                        DataRowView currRow = (DataRowView)sp07421_UT_Quote_Item_ListBindingSource.Current;
                        if (currRow.Row.RowState == DataRowState.Modified)
                        {
                            lueClientContractTypeID.EditValue = null;
                        }
                        break;
                    case "blockedit":
                    case "block_add":
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void FilterResourceRate()
        {
            //Contract1,Category1 - 1
            //Contract1,Category0 - 2
            //Contract0,Category1 - 3
            //Contract0,Category0 - 4
            strResourceRateFilter = "";
            int intCombination = 0;
            if (lueClientContractTypeID.EditValue != null && lueClientContractTypeID.Properties.GetDisplayText(lueClientContractTypeID.EditValue) != "")//Contract1
            {
                //if (cmbResourceRateID.EditValue != null && (cmbResourceRateID.Properties.GetDisplayText(cmbResourceRateID.EditValue) != "" || cmbResourceRateID.Properties.GetDisplayText(cmbResourceRateID.EditValue) != "1"))//Contract1,Category1
                if (chkComboChecked(cmbQuoteCategoryID))//Contract1,Category1
                {
                    intCombination = 1;
                }
                else//Contract1,Category0
                {
                    intCombination = 2;
                }
            }
            else
            {
                if (cmbResourceRateID.EditValue != null && cmbResourceRateID.Properties.GetDisplayText(cmbResourceRateID.EditValue) != "" && cmbResourceRateID.Properties.GetDisplayText(cmbResourceRateID.EditValue) != "1")//Contract0,Category1
                {
                    intCombination = 3;
                }
                else//Contract0,Category0
                {
                    intCombination = 4;
                }
            }

            switch (intCombination)
            {
                case 1://Contract1,Category1 - 1
                    //if (Convert.ToString(cmbQuoteCategoryID.EditValue) == "" || Convert.ToString(cmbQuoteCategoryID.EditValue) == "1")
                    if (!chkComboChecked(cmbQuoteCategoryID))
                    {
                        intCombination = 2;
                        goto case 2;
                        //strResourceRateFilter = String.Format("ClientContractTypeID = {0}", Convert.ToInt32(lueClientContractTypeID0.EditValue));
                    }
                    strResourceRateFilter = String.Format("ClientContractTypeID = {0} and QuoteCategoryID in ({1})", Convert.ToInt32(lueClientContractTypeID.EditValue), Convert.ToString(cmbQuoteCategoryID.EditValue));
                    break;
                case 2://Contract1,Category0 - 2
                    strResourceRateFilter = String.Format("ClientContractTypeID = {0}", Convert.ToInt32(lueClientContractTypeID.EditValue));
                    switch (strFormMode.ToLower())
                    {
                        case "add":
                            uncheckcmbQuoteCategoryID();
                            break;
                        case "edit":
                        case "view":
                            DataRowView currRow = (DataRowView)sp07421_UT_Quote_Item_ListBindingSource.Current;
                            if (currRow.Row.RowState == DataRowState.Modified)
                            {
                                uncheckcmbQuoteCategoryID();
                            }
                            break;
                        case "blockedit":
                        case "block_add":
                            break;
                    }
                    break;
                case 3://Contract0,Category1 - 3
                    strResourceRateFilter = String.Format("QuoteCategoryID in ({0})", Convert.ToString(cmbQuoteCategoryID.EditValue));
                    switch (strFormMode.ToLower())
                    {
                        case "add":
                            lueClientContractTypeID.EditValue = null;
                            break;
                        case "edit":
                        case "view":
                            DataRowView currRow = (DataRowView)sp07421_UT_Quote_Item_ListBindingSource.Current;
                            if (currRow.Row.RowState == DataRowState.Modified)
                            {
                                lueClientContractTypeID.EditValue = null;
                            }
                            break;
                        case "blockedit":
                        case "block_add":
                            break;
                    }
                    break;
                case 4://Contract0,Category0 - 4
                    strResourceRateFilter = "ClientContractTypeID = 0 and QuoteCategoryID = 0";
                    lueClientContractTypeID.EditValue = null;
                    uncheckcmbQuoteCategoryID();
                    break;
            }
            try
            {
                sp07438_UT_Resource_Rate_ListBindingSource.Filter = strResourceRateFilter;
            }
            catch (SyntaxErrorException)
            {
                sp07438_UT_Resource_Rate_ListBindingSource.Filter = "";
            }
        }

        private void uncheckcmbResourceRateID()
        {
            cmbResourceRateID.EditValue = null;
            for (int i = 0; i < cmbResourceRateID.Properties.Items.Count; i++)
            {
                cmbResourceRateID.Properties.Items[i].CheckState = CheckState.Unchecked;
            }
        }

        private void uncheckcmbQuoteCategoryID()
        {
            cmbQuoteCategoryID.EditValue = null;
            for (int i = 0; i < cmbQuoteCategoryID.Properties.Items.Count; i++)
            {
                cmbQuoteCategoryID.Properties.Items[i].CheckState = CheckState.Unchecked;
            }
        }

        private bool chkComboChecked(CheckedComboBoxEdit chkEdit)
        {
            bool blnChecked = false;
            for (int i = 0; i < chkEdit.Properties.Items.Count; i++)
            {
                if (chkEdit.Properties.Items[i].CheckState == CheckState.Checked)
                {
                    return true;
                }
            }
            return blnChecked;
        }

        private int getAnalysisCode(string strAnalysisCode)
        {
            DataRow[] drPickListItems = this.dataSet_UT_Quote.sp07446_UT_PL_AnalysisCode.Select();
            int defaultAnalysisCode = 0;
            foreach (DataSet_UT_Quote.sp07446_UT_PL_AnalysisCodeRow drPickListItem in drPickListItems)
            {
                if ((drPickListItem.Value).ToLower() == strAnalysisCode.ToLower())
                {
                    defaultAnalysisCode = Convert.ToInt32(drPickListItem.PickListID);
                    return defaultAnalysisCode;
                }
            }
            return defaultAnalysisCode;
        }

        private int getTaxRating(string strRating)
        {
            DataRow[] drPickListItems = this.dataSet_UT_Quote.sp07446_UT_Picklist_By_Header_ID.Select();
            int defaultRating = 0;
            foreach (DataSet_UT_Quote.sp07446_UT_Picklist_By_Header_IDRow drPickListItem in drPickListItems)
            {
                if ((drPickListItem.Value).ToLower() == strRating.ToLower())
                {
                    defaultRating = Convert.ToInt32(drPickListItem.PickListID);
                    return defaultRating;
                }
            }
            return defaultRating;
        }

        private int getGetDefaultItemUnits()
        {
            string strDefaultFilter = "";
            strDefaultFilter = "QuoteCategoryID = 5";
            DataRow[] drPickListItems = this.dataSet_UT_Quote.sp07435_UT_Item_Units_List.Select(strDefaultFilter);
            int defaultUnits = 0;
            foreach (DataSet_UT_Quote.sp07435_UT_Item_Units_ListRow drPickListItem in drPickListItems)
            {
                if (drPickListItem.Units.ToLower() == "none")
                {
                    defaultUnits = Convert.ToInt32(drPickListItem.ItemUnitsID);
                    return defaultUnits;
                }
            }
            return defaultUnits;
        }

        private void IdentifyChangedBindingSource(DataTable dt)
        {
            switch (dt.TableName)
            {
                case "sp07418_UT_Quote_List"://quote
                    editorDataChanged = true;
                    break;
                case "sp07421_UT_Quote_Item_List"://Quote item
                    quoteItemChanged = true;
                    break;
            }
        }

        private void EndEdit()
        {
            sp07418_UT_Quote_ListBindingSource.EndEdit();
            sp07421_UT_Quote_Item_ListBindingSource.EndEdit();
        }

        private string SaveChanges(Boolean ib_SuccessMessage, Boolean blnAddAnotherChildItem)
        {

            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //         
            EndEdit();
            
            this.Validate();
            ValidateChildren();
            bool shouldReturn;
            string result = SaveChangesExtracted(out shouldReturn);
            if (shouldReturn)
                return result;

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            EndEdit();

            CheckChangedTable(this.dataSet_UT_Quote.sp07418_UT_Quote_List);//Quote Check
            CheckChangedTable(this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List);//Quote Item Check

            try
            {
                // Insert and Update queries defined in Table Adapter //
                if (editorDataChanged)
                {
                    this.sp07418_UT_Quote_ListTableAdapter.Update(dataSet_UT_Quote);
                    editorDataChanged = false;
                }
                if (quoteItemChanged)
                {
                    DataRowView currentRow = (DataRowView)sp07418_UT_Quote_ListBindingSource.Current;
                    if (currentRow != null)
                    {
                        intQuoteID = Convert.ToInt32(currentRow[0]);
                        int iRowCount = 0;
                        foreach (DataSet_UT_Quote.sp07421_UT_Quote_Item_ListRow newQuoteItem in this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows)
                        {
                            this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows[iRowCount]["QuoteID"] = intQuoteID;// this.dataSet_UT_Quote.sp07418_UT_Quote_List[0].QuoteID;
                            iRowCount++;
                        }
                        this.sp07421_UT_Quote_Item_ListTableAdapter.Update(dataSet_UT_Quote);
                        //if (strFormMode.ToLower() == "add")
                        //{
                        //    int iRowCount = 0;
                        //    foreach (DataSet_UT_Quote.sp07421_UT_Quote_Item_ListRow newQuoteItem in this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows)
                        //    {
                        //        this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows[iRowCount]["QuoteID"] = intQuoteID;// this.dataSet_UT_Quote.sp07418_UT_Quote_List[0].QuoteID;
                        //        iRowCount++;
                        //    }
                        //    this.sp07421_UT_Quote_Item_ListTableAdapter.Update(dataSet_UT_Quote);
                        //}
                        //else
                        //{
                        //    this.sp07421_UT_Quote_Item_ListTableAdapter.Update(dataSet_UT_Quote);
                        //}
                        quoteItemChanged = false;
                    }

                }

            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
                var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);
                var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());
                dialog.Text = "Error Saving Changes";
                dialogType.GetProperty("Details").SetValue(dialog, ex.Message, null);
                dialogType.GetProperty("Message").SetValue(dialog, String.Format("An error occurred while saving the record changes [{0}]!\n\nTry saving again - if the problem persists, contact Technical Support.", ex.Message), null);
                var msgResult = dialog.ShowDialog();
                return "Error";
            }
            finally
            {
                if (!editorDataChanged && !quoteItemChanged && !blnAddAnotherChildItem)
                    forceFormClose = true;
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted // 
            UpdateManagerScreen();
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //

        }

        private void UpdateManagerScreen()
        {
            switch (strCaller)
            {
                case "frm_UT_WorkOrder_Manager":
                    // Notify any open instances of Sequence Manager they will need to refresh their data on activating //
                    if (Application.OpenForms[strCaller] != null)
                    {
                        (Application.OpenForms[strCaller] as frm_UT_WorkOrder_Manager).UpdateFormRefreshStatus(1, intWorkOrderNum.ToString(), strRecordIDs, "");
                        (Application.OpenForms[strCaller] as frm_UT_WorkOrder_Manager).frmActivated();
                    }

                    break;
            }
        }

        private void PopulatePickList()
        {
            sp07424_UT_Quote_Category_ItemTableAdapter.Fill(this.dataSet_UT_Quote.sp07424_UT_Quote_Category_Item, "2,3,4,5", "");
            sp07424_UT_Quote_Category_Item_AddModeTableAdapter.Fill(dataSet_UT_Quote.sp07424_UT_Quote_Category_Item_AddMode, "2,3,4", "");
            sp07427_UT_Client_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07427_UT_Client_List, intWorkOrderNum.ToString());
            sp07450_UT_SubContractor_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07450_UT_SubContractor_List, intWorkOrderNum.ToString());
            sp07452_UT_Circuit_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07452_UT_Circuit_List, intWorkOrderNum.ToString());
            sp07453_UT_FeederContract_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07453_UT_FeederContract_List, intWorkOrderNum.ToString());
            loadItemUnits();
            sp07438_UT_Resource_Rate_ListTableAdapter.Fill(dataSet_UT_Quote.sp07438_UT_Resource_Rate_List, "", "");
            sp07431_UT_Client_Contract_Type_ListTableAdapter.Fill(dataSet_UT_Quote.sp07431_UT_Client_Contract_Type_List, "", "view", 0);
            sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.Fill(dataSet_AS_DataEntry.sp_AS_11038_Billing_Centre_Code_Item, "", "view");
            sp07446_UT_Picklist_By_Header_IDTableAdapter.Fill(dataSet_UT_Quote.sp07446_UT_Picklist_By_Header_ID, 5);
            sp07446_UT_PL_AnalysisCodeTableAdapter.Fill(dataSet_UT_Quote.sp07446_UT_PL_AnalysisCode, 6);

            filterItemUnits();            
        }

        private void addNewRow(FormMode mode)
        {
            try
            {
                DataRow drNewRow = this.dataSet_UT_Quote.sp07418_UT_Quote_List.NewRow();
                drNewRow["Mode"] = (mode.ToString()).ToLower();
                drNewRow["WorkOrderID"] = intWorkOrderNum;
                drNewRow["CreationDate"] = getCurrentDate();
                drNewRow["CreatedBy"] = String.Format("{0}", GlobalSettings.Username);
                drNewRow["IsMainQuote"] = true;
                drNewRow["TransferredExchequer"] = false;
                drNewRow["PONumber"] = "";
                drNewRow["PaddedWorkOrderID"] = strPaddedWorkOrderNum;
                if (dataSet_UT_Quote.sp07450_UT_SubContractor_List.Count == 1)
                {
                    DataRow[] drSubContractorList = this.dataSet_UT_Quote.sp07450_UT_SubContractor_List.Select();
                    foreach (DataSet_UT_Quote.sp07450_UT_SubContractor_ListRow drSubContractor in drSubContractorList)
                    {
                        drNewRow["SubContractorID"] = drSubContractor.SubContractorID;
                    }
                }

                if (dataSet_UT_Quote.sp07452_UT_Circuit_List.Count == 1)
                {
                    DataRow[] drCircuitList = this.dataSet_UT_Quote.sp07452_UT_Circuit_List.Select();
                    int intRegionID = 0;
                    foreach (DataSet_UT_Quote.sp07452_UT_Circuit_ListRow drCircuit in drCircuitList)
                    {
                        intRegionID = drCircuit.RegionID;
                        drNewRow["CircuitID"] = drCircuit.CircuitID;
                    }
                    sp07009_UT_Region_ItemTableAdapter.Fill(dataSet_UT_Edit.sp07009_UT_Region_Item, intRegionID.ToString(), "");

                    if (dataSet_UT_Edit.sp07009_UT_Region_Item.Count == 1)
                    {
                        DataRow[] drRegionList = dataSet_UT_Edit.sp07009_UT_Region_Item.Select();
                        foreach (DataSet_UT_Edit.sp07009_UT_Region_ItemRow drRegion in drRegionList)
                        {
                            drNewRow["BillingCentreCodeID"] = drRegion.BillingCentreCodeID;
                        }
                        cmbQuoteCategoryID.Enabled = true;
                        cmbQuoteCategoryID.Focus();
                    }
                }

                if (dataSet_UT_Quote.sp07453_UT_FeederContract_List.Count == 1)
                {
                    DataRow[] drFeederContractList = this.dataSet_UT_Quote.sp07453_UT_FeederContract_List.Select();
                    foreach (DataSet_UT_Quote.sp07453_UT_FeederContract_ListRow drFeederContract in drFeederContractList)
                    {
                        drNewRow["FeederContractID"] = drFeederContract.FeederContractID;
                    }
                }

                this.dataSet_UT_Quote.sp07418_UT_Quote_List.Rows.Add(drNewRow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddNewChildRow()
        {
            //RejectChildChanges();
            this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.RejectChanges();

            DataRow drNewRow;
            drNewRow = this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.NewRow();
            drNewRow["Mode"] = "add";
            drNewRow["QuoteItemID"] = -1;
            drNewRow["CreationDate"] = getCurrentDate();
            drNewRow["LastUpdatedDate"] = getCurrentDate();
            drNewRow["LastUpdatedBy"] = GlobalSettings.Username;
            drNewRow["QuoteCategoryID"] = 5;
            drNewRow["RatingID"] = getTaxRating("S - Standard");
            drNewRow["AnalysisCodeID"] = getAnalysisCode("U-CUT1");
            drNewRow["Quantity"] = 1;
            drNewRow["BuyRate"] = 0;
            drNewRow["SellRate"] = 0;
            drNewRow["BuyRateVAT"] = 0;
            drNewRow["SellRateVAT"] = 0;
            drNewRow["ItemUnitsID"] = getGetDefaultItemUnits();
            drNewRow["BuyRateTotal"] = 0;
            drNewRow["FreeTextRate"] = 0;
            drNewRow["NetPrice"] = 0;
            drNewRow["NetCost"] = 0;
            drNewRow["SellRateTotal"] = 0;
            drNewRow["ResourceRateID"] = 0;
            drNewRow["MarkUp"] = Convert.ToDecimal(1.2); 
            

            if (sp07421_UT_Quote_Item_ListBindingSource.Find("QuoteItemID", "-1") < 0)
            {
                this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows.Add(drNewRow);
            }
        }

        private void AddNewChildRow(int intQuoteItemID, int intQuoteCategoryID, string strDescription, decimal dcSellRate, decimal dcSellRateVAT, decimal dcSellRateTotal, decimal dcBuyRate, decimal dcBuyRateVAT, decimal dcBuyRateTotal, decimal dcFreeTextRate, decimal dcNetPrice, decimal dcNetCost, decimal dcMarkUp, int intQuantity, int intItemUnitsID, int intRating, int intAnalysisCode, int intResourceRateID)
        {
            DataRow drNewRow;
            drNewRow = this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.NewRow();
            drNewRow["Mode"] = "add";
            drNewRow["QuoteItemID"] = intQuoteItemID;
            drNewRow["QuoteCategoryID"] = intQuoteCategoryID;
            drNewRow["Description"] = strDescription;
            drNewRow["SellRate"] = dcSellRate;
            drNewRow["SellRateVAT"] = dcSellRateVAT;
            drNewRow["SellRateTotal"] = dcSellRateTotal;
            drNewRow["BuyRateVAT"] = dcBuyRateVAT;
            drNewRow["BuyRate"] = dcBuyRate;
            drNewRow["BuyRateTotal"] = dcBuyRateTotal;
            drNewRow["FreeTextRate"] = dcFreeTextRate;
            drNewRow["NetPrice"] = dcNetPrice;
            drNewRow["NetCost"] = dcNetCost;
            drNewRow["MarkUp"] = dcMarkUp;
            drNewRow["Quantity"] = intQuantity;
            drNewRow["ItemUnitsID"] = intItemUnitsID;
            drNewRow["RatingID"] = intRating;
            drNewRow["AnalysisCodeID"] = intAnalysisCode;
            drNewRow["ResourceRateID"] = intResourceRateID;
            drNewRow["CreationDate"] = getCurrentDate();
            drNewRow["LastUpdatedDate"] = getCurrentDate();
            drNewRow["LastUpdatedBy"] = GlobalSettings.Username;

            if (sp07421_UT_Quote_Item_ListBindingSource.Find("QuoteItemID", intTempQuoteItem) < 0)
            {
                this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows.Add(drNewRow);
            }
        }

        private void loadQuote()
        {
            //load Quote
            sp07418_UT_Quote_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07418_UT_Quote_List, intQuoteID.ToString(), strFormMode);
            //load Quote Items
            sp07421_UT_Quote_Item_ListTableAdapter.Fill(this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List, intQuoteID.ToString(), strFormMode);
        }

        #endregion

        #region Standard Form Functions

        private DateTime getCurrentDate()
        {
            DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter GetDate = new DataSet_AS_DataEntryTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            GetDate.sp_AS_11138_Get_Server_Date();
            DateTime d = DateTime.Parse(GetDate.sp_AS_11138_Get_Server_Date().ToString());
            return d;
        }

        private string[] splitStrRecords(string records)
        {
            char[] delimiters = new char[] { ',', ';' };
            string[] parts;
            if (records == "")
            {
                parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = records.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            return parts;
        }

        private void ClearErrors(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is LookUpEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is TextEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is DateEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ColorPickEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is SpinEdit)
                    dxErrorProvider.SetError(item2, "");
                if (item2 is ContainerControl)
                    this.ClearErrors(item.Controls);
                if (item2 is DataLayoutControl)
                    this.ClearErrors(item.Controls);

            }
        }

        private string[] splitStrRecords()
        {
            char[] delimiters = new char[] { ',' };
            string[] parts = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            return parts;
        }

        private string SaveChangesExtracted(out bool shouldReturn)
        {
            shouldReturn = false;

            if (dxErrorProvider.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider, editFormDataLayoutControlGroup);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors +
                "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                shouldReturn = true;
                return "Error";
            }

            return String.Empty;
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
                if (item2 is DataLayoutControl) this.Attach_EditValueChanged_To_Children(item.Controls);

            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }

        private Boolean SetChangesPendingLabel()
        {
            EndEdit();

            DataSet dsChanges = this.dataSet_UT_Quote.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;

                if (strFormMode.ToLower() == "add")
                {
                    bbiFormSave.Enabled = true;
                }
                return false;
            }


        }

        private string CheckForPendingSave()
        {
            EndEdit();

            string strMessage = "";
            string strMessage1 = CheckTablePendingSave(this.dataSet_AS_DataEntry.sp_AS_11050_Depreciation_Item, " on the "+ this.Text + " Form ");


            if (strMessage1 != "")
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (strMessage1 != "") 
                    strMessage += strMessage1;
            }
            return strMessage;
        }

        private string CheckTablePendingSave(DataTable dt, string formArea)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (intRecordNew > 0) strMessage += String.Format("{0} New record(s){1}\n", Convert.ToString(intRecordNew), formArea);
                if (intRecordModified > 0) strMessage += String.Format("{0} Updated record(s){1}\n", Convert.ToString(intRecordModified), formArea);
                if (intRecordDeleted > 0) strMessage += String.Format("{0} Deleted record(s){1}\n", Convert.ToString(intRecordDeleted), formArea);
            }
            return strMessage;
        }

        private void CheckChangedTable(DataTable dt)
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                switch (dt.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                IdentifyChangedBindingSource(dt);
            }
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            bool_FormLoading = false;
            if (strFormMode == "edit")
            {
                filterItemUnits();
                refreshPerformanceGrids();
                categoryValueChanged();
                addOnlyControlVisible(false); 
            }
            else
            {
                addOnlyControlVisible(true); 
            }
        }
                
        #endregion

        #region Compulsory Implementation 
        
        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode.ToLower())
            {
                case "add":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.Appearance.ForeColor = color;
                        editFormDataNavigator.Visible = false;
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Adding";
                        bsiFormMode.ImageIndex = 0;  // Add //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        ////barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                       // txtEquipment_GC_Reference.Focus();
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Block Editing";
                        //barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        bsiFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        //barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        editFormDataNavigator.Visible = false;
                        //lueSupplier.Focus();
                        //txtEquipment_GC_Reference.Properties.ReadOnly = true;
                        //lueEquipment_Category.Properties.ReadOnly = true;
                        //txtManufacturer_ID.Properties.ReadOnly = true;
                        //lueMake.Properties.ReadOnly = true;
                        //lueModel.Properties.ReadOnly = true;
                        //deDepreciation_Start_Date.Properties.ReadOnly = true;
                        //txtEx_Asset_ID.Properties.ReadOnly = true;
                        //txtSage_Asset_Ref.Properties.ReadOnly = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        bsiFormMode.Caption = "Form Mode: Viewing";
                        bsiFormMode.ImageIndex = 4;  // View //
                        bsiFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        //barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error");
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
               // equipmentDataLayoutControl.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
        
            bbiSave.Enabled = false;
            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        #endregion

        #region Form Events

        private void bbiFormAddAnother_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ValidateChildren();

            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations), true)))
            {
                AddNewChildRow();
                intTempQuoteItem--;
                sp07421_UT_Quote_Item_ListBindingSource.MoveLast();
            }
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations), false)))
            {
                UpdateManagerScreen();
                this.Close();
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations),false);
        }

        private void frm_UT_Quote_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            if (forceFormClose) return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true,false))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        private void frm_UT_Quote_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

          //  IdentifyChangedBindingSource();
        }

        #endregion

        #region Data Navigator
                      
        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        private void layoutControlGroup9_Shown(object sender, EventArgs e)
        {
            refreshPerformanceGrids();
        }

        private void refreshPerformanceGrids()
        {
            if (bool_FormLoading) return;

            decimal sumNetCost = (decimal)0.00;
            decimal sumNetPrice = (decimal)0.00;
            float fProfit = (float)0.00;
            if (dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Rows.Count > 0)
            {
                sumNetCost = Convert.ToDecimal(this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Compute("Sum(NetCost)", string.Format("QuoteID = {0}", intQuoteID)));
                sumNetPrice = Convert.ToDecimal(this.dataSet_UT_Quote.sp07421_UT_Quote_Item_List.Compute("Sum(NetPrice)", string.Format("QuoteID = {0}", intQuoteID)));
                int intMax = Convert.ToInt32(Math.Max(sumNetCost, sumNetPrice)); ;
                int remainder = intMax % 100;
                intMax = Convert.ToInt32((intMax + (100 - remainder)));

                fProfit = (float)(sumNetPrice - sumNetCost);

                linearScaleComponent1.MaxValue = intMax;
                linearScaleComponent1.MinValue = -intMax;
                linearScaleComponent1.Value = fProfit;

                updateLinearScale(intMax, fProfit, sumNetCost, sumNetPrice);

                updateScale2(intMax, sumNetCost, sumNetPrice);
                //arcScaleComponent4.MaxValue = intMax;
                //arcScaleComponent2.MaxValue = intMax;
                //arcScaleComponent4.Value = (float)sumNetCost;
                //arcScaleComponent2.Value = (float)sumNetPrice;
                //labelComponent1.Text = String.Format("{0:C2}", sumNetCost);
                //labelComponent2.Text = String.Format("{0:C2}", sumNetPrice);
            }
            txtTotalNetCost.Text = String.Format("{0:C2}", sumNetCost);
            txtTotalNetPrice.Text = String.Format("{0:C2}", sumNetPrice);
            txtTotalNetProfit.Text = String.Format("{0:C2}", fProfit);
            if(fProfit < 0)
            {
                lciProfit.Text = "Net Loss:";
                txtTotalNetProfit.BackColor = Color.Red;
            }
            else
            {
                lciProfit.Text = "Net Profit:";
                txtTotalNetProfit.BackColor = Color.LimeGreen;
            }
        }

        private void updateLinearScale(int intMax,float fProfit, decimal sumNetCost, decimal sumNetPrice)
        {
            LinearScaleRange linearScaleRange1 = new LinearScaleRange();
            LinearScaleRange linearScaleRange2 = new LinearScaleRange();
            LinearScaleRange linearScaleRange3 = new LinearScaleRange();
            float fStartThickness = 11F;
            float fShapeOffSet = 10F;
            
            // linearScaleRangeBarComponent1
            this.linearGauge1.RangeBars.Remove(this.linearScaleRangeBarComponent1);
            
            linearScaleRangeBarComponent1.Value = fProfit;//30F;
            linearScaleRangeBarComponent1.StartOffset = 4F;
            linearScaleRangeBarComponent1.ZOrder = -100;
            linearScaleRangeBarComponent1.EndOffset = 8F;
            linearScaleRangeBarComponent1.Name = "linearGauge2_RangeBar1";            
            linearScaleRangeBarComponent1.LinearScale = this.linearScaleComponent1;
            linearScaleRangeBarComponent1.AppearanceRangeBar.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            if (fProfit > 0)
            {
                linearScaleRangeBarComponent1.AppearanceRangeBar.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LimeGreen");
                labelComponent1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LimeGreen");
                labelComponent1.Text = String.Format("Net Profit:{0:C2}", fProfit);
            }
            else if (fProfit == 0)
            {
                linearScaleRangeBarComponent1.AppearanceRangeBar.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Orange");
                labelComponent1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Orange");
                labelComponent1.Text = String.Format("Breakdown:{0:C2}", fProfit);
            }
            else
            {
                linearScaleRangeBarComponent1.AppearanceRangeBar.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Red");
                labelComponent1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Red");
                labelComponent1.Text = String.Format("Net Loss:{0:C2}", fProfit);
            }
            
            linearGauge1.RangeBars.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleRangeBarComponent[] {this.linearScaleRangeBarComponent1});
                        
            // Range 1
            linearScaleRange1.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LimeGreen");//("Color:#9BBB59");
            linearScaleRange1.EndThickness = fStartThickness;//11F;
            linearScaleRange1.EndValue = intMax;//30F;
            linearScaleRange1.Name = "Range0";
            linearScaleRange1.ShapeOffset = fShapeOffSet;//10F;
            linearScaleRange1.StartThickness =fStartThickness;// 11F;
            linearScaleRange1.StartValue = (float)(intMax * 0.1);
            // Range 2
            linearScaleRange2.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#F4F56B");
            linearScaleRange2.EndThickness = fStartThickness;
            linearScaleRange2.EndValue = (float)(intMax * 0.1);// 40F;
            linearScaleRange2.Name = "Range1";
            linearScaleRange2.ShapeOffset = fShapeOffSet;
            linearScaleRange2.StartThickness = fStartThickness;
            linearScaleRange2.StartValue = 0;// 30F;
            // Range 3
            linearScaleRange3.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#E73144");
            linearScaleRange3.EndThickness = fStartThickness;
            linearScaleRange3.EndValue = 0;//50F;
            linearScaleRange3.Name = "Range2";
            linearScaleRange3.ShapeOffset = fShapeOffSet;
            linearScaleRange3.StartThickness =fStartThickness;
            linearScaleRange3.StartValue = -intMax;// 40F;
            this.linearScaleComponent1.Ranges.AddRange(new DevExpress.XtraGauges.Core.Model.IRange[] {linearScaleRange1,linearScaleRange2,linearScaleRange3});
        }
        
        private void updateScale2(int intMax, decimal sumNetCost, decimal sumNetPrice)
        {
            //ArcScaleRange arcScaleRange1 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange2 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange3 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange8 = new ArcScaleRange();

            //float fx = 1F;
            //float fNeedle = 8F;

            //// Range 1
            //arcScaleRange1.AppearanceRange.ContentBrush = new SolidBrushObject(Color.FromArgb(0, 192, 0));
            //arcScaleRange1.StartValue = (float)(intMax * 0.7);
            //arcScaleRange1.EndValue = intMax;
            //arcScaleRange1.ShapeOffset = 6F;
            //arcScaleRange1.StartThickness = fx;
            //arcScaleRange1.EndThickness = fx;
            //// Range 2
            //arcScaleRange2.AppearanceRange.ContentBrush = new SolidBrushObject(Color.Orange);
            //arcScaleRange2.StartValue = (float)(intMax * 0.5);
            //arcScaleRange2.EndValue = (float)(intMax * 0.7);
            //arcScaleRange2.ShapeOffset = 6F;
            //arcScaleRange2.StartThickness = fx;
            //arcScaleRange2.EndThickness = fx;
            //// Range 3
            //arcScaleRange3.AppearanceRange.ContentBrush = new SolidBrushObject(Color.Red);
            //arcScaleRange3.StartValue = 0;
            //arcScaleRange3.EndValue = (float)(intMax * 0.5);
            //arcScaleRange3.ShapeOffset = 6F;
            //arcScaleRange3.StartThickness = fx;
            //arcScaleRange3.EndThickness = fx;

            ////Range 8
            //arcScaleRange8.AppearanceRange.ContentBrush = new SolidBrushObject(Color.LimeGreen);
            //arcScaleRange8.StartValue = 0;
            //arcScaleRange8.EndValue = (float)sumNetPrice;
            //arcScaleRange8.ShapeOffset = -50F;
            //arcScaleRange8.StartThickness = fNeedle;
            //arcScaleRange8.EndThickness = fNeedle;

            //this.arcScaleComponent2.Ranges.AddRange(new IRange[] { arcScaleRange1, arcScaleRange2, arcScaleRange3, arcScaleRange8 });

            //ArcScaleRange arcScaleRange4 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange5 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange6 = new ArcScaleRange();
            //ArcScaleRange arcScaleRange7 = new ArcScaleRange();

            //// Range 1
            //arcScaleRange4.AppearanceRange.ContentBrush = new SolidBrushObject(Color.Red);
            //arcScaleRange4.StartValue = (float)(intMax * 0.7);
            //arcScaleRange4.EndValue = intMax;
            //arcScaleRange4.ShapeOffset = 0F;
            //arcScaleRange4.StartThickness = fx;
            //arcScaleRange4.EndThickness = fx;
            //// Range 2
            //arcScaleRange5.AppearanceRange.ContentBrush = new SolidBrushObject(Color.Orange);
            //arcScaleRange5.StartValue = (float)(intMax * 0.5);
            //arcScaleRange5.EndValue = (float)(intMax * 0.7);
            //arcScaleRange5.ShapeOffset = 0F;
            //arcScaleRange5.StartThickness = fx;
            //arcScaleRange5.EndThickness = fx;
            //// Range 3
            //arcScaleRange6.AppearanceRange.ContentBrush = new SolidBrushObject(Color.FromArgb(0, 192, 0));
            //arcScaleRange6.StartValue = 0;
            //arcScaleRange6.EndValue = (float)(intMax * 0.5);
            //arcScaleRange6.ShapeOffset = 0F;
            //arcScaleRange6.StartThickness = fx;
            //arcScaleRange6.EndThickness = fx;

            ////Range 7

            //arcScaleRange7.AppearanceRange.ContentBrush = new SolidBrushObject(Color.Red);
            //arcScaleRange7.StartValue = 0;
            //arcScaleRange7.EndValue = (float)sumNetCost;
            //arcScaleRange7.ShapeOffset = -50F;
            //arcScaleRange7.StartThickness = fNeedle;
            //arcScaleRange7.EndThickness = fNeedle;

            //this.arcScaleComponent4.Ranges.AddRange(new IRange[] { arcScaleRange4, arcScaleRange5, arcScaleRange6, arcScaleRange7});
        }
        
    }
}
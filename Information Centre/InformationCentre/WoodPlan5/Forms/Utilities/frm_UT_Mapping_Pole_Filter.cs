﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;


namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Pole_Filter : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string _PassedInClientIDs = "";
        public string _PassedInRegionIDs = "";
        public string _PassedInSubAreaIDs = "";
        public string _PassedInPrimaryIDs = "";
        public string _PassedInFeederIDs = "";
        public string _PassedInCircuitIDs = "";

        public string i_str_selected_client_ids = "";
        public string i_str_selected_client_names = "";
        public string i_str_selected_region_ids = "";
        public string i_str_selected_region_names = "";
        public string i_str_selected_subarea_ids = "";
        public string i_str_selected_subarea_names = "";
        public string i_str_selected_primary_ids = "";
        public string i_str_selected_primary_names = "";
        public string i_str_selected_feeder_ids = "";
        public string i_str_selected_feeder_names = "";
        public string i_str_selected_circuit_ids = "";
        public string i_str_selected_circuit_names = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;
        BaseObjects.GridCheckMarksSelection selection5;
        BaseObjects.GridCheckMarksSelection selection6;

        #endregion

        public frm_UT_Mapping_Pole_Filter()
        {
            InitializeComponent();
            this.Size = new Size(827, 391);
        }

        private void frm_UT_Mapping_Pole_Filter_Load(object sender, EventArgs e)
        {
            this.FormID = 500012;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp07001_UT_Client_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07001_UT_Client_FilterTableAdapter.Fill(dataSet_UT.sp07001_UT_Client_Filter);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
            }
            gridControl2.ForceInitialize();

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl5.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl6.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl6.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl7.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl7.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl8.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection6 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl8.MainView);
            selection6.CheckMarkColumn.VisibleIndex = 0;
            selection6.CheckMarkColumn.Width = 30;

            popupContainerControlClients.Size = new System.Drawing.Size(312, 423);
            popupContainerControlRegions.Size = new System.Drawing.Size(312, 423);
            popupContainerControlSubAreas.Size = new System.Drawing.Size(312, 423);
            popupContainerControlPrimarys.Size = new System.Drawing.Size(312, 423);
            popupContainerControlFeeders.Size = new System.Drawing.Size(312, 423);
            popupContainerControlCircuits.Size = new System.Drawing.Size(512, 523);

            Application.DoEvents();  // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //


            // Passed In Clients //
            int intFoundRow = 0;
            string strItemFilter1 = _PassedInClientIDs;
            char[] delimiters = new char[] { ',' };
            if (!string.IsNullOrEmpty(strItemFilter1))
            {
                Array arrayItems = strItemFilter1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                GridView viewFilter = (GridView)gridControl2.MainView;
                viewFilter.BeginUpdate();
                intFoundRow = 0;
                foreach (string strElement in arrayItems)
                {
                    if (strElement == "") break;
                    intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ClientID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        viewFilter.MakeRowVisible(intFoundRow, false);
                    }
                }
                viewFilter.EndUpdate();
                popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                Load_Region_Filter();

                // Passed In Regions //
                intFoundRow = 0;
                strItemFilter1 = _PassedInRegionIDs;
                if (!string.IsNullOrEmpty(strItemFilter1))
                {
                    arrayItems = strItemFilter1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    viewFilter = (GridView)gridControl4.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["RegionID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit2_Get_Selected();
                    Load_SubArea_Filter();
                }

                // Passed In SubArea //
                intFoundRow = 0;
                strItemFilter1 = _PassedInSubAreaIDs;
                if (!string.IsNullOrEmpty(strItemFilter1))
                {
                    arrayItems = strItemFilter1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["SubAreaID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit3.EditValue = PopupContainerEdit3_Get_Selected();
                    Load_Primary_Filter();
                }

                // Passed In Primary //
                intFoundRow = 0;
                strItemFilter1 = _PassedInPrimaryIDs;
                if (!string.IsNullOrEmpty(strItemFilter1))
                {
                    arrayItems = strItemFilter1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    viewFilter = (GridView)gridControl6.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["PrimaryID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit4.EditValue = PopupContainerEdit4_Get_Selected();
                    Load_Feeder_Filter();
                }

                // Passed In Feeder //
                intFoundRow = 0;
                strItemFilter1 = _PassedInFeederIDs;
                if (!string.IsNullOrEmpty(strItemFilter1))
                {
                    arrayItems = strItemFilter1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    viewFilter = (GridView)gridControl7.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["FeederID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEdit5.EditValue = PopupContainerEdit5_Get_Selected();
                }

            }
 
        }

        public override void PostLoadView(object objParameter)
        {
        }


        #region Client Filter Panel

        private void btnClientFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Clients - Close the screen and re-open.", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalClientIDs = i_str_selected_client_ids;
            e.Value = PopupContainerEdit1_Get_Selected();
            if (strOriginalClientIDs != i_str_selected_client_ids)  // Only load if a the parent level selection changed //
            {
                Load_Region_Filter();
                Load_SubArea_Filter();
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_client_ids = "";    // Reset any prior values first //
            i_str_selected_client_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_client_ids = "";
                return "No Client Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_client_ids += Convert.ToString(view.GetRowCellValue(i, "ClientID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_client_names = Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_client_names += ", " + Convert.ToString(view.GetRowCellValue(i, "ClientName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_client_names) ? "No Client Filter" : i_str_selected_client_names);
        }

        #endregion


        #region Region Filter Panel

        private void btnRegionFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Regions - Select one or more Clients from the Client Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalRegionIDs = i_str_selected_region_ids;
            e.Value = PopupContainerEdit2_Get_Selected();
            if (strOriginalRegionIDs != i_str_selected_region_ids)
            {
                Load_SubArea_Filter();  // Only load if a the parent level selection changed //
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_region_ids = "";    // Reset any prior values first //
            i_str_selected_region_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_region_ids = "";
                return "No Region Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_region_ids += Convert.ToString(view.GetRowCellValue(i, "RegionID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_region_names = Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_region_names += ", " + Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_region_names) ? "No Region Filter" : i_str_selected_region_names);
        }

        #endregion


        #region Sub-Area Filter Panel

        private void btnSubAreaFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Sub-Areas - Select one or more Regions from the Region Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalSubAreaIDs = i_str_selected_subarea_ids;
            e.Value = PopupContainerEdit3_Get_Selected();
            if (strOriginalSubAreaIDs != i_str_selected_subarea_ids)
            {
                Load_Primary_Filter();
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_subarea_ids = "";    // Reset any prior values first //
            i_str_selected_subarea_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_subarea_ids = "";
                return "No Sub-Area Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_subarea_ids += Convert.ToString(view.GetRowCellValue(i, "SubAreaID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_subarea_names = Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_subarea_names += ", " + Convert.ToString(view.GetRowCellValue(i, "SubAreaName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_subarea_names) ? "No Sub-Area Filter" : i_str_selected_subarea_names);
        }

        #endregion


        #region Primary Filter Panel

        private void btnPrimaryFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView6_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Primarys - Select one or more Sub-Areas from the Sub-Area Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit4_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            string strOriginalPrimaryIDs = i_str_selected_primary_ids;
            e.Value = PopupContainerEdit4_Get_Selected();
            if (strOriginalPrimaryIDs != i_str_selected_primary_ids)
            {
                Load_Feeder_Filter();
            }
        }

        private string PopupContainerEdit4_Get_Selected()
        {
            i_str_selected_primary_ids = "";    // Reset any prior values first //
            i_str_selected_primary_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl6.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_primary_ids = "";
                return "No Primary Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_primary_ids += Convert.ToString(view.GetRowCellValue(i, "PrimaryID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_primary_names = Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_primary_names += ", " + Convert.ToString(view.GetRowCellValue(i, "PrimaryName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_primary_names) ? "No Primary Filter" : i_str_selected_primary_names);
        }

        #endregion


        #region Feeder Filter Panel

        private void btnFeederFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView7_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Feeders - Select one or more Primarys from the Primary Filter first", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit5_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit5_Get_Selected();
        }

        private string PopupContainerEdit5_Get_Selected()
        {
            i_str_selected_feeder_ids = "";    // Reset any prior values first //
            i_str_selected_feeder_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl7.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_feeder_ids = "";
                return "No Feeder Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_feeder_ids += Convert.ToString(view.GetRowCellValue(i, "FeederID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_feeder_names = Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_feeder_names += ", " + Convert.ToString(view.GetRowCellValue(i, "FeederName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_feeder_names) ? "No Feeder Filter" : i_str_selected_feeder_names);
        }

        #endregion


        #region Circuit Filter Panel

        private void btnCircuitFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView8_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.RowCount != 0) return;
            using (StringFormat drawFormat = new StringFormat())
            {
                drawFormat.Alignment = drawFormat.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString("No Circuits - Select any pre-filters from the other filter lists first then click Refresh Circuits", e.Appearance.Font, SystemBrushes.ControlDark, new RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height), drawFormat);
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit6_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit6_Get_Selected();
        }

        private string PopupContainerEdit6_Get_Selected()
        {
            i_str_selected_circuit_ids = "";    // Reset any prior values first //
            i_str_selected_circuit_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl8.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";

            }
            else if (selection6.SelectedCount <= 0)
            {
                i_str_selected_circuit_ids = "";
                return "No Circuit Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_circuit_ids += Convert.ToString(view.GetRowCellValue(i, "CircuitID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_circuit_names = Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_circuit_names += ", " + Convert.ToString(view.GetRowCellValue(i, "CircuitName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_circuit_names) ? "No Circuit Filter" : i_str_selected_circuit_names);
        }

        #endregion


        private void Load_Region_Filter()
        {
            popupContainerEdit2.EditValue = "No Region Filter";
            gridControl4.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_client_ids))
            {
                dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client.Clear();
                selection2.ClearSelection();
            }
            else
            {
                sp07036_UT_Region_Popup_Filtered_By_ClientTableAdapter.Fill(dataSet_UT.sp07036_UT_Region_Popup_Filtered_By_Client, i_str_selected_client_ids);
            }
            gridControl4.EndUpdate();
        }

        private void Load_SubArea_Filter()
        {
            popupContainerEdit3.EditValue = "No Sub-Area Filter";
            gridControl5.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_region_ids))
            {
                dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region.Clear();
                selection3.ClearSelection();
            }
            else
            {
                sp07037_UT_SubArea_Popup_Filtered_By_RegionTableAdapter.Fill(dataSet_UT.sp07037_UT_SubArea_Popup_Filtered_By_Region, i_str_selected_region_ids);
            }
            gridControl5.EndUpdate();
        }

        private void Load_Primary_Filter()
        {
            popupContainerEdit4.EditValue = "No Primary Filter";
            gridControl6.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_subarea_ids))
            {
                dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea.Clear();
                selection4.ClearSelection();
            }
            else
            {
                sp07038_UT_Primary_Popup_Filtered_By_SubAreaTableAdapter.Fill(dataSet_UT.sp07038_UT_Primary_Popup_Filtered_By_SubArea, i_str_selected_subarea_ids);
            }
            gridControl6.EndUpdate();
        }

        private void Load_Feeder_Filter()
        {
            popupContainerEdit5.EditValue = "No Feeder Filter";
            gridControl7.BeginUpdate();
            if (string.IsNullOrEmpty(i_str_selected_primary_ids))
            {
                dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary.Clear();
                selection5.ClearSelection();
            }
            else
            {
                sp07039_UT_Feeder_Popup_Filtered_By_PrimaryTableAdapter.Fill(dataSet_UT.sp07039_UT_Feeder_Popup_Filtered_By_Primary, i_str_selected_primary_ids);
            }
            gridControl7.EndUpdate();
        }

        private void Clear_Circuit_Filter()
        {
            popupContainerEdit6.EditValue = "No Circuit Filter";
            i_str_selected_circuit_ids = "";
            selection6.ClearSelection();
        }

        private void btnRefreshCircuits_Click(object sender, EventArgs e)
        {
            //if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_client_ids == null) i_str_selected_client_ids = "";
            if (i_str_selected_region_ids == null) i_str_selected_region_ids = "";
            if (i_str_selected_subarea_ids == null) i_str_selected_subarea_ids = "";
            if (i_str_selected_primary_ids == null) i_str_selected_primary_ids = "";
            if (i_str_selected_feeder_ids == null) i_str_selected_feeder_ids = "";
            GridView view = (GridView)gridControl8.MainView;
            view.BeginUpdate();
            sp07040_UT_Circuit_Popup_Filtered_By_VariousTableAdapter.Fill(this.dataSet_UT.sp07040_UT_Circuit_Popup_Filtered_By_Various, i_str_selected_client_ids, i_str_selected_region_ids, i_str_selected_subarea_ids, i_str_selected_primary_ids, i_str_selected_feeder_ids);
            view.EndUpdate();
            Clear_Circuit_Filter();
            //if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


    }
}

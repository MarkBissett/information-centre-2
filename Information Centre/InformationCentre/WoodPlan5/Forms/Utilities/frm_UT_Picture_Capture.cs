﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraEditors;

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Picture_Capture : DevExpress.XtraEditors.XtraForm
    {
        #region Instance Variables

        public string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strFormMode = "";
        public int intAddToRecordID = 0;
        public string strSaveNameValue = "";  // Optional - set a value here to include it as part of the filename instead of intAddToRecordID value //
        public int intAddToRecordTypeID = 0;
        public int intPictureTypeID = 0;
        public int intCreatedByStaffID = 0;
        public int _PassedInEditImageID = 0;
        public string _PassedInFileNamePrefix = "";
        public string _PassedInFileNameSuffix = "";
        
        public bool _CalledByOperations = false;
        public string _PassedInImageName = "";
        public string _PassedInRemarks = "";

        public int _AddedPolePictureID = 0;
        public Image _returnedImage = null;
        public Image imageBackup = null;
        public string _returnedRemarks = "";
        public string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private bool ChangesOutstanding = false;
        private string strLoadedPictureName = "";
        private Point? _Previous = null;
        private Color _color = Color.Black;
        private Pen _Pen = new Pen(System.Drawing.Color.Red);
        
        private string _ObjectDrawn = "";
        private bool isMoving = false;
        private Point mouseDownPosition = Point.Empty;
        private Point mouseMovePosition = Point.Empty;
        private Dictionary<Point, Point> Circles = new Dictionary<Point, Point>();
        private int _ArrowHeadSize = 3;
        #endregion

        public frm_UT_Picture_Capture()
        {
            InitializeComponent();
        }

        private void frm_UT_Picture_Capture_Load(object sender, EventArgs e)
        {
            _Pen.Width = (float)5;
            beiPenColour.EditValue = Color.Red;

            if (string.IsNullOrEmpty(strDefaultPath))
            {
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            if (strFormMode == "edit" && _PassedInEditImageID != 0)
            {
                if (!_CalledByOperations)
                {
                    // Load Image and Remarks //
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp07125_UT_Survey_Picture_Edit", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@SurveyPictureID", _PassedInEditImageID));
                    cmd.Parameters.Add(new SqlParameter("@PassedInPath", strDefaultPath));
                    SqlDataAdapter sdaImage = new SqlDataAdapter(cmd);
                    DataSet dsImage = new DataSet("NewDataSet");
                    try
                    {
                        sdaImage.Fill(dsImage, "Table");
                        foreach (DataRow dr in dsImage.Tables[0].Rows)
                        {
                            Bitmap bitmap = new Bitmap(dr["PicturePath"].ToString());
                            pictureEdit1.Image = bitmap;
                            ChangesOutstanding = false;
                            memoEditPictureRemarks.Text = dr["Remarks"].ToString();
                            strLoadedPictureName = dr["PicturePath"].ToString();
                            imageBackup = (Image)pictureEdit1.Image.Clone();
                        }
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Image [" + ex.Message + "].\n\nClose the screen then try again.", "Load  Image for Editing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;

                    }
                    cmd = null;
                }
                else // Operations //
                {
                    try
                    {

                        Bitmap bitmap = new Bitmap(_PassedInImageName);
                        pictureEdit1.Image = bitmap;
                        ChangesOutstanding = false;
                        memoEditPictureRemarks.Text = _PassedInRemarks;
                        strLoadedPictureName = _PassedInImageName;
                        imageBackup = (Image)pictureEdit1.Image.Clone();
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Image [" + ex.Message + "].\n\nClose the screen then try again.", "Load  Image for Editing", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }
            }
            else
            {
                Application.DoEvents();
                Take_Picture();
            }
        }


        #region Picture Box

        private void pictureEdit1_MouseDown(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            isMoving = true;
            mouseDownPosition = e.Location;
            _Previous = e.Location;
        }

        private void pictureEdit1_MouseMove(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            if (_ObjectDrawn != "freehand")
            {
                if (isMoving)
                {
                    mouseMovePosition = e.Location;
                    pictureEdit1.Invalidate();
                }
            }
            else  // Freehand //
            {
                if (_Previous != null)
                {
                    if (pictureEdit1.Image == null)
                    {
                        Bitmap bmp = new Bitmap(pictureEdit1.Width, pictureEdit1.Height);
                        using (Graphics g = Graphics.FromImage(bmp))
                        {
                            g.Clear(Color.White);
                        }
                        pictureEdit1.Image = bmp;
                    }
                    using (Graphics g = Graphics.FromImage(pictureEdit1.Image))
                    {
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        g.DrawLine(_Pen, _Previous.Value, e.Location);
                    }
                    pictureEdit1.Invalidate();
                    _Previous = e.Location;
                    ChangesOutstanding = true;
                }
            }
        }

        private void pictureEdit1_MouseUp(object sender, MouseEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            isMoving = false;
            _Previous = null;

            if (pictureEdit1.Image == null)
            {
                Bitmap bmp = new Bitmap(pictureEdit1.Width, pictureEdit1.Height);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.White);
                }
                pictureEdit1.Image = bmp;
            }
            using (Graphics g = Graphics.FromImage(pictureEdit1.Image))
            {
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                switch (_ObjectDrawn)
                {
                    case "elypsis":
                        {
                            int intStartX = mouseDownPosition.X;
                            int intStartY = mouseDownPosition.Y;
                            int intEndX = mouseMovePosition.X;
                            int intEndY = mouseMovePosition.Y;
                            if (intEndX < intStartX)
                            {
                                intEndX = intStartX;
                                intStartX = mouseMovePosition.X - 2;
                            }
                            else
                            {
                                intStartX += -2;
                            }
                            if (intEndY < intStartY)
                            {
                                intEndY = intStartY;
                                intStartY = mouseMovePosition.Y - 2;
                            }
                            else
                            {
                                intStartY += -2;
                            }
                            g.DrawEllipse(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX - 2, intEndY - intStartY - 2)));
                            Circles.Clear();
                            pictureEdit1.Invalidate();
                            _Previous = e.Location;
                            ChangesOutstanding = true;
                        }
                        break;
                    case "rectangle":
                        {
                            int intStartX = mouseDownPosition.X;
                            int intStartY = mouseDownPosition.Y;
                            int intEndX = mouseMovePosition.X;
                            int intEndY = mouseMovePosition.Y;
                            if (intEndX < intStartX)
                            {
                                intEndX = intStartX;
                                intStartX = mouseMovePosition.X - 2;
                            }
                            else
                            {
                                intStartX += -2;
                            }
                            if (intEndY < intStartY)
                            {
                                intEndY = intStartY;
                                intStartY = mouseMovePosition.Y - 2;
                            }
                            else
                            {
                                intStartY += -2;
                            }
                            g.DrawRectangle(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX - 2, intEndY - intStartY - 2)));
                            Circles.Clear();
                            pictureEdit1.Invalidate();
                            _Previous = e.Location;
                            ChangesOutstanding = true;
                        }
                        break;
                    case "line":
                        {
                            if (mouseDownPosition != mouseMovePosition)
                            {
                                int intStartX = mouseDownPosition.X += -2;
                                int intStartY = mouseDownPosition.Y += -2;
                                int intEndX = mouseMovePosition.X += -2;
                                int intEndY = mouseMovePosition.Y += -2;
                                Point StartPoint = new Point(intStartX, intStartY);
                                Point EndPoint = new Point(intEndX, intEndY);
                                DrawArrow(g, StartPoint, EndPoint, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, false);
                            }
                            Circles.Clear();
                            pictureEdit1.Invalidate();
                            _Previous = e.Location;
                            ChangesOutstanding = true;
                        }
                        break;
                    case "arrow":
                        {
                            if (mouseDownPosition != mouseMovePosition)
                            {
                                int intStartX = mouseDownPosition.X += -2;
                                int intStartY = mouseDownPosition.Y += -2;
                                int intEndX = mouseMovePosition.X += -2;
                                int intEndY = mouseMovePosition.Y += -2;
                                Point StartPoint = new Point(intStartX, intStartY);
                                Point EndPoint = new Point(intEndX, intEndY);
                                DrawArrow(g, StartPoint, EndPoint, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, true);
                            }
                            Circles.Clear();
                            pictureEdit1.Invalidate();
                            _Previous = e.Location;
                            ChangesOutstanding = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void pictureEdit1_Paint(object sender, PaintEventArgs e)
        {
            if (_ObjectDrawn == "") return;
            var g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            if (isMoving)
            {
                int intStartX = mouseDownPosition.X;
                int intStartY = mouseDownPosition.Y;
                int intEndX = mouseMovePosition.X;
                int intEndY = mouseMovePosition.Y;
                if (intEndX < intStartX)
                {
                    intEndX = intStartX;
                    intStartX = mouseMovePosition.X;
                }
                if (intEndY < intStartY)
                {
                    intEndY = intStartY;
                    intStartY = mouseMovePosition.Y;
                }
                switch (_ObjectDrawn)
                {
                    case "elypsis":
                        {
                            g.DrawEllipse(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                        }
                        break;
                    case "rectangle":
                        {
                            g.DrawRectangle(_Pen, new Rectangle(new Point(intStartX, intStartY), new Size(intEndX - intStartX, intEndY - intStartY)));
                        }
                        break;
                    case "line":
                        {
                            if (mouseDownPosition != mouseMovePosition) DrawArrow(g, mouseDownPosition, mouseMovePosition, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, false);
                        }
                        break;
                    case "arrow":
                        {
                            if (mouseDownPosition != mouseMovePosition) DrawArrow(g, mouseDownPosition, mouseMovePosition, _Pen.Color, (int)_Pen.Width, _ArrowHeadSize, true);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void repositoryItemColorPickEdit1_EditValueChanged(object sender, EventArgs e)
        {
            ColorPickEdit cpe = (ColorPickEdit)sender;
            _Pen.Color = (Color)cpe.Color;
        }

        private void repositoryItemSpinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            _Pen.Width = (float)Convert.ToDecimal(se.EditValue);
        }

        private void pictureEdit1_ImageChanged(object sender, EventArgs e)
        {
            PictureEdit pe = (PictureEdit)sender;
            if (pe.Image != null)
            {
                bsiSelectedDrawingTool.Enabled = true;
                bbiRotateAntiClockwise.Enabled = true;
                bbiRotateClockwise.Enabled = true;
                pictureEdit1.Size = new Size((pe.Image.Width < 200 ? 200 : pe.Image.Width), (pe.Image.Height < 200 ? 200 : pe.Image.Height));
                ChangesOutstanding = true;
                bbiClearDrawing.Enabled = true;
            }
            else
            {
                bciNoDrawing.Checked = true;
                bsiSelectedDrawingTool.Enabled = false;

                bbiRotateAntiClockwise.Enabled = false;
                bbiRotateClockwise.Enabled = false;
                pictureEdit1.Size = new Size(200, 200);
                bbiClearDrawing.Enabled = false;
            }
        }

        private void bbiClearPicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = null;
            pictureEdit1.Size = new Size(200, 200);

            bciNoDrawing.Checked = true;
            bsiSelectedDrawingTool.Enabled = false;
            bbiRotateAntiClockwise.Enabled = false;
            bbiRotateClockwise.Enabled = false;
            ChangesOutstanding = false;
            bbiClearDrawing.Enabled = false;
        }

        #endregion


        private void bbiSavePicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (pictureEdit1.Image == null)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Save picture, no picture is loaded.", "Save Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strPictureRemarks = memoEditPictureRemarks.Text;
            // Save the picture as an Image //
            DataSet_UT_EditTableAdapters.QueriesTableAdapter SavePicture = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            SavePicture.ChangeConnectionString(strConnectionString);
            if (strFormMode == "add")  // Add new picture (Also for Operations Manager) //
            {
                if (intAddToRecordID == -999)  // Calling screen will handle the saving of the image so return image and close screen //
                {
                    _returnedImage = pictureEdit1.Image;
                    _returnedRemarks = memoEditPictureRemarks.Text;
                    pictureEdit1.Image = null;
                    ChangesOutstanding = false;
                    Close();
                    return;
                }
                try
                {
                    string strSavedFileNameNoPath = "";
                    string strSaveFileName = strDefaultPath;
                    if (!strSaveFileName.EndsWith("\\")) strSaveFileName += "\\";
                    string strPrefix = _PassedInFileNamePrefix;
                    if (string.IsNullOrEmpty(strPrefix)) strPrefix = "Survey";

                    strSavedFileNameNoPath = strPrefix + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + (string.IsNullOrWhiteSpace(strSaveNameValue) ? intAddToRecordID.ToString() : strSaveNameValue) + (!string.IsNullOrEmpty(_PassedInFileNameSuffix) ? "__" + _PassedInFileNameSuffix : "") + ".jpg";
                    strSaveFileName += strSavedFileNameNoPath;
                    pictureEdit1.Image.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);


                    /*
                    // Geocode Image //
                    double dLat = (double)51.62867;
                    double dLong = (double)0.41963;

                    byte[] bLat = BitConverter.GetBytes(dLat);
                    byte[] bLong = BitConverter.GetBytes(dLong);

                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(System.IO.File.ReadAllBytes(strSaveFileName)))
                    {
                        System.IO.File.Delete(strSaveFileName);
                        using (Image Pic = Image.FromStream(ms))
                        {
                            System.Drawing.Imaging.PropertyItem pi = Pic.PropertyItems[0];

                            pi.Id = 0x0002;
                            pi.Type = 5;
                            pi.Len = bLong.Length;
                            pi.Value = bLong;
                            Pic.SetPropertyItem(pi);

                            pi.Id = 0x0004;
                            pi.Value = bLat;
                            Pic.SetPropertyItem(pi);

                            Pic.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                    }
                    */

                    _AddedPolePictureID = Convert.ToInt32(SavePicture.sp07118_UT_Survey_Picture_Add(intAddToRecordID, intAddToRecordTypeID, intPictureTypeID, strSavedFileNameNoPath, DateTime.Now, intCreatedByStaffID, strPictureRemarks));
                    pictureEdit1.Image = null;
                    ChangesOutstanding = false;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurrred while saving the picture... [" + ex.Message + "].\r\n\r\nTry Saving again. If the problem persists contact Technical Support.", " Save Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else // Update existing picture //;
            {
                if (!_CalledByOperations)
                {
                    try
                    {
                        string strSavedFileNameNoPath = "";
                        string strSaveFileName = strDefaultPath;
                        if (!strSaveFileName.EndsWith("\\")) strSaveFileName += "\\";
                        string strPrefix = _PassedInFileNamePrefix;
                        if (string.IsNullOrEmpty(strPrefix)) strPrefix = "Survey";

                        strSavedFileNameNoPath = strPrefix + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + (string.IsNullOrWhiteSpace(strSaveNameValue) ? intAddToRecordID.ToString() : strSaveNameValue) + ".jpg";
                        strSaveFileName += strSavedFileNameNoPath;
                        pictureEdit1.Image.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        pictureEdit1.Image = null;
                        GC.GetTotalMemory(true);
                        GC.Collect();

                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SqlCommand cmd = null;
                        SQlConn.Open();
                        cmd = new SqlCommand("sp07126_UT_Survey_Picture_Update", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@SurveyPictureID", _PassedInEditImageID));
                        cmd.Parameters.Add(new SqlParameter("@PicturePath", strSavedFileNameNoPath));
                        cmd.Parameters.Add(new SqlParameter("@Remarks", strPictureRemarks));
                        cmd.ExecuteScalar();
                        cmd = null;
                        ChangesOutstanding = false;
                        SQlConn.Close();
                        SQlConn.Dispose();
                    }
                    catch (Exception ex)
                    {
                        _PassedInEditImageID = 0;
                        strLoadedPictureName = "";
                        strFormMode = "add";
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurrred while saving the picture... [" + ex.Message + "].\r\n\r\nTry Saving again. If the problem persists contact Technical Support.", " Save Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    _PassedInEditImageID = 0;
                    strLoadedPictureName = "";
                    strFormMode = "add";
                }
                else // Called by Operations Manager //
                {
                    _returnedImage = pictureEdit1.Image;
                    _returnedRemarks = memoEditPictureRemarks.Text;
                    pictureEdit1.Image = null;
                    ChangesOutstanding = false;
                    Close();
                    return;
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frm_UT_Picture_Capture_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (pictureEdit1.Image != null && ChangesOutstanding)
            {
                string strMessage = "There are one or more outstanding changes.\n\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        bbiSavePicture.PerformClick();
                        break;
                }
            }
        }

        private void bbiRotateAntiClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = RotateImage(pictureEdit1.Image, RotateFlipType.Rotate270FlipNone);
            pictureEdit1.Size = new Size((pictureEdit1.Image.Width < 200 ? 200 : pictureEdit1.Image.Width), (pictureEdit1.Image.Height < 200 ? 200 : pictureEdit1.Image.Height));
            ChangesOutstanding = true;
        }
        private void bbiRotateClockwise_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            pictureEdit1.Image = RotateImage(pictureEdit1.Image, RotateFlipType.Rotate90FlipNone);
            pictureEdit1.Size = new Size((pictureEdit1.Image.Width < 200 ? 200 : pictureEdit1.Image.Width), (pictureEdit1.Image.Height < 200 ? 200 : pictureEdit1.Image.Height));
            ChangesOutstanding = true;
        }
        public static Image RotateImage(Image img, RotateFlipType rotate_type)
        {
            img.RotateFlip(rotate_type);
            return img;
        }

        private void bbiTakePicture_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Take_Picture();
        }

        private void Take_Picture()
        {
            frm_UT_Picture_Capture_Camera_Stream frmChild = new frm_UT_Picture_Capture_Camera_Stream();
            frmChild.ShowDialog();
            if (frmChild.imageCaptured != null)
            {
                pictureEdit1.Image = frmChild.imageCaptured;
                imageBackup = (Image)pictureEdit1.Image.Clone();
            }
        }

        private void bbiClearDrawing_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear any drawing performed on top of the picture - proceed?", "Clear Drawing", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                _returnedImage = imageBackup;
                pictureEdit1.Image = _returnedImage;
                imageBackup = (Image)_returnedImage.Clone();
                pictureEdit1.Refresh();
            }
        }



        private void bciNoDrawing_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "";
            isMoving = false;
            _Previous = null;
            pictureEdit1.Cursor = Cursors.Arrow;
            bsiSelectedDrawingTool.Caption = "Draw [None]";
        }

        private void bciDrawFreehand_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "freehand";
            isMoving = false;
            _Previous = null;
            try
            {
                pictureEdit1.Cursor = BaseObjects.LoadCursor.LoadCustomCursor(Application.StartupPath + "\\Pencil.cur");
            }
            catch (Exception)
            {
                pictureEdit1.Cursor = Cursors.Arrow;
            }
            bsiSelectedDrawingTool.Caption = "Draw [Freehand]";
        }

        private void bciDrawRectangle_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "rectangle";
            isMoving = false;
            _Previous = null;
            pictureEdit1.Cursor = Cursors.Cross;
            bsiSelectedDrawingTool.Caption = "Draw [Rectangle]";
        }

        private void bciDrawElypsis_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "elypsis";
            isMoving = false;
            _Previous = null;
            pictureEdit1.Cursor = Cursors.Cross;
            bsiSelectedDrawingTool.Caption = "Draw [Elypsis]";
        }

        private void bciDrawLine_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "line";
            isMoving = false;
            _Previous = null;
            pictureEdit1.Cursor = Cursors.Cross;
            bsiSelectedDrawingTool.Caption = "Draw [Line]";
        }

        private void bciDrawArrow_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _ObjectDrawn = "arrow";
            isMoving = false;
            _Previous = null;
            pictureEdit1.Cursor = Cursors.Cross;
            bsiSelectedDrawingTool.Caption = "Draw [Arrow]";
            beiArrowSize.Enabled = bciDrawArrow.Checked;
        }

        private void DrawArrow(Graphics g, Point ArrowStart, Point ArrowEnd, Color ArrowColor, int LineWidth, int ArrowMultiplier, bool IncludeEndShape)
        {
            // draw the line //
            g.DrawLine(_Pen, ArrowStart, ArrowEnd);

            // determine the coords for the arrow point //
            // tip of the arrow //
            PointF arrowPoint = ArrowEnd;

            // determine arrow length //
            double arrowLength = Math.Sqrt(Math.Pow(Math.Abs(ArrowStart.X - ArrowEnd.X), 2) + Math.Pow(Math.Abs(ArrowStart.Y - ArrowEnd.Y), 2));

            // determine arrow angle //
            double arrowAngle = Math.Atan2(Math.Abs(ArrowStart.Y - ArrowEnd.Y), Math.Abs(ArrowStart.X - ArrowEnd.X));

            // get the x,y of the back of the point - to change from an arrow to a diamond, change the 3 in the next if/else blocks to 6
            double pointX, pointY;
            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Cos(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)));
            }
            else
            {
                pointX = Math.Cos(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Sin(arrowAngle) * (arrowLength - (3 * ArrowMultiplier)));
            }
            else
            {
                pointY = (Math.Sin(arrowAngle) * (arrowLength - (3 * ArrowMultiplier))) + ArrowStart.Y;
            }

            PointF arrowPointBack = new PointF((float)pointX, (float)pointY);

            // get the secondary angle of the left tip //
            double angleB = Math.Atan2((3 * ArrowMultiplier), (arrowLength - (3 * ArrowMultiplier)));

            double angleC = Math.PI * (90 - (arrowAngle * (180 / Math.PI)) - (angleB * (180 / Math.PI))) / 180;

            // get the secondary length //
            double secondaryLength = (3 * ArrowMultiplier) / Math.Sin(angleB);

            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Sin(angleC) * secondaryLength);
            }
            else
            {
                pointX = (Math.Sin(angleC) * secondaryLength) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Cos(angleC) * secondaryLength);
            }
            else
            {
                pointY = (Math.Cos(angleC) * secondaryLength) + ArrowStart.Y;
            }

            // get the left point //
            PointF arrowPointLeft = new PointF((float)pointX, (float)pointY);

            // move to the right point //
            angleC = arrowAngle - angleB;

            if (ArrowStart.X > ArrowEnd.X)
            {
                pointX = ArrowStart.X - (Math.Cos(angleC) * secondaryLength);
            }
            else
            {
                pointX = (Math.Cos(angleC) * secondaryLength) + ArrowStart.X;
            }

            if (ArrowStart.Y > ArrowEnd.Y)
            {
                pointY = ArrowStart.Y - (Math.Sin(angleC) * secondaryLength);
            }
            else
            {
                pointY = (Math.Sin(angleC) * secondaryLength) + ArrowStart.Y;
            }

            PointF arrowPointRight = new PointF((float)pointX, (float)pointY);

            // create the point list //
            PointF[] arrowPoints = new PointF[4];
            arrowPoints[0] = arrowPoint;
            arrowPoints[1] = arrowPointLeft;
            arrowPoints[2] = arrowPointBack;
            arrowPoints[3] = arrowPointRight;

            // draw the outline //
            if (IncludeEndShape) g.DrawPolygon(_Pen, arrowPoints);

            // fill the polygon //
            if (IncludeEndShape) g.FillPolygon(new SolidBrush(ArrowColor), arrowPoints);
        }

        private void repositoryItemSpinEdit2_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            _ArrowHeadSize = Convert.ToInt32(se.EditValue);
        }




    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors;

using MapInfo.Engine;
using MapInfo.Geometry;
using MapInfo.Mapping;

using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_UT_Utilities : BaseObjects.frmBase
    {

        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        private string strDefaultMappingProjection = "";
        #endregion


        public frm_UT_Utilities()
        {
            InitializeComponent();
        }

        private void frm_UT_Utilities_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            // Get Default Mapping Projection //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            strDefaultMappingProjection = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesDefaultMappingProjection").ToString();
            if (string.IsNullOrEmpty(strDefaultMappingProjection)) strDefaultMappingProjection = "CoordSys Earth Projection 8, 79, \"m\", -2, 49, 0.9996012717, 400000, -100000";
        }

        #region Synchronise Page

        private void btnClose2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSynchronise_Click(object sender, EventArgs e)
        {
            bool boolPoles = checkEditPoles.Checked;
            bool boolTreess = checkEditTrees.Checked;
            int intDataAmount = (checkEditAllData.Checked ? 0 : 1);

            if (!boolPoles && !boolTreess)
            {
                XtraMessageBox.Show("Select one or more Update Data Types before proceeding.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (XtraMessageBox.Show("You are about to Synchronise the Lat \\ Long Values with the Easting and Northing Values.\n\nNote that this process may take some time to complete.\n\nAre you sure you wish to proceed?", "Synchronise Lat \\ Long Values", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;

            // Checks passed so delete selected record(s) //
            int intUpdateProgressThreshhold = 0;
            int intUpdateProgressTempCount = 0;
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Synchronising Lat \\ Long...");
            fProgress.Show();
            Application.DoEvents();

            if (boolPoles)
            {
                fProgress.UpdateCaption("Sync Lat \\ Long - Poles...");
                int intDataType = 0;
                SqlDataAdapter sdaRecords = new SqlDataAdapter();
                DataSet dsRecords = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp07353_UT_Mapping_Utilities_Get_Required_Lat_Long_Sync", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@intDataAmount", intDataAmount));
                cmd.Parameters.Add(new SqlParameter("@intDataType", intDataType));
                dsRecords.Clear();  // Remove old values first //
                sdaRecords = new SqlDataAdapter(cmd);
                try
                {
                    sdaRecords.Fill(dsRecords, "Table");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while loading the Tree records for synchronisation - [" + ex.Message + "]. Process aborted.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                intUpdateProgressThreshhold = dsRecords.Tables[0].Rows.Count / 10;
                intUpdateProgressTempCount = 0;

                string strType = "";
                float dX = (float)0.00;
                float dY = (float)0.00;
                string strNewPolyXY = "";

                Mapping_Functions MapFunctions = new Mapping_Functions();
                DataSet_UT_MappingTableAdapters.QueriesTableAdapter SyncData = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                SyncData.ChangeConnectionString(strConnectionString);

                foreach (DataRow dr in dsRecords.Tables[0].Rows)
                {
                    dX = (float)(double)dr["XCoordinate"];
                    dY = (float)(double)dr["YCoordinate"];
                    strNewPolyXY = dr["strPolygonXY"].ToString();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords(dX, dY, strNewPolyXY);
                    if (ftr == null) continue; // Skip to next row //
                    MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref dX, ref dY, ref strNewPolyXY);
                    // Update Back-End //
                    try
                    {
                        SyncData.sp07354_UT_Mapping_Utilities_Set_Lat_Long(0, Convert.ToInt32(dr["RecordID"]), (double)dX, (double)dY, strNewPolyXY);
                    }
                    catch (Exception ex)
                    {
                    }

                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
               }
            }
            if (boolTreess)
            {
                fProgress.UpdateCaption("Sync Lat \\ Long - Trees...");
                fProgress.SetProgressValue(0);
                int intDataType = 1;
                SqlDataAdapter sdaRecords = new SqlDataAdapter();
                DataSet dsRecords = new DataSet("NewDataSet");
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = new SqlCommand("sp07353_UT_Mapping_Utilities_Get_Required_Lat_Long_Sync", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@intDataAmount", intDataAmount));
                cmd.Parameters.Add(new SqlParameter("@intDataType", intDataType));
                dsRecords.Clear();  // Remove old values first //
                sdaRecords = new SqlDataAdapter(cmd);
                try
                {
                    sdaRecords.Fill(dsRecords, "Table");
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while loading the Asset records for synchronisation - [" + ex.Message + "]. Process aborted.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                intUpdateProgressThreshhold = dsRecords.Tables[0].Rows.Count / 10;
                intUpdateProgressTempCount = 0;

                string strType = "";
                float dX = (float)0.00;
                float dY = (float)0.00;
                string strNewPolyXY = "";

                Mapping_Functions MapFunctions = new Mapping_Functions();
                DataSet_UT_MappingTableAdapters.QueriesTableAdapter SyncData = new DataSet_UT_MappingTableAdapters.QueriesTableAdapter();
                SyncData.ChangeConnectionString(strConnectionString);

                foreach (DataRow dr in dsRecords.Tables[0].Rows)
                {

                    dX = (float)Convert.ToDecimal(dr["XCoordinate"]);
                    dY = (float)Convert.ToDecimal(dr["YCoordinate"]);
                    strNewPolyXY = dr["strPolygonXY"].ToString();
                    MapInfo.Geometry.FeatureGeometry ftr = MapFunctions.CreateLatLongFeatureFromNonLatLongCoords(dX, dY, strNewPolyXY);
                    if (ftr == null) continue; // Skip to next row //
                    MapFunctions.GetLatLongCoordsFromLatLongFeature(ftr, ref dX, ref dY, ref strNewPolyXY);
                    // Update Back-End //
                    try
                    {
                        SyncData.sp07354_UT_Mapping_Utilities_Set_Lat_Long(1, Convert.ToInt32(dr["RecordID"]), (double)dX, (double)dY, strNewPolyXY);
                    }
                    catch (Exception ex)
                    {
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
               }
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            XtraMessageBox.Show("Syncronising of Lat \\ Long Data Completed Successfully.", "Synchronise Lat \\ Long Values", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

       #endregion




    }
}


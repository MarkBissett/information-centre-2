using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.IO;

using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;
using DevExpress.XtraEditors.Repository;  // Required by emptyEditor //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Shutdown_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private string strDefaultMapPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_UT_Shutdown_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Shutdown_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 50007600;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            emptyEditor = new RepositoryItem();

            sp07377_UT_Shutdown_Status_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07377_UT_Shutdown_Status_ListTableAdapter.Fill(dataSet_UT.sp07377_UT_Shutdown_Status_List);
            
            sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SurveyedPoleID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            
            sp07375_UT_Shutdown_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["DateCreated"] = DateTime.Now;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedBy"] = (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : GlobalSettings.UserSurname) + (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : ": ") + (string.IsNullOrEmpty(GlobalSettings.UserForename) ? "" : GlobalSettings.UserForename);
                        drNewRow["StatusID"] = 0;
                        dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows.Add(drNewRow);
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows.Add(drNewRow);
                        dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07375_UT_Shutdown_EditTableAdapter.Fill(dataSet_UT_Edit.sp07375_UT_Shutdown_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    if (strFormMode == "edit")
                    {
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Held-Up", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberTextEdit.Focus();

                        ReferenceNumberTextEdit.Properties.ReadOnly = false;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberTextEdit.Focus();

                        ReferenceNumberTextEdit.Properties.ReadOnly = false;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ReferenceNumberTextEdit.Focus();

                        ReferenceNumberTextEdit.Properties.ReadOnly = true;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                gridControl1.Enabled = false;
            }
            SetMenuStatus();  // Just in case any default layout has permissions set wrongly //
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource.EndEdit();
                dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                    bbiSave.Enabled = false;
                    bbiFormSave.Enabled = false;
                    return false;
                }
            }
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

 
            if (!iBool_AllowEdit || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intShutdownID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
            DateTime dtDoneDate = Convert.ToDateTime("01/01/1900");
            if (currentRow != null)
            {
                intShutdownID = (currentRow["ShutdownID"] == null ? 0 : Convert.ToInt32(currentRow["ShutdownID"]));
                dtDoneDate = (currentRow["DoneDate"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(currentRow["DoneDate"]));
            }

            if (i_int_FocusedGrid == 1)  // Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intShutdownID > 0 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intShutdownID > 0 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length == 1 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowEdit && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);  // Move Up Btn //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowEdit && dtDoneDate == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);  // Move Down Btn //
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }


        private void frm_UT_Shutdown_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus == 99)  // Refresh Top Level data //
            {
                Load_Top_Level();
            }
            else if (UpdateRefreshStatus == 1)
            {
                LoadLinkedActions();
            }
            SetMenuStatus();
        }

        private void frm_UT_Shutdown_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            // Commit any outstanding changes within the grid control //
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Surveyed Pole Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            // Save Work Order Header //
            this.sp07375UTShutdownEditBindingSource.EndEdit();
            try
            {
                this.sp07375_UT_Shutdown_EditTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Actions //
            this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource.EndEdit();
            try
            {
                this.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked action changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["ShutdownID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Shutdown_Manager")
                    {
                        var fParentForm = (frm_UT_Shutdown_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intLinkedRecordNew = 0;
            int intLinkedRecordModified = 0;
            int intLinkedRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07375_UT_Shutdown_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_Edit.sp07378_UT_Shutdown_Edit_Linked_Surveyed_Poles.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07378_UT_Shutdown_Edit_Linked_Surveyed_Poles.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted++;
                        break;
                }
            }


            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedRecordNew > 0 || intLinkedRecordModified > 0 || intLinkedRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Shutdown record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Shutdown record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Shutdown record(s)\n";
                if (intLinkedRecordNew > 0) strMessage += Convert.ToString(intLinkedRecordNew) + " New Linked Surveyed Pole record(s)\n";
                if (intLinkedRecordModified > 0) strMessage += Convert.ToString(intLinkedRecordModified) + " Updated Linked Surveyed Pole record(s)\n";
                if (intLinkedRecordDeleted > 0) strMessage += Convert.ToString(intLinkedRecordDeleted) + " Deleted Linked Surveyed Pole record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        public void Load_Top_Level()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            // Refresh main data since it has been changed by the Create Permission Document screen [may have a signature file and a physical PDF document] //
            try
            {
                sp07375_UT_Shutdown_EditTableAdapter.Fill(this.dataSet_UT_Edit.sp07375_UT_Shutdown_Edit, strRecordIDs, strFormMode);
            }
            catch (Exception)
            {
            }
        }

        private void LoadLinkedActions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strShutdownID = "";
            DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
            if (currentRow != null)
            {
                strShutdownID = (currentRow["ShutdownID"] == null ? "" : currentRow["ShutdownID"].ToString() + ",");
            }

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter.Fill(dataSet_UT_Edit.sp07378_UT_Shutdown_Edit_Linked_Surveyed_Poles, strShutdownID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
       }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Surveyed Poles Linked - Click Add button to link Surveyed Poles");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "WorkOrderSortOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "WorkOrderSortOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        // Clean Up any holes in the Sort Order Sequence... //
                        int intCorrectOrder = 0;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            intCorrectOrder++;
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                        }
                        view.EndDataUpdate();
                        view.EndSort();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "WorkOrderSortOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "WorkOrderSortOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        // Clean Up any holes in the Sort Order Sequence... //
                        int intCorrectOrder = 0;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            intCorrectOrder++;
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                        }
                        view.EndDataUpdate();
                        view.EndSort();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "TrafficMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "AccessMapPath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "TrafficMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("TrafficMapPath").ToString())) e.Cancel = true;
                    break;
                case "AccessMapPath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("AccessMapPath").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            switch (view.FocusedColumn.Name)
            {
                case "colTrafficMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "TrafficMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
                case "colAccessMapPath":
                    {
                        string strMap = view.GetRowCellValue(view.FocusedRowHandle, "AccessMapPath").ToString();
                        if (string.IsNullOrEmpty(strMap))
                        {
                            XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        strMap += ".gif";
                        string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                        frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                        frm_preview.strImage = strImagePath;
                        frm_preview.ShowDialog();
                    }
                    break;
            }
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        #endregion


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveFirst();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveFirst();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveFirst();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //
                                        bs.MovePrevious();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MovePrevious();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MovePrevious();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveNext();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveNext();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveNext();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveLast();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveLast();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveLast();
                        }
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Past Inspections... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadLinkedActions();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ReferenceNumberTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ReferenceNumberTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ReferenceNumberTextEdit, "");
            }

        }

        private void DoneDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void DoneDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;

            DateEdit de = (DateEdit)sender;
            if (de.DateTime != Convert.ToDateTime("01/01/0001"))
            {
                // Set all outstanding actions to complete //
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "DoneDate").ToString())) view.SetRowCellValue(i, "DoneDate", de.DateTime);
                }
            }
            SetMenuStatus();  // Make Sure Add Action button is enabled if appropriate //
        }

        private void ContractorNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
                if (currentRow == null) return;
                int intContractorID = (string.IsNullOrEmpty(currentRow["ContractorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ContractorID"]));
                frm_HR_Select_Contractor fChildForm = new frm_HR_Select_Contractor();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalContractorID = intContractorID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["ContractorID"] = fChildForm.intSelectedContractorID;
                    currentRow["ContractorName"] = fChildForm.strSelectedContractorName;
                    sp07375UTShutdownEditBindingSource.EndEdit();
                }
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        public void Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveyed Poles //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intShutdownID = (currentRow["ShutdownID"] == null ? 0 : Convert.ToInt32(currentRow["ShutdownID"]));
                        if (intShutdownID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Shutdown must be saved before adding Linked Surveyed Poles.", "Add Linked Surveyed Poles", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        string strExcludedRecords = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludedRecords += view.GetRowCellValue(i, "SurveyedPoleID").ToString() + ";";
                        }

                        frm_UT_Shutdown_Add_Surveyed_Poles fChildForm = new frm_UT_Shutdown_Add_Surveyed_Poles();
                        fChildForm.strExcludedSurveyedPoleIDs = strExcludedRecords;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Linking Surveyed Poles...");

                            view.BeginUpdate();
                            view.BeginSort();
                            DataTable dtNew = fChildForm.dtSelectedSurveyedPoles;
                            // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                            //dtNew.Columns.Remove("ActionPriority");
                            
                            foreach (DataRow dr in dtNew.Rows)
                            {
                                dr["ShutdownID"] = intShutdownID;
                                this.dataSet_UT_Edit.sp07378_UT_Shutdown_Edit_Linked_Surveyed_Poles.ImportRow(dr);
                            }
                            view.EndSort();
                            view.EndUpdate();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                        }
                    }
                    break;
            }
        }

        public void Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Tree Work //
                    {
                        // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
                        bool boolProceed = true;
                        string strMessage = CheckForPendingSave();
                        if (!string.IsNullOrEmpty(strMessage))
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before editing an action!\n\nSave Changes?", "Edit Surveyed Pole", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                            }
                            else
                            {
                                boolProceed = false;  // The user said no to save changes //
                            }
                        }
                        else  // No pending changes in screen //
                        {
                            boolProceed = true;
                        }
                        if (!boolProceed) return;

                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyedPoleID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Survey_Pole2 frmInstance = new frm_UT_Survey_Pole2();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        //frmInstance.strDefaultPath = strDefaultPicturePath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Linked Surveyed Poles //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Surveyed Poles to remove from the Work Order by clicking on them then try again.", "Remove Linked Surveyed Poles from Shutdown", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Surveyed Pole" : Convert.ToString(intRowHandles.Length) + " Linked Surveyed Poles") + " selected for removing from the current Shutdown!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Surveyed Pole" : "these Linked Surveyed Poles") + " will be removed from the work order. They will then be available for linking to a different shutdown.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Shutdown.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Shutdown.", "Remove Linked Surveyed Poles from Shutdown", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }


        public Boolean Action_Adding_Allowed(ref string strShutdownIDs)
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // strShutdownIDs passed by reference so original value is updated if there is a ShutdownID present on the form //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
            if (currentRow == null) return false;
            int intShutdownID = (currentRow["ShutdownID"] == null ? 0 : Convert.ToInt32(currentRow["ShutdownID"]));
            if (intShutdownID == 0) return false;

            DateTime dt = (currentRow["DoneDate"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["DoneDate"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                strShutdownIDs += intShutdownID.ToString() + ",";
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Return_ActionsID_To_Calling_Form()
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // Returns the ShutdownID if the work order is not completed. If completed it returns 0 //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp07375UTShutdownEditBindingSource.Current;
            if (currentRow == null) return 0;
            int intShutdownID = (currentRow["ShutdownID"] == null ? 0 : Convert.ToInt32(currentRow["ShutdownID"]));
            if (intShutdownID == 0) return 0;

            DateTime dt = (currentRow["DoneDate"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["DoneDate"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                return intShutdownID;
            }
            else
            {
                return 0;
            }
        }







    }
}


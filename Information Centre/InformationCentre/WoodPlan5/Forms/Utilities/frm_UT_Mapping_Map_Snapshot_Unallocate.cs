using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Map_Snapshot_Unallocate : BaseObjects.frmBase
    {
        #region Instance Variables

        public string strMapDescription;
        public string strMapRemarks;
        public string _LinkedToRecordDescription = "";

        #endregion
        
        public frm_UT_Mapping_Map_Snapshot_Unallocate()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Map_Snapshot_Unallocate_Load(object sender, EventArgs e)
        {
            this.Text += "  [Linked To: " + _LinkedToRecordDescription + "]";
            teMapDescription.EditValue = strMapDescription;
            meMapRemarks.EditValue = strMapRemarks;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            strMapDescription = teMapDescription.EditValue.ToString();
            strMapRemarks = meMapRemarks.EditValue.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}


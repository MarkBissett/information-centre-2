namespace WoodPlan5
{
    partial class frm_UT_pole_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_pole_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SubAreaNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp07043UTPoleItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.SubAreaIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleElectricalHazardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalHazardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditElectricalHazard = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07083UTElectricalHazardsListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleSiteHazardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteHazardID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditSiteHazard = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07078UTSiteHazardsListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleEnvironmentalIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnvironmentalIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditEnvronmentalIssue = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07073UTEnvironmentalIssueListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessIssueID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditAccessIssue = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07068UTAccessIssueListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colstrRecordIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MapIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TransformerNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IsTransformerCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ClientNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LongitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LatitudeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.XTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.YTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NextInspectionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.InspectionUnitGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07046UTInspectionCyclesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InspectionCycleSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.LastInspectionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.PoleIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoleTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07045UTPoleTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07006UTCircuitStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.CircuitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CircuitNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PoleNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForPoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCircuitName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPoleTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLastInspectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionCycle = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNextInspectionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInspectionUnit = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIsTransformer = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTransformerNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLongitude = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMapID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPoleNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07007UTVoltagesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07006_UT_Circuit_StatusTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter();
            this.sp07007_UT_Voltages_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07007_UT_Voltages_With_BlankTableAdapter();
            this.sp07043_UT_Pole_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07043_UT_Pole_ItemTableAdapter();
            this.sp07045_UT_Pole_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07045_UT_Pole_Types_With_BlankTableAdapter();
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter();
            this.sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter();
            this.sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter();
            this.sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter();
            this.sp07068_UT_Access_Issue_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07068_UT_Access_Issue_List_With_BlankTableAdapter();
            this.sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter();
            this.sp07078_UT_Site_Hazards_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07078_UT_Site_Hazards_List_With_BlankTableAdapter();
            this.sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07043UTPoleItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditElectricalHazard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07083UTElectricalHazardsListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSiteHazard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07078UTSiteHazardsListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditEnvronmentalIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07073UTEnvironmentalIssueListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAccessIssue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07068UTAccessIssueListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransformerNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsTransformerCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextInspectionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextInspectionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionUnitGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionCycleSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastInspectionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastInspectionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07045UTPoleTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNextInspectionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTransformer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransformerNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07007UTVoltagesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 615);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 589);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID5
            // 
            this.colID5.Caption = "ID";
            this.colID5.FieldName = "ID";
            this.colID5.Name = "colID5";
            this.colID5.OptionsColumn.AllowEdit = false;
            this.colID5.OptionsColumn.AllowFocus = false;
            this.colID5.OptionsColumn.ReadOnly = true;
            this.colID5.Width = 85;
            // 
            // colID4
            // 
            this.colID4.Caption = "ID";
            this.colID4.FieldName = "ID";
            this.colID4.Name = "colID4";
            this.colID4.OptionsColumn.AllowEdit = false;
            this.colID4.OptionsColumn.AllowFocus = false;
            this.colID4.OptionsColumn.ReadOnly = true;
            // 
            // colID3
            // 
            this.colID3.Caption = "ID";
            this.colID3.FieldName = "ID";
            this.colID3.Name = "colID3";
            this.colID3.OptionsColumn.AllowEdit = false;
            this.colID3.OptionsColumn.AllowFocus = false;
            this.colID3.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.Caption = "Access Issue ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Width = 97;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Inspection Cycle Unit ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // colID2
            // 
            this.colID2.Caption = "Pole Type ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 615);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 589);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 589);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "BlockAdd_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SubAreaNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SubAreaIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl4);
            this.dataLayoutControl1.Controls.Add(this.gridControl3);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.MapIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TransformerNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IsTransformerCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LongitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LatitudeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.XTextEdit);
            this.dataLayoutControl1.Controls.Add(this.YTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NextInspectionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionUnitGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.InspectionCycleSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.LastInspectionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.CircuitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PoleNumberButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07043UTPoleItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPoleID,
            this.ItemForCircuitID,
            this.ItemForCircuitNumber,
            this.ItemForClientID,
            this.ItemForClientName,
            this.layoutControlGroup3,
            this.ItemForCreatedByStaffID,
            this.ItemForSubAreaID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(809, 410, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 589);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SubAreaNameButtonEdit
            // 
            this.SubAreaNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "SubAreaName", true));
            this.SubAreaNameButtonEdit.EditValue = "";
            this.SubAreaNameButtonEdit.Location = new System.Drawing.Point(142, 163);
            this.SubAreaNameButtonEdit.MenuManager = this.barManager1;
            this.SubAreaNameButtonEdit.Name = "SubAreaNameButtonEdit";
            this.SubAreaNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SubAreaNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SubAreaNameButtonEdit.Size = new System.Drawing.Size(450, 20);
            this.SubAreaNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaNameButtonEdit.TabIndex = 13;
            this.SubAreaNameButtonEdit.TabStop = false;
            this.SubAreaNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubAreaNameButtonEdit_ButtonClick);
            // 
            // sp07043UTPoleItemBindingSource
            // 
            this.sp07043UTPoleItemBindingSource.DataMember = "sp07043_UT_Pole_Item";
            this.sp07043UTPoleItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SubAreaIDTextEdit
            // 
            this.SubAreaIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "SubAreaID", true));
            this.SubAreaIDTextEdit.Location = new System.Drawing.Point(130, 375);
            this.SubAreaIDTextEdit.MenuManager = this.barManager1;
            this.SubAreaIDTextEdit.Name = "SubAreaIDTextEdit";
            this.SubAreaIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubAreaIDTextEdit, true);
            this.SubAreaIDTextEdit.Size = new System.Drawing.Size(474, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubAreaIDTextEdit, optionsSpelling1);
            this.SubAreaIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaIDTextEdit.TabIndex = 50;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(130, 375);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(474, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling2);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 49;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(24, 456);
            this.gridControl4.MainView = this.gridView6;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemGridLookUpEditElectricalHazard});
            this.gridControl4.Size = new System.Drawing.Size(580, 109);
            this.gridControl4.TabIndex = 48;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource
            // 
            this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource.DataMember = "sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole";
            this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleElectricalHazardID,
            this.colPoleID3,
            this.colElectricalHazardID,
            this.colRemarks3,
            this.colGUID3,
            this.colstrMode3,
            this.colstrRecordIDs3});
            this.gridView6.GridControl = this.gridControl4;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView6.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colPoleElectricalHazardID
            // 
            this.colPoleElectricalHazardID.Caption = "Pole Electrical Hazard ID";
            this.colPoleElectricalHazardID.FieldName = "PoleElectricalHazardID";
            this.colPoleElectricalHazardID.Name = "colPoleElectricalHazardID";
            this.colPoleElectricalHazardID.OptionsColumn.AllowEdit = false;
            this.colPoleElectricalHazardID.OptionsColumn.AllowFocus = false;
            this.colPoleElectricalHazardID.OptionsColumn.ReadOnly = true;
            this.colPoleElectricalHazardID.Width = 137;
            // 
            // colPoleID3
            // 
            this.colPoleID3.Caption = "Pole ID";
            this.colPoleID3.FieldName = "PoleID";
            this.colPoleID3.Name = "colPoleID3";
            this.colPoleID3.OptionsColumn.AllowEdit = false;
            this.colPoleID3.OptionsColumn.AllowFocus = false;
            this.colPoleID3.OptionsColumn.ReadOnly = true;
            // 
            // colElectricalHazardID
            // 
            this.colElectricalHazardID.Caption = "Electrical Hazard";
            this.colElectricalHazardID.ColumnEdit = this.repositoryItemGridLookUpEditElectricalHazard;
            this.colElectricalHazardID.FieldName = "ElectricalHazardID";
            this.colElectricalHazardID.Name = "colElectricalHazardID";
            this.colElectricalHazardID.Visible = true;
            this.colElectricalHazardID.VisibleIndex = 0;
            this.colElectricalHazardID.Width = 210;
            // 
            // repositoryItemGridLookUpEditElectricalHazard
            // 
            this.repositoryItemGridLookUpEditElectricalHazard.AutoHeight = false;
            this.repositoryItemGridLookUpEditElectricalHazard.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditElectricalHazard.DataSource = this.sp07083UTElectricalHazardsListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditElectricalHazard.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditElectricalHazard.Name = "repositoryItemGridLookUpEditElectricalHazard";
            this.repositoryItemGridLookUpEditElectricalHazard.PopupView = this.gridView9;
            this.repositoryItemGridLookUpEditElectricalHazard.ValueMember = "ID";
            // 
            // sp07083UTElectricalHazardsListWithBlankBindingSource
            // 
            this.sp07083UTElectricalHazardsListWithBlankBindingSource.DataMember = "sp07083_UT_Electrical_Hazards_List_With_Blank";
            this.sp07083UTElectricalHazardsListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription5,
            this.colID5,
            this.colRecordOrder4});
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID5;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView9.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView9.OptionsView.ShowIndicator = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription5
            // 
            this.colDescription5.Caption = "Electrical Hazard";
            this.colDescription5.FieldName = "Description";
            this.colDescription5.Name = "colDescription5";
            this.colDescription5.OptionsColumn.AllowEdit = false;
            this.colDescription5.OptionsColumn.AllowFocus = false;
            this.colDescription5.OptionsColumn.ReadOnly = true;
            this.colDescription5.Visible = true;
            this.colDescription5.VisibleIndex = 0;
            this.colDescription5.Width = 210;
            // 
            // colRecordOrder4
            // 
            this.colRecordOrder4.Caption = "Order";
            this.colRecordOrder4.FieldName = "RecordOrder";
            this.colRecordOrder4.Name = "colRecordOrder4";
            this.colRecordOrder4.OptionsColumn.AllowEdit = false;
            this.colRecordOrder4.OptionsColumn.AllowFocus = false;
            this.colRecordOrder4.OptionsColumn.ReadOnly = true;
            this.colRecordOrder4.Width = 88;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 1;
            this.colRemarks3.Width = 250;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colGUID3
            // 
            this.colGUID3.Caption = "GUID";
            this.colGUID3.FieldName = "GUID";
            this.colGUID3.Name = "colGUID3";
            this.colGUID3.OptionsColumn.AllowEdit = false;
            this.colGUID3.OptionsColumn.AllowFocus = false;
            this.colGUID3.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode3
            // 
            this.colstrMode3.FieldName = "strMode";
            this.colstrMode3.Name = "colstrMode3";
            this.colstrMode3.OptionsColumn.AllowEdit = false;
            this.colstrMode3.OptionsColumn.AllowFocus = false;
            this.colstrMode3.OptionsColumn.ReadOnly = true;
            this.colstrMode3.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs3
            // 
            this.colstrRecordIDs3.FieldName = "strRecordIDs";
            this.colstrRecordIDs3.Name = "colstrRecordIDs3";
            this.colstrRecordIDs3.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs3.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs3.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs3.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrRecordIDs3.Width = 90;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(24, 456);
            this.gridControl3.MainView = this.gridView5;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemGridLookUpEditSiteHazard});
            this.gridControl3.Size = new System.Drawing.Size(580, 109);
            this.gridControl3.TabIndex = 47;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07076UTPoleSiteHazardsLinkedToPoleBindingSource
            // 
            this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource.DataMember = "sp07076_UT_Pole_Site_Hazards_Linked_To_Pole";
            this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleSiteHazardID,
            this.colPoleID2,
            this.colSiteHazardID,
            this.colRemarks2,
            this.colGUID2,
            this.colstrMode2,
            this.colstrRecordIDs2});
            this.gridView5.GridControl = this.gridControl3;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowFilterEditor = false;
            this.gridView5.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView5.OptionsFilter.AllowMRUFilterList = false;
            this.gridView5.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView5.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView5.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colPoleSiteHazardID
            // 
            this.colPoleSiteHazardID.Caption = "Pole Site Hazard ID";
            this.colPoleSiteHazardID.FieldName = "PoleSiteHazardID";
            this.colPoleSiteHazardID.Name = "colPoleSiteHazardID";
            this.colPoleSiteHazardID.OptionsColumn.AllowEdit = false;
            this.colPoleSiteHazardID.OptionsColumn.AllowFocus = false;
            this.colPoleSiteHazardID.OptionsColumn.ReadOnly = true;
            this.colPoleSiteHazardID.Width = 113;
            // 
            // colPoleID2
            // 
            this.colPoleID2.Caption = "Pole ID";
            this.colPoleID2.FieldName = "PoleID";
            this.colPoleID2.Name = "colPoleID2";
            this.colPoleID2.OptionsColumn.AllowEdit = false;
            this.colPoleID2.OptionsColumn.AllowFocus = false;
            this.colPoleID2.OptionsColumn.ReadOnly = true;
            // 
            // colSiteHazardID
            // 
            this.colSiteHazardID.Caption = "Site Hazard";
            this.colSiteHazardID.ColumnEdit = this.repositoryItemGridLookUpEditSiteHazard;
            this.colSiteHazardID.FieldName = "SiteHazardID";
            this.colSiteHazardID.Name = "colSiteHazardID";
            this.colSiteHazardID.Visible = true;
            this.colSiteHazardID.VisibleIndex = 0;
            this.colSiteHazardID.Width = 210;
            // 
            // repositoryItemGridLookUpEditSiteHazard
            // 
            this.repositoryItemGridLookUpEditSiteHazard.AutoHeight = false;
            this.repositoryItemGridLookUpEditSiteHazard.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditSiteHazard.DataSource = this.sp07078UTSiteHazardsListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditSiteHazard.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditSiteHazard.Name = "repositoryItemGridLookUpEditSiteHazard";
            this.repositoryItemGridLookUpEditSiteHazard.PopupView = this.gridView8;
            this.repositoryItemGridLookUpEditSiteHazard.ValueMember = "ID";
            // 
            // sp07078UTSiteHazardsListWithBlankBindingSource
            // 
            this.sp07078UTSiteHazardsListWithBlankBindingSource.DataMember = "sp07078_UT_Site_Hazards_List_With_Blank";
            this.sp07078UTSiteHazardsListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription4,
            this.colID4,
            this.colRecordOrder3});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID4;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView8.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription4
            // 
            this.colDescription4.Caption = "Site Hazard";
            this.colDescription4.FieldName = "Description";
            this.colDescription4.Name = "colDescription4";
            this.colDescription4.OptionsColumn.AllowEdit = false;
            this.colDescription4.OptionsColumn.AllowFocus = false;
            this.colDescription4.OptionsColumn.ReadOnly = true;
            this.colDescription4.Visible = true;
            this.colDescription4.VisibleIndex = 0;
            this.colDescription4.Width = 210;
            // 
            // colRecordOrder3
            // 
            this.colRecordOrder3.Caption = "Order";
            this.colRecordOrder3.FieldName = "RecordOrder";
            this.colRecordOrder3.Name = "colRecordOrder3";
            this.colRecordOrder3.OptionsColumn.AllowEdit = false;
            this.colRecordOrder3.OptionsColumn.AllowFocus = false;
            this.colRecordOrder3.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 1;
            this.colRemarks2.Width = 250;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID2
            // 
            this.colGUID2.Caption = "GUID";
            this.colGUID2.FieldName = "GUID";
            this.colGUID2.Name = "colGUID2";
            this.colGUID2.OptionsColumn.AllowEdit = false;
            this.colGUID2.OptionsColumn.AllowFocus = false;
            this.colGUID2.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode2
            // 
            this.colstrMode2.FieldName = "strMode";
            this.colstrMode2.Name = "colstrMode2";
            this.colstrMode2.OptionsColumn.AllowEdit = false;
            this.colstrMode2.OptionsColumn.AllowFocus = false;
            this.colstrMode2.OptionsColumn.ReadOnly = true;
            this.colstrMode2.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs2
            // 
            this.colstrRecordIDs2.FieldName = "strRecordIDs";
            this.colstrRecordIDs2.Name = "colstrRecordIDs2";
            this.colstrRecordIDs2.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs2.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs2.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs2.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrRecordIDs2.Width = 90;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(24, 456);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemGridLookUpEditEnvronmentalIssue});
            this.gridControl2.Size = new System.Drawing.Size(580, 109);
            this.gridControl2.TabIndex = 47;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource
            // 
            this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource.DataMember = "sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole";
            this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleEnvironmentalIssueID,
            this.colPoleID1,
            this.colEnvironmentalIssueID,
            this.colRemarks1,
            this.colGUID1,
            this.colstrMode1,
            this.colstrRecordIDs1});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowFilterEditor = false;
            this.gridView3.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView3.OptionsFilter.AllowMRUFilterList = false;
            this.gridView3.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView3.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView3.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colPoleEnvironmentalIssueID
            // 
            this.colPoleEnvironmentalIssueID.Caption = "Pole Environmental Issue ID";
            this.colPoleEnvironmentalIssueID.FieldName = "PoleEnvironmentalIssueID";
            this.colPoleEnvironmentalIssueID.Name = "colPoleEnvironmentalIssueID";
            this.colPoleEnvironmentalIssueID.OptionsColumn.AllowEdit = false;
            this.colPoleEnvironmentalIssueID.OptionsColumn.AllowFocus = false;
            this.colPoleEnvironmentalIssueID.OptionsColumn.ReadOnly = true;
            this.colPoleEnvironmentalIssueID.Width = 155;
            // 
            // colPoleID1
            // 
            this.colPoleID1.Caption = "Pole ID";
            this.colPoleID1.FieldName = "PoleID";
            this.colPoleID1.Name = "colPoleID1";
            this.colPoleID1.OptionsColumn.AllowEdit = false;
            this.colPoleID1.OptionsColumn.AllowFocus = false;
            this.colPoleID1.OptionsColumn.ReadOnly = true;
            // 
            // colEnvironmentalIssueID
            // 
            this.colEnvironmentalIssueID.Caption = "Environmental Issue";
            this.colEnvironmentalIssueID.ColumnEdit = this.repositoryItemGridLookUpEditEnvronmentalIssue;
            this.colEnvironmentalIssueID.FieldName = "EnvironmentalIssueID";
            this.colEnvironmentalIssueID.Name = "colEnvironmentalIssueID";
            this.colEnvironmentalIssueID.Visible = true;
            this.colEnvironmentalIssueID.VisibleIndex = 0;
            this.colEnvironmentalIssueID.Width = 210;
            // 
            // repositoryItemGridLookUpEditEnvronmentalIssue
            // 
            this.repositoryItemGridLookUpEditEnvronmentalIssue.AutoHeight = false;
            this.repositoryItemGridLookUpEditEnvronmentalIssue.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditEnvronmentalIssue.DataSource = this.sp07073UTEnvironmentalIssueListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditEnvronmentalIssue.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditEnvronmentalIssue.Name = "repositoryItemGridLookUpEditEnvronmentalIssue";
            this.repositoryItemGridLookUpEditEnvronmentalIssue.NullText = "";
            this.repositoryItemGridLookUpEditEnvronmentalIssue.PopupView = this.gridView7;
            this.repositoryItemGridLookUpEditEnvronmentalIssue.ValueMember = "ID";
            // 
            // sp07073UTEnvironmentalIssueListWithBlankBindingSource
            // 
            this.sp07073UTEnvironmentalIssueListWithBlankBindingSource.DataMember = "sp07073_UT_Environmental_Issue_List_With_Blank";
            this.sp07073UTEnvironmentalIssueListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription3,
            this.colID3,
            this.colRecordOrder2});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colID3;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Environmental Issue";
            this.colDescription3.FieldName = "Description";
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowEdit = false;
            this.colDescription3.OptionsColumn.AllowFocus = false;
            this.colDescription3.OptionsColumn.ReadOnly = true;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 210;
            // 
            // colRecordOrder2
            // 
            this.colRecordOrder2.Caption = "Order";
            this.colRecordOrder2.FieldName = "RecordOrder";
            this.colRecordOrder2.Name = "colRecordOrder2";
            this.colRecordOrder2.OptionsColumn.AllowEdit = false;
            this.colRecordOrder2.OptionsColumn.AllowFocus = false;
            this.colRecordOrder2.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 1;
            this.colRemarks1.Width = 250;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode1
            // 
            this.colstrMode1.FieldName = "strMode";
            this.colstrMode1.Name = "colstrMode1";
            this.colstrMode1.OptionsColumn.AllowEdit = false;
            this.colstrMode1.OptionsColumn.AllowFocus = false;
            this.colstrMode1.OptionsColumn.ReadOnly = true;
            this.colstrMode1.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs1
            // 
            this.colstrRecordIDs1.FieldName = "strRecordIDs";
            this.colstrRecordIDs1.Name = "colstrRecordIDs1";
            this.colstrRecordIDs1.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs1.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs1.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs1.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrRecordIDs1.Width = 90;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Add Multiple New Records", "block add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 456);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemGridLookUpEditAccessIssue});
            this.gridControl1.Size = new System.Drawing.Size(580, 109);
            this.gridControl1.TabIndex = 46;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07066UTPoleAccessIssuesLinkedToPoleBindingSource
            // 
            this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource.DataMember = "sp07066_UT_Pole_Access_Issues_Linked_To_Pole";
            this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleAccessIssueID,
            this.colPoleID,
            this.colAccessIssueID,
            this.colRemarks,
            this.colGUID,
            this.colstrMode,
            this.colstrRecordIDs});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colPoleAccessIssueID
            // 
            this.colPoleAccessIssueID.Caption = "Pole Access Issue ID";
            this.colPoleAccessIssueID.FieldName = "PoleAccessIssueID";
            this.colPoleAccessIssueID.Name = "colPoleAccessIssueID";
            this.colPoleAccessIssueID.OptionsColumn.AllowEdit = false;
            this.colPoleAccessIssueID.OptionsColumn.AllowFocus = false;
            this.colPoleAccessIssueID.OptionsColumn.ReadOnly = true;
            this.colPoleAccessIssueID.Width = 137;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colAccessIssueID
            // 
            this.colAccessIssueID.Caption = "Access Issue";
            this.colAccessIssueID.ColumnEdit = this.repositoryItemGridLookUpEditAccessIssue;
            this.colAccessIssueID.FieldName = "AccessIssueID";
            this.colAccessIssueID.Name = "colAccessIssueID";
            this.colAccessIssueID.Visible = true;
            this.colAccessIssueID.VisibleIndex = 0;
            this.colAccessIssueID.Width = 210;
            // 
            // repositoryItemGridLookUpEditAccessIssue
            // 
            this.repositoryItemGridLookUpEditAccessIssue.AutoHeight = false;
            this.repositoryItemGridLookUpEditAccessIssue.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditAccessIssue.DataSource = this.sp07068UTAccessIssueListWithBlankBindingSource;
            this.repositoryItemGridLookUpEditAccessIssue.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditAccessIssue.Name = "repositoryItemGridLookUpEditAccessIssue";
            this.repositoryItemGridLookUpEditAccessIssue.NullText = "";
            this.repositoryItemGridLookUpEditAccessIssue.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditAccessIssue.ValueMember = "ID";
            // 
            // sp07068UTAccessIssueListWithBlankBindingSource
            // 
            this.sp07068UTAccessIssueListWithBlankBindingSource.DataMember = "sp07068_UT_Access_Issue_List_With_Blank";
            this.sp07068UTAccessIssueListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colRecordOrder1});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Access Issue";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 210;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            this.colRemarks.Width = 250;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colstrMode
            // 
            this.colstrMode.FieldName = "strMode";
            this.colstrMode.Name = "colstrMode";
            this.colstrMode.OptionsColumn.AllowEdit = false;
            this.colstrMode.OptionsColumn.AllowFocus = false;
            this.colstrMode.OptionsColumn.ReadOnly = true;
            this.colstrMode.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colstrRecordIDs
            // 
            this.colstrRecordIDs.FieldName = "strRecordIDs";
            this.colstrRecordIDs.Name = "colstrRecordIDs";
            this.colstrRecordIDs.OptionsColumn.AllowEdit = false;
            this.colstrRecordIDs.OptionsColumn.AllowFocus = false;
            this.colstrRecordIDs.OptionsColumn.ReadOnly = true;
            this.colstrRecordIDs.OptionsColumn.ShowInCustomizationForm = false;
            this.colstrRecordIDs.Width = 80;
            // 
            // MapIDTextEdit
            // 
            this.MapIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "MapID", true));
            this.MapIDTextEdit.Location = new System.Drawing.Point(142, 163);
            this.MapIDTextEdit.MenuManager = this.barManager1;
            this.MapIDTextEdit.Name = "MapIDTextEdit";
            this.MapIDTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MapIDTextEdit, true);
            this.MapIDTextEdit.Size = new System.Drawing.Size(450, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MapIDTextEdit, optionsSpelling3);
            this.MapIDTextEdit.StyleController = this.dataLayoutControl1;
            this.MapIDTextEdit.TabIndex = 45;
            // 
            // TransformerNumberTextEdit
            // 
            this.TransformerNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "TransformerNumber", true));
            this.TransformerNumberTextEdit.Location = new System.Drawing.Point(383, 237);
            this.TransformerNumberTextEdit.MenuManager = this.barManager1;
            this.TransformerNumberTextEdit.Name = "TransformerNumberTextEdit";
            this.TransformerNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TransformerNumberTextEdit, true);
            this.TransformerNumberTextEdit.Size = new System.Drawing.Size(209, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TransformerNumberTextEdit, optionsSpelling4);
            this.TransformerNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.TransformerNumberTextEdit.TabIndex = 44;
            // 
            // IsTransformerCheckEdit
            // 
            this.IsTransformerCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "IsTransformer", true));
            this.IsTransformerCheckEdit.Location = new System.Drawing.Point(142, 237);
            this.IsTransformerCheckEdit.MenuManager = this.barManager1;
            this.IsTransformerCheckEdit.Name = "IsTransformerCheckEdit";
            this.IsTransformerCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsTransformerCheckEdit.Properties.ValueChecked = 1;
            this.IsTransformerCheckEdit.Properties.ValueUnchecked = 0;
            this.IsTransformerCheckEdit.Size = new System.Drawing.Size(131, 19);
            this.IsTransformerCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsTransformerCheckEdit.TabIndex = 43;
            // 
            // ClientNameTextEdit
            // 
            this.ClientNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "ClientName", true));
            this.ClientNameTextEdit.Location = new System.Drawing.Point(108, 103);
            this.ClientNameTextEdit.MenuManager = this.barManager1;
            this.ClientNameTextEdit.Name = "ClientNameTextEdit";
            this.ClientNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameTextEdit, true);
            this.ClientNameTextEdit.Size = new System.Drawing.Size(508, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameTextEdit, optionsSpelling5);
            this.ClientNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameTextEdit.TabIndex = 37;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(120, 330);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(484, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling6);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 36;
            // 
            // LongitudeTextEdit
            // 
            this.LongitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "Longitude", true));
            this.LongitudeTextEdit.Location = new System.Drawing.Point(142, 259);
            this.LongitudeTextEdit.MenuManager = this.barManager1;
            this.LongitudeTextEdit.Name = "LongitudeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LongitudeTextEdit, true);
            this.LongitudeTextEdit.Size = new System.Drawing.Size(450, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LongitudeTextEdit, optionsSpelling7);
            this.LongitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LongitudeTextEdit.TabIndex = 42;
            // 
            // LatitudeTextEdit
            // 
            this.LatitudeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "Latitude", true));
            this.LatitudeTextEdit.Location = new System.Drawing.Point(142, 235);
            this.LatitudeTextEdit.MenuManager = this.barManager1;
            this.LatitudeTextEdit.Name = "LatitudeTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatitudeTextEdit, true);
            this.LatitudeTextEdit.Size = new System.Drawing.Size(450, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatitudeTextEdit, optionsSpelling8);
            this.LatitudeTextEdit.StyleController = this.dataLayoutControl1;
            this.LatitudeTextEdit.TabIndex = 41;
            // 
            // XTextEdit
            // 
            this.XTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "X", true));
            this.XTextEdit.Location = new System.Drawing.Point(142, 187);
            this.XTextEdit.MenuManager = this.barManager1;
            this.XTextEdit.Name = "XTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.XTextEdit, true);
            this.XTextEdit.Size = new System.Drawing.Size(450, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.XTextEdit, optionsSpelling9);
            this.XTextEdit.StyleController = this.dataLayoutControl1;
            this.XTextEdit.TabIndex = 40;
            // 
            // YTextEdit
            // 
            this.YTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "Y", true));
            this.YTextEdit.Location = new System.Drawing.Point(142, 211);
            this.YTextEdit.MenuManager = this.barManager1;
            this.YTextEdit.Name = "YTextEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.YTextEdit, true);
            this.YTextEdit.Size = new System.Drawing.Size(450, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.YTextEdit, optionsSpelling10);
            this.YTextEdit.StyleController = this.dataLayoutControl1;
            this.YTextEdit.TabIndex = 39;
            // 
            // NextInspectionDateDateEdit
            // 
            this.NextInspectionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "NextInspectionDate", true));
            this.NextInspectionDateDateEdit.EditValue = null;
            this.NextInspectionDateDateEdit.Location = new System.Drawing.Point(154, 353);
            this.NextInspectionDateDateEdit.MenuManager = this.barManager1;
            this.NextInspectionDateDateEdit.Name = "NextInspectionDateDateEdit";
            this.NextInspectionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.NextInspectionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NextInspectionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.NextInspectionDateDateEdit.Properties.Mask.EditMask = "g";
            this.NextInspectionDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NextInspectionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.NextInspectionDateDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.NextInspectionDateDateEdit.Size = new System.Drawing.Size(426, 20);
            this.NextInspectionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.NextInspectionDateDateEdit.TabIndex = 35;
            // 
            // InspectionUnitGridLookUpEdit
            // 
            this.InspectionUnitGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "InspectionUnit", true));
            this.InspectionUnitGridLookUpEdit.Location = new System.Drawing.Point(385, 329);
            this.InspectionUnitGridLookUpEdit.MenuManager = this.barManager1;
            this.InspectionUnitGridLookUpEdit.Name = "InspectionUnitGridLookUpEdit";
            this.InspectionUnitGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InspectionUnitGridLookUpEdit.Properties.DataSource = this.sp07046UTInspectionCyclesWithBlankBindingSource;
            this.InspectionUnitGridLookUpEdit.Properties.DisplayMember = "Description";
            this.InspectionUnitGridLookUpEdit.Properties.NullText = "";
            this.InspectionUnitGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.InspectionUnitGridLookUpEdit.Properties.ValueMember = "ID";
            this.InspectionUnitGridLookUpEdit.Size = new System.Drawing.Size(195, 20);
            this.InspectionUnitGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.InspectionUnitGridLookUpEdit.TabIndex = 34;
            this.InspectionUnitGridLookUpEdit.EditValueChanged += new System.EventHandler(this.InspectionUnitGridLookUpEdit_EditValueChanged);
            this.InspectionUnitGridLookUpEdit.Validated += new System.EventHandler(this.InspectionUnitGridLookUpEdit_Validated);
            // 
            // sp07046UTInspectionCyclesWithBlankBindingSource
            // 
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataMember = "sp07046_UT_Inspection_Cycles_With_Blank";
            this.sp07046UTInspectionCyclesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn1;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Inspection Cycle Unit";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 201;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // InspectionCycleSpinEdit
            // 
            this.InspectionCycleSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "InspectionCycle", true));
            this.InspectionCycleSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.InspectionCycleSpinEdit.Location = new System.Drawing.Point(154, 329);
            this.InspectionCycleSpinEdit.MenuManager = this.barManager1;
            this.InspectionCycleSpinEdit.Name = "InspectionCycleSpinEdit";
            this.InspectionCycleSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.InspectionCycleSpinEdit.Properties.IsFloatValue = false;
            this.InspectionCycleSpinEdit.Properties.Mask.EditMask = "N00";
            this.InspectionCycleSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.InspectionCycleSpinEdit.Size = new System.Drawing.Size(121, 20);
            this.InspectionCycleSpinEdit.StyleController = this.dataLayoutControl1;
            this.InspectionCycleSpinEdit.TabIndex = 33;
            this.InspectionCycleSpinEdit.EditValueChanged += new System.EventHandler(this.InspectionCycleSpinEdit_EditValueChanged);
            this.InspectionCycleSpinEdit.Validated += new System.EventHandler(this.InspectionCycleSpinEdit_Validated);
            // 
            // LastInspectionDateDateEdit
            // 
            this.LastInspectionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "LastInspectionDate", true));
            this.LastInspectionDateDateEdit.EditValue = null;
            this.LastInspectionDateDateEdit.Location = new System.Drawing.Point(154, 305);
            this.LastInspectionDateDateEdit.MenuManager = this.barManager1;
            this.LastInspectionDateDateEdit.Name = "LastInspectionDateDateEdit";
            this.LastInspectionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LastInspectionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LastInspectionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LastInspectionDateDateEdit.Properties.Mask.EditMask = "g";
            this.LastInspectionDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LastInspectionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.LastInspectionDateDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.LastInspectionDateDateEdit.Size = new System.Drawing.Size(426, 20);
            this.LastInspectionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.LastInspectionDateDateEdit.TabIndex = 32;
            this.LastInspectionDateDateEdit.EditValueChanged += new System.EventHandler(this.LastInspectionDateDateEdit_EditValueChanged);
            this.LastInspectionDateDateEdit.Validated += new System.EventHandler(this.LastInspectionDateDateEdit_Validated);
            // 
            // PoleIDTextEdit
            // 
            this.PoleIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "PoleID", true));
            this.PoleIDTextEdit.Location = new System.Drawing.Point(98, 81);
            this.PoleIDTextEdit.MenuManager = this.barManager1;
            this.PoleIDTextEdit.Name = "PoleIDTextEdit";
            this.PoleIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PoleIDTextEdit, true);
            this.PoleIDTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PoleIDTextEdit, optionsSpelling11);
            this.PoleIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PoleIDTextEdit.TabIndex = 31;
            // 
            // PoleTypeIDGridLookUpEdit
            // 
            this.PoleTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "PoleTypeID", true));
            this.PoleTypeIDGridLookUpEdit.Location = new System.Drawing.Point(142, 211);
            this.PoleTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.PoleTypeIDGridLookUpEdit.Name = "PoleTypeIDGridLookUpEdit";
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.PoleTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PoleTypeIDGridLookUpEdit.Properties.DataSource = this.sp07045UTPoleTypesWithBlankBindingSource;
            this.PoleTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.PoleTypeIDGridLookUpEdit.Properties.NullText = "";
            this.PoleTypeIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.PoleTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.PoleTypeIDGridLookUpEdit.Size = new System.Drawing.Size(450, 22);
            this.PoleTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PoleTypeIDGridLookUpEdit.TabIndex = 27;
            this.PoleTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PoleTypeIDGridLookUpEdit_ButtonClick);
            // 
            // sp07045UTPoleTypesWithBlankBindingSource
            // 
            this.sp07045UTPoleTypesWithBlankBindingSource.DataMember = "sp07045_UT_Pole_Types_With_Blank";
            this.sp07045UTPoleTypesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.colID2;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Pole Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.EditValue = "";
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(142, 187);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp07006UTCircuitStatusBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(450, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 26;
            // 
            // sp07006UTCircuitStatusBindingSource
            // 
            this.sp07006UTCircuitStatusBindingSource.DataMember = "sp07006_UT_Circuit_Status";
            this.sp07006UTCircuitStatusBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription1,
            this.colItemOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID1
            // 
            this.colID1.Caption = "Status ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 174;
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07043UTPoleItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(118, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(236, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // CircuitIDTextEdit
            // 
            this.CircuitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "CircuitID", true));
            this.CircuitIDTextEdit.Location = new System.Drawing.Point(98, 77);
            this.CircuitIDTextEdit.MenuManager = this.barManager1;
            this.CircuitIDTextEdit.Name = "CircuitIDTextEdit";
            this.CircuitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitIDTextEdit, true);
            this.CircuitIDTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitIDTextEdit, optionsSpelling12);
            this.CircuitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitIDTextEdit.TabIndex = 4;
            // 
            // CircuitNumberTextEdit
            // 
            this.CircuitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "CircuitNumber", true));
            this.CircuitNumberTextEdit.Location = new System.Drawing.Point(98, 87);
            this.CircuitNumberTextEdit.MenuManager = this.barManager1;
            this.CircuitNumberTextEdit.Name = "CircuitNumberTextEdit";
            this.CircuitNumberTextEdit.Properties.MaxLength = 50;
            this.CircuitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNumberTextEdit, true);
            this.CircuitNumberTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNumberTextEdit, optionsSpelling13);
            this.CircuitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNumberTextEdit.TabIndex = 6;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 163);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(556, 222);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling14);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // CircuitNameButtonEdit
            // 
            this.CircuitNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "CircuitName", true));
            this.CircuitNameButtonEdit.EditValue = "";
            this.CircuitNameButtonEdit.Location = new System.Drawing.Point(118, 35);
            this.CircuitNameButtonEdit.MenuManager = this.barManager1;
            this.CircuitNameButtonEdit.Name = "CircuitNameButtonEdit";
            this.CircuitNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CircuitNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CircuitNameButtonEdit.Size = new System.Drawing.Size(498, 20);
            this.CircuitNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNameButtonEdit.TabIndex = 5;
            this.CircuitNameButtonEdit.TabStop = false;
            this.CircuitNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CircuitNameButtonEdit_ButtonClick);
            this.CircuitNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CircuitNameButtonEdit_Validating);
            // 
            // PoleNumberButtonEdit
            // 
            this.PoleNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07043UTPoleItemBindingSource, "PoleNumber", true));
            this.PoleNumberButtonEdit.Location = new System.Drawing.Point(118, 59);
            this.PoleNumberButtonEdit.MenuManager = this.barManager1;
            this.PoleNumberButtonEdit.Name = "PoleNumberButtonEdit";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Sequence button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to <b>open</b> the <b>Choose Sequence screen</b> to <b>select the prefix" +
    "</b> for the sequence.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem5.Text = "Number button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>calculate</b> the <b>number suffix</b> for the sequence.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.PoleNumberButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Sequence", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "Sequence", superToolTip4, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Number", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "Number", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.PoleNumberButtonEdit.Properties.MaxLength = 50;
            this.PoleNumberButtonEdit.Size = new System.Drawing.Size(498, 20);
            this.PoleNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.PoleNumberButtonEdit.TabIndex = 7;
            this.PoleNumberButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PoleNumberButtonEdit_ButtonClick);
            this.PoleNumberButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PoleNumberButtonEdit_Validating);
            // 
            // ItemForPoleID
            // 
            this.ItemForPoleID.Control = this.PoleIDTextEdit;
            this.ItemForPoleID.CustomizationFormText = "Pole ID:";
            this.ItemForPoleID.Location = new System.Drawing.Point(0, 129);
            this.ItemForPoleID.Name = "ItemForPoleID";
            this.ItemForPoleID.Size = new System.Drawing.Size(591, 24);
            this.ItemForPoleID.Text = "Pole ID:";
            this.ItemForPoleID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCircuitID
            // 
            this.ItemForCircuitID.Control = this.CircuitIDTextEdit;
            this.ItemForCircuitID.CustomizationFormText = "Circuit ID:";
            this.ItemForCircuitID.Location = new System.Drawing.Point(0, 105);
            this.ItemForCircuitID.Name = "ItemForCircuitID";
            this.ItemForCircuitID.Size = new System.Drawing.Size(591, 24);
            this.ItemForCircuitID.Text = "Circuit ID:";
            this.ItemForCircuitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCircuitNumber
            // 
            this.ItemForCircuitNumber.Control = this.CircuitNumberTextEdit;
            this.ItemForCircuitNumber.CustomizationFormText = "Circuit Number:";
            this.ItemForCircuitNumber.Location = new System.Drawing.Point(0, 91);
            this.ItemForCircuitNumber.Name = "ItemForCircuitNumber";
            this.ItemForCircuitNumber.Size = new System.Drawing.Size(591, 24);
            this.ItemForCircuitNumber.Text = "Circuit Number:";
            this.ItemForCircuitNumber.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 194);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(584, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.Control = this.ClientNameTextEdit;
            this.ItemForClientName.CustomizationFormText = "Client Name:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 91);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(608, 24);
            this.ItemForClientName.Text = "Client Name:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 249);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(584, 8);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 249);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(584, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSubAreaID
            // 
            this.ItemForSubAreaID.Control = this.SubAreaIDTextEdit;
            this.ItemForSubAreaID.Location = new System.Drawing.Point(0, 249);
            this.ItemForSubAreaID.Name = "ItemForSubAreaID";
            this.ItemForSubAreaID.Size = new System.Drawing.Size(584, 24);
            this.ItemForSubAreaID.Text = "Sub Area ID:";
            this.ItemForSubAreaID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 589);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCircuitName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.emptySpaceItem5,
            this.ItemForPoleNumber,
            this.tabbedControlGroup2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 569);
            // 
            // ItemForCircuitName
            // 
            this.ItemForCircuitName.AllowHide = false;
            this.ItemForCircuitName.Control = this.CircuitNameButtonEdit;
            this.ItemForCircuitName.CustomizationFormText = "Linked to Circuit:";
            this.ItemForCircuitName.Location = new System.Drawing.Point(0, 23);
            this.ItemForCircuitName.Name = "ItemForCircuitName";
            this.ItemForCircuitName.Size = new System.Drawing.Size(608, 24);
            this.ItemForCircuitName.Text = "Linked to Circuit:";
            this.ItemForCircuitName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(106, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(106, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(106, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(346, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(262, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(106, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(240, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 81);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(608, 320);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 274);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPoleTypeID,
            this.ItemForStatusID,
            this.layoutControlGroup8,
            this.emptySpaceItem7,
            this.ItemForIsTransformer,
            this.ItemForTransformerNumber,
            this.ItemForSubAreaName});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 226);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForPoleTypeID
            // 
            this.ItemForPoleTypeID.Control = this.PoleTypeIDGridLookUpEdit;
            this.ItemForPoleTypeID.CustomizationFormText = "Pole Type:";
            this.ItemForPoleTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForPoleTypeID.Name = "ItemForPoleTypeID";
            this.ItemForPoleTypeID.Size = new System.Drawing.Size(560, 26);
            this.ItemForPoleTypeID.Text = "Pole Type:";
            this.ItemForPoleTypeID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 24);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(560, 24);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Inspections";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLastInspectionDate,
            this.ItemForInspectionCycle,
            this.ItemForNextInspectionDate,
            this.ItemForInspectionUnit});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 108);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(560, 118);
            this.layoutControlGroup8.Text = "Inspections";
            // 
            // ItemForLastInspectionDate
            // 
            this.ItemForLastInspectionDate.Control = this.LastInspectionDateDateEdit;
            this.ItemForLastInspectionDate.CustomizationFormText = "Last Inspection:";
            this.ItemForLastInspectionDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForLastInspectionDate.Name = "ItemForLastInspectionDate";
            this.ItemForLastInspectionDate.Size = new System.Drawing.Size(536, 24);
            this.ItemForLastInspectionDate.Text = "Last Date:";
            this.ItemForLastInspectionDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForInspectionCycle
            // 
            this.ItemForInspectionCycle.Control = this.InspectionCycleSpinEdit;
            this.ItemForInspectionCycle.CustomizationFormText = "Inspection Cycle:";
            this.ItemForInspectionCycle.Location = new System.Drawing.Point(0, 24);
            this.ItemForInspectionCycle.Name = "ItemForInspectionCycle";
            this.ItemForInspectionCycle.Size = new System.Drawing.Size(231, 24);
            this.ItemForInspectionCycle.Text = "Cycle:";
            this.ItemForInspectionCycle.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForNextInspectionDate
            // 
            this.ItemForNextInspectionDate.Control = this.NextInspectionDateDateEdit;
            this.ItemForNextInspectionDate.CustomizationFormText = "layoutControlItem3";
            this.ItemForNextInspectionDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForNextInspectionDate.Name = "ItemForNextInspectionDate";
            this.ItemForNextInspectionDate.Size = new System.Drawing.Size(536, 24);
            this.ItemForNextInspectionDate.Text = "Next Date:";
            this.ItemForNextInspectionDate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForInspectionUnit
            // 
            this.ItemForInspectionUnit.Control = this.InspectionUnitGridLookUpEdit;
            this.ItemForInspectionUnit.CustomizationFormText = "Cycle Unit:";
            this.ItemForInspectionUnit.Location = new System.Drawing.Point(231, 24);
            this.ItemForInspectionUnit.Name = "ItemForInspectionUnit";
            this.ItemForInspectionUnit.Size = new System.Drawing.Size(305, 24);
            this.ItemForInspectionUnit.Text = "Cycle Unit:";
            this.ItemForInspectionUnit.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(560, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIsTransformer
            // 
            this.ItemForIsTransformer.Control = this.IsTransformerCheckEdit;
            this.ItemForIsTransformer.CustomizationFormText = "Is Transformer:";
            this.ItemForIsTransformer.Location = new System.Drawing.Point(0, 74);
            this.ItemForIsTransformer.Name = "ItemForIsTransformer";
            this.ItemForIsTransformer.Size = new System.Drawing.Size(241, 24);
            this.ItemForIsTransformer.Text = "Is Transformer:";
            this.ItemForIsTransformer.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForTransformerNumber
            // 
            this.ItemForTransformerNumber.Control = this.TransformerNumberTextEdit;
            this.ItemForTransformerNumber.CustomizationFormText = "Transformer Number:";
            this.ItemForTransformerNumber.Location = new System.Drawing.Point(241, 74);
            this.ItemForTransformerNumber.Name = "ItemForTransformerNumber";
            this.ItemForTransformerNumber.Size = new System.Drawing.Size(319, 24);
            this.ItemForTransformerNumber.Text = "Transformer Number:";
            this.ItemForTransformerNumber.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForSubAreaName
            // 
            this.ItemForSubAreaName.Control = this.SubAreaNameButtonEdit;
            this.ItemForSubAreaName.Location = new System.Drawing.Point(0, 0);
            this.ItemForSubAreaName.Name = "ItemForSubAreaName";
            this.ItemForSubAreaName.Size = new System.Drawing.Size(560, 24);
            this.ItemForSubAreaName.Text = "Linked To Sub-Area:";
            this.ItemForSubAreaName.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Mapping";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForX,
            this.ItemForY,
            this.ItemForLatitude,
            this.ItemForLongitude,
            this.ItemForMapID});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(560, 226);
            this.layoutControlGroup7.Text = "Mapping";
            // 
            // ItemForX
            // 
            this.ItemForX.Control = this.XTextEdit;
            this.ItemForX.CustomizationFormText = "X Coordinate:";
            this.ItemForX.Location = new System.Drawing.Point(0, 24);
            this.ItemForX.Name = "ItemForX";
            this.ItemForX.Size = new System.Drawing.Size(560, 24);
            this.ItemForX.Text = "X Coordinate:";
            this.ItemForX.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForY
            // 
            this.ItemForY.Control = this.YTextEdit;
            this.ItemForY.CustomizationFormText = "Y Coordinate:";
            this.ItemForY.Location = new System.Drawing.Point(0, 48);
            this.ItemForY.Name = "ItemForY";
            this.ItemForY.Size = new System.Drawing.Size(560, 24);
            this.ItemForY.Text = "Y Coordinate:";
            this.ItemForY.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForLatitude
            // 
            this.ItemForLatitude.Control = this.LatitudeTextEdit;
            this.ItemForLatitude.CustomizationFormText = "Latitude:";
            this.ItemForLatitude.Location = new System.Drawing.Point(0, 72);
            this.ItemForLatitude.Name = "ItemForLatitude";
            this.ItemForLatitude.Size = new System.Drawing.Size(560, 24);
            this.ItemForLatitude.Text = "Latitude:";
            this.ItemForLatitude.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForLongitude
            // 
            this.ItemForLongitude.Control = this.LongitudeTextEdit;
            this.ItemForLongitude.CustomizationFormText = "Longitude:";
            this.ItemForLongitude.Location = new System.Drawing.Point(0, 96);
            this.ItemForLongitude.Name = "ItemForLongitude";
            this.ItemForLongitude.Size = new System.Drawing.Size(560, 130);
            this.ItemForLongitude.Text = "Longitude:";
            this.ItemForLongitude.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForMapID
            // 
            this.ItemForMapID.Control = this.MapIDTextEdit;
            this.ItemForMapID.CustomizationFormText = "Map ID:";
            this.ItemForMapID.Location = new System.Drawing.Point(0, 0);
            this.ItemForMapID.Name = "ItemForMapID";
            this.ItemForMapID.Size = new System.Drawing.Size(560, 24);
            this.ItemForMapID.Text = "Map ID:";
            this.ItemForMapID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 226);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 226);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 401);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPoleNumber
            // 
            this.ItemForPoleNumber.AllowHide = false;
            this.ItemForPoleNumber.Control = this.PoleNumberButtonEdit;
            this.ItemForPoleNumber.CustomizationFormText = "Pole Number:";
            this.ItemForPoleNumber.Location = new System.Drawing.Point(0, 47);
            this.ItemForPoleNumber.Name = "ItemForPoleNumber";
            this.ItemForPoleNumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForPoleNumber.Text = "Pole Number:";
            this.ItemForPoleNumber.TextSize = new System.Drawing.Size(103, 13);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 411);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup9;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(608, 158);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup9,
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.layoutControlGroup12});
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Linked Access Issues";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(584, 113);
            this.layoutControlGroup9.Text = "Linked Access Issues";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Access Issues Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(584, 113);
            this.layoutControlItem2.Text = "Linked Access Issues Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Linked Environmental Issues";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(584, 113);
            this.layoutControlGroup10.Text = "Linked Environmental Issues";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "Linked Environmental Issues Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(584, 113);
            this.layoutControlItem3.Text = "Linked Environmental Issues Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Linked Site Hazards";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(584, 113);
            this.layoutControlGroup11.Text = "Linked Site Hazards";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl3;
            this.layoutControlItem5.CustomizationFormText = "Linked Site Hazards Grid:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(584, 113);
            this.layoutControlItem5.Text = "Linked Site Hazards Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Linked Electical Hazards";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(584, 113);
            this.layoutControlGroup12.Text = "Linked Electical Hazards";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControl4;
            this.layoutControlItem6.CustomizationFormText = "Linked Electrical Hazards Grid:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(584, 113);
            this.layoutControlItem6.Text = "Linked Electrical Hazards Grid:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // sp07007UTVoltagesWithBlankBindingSource
            // 
            this.sp07007UTVoltagesWithBlankBindingSource.DataMember = "sp07007_UT_Voltages_With_Blank";
            this.sp07007UTVoltagesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07006_UT_Circuit_StatusTableAdapter
            // 
            this.sp07006_UT_Circuit_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp07007_UT_Voltages_With_BlankTableAdapter
            // 
            this.sp07007_UT_Voltages_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07043_UT_Pole_ItemTableAdapter
            // 
            this.sp07043_UT_Pole_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07045_UT_Pole_Types_With_BlankTableAdapter
            // 
            this.sp07045_UT_Pole_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07046_UT_Inspection_Cycles_With_BlankTableAdapter
            // 
            this.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl2;
            this.layoutControlItem4.CustomizationFormText = "Linked Environmental Issues Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem3";
            this.layoutControlItem4.Size = new System.Drawing.Size(584, 127);
            this.layoutControlItem4.Text = "Linked Environmental Issues Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(161, 13);
            // 
            // sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter
            // 
            this.sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter
            // 
            this.sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter
            // 
            this.sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter
            // 
            this.sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07068_UT_Access_Issue_List_With_BlankTableAdapter
            // 
            this.sp07068_UT_Access_Issue_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter
            // 
            this.sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07078_UT_Site_Hazards_List_With_BlankTableAdapter
            // 
            this.sp07078_UT_Site_Hazards_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter
            // 
            this.sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_pole_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 645);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_pole_Edit";
            this.Text = "Edit Pole";
            this.Activated += new System.EventHandler(this.frm_UT_pole_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_pole_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_pole_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07043UTPoleItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditElectricalHazard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07083UTElectricalHazardsListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditSiteHazard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07078UTSiteHazardsListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditEnvronmentalIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07073UTEnvironmentalIssueListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAccessIssue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07068UTAccessIssueListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransformerNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsTransformerCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LongitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatitudeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextInspectionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NextInspectionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionUnitGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07046UTInspectionCyclesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InspectionCycleSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastInspectionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastInspectionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07045UTPoleTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoleNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNextInspectionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInspectionUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsTransformer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTransformerNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLongitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07007UTVoltagesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit CircuitIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit CircuitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleNumber;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit CircuitNameButtonEdit;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07006UTCircuitStatusBindingSource;
        private DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter sp07006_UT_Circuit_StatusTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraEditors.GridLookUpEdit PoleTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleTypeID;
        private System.Windows.Forms.BindingSource sp07007UTVoltagesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07007_UT_Voltages_With_BlankTableAdapter sp07007_UT_Voltages_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.TextEdit PoleIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleID;
        private System.Windows.Forms.BindingSource sp07043UTPoleItemBindingSource;
        private DataSet_UT_EditTableAdapters.sp07043_UT_Pole_ItemTableAdapter sp07043_UT_Pole_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp07045UTPoleTypesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07045_UT_Pole_Types_With_BlankTableAdapter sp07045_UT_Pole_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.DateEdit LastInspectionDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastInspectionDate;
        private DevExpress.XtraEditors.SpinEdit InspectionCycleSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionCycle;
        private DevExpress.XtraEditors.DateEdit NextInspectionDateDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit InspectionUnitGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInspectionUnit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNextInspectionDate;
        private System.Windows.Forms.BindingSource sp07046UTInspectionCyclesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07046_UT_Inspection_Cycles_With_BlankTableAdapter sp07046_UT_Inspection_Cycles_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.TextEdit ClientNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraEditors.TextEdit YTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForY;
        private DevExpress.XtraEditors.TextEdit XTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForX;
        private DevExpress.XtraEditors.TextEdit LatitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatitude;
        private DevExpress.XtraEditors.TextEdit LongitudeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLongitude;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.CheckEdit IsTransformerCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsTransformer;
        private DevExpress.XtraEditors.TextEdit TransformerNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTransformerNumber;
        private DevExpress.XtraEditors.TextEdit MapIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMapID;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleElectricalHazardID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID3;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalHazardID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode3;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs3;
        private System.Windows.Forms.BindingSource sp07076UTPoleSiteHazardsLinkedToPoleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleSiteHazardID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteHazardID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs2;
        private System.Windows.Forms.BindingSource sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleEnvironmentalIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEnvironmentalIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode1;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs1;
        private System.Windows.Forms.BindingSource sp07066UTPoleAccessIssuesLinkedToPoleBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleAccessIssueID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessIssueID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditAccessIssue;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colstrMode;
        private DevExpress.XtraGrid.Columns.GridColumn colstrRecordIDs;
        private DataSet_UT_EditTableAdapters.sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter;
        private DataSet_UT_EditTableAdapters.sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter;
        private DataSet_UT_EditTableAdapters.sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter;
        private DataSet_UT_EditTableAdapters.sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter;
        private System.Windows.Forms.BindingSource sp07068UTAccessIssueListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07068_UT_Access_Issue_List_With_BlankTableAdapter sp07068_UT_Access_Issue_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditEnvronmentalIssue;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource sp07073UTEnvironmentalIssueListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colID3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder2;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditSiteHazard;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp07078UTSiteHazardsListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07078_UT_Site_Hazards_List_With_BlankTableAdapter sp07078_UT_Site_Hazards_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colID4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder3;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditElectricalHazard;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp07083UTElectricalHazardsListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription5;
        private DevExpress.XtraGrid.Columns.GridColumn colID5;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder4;
        private DevExpress.XtraEditors.ButtonEdit PoleNumberButtonEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraEditors.TextEdit SubAreaIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaID;
        private DevExpress.XtraEditors.ButtonEdit SubAreaNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

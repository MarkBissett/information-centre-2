namespace WoodPlan5
{
    partial class frm_UT_Mapping_Styles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Mapping_Styles));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn49 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn50 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox_ThematicPointStyle = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListPointStyles = new System.Windows.Forms.ImageList(this.components);
            this.gridColumn52 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit_ThematicPointSize = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit_ThematicColour = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn55 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox_ThematicFillPattern = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListFillPatterns = new System.Windows.Forms.ImageList(this.components);
            this.gridColumn57 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn58 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox_ThematicLineWidth = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListLineWidths = new System.Windows.Forms.ImageList(this.components);
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn60 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox_ThematicLineStyle = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageListLineStyles = new System.Windows.Forms.ImageList(this.components);
            this.gridColumn61 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn62 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn63 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn64 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit_BandSize = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn65 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_TreePicker = new WoodPlan5.DataSet_AT_TreePicker();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSymbol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSymbolColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonFillColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonLineColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonFillPattern = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonFillPatternColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolygonLineWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolylineColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolylineStyle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPolylineWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicklistItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShowInLegend = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndBand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartBand = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imageListLegend = new System.Windows.Forms.ImageList(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Mapping = new WoodPlan5.DataSet_UT_Mapping();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox3 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox4 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSymbol2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSymbol2Colour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.glueThematicField = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSetType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSetName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasePicklistID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.spinEditDefaultBandIncrement = new DevExpress.XtraEditors.SpinEdit();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.trackBarControl1 = new DevExpress.XtraEditors.TrackBarControl();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.ceUseThematicStyling = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter();
            this.sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter();
            this.popupMenu_ThematicGrid = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiBlockEditThematicStyles = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAddBand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteBand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateDescriptionsFromBands = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSet = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveThematicSetAs = new DevExpress.XtraBars.BarButtonItem();
            this.sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter = new WoodPlan5.DataSet_UT_MappingTableAdapters.sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicPointStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit_ThematicPointSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit_ThematicColour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicFillPattern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicLineWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicLineStyle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit_BandSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueThematicField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDefaultBandIncrement.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ceUseThematicStyling.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1084, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 594);
            this.barDockControlBottom.Size = new System.Drawing.Size(1084, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 594);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1084, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 594);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiBlockEditThematicStyles,
            this.bbiSaveThematicSet,
            this.bbiSaveThematicSetAs,
            this.bbiAddBand,
            this.bbiDeleteBand,
            this.bbiCreateDescriptionsFromBands});
            this.barManager1.MaxItemId = 31;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.ColumnPanelRowHeight = 35;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn45,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn49,
            this.gridColumn50,
            this.gridColumn51,
            this.gridColumn52,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn55,
            this.gridColumn56,
            this.gridColumn57,
            this.gridColumn58,
            this.gridColumn59,
            this.gridColumn60,
            this.gridColumn61,
            this.gridColumn62,
            this.gridColumn63,
            this.gridColumn64,
            this.gridColumn65});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 541, 208, 275);
            this.gridView1.GridControl = this.gridControl6;
            this.gridView1.Images = this.imageListLegend;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn50, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // gridColumn45
            // 
            this.gridColumn45.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn45.FieldName = "ItemDescription";
            this.gridColumn45.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 1;
            this.gridColumn45.Width = 189;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.MaxLength = 50;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridColumn46
            // 
            this.gridColumn46.FieldName = "ItemCode";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowEdit = false;
            this.gridColumn46.OptionsColumn.AllowFocus = false;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.OptionsColumn.TabStop = false;
            this.gridColumn46.Width = 62;
            // 
            // gridColumn47
            // 
            this.gridColumn47.FieldName = "ItemID";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.AllowEdit = false;
            this.gridColumn47.OptionsColumn.AllowFocus = false;
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsColumn.TabStop = false;
            this.gridColumn47.Width = 48;
            // 
            // gridColumn48
            // 
            this.gridColumn48.FieldName = "HeaderID";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.AllowEdit = false;
            this.gridColumn48.OptionsColumn.AllowFocus = false;
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsColumn.TabStop = false;
            this.gridColumn48.Width = 61;
            // 
            // gridColumn49
            // 
            this.gridColumn49.FieldName = "HeaderDescription";
            this.gridColumn49.Name = "gridColumn49";
            this.gridColumn49.OptionsColumn.AllowEdit = false;
            this.gridColumn49.OptionsColumn.AllowFocus = false;
            this.gridColumn49.OptionsColumn.ReadOnly = true;
            this.gridColumn49.OptionsColumn.TabStop = false;
            this.gridColumn49.Width = 103;
            // 
            // gridColumn50
            // 
            this.gridColumn50.FieldName = "Order";
            this.gridColumn50.Name = "gridColumn50";
            this.gridColumn50.OptionsColumn.AllowEdit = false;
            this.gridColumn50.OptionsColumn.AllowFocus = false;
            this.gridColumn50.OptionsColumn.ReadOnly = true;
            this.gridColumn50.OptionsColumn.TabStop = false;
            this.gridColumn50.Width = 53;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "<color=red>Point Symbol</color>";
            this.gridColumn51.ColumnEdit = this.repositoryItemImageComboBox_ThematicPointStyle;
            this.gridColumn51.FieldName = "Symbol";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 4;
            this.gridColumn51.Width = 63;
            // 
            // repositoryItemImageComboBox_ThematicPointStyle
            // 
            this.repositoryItemImageComboBox_ThematicPointStyle.AutoHeight = false;
            this.repositoryItemImageComboBox_ThematicPointStyle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox_ThematicPointStyle.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle [Solid]", 34, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square [Solid]", 32, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond [Solid]", 33, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1 [Solid]", 36, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2 [Solid]", 37, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star [Solid]", 35, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle [Hollow]", 40, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square [Hollow]", 38, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond [Hollow]", 39, 8),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1 [Hollow]", 42, 9),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2 [Hollow]", 43, 10),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star [Hollow]", 41, 11)});
            this.repositoryItemImageComboBox_ThematicPointStyle.Name = "repositoryItemImageComboBox_ThematicPointStyle";
            this.repositoryItemImageComboBox_ThematicPointStyle.SmallImages = this.imageListPointStyles;
            // 
            // imageListPointStyles
            // 
            this.imageListPointStyles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListPointStyles.ImageStream")));
            this.imageListPointStyles.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListPointStyles.Images.SetKeyName(0, "symbol_circle_16.png");
            this.imageListPointStyles.Images.SetKeyName(1, "symbol_square_16.png");
            this.imageListPointStyles.Images.SetKeyName(2, "symbol_diamond_16.png");
            this.imageListPointStyles.Images.SetKeyName(3, "symbol_triangle1_16.png");
            this.imageListPointStyles.Images.SetKeyName(4, "symbol_triangle2_16.png");
            this.imageListPointStyles.Images.SetKeyName(5, "symbol_star_16.png");
            this.imageListPointStyles.Images.SetKeyName(6, "symbol_hollow_circle_16.png");
            this.imageListPointStyles.Images.SetKeyName(7, "symbol_hollow_square_16.png");
            this.imageListPointStyles.Images.SetKeyName(8, "symbol_hollow_diamond_16.png");
            this.imageListPointStyles.Images.SetKeyName(9, "symbol_hollow_triangle1_16.png");
            this.imageListPointStyles.Images.SetKeyName(10, "symbol_hollow_triangle2_16.png");
            this.imageListPointStyles.Images.SetKeyName(11, "symbol_hollow_star_16.png");
            // 
            // gridColumn52
            // 
            this.gridColumn52.Caption = "<color=red>Point Size</color>";
            this.gridColumn52.ColumnEdit = this.repositoryItemSpinEdit_ThematicPointSize;
            this.gridColumn52.FieldName = "Size";
            this.gridColumn52.Name = "gridColumn52";
            this.gridColumn52.Visible = true;
            this.gridColumn52.VisibleIndex = 5;
            this.gridColumn52.Width = 54;
            // 
            // repositoryItemSpinEdit_ThematicPointSize
            // 
            this.repositoryItemSpinEdit_ThematicPointSize.AutoHeight = false;
            this.repositoryItemSpinEdit_ThematicPointSize.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit_ThematicPointSize.DisplayFormat.FormatString = "f2";
            this.repositoryItemSpinEdit_ThematicPointSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit_ThematicPointSize.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit_ThematicPointSize.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit_ThematicPointSize.MaxValue = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.repositoryItemSpinEdit_ThematicPointSize.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit_ThematicPointSize.Name = "repositoryItemSpinEdit_ThematicPointSize";
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "<color=red>Point Colour</color>";
            this.gridColumn53.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn53.FieldName = "SymbolColour";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 6;
            this.gridColumn53.Width = 56;
            // 
            // repositoryItemColorEdit_ThematicColour
            // 
            this.repositoryItemColorEdit_ThematicColour.AutoHeight = false;
            this.repositoryItemColorEdit_ThematicColour.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit_ThematicColour.Name = "repositoryItemColorEdit_ThematicColour";
            this.repositoryItemColorEdit_ThematicColour.StoreColorAsInteger = true;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "<color=blue>Polygon Fill Colour</color>";
            this.gridColumn54.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn54.FieldName = "PolygonFillColour";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 7;
            this.gridColumn54.Width = 71;
            // 
            // gridColumn55
            // 
            this.gridColumn55.Caption = "<color=blue>Polygon Line Colour</color>";
            this.gridColumn55.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn55.FieldName = "PolygonLineColour";
            this.gridColumn55.Name = "gridColumn55";
            this.gridColumn55.Visible = true;
            this.gridColumn55.VisibleIndex = 11;
            this.gridColumn55.Width = 73;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "<color=blue>Polygon Fill Pattern</color>";
            this.gridColumn56.ColumnEdit = this.repositoryItemImageComboBox_ThematicFillPattern;
            this.gridColumn56.FieldName = "PolygonFillPattern";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 8;
            this.gridColumn56.Width = 74;
            // 
            // repositoryItemImageComboBox_ThematicFillPattern
            // 
            this.repositoryItemImageComboBox_ThematicFillPattern.AutoHeight = false;
            this.repositoryItemImageComboBox_ThematicFillPattern.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox_ThematicFillPattern.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Transparent", "1", 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Horizontal Lines", 3, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Vertical Lines", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diagonal Lines", 6, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crosshatch", 8, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 1", 14, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 2", 17, 6)});
            this.repositoryItemImageComboBox_ThematicFillPattern.Name = "repositoryItemImageComboBox_ThematicFillPattern";
            this.repositoryItemImageComboBox_ThematicFillPattern.SmallImages = this.imageListFillPatterns;
            // 
            // imageListFillPatterns
            // 
            this.imageListFillPatterns.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFillPatterns.ImageStream")));
            this.imageListFillPatterns.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListFillPatterns.Images.SetKeyName(0, "pattern_02.png");
            this.imageListFillPatterns.Images.SetKeyName(1, "pattern_03.png");
            this.imageListFillPatterns.Images.SetKeyName(2, "pattern_04.png");
            this.imageListFillPatterns.Images.SetKeyName(3, "pattern_06.png");
            this.imageListFillPatterns.Images.SetKeyName(4, "pattern_08.png");
            this.imageListFillPatterns.Images.SetKeyName(5, "pattern_14.png");
            this.imageListFillPatterns.Images.SetKeyName(6, "pattern_17.png");
            this.imageListFillPatterns.Images.SetKeyName(7, "pattern_01.png");
            // 
            // gridColumn57
            // 
            this.gridColumn57.Caption = "<color=blue>Polygon Fill Pattern Colour</color>";
            this.gridColumn57.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn57.FieldName = "PolygonFillPatternColour";
            this.gridColumn57.Name = "gridColumn57";
            this.gridColumn57.Visible = true;
            this.gridColumn57.VisibleIndex = 9;
            this.gridColumn57.Width = 91;
            // 
            // gridColumn58
            // 
            this.gridColumn58.Caption = "<color=blue>Polygon Line Width</color>";
            this.gridColumn58.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.gridColumn58.FieldName = "PolygonLineWidth";
            this.gridColumn58.Name = "gridColumn58";
            this.gridColumn58.Visible = true;
            this.gridColumn58.VisibleIndex = 10;
            this.gridColumn58.Width = 71;
            // 
            // repositoryItemImageComboBox_ThematicLineWidth
            // 
            this.repositoryItemImageComboBox_ThematicLineWidth.AutoHeight = false;
            this.repositoryItemImageComboBox_ThematicLineWidth.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox_ThematicLineWidth.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1 Pixel", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2 Pixels", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3 Pixels", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4 Pixels", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("5 Pixels", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("6 Pixels", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("7 Pixels", 7, 6)});
            this.repositoryItemImageComboBox_ThematicLineWidth.Name = "repositoryItemImageComboBox_ThematicLineWidth";
            this.repositoryItemImageComboBox_ThematicLineWidth.SmallImages = this.imageListLineWidths;
            // 
            // imageListLineWidths
            // 
            this.imageListLineWidths.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineWidths.ImageStream")));
            this.imageListLineWidths.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineWidths.Images.SetKeyName(0, "Border_01.png");
            this.imageListLineWidths.Images.SetKeyName(1, "Border_02.png");
            this.imageListLineWidths.Images.SetKeyName(2, "Border_03.png");
            this.imageListLineWidths.Images.SetKeyName(3, "Border_04.png");
            this.imageListLineWidths.Images.SetKeyName(4, "Border_05.png");
            this.imageListLineWidths.Images.SetKeyName(5, "Border_06.png");
            this.imageListLineWidths.Images.SetKeyName(6, "Border_07.png");
            // 
            // gridColumn59
            // 
            this.gridColumn59.Caption = "<color=green>Polyline Colour</color>";
            this.gridColumn59.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn59.FieldName = "PolylineColour";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 13;
            this.gridColumn59.Width = 71;
            // 
            // gridColumn60
            // 
            this.gridColumn60.Caption = "<color=green>Polyline Style</color>";
            this.gridColumn60.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineStyle;
            this.gridColumn60.FieldName = "PolylineStyle";
            this.gridColumn60.Name = "gridColumn60";
            this.gridColumn60.Visible = true;
            this.gridColumn60.VisibleIndex = 12;
            this.gridColumn60.Width = 82;
            // 
            // repositoryItemImageComboBox_ThematicLineStyle
            // 
            this.repositoryItemImageComboBox_ThematicLineStyle.AutoHeight = false;
            this.repositoryItemImageComboBox_ThematicLineStyle.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox_ThematicLineStyle.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dashed", 6, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dotted", 10, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dot Dashed", 31, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Arrowed", 54, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Double", 63, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triple", 64, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Dashed", 73, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Squares", 93, 8)});
            this.repositoryItemImageComboBox_ThematicLineStyle.Name = "repositoryItemImageComboBox_ThematicLineStyle";
            this.repositoryItemImageComboBox_ThematicLineStyle.SmallImages = this.imageListLineStyles;
            // 
            // imageListLineStyles
            // 
            this.imageListLineStyles.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLineStyles.ImageStream")));
            this.imageListLineStyles.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLineStyles.Images.SetKeyName(0, "line_02.png");
            this.imageListLineStyles.Images.SetKeyName(1, "line_06.png");
            this.imageListLineStyles.Images.SetKeyName(2, "line_10.png");
            this.imageListLineStyles.Images.SetKeyName(3, "line_41.png");
            this.imageListLineStyles.Images.SetKeyName(4, "line_54.png");
            this.imageListLineStyles.Images.SetKeyName(5, "line_63.png");
            this.imageListLineStyles.Images.SetKeyName(6, "line_64.png");
            this.imageListLineStyles.Images.SetKeyName(7, "line_73.png");
            this.imageListLineStyles.Images.SetKeyName(8, "line_93.png");
            // 
            // gridColumn61
            // 
            this.gridColumn61.Caption = "<color=green>Polyline Width</color>";
            this.gridColumn61.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.gridColumn61.FieldName = "PolylineWidth";
            this.gridColumn61.Name = "gridColumn61";
            this.gridColumn61.Visible = true;
            this.gridColumn61.VisibleIndex = 14;
            this.gridColumn61.Width = 71;
            // 
            // gridColumn62
            // 
            this.gridColumn62.Caption = "Picklist Item ID";
            this.gridColumn62.FieldName = "PicklistItemID";
            this.gridColumn62.Name = "gridColumn62";
            this.gridColumn62.OptionsColumn.AllowEdit = false;
            this.gridColumn62.OptionsColumn.AllowFocus = false;
            this.gridColumn62.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn63
            // 
            this.gridColumn63.AppearanceHeader.Options.UseImage = true;
            this.gridColumn63.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn63.CustomizationCaption = "Show In Legend";
            this.gridColumn63.FieldName = "ShowInLegend";
            this.gridColumn63.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn63.ImageOptions.ImageIndex = 0;
            this.gridColumn63.Name = "gridColumn63";
            this.gridColumn63.OptionsColumn.ShowCaption = false;
            this.gridColumn63.ToolTip = "Controls Legend Visibility. Tick to include in legend, un-tick to exclude from le" +
    "gend.";
            this.gridColumn63.Visible = true;
            this.gridColumn63.VisibleIndex = 0;
            this.gridColumn63.Width = 39;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn64
            // 
            this.gridColumn64.Caption = "Band End Value";
            this.gridColumn64.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.gridColumn64.FieldName = "EndBand";
            this.gridColumn64.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn64.Name = "gridColumn64";
            this.gridColumn64.Visible = true;
            this.gridColumn64.VisibleIndex = 3;
            // 
            // repositoryItemSpinEdit_BandSize
            // 
            this.repositoryItemSpinEdit_BandSize.AutoHeight = false;
            this.repositoryItemSpinEdit_BandSize.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit_BandSize.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit_BandSize.MaxLength = 11;
            this.repositoryItemSpinEdit_BandSize.MaxValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            131072});
            this.repositoryItemSpinEdit_BandSize.MinValue = new decimal(new int[] {
            1410065407,
            2,
            0,
            -2147352576});
            this.repositoryItemSpinEdit_BandSize.Name = "repositoryItemSpinEdit_BandSize";
            this.repositoryItemSpinEdit_BandSize.NullValuePrompt = "Enter a Value";
            this.repositoryItemSpinEdit_BandSize.NullValuePromptShowForEmptyValue = true;
            // 
            // gridColumn65
            // 
            this.gridColumn65.Caption = "Band Start Value";
            this.gridColumn65.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.gridColumn65.FieldName = "StartBand";
            this.gridColumn65.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn65.Name = "gridColumn65";
            this.gridColumn65.Visible = true;
            this.gridColumn65.VisibleIndex = 2;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.LevelTemplate = this.gridView1;
            gridLevelNode1.RelationName = "Level1";
            this.gridControl6.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox_ThematicPointStyle,
            this.repositoryItemColorEdit_ThematicColour,
            this.repositoryItemSpinEdit_ThematicPointSize,
            this.repositoryItemImageComboBox_ThematicLineWidth,
            this.repositoryItemImageComboBox_ThematicFillPattern,
            this.repositoryItemImageComboBox_ThematicLineStyle,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemSpinEdit_BandSize});
            this.gridControl6.Size = new System.Drawing.Size(1046, 244);
            this.gridControl6.TabIndex = 32;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6,
            this.gridView2,
            this.gridView1});
            // 
            // sp01255ATTreePickerThematicMappingDefaultItemsBindingSource
            // 
            this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource.DataMember = "sp01255_AT_Tree_Picker_Thematic_Mapping_Default_Items";
            this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // dataSet_AT_TreePicker
            // 
            this.dataSet_AT_TreePicker.DataSetName = "DataSet_AT_TreePicker";
            this.dataSet_AT_TreePicker.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView6.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView6.ColumnPanelRowHeight = 35;
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.colSymbol,
            this.colSize,
            this.colSymbolColour,
            this.colPolygonFillColour,
            this.colPolygonLineColour,
            this.colPolygonFillPattern,
            this.colPolygonFillPatternColour,
            this.colPolygonLineWidth,
            this.colPolylineColour,
            this.colPolylineStyle,
            this.colPolylineWidth,
            this.colPicklistItemID,
            this.colShowInLegend,
            this.colEndBand,
            this.colStartBand});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Images = this.imageListLegend;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn16, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseDown);
            // 
            // gridColumn11
            // 
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn11.FieldName = "ItemDescription";
            this.gridColumn11.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 1;
            this.gridColumn11.Width = 189;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "ItemCode";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsColumn.TabStop = false;
            this.gridColumn12.Width = 62;
            // 
            // gridColumn13
            // 
            this.gridColumn13.FieldName = "ItemID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsColumn.TabStop = false;
            this.gridColumn13.Width = 48;
            // 
            // gridColumn14
            // 
            this.gridColumn14.FieldName = "HeaderID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsColumn.TabStop = false;
            this.gridColumn14.Width = 61;
            // 
            // gridColumn15
            // 
            this.gridColumn15.FieldName = "HeaderDescription";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsColumn.TabStop = false;
            this.gridColumn15.Width = 103;
            // 
            // gridColumn16
            // 
            this.gridColumn16.FieldName = "Order";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsColumn.TabStop = false;
            this.gridColumn16.Width = 53;
            // 
            // colSymbol
            // 
            this.colSymbol.Caption = "<color=red>Point Symbol</color>";
            this.colSymbol.ColumnEdit = this.repositoryItemImageComboBox_ThematicPointStyle;
            this.colSymbol.FieldName = "Symbol";
            this.colSymbol.Name = "colSymbol";
            this.colSymbol.Visible = true;
            this.colSymbol.VisibleIndex = 2;
            this.colSymbol.Width = 63;
            // 
            // colSize
            // 
            this.colSize.Caption = "<color=red>Point Size</color>";
            this.colSize.ColumnEdit = this.repositoryItemSpinEdit_ThematicPointSize;
            this.colSize.FieldName = "Size";
            this.colSize.Name = "colSize";
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 3;
            this.colSize.Width = 54;
            // 
            // colSymbolColour
            // 
            this.colSymbolColour.Caption = "<color=red>Point Colour</color>";
            this.colSymbolColour.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.colSymbolColour.FieldName = "SymbolColour";
            this.colSymbolColour.Name = "colSymbolColour";
            this.colSymbolColour.Visible = true;
            this.colSymbolColour.VisibleIndex = 4;
            this.colSymbolColour.Width = 56;
            // 
            // colPolygonFillColour
            // 
            this.colPolygonFillColour.Caption = "<color=blue>Polygon Fill Colour</color>";
            this.colPolygonFillColour.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.colPolygonFillColour.FieldName = "PolygonFillColour";
            this.colPolygonFillColour.Name = "colPolygonFillColour";
            this.colPolygonFillColour.Visible = true;
            this.colPolygonFillColour.VisibleIndex = 5;
            this.colPolygonFillColour.Width = 71;
            // 
            // colPolygonLineColour
            // 
            this.colPolygonLineColour.Caption = "<color=blue>Polygon Line Colour</color>";
            this.colPolygonLineColour.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.colPolygonLineColour.FieldName = "PolygonLineColour";
            this.colPolygonLineColour.Name = "colPolygonLineColour";
            this.colPolygonLineColour.Visible = true;
            this.colPolygonLineColour.VisibleIndex = 9;
            this.colPolygonLineColour.Width = 73;
            // 
            // colPolygonFillPattern
            // 
            this.colPolygonFillPattern.Caption = "<color=blue>Polygon Fill Pattern</color>";
            this.colPolygonFillPattern.ColumnEdit = this.repositoryItemImageComboBox_ThematicFillPattern;
            this.colPolygonFillPattern.FieldName = "PolygonFillPattern";
            this.colPolygonFillPattern.Name = "colPolygonFillPattern";
            this.colPolygonFillPattern.Visible = true;
            this.colPolygonFillPattern.VisibleIndex = 6;
            this.colPolygonFillPattern.Width = 74;
            // 
            // colPolygonFillPatternColour
            // 
            this.colPolygonFillPatternColour.Caption = "<color=blue>Polygon Fill Pattern Colour</color>";
            this.colPolygonFillPatternColour.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.colPolygonFillPatternColour.FieldName = "PolygonFillPatternColour";
            this.colPolygonFillPatternColour.Name = "colPolygonFillPatternColour";
            this.colPolygonFillPatternColour.Visible = true;
            this.colPolygonFillPatternColour.VisibleIndex = 7;
            this.colPolygonFillPatternColour.Width = 91;
            // 
            // colPolygonLineWidth
            // 
            this.colPolygonLineWidth.Caption = "<color=blue>Polygon Line Width</color>";
            this.colPolygonLineWidth.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.colPolygonLineWidth.FieldName = "PolygonLineWidth";
            this.colPolygonLineWidth.Name = "colPolygonLineWidth";
            this.colPolygonLineWidth.Visible = true;
            this.colPolygonLineWidth.VisibleIndex = 8;
            this.colPolygonLineWidth.Width = 71;
            // 
            // colPolylineColour
            // 
            this.colPolylineColour.Caption = "<color=green>Polyline Colour</color>";
            this.colPolylineColour.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.colPolylineColour.FieldName = "PolylineColour";
            this.colPolylineColour.Name = "colPolylineColour";
            this.colPolylineColour.Visible = true;
            this.colPolylineColour.VisibleIndex = 11;
            this.colPolylineColour.Width = 71;
            // 
            // colPolylineStyle
            // 
            this.colPolylineStyle.Caption = "<color=green>Polyline Style</color>";
            this.colPolylineStyle.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineStyle;
            this.colPolylineStyle.FieldName = "PolylineStyle";
            this.colPolylineStyle.Name = "colPolylineStyle";
            this.colPolylineStyle.Visible = true;
            this.colPolylineStyle.VisibleIndex = 10;
            this.colPolylineStyle.Width = 82;
            // 
            // colPolylineWidth
            // 
            this.colPolylineWidth.Caption = "<color=green>Polyline Width</color>";
            this.colPolylineWidth.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.colPolylineWidth.FieldName = "PolylineWidth";
            this.colPolylineWidth.Name = "colPolylineWidth";
            this.colPolylineWidth.Visible = true;
            this.colPolylineWidth.VisibleIndex = 12;
            this.colPolylineWidth.Width = 71;
            // 
            // colPicklistItemID
            // 
            this.colPicklistItemID.Caption = "Picklist Item ID";
            this.colPicklistItemID.FieldName = "PicklistItemID";
            this.colPicklistItemID.Name = "colPicklistItemID";
            this.colPicklistItemID.OptionsColumn.AllowEdit = false;
            this.colPicklistItemID.OptionsColumn.AllowFocus = false;
            this.colPicklistItemID.OptionsColumn.ReadOnly = true;
            // 
            // colShowInLegend
            // 
            this.colShowInLegend.AppearanceHeader.Options.UseImage = true;
            this.colShowInLegend.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colShowInLegend.CustomizationCaption = "Show In Legend";
            this.colShowInLegend.FieldName = "ShowInLegend";
            this.colShowInLegend.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colShowInLegend.ImageOptions.ImageIndex = 0;
            this.colShowInLegend.Name = "colShowInLegend";
            this.colShowInLegend.OptionsColumn.ShowCaption = false;
            this.colShowInLegend.ToolTip = "Controls Legend Visibility. Tick to include in legend, un-tick to exclude from le" +
    "gend.";
            this.colShowInLegend.Visible = true;
            this.colShowInLegend.VisibleIndex = 0;
            this.colShowInLegend.Width = 39;
            // 
            // colEndBand
            // 
            this.colEndBand.Caption = "Band End [N\\A]";
            this.colEndBand.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.colEndBand.FieldName = "EndBand";
            this.colEndBand.Name = "colEndBand";
            this.colEndBand.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // colStartBand
            // 
            this.colStartBand.Caption = "Band Start [N\\A]";
            this.colStartBand.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.colStartBand.FieldName = "StartBand";
            this.colStartBand.Name = "colStartBand";
            this.colStartBand.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // imageListLegend
            // 
            this.imageListLegend.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLegend.ImageStream")));
            this.imageListLegend.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListLegend.Images.SetKeyName(0, "legend_16.png");
            // 
            // gridView2
            // 
            this.gridView2.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView2.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView2.ColumnPanelRowHeight = 35;
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn43,
            this.gridColumn44});
            this.gridView2.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 541, 208, 275);
            this.gridView2.GridControl = this.gridControl6;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn22, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn17
            // 
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEdit1;
            this.gridColumn17.FieldName = "ItemDescription";
            this.gridColumn17.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 1;
            this.gridColumn17.Width = 188;
            // 
            // gridColumn18
            // 
            this.gridColumn18.FieldName = "ItemCode";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsColumn.TabStop = false;
            this.gridColumn18.Width = 62;
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "ItemID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.OptionsColumn.TabStop = false;
            this.gridColumn19.Width = 48;
            // 
            // gridColumn20
            // 
            this.gridColumn20.FieldName = "HeaderID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.OptionsColumn.TabStop = false;
            this.gridColumn20.Width = 61;
            // 
            // gridColumn21
            // 
            this.gridColumn21.FieldName = "HeaderDescription";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.OptionsColumn.TabStop = false;
            this.gridColumn21.Width = 103;
            // 
            // gridColumn22
            // 
            this.gridColumn22.FieldName = "Order";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.OptionsColumn.TabStop = false;
            this.gridColumn22.Width = 53;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "<color=red>Point Symbol</color>";
            this.gridColumn23.ColumnEdit = this.repositoryItemImageComboBox_ThematicPointStyle;
            this.gridColumn23.FieldName = "Symbol";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 4;
            this.gridColumn23.Width = 63;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "<color=red>Point Size</color>";
            this.gridColumn24.ColumnEdit = this.repositoryItemSpinEdit_ThematicPointSize;
            this.gridColumn24.FieldName = "Size";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 5;
            this.gridColumn24.Width = 54;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "<color=red>Point Colour</color>";
            this.gridColumn25.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn25.FieldName = "SymbolColour";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 6;
            this.gridColumn25.Width = 56;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "<color=blue>Polygon Fill Colour</color>";
            this.gridColumn26.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn26.FieldName = "PolygonFillColour";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 7;
            this.gridColumn26.Width = 71;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "<color=blue>Polygon Line Colour</color>";
            this.gridColumn27.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn27.FieldName = "PolygonLineColour";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 11;
            this.gridColumn27.Width = 73;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "<color=blue>Polygon Fill Pattern</color>";
            this.gridColumn28.ColumnEdit = this.repositoryItemImageComboBox_ThematicFillPattern;
            this.gridColumn28.FieldName = "PolygonFillPattern";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 8;
            this.gridColumn28.Width = 74;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "<color=blue>Polygon Fill Pattern Colour</color>";
            this.gridColumn29.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn29.FieldName = "PolygonFillPatternColour";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 9;
            this.gridColumn29.Width = 91;
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "<color=blue>Polygon Line Width</color>";
            this.gridColumn30.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.gridColumn30.FieldName = "PolygonLineWidth";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.Visible = true;
            this.gridColumn30.VisibleIndex = 10;
            this.gridColumn30.Width = 71;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "<color=green>Polyline Colour</color>";
            this.gridColumn31.ColumnEdit = this.repositoryItemColorEdit_ThematicColour;
            this.gridColumn31.FieldName = "PolylineColour";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 13;
            this.gridColumn31.Width = 71;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "<color=green>Polyline Style</color>";
            this.gridColumn32.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineStyle;
            this.gridColumn32.FieldName = "PolylineStyle";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 12;
            this.gridColumn32.Width = 82;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "<color=green>Polyline Width</color>";
            this.gridColumn33.ColumnEdit = this.repositoryItemImageComboBox_ThematicLineWidth;
            this.gridColumn33.FieldName = "PolylineWidth";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 14;
            this.gridColumn33.Width = 71;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Picklist Item ID";
            this.gridColumn34.FieldName = "PicklistItemID";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // gridColumn35
            // 
            this.gridColumn35.AppearanceHeader.Options.UseImage = true;
            this.gridColumn35.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn35.CustomizationCaption = "Show In Legend";
            this.gridColumn35.FieldName = "ShowInLegend";
            this.gridColumn35.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn35.ImageOptions.ImageIndex = 0;
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.ShowCaption = false;
            this.gridColumn35.ToolTip = "Controls Legend Visibility. Tick to include in legend, un-tick to exclude from le" +
    "gend.";
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 0;
            this.gridColumn35.Width = 39;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Band End Value";
            this.gridColumn43.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.gridColumn43.FieldName = "BandEnd";
            this.gridColumn43.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 3;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Band Start Value";
            this.gridColumn44.ColumnEdit = this.repositoryItemSpinEdit_BandSize;
            this.gridColumn44.FieldName = "BandStart";
            this.gridColumn44.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.Visible = true;
            this.gridColumn44.VisibleIndex = 2;
            this.gridColumn44.Width = 71;
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageComboBox1,
            this.repositoryItemColorEdit1,
            this.repositoryItemSpinEdit2,
            this.repositoryItemImageComboBox3,
            this.repositoryItemImageComboBox2,
            this.repositoryItemImageComboBox4});
            this.gridControl5.Size = new System.Drawing.Size(1046, 78);
            this.gridControl5.TabIndex = 33;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07054UTTreePickerThematicMappingDefaultDummyBindingSource
            // 
            this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource.DataMember = "sp07054_UT_Tree_Picker_Thematic_Mapping_Default_Dummy";
            this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource.DataSource = this.dataSet_UT_Mapping;
            // 
            // dataSet_UT_Mapping
            // 
            this.dataSet_UT_Mapping.DataSetName = "DataSet_UT_Mapping";
            this.dataSet_UT_Mapping.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView5
            // 
            this.gridView5.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView5.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView5.ColumnPanelRowHeight = 35;
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.colSymbol2,
            this.colSize2,
            this.colSymbol2Colour});
            this.gridView5.CustomizationFormBounds = new System.Drawing.Rectangle(1392, 541, 208, 275);
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "ItemDescription";
            this.gridColumn1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsColumn.TabStop = false;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 228;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "ItemCode";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsColumn.TabStop = false;
            this.gridColumn2.Width = 62;
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "ItemID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsColumn.TabStop = false;
            this.gridColumn3.Width = 48;
            // 
            // gridColumn4
            // 
            this.gridColumn4.FieldName = "HeaderID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsColumn.TabStop = false;
            this.gridColumn4.Width = 61;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "HeaderDescription";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsColumn.TabStop = false;
            this.gridColumn5.Width = 103;
            // 
            // gridColumn6
            // 
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsColumn.TabStop = false;
            this.gridColumn6.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "<color=red>Point Symbol</color>";
            this.gridColumn7.ColumnEdit = this.repositoryItemImageComboBox1;
            this.gridColumn7.FieldName = "Symbol";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 63;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle [Solid]", 34, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square [Solid]", 32, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond [Solid]", 33, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1 [Solid]", 36, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2 [Solid]", 37, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star [Solid]", 35, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Circle [Hollow]", 40, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Square [Hollow]", 38, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diamond [Hollow]", 39, 8),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 1 [Hollow]", 42, 9),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triangle 2 [Hollow]", 43, 10),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Star [Hollow]", 41, 11)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.imageListPointStyles;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "<color=red>Point Size</color>";
            this.gridColumn8.ColumnEdit = this.repositoryItemSpinEdit2;
            this.gridColumn8.FieldName = "Size";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 5;
            this.gridColumn8.Width = 54;
            // 
            // repositoryItemSpinEdit2
            // 
            this.repositoryItemSpinEdit2.AutoHeight = false;
            this.repositoryItemSpinEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2.DisplayFormat.FormatString = "f2";
            this.repositoryItemSpinEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemSpinEdit2.Mask.EditMask = "f2";
            this.repositoryItemSpinEdit2.MaxValue = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.MinValue = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.repositoryItemSpinEdit2.Name = "repositoryItemSpinEdit2";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "<color=red>Point Colour</color>";
            this.gridColumn9.ColumnEdit = this.repositoryItemColorEdit1;
            this.gridColumn9.FieldName = "SymbolColour";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            this.gridColumn9.Width = 56;
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.StoreColorAsInteger = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "<color=blue>Polygon Fill Colour</color>";
            this.gridColumn10.ColumnEdit = this.repositoryItemColorEdit1;
            this.gridColumn10.FieldName = "PolygonFillColour";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 7;
            this.gridColumn10.Width = 77;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "<color=blue>Polygon Line Colour</color>";
            this.gridColumn36.ColumnEdit = this.repositoryItemColorEdit1;
            this.gridColumn36.FieldName = "PolygonLineColour";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 11;
            this.gridColumn36.Width = 74;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "<color=blue>Polygon Fill Pattern</color>";
            this.gridColumn37.ColumnEdit = this.repositoryItemImageComboBox2;
            this.gridColumn37.FieldName = "PolygonFillPattern";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 8;
            this.gridColumn37.Width = 74;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Transparent", "1", 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Horizontal Lines", 3, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Vertical Lines", 4, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Diagonal Lines", 6, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Crosshatch", 8, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 1", 14, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dots 2", 17, 6)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            this.repositoryItemImageComboBox2.SmallImages = this.imageListFillPatterns;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "<color=blue>Polygon Fill Pattern Colour</color>";
            this.gridColumn38.ColumnEdit = this.repositoryItemColorEdit1;
            this.gridColumn38.FieldName = "PolygonFillPatternColour";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 9;
            this.gridColumn38.Width = 90;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "<color=blue>Polygon Line Width</color>";
            this.gridColumn39.ColumnEdit = this.repositoryItemImageComboBox3;
            this.gridColumn39.FieldName = "PolygonLineWidth";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 10;
            this.gridColumn39.Width = 71;
            // 
            // repositoryItemImageComboBox3
            // 
            this.repositoryItemImageComboBox3.AutoHeight = false;
            this.repositoryItemImageComboBox3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox3.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("1 Pixel", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("2 Pixels", 2, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("3 Pixels", 3, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("4 Pixels", 4, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("5 Pixels", 5, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("6 Pixels", 6, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("7 Pixels", 7, 6)});
            this.repositoryItemImageComboBox3.Name = "repositoryItemImageComboBox3";
            this.repositoryItemImageComboBox3.SmallImages = this.imageListLineWidths;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "<color=green>Polyline Colour</color>";
            this.gridColumn40.ColumnEdit = this.repositoryItemColorEdit1;
            this.gridColumn40.FieldName = "PolylineColour";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 13;
            this.gridColumn40.Width = 56;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "<color=green>Polyline Style</color>";
            this.gridColumn41.ColumnEdit = this.repositoryItemImageComboBox4;
            this.gridColumn41.FieldName = "PolylineStyle";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 12;
            this.gridColumn41.Width = 60;
            // 
            // repositoryItemImageComboBox4
            // 
            this.repositoryItemImageComboBox4.AutoHeight = false;
            this.repositoryItemImageComboBox4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox4.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Solid", 2, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dashed", 6, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dotted", 10, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Dot Dashed", 31, 3),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Arrowed", 54, 4),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Double", 63, 5),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Triple", 64, 6),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Dashed", 73, 7),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("Thick Squares", 93, 8)});
            this.repositoryItemImageComboBox4.Name = "repositoryItemImageComboBox4";
            this.repositoryItemImageComboBox4.SmallImages = this.imageListLineStyles;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "<color=green>Polyline Width</color>";
            this.gridColumn42.ColumnEdit = this.repositoryItemImageComboBox3;
            this.gridColumn42.FieldName = "PolylineWidth";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 14;
            this.gridColumn42.Width = 57;
            // 
            // colSymbol2
            // 
            this.colSymbol2.Caption = "Pole Point Symbol";
            this.colSymbol2.ColumnEdit = this.repositoryItemImageComboBox1;
            this.colSymbol2.FieldName = "Symbol2";
            this.colSymbol2.Name = "colSymbol2";
            this.colSymbol2.Visible = true;
            this.colSymbol2.VisibleIndex = 1;
            this.colSymbol2.Width = 68;
            // 
            // colSize2
            // 
            this.colSize2.Caption = "Pole Point Size";
            this.colSize2.ColumnEdit = this.repositoryItemSpinEdit2;
            this.colSize2.FieldName = "Size2";
            this.colSize2.Name = "colSize2";
            this.colSize2.Visible = true;
            this.colSize2.VisibleIndex = 2;
            this.colSize2.Width = 67;
            // 
            // colSymbol2Colour
            // 
            this.colSymbol2Colour.Caption = "Pole Point Colour";
            this.colSymbol2Colour.ColumnEdit = this.repositoryItemColorEdit1;
            this.colSymbol2Colour.FieldName = "Symbol2Colour";
            this.colSymbol2Colour.Name = "colSymbol2Colour";
            this.colSymbol2Colour.Visible = true;
            this.colSymbol2Colour.VisibleIndex = 3;
            this.colSymbol2Colour.Width = 72;
            // 
            // glueThematicField
            // 
            this.glueThematicField.EditValue = "No Thematic Field";
            this.glueThematicField.Location = new System.Drawing.Point(140, 271);
            this.glueThematicField.MenuManager = this.barManager1;
            this.glueThematicField.Name = "glueThematicField";
            editorButtonImageOptions1.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "No Thematics Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>clear</b> all thematic map object styling.\r\n\r\nMap Object styling w" +
    "ill be driven by the Default Styling settings.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions2.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Delete Thematic Set Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>delete</b> the selected Thematic Set.\r\n\r\n<color=red>Important Note" +
    ":</color> Thematic Sets can only be deleted by the person who created them.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.glueThematicField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "No Thematics", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Delete Thematic Set", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.glueThematicField.Properties.DataSource = this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource;
            this.glueThematicField.Properties.DisplayMember = "SetName";
            this.glueThematicField.Properties.NullText = "No Thematic Field";
            this.glueThematicField.Properties.NullValuePrompt = "No Thematic Field";
            this.glueThematicField.Properties.PopupView = this.gridView7;
            this.glueThematicField.Properties.ValueMember = "HeaderID";
            this.glueThematicField.Size = new System.Drawing.Size(721, 22);
            this.glueThematicField.StyleController = this.layoutControl1;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Thematic Field - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Controls the field used for thematic styling of map objects.";
            toolTipTitleItem5.LeftIndent = 6;
            toolTipTitleItem5.Text = "To clear thematics click the No Thematics button.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.Items.Add(toolTipSeparatorItem1);
            superToolTip4.Items.Add(toolTipTitleItem5);
            this.glueThematicField.SuperTip = superToolTip4;
            this.glueThematicField.TabIndex = 10;
            this.glueThematicField.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.glueThematicField_ButtonClick);
            this.glueThematicField.EditValueChanged += new System.EventHandler(this.glueThematicField_EditValueChanged);
            // 
            // sp01256ATTreePickerThematicMappingAvailableSetsBindingSource
            // 
            this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource.DataMember = "sp01256_AT_Tree_Picker_Thematic_Mapping_Available_Sets";
            this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource.DataSource = this.dataSet_AT_TreePicker;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSetType,
            this.colSetName,
            this.colOwnerID,
            this.colOwnerName,
            this.colOrder,
            this.colHeaderID,
            this.colBasePicklistID});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.GroupCount = 2;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceOddRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSetType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOwnerName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSetName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colSetType
            // 
            this.colSetType.Caption = "Thematic Set Type";
            this.colSetType.FieldName = "SetType";
            this.colSetType.Name = "colSetType";
            this.colSetType.OptionsColumn.AllowEdit = false;
            this.colSetType.OptionsColumn.AllowFocus = false;
            this.colSetType.OptionsColumn.ReadOnly = true;
            this.colSetType.Width = 136;
            // 
            // colSetName
            // 
            this.colSetName.Caption = "Thematic Set Name";
            this.colSetName.FieldName = "SetName";
            this.colSetName.Name = "colSetName";
            this.colSetName.OptionsColumn.AllowEdit = false;
            this.colSetName.OptionsColumn.AllowFocus = false;
            this.colSetName.OptionsColumn.ReadOnly = true;
            this.colSetName.Visible = true;
            this.colSetName.VisibleIndex = 0;
            this.colSetName.Width = 436;
            // 
            // colOwnerID
            // 
            this.colOwnerID.FieldName = "OwnerID";
            this.colOwnerID.Name = "colOwnerID";
            this.colOwnerID.OptionsColumn.AllowEdit = false;
            this.colOwnerID.OptionsColumn.AllowFocus = false;
            this.colOwnerID.OptionsColumn.ReadOnly = true;
            this.colOwnerID.Width = 57;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Created By";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Width = 140;
            // 
            // colOrder
            // 
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.Width = 39;
            // 
            // colHeaderID
            // 
            this.colHeaderID.FieldName = "HeaderID";
            this.colHeaderID.Name = "colHeaderID";
            this.colHeaderID.OptionsColumn.AllowEdit = false;
            this.colHeaderID.OptionsColumn.AllowFocus = false;
            this.colHeaderID.OptionsColumn.ReadOnly = true;
            this.colHeaderID.Width = 60;
            // 
            // colBasePicklistID
            // 
            this.colBasePicklistID.Caption = "Base PickList ID";
            this.colBasePicklistID.FieldName = "BasePicklistID";
            this.colBasePicklistID.Name = "colBasePicklistID";
            this.colBasePicklistID.OptionsColumn.AllowEdit = false;
            this.colBasePicklistID.OptionsColumn.AllowFocus = false;
            this.colBasePicklistID.OptionsColumn.ReadOnly = true;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.spinEditDefaultBandIncrement);
            this.layoutControl1.Controls.Add(this.btnCancel);
            this.layoutControl1.Controls.Add(this.btnOK);
            this.layoutControl1.Controls.Add(this.gridSplitContainer1);
            this.layoutControl1.Controls.Add(this.trackBarControl1);
            this.layoutControl1.Controls.Add(this.gridSplitContainer2);
            this.layoutControl1.Controls.Add(this.glueThematicField);
            this.layoutControl1.Controls.Add(this.ceUseThematicStyling);
            this.layoutControl1.Controls.Add(this.groupControl2);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup2;
            this.layoutControl1.Size = new System.Drawing.Size(1084, 594);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl2";
            // 
            // spinEditDefaultBandIncrement
            // 
            this.spinEditDefaultBandIncrement.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDefaultBandIncrement.Location = new System.Drawing.Point(986, 271);
            this.spinEditDefaultBandIncrement.MenuManager = this.barManager1;
            this.spinEditDefaultBandIncrement.Name = "spinEditDefaultBandIncrement";
            this.spinEditDefaultBandIncrement.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditDefaultBandIncrement.Properties.Mask.EditMask = "f2";
            this.spinEditDefaultBandIncrement.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditDefaultBandIncrement.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.spinEditDefaultBandIncrement.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.spinEditDefaultBandIncrement.Size = new System.Drawing.Size(79, 20);
            this.spinEditDefaultBandIncrement.StyleController = this.layoutControl1;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Default Band Increment - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = resources.GetString("toolTipItem3.Text");
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.spinEditDefaultBandIncrement.SuperTip = superToolTip3;
            this.spinEditDefaultBandIncrement.TabIndex = 39;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(556, 567);
            this.btnCancel.MinimumSize = new System.Drawing.Size(104, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(111, 20);
            this.btnCancel.StyleController = this.layoutControl1;
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(441, 567);
            this.btnOK.MinimumSize = new System.Drawing.Size(104, 0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(111, 20);
            this.btnOK.StyleController = this.layoutControl1;
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl5;
            this.gridSplitContainer1.Location = new System.Drawing.Point(19, 122);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl5);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1046, 78);
            this.gridSplitContainer1.TabIndex = 40;
            // 
            // trackBarControl1
            // 
            this.trackBarControl1.EditValue = null;
            this.trackBarControl1.Location = new System.Drawing.Point(540, 7);
            this.trackBarControl1.MenuManager = this.barManager1;
            this.trackBarControl1.Name = "trackBarControl1";
            this.trackBarControl1.Properties.Maximum = 100;
            this.trackBarControl1.Properties.ShowValueToolTip = true;
            this.trackBarControl1.Properties.TickFrequency = 5;
            this.trackBarControl1.Size = new System.Drawing.Size(537, 45);
            this.trackBarControl1.StyleController = this.layoutControl1;
            this.trackBarControl1.TabIndex = 37;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer2.Grid = this.gridControl6;
            this.gridSplitContainer2.Location = new System.Drawing.Point(19, 297);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1046, 244);
            this.gridSplitContainer2.TabIndex = 41;
            // 
            // ceUseThematicStyling
            // 
            this.ceUseThematicStyling.EditValue = 1;
            this.ceUseThematicStyling.Location = new System.Drawing.Point(19, 248);
            this.ceUseThematicStyling.MenuManager = this.barManager1;
            this.ceUseThematicStyling.Name = "ceUseThematicStyling";
            this.ceUseThematicStyling.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.ceUseThematicStyling.Properties.Appearance.Options.UseFont = true;
            this.ceUseThematicStyling.Properties.Caption = "Use Thematic Styling";
            this.ceUseThematicStyling.Properties.ValueChecked = 1;
            this.ceUseThematicStyling.Properties.ValueUnchecked = 0;
            this.ceUseThematicStyling.Size = new System.Drawing.Size(1046, 19);
            this.ceUseThematicStyling.StyleController = this.layoutControl1;
            this.ceUseThematicStyling.TabIndex = 34;
            this.ceUseThematicStyling.CheckedChanged += new System.EventHandler(this.ceUseThematicStyling_CheckedChanged);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.checkEdit2);
            this.groupControl2.Controls.Add(this.checkEdit3);
            this.groupControl2.Location = new System.Drawing.Point(7, 7);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(408, 45);
            this.groupControl2.TabIndex = 38;
            this.groupControl2.Text = "Map Object Sizing";
            // 
            // checkEdit2
            // 
            this.checkEdit2.EditValue = true;
            this.checkEdit2.Location = new System.Drawing.Point(8, 25);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Fixed";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(47, 19);
            this.checkEdit2.TabIndex = 35;
            this.checkEdit2.CheckedChanged += new System.EventHandler(this.checkEdit1_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Enabled = false;
            this.checkEdit3.Location = new System.Drawing.Point(63, 25);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Dynamic using Crown Diameter";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(177, 19);
            this.checkEdit3.TabIndex = 36;
            this.checkEdit3.TabStop = false;
            this.checkEdit3.CheckedChanged += new System.EventHandler(this.checkEdit2_CheckedChanged);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.emptySpaceItem2,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.emptySpaceItem3,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.emptySpaceItem4});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(1084, 594);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Map Object Styling";
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.splitterItem1,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 59);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1074, 491);
            this.layoutControlGroup1.Text = "Map Object Styling";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Default Styling";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(1066, 120);
            this.layoutControlGroup3.Text = "Default Styling";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridSplitContainer1;
            this.layoutControlItem3.CustomizationFormText = "Default Styling Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1050, 82);
            this.layoutControlItem3.Text = "Default Styling Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 120);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1066, 6);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Thematic Styling";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem8});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 126);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup4.Size = new System.Drawing.Size(1066, 335);
            this.layoutControlGroup4.Text = "Thematic Styling";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.glueThematicField;
            this.layoutControlItem1.CustomizationFormText = "Thematic Field:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(846, 26);
            this.layoutControlItem1.Text = "Thematic Field:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridSplitContainer2;
            this.layoutControlItem2.CustomizationFormText = "Thematic Styling Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1050, 248);
            this.layoutControlItem2.Text = "Thematic Styling Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.ceUseThematicStyling;
            this.layoutControlItem4.CustomizationFormText = "Use Thematic Styling:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1050, 23);
            this.layoutControlItem4.Text = "Use Thematic Styling:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.spinEditDefaultBandIncrement;
            this.layoutControlItem8.CustomizationFormText = "Default Band Increment:";
            this.layoutControlItem8.Location = new System.Drawing.Point(846, 23);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(204, 26);
            this.layoutControlItem8.Text = "Default Band Increment:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 550);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(1074, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnCancel;
            this.layoutControlItem6.CustomizationFormText = "Cancel Button";
            this.layoutControlItem6.Location = new System.Drawing.Point(549, 560);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(108, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(115, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "Cancel Button";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(664, 560);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(410, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnOK;
            this.layoutControlItem7.CustomizationFormText = "OK Button";
            this.layoutControlItem7.Location = new System.Drawing.Point(434, 560);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(108, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(115, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "OK Button";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 560);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(434, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.groupControl2;
            this.layoutControlItem5.CustomizationFormText = "Sizing:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(412, 49);
            this.layoutControlItem5.Text = "Sizing:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.trackBarControl1;
            this.layoutControlItem9.CustomizationFormText = "Object Transparency:";
            this.layoutControlItem9.Location = new System.Drawing.Point(412, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(662, 49);
            this.layoutControlItem9.Text = "Object Transparency:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(118, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 49);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(104, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(1074, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter
            // 
            this.sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter
            // 
            this.sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu_ThematicGrid
            // 
            this.popupMenu_ThematicGrid.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiBlockEditThematicStyles),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddBand, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDeleteBand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateDescriptionsFromBands),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSet, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveThematicSetAs)});
            this.popupMenu_ThematicGrid.Manager = this.barManager1;
            this.popupMenu_ThematicGrid.MenuCaption = "Thematic Menu";
            this.popupMenu_ThematicGrid.Name = "popupMenu_ThematicGrid";
            this.popupMenu_ThematicGrid.ShowCaption = true;
            // 
            // bbiBlockEditThematicStyles
            // 
            this.bbiBlockEditThematicStyles.Caption = "Block Edit thematic Styles";
            this.bbiBlockEditThematicStyles.Id = 25;
            this.bbiBlockEditThematicStyles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEditThematicStyles.ImageOptions.Image")));
            this.bbiBlockEditThematicStyles.Name = "bbiBlockEditThematicStyles";
            this.bbiBlockEditThematicStyles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBlockEditThematicStyles_ItemClick);
            // 
            // bbiAddBand
            // 
            this.bbiAddBand.Caption = "Add Band";
            this.bbiAddBand.Id = 28;
            this.bbiAddBand.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddBand.ImageOptions.Image")));
            this.bbiAddBand.Name = "bbiAddBand";
            this.bbiAddBand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddBand_ItemClick);
            // 
            // bbiDeleteBand
            // 
            this.bbiDeleteBand.Caption = "Delete Band";
            this.bbiDeleteBand.Id = 29;
            this.bbiDeleteBand.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDeleteBand.ImageOptions.Image")));
            this.bbiDeleteBand.Name = "bbiDeleteBand";
            this.bbiDeleteBand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDeleteBand_ItemClick);
            // 
            // bbiCreateDescriptionsFromBands
            // 
            this.bbiCreateDescriptionsFromBands.Caption = "Create Item Descriptions From Bands";
            this.bbiCreateDescriptionsFromBands.Id = 30;
            this.bbiCreateDescriptionsFromBands.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCreateDescriptionsFromBands.ImageOptions.Image")));
            this.bbiCreateDescriptionsFromBands.Name = "bbiCreateDescriptionsFromBands";
            this.bbiCreateDescriptionsFromBands.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreatDescriptionsFromBands_ItemClick);
            // 
            // bbiSaveThematicSet
            // 
            this.bbiSaveThematicSet.Caption = "Save Thematic Set";
            this.bbiSaveThematicSet.Id = 26;
            this.bbiSaveThematicSet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSet.ImageOptions.Image")));
            this.bbiSaveThematicSet.Name = "bbiSaveThematicSet";
            this.bbiSaveThematicSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveThematicSet_ItemClick);
            // 
            // bbiSaveThematicSetAs
            // 
            this.bbiSaveThematicSetAs.Caption = "Save Thematic Set As";
            this.bbiSaveThematicSetAs.Id = 27;
            this.bbiSaveThematicSetAs.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSaveThematicSetAs.ImageOptions.Image")));
            this.bbiSaveThematicSetAs.Name = "bbiSaveThematicSetAs";
            this.bbiSaveThematicSetAs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveThematicSetAs_ItemClick);
            // 
            // sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter
            // 
            this.sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // frm_UT_Mapping_Styles
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(1084, 594);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Mapping_Styles";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Map Object - Properties";
            this.Load += new System.EventHandler(this.frm_UT_Mapping_Styles_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicPointStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit_ThematicPointSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit_ThematicColour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicFillPattern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicLineWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox_ThematicLineStyle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit_BandSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01255ATTreePickerThematicMappingDefaultItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_TreePicker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07054UTTreePickerThematicMappingDefaultDummyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Mapping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueThematicField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01256ATTreePickerThematicMappingAvailableSetsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDefaultBandIncrement.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ceUseThematicStyling.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu_ThematicGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DataSet_AT_TreePicker dataSet_AT_TreePicker;
        private System.Windows.Forms.ImageList imageListPointStyles;
        private System.Windows.Forms.ImageList imageListFillPatterns;
        private System.Windows.Forms.ImageList imageListLineWidths;
        private System.Windows.Forms.ImageList imageListLineStyles;
        private DevExpress.XtraEditors.GridLookUpEdit glueThematicField;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colSetType;
        private DevExpress.XtraGrid.Columns.GridColumn colSetName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerID;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderID;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource sp01255ATTreePickerThematicMappingDefaultItemsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbol;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox_ThematicPointStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit_ThematicPointSize;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbolColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit_ThematicColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonLineColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillPattern;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox_ThematicFillPattern;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonFillPatternColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolygonLineWidth;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox_ThematicLineWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colPolylineColour;
        private DevExpress.XtraGrid.Columns.GridColumn colPolylineStyle;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox_ThematicLineStyle;
        private DevExpress.XtraGrid.Columns.GridColumn colPolylineWidth;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter sp01255_AT_Tree_Picker_Thematic_Mapping_Default_ItemsTableAdapter;
        private System.Windows.Forms.BindingSource sp01256ATTreePickerThematicMappingAvailableSetsBindingSource;
        private WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter sp01256_AT_Tree_Picker_Thematic_Mapping_Available_SetsTableAdapter;
        private DevExpress.XtraBars.PopupMenu popupMenu_ThematicGrid;
        private DevExpress.XtraBars.BarButtonItem bbiBlockEditThematicStyles;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSet;
        private DevExpress.XtraBars.BarButtonItem bbiSaveThematicSetAs;
        private DevExpress.XtraGrid.Columns.GridColumn colPicklistItemID;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.CheckEdit ceUseThematicStyling;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colShowInLegend;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.ImageList imageListLegend;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndBand;
        private DevExpress.XtraGrid.Columns.GridColumn colStartBand;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit_BandSize;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn colBasePicklistID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn49;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn50;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn52;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn55;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn57;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn58;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn60;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn61;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn62;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn63;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn64;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn65;
        private DevExpress.XtraBars.BarButtonItem bbiAddBand;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteBand;
        private DevExpress.XtraEditors.SpinEdit spinEditDefaultBandIncrement;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.BarButtonItem bbiCreateDescriptionsFromBands;
        private DevExpress.XtraEditors.TrackBarControl trackBarControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private System.Windows.Forms.BindingSource sp07054UTTreePickerThematicMappingDefaultDummyBindingSource;
        private DataSet_UT_Mapping dataSet_UT_Mapping;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbol2;
        private DevExpress.XtraGrid.Columns.GridColumn colSize2;
        private DevExpress.XtraGrid.Columns.GridColumn colSymbol2Colour;
        private DataSet_UT_MappingTableAdapters.sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter sp07054_UT_Tree_Picker_Thematic_Mapping_Default_DummyTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
    }
}

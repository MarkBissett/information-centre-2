using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using DevExpress.Utils;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Layer_Label_Properties : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int _Transparency = 0;
        public int _LineColour = 0;
        public int _LineWidth = 1;
        public string _LabelFontName = "Tahoma";
        public int _LabelFontSize = 8;
        public int _LabelFontColour = 0;
        public int _LabelAngle = 0;
        public string _LabelPosition = "CentreRight";
        public int _LabelOffset = 6;
        public int _LabelVisibleRangeFrom = 0;
        public int _LabelVisibleRangeTo = 10000;
        public int _LabelOverlap = 1;
        public int _LabelDuplicates = 1;
        #endregion

        public frm_UT_Mapping_Layer_Label_Properties()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Layer_Label_Properties_Load(object sender, EventArgs e)
        {
            this.FormID = 500013;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked at end of event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            tbcTransparency.EditValue = _Transparency;
            colorEdit1.EditValue = _LineColour;
            spinEdit1.EditValue = _LineWidth;
            fontEdit1.EditValue = _LabelFontName;
            seFontSize.EditValue = _LabelFontSize;
            colorEditLabelFontColour.EditValue = _LabelFontColour;
            seFontAngle.EditValue = _LabelAngle;
            popupContainerEditLabelPosition.EditValue = _LabelPosition;
            seFontOffset.EditValue = _LabelOffset;
            seLabelRangeFrom.EditValue = _LabelVisibleRangeFrom;
            seLabelRangeTo.EditValue = _LabelVisibleRangeTo;
            ceShowOverlaps.EditValue = _LabelOverlap;
            ceDuplicates.EditValue = _LabelDuplicates;
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

        }

        private void popupContainerEditLabelPosition_QueryPopUp(object sender, CancelEventArgs e)
        {
            switch (popupContainerEditLabelPosition.EditValue.ToString())
            {
                case "BottomCentre":
                    alignmentControl1.Alignment = ContentAlignment.BottomCenter;
                    break;
                case "BottomLeft":
                    alignmentControl1.Alignment = ContentAlignment.BottomLeft;
                    break;
                case "BottomRight":
                    alignmentControl1.Alignment = ContentAlignment.BottomRight;
                    break;
                case "CentreCenter":
                    alignmentControl1.Alignment = ContentAlignment.MiddleCenter;  //.CenterCenter;
                    break;
                case "CentreLeft":
                    alignmentControl1.Alignment = ContentAlignment.MiddleLeft;  //.CenterLeft;
                    break;
                case "CentreRight":
                    alignmentControl1.Alignment = ContentAlignment.MiddleRight;  //.CenterRight;
                    break;
                case "TopCentre":
                    alignmentControl1.Alignment = ContentAlignment.TopCenter;
                    break;
                case "TopLeft":
                    alignmentControl1.Alignment = ContentAlignment.TopLeft;
                    break;
                case "TopRight":
                    alignmentControl1.Alignment = ContentAlignment.TopRight;
                    break;
                default:
                    break;
            }
        }

        private void alignmentControl1_AlignmentChanged(object sender, EventArgs e)
        {
            switch (alignmentControl1.Alignment.ToString())
            {
                case "MiddleCenter":
                    popupContainerEditLabelPosition.EditValue = "CenterCenter";
                    break;
                case "MiddleLeft":
                    popupContainerEditLabelPosition.EditValue = "CenterLeft";
                    break;
                case "MiddleRight":
                    popupContainerEditLabelPosition.EditValue = "CenterRight";
                    break;
                default:  // All other choices //
                    popupContainerEditLabelPosition.EditValue = alignmentControl1.Alignment.ToString();
                    break;
            }
            // Close the dropdown accepting the user's choice //
            Control obj = sender as Control;
            (obj.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _Transparency = Convert.ToInt32(tbcTransparency.EditValue);
            _LineColour = Convert.ToInt32(colorEdit1.EditValue);
            _LineWidth = Convert.ToInt32(spinEdit1.EditValue);
            _LabelFontName = fontEdit1.EditValue.ToString();
            _LabelFontSize = Convert.ToInt32(seFontSize.EditValue);
            _LabelFontColour =  Convert.ToInt32(colorEditLabelFontColour.EditValue);
            _LabelAngle =  Convert.ToInt32(seFontAngle.EditValue);
            _LabelPosition = popupContainerEditLabelPosition.EditValue.ToString();
            _LabelOffset = Convert.ToInt32(seFontOffset.EditValue);
            _LabelVisibleRangeFrom = Convert.ToInt32(seLabelRangeFrom.EditValue);
            _LabelVisibleRangeTo = Convert.ToInt32(seLabelRangeTo.EditValue);
            _LabelOverlap = Convert.ToInt32(ceShowOverlaps.EditValue);
            _LabelDuplicates = Convert.ToInt32(ceDuplicates.EditValue);

            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails any validation tests //
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


    
    }
}


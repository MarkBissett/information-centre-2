namespace WoodPlan5
{
    partial class frm_UT_Analysis_Poles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Analysis_Poles));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup1 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup2 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.XtraPivotGrid.PivotGridGroup pivotGridGroup3 = new DevExpress.XtraPivotGrid.PivotGridGroup();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.fieldLastInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fielLastInspectionWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyQuarter = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyWeek = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_Reports = new WoodPlan5.DataSet_AT_Reports();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnAnalyse = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlSelectedCount = new DevExpress.XtraEditors.LabelControl();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07320UTAnalysisPolesAvailablePolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionElapsedDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnSurvey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveySpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlDateFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.deFromDate = new DevExpress.XtraEditors.DateEdit();
            this.deToDate = new DevExpress.XtraEditors.DateEdit();
            this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.sp07321UTAnalysisPolesForAnalysisBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldPoleNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionCycle1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInspectionUnitDesc1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNextInspectionDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsTransformer1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTransformerNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRemarks1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClientCode1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldCircuitNumber1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldVoltageType1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFeederName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldRegionName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPrimaryName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSubAreaName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLastInspectionElapsedDays1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldMapID1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOnSurvey1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveySpanClear1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyorName1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyDate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldInfestationRate1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsShutdownRequired1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldHotGloveRequired1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldLinesmanPossible1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldClearanceDistance1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTrafficManagementRequired1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTrafficManagementResolved1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldFiveYearClearanceAchieved1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeWithin3Meters1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTreeClimbable1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIsBaseClimbable1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldSurveyStatus1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldEstimatedHours = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActualHours = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIncomeValue1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldG55Category1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldPoleCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldOnSurveyPoleCount1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCountTotal1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCountToBeStarted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCountStarted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCountOnHold1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldActionCountCompleted1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNotOnSurveyPoleCount = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldReactive = new DevExpress.XtraPivotGrid.PivotGridField();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter();
            this.sp07320_UT_Analysis_Poles_Available_PolesTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07320_UT_Analysis_Poles_Available_PolesTableAdapter();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiToggleAvailableColumnsVisibility = new DevExpress.XtraBars.BarButtonItem();
            this.pmChart = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiRotateAxis = new DevExpress.XtraBars.BarButtonItem();
            this.bbiChartWizard = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.buttonEditFilterCircuits = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemButtonEditFilterCircuits = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.bbiReloadData = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAnalyse = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItemStatusFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStatusFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07320UTAnalysisPolesAvailablePolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).BeginInit();
            this.popupContainerControlDateFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07321UTAnalysisPolesForAnalysisBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(1293, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1293, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.bbiChartWizard,
            this.bbiRotateAxis,
            this.barEditItemStatusFilter,
            this.barEditItemDateFilter,
            this.bbiReloadData,
            this.bbiAnalyse,
            this.bbiToggleAvailableColumnsVisibility,
            this.barEditItem1,
            this.buttonEditFilterCircuits});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditStatusFilter,
            this.repositoryItemPopupContainerEditDateFilter,
            this.repositoryItemTextEdit2,
            this.repositoryItemButtonEditFilterCircuits});
            // 
            // fieldLastInspectionYear
            // 
            this.fieldLastInspectionYear.AreaIndex = 44;
            this.fieldLastInspectionYear.Caption = "Last Inspection Year";
            this.fieldLastInspectionYear.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionYear.FieldName = "LastInspectionDate";
            this.fieldLastInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldLastInspectionYear.Name = "fieldLastInspectionYear";
            this.fieldLastInspectionYear.UnboundFieldName = "fieldLastInspectionYear";
            // 
            // fieldLastInspectionQuarter
            // 
            this.fieldLastInspectionQuarter.AreaIndex = 1;
            this.fieldLastInspectionQuarter.Caption = "Last Inspection Quarter";
            this.fieldLastInspectionQuarter.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionQuarter.FieldName = "LastInspectionDate";
            this.fieldLastInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldLastInspectionQuarter.Name = "fieldLastInspectionQuarter";
            this.fieldLastInspectionQuarter.UnboundFieldName = "fieldLastInspectionQuarter";
            this.fieldLastInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldLastInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldLastInspectionQuarter.Visible = false;
            // 
            // fieldLastInspectionMonth
            // 
            this.fieldLastInspectionMonth.AreaIndex = 2;
            this.fieldLastInspectionMonth.Caption = "Last Inspection Month";
            this.fieldLastInspectionMonth.ExpandedInFieldsGroup = false;
            this.fieldLastInspectionMonth.FieldName = "LastInspectionDate";
            this.fieldLastInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldLastInspectionMonth.Name = "fieldLastInspectionMonth";
            this.fieldLastInspectionMonth.UnboundFieldName = "fieldLastInspectionMonth";
            this.fieldLastInspectionMonth.Visible = false;
            // 
            // fielLastInspectionWeek
            // 
            this.fielLastInspectionWeek.AreaIndex = 3;
            this.fielLastInspectionWeek.Caption = "Last Inspection Week";
            this.fielLastInspectionWeek.ExpandedInFieldsGroup = false;
            this.fielLastInspectionWeek.FieldName = "LastInspectionDate";
            this.fielLastInspectionWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fielLastInspectionWeek.Name = "fielLastInspectionWeek";
            this.fielLastInspectionWeek.UnboundFieldName = "fielLastInspectionWeek";
            this.fielLastInspectionWeek.ValueFormat.FormatString = "Week {0}";
            this.fielLastInspectionWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fielLastInspectionWeek.Visible = false;
            // 
            // fieldNextInspectionYear
            // 
            this.fieldNextInspectionYear.AreaIndex = 45;
            this.fieldNextInspectionYear.Caption = "Next Inspection Year";
            this.fieldNextInspectionYear.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionYear.FieldName = "NextInspectionDate";
            this.fieldNextInspectionYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldNextInspectionYear.Name = "fieldNextInspectionYear";
            this.fieldNextInspectionYear.UnboundFieldName = "fieldNextInspectionYear";
            // 
            // fieldNextInspectionQuarter
            // 
            this.fieldNextInspectionQuarter.AreaIndex = 1;
            this.fieldNextInspectionQuarter.Caption = "Next Inspection Quarter";
            this.fieldNextInspectionQuarter.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionQuarter.FieldName = "NextInspectionDate";
            this.fieldNextInspectionQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldNextInspectionQuarter.Name = "fieldNextInspectionQuarter";
            this.fieldNextInspectionQuarter.UnboundFieldName = "fieldNextInspectionQuarter";
            this.fieldNextInspectionQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldNextInspectionQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionQuarter.Visible = false;
            // 
            // fieldNextInspectionMonth
            // 
            this.fieldNextInspectionMonth.AreaIndex = 1;
            this.fieldNextInspectionMonth.Caption = "Next Inspection Month";
            this.fieldNextInspectionMonth.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionMonth.FieldName = "NextInspectionDate";
            this.fieldNextInspectionMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldNextInspectionMonth.Name = "fieldNextInspectionMonth";
            this.fieldNextInspectionMonth.UnboundFieldName = "fieldNextInspectionMonth";
            this.fieldNextInspectionMonth.Visible = false;
            // 
            // fieldNextInspectionWeek
            // 
            this.fieldNextInspectionWeek.AreaIndex = 1;
            this.fieldNextInspectionWeek.Caption = "Next Inspection Week";
            this.fieldNextInspectionWeek.ExpandedInFieldsGroup = false;
            this.fieldNextInspectionWeek.FieldName = "NextInspectionDate";
            this.fieldNextInspectionWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldNextInspectionWeek.Name = "fieldNextInspectionWeek";
            this.fieldNextInspectionWeek.UnboundFieldName = "fieldNextInspectionWeek";
            this.fieldNextInspectionWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldNextInspectionWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldNextInspectionWeek.Visible = false;
            // 
            // fieldSurveyYear
            // 
            this.fieldSurveyYear.AreaIndex = 46;
            this.fieldSurveyYear.Caption = "Survey Year";
            this.fieldSurveyYear.ExpandedInFieldsGroup = false;
            this.fieldSurveyYear.FieldName = "SurveyDate";
            this.fieldSurveyYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.fieldSurveyYear.Name = "fieldSurveyYear";
            this.fieldSurveyYear.UnboundFieldName = "fieldSurveyYear";
            // 
            // fieldSurveyQuarter
            // 
            this.fieldSurveyQuarter.AreaIndex = 1;
            this.fieldSurveyQuarter.Caption = "Survey Quarter";
            this.fieldSurveyQuarter.ExpandedInFieldsGroup = false;
            this.fieldSurveyQuarter.FieldName = "SurveyDate";
            this.fieldSurveyQuarter.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateQuarter;
            this.fieldSurveyQuarter.Name = "fieldSurveyQuarter";
            this.fieldSurveyQuarter.UnboundFieldName = "fieldSurveyQuarter";
            this.fieldSurveyQuarter.ValueFormat.FormatString = "Qtr {0}";
            this.fieldSurveyQuarter.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSurveyQuarter.Visible = false;
            // 
            // fieldSurveyMonth
            // 
            this.fieldSurveyMonth.AreaIndex = 1;
            this.fieldSurveyMonth.Caption = "Survey Month";
            this.fieldSurveyMonth.ExpandedInFieldsGroup = false;
            this.fieldSurveyMonth.FieldName = "SurveyDate";
            this.fieldSurveyMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.fieldSurveyMonth.Name = "fieldSurveyMonth";
            this.fieldSurveyMonth.UnboundFieldName = "fieldSurveyMonth";
            this.fieldSurveyMonth.Visible = false;
            // 
            // fieldSurveyWeek
            // 
            this.fieldSurveyWeek.AreaIndex = 2;
            this.fieldSurveyWeek.Caption = "Survey Week";
            this.fieldSurveyWeek.ExpandedInFieldsGroup = false;
            this.fieldSurveyWeek.FieldName = "SurveyDate";
            this.fieldSurveyWeek.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateWeekOfYear;
            this.fieldSurveyWeek.Name = "fieldSurveyWeek";
            this.fieldSurveyWeek.UnboundFieldName = "fieldSurveyWeek";
            this.fieldSurveyWeek.ValueFormat.FormatString = "Week {0}";
            this.fieldSurveyWeek.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldSurveyWeek.Visible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "ProjectManDataSet";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_Reports
            // 
            this.dataSet_AT_Reports.DataSetName = "DataSet_AT_Reports";
            this.dataSet_AT_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("eca7f263-f03a-476e-be2c-7e2164986cdc");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(381, 200);
            this.dockPanel1.Size = new System.Drawing.Size(381, 494);
            this.dockPanel1.Text = "Data Supply";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnAnalyse);
            this.dockPanel1_Container.Controls.Add(this.labelControlSelectedCount);
            this.dockPanel1_Container.Controls.Add(this.standaloneBarDockControl1);
            this.dockPanel1_Container.Controls.Add(this.gridControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 29);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(375, 462);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnAnalyse
            // 
            this.btnAnalyse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnalyse.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAnalyse.Appearance.Options.UseFont = true;
            this.btnAnalyse.Image = ((System.Drawing.Image)(resources.GetObject("btnAnalyse.Image")));
            this.btnAnalyse.Location = new System.Drawing.Point(263, 436);
            this.btnAnalyse.Name = "btnAnalyse";
            this.btnAnalyse.Size = new System.Drawing.Size(112, 26);
            this.btnAnalyse.TabIndex = 5;
            this.btnAnalyse.Text = "Analyse Data";
            this.btnAnalyse.Click += new System.EventHandler(this.btnAnalyse_Click);
            // 
            // labelControlSelectedCount
            // 
            this.labelControlSelectedCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedCount.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControlSelectedCount.Appearance.Image")));
            this.labelControlSelectedCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedCount.Location = new System.Drawing.Point(2, 440);
            this.labelControlSelectedCount.Name = "labelControlSelectedCount";
            this.labelControlSelectedCount.Size = new System.Drawing.Size(253, 18);
            this.labelControlSelectedCount.TabIndex = 7;
            this.labelControlSelectedCount.Text = "        0 Poles Selected For Analysis";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(375, 34);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp07320UTAnalysisPolesAvailablePolesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 34);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(375, 401);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07320UTAnalysisPolesAvailablePolesBindingSource
            // 
            this.sp07320UTAnalysisPolesAvailablePolesBindingSource.DataMember = "sp07320_UT_Analysis_Poles_Available_Poles";
            this.sp07320UTAnalysisPolesAvailablePolesBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleID,
            this.colCircuitID,
            this.colPoleNumber,
            this.colPoleTypeID,
            this.colStatusID,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colX,
            this.colY,
            this.colLatitude,
            this.colLongitude,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.colRemarks1,
            this.colGUID,
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colCircuitStatus,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colRegionName,
            this.colStatus,
            this.colPrimaryName,
            this.colSubAreaName,
            this.colRegionID,
            this.colSubAreaID,
            this.colPrimaryID,
            this.colFeederID,
            this.colPoleType,
            this.colLastInspectionElapsedDays,
            this.colMapID,
            this.colOnSurvey,
            this.colSurveySpanClear,
            this.colSurveyorName});
            this.gridView1.CustomizationFormBounds = new System.Drawing.Rectangle(1158, 513, 208, 191);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 3;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            this.colPoleID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCircuitID
            // 
            this.colCircuitID.Caption = "Circuit ID";
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            this.colCircuitID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 3;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleTypeID
            // 
            this.colPoleTypeID.Caption = "Pole Type ID";
            this.colPoleTypeID.FieldName = "PoleTypeID";
            this.colPoleTypeID.Name = "colPoleTypeID";
            this.colPoleTypeID.OptionsColumn.AllowEdit = false;
            this.colPoleTypeID.OptionsColumn.AllowFocus = false;
            this.colPoleTypeID.OptionsColumn.ReadOnly = true;
            this.colPoleTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 7;
            this.colLastInspectionDate.Width = 94;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 9;
            this.colInspectionCycle.Width = 55;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Cycle Unit";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInspectionUnitDesc.Visible = true;
            this.colInspectionUnitDesc.VisibleIndex = 10;
            this.colInspectionUnitDesc.Width = 69;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 11;
            this.colNextInspectionDate.Width = 97;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colX.Width = 59;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colY.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLatitude.Width = 66;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLongitude.Width = 68;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 13;
            this.colIsTransformer.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTransformerNumber.Visible = true;
            this.colTransformerNumber.VisibleIndex = 14;
            this.colTransformerNumber.Width = 120;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 17;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            this.colGUID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 130;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientCode1.Width = 76;
            // 
            // colCircuitStatus
            // 
            this.colCircuitStatus.Caption = "Client Status";
            this.colCircuitStatus.FieldName = "CircuitStatus";
            this.colCircuitStatus.Name = "colCircuitStatus";
            this.colCircuitStatus.OptionsColumn.AllowEdit = false;
            this.colCircuitStatus.OptionsColumn.AllowFocus = false;
            this.colCircuitStatus.OptionsColumn.ReadOnly = true;
            this.colCircuitStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitStatus.Width = 82;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 0;
            this.colCircuitName.Width = 130;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Width = 91;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 15;
            this.colVoltageType.Width = 84;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 2;
            this.colFeederName.Width = 130;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 0;
            this.colRegionName.Width = 130;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 16;
            this.colStatus.Width = 63;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 1;
            this.colPrimaryName.Width = 130;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 0;
            this.colSubAreaName.Width = 130;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            this.colRegionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSubAreaID
            // 
            this.colSubAreaID.Caption = "Sub-Area ID";
            this.colSubAreaID.FieldName = "SubAreaID";
            this.colSubAreaID.Name = "colSubAreaID";
            this.colSubAreaID.OptionsColumn.AllowEdit = false;
            this.colSubAreaID.OptionsColumn.AllowFocus = false;
            this.colSubAreaID.OptionsColumn.ReadOnly = true;
            this.colSubAreaID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaID.Width = 80;
            // 
            // colPrimaryID
            // 
            this.colPrimaryID.Caption = "Primary ID";
            this.colPrimaryID.FieldName = "PrimaryID";
            this.colPrimaryID.Name = "colPrimaryID";
            this.colPrimaryID.OptionsColumn.AllowEdit = false;
            this.colPrimaryID.OptionsColumn.AllowFocus = false;
            this.colPrimaryID.OptionsColumn.ReadOnly = true;
            this.colPrimaryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colFeederID
            // 
            this.colFeederID.Caption = "Feeder ID";
            this.colFeederID.FieldName = "FeederID";
            this.colFeederID.Name = "colFeederID";
            this.colFeederID.OptionsColumn.AllowEdit = false;
            this.colFeederID.OptionsColumn.AllowFocus = false;
            this.colFeederID.OptionsColumn.ReadOnly = true;
            this.colFeederID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleType.Visible = true;
            this.colPoleType.VisibleIndex = 12;
            // 
            // colLastInspectionElapsedDays
            // 
            this.colLastInspectionElapsedDays.Caption = "Elapsed Days";
            this.colLastInspectionElapsedDays.FieldName = "LastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.Name = "colLastInspectionElapsedDays";
            this.colLastInspectionElapsedDays.OptionsColumn.AllowEdit = false;
            this.colLastInspectionElapsedDays.OptionsColumn.AllowFocus = false;
            this.colLastInspectionElapsedDays.OptionsColumn.ReadOnly = true;
            this.colLastInspectionElapsedDays.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLastInspectionElapsedDays.Visible = true;
            this.colLastInspectionElapsedDays.VisibleIndex = 8;
            this.colLastInspectionElapsedDays.Width = 85;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Width = 240;
            // 
            // colOnSurvey
            // 
            this.colOnSurvey.Caption = "Surveyed";
            this.colOnSurvey.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colOnSurvey.FieldName = "OnSurvey";
            this.colOnSurvey.Name = "colOnSurvey";
            this.colOnSurvey.OptionsColumn.AllowEdit = false;
            this.colOnSurvey.OptionsColumn.AllowFocus = false;
            this.colOnSurvey.OptionsColumn.ReadOnly = true;
            this.colOnSurvey.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colOnSurvey.Visible = true;
            this.colOnSurvey.VisibleIndex = 4;
            this.colOnSurvey.Width = 67;
            // 
            // colSurveySpanClear
            // 
            this.colSurveySpanClear.Caption = "Span Clear";
            this.colSurveySpanClear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSurveySpanClear.FieldName = "SurveySpanClear";
            this.colSurveySpanClear.Name = "colSurveySpanClear";
            this.colSurveySpanClear.OptionsColumn.AllowEdit = false;
            this.colSurveySpanClear.OptionsColumn.AllowFocus = false;
            this.colSurveySpanClear.OptionsColumn.ReadOnly = true;
            this.colSurveySpanClear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveySpanClear.Visible = true;
            this.colSurveySpanClear.VisibleIndex = 5;
            this.colSurveySpanClear.Width = 73;
            // 
            // colSurveyorName
            // 
            this.colSurveyorName.Caption = "Surveyor Name";
            this.colSurveyorName.FieldName = "SurveyorName";
            this.colSurveyorName.Name = "colSurveyorName";
            this.colSurveyorName.OptionsColumn.AllowEdit = false;
            this.colSurveyorName.OptionsColumn.AllowFocus = false;
            this.colSurveyorName.OptionsColumn.ReadOnly = true;
            this.colSurveyorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyorName.Visible = true;
            this.colSurveyorName.VisibleIndex = 6;
            this.colSurveyorName.Width = 95;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(381, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.pivotGridControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(912, 494);
            this.splitContainerControl1.SplitterPosition = 388;
            this.splitContainerControl1.TabIndex = 8;
            this.splitContainerControl1.Text = "splitContainerControl2";
            // 
            // popupContainerControlDateFilter
            // 
            this.popupContainerControlDateFilter.Controls.Add(this.btnDateFilterOK);
            this.popupContainerControlDateFilter.Controls.Add(this.groupControl2);
            this.popupContainerControlDateFilter.Location = new System.Drawing.Point(6, 283);
            this.popupContainerControlDateFilter.Name = "popupContainerControlDateFilter";
            this.popupContainerControlDateFilter.Size = new System.Drawing.Size(313, 82);
            this.popupContainerControlDateFilter.TabIndex = 35;
            // 
            // btnDateFilterOK
            // 
            this.btnDateFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateFilterOK.Location = new System.Drawing.Point(3, 57);
            this.btnDateFilterOK.Name = "btnDateFilterOK";
            this.btnDateFilterOK.Size = new System.Drawing.Size(35, 22);
            this.btnDateFilterOK.TabIndex = 18;
            this.btnDateFilterOK.Text = "OK";
            this.btnDateFilterOK.Click += new System.EventHandler(this.btnDateFilterOK_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.deFromDate);
            this.groupControl2.Controls.Add(this.deToDate);
            this.groupControl2.Location = new System.Drawing.Point(3, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(307, 51);
            this.groupControl2.TabIndex = 11;
            this.groupControl2.Text = "Date Filter";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(170, 28);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 12;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 11;
            this.labelControl1.Text = "From:";
            // 
            // deFromDate
            // 
            this.deFromDate.EditValue = null;
            this.deFromDate.Location = new System.Drawing.Point(41, 25);
            this.deFromDate.Name = "deFromDate";
            this.deFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFromDate.Properties.Mask.EditMask = "g";
            this.deFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deFromDate.Properties.NullText = "Not Used";
            this.deFromDate.Size = new System.Drawing.Size(108, 20);
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "From Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "The Poles Surveyed Status will be taken from between the From and To Date.\r\n";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.deFromDate.SuperTip = superToolTip1;
            this.deFromDate.TabIndex = 10;
            // 
            // deToDate
            // 
            this.deToDate.EditValue = null;
            this.deToDate.Location = new System.Drawing.Point(192, 25);
            this.deToDate.Name = "deToDate";
            this.deToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.deToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deToDate.Properties.Mask.EditMask = "g";
            this.deToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.deToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.deToDate.Properties.NullText = "Not Used";
            this.deToDate.Size = new System.Drawing.Size(108, 20);
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "To Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "The Poles Surveyed Status will be taken from between the From and To Date.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.deToDate.SuperTip = superToolTip2;
            this.deToDate.TabIndex = 7;
            // 
            // pivotGridControl1
            // 
            this.pivotGridControl1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControl1.DataSource = this.sp07321UTAnalysisPolesForAnalysisBindingSource;
            this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldPoleNumber1,
            this.fieldLastInspectionDate1,
            this.fieldInspectionCycle1,
            this.fieldInspectionUnitDesc1,
            this.fieldNextInspectionDate1,
            this.fieldIsTransformer1,
            this.fieldTransformerNumber1,
            this.fieldRemarks1,
            this.fieldClientName1,
            this.fieldClientCode1,
            this.fieldCircuitStatus1,
            this.fieldCircuitName1,
            this.fieldCircuitNumber1,
            this.fieldVoltageType1,
            this.fieldFeederName1,
            this.fieldRegionName1,
            this.fieldStatus1,
            this.fieldPrimaryName1,
            this.fieldSubAreaName1,
            this.fieldLastInspectionElapsedDays1,
            this.fieldMapID1,
            this.fieldOnSurvey1,
            this.fieldSurveySpanClear1,
            this.fieldSurveyorName1,
            this.fieldSurveyDate1,
            this.fieldInfestationRate1,
            this.fieldIsShutdownRequired1,
            this.fieldHotGloveRequired1,
            this.fieldLinesmanPossible1,
            this.fieldClearanceDistance1,
            this.fieldTrafficManagementRequired1,
            this.fieldTrafficManagementResolved1,
            this.fieldFiveYearClearanceAchieved1,
            this.fieldTreeWithin3Meters1,
            this.fieldTreeClimbable1,
            this.fieldIsBaseClimbable1,
            this.fieldSurveyStatus1,
            this.fieldEstimatedHours,
            this.fieldActualHours,
            this.fieldIncomeValue1,
            this.fieldG55Category1,
            this.fieldPoleCount1,
            this.fieldOnSurveyPoleCount1,
            this.fieldActionCountTotal1,
            this.fieldActionCountToBeStarted1,
            this.fieldActionCountStarted1,
            this.fieldActionCountOnHold1,
            this.fieldActionCountCompleted1,
            this.fieldLastInspectionYear,
            this.fieldLastInspectionQuarter,
            this.fieldLastInspectionMonth,
            this.fielLastInspectionWeek,
            this.fieldNextInspectionYear,
            this.fieldNextInspectionQuarter,
            this.fieldNextInspectionMonth,
            this.fieldNextInspectionWeek,
            this.fieldSurveyYear,
            this.fieldSurveyQuarter,
            this.fieldSurveyMonth,
            this.fieldSurveyWeek,
            this.fieldNotOnSurveyPoleCount,
            this.fieldReactive});
            pivotGridGroup1.Caption = "Last Insection Drill Down";
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionYear);
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionQuarter);
            pivotGridGroup1.Fields.Add(this.fieldLastInspectionMonth);
            pivotGridGroup1.Fields.Add(this.fielLastInspectionWeek);
            pivotGridGroup1.Hierarchy = null;
            pivotGridGroup1.ShowNewValues = true;
            pivotGridGroup2.Caption = "Next Inspection Drill Down";
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionYear);
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionQuarter);
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionMonth);
            pivotGridGroup2.Fields.Add(this.fieldNextInspectionWeek);
            pivotGridGroup2.Hierarchy = null;
            pivotGridGroup2.ShowNewValues = true;
            pivotGridGroup3.Caption = "Survey Drill Down";
            pivotGridGroup3.Fields.Add(this.fieldSurveyYear);
            pivotGridGroup3.Fields.Add(this.fieldSurveyQuarter);
            pivotGridGroup3.Fields.Add(this.fieldSurveyMonth);
            pivotGridGroup3.Fields.Add(this.fieldSurveyWeek);
            pivotGridGroup3.Hierarchy = null;
            pivotGridGroup3.ShowNewValues = true;
            this.pivotGridControl1.Groups.AddRange(new DevExpress.XtraPivotGrid.PivotGridGroup[] {
            pivotGridGroup1,
            pivotGridGroup2,
            pivotGridGroup3});
            this.pivotGridControl1.Location = new System.Drawing.Point(0, 0);
            this.pivotGridControl1.MenuManager = this.barManager1;
            this.pivotGridControl1.Name = "pivotGridControl1";
            this.pivotGridControl1.OptionsCustomization.CustomizationFormStyle = DevExpress.XtraPivotGrid.Customization.CustomizationFormStyle.Excel2007;
            this.pivotGridControl1.Size = new System.Drawing.Size(912, 388);
            this.pivotGridControl1.TabIndex = 0;
            this.pivotGridControl1.PopupMenuShowing += new DevExpress.XtraPivotGrid.PopupMenuShowingEventHandler(this.pivotGridControl1_PopupMenuShowing);
            this.pivotGridControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pivotGridControl1_MouseUp);
            // 
            // sp07321UTAnalysisPolesForAnalysisBindingSource
            // 
            this.sp07321UTAnalysisPolesForAnalysisBindingSource.DataMember = "sp07321_UT_Analysis_Poles_For_Analysis";
            this.sp07321UTAnalysisPolesForAnalysisBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // fieldPoleNumber1
            // 
            this.fieldPoleNumber1.AreaIndex = 8;
            this.fieldPoleNumber1.Caption = "Pole #";
            this.fieldPoleNumber1.FieldName = "PoleNumber";
            this.fieldPoleNumber1.Name = "fieldPoleNumber1";
            // 
            // fieldLastInspectionDate1
            // 
            this.fieldLastInspectionDate1.AreaIndex = 10;
            this.fieldLastInspectionDate1.Caption = "Last Inspection Date";
            this.fieldLastInspectionDate1.FieldName = "LastInspectionDate";
            this.fieldLastInspectionDate1.Name = "fieldLastInspectionDate1";
            this.fieldLastInspectionDate1.ValueFormat.FormatString = "d";
            this.fieldLastInspectionDate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldInspectionCycle1
            // 
            this.fieldInspectionCycle1.AreaIndex = 3;
            this.fieldInspectionCycle1.Caption = "Inspection Cycle";
            this.fieldInspectionCycle1.FieldName = "InspectionCycle";
            this.fieldInspectionCycle1.Name = "fieldInspectionCycle1";
            this.fieldInspectionCycle1.Visible = false;
            // 
            // fieldInspectionUnitDesc1
            // 
            this.fieldInspectionUnitDesc1.AreaIndex = 3;
            this.fieldInspectionUnitDesc1.Caption = "Inspection Unit Desc";
            this.fieldInspectionUnitDesc1.FieldName = "InspectionUnitDesc";
            this.fieldInspectionUnitDesc1.Name = "fieldInspectionUnitDesc1";
            this.fieldInspectionUnitDesc1.Visible = false;
            // 
            // fieldNextInspectionDate1
            // 
            this.fieldNextInspectionDate1.AreaIndex = 11;
            this.fieldNextInspectionDate1.Caption = "Next Inspection Date";
            this.fieldNextInspectionDate1.FieldName = "NextInspectionDate";
            this.fieldNextInspectionDate1.Name = "fieldNextInspectionDate1";
            this.fieldNextInspectionDate1.ValueFormat.FormatString = "d";
            this.fieldNextInspectionDate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldIsTransformer1
            // 
            this.fieldIsTransformer1.AreaIndex = 13;
            this.fieldIsTransformer1.Caption = "Is Transformer";
            this.fieldIsTransformer1.FieldName = "IsTransformer";
            this.fieldIsTransformer1.Name = "fieldIsTransformer1";
            // 
            // fieldTransformerNumber1
            // 
            this.fieldTransformerNumber1.AreaIndex = 14;
            this.fieldTransformerNumber1.Caption = "Transformer #";
            this.fieldTransformerNumber1.FieldName = "TransformerNumber";
            this.fieldTransformerNumber1.Name = "fieldTransformerNumber1";
            // 
            // fieldRemarks1
            // 
            this.fieldRemarks1.AreaIndex = 8;
            this.fieldRemarks1.Caption = "Remarks";
            this.fieldRemarks1.FieldName = "Remarks";
            this.fieldRemarks1.Name = "fieldRemarks1";
            this.fieldRemarks1.Visible = false;
            // 
            // fieldClientName1
            // 
            this.fieldClientName1.AreaIndex = 0;
            this.fieldClientName1.Caption = "Client Name";
            this.fieldClientName1.FieldName = "ClientName";
            this.fieldClientName1.Name = "fieldClientName1";
            // 
            // fieldClientCode1
            // 
            this.fieldClientCode1.AreaIndex = 9;
            this.fieldClientCode1.Caption = "Client Code";
            this.fieldClientCode1.FieldName = "ClientCode";
            this.fieldClientCode1.Name = "fieldClientCode1";
            this.fieldClientCode1.Visible = false;
            // 
            // fieldCircuitStatus1
            // 
            this.fieldCircuitStatus1.AreaIndex = 7;
            this.fieldCircuitStatus1.Caption = "Circuit Status";
            this.fieldCircuitStatus1.FieldName = "CircuitStatus";
            this.fieldCircuitStatus1.Name = "fieldCircuitStatus1";
            // 
            // fieldCircuitName1
            // 
            this.fieldCircuitName1.AreaIndex = 5;
            this.fieldCircuitName1.Caption = "Circuit Name";
            this.fieldCircuitName1.FieldName = "CircuitName";
            this.fieldCircuitName1.Name = "fieldCircuitName1";
            // 
            // fieldCircuitNumber1
            // 
            this.fieldCircuitNumber1.AreaIndex = 6;
            this.fieldCircuitNumber1.Caption = "Circuit #";
            this.fieldCircuitNumber1.FieldName = "CircuitNumber";
            this.fieldCircuitNumber1.Name = "fieldCircuitNumber1";
            // 
            // fieldVoltageType1
            // 
            this.fieldVoltageType1.AreaIndex = 15;
            this.fieldVoltageType1.Caption = "Voltage Type";
            this.fieldVoltageType1.FieldName = "VoltageType";
            this.fieldVoltageType1.Name = "fieldVoltageType1";
            // 
            // fieldFeederName1
            // 
            this.fieldFeederName1.AreaIndex = 4;
            this.fieldFeederName1.Caption = "Feeder Name";
            this.fieldFeederName1.FieldName = "FeederName";
            this.fieldFeederName1.Name = "fieldFeederName1";
            // 
            // fieldRegionName1
            // 
            this.fieldRegionName1.AreaIndex = 1;
            this.fieldRegionName1.Caption = "Region Name";
            this.fieldRegionName1.FieldName = "RegionName";
            this.fieldRegionName1.Name = "fieldRegionName1";
            // 
            // fieldStatus1
            // 
            this.fieldStatus1.AreaIndex = 9;
            this.fieldStatus1.Caption = "Pole Status";
            this.fieldStatus1.FieldName = "Status";
            this.fieldStatus1.Name = "fieldStatus1";
            // 
            // fieldPrimaryName1
            // 
            this.fieldPrimaryName1.AreaIndex = 3;
            this.fieldPrimaryName1.Caption = "Primary Name";
            this.fieldPrimaryName1.FieldName = "PrimaryName";
            this.fieldPrimaryName1.Name = "fieldPrimaryName1";
            // 
            // fieldSubAreaName1
            // 
            this.fieldSubAreaName1.AreaIndex = 2;
            this.fieldSubAreaName1.Caption = "Sub Area Name";
            this.fieldSubAreaName1.FieldName = "SubAreaName";
            this.fieldSubAreaName1.Name = "fieldSubAreaName1";
            // 
            // fieldLastInspectionElapsedDays1
            // 
            this.fieldLastInspectionElapsedDays1.AreaIndex = 12;
            this.fieldLastInspectionElapsedDays1.Caption = "Last Inspection Elapsed Days";
            this.fieldLastInspectionElapsedDays1.FieldName = "LastInspectionElapsedDays";
            this.fieldLastInspectionElapsedDays1.Name = "fieldLastInspectionElapsedDays1";
            // 
            // fieldMapID1
            // 
            this.fieldMapID1.AreaIndex = 21;
            this.fieldMapID1.Caption = "Map ID";
            this.fieldMapID1.FieldName = "MapID";
            this.fieldMapID1.Name = "fieldMapID1";
            this.fieldMapID1.Visible = false;
            // 
            // fieldOnSurvey1
            // 
            this.fieldOnSurvey1.AreaIndex = 16;
            this.fieldOnSurvey1.Caption = "On Survey";
            this.fieldOnSurvey1.FieldName = "OnSurvey";
            this.fieldOnSurvey1.Name = "fieldOnSurvey1";
            // 
            // fieldSurveySpanClear1
            // 
            this.fieldSurveySpanClear1.AreaIndex = 17;
            this.fieldSurveySpanClear1.Caption = "Survey Span Clear";
            this.fieldSurveySpanClear1.FieldName = "SurveySpanClear";
            this.fieldSurveySpanClear1.Name = "fieldSurveySpanClear1";
            // 
            // fieldSurveyorName1
            // 
            this.fieldSurveyorName1.AreaIndex = 18;
            this.fieldSurveyorName1.Caption = "Surveyor Name";
            this.fieldSurveyorName1.FieldName = "SurveyorName";
            this.fieldSurveyorName1.Name = "fieldSurveyorName1";
            // 
            // fieldSurveyDate1
            // 
            this.fieldSurveyDate1.AreaIndex = 19;
            this.fieldSurveyDate1.Caption = "Survey Date";
            this.fieldSurveyDate1.FieldName = "SurveyDate";
            this.fieldSurveyDate1.Name = "fieldSurveyDate1";
            this.fieldSurveyDate1.ValueFormat.FormatString = "d";
            this.fieldSurveyDate1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // fieldInfestationRate1
            // 
            this.fieldInfestationRate1.AreaIndex = 22;
            this.fieldInfestationRate1.Caption = "Infestation Rate";
            this.fieldInfestationRate1.FieldName = "InfestationRate";
            this.fieldInfestationRate1.Name = "fieldInfestationRate1";
            // 
            // fieldIsShutdownRequired1
            // 
            this.fieldIsShutdownRequired1.AreaIndex = 23;
            this.fieldIsShutdownRequired1.Caption = "Shutdown Required";
            this.fieldIsShutdownRequired1.FieldName = "IsShutdownRequired";
            this.fieldIsShutdownRequired1.Name = "fieldIsShutdownRequired1";
            // 
            // fieldHotGloveRequired1
            // 
            this.fieldHotGloveRequired1.AreaIndex = 24;
            this.fieldHotGloveRequired1.Caption = "Hot Glove Required";
            this.fieldHotGloveRequired1.FieldName = "HotGloveRequired";
            this.fieldHotGloveRequired1.Name = "fieldHotGloveRequired1";
            // 
            // fieldLinesmanPossible1
            // 
            this.fieldLinesmanPossible1.AreaIndex = 25;
            this.fieldLinesmanPossible1.Caption = "Linesman Possible";
            this.fieldLinesmanPossible1.FieldName = "LinesmanPossible";
            this.fieldLinesmanPossible1.Name = "fieldLinesmanPossible1";
            // 
            // fieldClearanceDistance1
            // 
            this.fieldClearanceDistance1.AreaIndex = 26;
            this.fieldClearanceDistance1.Caption = "Clearance Distance";
            this.fieldClearanceDistance1.FieldName = "ClearanceDistance";
            this.fieldClearanceDistance1.Name = "fieldClearanceDistance1";
            // 
            // fieldTrafficManagementRequired1
            // 
            this.fieldTrafficManagementRequired1.AreaIndex = 27;
            this.fieldTrafficManagementRequired1.Caption = "Traffic Management Required";
            this.fieldTrafficManagementRequired1.FieldName = "TrafficManagementRequired";
            this.fieldTrafficManagementRequired1.Name = "fieldTrafficManagementRequired1";
            // 
            // fieldTrafficManagementResolved1
            // 
            this.fieldTrafficManagementResolved1.AreaIndex = 28;
            this.fieldTrafficManagementResolved1.Caption = "Traffic Management Resolved";
            this.fieldTrafficManagementResolved1.FieldName = "TrafficManagementResolved";
            this.fieldTrafficManagementResolved1.Name = "fieldTrafficManagementResolved1";
            // 
            // fieldFiveYearClearanceAchieved1
            // 
            this.fieldFiveYearClearanceAchieved1.AreaIndex = 29;
            this.fieldFiveYearClearanceAchieved1.Caption = "Five Year Clearance Achieved";
            this.fieldFiveYearClearanceAchieved1.FieldName = "FiveYearClearanceAchieved";
            this.fieldFiveYearClearanceAchieved1.Name = "fieldFiveYearClearanceAchieved1";
            // 
            // fieldTreeWithin3Meters1
            // 
            this.fieldTreeWithin3Meters1.AreaIndex = 30;
            this.fieldTreeWithin3Meters1.Caption = "Tree Within 3 M";
            this.fieldTreeWithin3Meters1.FieldName = "TreeWithin3Meters";
            this.fieldTreeWithin3Meters1.Name = "fieldTreeWithin3Meters1";
            // 
            // fieldTreeClimbable1
            // 
            this.fieldTreeClimbable1.AreaIndex = 31;
            this.fieldTreeClimbable1.Caption = "Tree Climbable";
            this.fieldTreeClimbable1.FieldName = "TreeClimbable";
            this.fieldTreeClimbable1.Name = "fieldTreeClimbable1";
            // 
            // fieldIsBaseClimbable1
            // 
            this.fieldIsBaseClimbable1.AreaIndex = 32;
            this.fieldIsBaseClimbable1.Caption = "Base Climbable";
            this.fieldIsBaseClimbable1.FieldName = "IsBaseClimbable";
            this.fieldIsBaseClimbable1.Name = "fieldIsBaseClimbable1";
            // 
            // fieldSurveyStatus1
            // 
            this.fieldSurveyStatus1.AreaIndex = 20;
            this.fieldSurveyStatus1.Caption = "Survey Status";
            this.fieldSurveyStatus1.FieldName = "SurveyStatus";
            this.fieldSurveyStatus1.Name = "fieldSurveyStatus1";
            // 
            // fieldEstimatedHours
            // 
            this.fieldEstimatedHours.AreaIndex = 33;
            this.fieldEstimatedHours.Caption = "Estimated Hours";
            this.fieldEstimatedHours.CellFormat.FormatString = "######0.00 Hours";
            this.fieldEstimatedHours.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldEstimatedHours.FieldName = "EstimatedHours";
            this.fieldEstimatedHours.Name = "fieldEstimatedHours";
            this.fieldEstimatedHours.ValueFormat.FormatString = "######0.00 Hours";
            this.fieldEstimatedHours.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldActualHours
            // 
            this.fieldActualHours.AreaIndex = 34;
            this.fieldActualHours.Caption = "Actual Hours";
            this.fieldActualHours.CellFormat.FormatString = "######0.00 Hours";
            this.fieldActualHours.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldActualHours.FieldName = "ActualHours";
            this.fieldActualHours.Name = "fieldActualHours";
            this.fieldActualHours.ValueFormat.FormatString = "######0.00 Hours";
            this.fieldActualHours.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldIncomeValue1
            // 
            this.fieldIncomeValue1.AreaIndex = 35;
            this.fieldIncomeValue1.Caption = "Income Value";
            this.fieldIncomeValue1.CellFormat.FormatString = "c";
            this.fieldIncomeValue1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldIncomeValue1.FieldName = "IncomeValue";
            this.fieldIncomeValue1.Name = "fieldIncomeValue1";
            this.fieldIncomeValue1.ValueFormat.FormatString = "c";
            this.fieldIncomeValue1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            // 
            // fieldG55Category1
            // 
            this.fieldG55Category1.AreaIndex = 36;
            this.fieldG55Category1.Caption = "G55Category";
            this.fieldG55Category1.FieldName = "G55Category";
            this.fieldG55Category1.Name = "fieldG55Category1";
            // 
            // fieldPoleCount1
            // 
            this.fieldPoleCount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldPoleCount1.AreaIndex = 0;
            this.fieldPoleCount1.Caption = "Pole Count";
            this.fieldPoleCount1.FieldName = "PoleCount";
            this.fieldPoleCount1.Name = "fieldPoleCount1";
            // 
            // fieldOnSurveyPoleCount1
            // 
            this.fieldOnSurveyPoleCount1.AreaIndex = 37;
            this.fieldOnSurveyPoleCount1.Caption = "On Survey Pole Count";
            this.fieldOnSurveyPoleCount1.FieldName = "OnSurveyPoleCount";
            this.fieldOnSurveyPoleCount1.Name = "fieldOnSurveyPoleCount1";
            // 
            // fieldActionCountTotal1
            // 
            this.fieldActionCountTotal1.AreaIndex = 39;
            this.fieldActionCountTotal1.Caption = "Action Count Total";
            this.fieldActionCountTotal1.FieldName = "ActionCountTotal";
            this.fieldActionCountTotal1.Name = "fieldActionCountTotal1";
            // 
            // fieldActionCountToBeStarted1
            // 
            this.fieldActionCountToBeStarted1.AreaIndex = 40;
            this.fieldActionCountToBeStarted1.Caption = "Action Count To Be Started";
            this.fieldActionCountToBeStarted1.FieldName = "ActionCountToBeStarted";
            this.fieldActionCountToBeStarted1.Name = "fieldActionCountToBeStarted1";
            // 
            // fieldActionCountStarted1
            // 
            this.fieldActionCountStarted1.AreaIndex = 41;
            this.fieldActionCountStarted1.Caption = "Action Count Started";
            this.fieldActionCountStarted1.FieldName = "ActionCountStarted";
            this.fieldActionCountStarted1.Name = "fieldActionCountStarted1";
            // 
            // fieldActionCountOnHold1
            // 
            this.fieldActionCountOnHold1.AreaIndex = 42;
            this.fieldActionCountOnHold1.Caption = "Action Count On-Hold";
            this.fieldActionCountOnHold1.FieldName = "ActionCountOnHold";
            this.fieldActionCountOnHold1.Name = "fieldActionCountOnHold1";
            // 
            // fieldActionCountCompleted1
            // 
            this.fieldActionCountCompleted1.AreaIndex = 43;
            this.fieldActionCountCompleted1.Caption = "Action Count Completed";
            this.fieldActionCountCompleted1.FieldName = "ActionCountCompleted";
            this.fieldActionCountCompleted1.Name = "fieldActionCountCompleted1";
            // 
            // fieldNotOnSurveyPoleCount
            // 
            this.fieldNotOnSurveyPoleCount.AreaIndex = 38;
            this.fieldNotOnSurveyPoleCount.Caption = "Not On Survey Pole Count";
            this.fieldNotOnSurveyPoleCount.FieldName = "NotOnSurveyPoleCount";
            this.fieldNotOnSurveyPoleCount.Name = "fieldNotOnSurveyPoleCount";
            // 
            // fieldReactive
            // 
            this.fieldReactive.AreaIndex = 21;
            this.fieldReactive.Caption = "Reactive";
            this.fieldReactive.FieldName = "Reactive";
            this.fieldReactive.Name = "fieldReactive";
            // 
            // chartControl1
            // 
            this.chartControl1.DataSource = this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chartControl1.EmptyChartText.Text = "No Data To Graph - Try Selecting Data from the Analysis Grid.";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.Size = new System.Drawing.Size(912, 100);
            this.chartControl1.TabIndex = 1;
            // 
            // sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter
            // 
            this.sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter.ClearBeforeFill = true;
            // 
            // sp07320_UT_Analysis_Poles_Available_PolesTableAdapter
            // 
            this.sp07320_UT_Analysis_Poles_Available_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiToggleAvailableColumnsVisibility, true)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Copy to Clipboard";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Copy the selected cells to the Clipboard for pasting to external application.";
            this.barButtonItem1.Id = 25;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiToggleAvailableColumnsVisibility
            // 
            this.bbiToggleAvailableColumnsVisibility.Caption = "Toggle Available Columns Visibility";
            this.bbiToggleAvailableColumnsVisibility.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiToggleAvailableColumnsVisibility.Glyph")));
            this.bbiToggleAvailableColumnsVisibility.Id = 32;
            this.bbiToggleAvailableColumnsVisibility.Name = "bbiToggleAvailableColumnsVisibility";
            this.bbiToggleAvailableColumnsVisibility.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiToggleAvailableColumnsVisibility_ItemClick);
            // 
            // pmChart
            // 
            this.pmChart.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRotateAxis),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiChartWizard, true)});
            this.pmChart.Manager = this.barManager1;
            this.pmChart.MenuCaption = "Chart Menu";
            this.pmChart.Name = "pmChart";
            // 
            // bbiRotateAxis
            // 
            this.bbiRotateAxis.Caption = "Rotate Axis";
            this.bbiRotateAxis.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRotateAxis.Glyph")));
            this.bbiRotateAxis.Hint = "Rotate Chart Axis";
            this.bbiRotateAxis.Id = 27;
            this.bbiRotateAxis.Name = "bbiRotateAxis";
            this.bbiRotateAxis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRotateAxis_ItemClick);
            // 
            // bbiChartWizard
            // 
            this.bbiChartWizard.Caption = "Chart Wizard";
            this.bbiChartWizard.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiChartWizard.Glyph")));
            this.bbiChartWizard.Hint = "Open Chart Wizard";
            this.bbiChartWizard.Id = 26;
            this.bbiChartWizard.Name = "bbiChartWizard";
            this.bbiChartWizard.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiChartWizard_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(580, 206);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonEditFilterCircuits),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiReloadData)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // barEditItemDateFilter
            // 
            this.barEditItemDateFilter.Caption = "Date Filter";
            this.barEditItemDateFilter.Edit = this.repositoryItemPopupContainerEditDateFilter;
            this.barEditItemDateFilter.EditValue = "No Date Filter";
            this.barEditItemDateFilter.EditWidth = 120;
            this.barEditItemDateFilter.Id = 29;
            this.barEditItemDateFilter.Name = "barEditItemDateFilter";
            // 
            // repositoryItemPopupContainerEditDateFilter
            // 
            this.repositoryItemPopupContainerEditDateFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateFilter.Name = "repositoryItemPopupContainerEditDateFilter";
            this.repositoryItemPopupContainerEditDateFilter.PopupControl = this.popupContainerControlDateFilter;
            this.repositoryItemPopupContainerEditDateFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditDateFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateFilter_QueryResultValue);
            // 
            // buttonEditFilterCircuits
            // 
            this.buttonEditFilterCircuits.Caption = "Circuit Filter";
            this.buttonEditFilterCircuits.Edit = this.repositoryItemButtonEditFilterCircuits;
            this.buttonEditFilterCircuits.EditValue = "No Circuit Filter";
            this.buttonEditFilterCircuits.EditWidth = 151;
            this.buttonEditFilterCircuits.Id = 34;
            this.buttonEditFilterCircuits.Name = "buttonEditFilterCircuits";
            // 
            // repositoryItemButtonEditFilterCircuits
            // 
            this.repositoryItemButtonEditFilterCircuits.AutoHeight = false;
            this.repositoryItemButtonEditFilterCircuits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "choose", null, true)});
            this.repositoryItemButtonEditFilterCircuits.Name = "repositoryItemButtonEditFilterCircuits";
            this.repositoryItemButtonEditFilterCircuits.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditFilterCircuits.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFilterCircuits_ButtonClick);
            // 
            // bbiReloadData
            // 
            this.bbiReloadData.Caption = "Reload";
            this.bbiReloadData.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiReloadData.Glyph")));
            this.bbiReloadData.Id = 30;
            this.bbiReloadData.Name = "bbiReloadData";
            this.bbiReloadData.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Reload Data - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Data Supply List using the data supplied in the Filter.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiReloadData.SuperTip = superToolTip3;
            this.bbiReloadData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiReloadData_ItemClick);
            // 
            // bbiAnalyse
            // 
            this.bbiAnalyse.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiAnalyse.Caption = "Analyse";
            this.bbiAnalyse.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAnalyse.Glyph")));
            this.bbiAnalyse.Id = 31;
            this.bbiAnalyse.Name = "bbiAnalyse";
            this.bbiAnalyse.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Text = "Analyse - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the Analysis Grid and Chart with the records selected in the Dat" +
    "a Supply list.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiAnalyse.SuperTip = superToolTip4;
            // 
            // barEditItemStatusFilter
            // 
            this.barEditItemStatusFilter.Caption = "Status Filter";
            this.barEditItemStatusFilter.Edit = this.repositoryItemPopupContainerEditStatusFilter;
            this.barEditItemStatusFilter.EditValue = "All Statuses";
            this.barEditItemStatusFilter.EditWidth = 83;
            this.barEditItemStatusFilter.Id = 28;
            this.barEditItemStatusFilter.Name = "barEditItemStatusFilter";
            // 
            // repositoryItemPopupContainerEditStatusFilter
            // 
            this.repositoryItemPopupContainerEditStatusFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditStatusFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStatusFilter.Name = "repositoryItemPopupContainerEditStatusFilter";
            this.repositoryItemPopupContainerEditStatusFilter.PopupSizeable = false;
            this.repositoryItemPopupContainerEditStatusFilter.ShowPopupCloseButton = false;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemTextEdit2;
            this.barEditItem1.Id = 33;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Analysis_Poles
            // 
            this.ClientSize = new System.Drawing.Size(1293, 494);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dockPanel1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Analysis_Poles";
            this.Text = "Utilities - Pole Analysis";
            this.Activated += new System.EventHandler(this.frm_UT_Analysis_Poles_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_UT_Analysis_Poles_FormClosed);
            this.Load += new System.EventHandler(this.frm_UT_Analysis_Poles_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07320UTAnalysisPolesAvailablePolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateFilter)).EndInit();
            this.popupContainerControlDateFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07321UTAnalysisPolesForAnalysisBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStatusFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT_Reports dataSet_AT_Reports;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiChartWizard;
        private DevExpress.XtraBars.PopupMenu pmChart;
        private DevExpress.XtraBars.BarButtonItem bbiRotateAxis;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemStatusFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStatusFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateFilter;
        private DevExpress.XtraBars.BarButtonItem bbiReloadData;
        private DevExpress.XtraBars.BarButtonItem bbiAnalyse;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit deFromDate;
        private DevExpress.XtraEditors.DateEdit deToDate;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateFilter;
        private DevExpress.XtraEditors.SimpleButton btnDateFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraBars.BarButtonItem bbiToggleAvailableColumnsVisibility;
        private DevExpress.XtraBars.BarEditItem buttonEditFilterCircuits;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditFilterCircuits;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.SimpleButton btnAnalyse;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedCount;
        private DataSet_UT_ReportingTableAdapters.sp07320_UT_Analysis_Poles_Available_PolesTableAdapter sp07320_UT_Analysis_Poles_Available_PolesTableAdapter;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private System.Windows.Forms.BindingSource sp07320UTAnalysisPolesAvailablePolesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionElapsedDays;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colOnSurvey;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveySpanClear;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyorName;
        private System.Windows.Forms.BindingSource sp07321UTAnalysisPolesForAnalysisBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter sp07321_UT_Analysis_Poles_For_AnalysisTableAdapter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionCycle1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInspectionUnitDesc1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsTransformer1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTransformerNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRemarks1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClientCode1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldCircuitNumber1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldVoltageType1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFeederName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldRegionName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPrimaryName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSubAreaName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionElapsedDays1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldMapID1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOnSurvey1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveySpanClear1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyorName1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyDate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldInfestationRate1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsShutdownRequired1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldHotGloveRequired1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLinesmanPossible1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldClearanceDistance1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTrafficManagementRequired1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTrafficManagementResolved1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldFiveYearClearanceAchieved1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeWithin3Meters1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTreeClimbable1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIsBaseClimbable1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyStatus1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldEstimatedHours;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActualHours;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIncomeValue1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldG55Category1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldPoleCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldOnSurveyPoleCount1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCountTotal1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCountToBeStarted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCountStarted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCountOnHold1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldActionCountCompleted1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldLastInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fielLastInspectionWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNextInspectionWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyYear;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyQuarter;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyMonth;
        private DevExpress.XtraPivotGrid.PivotGridField fieldSurveyWeek;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNotOnSurveyPoleCount;
        private DevExpress.XtraPivotGrid.PivotGridField fieldReactive;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

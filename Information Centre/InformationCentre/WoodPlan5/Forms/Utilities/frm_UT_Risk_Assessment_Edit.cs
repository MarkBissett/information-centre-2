using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;

using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors.Repository;  // Required by Hyperlink Repository Items //
using DevExpress.XtraReports.UserDesigner;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Risk_Assessment_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int _PassedInWorkOrderID = 0;
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //

        private string strSignaturePath = "";
        rpt_AT_Report_Layout_Risk_Assessment_Site_Specific rptReport;
        public string _ReportLayoutFolder = "";
        WaitDialogForm loadingForm = null;

        #endregion

        public frm_UT_Risk_Assessment_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Risk_Assessment_Edit_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet_UT.sp07417_UT_Risk_Assessment_Question_Answers_List' table. You can move, or remove it, as needed.
            this.sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter.Fill(this.dataSet_UT.sp07417_UT_Risk_Assessment_Question_Answers_List);
            this.FormID = 500062;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            emptyEditor = new RepositoryItem();

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Signature Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _ReportLayoutFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "UtilitiesSavedReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Risk Assessment Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Risk Assessment Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!_ReportLayoutFolder.EndsWith("\\")) _ReportLayoutFolder += "\\";  // Add Backslash to end //
    
            sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07417_UT_Risk_Assessment_Question_Answers_ListTableAdapter.Fill(dataSet_UT.sp07417_UT_Risk_Assessment_Question_Answers_List);

            sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07264_UT_Electrical_Risk_Categories_With_BlankTableAdapter.Fill(dataSet_UT.sp07264_UT_Electrical_Risk_Categories_With_Blank);

            sp07263_UT_Type_Of_Work_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07263_UT_Type_Of_Work_With_BlankTableAdapter.Fill(dataSet_UT.sp07263_UT_Type_Of_Work_With_Blank);

            sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "RiskQuestionID");

            sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "SubContractorSignatureID");
            
            // Populate Main Dataset //
            sp07256_UT_Risk_Assessment_Question_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow = this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["DateCreated"] = DateTime.Now;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedBy"] = (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : GlobalSettings.UserSurname) + (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : ": ") + (string.IsNullOrEmpty(GlobalSettings.UserForename) ? "" : GlobalSettings.UserForename);
                        drNewRow["SubContractorID"] = 0;
                        drNewRow["PaddedWorkOrderID"] = "Set on First Save";
                        this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow = this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows.Add(drNewRow);
                        this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07256_UT_Risk_Assessment_Question_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                   break;
            }
            if (strFormMode == "view") iBool_AllowDelete = false;
                                                      
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        PermitHolderNameTextEdit.Focus();

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1"; // +intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        PermitHolderNameTextEdit.Focus();

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        PermitHolderNameTextEdit.Focus();

                        barButtonItemPrint.Enabled = false;
                        gridControl1.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1"; // +intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }

            DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
            if (currentRow != null)
            {
                int intRiskAssessmentTypeID = (string.IsNullOrEmpty(currentRow["RiskAssessmentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RiskAssessmentTypeID"]));
                GridView view = (GridView)gridControl1.MainView;
                view.Columns["Answer"].Visible = (intRiskAssessmentTypeID == 0 ? false : true);
                view.Columns["AnswerText1"].Visible = (intRiskAssessmentTypeID == 0 ? true : false);
                view.Columns["AnswerText2"].Visible = (intRiskAssessmentTypeID == 0 ? true : false);
                view.Columns["AnswerText3"].Visible = (intRiskAssessmentTypeID == 0 ? true : false);

                view.Columns["Answer"].OptionsColumn.ReadOnly = (intRiskAssessmentTypeID == 0 ? true : false);
                view.Columns["AnswerText1"].OptionsColumn.ReadOnly = (intRiskAssessmentTypeID == 0 ? false : true);
                view.Columns["AnswerText2"].OptionsColumn.ReadOnly = (intRiskAssessmentTypeID == 0 ? false : true);
                view.Columns["AnswerText3"].OptionsColumn.ReadOnly = (intRiskAssessmentTypeID == 0 ? false : true);

                try
                {
                    ItemForSiteAddress.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForMEWPArialRescuers.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForEmergencyKitLocation.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForAirAmbulanceLat.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForAirAmbulanceLong.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForBanksMan.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForEmergencyServicesMeetPoint.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForMobilePhoneSignalBars.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForNearestAandE.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForNearestLandLine.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);

                    layoutControlGroupSiteSpecific.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    //layoutControlGroupTeamSignatures.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    layoutControlGroupNearMisses.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);

                    ItemForPermissionChecked.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForElectricalAssessmentVerified.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForElectricalRiskCategoryID.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForTypeOfWorkingID.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForPermitHolderName.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);
                    ItemForNRSWAOpeningNoticeNo.Visibility = (intRiskAssessmentTypeID == 0 ? DevExpress.XtraLayout.Utils.LayoutVisibility.Always : DevExpress.XtraLayout.Utils.LayoutVisibility.Never);

                    //barButtonItemPrint.Enabled = (intRiskAssessmentTypeID == 0 ? true : false);
                }
                catch (Exception) { }
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            repositoryItemButtonEditSignatureCapture.ReadOnly = strFormMode == "view";

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                //barButtonItemPrint.Enabled = false;
                gridControl1.Enabled = false;
                gridControl3.Enabled = false;
            }
            SetMenuStatus();  // Just in case any default layout has permissions set wrongly //
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource.EndEdit();
                dsChanges = this.dataSet_UT_Edit.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                    bbiSave.Enabled = false;
                    bbiFormSave.Enabled = false;
                    return false;
                }
            }
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)  // Questions //
            {
                if (iBool_AllowDelete && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Signatures //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && _PassedInWorkOrderID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (strFormMode != "view")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (_PassedInWorkOrderID > 0 ? true : false);
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
             }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }


        private void frm_UT_Risk_Assessment_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                LoadLinkedQuestions();
                LoadLinkedSignatures();
            }
            SetMenuStatus();
        }

        private void frm_UT_Risk_Assessment_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            // Commit any outstanding changes within the grid control //
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            // Save Work Order Header //
            this.sp07256UTRiskAssessmentQuestionItemBindingSource.EndEdit();
            try
            {
                this.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Actions //
            this.sp07258UTSurveyedPoleRiskAssessmentQuestionsEditBindingSource.EndEdit();
            try
            {
                this.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked action changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["RiskAssessmentID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Pole2")
                    {
                        var fParentForm = (frm_UT_Survey_Pole2)frmChild;
                        fParentForm.UpdateFormRefreshStatus(22, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", strNewIDs);
                    }
                    if (frmChild.Name == "frm_UT_Team_WorkOrder_Manager")
                    {
                        var fParentForm = (frm_UT_Team_WorkOrder_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(22, "", "", "", "", "", strNewIDs, "");
                    }
                    if (frmChild.Name == "frm_UT_Team_WorkOrder_Edit")
                    {
                        var fParentForm = (frm_UT_Team_WorkOrder_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(10, "", strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intLinkedRecordNew = 0;
            int intLinkedRecordModified = 0;
            int intLinkedRecordDeleted = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07256_UT_Risk_Assessment_Question_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }


            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_Edit.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedRecordNew > 0 || intLinkedRecordModified > 0 || intLinkedRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Work Order record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Work Order record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Work Order record(s)\n";
                if (intLinkedRecordNew > 0) strMessage += Convert.ToString(intLinkedRecordNew) + " New Linked Action record(s)\n";
                if (intLinkedRecordModified > 0) strMessage += Convert.ToString(intLinkedRecordModified) + " Updated Linked Action record(s)\n";
                if (intLinkedRecordDeleted > 0) strMessage += Convert.ToString(intLinkedRecordDeleted) + " Deleted Linked Action record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRiskAssessmentID = 0;
            int intRiskAssessmentTypeID = 0;
            DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
            if (currentRow != null)
            {
                intRiskAssessmentID = (currentRow["RiskAssessmentID"] == null || string.IsNullOrEmpty(currentRow["RiskAssessmentID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RiskAssessmentID"]));
                intRiskAssessmentTypeID = (currentRow["RiskAssessmentTypeID"] == null || string.IsNullOrEmpty(currentRow["RiskAssessmentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RiskAssessmentTypeID"]));
            }
            // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before printing the Risk Assessment!\n\nSave Changes?", "Print Risk Assessment", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (!boolProceed) return;

            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }
            string strReportFileName = (intRiskAssessmentTypeID == 0 ? "Risk_Assessment_Site_Specific_Layout.repx" : "Risk_Assessment_G552_Layout.repx");
            rpt_AT_Report_Layout_Risk_Assessment_Site_Specific rptReport = new rpt_AT_Report_Layout_Risk_Assessment_Site_Specific(this.GlobalSettings, intRiskAssessmentID, strSignaturePath);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                rptReport.ShowRibbonPreview();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();

        }

        private void bbiEditReportLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
             if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                int intRiskAssessmentID = 0;
                int intRiskAssessmentTypeID = 0;
                DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
                if (currentRow != null)
                {
                    intRiskAssessmentID = (currentRow["RiskAssessmentID"] == null || string.IsNullOrEmpty(currentRow["RiskAssessmentID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RiskAssessmentID"]));
                    intRiskAssessmentTypeID = (currentRow["RiskAssessmentTypeID"] == null || string.IsNullOrEmpty(currentRow["RiskAssessmentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RiskAssessmentTypeID"]));
                }
                string strReportFileName = (intRiskAssessmentTypeID == 0 ? "Risk_Assessment_Site_Specific_Layout.repx" : "Risk_Assessment_G552_Layout.repx");

                rpt_AT_Report_Layout_Risk_Assessment_Site_Specific rptReport = null;
                rptReport = new rpt_AT_Report_Layout_Risk_Assessment_Site_Specific(this.GlobalSettings, intRiskAssessmentID, strSignaturePath);
                try
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (!splashScreenManager.IsSplashFormVisible)
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    this.splashScreenManager = splashScreenManager1;
                    this.splashScreenManager.ShowWaitForm();
                }

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, _ReportLayoutFolder, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void LoadLinkedQuestions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            int intRiskAssessmentID = 0;
            DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
            if (currentRow != null)
            {
                intRiskAssessmentID = (currentRow["RiskAssessmentID"] == null ? 0 : Convert.ToInt32(currentRow["RiskAssessmentID"]));
             }

            // Questions //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter.Fill(this.dataSet_UT_Edit.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_Edit, intRiskAssessmentID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskQuestionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            // Team Signatures //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl3.BeginUpdate();
            sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter.Fill(this.dataSet_UT_Edit.sp07265_UT_Risk_Assessment_Team_Signatures_List, intRiskAssessmentID.ToString() + ",");
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskQuestionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

        }

        private void LoadLinkedSignatures()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            int intRiskAssessmentID = 0;
            DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
            if (currentRow != null)
            {
                intRiskAssessmentID = (currentRow["RiskAssessmentID"] == null ? 0 : Convert.ToInt32(currentRow["RiskAssessmentID"]));
            }

            // Team Signatures //
            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl3.BeginUpdate();
            sp07265_UT_Risk_Assessment_Team_Signatures_ListTableAdapter.Fill(this.dataSet_UT_Edit.sp07265_UT_Risk_Assessment_Team_Signatures_List, intRiskAssessmentID.ToString() + ",");
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskQuestionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }

        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Questions Linked";
                    break;
                case "gridView3":
                    message = "No Team Member Signatures Linked";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridView1

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }


        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditAnswer_EditValueChanging(object sender, ChangingEventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        #endregion

        #endregion


        #region GridView3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SignatureFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SignatureFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView3;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditSignature_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colSignatureFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "SignatureFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strSignaturePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    System.Diagnostics.Process.Start(strFilePath);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void repositoryItemButtonEditSignatureCapture_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTeamMemberName = "";
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            strTeamMemberName = view.GetFocusedRowCellValue("TeamMemberName").ToString();
            frm_UT_Signature_Capture fChildForm = new frm_UT_Signature_Capture();
            fChildForm.strLoadedSignatureName = strTeamMemberName;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            Image returnedImage = fChildForm._returnedImage;
            if (returnedImage == null) return;
            
            // Save Signature File //
            int intSubContractorSignatureID = Convert.ToInt32(view.GetFocusedRowCellValue("SubContractorSignatureID"));
            try
            {
                string strSaveFileName = strSignaturePath;
                if (!strSaveFileName.EndsWith("\\")) strSaveFileName += "\\";
                strSaveFileName += "Signature_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + intSubContractorSignatureID.ToString() + ".jpg";
                returnedImage.Save(strSaveFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

                strSaveFileName = "Signature_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "__" + intSubContractorSignatureID.ToString() + ".jpg";  // Change to filename without path //
                DataSet_UT_EditTableAdapters.QueriesTableAdapter UpdateRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                UpdateRecord.ChangeConnectionString(strConnectionString);
                UpdateRecord.sp07313_UT_Risk_Assessment_Store_Team_Members_Signature(intSubContractorSignatureID, strSaveFileName);
                LoadLinkedSignatures();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurrred while saving the signature... [" + ex.Message + "].\r\n\r\nTry Saving again. If the problem persists contact Technical Support.", "Save Team Member Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Store Linked Team Member Signature //
            try
            {
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to save the Team Member Signature [" + ex.Message + "]. Try again. If the problem persists contact technical support.", "Save Team Member Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
       }


        #endregion


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveFirst();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveFirst();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveFirst();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //
                                        bs.MovePrevious();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MovePrevious();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MovePrevious();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveNext();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveNext();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveNext();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveLast();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveLast();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveLast();
                        }
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Linked Records... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadLinkedQuestions();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();
                        LoadLinkedSignatures();
                        view = (GridView)gridControl3.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors



        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        public void Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Team Member Signatures //
                    {
                        int intRiskAssessmentID = 0;
                        DataRowView currentRow = (DataRowView)sp07256UTRiskAssessmentQuestionItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            intRiskAssessmentID = (currentRow["RiskAssessmentID"] == null ? 0 : Convert.ToInt32(currentRow["RiskAssessmentID"]));
                        }
                        string strTeamMemberIDs = "";
                        frm_UT_Risk_Assessment_Block_Add_Team_Members fChildForm = new frm_UT_Risk_Assessment_Block_Add_Team_Members();
                        fChildForm.PassedInWorkOrderID = _PassedInWorkOrderID;
                        fChildForm.PassedInRiskAssessmentID = intRiskAssessmentID;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        switch (fChildForm.ShowDialog())
                        {
                            case DialogResult.OK:
                                strTeamMemberIDs = fChildForm.strSelectedIDs;
                                break;
                            case DialogResult.Cancel:
                                return;
                        }
                        if (string.IsNullOrEmpty(strTeamMemberIDs)) return;

                        // Add Team Members //
                        string strNewIDs = "";
                        try
                        {
                            DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                            AddRecord.ChangeConnectionString(strConnectionString);
                            strNewIDs = AddRecord.sp07312_UT_Risk_Assessment_Add_Team_Members(intRiskAssessmentID, strTeamMemberIDs).ToString(); ;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to add the Risk Assessment Team Member Signatures [" + ex.Message + "]. Try again. If the problem persists contact technical support.", "Add Risk Assessment Team Member Signatures", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        i_str_AddedRecordIDs3 = strNewIDs;
                        LoadLinkedSignatures();
                    }
                    break;
            }
        }


        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Questions //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Questions to remove from the Risk Assessment by clicking on them then try again.", "Remove Linked Questions from Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Question" : Convert.ToString(intRowHandles.Length) + " Linked Questions") + " selected for removing from the current Risk Assessment!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Question" : "these Linked Questions") + " will be removed from the Risk Assessment.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for deleting from Risk Assessment.\n\nIMPORTANT NOTE: You must save changes to this risk assessment before the marked questions records are physically removed from the Risk Assessment.", "Remove Linked Questions from Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:     // Team Member Signatures //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Team Member Signatures to remove from the Risk Assessment by clicking on them then try again.", "Remove Linked Team Member Signatures from Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Team Member Signature" : Convert.ToString(intRowHandles.Length) + " Linked Team Member Signatures") + " selected for removing from the current Risk Assessment!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Team Member Signature" : "these Linked Team Member Signatures") + " will be removed from the Risk Assessment.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strFilePath = strSignaturePath;
                            if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
 
                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "SignatureFile").ToString();
                                if (!string.IsNullOrEmpty(strImageName))
                                {
                                    strImageName = strFilePath + strImageName;
                                    try
                                    {
                                        base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                        GC.GetTotalMemory(true);
                                        System.IO.File.Delete(strImageName);
                                    }
                                    catch (Exception ex)
                                    {
                                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked signature [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                        continue;
                                    }
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SubContractorSignatureID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("risk_assessment_team_member_signature", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedSignatures();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }




        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            //if (loadingForm != null) loadingForm.Close();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Data...", "Report Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        /*
               public class SaveCommandHandler : ICommandHandler
               {
                   XRDesignPanel panel;

                   public string strFullPath = "";

                   public string strFileName = "";

                   public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                   {
                       this.panel = panel;
                       this.strFullPath = strFullPath;
                       this.strFileName = strFileName;
                   }

                   public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                   {
                       if (!CanHandleCommand(command)) return;
                       Save();  // Save report //
                       handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                   }

                   public virtual bool CanHandleCommand(ReportCommand command)
                   {
                       // This handler is used for SaveFile, SaveFileAs and Closing commands.
                       return command == ReportCommand.SaveFile ||
                           command == ReportCommand.SaveFileAs ||
                           command == ReportCommand.Closing;
                   }

                   void Save()
                   {
                       // Custom Saving Logic //
                       Boolean blSaved = false;
                       panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                       // Update existing file layout //
                       panel.Report.DataSource = null;
                       panel.Report.DataMember = null;
                       panel.Report.DataAdapter = null;
                       try
                       {
                           panel.Report.SaveLayout(strFullPath + strFileName);
                           blSaved = true;
                       }
                       catch (Exception ex)
                       {
                           DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                           Console.WriteLine(ex.Message);
                           blSaved = false;
                       }
                       if (blSaved)
                       {
                           panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                       }
                   }
               }
       */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }






    }
}


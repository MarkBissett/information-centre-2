using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Team_WorkOrder_Team_Members_On_Site : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool isRunning = false;

        public int intWorkOrderID = 0;
        public string strWorkOrderTeamIDs = "";
        public bool boolChangesMade = false;  // Used to pass back to caller if changes were made so caller needs to reload data //
        #endregion

        public frm_UT_Team_WorkOrder_Team_Members_On_Site()
        {
            InitializeComponent();
        }

        private void frm_UT_Team_WorkOrder_Team_Members_On_Site_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500069;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            sp07295_UT_Team_Work_Order_Available_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07296_UT_Team_Work_Order_Selected_Team_MembersTableAdapter.Connection.ConnectionString = strConnectionString;
  
            LoadData();
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07295_UT_Team_Work_Order_Available_Team_MembersTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07295_UT_Team_Work_Order_Available_Team_Members, strWorkOrderTeamIDs);
            view.EndUpdate();
            view.ExpandAllGroups();

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            sp07296_UT_Team_Work_Order_Selected_Team_MembersTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07296_UT_Team_Work_Order_Selected_Team_Members, strWorkOrderTeamIDs);
            view.EndUpdate();
            view.ExpandAllGroups();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Team Members Available";
                    break;
                case "gridView2":
                    message = "No Team Members Selected";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            btnSelectOne.PerformClick();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }

        #endregion


        #region GridView2

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            btnBackOne.PerformClick();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }

        #endregion


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSelectOne_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Members to set as 'On Site' before proceeding.", "Add Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intTeamMemberID = 0;
            using (DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddTeamMember = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter())
            {
                AddTeamMember.ChangeConnectionString(strConnectionString);
                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["Active"])) == 0)
                    {
                        view.UnselectRow(intRowHandle);
                    }
                    else
                    {
                        intTeamMemberID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["TeamMemberID"]));
                        try
                        {
                            AddTeamMember.sp07297_UT_Team_Work_Order_Add_Team_Member(intWorkOrderID, intTeamMemberID);
                        }
                        catch (Exception) { }
                    }
                }
            }
            boolChangesMade = true;
            LoadData();
        }

        private void btnBackOne_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Team Members to set as 'Not On Site' before proceeding.", "Remove Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intWorkOrderTeamMemberID = 0;
            using (DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter RemoveTeamMember = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter())
            {
                RemoveTeamMember.ChangeConnectionString(strConnectionString);
                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["Active"])) == 0)
                    {
                        view.UnselectRow(intRowHandle);
                    }
                    else
                    {
                        intWorkOrderTeamMemberID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["WorkOrderTeamMemberID"]));
                        try
                        {
                            RemoveTeamMember.sp07298_UT_Team_Work_Order_Delete_Team_Member(intWorkOrderTeamMemberID);
                        }
                        catch (Exception) { }
                    }
                }
            }
            boolChangesMade = true;
            LoadData();
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int intRowCount = view.DataRowCount;
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Members available to set as 'On Site'.", "Add Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
            int intTeamMemberID = 0;
            using (DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddTeamMember = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter())
            {
                AddTeamMember.ChangeConnectionString(strConnectionString);
                for (int intRowHandle = 0; intRowHandle < intRowCount; intRowHandle++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["Active"])) == 0)
                    {
                        view.UnselectRow(intRowHandle);
                    }
                    else
                    {
                        intTeamMemberID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["TeamMemberID"]));
                        try
                        {
                            AddTeamMember.sp07297_UT_Team_Work_Order_Add_Team_Member(intWorkOrderID, intTeamMemberID);
                        }
                        catch (Exception) { }
                    }
                }
            }
            boolChangesMade = true;
            LoadData();
        }

        private void btnBackAll_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intRowCount = view.DataRowCount;
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Team Members available to set as 'Not On Site'.", "Remove Team Members", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intWorkOrderTeamMemberID = 0;
            using (DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter RemoveTeamMember = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter())
            {
                RemoveTeamMember.ChangeConnectionString(strConnectionString);
                for (int intRowHandle = 0; intRowHandle < intRowCount; intRowHandle++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["Active"])) == 0)
                    {
                        view.UnselectRow(intRowHandle);
                    }
                    else
                    {
                        intWorkOrderTeamMemberID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["WorkOrderTeamMemberID"]));
                        try
                        {
                            RemoveTeamMember.sp07298_UT_Team_Work_Order_Delete_Team_Member(intWorkOrderTeamMemberID);
                        }
                        catch (Exception) { }
                    }
                }
            }
            boolChangesMade = true;
            LoadData();
        }






    }
}


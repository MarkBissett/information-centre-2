using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_pole_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToClientID = 0;
        public int intLinkedToCircuitID = 0;
        public string strLinkedToCircuitName = "";
        public string strLinkedToCircuitNumber = "";
        public string strLinkedToClientName = "";
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = true;

        // Passed in when called by mapping screen //
        public bool boolCalledByMapping = false;
        public float flX = (float)0;
        public float flY = (float)0;
        public float flLatitude = (float)0;
        public float flLongitude = (float)0;
        public string MapID = "";
        public string strMapIDs = "";  // Only required when block editing from the map - used to tell the tree picker which objects to reload //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";

        #endregion

        public frm_UT_pole_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_pole_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500010;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07006_UT_Circuit_StatusTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07006_UT_Circuit_StatusTableAdapter.Fill(dataSet_UT.sp07006_UT_Circuit_Status);

            sp07045_UT_Pole_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07045_UT_Pole_Types_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07045_UT_Pole_Types_With_Blank);

            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07046_UT_Inspection_Cycles_With_Blank);


            sp07068_UT_Access_Issue_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07068_UT_Access_Issue_List_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07068_UT_Access_Issue_List_With_Blank);

            sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07073_UT_Environmental_Issue_List_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07073_UT_Environmental_Issue_List_With_Blank);

            sp07078_UT_Site_Hazards_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07078_UT_Site_Hazards_List_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07078_UT_Site_Hazards_List_With_Blank);

            sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07083_UT_Electrical_Hazards_List_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07083_UT_Electrical_Hazards_List_With_Blank);


            sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "PoleAccessIssueID");

            sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView3, "PoleEnvironmentalIssueID");

            sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView5, "PoleSiteHazardID");

            sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView6, "PoleElectricalHazardID");

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            // Populate Main Dataset //          
            sp07043_UT_Pole_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07043_UT_Pole_Item.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientID"] = intLinkedToClientID;
                        drNewRow["ClientName"] = strLinkedToClientName;
                        drNewRow["CircuitID"] = intLinkedToCircuitID;
                        drNewRow["CircuitName"] = strLinkedToCircuitName;
                        drNewRow["CircuitNumber"] = strLinkedToCircuitNumber;
                        drNewRow["PoleTypeID"] = 0;
                        drNewRow["InspectionCycle"] = 0;
                        drNewRow["InspectionUnit"] = 0;
                        drNewRow["IsTransformer"] = 0;
                        drNewRow["StatusID"] = 1;  // Actual //
                        drNewRow["PoleNumber"] = _strLastUsedReferencePrefix;
                        drNewRow["X"] = flX;
                        drNewRow["Y"] = flY;
                        drNewRow["Latitude"] = flLatitude;
                        drNewRow["Longitude"] = flLongitude;
                        drNewRow["MapID"] = MapID;

                        this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows.Add(drNewRow);
                        if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix))  // Add Suffix number to Reference Number //
                        {
                            Get_Suffix_Number();  // Get next value in sequence //
                        }
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_Edit.sp07043_UT_Pole_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["CircuitName"] = "";
                        drNewRow["CircuitNumber"] = "";
                        drNewRow["ClientName"] = strLinkedToClientName;
                        drNewRow["PoleNumber"] = "";
                        this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows.Add(drNewRow);
                        this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07043_UT_Pole_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07043_UT_Pole_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
 
                    gridControl1.BeginUpdate();
                    try
                    {
                        sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter.Fill(dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    gridControl1.EndUpdate();
 
                    gridControl2.BeginUpdate();
                    try
                    {
                        sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter.Fill(dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    gridControl2.EndUpdate();

                    gridControl3.BeginUpdate();
                    try
                    {
                        sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter.Fill(dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    gridControl3.EndUpdate();
                  
                    gridControl4.BeginUpdate();
                    try
                    {
                        sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter.Fill(dataSet_UT_Edit .sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    gridControl4.EndUpdate();
                  
                    if (strFormMode == "view")
                    {
                        iBool_AllowDelete = false;
                        iBool_AllowAdd = false;
                    }
                    break;

            }
            dataLayoutControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        PoleNumberButtonEdit.Focus();

                        CircuitNameButtonEdit.Properties.ReadOnly = false;
                        PoleNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        PoleNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        XTextEdit.Properties.ReadOnly = false;
                        YTextEdit.Properties.ReadOnly = false;
                        LatitudeTextEdit.Properties.ReadOnly = false;
                        LongitudeTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StatusIDGridLookUpEdit.Focus();

                        CircuitNameButtonEdit.Properties.ReadOnly = true;
                        PoleNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        PoleNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        XTextEdit.Properties.ReadOnly = true;
                        YTextEdit.Properties.ReadOnly = true;
                        LatitudeTextEdit.Properties.ReadOnly = true;
                        LongitudeTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        StatusIDGridLookUpEdit.Focus();

                        CircuitNameButtonEdit.Properties.ReadOnly = false;
                        PoleNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        PoleNumberButtonEdit.Properties.Buttons[1].Enabled = true;
                        XTextEdit.Properties.ReadOnly = false;
                        YTextEdit.Properties.ReadOnly = false;
                        LatitudeTextEdit.Properties.ReadOnly = false;
                        LongitudeTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        StatusIDGridLookUpEdit.Focus();

                        CircuitNameButtonEdit.Properties.ReadOnly = true;
                        PoleNumberButtonEdit.Properties.Buttons[0].Enabled = false;
                        PoleNumberButtonEdit.Properties.Buttons[1].Enabled = false;
                        XTextEdit.Properties.ReadOnly = true;
                        YTextEdit.Properties.ReadOnly = true;
                        LatitudeTextEdit.Properties.ReadOnly = true;
                        LongitudeTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9151", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9151:  // Pole Types Picklist //    
                        {
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            PoleTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
                gridControl2.Enabled = false;
                gridControl3.Enabled = false;
                gridControl4.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intPoleID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                intPoleID = (currentRow["PoleID"] == null ? 0 : Convert.ToInt32(currentRow["PoleID"]));
            }

            if (i_int_FocusedGrid == 1)  // Access Issues //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Environmental Issues //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Site Hazards //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 4)  // Electrical Hazards //
            {
                view = (GridView)gridControl4.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView2 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView4 navigator custom buttons //
            if (iBool_AllowAdd && strFormMode != "blockedit")
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                gridControl4.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

        }


        private void frm_UT_pole_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_UT_pole_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix))
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PoleNumberPrefix", _strLastUsedReferencePrefix);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("PoleNumberPrefix");
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07043UTPoleItemBindingSource.EndEdit();
            try
            {
                this.sp07043_UT_Pole_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view = (GridView)gridControl2.MainView;
            view.PostEditor();
            view = (GridView)gridControl3.MainView;
            view.PostEditor();
            view = (GridView)gridControl4.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["PoleID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }

                // Pass new ID to any records in the linked Jobs grid //
                view = (GridView)gridControl1.MainView;
                int intPoleID = Convert.ToInt32(currentRow["PoleID"]);
                //strRecordIDs = intPoleID.ToString() + ",";
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.Rows)
                {
                    dr["PoleID"] = intPoleID;
                }
                this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();

                view = (GridView)gridControl2.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.Rows)
                {
                    dr["PoleID"] = intPoleID;
                }
                this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();

                view = (GridView)gridControl3.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.Rows)
                {
                    dr["PoleID"] = intPoleID;
                }
                this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();

                view = (GridView)gridControl4.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.Rows)
                {
                    dr["PoleID"] = intPoleID;
                }
                this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();
            }

            // Pick up all Map IDs and concatenate together so Tree Picker can be updated if it is running //
            string strUpdateIDs = "";
            if (this.strFormMode.ToLower() == "blockedit")
            {
                strUpdateIDs = strMapIDs;
            }
            else
            {
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows)
                {
                    if (!string.IsNullOrEmpty(dr["MapID"].ToString()))
                    {
                        strUpdateIDs += dr["MapID"].ToString() + ",";
                    }
                }
            }

            try  // Attempt to save any changes to Linked Access Issues... //
            {
                sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked access issue record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            try  // Attempt to save any changes to Linked Environmental Issues... //
            {
                sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked environmental issue record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            try  // Attempt to save any changes to Linked Site Hazards... //
            {
                sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked site hazard record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            try  // Attempt to save any changes to Linked Electrical Hazards... //
            {
                sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked electrical hazard record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }


            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Pole_Manager")
                    {
                        var fParentForm = (frm_UT_Pole_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "", "", "");
                    }
                    if (frmChild.Name == "frm_UT_Mapping" && !string.IsNullOrEmpty(strUpdateIDs))  // Mapping Form //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.RefreshMapObjects(strUpdateIDs, 0);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            int intAccessIssuesNew = 0;
            int intAccessIssuesModified = 0;
            int intAccessIssuesDeleted = 0;
            this.sp07066UTPoleAccessIssuesLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intAccessIssuesNew++;
                        break;
                    case DataRowState.Modified:
                        intAccessIssuesModified++;
                        break;
                    case DataRowState.Deleted:
                        intAccessIssuesDeleted++;
                        break;
                }
            }

            int intEnvironmentalIssuesNew = 0;
            int intEnvironmentalIssuesModified = 0;
            int intEnvironmentalIssuesDeleted = 0;
            this.sp07071UTPoleEnvironmentalIssuesLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intEnvironmentalIssuesNew++;
                        break;
                    case DataRowState.Modified:
                        intEnvironmentalIssuesModified++;
                        break;
                    case DataRowState.Deleted:
                        intEnvironmentalIssuesDeleted++;
                        break;
                }
            }

            int intSiteHazardsNew = 0;
            int intSiteHazardsModified = 0;
            int intSiteHazardsDeleted = 0;
            this.sp07076UTPoleSiteHazardsLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intSiteHazardsNew++;
                        break;
                    case DataRowState.Modified:
                        intSiteHazardsModified++;
                        break;
                    case DataRowState.Deleted:
                        intSiteHazardsDeleted++;
                        break;
                }
            }

            int intElecticalHazardsNew = 0;
            int intElecticalHazardsModified = 0;
            int intElecticalHazardsDeleted = 0;
            this.sp07081UTPoleElecticalHazardsLinkedToPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intElecticalHazardsNew++;
                        break;
                    case DataRowState.Modified:
                        intElecticalHazardsModified++;
                        break;
                    case DataRowState.Deleted:
                        intElecticalHazardsDeleted++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intAccessIssuesNew > 0 || intAccessIssuesModified > 0 || intAccessIssuesDeleted > 0 || intEnvironmentalIssuesNew > 0 || intEnvironmentalIssuesModified > 0 || intEnvironmentalIssuesDeleted > 0 || intSiteHazardsNew > 0 || intSiteHazardsModified > 0 || intSiteHazardsDeleted > 0 || intElecticalHazardsNew > 0 || intElecticalHazardsModified > 0 || intElecticalHazardsDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                if (intAccessIssuesNew > 0) strMessage += Convert.ToString(intAccessIssuesNew) + " New linked Access Issue record(s)\n";
                if (intAccessIssuesModified > 0) strMessage += Convert.ToString(intAccessIssuesModified) + " Updated linked Access Issue record(s)\n";
                if (intAccessIssuesDeleted > 0) strMessage += Convert.ToString(intAccessIssuesDeleted) + " Deleted linked Access Issue record(s)\n";
                if (intEnvironmentalIssuesNew > 0) strMessage += Convert.ToString(intEnvironmentalIssuesNew) + " New linked Environmental Issue record(s)\n";
                if (intEnvironmentalIssuesModified > 0) strMessage += Convert.ToString(intEnvironmentalIssuesModified) + " Updated linked Environmental Issue record(s)\n";
                if (intEnvironmentalIssuesDeleted > 0) strMessage += Convert.ToString(intEnvironmentalIssuesDeleted) + " Deleted linked Environmental Issue record(s)\n";
                if (intSiteHazardsNew > 0) strMessage += Convert.ToString(intSiteHazardsNew) + " New linked Site Hazard record(s)\n";
                if (intSiteHazardsModified > 0) strMessage += Convert.ToString(intSiteHazardsModified) + " Updated linked Site Hazard record(s)\n";
                if (intSiteHazardsDeleted > 0) strMessage += Convert.ToString(intSiteHazardsDeleted) + " Deleted linked Site Hazard record(s)\n";
                if (intElecticalHazardsNew > 0) strMessage += Convert.ToString(intElecticalHazardsNew) + " New linked Electrical Hazard record(s)\n";
                if (intElecticalHazardsModified > 0) strMessage += Convert.ToString(intElecticalHazardsModified) + " Updated linked Electrical Hazard record(s)\n";
                if (intElecticalHazardsDeleted > 0) strMessage += Convert.ToString(intElecticalHazardsDeleted) + " Deleted linked Electrical Hazard record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void CircuitNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                int intCircuitID = (string.IsNullOrEmpty(currentRow["CircuitID"].ToString()) ? 0 : Convert.ToInt32(currentRow["CircuitID"]));
                frm_UT_Select_Circuit fChildForm = new frm_UT_Select_Circuit();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInClientID = intClientID;
                fChildForm.intPassedInCircuitID = intCircuitID;
                fChildForm.intFilterUtilityArbClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["CircuitID"] = fChildForm.intSelectedCircuitID;
                    currentRow["ClientID"] = fChildForm.intSelectedClientID;
                    currentRow["CircuitName"] = fChildForm.strSelectedClientName + " \\ " + fChildForm.strSelectedCircuit;
                    currentRow["CircuitNumber"] = fChildForm.strSelectedCircuitNumber;
                    currentRow["ClientName"] = fChildForm.strSelectedClientName;
                    sp07043UTPoleItemBindingSource.EndEdit();
                }
            }
            else if ("edit".Equals(e.Button.Tag))
            {
                //Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3001, "Clients");
            }
        }
        private void CircuitNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(CircuitNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(CircuitNameButtonEdit, "");
            }
        }

        private void PoleNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "UT_Pole";
                fChildForm.PassedInFieldName = "PoleNumber";
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    PoleNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                Get_Suffix_Number();  // Get next value in sequence //  
            }
        }
        private void PoleNumberButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(PoleNumberButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PoleNumberButtonEdit, "");
            }
        }
        private void Get_Suffix_Number()
        {
            string strSequence = PoleNumberButtonEdit.EditValue.ToString() ?? "";

            SequenceNumberGetNext sequence = new SequenceNumberGetNext();
            string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, "PoleNumber", "UT_Pole", strSequence);  // Calculate Next in Sequence //

            if (strNextNumber == "")
            {
                return;
            }
            if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

            int intRequiredLength = strNextNumber.Length;
            int intNextNumber = Convert.ToInt32(strNextNumber);
            int intIncrement = 0;
            string strTempNumber = "";


            // Check if value is already present within dataset //
            DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strCurrentActionID = "";
            if (!string.IsNullOrEmpty(currentRow["PoleID"].ToString())) strCurrentActionID = currentRow["PoleID"].ToString();

            bool boolUniqueValueFound = false;
            bool boolDuplicateFound = false;
            do
            {
                boolDuplicateFound = false;
                strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                }
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07043_UT_Pole_Item.Rows)
                {
                    if (dr["PoleNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                    {
                        intIncrement++;
                        boolDuplicateFound = true;
                        break;
                    }
                }
                if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
            } while (!boolUniqueValueFound);
            PoleNumberButtonEdit.EditValue = strSequence + strTempNumber;
        }

        private void PoleTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 251, "Utilities Pole Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp07045_UT_Pole_Types_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07045_UT_Pole_Types_With_Blank);
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();
                }
            }

        }

        private void LastInspectionDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void LastInspectionDateDateEdit_Validated(object sender, EventArgs e)
        {
            Calc_Next_Date(true);
        }

        private void InspectionCycleSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void InspectionCycleSpinEdit_Validated(object sender, EventArgs e)
        {
            Calc_Next_Date(true);
        }

        private void InspectionUnitGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        }
        private void InspectionUnitGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Calc_Next_Date(true);
        }

        private void SubAreaNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
                if (currentRow == null) return;
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                int intSubAreaID = (string.IsNullOrEmpty(currentRow["SubAreaID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SubAreaID"]));
                frm_UT_Select_SubArea fChildForm = new frm_UT_Select_SubArea();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInClientID = intClientID;
                fChildForm.intPassedInSubAreaID = intSubAreaID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["SubAreaID"] = fChildForm.intSelectedSubAreaID;
                    currentRow["SubAreaName"] = fChildForm.strSelectedSubAreaName;
                    sp07043UTPoleItemBindingSource.EndEdit();
                }
            }
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView2":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Access Issues NOT Shown When Block Adding and Block Editing" : "No Linked Access Issues Available - Click the Add button to Create Access Issues");
                    break;
                case "gridView3":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Environmental Issues NOT Shown When Block Adding and Block Editing" : "No Linked Environmental Issues Available - Click the Add button to Create Environmental Issues");
                    break;
                case "gridView5":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Site Hazards NOT Shown When Block Adding and Block Editing" : "No Linked Site Hazards Available - Click the Add button to Create Site Hazards");
                    break;
                case "gridView6":
                    message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Electrical Hazards NOT Shown When Block Adding and Block Editing" : "No Linked Electrical Hazards Available - Click the Add button to Create Electrical Hazards");
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        #endregion


        #region GridControl1

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridControl2

        private void gridView3_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.Color.Gray;
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridControl3

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridControl4

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            if (!e.Column.OptionsColumn.AllowEdit || e.Column.ReadOnly) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block add".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void Calc_Next_Date(bool boolShowConfirmation)
        {
            if (ibool_ignoreValidation) return; ;  // Toggles validation on and off for Next Inspection Date Calculation //
            ibool_ignoreValidation = true;

            if (InspectionCycleSpinEdit.EditValue == null || string.IsNullOrEmpty(InspectionCycleSpinEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            int intCycle = Convert.ToInt32(InspectionCycleSpinEdit.EditValue);

            if (InspectionUnitGridLookUpEdit.EditValue == null || string.IsNullOrEmpty(InspectionUnitGridLookUpEdit.EditValue.ToString())) return;  // Test numeric value for null first //
            int intCycleDescriptor = Convert.ToInt32(InspectionUnitGridLookUpEdit.EditValue);
            if (intCycleDescriptor < 1 || intCycleDescriptor > 4) return;
        
            if (String.IsNullOrEmpty(LastInspectionDateDateEdit.DateTime.ToString()) || LastInspectionDateDateEdit.DateTime < Convert.ToDateTime("1900-01-01")) return;
            DateTime dtLastDate = Convert.ToDateTime(LastInspectionDateDateEdit.EditValue);

            DateTime dtCalculatedDate = DateTime.MinValue;

            switch (intCycleDescriptor)
            {
                case 1:  // Days //
                    dtCalculatedDate = dtLastDate.AddDays(intCycle);
                    break;
                case 2:  // Weeks //
                    dtCalculatedDate = dtLastDate.AddDays(intCycle * 7);
                    break;
                case 3:  // Months //
                    dtCalculatedDate = dtLastDate.AddMonths(intCycle);
                    break;
                case 4:  // Years //
                    dtCalculatedDate = dtLastDate.AddYears(intCycle);
                    break;
                default:
                    break;
            }
            if (!String.IsNullOrEmpty(NextInspectionDateDateEdit.EditValue.ToString()))
            {
                DateTime dtNextDate = Convert.ToDateTime(NextInspectionDateDateEdit.EditValue);
                if (dtNextDate != dtCalculatedDate && boolShowConfirmation)
                {
                    string strMessage = "The calculated " + ItemForNextInspectionDate.Text + " is different to the value entered in the " + ItemForNextInspectionDate.Text + " field!\n\nCurrent Value:\t\t" + dtNextDate.ToString() + "\nCalculated Value:\t" + dtCalculatedDate.ToString() + "\n\nDo you want to set the current value to the calculated value?";
                    if (XtraMessageBox.Show(strMessage, "Calculate Next Inspection Date", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.No) return;
                }
            }
            // Notice the the dtNextInspectionDateDateEdit update is wrapped with an End Edit before and after it - as sometimes it doesn't always do it but by doing this it solves the problem. //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            NextInspectionDateDateEdit.EditValue = dtCalculatedDate;
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }


        private void Add_Record()
        {
            DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intPoleID = 0;
            if (!string.IsNullOrEmpty(currentRow["PoleID"].ToString())) intPoleID = Convert.ToInt32(currentRow["PoleID"]);
            switch (i_int_FocusedGrid)
            {
                case 1:  // Access Issues //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["PoleID"] = intPoleID;
                            drNewRow["AccessIssueID"] = 0;
                            this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new access issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Access Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case 2:  // Environmental Issues //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["PoleID"] = intPoleID;
                            drNewRow["EnvironmentalIssueID"] = 0;
                            this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new environmental issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Environmental Issues", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case 3:  // Site Hazards //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["PoleID"] = intPoleID;
                            drNewRow["SiteHazardID"] = 0;
                            this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new site hazard - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Site Hazards", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case 4:  // Electrical Hazards //
                    {
                        try
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["PoleID"] = intPoleID;
                            drNewRow["ElectricalHazardID"] = 0;
                            this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new electrical hazard - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Electrical Hazards", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                default:
                    break;
            }
 
         }

        private void Block_Add_Record()
        {
            DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intPoleID = 0;
            if (!string.IsNullOrEmpty(currentRow["PoleID"].ToString())) intPoleID = Convert.ToInt32(currentRow["PoleID"]);
            int intHeaderID = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Access Issues //
                    {
                        intHeaderID = 254;
                    }
                    break;
                case 2:  // Environmental Issues //
                    {
                        intHeaderID = 255;
                    }
                    break;
                case 3:  // Site Hazards //
                    {
                        intHeaderID = 256;
                    }
                    break;
                case 4:  // Electrical Hazards //
                    {
                        intHeaderID = 257;
                    }
                    break;
                default:
                    break;
            }
            frm_UT_Pole_Edit_Block_Add_Links fChildForm = new frm_UT_Pole_Edit_Block_Add_Links();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intPassedInHeaderID = intHeaderID;
            string strSelectedIDs = "";
            int intSelectedCount = 0;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            strSelectedIDs = fChildForm.strSelectedIDs;
            intSelectedCount = fChildForm.intSelectedCount;

            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strSelectedIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            switch (i_int_FocusedGrid)
            {
                case 1:  // Access Issues //
                    {
                        foreach (string item in strArray)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["PoleID"] = intPoleID;
                                drNewRow["AccessIssueID"] = Convert.ToInt32(item);
                                this.dataSet_UT_Edit.sp07066_UT_Pole_Access_Issues_Linked_To_Pole.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new access issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Access Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                        }
                    }
                    break;
                case 2:  // Environmental Issues //
                    {
                        foreach (string item in strArray)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["PoleID"] = intPoleID;
                                drNewRow["EnvironmentalIssueID"] = Convert.ToInt32(item);
                                this.dataSet_UT_Edit.sp07071_UT_Pole_Environmental_Issues_Linked_To_Pole.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new access issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Access Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                        }
                    }
                    break;
                case 3:  // Site Hazards //
                    {
                        foreach (string item in strArray)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["PoleID"] = intPoleID;
                                drNewRow["SiteHazardID"] = Convert.ToInt32(item);
                                this.dataSet_UT_Edit.sp07076_UT_Pole_Site_Hazards_Linked_To_Pole.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new access issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Access Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                        }
                    }
                    break;
                case 4:  // Electrical Hazards //
                    {
                        foreach (string item in strArray)
                        {

                            try
                            {
                                DataRow drNewRow;
                                drNewRow = this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.NewRow();
                                drNewRow["strMode"] = "add";
                                drNewRow["PoleID"] = intPoleID;
                                drNewRow["ElectricalHazardID"] = Convert.ToInt32(item);
                                this.dataSet_UT_Edit.sp07081_UT_Pole_Electical_Hazards_Linked_To_Pole.Rows.Add(drNewRow);
                            }
                            catch (Exception Ex)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new access issue - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Access Issue", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Access Issues //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Access Issues to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Access Issue" : Convert.ToString(intRowHandles.Length) + " Access Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Access Issue" : "these Access Issues") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Access Issue(s) deleted.\n\nImportant Note: Deleted Access Issues are only physically removed from the pole on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
                case 2:  // Environmental Issues //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Environmental Issue to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Environmental Issue" : Convert.ToString(intRowHandles.Length) + " Environmental Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Environmental Issue" : "these Environmental Issues") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Environmental Issue(s) deleted.\n\nImportant Note: Deleted Environmental Issues are only physically removed from the pole on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
                case 3:  // Site Hazards //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Site Hazard" : Convert.ToString(intRowHandles.Length) + " Site Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Site Hazard" : "these Site Hazards") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Site Hazard(s) deleted.\n\nImportant Note: Deleted Site Hazards are only physically removed from the pole on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
                case 4:  // Electrical Hazards //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Electrical Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Electrical Hazard" : Convert.ToString(intRowHandles.Length) + " Electrical Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Electrical Hazard" : "these Electrical Hazards") + " will no longer be available for selection!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " Electrical Hazard(s) deleted.\n\nImportant Note: Deleted Access Issues are only physically removed from the pole on saving changes.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetChangesPendingLabel();
                        }
                    }
                    break;
            }

        }

        private void FilterGrids()
        {
            string strPoleID = "";
            DataRowView currentRow = (DataRowView)sp07043UTPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                strPoleID = (currentRow["PoleID"] == null ? "0" : currentRow["PoleID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[PoleID] = " + Convert.ToString(strPoleID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControl2.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[PoleID] = " + Convert.ToString(strPoleID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControl3.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[PoleID] = " + Convert.ToString(strPoleID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();

            view = (GridView)gridControl4.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[PoleID] = " + Convert.ToString(strPoleID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }






    }
}


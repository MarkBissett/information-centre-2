namespace WoodPlan5
{
    partial class frm_UT_Utilities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Utilities));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditAllData = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditMissingData = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditTrees = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditPoles = new DevExpress.XtraEditors.CheckEdit();
            this.btnClose2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnSynchronise = new DevExpress.XtraEditors.SimpleButton();
            this.bbiRemove = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearGrid = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMissingData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTrees.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPoles.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(712, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 469);
            this.barDockControlBottom.Size = new System.Drawing.Size(712, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 469);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(712, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 469);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRemove,
            this.bbiClearGrid});
            this.barManager1.MaxItemId = 28;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.Size = new System.Drawing.Size(712, 469);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(707, 443);
            this.xtraTabPage2.Text = "Synchronise Lat \\ Long Values";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.labelControl8);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Information";
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl4);
            this.splitContainerControl2.Panel2.Controls.Add(this.groupControl3);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnClose2);
            this.splitContainerControl2.Panel2.Controls.Add(this.btnSynchronise);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(707, 443);
            this.splitContainerControl2.SplitterPosition = 69;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("labelControl8.Appearance.Image")));
            this.labelControl8.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl8.Location = new System.Drawing.Point(0, 0);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(703, 46);
            this.labelControl8.TabIndex = 9;
            this.labelControl8.Text = resources.GetString("labelControl8.Text");
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.checkEditAllData);
            this.groupControl4.Controls.Add(this.checkEditMissingData);
            this.groupControl4.Location = new System.Drawing.Point(11, 96);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(241, 72);
            this.groupControl4.TabIndex = 20;
            this.groupControl4.Text = "Update Condition";
            // 
            // checkEditAllData
            // 
            this.checkEditAllData.Location = new System.Drawing.Point(6, 48);
            this.checkEditAllData.MenuManager = this.barManager1;
            this.checkEditAllData.Name = "checkEditAllData";
            this.checkEditAllData.Properties.Caption = "Update All Records";
            this.checkEditAllData.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditAllData.Properties.RadioGroupIndex = 1;
            this.checkEditAllData.Size = new System.Drawing.Size(230, 19);
            this.checkEditAllData.TabIndex = 1;
            this.checkEditAllData.TabStop = false;
            // 
            // checkEditMissingData
            // 
            this.checkEditMissingData.EditValue = true;
            this.checkEditMissingData.Location = new System.Drawing.Point(6, 26);
            this.checkEditMissingData.MenuManager = this.barManager1;
            this.checkEditMissingData.Name = "checkEditMissingData";
            this.checkEditMissingData.Properties.Caption = "Update Only Records With Missing Data";
            this.checkEditMissingData.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditMissingData.Properties.RadioGroupIndex = 1;
            this.checkEditMissingData.Size = new System.Drawing.Size(230, 19);
            this.checkEditMissingData.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.checkEditTrees);
            this.groupControl3.Controls.Add(this.checkEditPoles);
            this.groupControl3.Location = new System.Drawing.Point(11, 9);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(241, 72);
            this.groupControl3.TabIndex = 19;
            this.groupControl3.Text = "Update Data Type";
            // 
            // checkEditTrees
            // 
            this.checkEditTrees.EditValue = true;
            this.checkEditTrees.Location = new System.Drawing.Point(6, 48);
            this.checkEditTrees.MenuManager = this.barManager1;
            this.checkEditTrees.Name = "checkEditTrees";
            this.checkEditTrees.Properties.Caption = "Utilities - Trees";
            this.checkEditTrees.Size = new System.Drawing.Size(230, 19);
            this.checkEditTrees.TabIndex = 1;
            // 
            // checkEditPoles
            // 
            this.checkEditPoles.EditValue = true;
            this.checkEditPoles.Location = new System.Drawing.Point(6, 26);
            this.checkEditPoles.MenuManager = this.barManager1;
            this.checkEditPoles.Name = "checkEditPoles";
            this.checkEditPoles.Properties.Caption = "Utilities - Poles";
            this.checkEditPoles.Size = new System.Drawing.Size(230, 19);
            this.checkEditPoles.TabIndex = 0;
            // 
            // btnClose2
            // 
            this.btnClose2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose2.Location = new System.Drawing.Point(627, 340);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.Size = new System.Drawing.Size(75, 23);
            this.btnClose2.TabIndex = 18;
            this.btnClose2.Text = "Close";
            this.btnClose2.Click += new System.EventHandler(this.btnClose2_Click);
            // 
            // btnSynchronise
            // 
            this.btnSynchronise.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSynchronise.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSynchronise.Appearance.Options.UseFont = true;
            this.btnSynchronise.Location = new System.Drawing.Point(547, 340);
            this.btnSynchronise.Name = "btnSynchronise";
            this.btnSynchronise.Size = new System.Drawing.Size(75, 23);
            this.btnSynchronise.TabIndex = 17;
            this.btnSynchronise.Text = "Synchronise";
            this.btnSynchronise.Click += new System.EventHandler(this.btnSynchronise_Click);
            // 
            // bbiRemove
            // 
            this.bbiRemove.Caption = "Remove Selected Files From List";
            this.bbiRemove.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRemove.Glyph")));
            this.bbiRemove.Id = 26;
            this.bbiRemove.Name = "bbiRemove";
            // 
            // bbiClearGrid
            // 
            this.bbiClearGrid.Caption = "Clear List";
            this.bbiClearGrid.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearGrid.Glyph")));
            this.bbiClearGrid.Id = 27;
            this.bbiClearGrid.Name = "bbiClearGrid";
            // 
            // frm_UT_Utilities
            // 
            this.ClientSize = new System.Drawing.Size(712, 469);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Utilities";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utilities";
            this.Load += new System.EventHandler(this.frm_UT_Utilities_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditAllData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditMissingData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditTrees.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPoles.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraBars.BarButtonItem bbiRemove;
        private DevExpress.XtraBars.BarButtonItem bbiClearGrid;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.SimpleButton btnClose2;
        private DevExpress.XtraEditors.SimpleButton btnSynchronise;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditTrees;
        private DevExpress.XtraEditors.CheckEdit checkEditPoles;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditAllData;
        private DevExpress.XtraEditors.CheckEdit checkEditMissingData;
    }
}

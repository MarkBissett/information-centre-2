namespace WoodPlan5
{
    partial class frm_UT_Region_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Region_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07009UTRegionItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.txtBillingCentreDetail = new DevExpress.XtraEditors.TextEdit();
            this.slueBillingCentreCodeID = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.spAS11038BillingCentreCodeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.billingCentreCodeIDSearchLookUpEditView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkspaceIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07108UTMappingWorkspacesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.RegionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkspaceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07009_UT_Region_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07009_UT_Region_ItemTableAdapter();
            this.sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_UT_EditTableAdapters.TableAdapterManager();
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07009UTRegionItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreDetail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slueBillingCentreCodeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeIDSearchLookUpEditView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07108UTMappingWorkspacesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(702, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 552);
            this.barDockControlBottom.Size = new System.Drawing.Size(702, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 526);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(702, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 526);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID2
            // 
            this.colID2.Caption = "Mapping Workspace ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(702, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 552);
            this.barDockControl2.Size = new System.Drawing.Size(702, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 526);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(702, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 526);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.txtBillingCentreDetail);
            this.dataLayoutControl1.Controls.Add(this.slueBillingCentreCodeID);
            this.dataLayoutControl1.Controls.Add(this.WorkspaceIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.RegionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07009UTRegionItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientID,
            this.ItemForRegionID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(809, 410, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(702, 526);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(127, 362);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(551, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 29;
            // 
            // sp07009UTRegionItemBindingSource
            // 
            this.sp07009UTRegionItemBindingSource.DataMember = "sp07009_UT_Region_Item";
            this.sp07009UTRegionItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtBillingCentreDetail
            // 
            this.txtBillingCentreDetail.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "BillingCentreDetail", true));
            this.txtBillingCentreDetail.Location = new System.Drawing.Point(115, 167);
            this.txtBillingCentreDetail.MenuManager = this.barManager1;
            this.txtBillingCentreDetail.Name = "txtBillingCentreDetail";
            this.txtBillingCentreDetail.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtBillingCentreDetail, true);
            this.txtBillingCentreDetail.Size = new System.Drawing.Size(575, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtBillingCentreDetail, optionsSpelling2);
            this.txtBillingCentreDetail.StyleController = this.dataLayoutControl1;
            this.txtBillingCentreDetail.TabIndex = 28;
            // 
            // slueBillingCentreCodeID
            // 
            this.slueBillingCentreCodeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "BillingCentreCodeID", true));
            this.slueBillingCentreCodeID.EditValue = "";
            this.slueBillingCentreCodeID.Location = new System.Drawing.Point(115, 141);
            this.slueBillingCentreCodeID.MenuManager = this.barManager1;
            this.slueBillingCentreCodeID.Name = "slueBillingCentreCodeID";
            this.slueBillingCentreCodeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("slueBillingCentreCodeID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("slueBillingCentreCodeID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "refresh", null, true)});
            this.slueBillingCentreCodeID.Properties.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.slueBillingCentreCodeID.Properties.DisplayMember = "BillingCentreCode";
            this.slueBillingCentreCodeID.Properties.NullText = "";
            this.slueBillingCentreCodeID.Properties.NullValuePrompt = "-- Please Select Billing Centre --";
            this.slueBillingCentreCodeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.slueBillingCentreCodeID.Properties.ValueMember = "BillingCentreCodeID";
            this.slueBillingCentreCodeID.Properties.View = this.billingCentreCodeIDSearchLookUpEditView;
            this.slueBillingCentreCodeID.Size = new System.Drawing.Size(575, 22);
            this.slueBillingCentreCodeID.StyleController = this.dataLayoutControl1;
            this.slueBillingCentreCodeID.TabIndex = 27;
            this.slueBillingCentreCodeID.Tag = "Billing Centre";
            this.slueBillingCentreCodeID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.slueBillingCentreCodeID_ButtonPressed);
            this.slueBillingCentreCodeID.EditValueChanged += new System.EventHandler(this.slueBillingCentreCodeID_EditValueChanged);
            this.slueBillingCentreCodeID.Validating += new System.ComponentModel.CancelEventHandler(this.slueBillingCentreCodeID_Validating);
            // 
            // spAS11038BillingCentreCodeItemBindingSource
            // 
            this.spAS11038BillingCentreCodeItemBindingSource.DataMember = "sp_AS_11038_Billing_Centre_Code_Item";
            this.spAS11038BillingCentreCodeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "DataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // billingCentreCodeIDSearchLookUpEditView
            // 
            this.billingCentreCodeIDSearchLookUpEditView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCodeID,
            this.colBillingCentreCode,
            this.colCompanyID,
            this.colCompany,
            this.colCompanyCode,
            this.colDepartmentID,
            this.colDepartment,
            this.colDepartmentCode,
            this.colCostCentreID,
            this.colCostCentre,
            this.colCostCentreCode,
            this.colMode,
            this.colRecordID});
            this.billingCentreCodeIDSearchLookUpEditView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.billingCentreCodeIDSearchLookUpEditView.Name = "billingCentreCodeIDSearchLookUpEditView";
            this.billingCentreCodeIDSearchLookUpEditView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.billingCentreCodeIDSearchLookUpEditView.OptionsView.ShowGroupPanel = false;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCodeID.Width = 126;
            // 
            // colBillingCentreCode
            // 
            this.colBillingCentreCode.FieldName = "BillingCentreCode";
            this.colBillingCentreCode.Name = "colBillingCentreCode";
            this.colBillingCentreCode.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCode.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCode.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCode.Visible = true;
            this.colBillingCentreCode.VisibleIndex = 0;
            this.colBillingCentreCode.Width = 112;
            // 
            // colCompanyID
            // 
            this.colCompanyID.FieldName = "CompanyID";
            this.colCompanyID.Name = "colCompanyID";
            this.colCompanyID.OptionsColumn.AllowEdit = false;
            this.colCompanyID.OptionsColumn.AllowFocus = false;
            this.colCompanyID.OptionsColumn.ReadOnly = true;
            this.colCompanyID.Width = 81;
            // 
            // colCompany
            // 
            this.colCompany.FieldName = "Company";
            this.colCompany.Name = "colCompany";
            this.colCompany.OptionsColumn.AllowEdit = false;
            this.colCompany.OptionsColumn.AllowFocus = false;
            this.colCompany.OptionsColumn.ReadOnly = true;
            this.colCompany.Visible = true;
            this.colCompany.VisibleIndex = 1;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.AllowFocus = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Width = 95;
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.Width = 93;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 3;
            this.colDepartment.Width = 79;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.Width = 107;
            // 
            // colCostCentreID
            // 
            this.colCostCentreID.FieldName = "CostCentreID";
            this.colCostCentreID.Name = "colCostCentreID";
            this.colCostCentreID.OptionsColumn.AllowEdit = false;
            this.colCostCentreID.OptionsColumn.AllowFocus = false;
            this.colCostCentreID.OptionsColumn.ReadOnly = true;
            this.colCostCentreID.Width = 94;
            // 
            // colCostCentre
            // 
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.AllowEdit = false;
            this.colCostCentre.OptionsColumn.AllowFocus = false;
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 2;
            this.colCostCentre.Width = 80;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.Width = 108;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // WorkspaceIDGridLookUpEdit
            // 
            this.WorkspaceIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "WorkspaceID", true));
            this.WorkspaceIDGridLookUpEdit.Location = new System.Drawing.Point(115, 117);
            this.WorkspaceIDGridLookUpEdit.MenuManager = this.barManager1;
            this.WorkspaceIDGridLookUpEdit.Name = "WorkspaceIDGridLookUpEdit";
            this.WorkspaceIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WorkspaceIDGridLookUpEdit.Properties.DataSource = this.sp07108UTMappingWorkspacesListWithBlankBindingSource;
            this.WorkspaceIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.WorkspaceIDGridLookUpEdit.Properties.NullText = "";
            this.WorkspaceIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.WorkspaceIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.WorkspaceIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.WorkspaceIDGridLookUpEdit.Size = new System.Drawing.Size(575, 20);
            this.WorkspaceIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.WorkspaceIDGridLookUpEdit.TabIndex = 26;
            // 
            // sp07108UTMappingWorkspacesListWithBlankBindingSource
            // 
            this.sp07108UTMappingWorkspacesListWithBlankBindingSource.DataMember = "sp07108_UT_Mapping_Workspaces_List_With_Blank";
            this.sp07108UTMappingWorkspacesListWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder,
            this.colRemarks});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Workspace";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 302;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(98, 77);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling3);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 25;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07009UTRegionItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(115, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(199, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // RegionIDTextEdit
            // 
            this.RegionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "RegionID", true));
            this.RegionIDTextEdit.Location = new System.Drawing.Point(98, 77);
            this.RegionIDTextEdit.MenuManager = this.barManager1;
            this.RegionIDTextEdit.Name = "RegionIDTextEdit";
            this.RegionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionIDTextEdit, true);
            this.RegionIDTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionIDTextEdit, optionsSpelling4);
            this.RegionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionIDTextEdit.TabIndex = 4;
            // 
            // RegionNumberTextEdit
            // 
            this.RegionNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "RegionNumber", true));
            this.RegionNumberTextEdit.Location = new System.Drawing.Point(115, 93);
            this.RegionNumberTextEdit.MenuManager = this.barManager1;
            this.RegionNumberTextEdit.Name = "RegionNumberTextEdit";
            this.RegionNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNumberTextEdit, true);
            this.RegionNumberTextEdit.Size = new System.Drawing.Size(575, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNumberTextEdit, optionsSpelling5);
            this.RegionNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNumberTextEdit.TabIndex = 6;
            // 
            // RegionNameTextEdit
            // 
            this.RegionNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "RegionName", true));
            this.RegionNameTextEdit.Location = new System.Drawing.Point(115, 69);
            this.RegionNameTextEdit.MenuManager = this.barManager1;
            this.RegionNameTextEdit.Name = "RegionNameTextEdit";
            this.RegionNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNameTextEdit, true);
            this.RegionNameTextEdit.Size = new System.Drawing.Size(575, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNameTextEdit, optionsSpelling6);
            this.RegionNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameTextEdit.TabIndex = 7;
            this.RegionNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RegionNameTextEdit_Validating);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 271);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(630, 99);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling7);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07009UTRegionItemBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(115, 35);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Select Client screen", "choose", null, true)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(575, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 5;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 105);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(591, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRegionID
            // 
            this.ItemForRegionID.Control = this.RegionIDTextEdit;
            this.ItemForRegionID.CustomizationFormText = "Region ID:";
            this.ItemForRegionID.Location = new System.Drawing.Point(0, 105);
            this.ItemForRegionID.Name = "ItemForRegionID";
            this.ItemForRegionID.Size = new System.Drawing.Size(591, 24);
            this.ItemForRegionID.Text = "Region ID:";
            this.ItemForRegionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 128);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(658, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(702, 526);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientName,
            this.ItemForRegionNumber,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForRegionName,
            this.ItemForWorkspaceID,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(682, 396);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.AllowHide = false;
            this.ItemForClientName.Control = this.ClientNameButtonEdit;
            this.ItemForClientName.CustomizationFormText = "Linked to Client:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(682, 24);
            this.ItemForClientName.Text = "Linked to Client:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForRegionNumber
            // 
            this.ItemForRegionNumber.AllowHide = false;
            this.ItemForRegionNumber.Control = this.RegionNumberTextEdit;
            this.ItemForRegionNumber.CustomizationFormText = "Region Number:";
            this.ItemForRegionNumber.Location = new System.Drawing.Point(0, 81);
            this.ItemForRegionNumber.Name = "ItemForRegionNumber";
            this.ItemForRegionNumber.Size = new System.Drawing.Size(682, 24);
            this.ItemForRegionNumber.Text = "Region Number:";
            this.ItemForRegionNumber.TextSize = new System.Drawing.Size(100, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(103, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(103, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(103, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(306, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(376, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(103, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(203, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 179);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(682, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 189);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(682, 197);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(658, 151);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(634, 103);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(634, 103);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 386);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(682, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(682, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.AllowHide = false;
            this.ItemForRegionName.Control = this.RegionNameTextEdit;
            this.ItemForRegionName.CustomizationFormText = "Region Name:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 57);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(682, 24);
            this.ItemForRegionName.Text = "Region Name:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(100, 13);
            // 
            // ItemForWorkspaceID
            // 
            this.ItemForWorkspaceID.Control = this.WorkspaceIDGridLookUpEdit;
            this.ItemForWorkspaceID.CustomizationFormText = "Mapping Workspace:";
            this.ItemForWorkspaceID.Location = new System.Drawing.Point(0, 105);
            this.ItemForWorkspaceID.Name = "ItemForWorkspaceID";
            this.ItemForWorkspaceID.Size = new System.Drawing.Size(682, 24);
            this.ItemForWorkspaceID.Text = "Mapping Workspace:";
            this.ItemForWorkspaceID.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.slueBillingCentreCodeID;
            this.layoutControlItem3.CustomizationFormText = "Billing Centre Code:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 129);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(682, 26);
            this.layoutControlItem3.Text = "Billing Centre Code:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtBillingCentreDetail;
            this.layoutControlItem4.CustomizationFormText = "Billing Centre Detail:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 155);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(682, 24);
            this.layoutControlItem4.Text = "Billing Centre Detail:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 396);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(682, 110);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(682, 110);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07009_UT_Region_ItemTableAdapter
            // 
            this.sp07009_UT_Region_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter
            // 
            this.sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp07004_UT_Circuit_ItemTableAdapter = null;
            this.tableAdapterManager.sp07009_UT_Region_ItemTableAdapter = null;
            this.tableAdapterManager.sp07012_UT_SubArea_ItemTableAdapter = null;
            this.tableAdapterManager.sp07019_UT_Primary_ItemTableAdapter = null;
            this.tableAdapterManager.sp07024_UT_Feeder_ItemTableAdapter = null;
            this.tableAdapterManager.sp07043_UT_Pole_ItemTableAdapter = null;
            this.tableAdapterManager.sp07056_UT_Tree_ItemTableAdapter = null;
            this.tableAdapterManager.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter = null;
            this.tableAdapterManager.sp07066_UT_Pole_Access_Issues_Linked_To_PoleTableAdapter = null;
            this.tableAdapterManager.sp07071_UT_Pole_Environmental_Issues_Linked_To_PoleTableAdapter = null;
            this.tableAdapterManager.sp07076_UT_Pole_Site_Hazards_Linked_To_PoleTableAdapter = null;
            this.tableAdapterManager.sp07081_UT_Pole_Electical_Hazards_Linked_To_PoleTableAdapter = null;
            this.tableAdapterManager.sp07089_UT_Access_Issue_ItemTableAdapter = null;
            this.tableAdapterManager.sp07091_UT_Environmental_Issue_ItemTableAdapter = null;
            this.tableAdapterManager.sp07093_UT_Site_Hazard_ItemTableAdapter = null;
            this.tableAdapterManager.sp07095_UT_Electrical_Hazard_ItemTableAdapter = null;
            this.tableAdapterManager.sp07103_UT_Survey_ItemTableAdapter = null;
            this.tableAdapterManager.sp07114_UT_Surveyed_Pole_ItemTableAdapter = null;
            this.tableAdapterManager.sp07128_UT_Surveyed_Tree_Work_EditTableAdapter = null;
            this.tableAdapterManager.sp07131_UT_Surveyed_Tree_Work_Linked_EquipmentTableAdapter = null;
            this.tableAdapterManager.sp07132_UT_Surveyed_Tree_Work_Linked_MaterialsTableAdapter = null;
            this.tableAdapterManager.sp07142_UT_Materials_ItemTableAdapter = null;
            this.tableAdapterManager.sp07146_UT_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp07151_UT_Surveyed_Tree_Defect_EditTableAdapter = null;
            this.tableAdapterManager.sp07158_UT_Tree_Defect_ItemTableAdapter = null;
            this.tableAdapterManager.sp07188_UT_Circuit_Contract_ItemTableAdapter = null;
            this.tableAdapterManager.sp07221_UT_Survey_Costing_PoleTableAdapter = null;
            this.tableAdapterManager.sp07222_UT_Survey_Costing_ItemTableAdapter = null;
            this.tableAdapterManager.sp07223_UT_Suvey_Costing_Header_ItemTableAdapter = null;
            this.tableAdapterManager.sp07236_UT_Linked_Reference_Files_ItemTableAdapter = null;
            this.tableAdapterManager.sp07252_UT_Risk_Assessment_Question_ItemTableAdapter = null;
            this.tableAdapterManager.sp07256_UT_Risk_Assessment_Question_ItemTableAdapter = null;
            this.tableAdapterManager.sp07258_UT_Surveyed_Pole_Risk_Assessment_Questions_EditTableAdapter = null;
            this.tableAdapterManager.sp07302_UT_Site_Completion_Question_ItemTableAdapter = null;
            this.tableAdapterManager.sp07362_UT_Traffic_Management_ItemTableAdapter = null;
            this.tableAdapterManager.sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter = null;
            this.tableAdapterManager.sp07375_UT_Shutdown_EditTableAdapter = null;
            this.tableAdapterManager.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter = null;
            this.tableAdapterManager.sp07410_UT_Master_Answer_ItemTableAdapter = null;
            this.tableAdapterManager.sp07415_UT_Master_Question_Answer_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_UT_EditTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_AS_11038_Billing_Centre_Code_ItemTableAdapter
            // 
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Region_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(702, 582);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Region_Edit";
            this.Text = "Edit Region";
            this.Activated += new System.EventHandler(this.frm_UT_Region_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Region_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Region_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07009UTRegionItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreDetail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slueBillingCentreCodeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.billingCentreCodeIDSearchLookUpEditView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07108UTMappingWorkspacesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit RegionIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit RegionNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit RegionNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource sp07009UTRegionItemBindingSource;
        private DataSet_UT_EditTableAdapters.sp07009_UT_Region_ItemTableAdapter sp07009_UT_Region_ItemTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit WorkspaceIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkspaceID;
        private System.Windows.Forms.BindingSource sp07108UTMappingWorkspacesListWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter sp07108_UT_Mapping_Workspaces_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.TextEdit txtBillingCentreDetail;
        private DevExpress.XtraEditors.SearchLookUpEdit slueBillingCentreCodeID;
        private DevExpress.XtraGrid.Views.Grid.GridView billingCentreCodeIDSearchLookUpEditView;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DataSet_UT_EditTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource spAS11038BillingCentreCodeItemBindingSource;
        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreID;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter sp_AS_11038_Billing_Centre_Code_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
    }
}

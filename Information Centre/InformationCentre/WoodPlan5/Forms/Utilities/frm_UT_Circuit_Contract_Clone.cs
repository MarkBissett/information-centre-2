using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_UT_Circuit_Contract_Clone : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strRecordIDs = "";
        public string strParentIDs = "";
        public int intRecordCount = 0;

        public string strReturnedNewIDs = "";
        #endregion

        public frm_UT_Circuit_Contract_Clone()
        {
            InitializeComponent();
        }

        private void frm_UT_Circuit_Contract_Clone_Load(object sender, EventArgs e)
        {
            this.FormID = 20114;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            barStaticItemInformation.Caption = "Records Selected For Update: " + intRecordCount.ToString();
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }

        private void checkEditClone_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                groupControlClonedValues.Enabled = true;
                FromDate.Enabled = true;
                ToDate.Enabled = true;
                checkEditActive.Enabled = true;
                btnOK.Text = "Clone Contracts";
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            }
        }

        private void checkEditApplyRateUplift_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                groupControlClonedValues.Enabled = false;
                FromDate.Enabled = false;
                ToDate.Enabled = false;
                checkEditActive.Enabled = false;
                btnOK.Text = "Apply Rate Uplift";
                this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            }
        }


        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\nTip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Clone Contracts\\Apply Rate Uplifts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (checkEditApplyRateUplift.Checked && Convert.ToDecimal(spinEditAmount.EditValue) == (decimal)0.00)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Uplift Amount\\Percentage before proceeding.", "Clone Contracts\\Apply Rate Uplifts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }



            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Updating Rates...");
            fProgress.Show();
            Application.DoEvents();
            DataSet_UT_EditTableAdapters.QueriesTableAdapter CloneContracts = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            CloneContracts.ChangeConnectionString(strConnectionString);
            
            string strTask = (checkEditClone.Checked ? "clone" : "uplift");
            DateTime dtFromDate = (checkEditClone.Checked ? FromDate.DateTime : DateTime.Now);
            DateTime dtToDate = (checkEditClone.Checked ? ToDate.DateTime : DateTime.Now);
            int intActive =  (checkEditActive.Checked ? 1 : 0);
            string strUpliftType = (checkEdit1.Checked ? "percentage" : "fixed amount");
            try
            {
                strReturnedNewIDs = CloneContracts.sp07190_UT_Circuit_Contract_Clone_Adjust_Rate(strRecordIDs, strParentIDs, strTask, dtFromDate, dtToDate, intActive, Convert.ToDecimal(spinEditAmount.EditValue), strUpliftType).ToString();
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while cloning contracts\\uplifting rates [" + ex.Message + "]!\n\nTry again - if the problem persists, contact Technical Support.", "Clone Contracts\\Apply Rate Uplifts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            fProgress.SetProgressValue(100);
            if (this.GlobalSettings.ShowConfirmations == 1)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Process Completed Successfully");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void FromDate_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (checkEditClone.Checked && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(FromDate, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(FromDate, "");
            }
        }

        private void ToDate_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (checkEditClone.Checked && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(ToDate, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ToDate, "");
            }
        }

        private void spinEditAmount_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (checkEditApplyRateUplift.Checked && (se.EditValue == null || Convert.ToInt32(se.EditValue) == 0))
            {
                dxErrorProvider1.SetError(spinEditAmount, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(spinEditAmount, "");
            }
        }




    
    }
}


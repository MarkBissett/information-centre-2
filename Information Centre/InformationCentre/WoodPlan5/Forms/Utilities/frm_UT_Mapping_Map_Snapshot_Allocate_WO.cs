using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Map_Snapshot_Allocate_WO : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strMapDescription;
        public string strMapRemarks;
        public int intSelectedRecordID = 0;
        GridHitInfo downHitInfo = null;
        public string strExcludedWorkOrderIDs = "";
        public int intOriginalSelectedValue = 0;

        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        #endregion
    
        public frm_UT_Mapping_Map_Snapshot_Allocate_WO()
        {
            InitializeComponent();
        }

        private void frm_UT_Mapping_Map_Snapshot_Allocate_WO_Load(object sender, EventArgs e)
        {
            this.FormID = 500065;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.Text += "  [Map: " + (string.IsNullOrEmpty(strMapDescription) ? "No Description" : strMapDescription) + "]";

            dateEditFromDate.DateTime = DateTime.Now.AddMonths(-3);
            dateEditToDate.DateTime = DateTime.Now;
            
            this.sp07246_UT_Work_Order_Select_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Data();
            gridControl1.ForceInitialize();

            if (intOriginalSelectedValue != 0)  // Rate selected so try to find and highlight //
            {
                GridView view = (GridView)gridControl1.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["WorkOrderID"], intOriginalSelectedValue);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            teMapDescription.EditValue = strMapDescription;
            meMapRemarks.EditValue = strMapRemarks;
        }

        private void Load_Data()
        {
            DateTime dtFromDate = dateEditFromDate.DateTime;
            DateTime dtToDate = dateEditToDate.DateTime;
            try
            {
                sp07246_UT_Work_Order_Select_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07246_UT_Work_Order_Select_List, dtFromDate, dtToDate);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the work orders list.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            popupContainerDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
        }

        private void btnLoadWorkOrders_Click(object sender, EventArgs e)
        {
           Load_Data();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Records Available For Selection");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle < 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Work Order to allocate the map to before proceeding.", "Allocate Work Order Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intSelectedRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "WorkOrderID"));
            strMapDescription = teMapDescription.EditValue.ToString();
            strMapRemarks = meMapRemarks.EditValue.ToString();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl1.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion



    }
}


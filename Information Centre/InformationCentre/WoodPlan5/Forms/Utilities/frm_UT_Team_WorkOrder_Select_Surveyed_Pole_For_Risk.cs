using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Team_WorkOrder_Select_Surveyed_Pole_For_Risk : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public int intOriginalID = 0;
        public int intSelectedID = 0;
        public string strSelectedDescription = "";
        public string strPassedInPoleIDs = "";
        #endregion

        public frm_UT_Team_WorkOrder_Select_Surveyed_Pole_For_Risk()
        {
            InitializeComponent();
        }

        private void frm_UT_Team_WorkOrder_Select_Surveyed_Pole_For_Risk_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 50002600;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp07293_UT_Team_Work_Order_Surveyed_Pole_For_Risk_AssessmentTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;
            //view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

            LoadData();
            gridControl1.ForceInitialize();

            if (intOriginalID != 0)  // Inspection selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intOriginalID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07293_UT_Team_Work_Order_Surveyed_Pole_For_Risk_AssessmentTableAdapter.Fill(dataSet_UT_WorkOrder.sp07293_UT_Team_Work_Order_Surveyed_Pole_For_Risk_Assessment, strPassedInPoleIDs);
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Surveyed Poles Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }
        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnSiteSpecific_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedDescription == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a Surveyed Pole before proceeding.", "Select Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnG552_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedDescription == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a Surveyed Pole before proceeding.", "Select Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                 intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyedPoleID"));
                 strSelectedDescription = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "PoleNumber"))) ? "Unknown Pole" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "PoleNumber")));
            }
        }




    }
}


namespace WoodPlan5
{
    partial class frm_UT_Survey_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Survey_Edit));
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule6 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barCheckItemSurveyDetails = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemSurveyCostings = new DevExpress.XtraBars.BarCheckItem();
            this.barEditItemEditTotals = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditEditTotals = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.barCheckItemEnableTotals = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ContractTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07103UTSurveyItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.sp07468UTContractTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.TargetWorkCompletionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TargetPermissionCompletionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TargetSurveyCompletionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPDFFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSignatureDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyAccess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestTelephonePoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestAandE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveAccessAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAccessAgreedWithLandOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessDetailsOnMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRoadsideAccessOnly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTPOTree = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPlanningConservation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWildlifeDesignation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpecialInstructions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimarySubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLineNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLVSubNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colManagedUnitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnvironmentalRANumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandscapeImpactNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteGridReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostageRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByPost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerSalutation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipRemove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsChipOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsStackOnSite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArisingsOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToDoActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotPermissionedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandownerRestrictedCut1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ContractYearSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.CopyAddressButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.CopyAddressButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.ReactiveCategoryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07405UTReactiveCategoryTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IncidentPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IncidentAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReactiveRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReportedByEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReportedByTelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReportedByPostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReportedByAddressMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReportedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnCalculateTotals = new DevExpress.XtraEditors.SimpleButton();
            this.QuoteApprovedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.QuoteReceivedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExchequerNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientPONumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalIncomeSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TotalCostSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WorkspaceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastMapRecordIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnLoadMapWithSurvey = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.WorkspaceNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastMapRecordDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.MapStartTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07105UTMapStartTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ReactiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SurveyorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07328UTSurveyorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SurveyStatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07106UTSurveyStatusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SurveyDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastMapYTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LastMapXTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanResilient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIvyOnPole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinearCutLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredEstimatedCuttingHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceAbove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.SurveyIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForSurveyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastMapRecordID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkspaceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastMapX = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastMapY = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkspaceName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForMapStartTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLastMapRecordDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractYear = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTargetSurveyCompletionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTargetPermissionCompletionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTargetWorkCompletionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLoadSurveyIntoMap = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForReactive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientPONumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExchequerNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteReceivedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQuoteApprovedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForReportedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportedByEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCopyAddressButton1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForReactiveRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIncidentAddress = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForIncidentPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCopyAddressButton2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.ItemForReactiveCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoleName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalIncome = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter();
            this.sp07103_UT_Survey_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07103_UT_Survey_ItemTableAdapter();
            this.sp07105_UT_Map_Start_TypesTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07105_UT_Map_Start_TypesTableAdapter();
            this.sp07106_UT_Survey_StatusesTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07106_UT_Survey_StatusesTableAdapter();
            this.sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07220UTSurveyCostingHeadersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyCostingHeaderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSurveyDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalIncome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07221UTSurveyCostingPoleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView3 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand17 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand18 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand19 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSurveyCostingPoleID = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSurveyCostingHeaderID1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colLiveWorkDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colLiveWorkPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoEditPoleNumbers = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.colLiveWorkPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD1Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD1PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD1Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD2PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD3PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD4PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD5PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD6PoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear3DeferralPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear5DeferralPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear1RevisitPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear2RevisitPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear3RevisitPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear4RevisitPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear5RevisitPoleIDs = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD2Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD2Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD3Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD3Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD4Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD4Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD5Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD5Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colSD6Date = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSD6Poles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear3DeferralDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear3DeferralPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand9 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear5DeferralDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear5DeferralPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand10 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear1RevisitDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear1RevisitPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand11 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear2RevisitDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear2RevisitPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand12 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear3RevisitDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear3RevisitPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand13 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear4RevisitDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear4RevisitPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand14 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colYear5RevisitDate = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colYear5RevisitPoles = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand15 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colRemarks2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridBand20 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07222UTSurveyCostingItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyCostingItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyCostingHeaderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditResource = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditUnits = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07138UTUnitDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLiveWorkUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditUnits = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colLiveWorkCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEditCost = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colSD1Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD1Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD2Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD2Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD3Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD3Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD4Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD4Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD5Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD5Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD6Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSD6Cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear3DeferralUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear3DeferralCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear5DeferralUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear5DeferralCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear1RevisitUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear1RevisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear2RevisitUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear2RevisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear3RevisitUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear3RevisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear4RevisitUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear4RevisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear5RevisitUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYear5RevisitCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTotalUnits = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTotalCost = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07220_UT_Survey_Costing_HeadersTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07220_UT_Survey_Costing_HeadersTableAdapter();
            this.sp07221_UT_Survey_Costing_PoleTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07221_UT_Survey_Costing_PoleTableAdapter();
            this.sp07222_UT_Survey_Costing_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07222_UT_Survey_Costing_ItemTableAdapter();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter();
            this.pmSurveyPoles = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddPoles = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearPoles = new DevExpress.XtraBars.BarButtonItem();
            this.sp07328_UT_Surveyor_List_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07328_UT_Surveyor_List_With_BlankTableAdapter();
            this.sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter();
            this.sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07468_UT_Contract_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07468_UT_Contract_Types_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditEditTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07103UTSurveyItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07468UTContractTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetWorkCompletionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetWorkCompletionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetPermissionCompletionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetPermissionCompletionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetSurveyCompletionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetSurveyCompletionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractYearSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCategoryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07405UTReactiveCategoryTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByTelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByAddressMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteApprovedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteApprovedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteReceivedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteReceivedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExchequerNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalIncomeSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapRecordIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapRecordDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapStartTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07105UTMapStartTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07328UTSurveyorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07106UTSurveyStatusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapYTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapXTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapRecordID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapStartTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapRecordDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetSurveyCompletionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetPermissionCompletionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetWorkCompletionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLoadSurveyIntoMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteReceivedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteApprovedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyAddressButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactiveRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyAddressButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactiveCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalIncome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07220UTSurveyCostingHeadersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07221UTSurveyCostingPoleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEditPoleNumbers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07222UTSurveyCostingItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditResource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07138UTUnitDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSurveyPoles)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(908, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 682);
            this.barDockControlBottom.Size = new System.Drawing.Size(908, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 656);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(908, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 656);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddPoles,
            this.bbiClearPoles});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ID";
            this.gridColumn6.FieldName = "ID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 53;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.barCheckItemSurveyDetails,
            this.barCheckItemSurveyCostings,
            this.barCheckItemEnableTotals,
            this.barEditItemEditTotals});
            this.barManager2.MaxItemId = 19;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3,
            this.repositoryItemCheckEditEditTotals});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip7.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem7.Text = "Save Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.bbiFormSave.SuperTip = superToolTip7;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem8.Text = "Cancel Button - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.bbiFormCancel.SuperTip = superToolTip8;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem9.Text = "Form Mode - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.barStaticItemFormMode.SuperTip = superToolTip9;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Survey Toolbar";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemSurveyDetails),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemSurveyCostings),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemEditTotals, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.Text = "Custom 4";
            // 
            // barCheckItemSurveyDetails
            // 
            this.barCheckItemSurveyDetails.BindableChecked = true;
            this.barCheckItemSurveyDetails.Caption = "Survey Details";
            this.barCheckItemSurveyDetails.Checked = true;
            this.barCheckItemSurveyDetails.GroupIndex = 1;
            this.barCheckItemSurveyDetails.Id = 15;
            this.barCheckItemSurveyDetails.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemSurveyDetails.ImageOptions.Image")));
            this.barCheckItemSurveyDetails.Name = "barCheckItemSurveyDetails";
            this.barCheckItemSurveyDetails.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barCheckItemSurveyDetails.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemSurveyDetails_ItemClick);
            // 
            // barCheckItemSurveyCostings
            // 
            this.barCheckItemSurveyCostings.Caption = "Survey Costings";
            this.barCheckItemSurveyCostings.GroupIndex = 1;
            this.barCheckItemSurveyCostings.Id = 16;
            this.barCheckItemSurveyCostings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemSurveyCostings.ImageOptions.Image")));
            this.barCheckItemSurveyCostings.Name = "barCheckItemSurveyCostings";
            this.barCheckItemSurveyCostings.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barCheckItemSurveyCostings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemSurveyCostings_ItemClick);
            // 
            // barEditItemEditTotals
            // 
            this.barEditItemEditTotals.Caption = "Edit Totals:";
            this.barEditItemEditTotals.Edit = this.repositoryItemCheckEditEditTotals;
            this.barEditItemEditTotals.EditValue = false;
            this.barEditItemEditTotals.EditWidth = 74;
            this.barEditItemEditTotals.Enabled = false;
            this.barEditItemEditTotals.Id = 18;
            this.barEditItemEditTotals.Name = "barEditItemEditTotals";
            // 
            // repositoryItemCheckEditEditTotals
            // 
            this.repositoryItemCheckEditEditTotals.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditEditTotals.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditEditTotals.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditEditTotals.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditEditTotals.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditEditTotals.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditEditTotals.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditEditTotals.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditEditTotals.AutoHeight = false;
            this.repositoryItemCheckEditEditTotals.Caption = "Edit Totals:";
            this.repositoryItemCheckEditEditTotals.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditEditTotals.Name = "repositoryItemCheckEditEditTotals";
            this.repositoryItemCheckEditEditTotals.CheckedChanged += new System.EventHandler(this.repositoryItemCheckEditEditTotals_CheckedChanged);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(908, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 682);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(908, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 656);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(908, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 656);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Refresh_16x16.png");
            this.imageCollection1.Images.SetKeyName(7, "linked_documents_16.png");
            // 
            // barCheckItemEnableTotals
            // 
            this.barCheckItemEnableTotals.Caption = "Edit Totals";
            this.barCheckItemEnableTotals.Id = 17;
            this.barCheckItemEnableTotals.Name = "barCheckItemEnableTotals";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ContractTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetWorkCompletionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetPermissionCompletionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.TargetSurveyCompletionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl7);
            this.dataLayoutControl1.Controls.Add(this.ContractYearSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.CopyAddressButton2);
            this.dataLayoutControl1.Controls.Add(this.CopyAddressButton1);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCategoryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IncidentPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IncidentAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByEmailTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByTelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByPostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByAddressMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnCalculateTotals);
            this.dataLayoutControl1.Controls.Add(this.QuoteApprovedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.QuoteReceivedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExchequerNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientPONumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalIncomeSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkspaceIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LastMapRecordIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnLoadMapWithSurvey);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.WorkspaceNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LastMapRecordDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.MapStartTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ReactiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyStatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SurveyDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LastMapYTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LastMapXTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.SurveyIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07103UTSurveyItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSurveyID,
            this.ItemForLastMapRecordID,
            this.ItemForClientID,
            this.ItemForWorkspaceID,
            this.ItemForCreatedByStaffID,
            this.ItemForLastMapX,
            this.ItemForLastMapY,
            this.ItemForWorkspaceName});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1017, 201, 378, 532);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(903, 630);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ContractTypeIDGridLookUpEdit
            // 
            this.ContractTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ContractTypeID", true));
            this.ContractTypeIDGridLookUpEdit.EditValue = "";
            this.ContractTypeIDGridLookUpEdit.Location = new System.Drawing.Point(565, 191);
            this.ContractTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ContractTypeIDGridLookUpEdit.Name = "ContractTypeIDGridLookUpEdit";
            this.ContractTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractTypeIDGridLookUpEdit.Properties.DataSource = this.sp07468UTContractTypesWithBlankBindingSource;
            this.ContractTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ContractTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ContractTypeIDGridLookUpEdit.Properties.PopupView = this.gridView8;
            this.ContractTypeIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6});
            this.ContractTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ContractTypeIDGridLookUpEdit.Size = new System.Drawing.Size(310, 20);
            this.ContractTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ContractTypeIDGridLookUpEdit.TabIndex = 5;
            this.ContractTypeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ContractTypeIDGridLookUpEdit_EditValueChanged);
            this.ContractTypeIDGridLookUpEdit.Validated += new System.EventHandler(this.ContractTypeIDGridLookUpEdit_Validated);
            // 
            // sp07103UTSurveyItemBindingSource
            // 
            this.sp07103UTSurveyItemBindingSource.DataMember = "sp07103_UT_Survey_Item";
            this.sp07103UTSurveyItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07468UTContractTypesWithBlankBindingSource
            // 
            this.sp07468UTContractTypesWithBlankBindingSource.DataMember = "sp07468_UT_Contract_Types_With_Blank";
            this.sp07468UTContractTypesWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.colRemarks4});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.gridColumn6;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridView8.FormatRules.Add(gridFormatRule5);
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Contract Type";
            this.gridColumn7.FieldName = "Description";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 0;
            this.gridColumn7.Width = 220;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Order";
            this.gridColumn8.FieldName = "RecordOrder";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Width = 102;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // TargetWorkCompletionDateDateEdit
            // 
            this.TargetWorkCompletionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "TargetWorkCompletionDate", true));
            this.TargetWorkCompletionDateDateEdit.EditValue = null;
            this.TargetWorkCompletionDateDateEdit.Location = new System.Drawing.Point(577, 297);
            this.TargetWorkCompletionDateDateEdit.MenuManager = this.barManager1;
            this.TargetWorkCompletionDateDateEdit.Name = "TargetWorkCompletionDateDateEdit";
            this.TargetWorkCompletionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TargetWorkCompletionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TargetWorkCompletionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TargetWorkCompletionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.TargetWorkCompletionDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.TargetWorkCompletionDateDateEdit.Size = new System.Drawing.Size(286, 20);
            this.TargetWorkCompletionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetWorkCompletionDateDateEdit.TabIndex = 10;
            // 
            // TargetPermissionCompletionDateDateEdit
            // 
            this.TargetPermissionCompletionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "TargetPermissionCompletionDate", true));
            this.TargetPermissionCompletionDateDateEdit.EditValue = null;
            this.TargetPermissionCompletionDateDateEdit.Location = new System.Drawing.Point(577, 273);
            this.TargetPermissionCompletionDateDateEdit.MenuManager = this.barManager1;
            this.TargetPermissionCompletionDateDateEdit.Name = "TargetPermissionCompletionDateDateEdit";
            this.TargetPermissionCompletionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TargetPermissionCompletionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TargetPermissionCompletionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TargetPermissionCompletionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.TargetPermissionCompletionDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.TargetPermissionCompletionDateDateEdit.Size = new System.Drawing.Size(286, 20);
            this.TargetPermissionCompletionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetPermissionCompletionDateDateEdit.TabIndex = 9;
            // 
            // TargetSurveyCompletionDateDateEdit
            // 
            this.TargetSurveyCompletionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "TargetSurveyCompletionDate", true));
            this.TargetSurveyCompletionDateDateEdit.EditValue = null;
            this.TargetSurveyCompletionDateDateEdit.Location = new System.Drawing.Point(577, 249);
            this.TargetSurveyCompletionDateDateEdit.MenuManager = this.barManager1;
            this.TargetSurveyCompletionDateDateEdit.Name = "TargetSurveyCompletionDateDateEdit";
            this.TargetSurveyCompletionDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TargetSurveyCompletionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TargetSurveyCompletionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TargetSurveyCompletionDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.TargetSurveyCompletionDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.TargetSurveyCompletionDateDateEdit.Size = new System.Drawing.Size(286, 20);
            this.TargetSurveyCompletionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.TargetSurveyCompletionDateDateEdit.TabIndex = 8;
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(387, 249);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(177, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling19);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 18;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Refresh Data", "refresh")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(40, 388);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEditNumeric0DP,
            this.repositoryItemCheckEdit4});
            this.gridControl7.Size = new System.Drawing.Size(823, 202);
            this.gridControl7.TabIndex = 18;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp07466UTPermissionDocumentsForSurveyedPoleBindingSource
            // 
            this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource.DataMember = "sp07466_UT_Permission_Documents_For_Surveyed_Pole";
            this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.colDateRaised,
            this.colRaisedByID,
            this.colSignatureFile,
            this.colPDFFile,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.gridColumn4,
            this.colSignatureDate,
            this.colEmergencyAccess,
            this.colNearestTelephonePoint,
            this.colGridReference,
            this.colNearestAandE,
            this.colHotGloveAccessAvailable,
            this.colAccessAgreedWithLandOwner,
            this.colAccessDetailsOnMap,
            this.colRoadsideAccessOnly,
            this.colEstimatedRevisitDate,
            this.colTPOTree,
            this.colPlanningConservation,
            this.colWildlifeDesignation,
            this.colSpecialInstructions,
            this.colPrimarySubName,
            this.colPrimarySubNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colLineNumber,
            this.colLVSubName,
            this.colLVSubNumber,
            this.colManagedUnitNumber,
            this.colEnvironmentalRANumber,
            this.colLandscapeImpactNumber,
            this.colSiteGridReference,
            this.colPermissionEmailed,
            this.colPostageRequired,
            this.colSentByPost,
            this.colSentByStaffID,
            this.colReferenceNumber,
            this.colOwnerName,
            this.colOwnerSalutation,
            this.colOwnerAddress,
            this.colOwnerPostcode,
            this.colOwnerTelephone,
            this.colOwnerEmail,
            this.colOwnerTypeID,
            this.colSiteAddress,
            this.colContractor,
            this.colArisingsChipRemove,
            this.colArisingsChipOnSite,
            this.colArisingsStackOnSite,
            this.colArisingsOther,
            this.colClientID,
            this.colRaisedByName,
            this.colSentByStaffName,
            this.colLinkedActionCount,
            this.colLinkedToDoActionCount,
            this.colOwnerType,
            this.colClientName,
            this.colDataEntryScreenName,
            this.colReportLayoutName,
            this.colNotPermissionedCount,
            this.colOnHoldCount,
            this.colPermissionedCount,
            this.gridColumn5,
            this.colLandownerRestrictedCut1,
            this.colPoleNumber1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsFind.AlwaysVisible = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReferenceNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView7_CustomDrawCell);
            this.gridView7.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView7_CustomRowCellEdit);
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView7_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView7_ShowingEditor);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 108;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colRaisedByID
            // 
            this.colRaisedByID.Caption = "Raised By ID";
            this.colRaisedByID.FieldName = "RaisedByID";
            this.colRaisedByID.Name = "colRaisedByID";
            this.colRaisedByID.OptionsColumn.AllowEdit = false;
            this.colRaisedByID.OptionsColumn.AllowFocus = false;
            this.colRaisedByID.OptionsColumn.ReadOnly = true;
            this.colRaisedByID.Width = 82;
            // 
            // colSignatureFile
            // 
            this.colSignatureFile.Caption = "Signature File";
            this.colSignatureFile.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colSignatureFile.FieldName = "SignatureFile";
            this.colSignatureFile.Name = "colSignatureFile";
            this.colSignatureFile.OptionsColumn.ReadOnly = true;
            this.colSignatureFile.Visible = true;
            this.colSignatureFile.VisibleIndex = 12;
            this.colSignatureFile.Width = 106;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colPDFFile
            // 
            this.colPDFFile.Caption = "PDF File";
            this.colPDFFile.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colPDFFile.FieldName = "PDFFile";
            this.colPDFFile.Name = "colPDFFile";
            this.colPDFFile.OptionsColumn.ReadOnly = true;
            this.colPDFFile.Visible = true;
            this.colPDFFile.VisibleIndex = 13;
            this.colPDFFile.Width = 118;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "Email to Client Date";
            this.colEmailedToClientDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToClientDate.Visible = true;
            this.colEmailedToClientDate.VisibleIndex = 19;
            this.colEmailedToClientDate.Width = 114;
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "Emailed To Customer Date";
            this.colEmailedToCustomerDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToCustomerDate.Visible = true;
            this.colEmailedToCustomerDate.VisibleIndex = 20;
            this.colEmailedToCustomerDate.Width = 147;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Remarks";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn4.FieldName = "Remarks";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 21;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colSignatureDate
            // 
            this.colSignatureDate.Caption = "Signature Date";
            this.colSignatureDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colSignatureDate.FieldName = "SignatureDate";
            this.colSignatureDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSignatureDate.Name = "colSignatureDate";
            this.colSignatureDate.OptionsColumn.AllowEdit = false;
            this.colSignatureDate.OptionsColumn.AllowFocus = false;
            this.colSignatureDate.OptionsColumn.ReadOnly = true;
            this.colSignatureDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSignatureDate.Width = 93;
            // 
            // colEmergencyAccess
            // 
            this.colEmergencyAccess.Caption = "Emergency Date";
            this.colEmergencyAccess.FieldName = "EmergencyAccess";
            this.colEmergencyAccess.Name = "colEmergencyAccess";
            this.colEmergencyAccess.OptionsColumn.AllowEdit = false;
            this.colEmergencyAccess.OptionsColumn.AllowFocus = false;
            this.colEmergencyAccess.OptionsColumn.ReadOnly = true;
            this.colEmergencyAccess.Visible = true;
            this.colEmergencyAccess.VisibleIndex = 22;
            this.colEmergencyAccess.Width = 100;
            // 
            // colNearestTelephonePoint
            // 
            this.colNearestTelephonePoint.Caption = "Nearest Telephone Point";
            this.colNearestTelephonePoint.FieldName = "NearestTelephonePoint";
            this.colNearestTelephonePoint.Name = "colNearestTelephonePoint";
            this.colNearestTelephonePoint.OptionsColumn.AllowEdit = false;
            this.colNearestTelephonePoint.OptionsColumn.AllowFocus = false;
            this.colNearestTelephonePoint.OptionsColumn.ReadOnly = true;
            this.colNearestTelephonePoint.Visible = true;
            this.colNearestTelephonePoint.VisibleIndex = 23;
            this.colNearestTelephonePoint.Width = 139;
            // 
            // colGridReference
            // 
            this.colGridReference.Caption = "Grid Reference";
            this.colGridReference.FieldName = "GridReference";
            this.colGridReference.Name = "colGridReference";
            this.colGridReference.OptionsColumn.AllowEdit = false;
            this.colGridReference.OptionsColumn.AllowFocus = false;
            this.colGridReference.OptionsColumn.ReadOnly = true;
            this.colGridReference.Visible = true;
            this.colGridReference.VisibleIndex = 24;
            this.colGridReference.Width = 93;
            // 
            // colNearestAandE
            // 
            this.colNearestAandE.Caption = "Nearest A & E";
            this.colNearestAandE.FieldName = "NearestAandE";
            this.colNearestAandE.Name = "colNearestAandE";
            this.colNearestAandE.OptionsColumn.AllowEdit = false;
            this.colNearestAandE.OptionsColumn.AllowFocus = false;
            this.colNearestAandE.OptionsColumn.ReadOnly = true;
            this.colNearestAandE.Visible = true;
            this.colNearestAandE.VisibleIndex = 25;
            this.colNearestAandE.Width = 107;
            // 
            // colHotGloveAccessAvailable
            // 
            this.colHotGloveAccessAvailable.Caption = "Hot Glove Access Available";
            this.colHotGloveAccessAvailable.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colHotGloveAccessAvailable.FieldName = "HotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.Name = "colHotGloveAccessAvailable";
            this.colHotGloveAccessAvailable.OptionsColumn.AllowEdit = false;
            this.colHotGloveAccessAvailable.OptionsColumn.AllowFocus = false;
            this.colHotGloveAccessAvailable.OptionsColumn.ReadOnly = true;
            this.colHotGloveAccessAvailable.Visible = true;
            this.colHotGloveAccessAvailable.VisibleIndex = 26;
            this.colHotGloveAccessAvailable.Width = 150;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colAccessAgreedWithLandOwner
            // 
            this.colAccessAgreedWithLandOwner.Caption = "Access Agreed with Landowner";
            this.colAccessAgreedWithLandOwner.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAccessAgreedWithLandOwner.FieldName = "AccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.Name = "colAccessAgreedWithLandOwner";
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowEdit = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.AllowFocus = false;
            this.colAccessAgreedWithLandOwner.OptionsColumn.ReadOnly = true;
            this.colAccessAgreedWithLandOwner.Visible = true;
            this.colAccessAgreedWithLandOwner.VisibleIndex = 28;
            this.colAccessAgreedWithLandOwner.Width = 171;
            // 
            // colAccessDetailsOnMap
            // 
            this.colAccessDetailsOnMap.Caption = "Access Details On Map";
            this.colAccessDetailsOnMap.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAccessDetailsOnMap.FieldName = "AccessDetailsOnMap";
            this.colAccessDetailsOnMap.Name = "colAccessDetailsOnMap";
            this.colAccessDetailsOnMap.OptionsColumn.AllowEdit = false;
            this.colAccessDetailsOnMap.OptionsColumn.AllowFocus = false;
            this.colAccessDetailsOnMap.OptionsColumn.ReadOnly = true;
            this.colAccessDetailsOnMap.Visible = true;
            this.colAccessDetailsOnMap.VisibleIndex = 29;
            this.colAccessDetailsOnMap.Width = 129;
            // 
            // colRoadsideAccessOnly
            // 
            this.colRoadsideAccessOnly.Caption = "Roadside Access Only";
            this.colRoadsideAccessOnly.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colRoadsideAccessOnly.FieldName = "RoadsideAccessOnly";
            this.colRoadsideAccessOnly.Name = "colRoadsideAccessOnly";
            this.colRoadsideAccessOnly.OptionsColumn.AllowEdit = false;
            this.colRoadsideAccessOnly.OptionsColumn.AllowFocus = false;
            this.colRoadsideAccessOnly.OptionsColumn.ReadOnly = true;
            this.colRoadsideAccessOnly.Visible = true;
            this.colRoadsideAccessOnly.VisibleIndex = 30;
            this.colRoadsideAccessOnly.Width = 126;
            // 
            // colEstimatedRevisitDate
            // 
            this.colEstimatedRevisitDate.Caption = "Estimated Revisit Date";
            this.colEstimatedRevisitDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEstimatedRevisitDate.FieldName = "EstimatedRevisitDate";
            this.colEstimatedRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEstimatedRevisitDate.Name = "colEstimatedRevisitDate";
            this.colEstimatedRevisitDate.OptionsColumn.AllowEdit = false;
            this.colEstimatedRevisitDate.OptionsColumn.AllowFocus = false;
            this.colEstimatedRevisitDate.OptionsColumn.ReadOnly = true;
            this.colEstimatedRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEstimatedRevisitDate.Visible = true;
            this.colEstimatedRevisitDate.VisibleIndex = 31;
            this.colEstimatedRevisitDate.Width = 129;
            // 
            // colTPOTree
            // 
            this.colTPOTree.Caption = "TPO Tree";
            this.colTPOTree.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colTPOTree.FieldName = "TPOTree";
            this.colTPOTree.Name = "colTPOTree";
            this.colTPOTree.OptionsColumn.AllowEdit = false;
            this.colTPOTree.OptionsColumn.AllowFocus = false;
            this.colTPOTree.OptionsColumn.ReadOnly = true;
            this.colTPOTree.Visible = true;
            this.colTPOTree.VisibleIndex = 32;
            // 
            // colPlanningConservation
            // 
            this.colPlanningConservation.Caption = "Planning Conservation";
            this.colPlanningConservation.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPlanningConservation.FieldName = "PlanningConservation";
            this.colPlanningConservation.Name = "colPlanningConservation";
            this.colPlanningConservation.OptionsColumn.AllowEdit = false;
            this.colPlanningConservation.OptionsColumn.AllowFocus = false;
            this.colPlanningConservation.OptionsColumn.ReadOnly = true;
            this.colPlanningConservation.Visible = true;
            this.colPlanningConservation.VisibleIndex = 33;
            this.colPlanningConservation.Width = 128;
            // 
            // colWildlifeDesignation
            // 
            this.colWildlifeDesignation.Caption = "Wildlife Designation";
            this.colWildlifeDesignation.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colWildlifeDesignation.FieldName = "WildlifeDesignation";
            this.colWildlifeDesignation.Name = "colWildlifeDesignation";
            this.colWildlifeDesignation.OptionsColumn.AllowEdit = false;
            this.colWildlifeDesignation.OptionsColumn.AllowFocus = false;
            this.colWildlifeDesignation.OptionsColumn.ReadOnly = true;
            this.colWildlifeDesignation.Visible = true;
            this.colWildlifeDesignation.VisibleIndex = 34;
            this.colWildlifeDesignation.Width = 114;
            // 
            // colSpecialInstructions
            // 
            this.colSpecialInstructions.Caption = "Special Instructions";
            this.colSpecialInstructions.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colSpecialInstructions.FieldName = "SpecialInstructions";
            this.colSpecialInstructions.Name = "colSpecialInstructions";
            this.colSpecialInstructions.OptionsColumn.ReadOnly = true;
            this.colSpecialInstructions.Visible = true;
            this.colSpecialInstructions.VisibleIndex = 35;
            this.colSpecialInstructions.Width = 114;
            // 
            // colPrimarySubName
            // 
            this.colPrimarySubName.Caption = "Primary Sub Name";
            this.colPrimarySubName.FieldName = "PrimarySubName";
            this.colPrimarySubName.Name = "colPrimarySubName";
            this.colPrimarySubName.OptionsColumn.AllowEdit = false;
            this.colPrimarySubName.OptionsColumn.AllowFocus = false;
            this.colPrimarySubName.OptionsColumn.ReadOnly = true;
            this.colPrimarySubName.Visible = true;
            this.colPrimarySubName.VisibleIndex = 36;
            this.colPrimarySubName.Width = 108;
            // 
            // colPrimarySubNumber
            // 
            this.colPrimarySubNumber.Caption = "Primary Sub #";
            this.colPrimarySubNumber.FieldName = "PrimarySubNumber";
            this.colPrimarySubNumber.Name = "colPrimarySubNumber";
            this.colPrimarySubNumber.OptionsColumn.AllowEdit = false;
            this.colPrimarySubNumber.OptionsColumn.AllowFocus = false;
            this.colPrimarySubNumber.OptionsColumn.ReadOnly = true;
            this.colPrimarySubNumber.Visible = true;
            this.colPrimarySubNumber.VisibleIndex = 37;
            this.colPrimarySubNumber.Width = 89;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 38;
            this.colFeederName.Width = 85;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 39;
            // 
            // colLineNumber
            // 
            this.colLineNumber.Caption = "Line #";
            this.colLineNumber.FieldName = "LineNumber";
            this.colLineNumber.Name = "colLineNumber";
            this.colLineNumber.OptionsColumn.AllowEdit = false;
            this.colLineNumber.OptionsColumn.AllowFocus = false;
            this.colLineNumber.OptionsColumn.ReadOnly = true;
            this.colLineNumber.Visible = true;
            this.colLineNumber.VisibleIndex = 40;
            // 
            // colLVSubName
            // 
            this.colLVSubName.Caption = "LV Sub Name";
            this.colLVSubName.FieldName = "LVSubName";
            this.colLVSubName.Name = "colLVSubName";
            this.colLVSubName.OptionsColumn.AllowEdit = false;
            this.colLVSubName.OptionsColumn.AllowFocus = false;
            this.colLVSubName.OptionsColumn.ReadOnly = true;
            this.colLVSubName.Visible = true;
            this.colLVSubName.VisibleIndex = 41;
            this.colLVSubName.Width = 83;
            // 
            // colLVSubNumber
            // 
            this.colLVSubNumber.Caption = "LV Sub #";
            this.colLVSubNumber.FieldName = "LVSubNumber";
            this.colLVSubNumber.Name = "colLVSubNumber";
            this.colLVSubNumber.OptionsColumn.AllowEdit = false;
            this.colLVSubNumber.OptionsColumn.AllowFocus = false;
            this.colLVSubNumber.OptionsColumn.ReadOnly = true;
            this.colLVSubNumber.Visible = true;
            this.colLVSubNumber.VisibleIndex = 42;
            // 
            // colManagedUnitNumber
            // 
            this.colManagedUnitNumber.Caption = "Managed Unit #";
            this.colManagedUnitNumber.FieldName = "ManagedUnitNumber";
            this.colManagedUnitNumber.Name = "colManagedUnitNumber";
            this.colManagedUnitNumber.OptionsColumn.AllowEdit = false;
            this.colManagedUnitNumber.OptionsColumn.AllowFocus = false;
            this.colManagedUnitNumber.OptionsColumn.ReadOnly = true;
            this.colManagedUnitNumber.Visible = true;
            this.colManagedUnitNumber.VisibleIndex = 43;
            this.colManagedUnitNumber.Width = 98;
            // 
            // colEnvironmentalRANumber
            // 
            this.colEnvironmentalRANumber.Caption = "Environmental RA #";
            this.colEnvironmentalRANumber.FieldName = "EnvironmentalRANumber";
            this.colEnvironmentalRANumber.Name = "colEnvironmentalRANumber";
            this.colEnvironmentalRANumber.OptionsColumn.AllowEdit = false;
            this.colEnvironmentalRANumber.OptionsColumn.AllowFocus = false;
            this.colEnvironmentalRANumber.OptionsColumn.ReadOnly = true;
            this.colEnvironmentalRANumber.Visible = true;
            this.colEnvironmentalRANumber.VisibleIndex = 44;
            this.colEnvironmentalRANumber.Width = 117;
            // 
            // colLandscapeImpactNumber
            // 
            this.colLandscapeImpactNumber.Caption = "Landscape Impact #";
            this.colLandscapeImpactNumber.FieldName = "LandscapeImpactNumber";
            this.colLandscapeImpactNumber.Name = "colLandscapeImpactNumber";
            this.colLandscapeImpactNumber.OptionsColumn.AllowEdit = false;
            this.colLandscapeImpactNumber.OptionsColumn.AllowFocus = false;
            this.colLandscapeImpactNumber.OptionsColumn.ReadOnly = true;
            this.colLandscapeImpactNumber.Visible = true;
            this.colLandscapeImpactNumber.VisibleIndex = 45;
            this.colLandscapeImpactNumber.Width = 119;
            // 
            // colSiteGridReference
            // 
            this.colSiteGridReference.Caption = "Site Grid Ref.";
            this.colSiteGridReference.FieldName = "SiteGridReference";
            this.colSiteGridReference.Name = "colSiteGridReference";
            this.colSiteGridReference.OptionsColumn.AllowEdit = false;
            this.colSiteGridReference.OptionsColumn.AllowFocus = false;
            this.colSiteGridReference.OptionsColumn.ReadOnly = true;
            this.colSiteGridReference.Visible = true;
            this.colSiteGridReference.VisibleIndex = 46;
            this.colSiteGridReference.Width = 85;
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Visible = true;
            this.colPermissionEmailed.VisibleIndex = 47;
            this.colPermissionEmailed.Width = 110;
            // 
            // colPostageRequired
            // 
            this.colPostageRequired.Caption = "Postage Required";
            this.colPostageRequired.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPostageRequired.FieldName = "PostageRequired";
            this.colPostageRequired.Name = "colPostageRequired";
            this.colPostageRequired.OptionsColumn.AllowEdit = false;
            this.colPostageRequired.OptionsColumn.AllowFocus = false;
            this.colPostageRequired.OptionsColumn.ReadOnly = true;
            this.colPostageRequired.Visible = true;
            this.colPostageRequired.VisibleIndex = 48;
            this.colPostageRequired.Width = 106;
            // 
            // colSentByPost
            // 
            this.colSentByPost.Caption = "Sent By Post";
            this.colSentByPost.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSentByPost.FieldName = "SentByPost";
            this.colSentByPost.Name = "colSentByPost";
            this.colSentByPost.OptionsColumn.AllowEdit = false;
            this.colSentByPost.OptionsColumn.AllowFocus = false;
            this.colSentByPost.OptionsColumn.ReadOnly = true;
            this.colSentByPost.Visible = true;
            this.colSentByPost.VisibleIndex = 49;
            this.colSentByPost.Width = 82;
            // 
            // colSentByStaffID
            // 
            this.colSentByStaffID.Caption = "Sent by Staff ID";
            this.colSentByStaffID.FieldName = "SentByStaffID";
            this.colSentByStaffID.Name = "colSentByStaffID";
            this.colSentByStaffID.OptionsColumn.AllowEdit = false;
            this.colSentByStaffID.OptionsColumn.AllowFocus = false;
            this.colSentByStaffID.OptionsColumn.ReadOnly = true;
            this.colSentByStaffID.Width = 99;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 1;
            this.colReferenceNumber.Width = 124;
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Owner Name";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.OptionsColumn.AllowEdit = false;
            this.colOwnerName.OptionsColumn.AllowFocus = false;
            this.colOwnerName.OptionsColumn.ReadOnly = true;
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 4;
            this.colOwnerName.Width = 104;
            // 
            // colOwnerSalutation
            // 
            this.colOwnerSalutation.Caption = "Owner Saluation";
            this.colOwnerSalutation.FieldName = "OwnerSalutation";
            this.colOwnerSalutation.Name = "colOwnerSalutation";
            this.colOwnerSalutation.OptionsColumn.AllowEdit = false;
            this.colOwnerSalutation.OptionsColumn.AllowFocus = false;
            this.colOwnerSalutation.OptionsColumn.ReadOnly = true;
            this.colOwnerSalutation.Visible = true;
            this.colOwnerSalutation.VisibleIndex = 9;
            this.colOwnerSalutation.Width = 100;
            // 
            // colOwnerAddress
            // 
            this.colOwnerAddress.Caption = "Owner Address";
            this.colOwnerAddress.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colOwnerAddress.FieldName = "OwnerAddress";
            this.colOwnerAddress.Name = "colOwnerAddress";
            this.colOwnerAddress.OptionsColumn.ReadOnly = true;
            this.colOwnerAddress.Visible = true;
            this.colOwnerAddress.VisibleIndex = 5;
            this.colOwnerAddress.Width = 134;
            // 
            // colOwnerPostcode
            // 
            this.colOwnerPostcode.Caption = "Owner Postcode";
            this.colOwnerPostcode.FieldName = "OwnerPostcode";
            this.colOwnerPostcode.Name = "colOwnerPostcode";
            this.colOwnerPostcode.OptionsColumn.AllowEdit = false;
            this.colOwnerPostcode.OptionsColumn.AllowFocus = false;
            this.colOwnerPostcode.OptionsColumn.ReadOnly = true;
            this.colOwnerPostcode.Visible = true;
            this.colOwnerPostcode.VisibleIndex = 6;
            this.colOwnerPostcode.Width = 100;
            // 
            // colOwnerTelephone
            // 
            this.colOwnerTelephone.Caption = "Owner Telephone";
            this.colOwnerTelephone.FieldName = "OwnerTelephone";
            this.colOwnerTelephone.Name = "colOwnerTelephone";
            this.colOwnerTelephone.OptionsColumn.AllowEdit = false;
            this.colOwnerTelephone.OptionsColumn.AllowFocus = false;
            this.colOwnerTelephone.OptionsColumn.ReadOnly = true;
            this.colOwnerTelephone.Visible = true;
            this.colOwnerTelephone.VisibleIndex = 7;
            this.colOwnerTelephone.Width = 106;
            // 
            // colOwnerEmail
            // 
            this.colOwnerEmail.Caption = "Owner Email";
            this.colOwnerEmail.FieldName = "OwnerEmail";
            this.colOwnerEmail.Name = "colOwnerEmail";
            this.colOwnerEmail.OptionsColumn.AllowEdit = false;
            this.colOwnerEmail.OptionsColumn.AllowFocus = false;
            this.colOwnerEmail.OptionsColumn.ReadOnly = true;
            this.colOwnerEmail.Visible = true;
            this.colOwnerEmail.VisibleIndex = 8;
            this.colOwnerEmail.Width = 80;
            // 
            // colOwnerTypeID
            // 
            this.colOwnerTypeID.Caption = "Owner Type ID";
            this.colOwnerTypeID.FieldName = "OwnerTypeID";
            this.colOwnerTypeID.Name = "colOwnerTypeID";
            this.colOwnerTypeID.OptionsColumn.AllowEdit = false;
            this.colOwnerTypeID.OptionsColumn.AllowFocus = false;
            this.colOwnerTypeID.OptionsColumn.ReadOnly = true;
            this.colOwnerTypeID.Width = 94;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 11;
            this.colSiteAddress.Width = 103;
            // 
            // colContractor
            // 
            this.colContractor.Caption = "Contractor";
            this.colContractor.FieldName = "Contractor";
            this.colContractor.Name = "colContractor";
            this.colContractor.OptionsColumn.AllowEdit = false;
            this.colContractor.OptionsColumn.AllowFocus = false;
            this.colContractor.OptionsColumn.ReadOnly = true;
            this.colContractor.Visible = true;
            this.colContractor.VisibleIndex = 50;
            // 
            // colArisingsChipRemove
            // 
            this.colArisingsChipRemove.Caption = "Arising Chip & Remove";
            this.colArisingsChipRemove.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colArisingsChipRemove.FieldName = "ArisingsChipRemove";
            this.colArisingsChipRemove.Name = "colArisingsChipRemove";
            this.colArisingsChipRemove.OptionsColumn.AllowEdit = false;
            this.colArisingsChipRemove.OptionsColumn.AllowFocus = false;
            this.colArisingsChipRemove.OptionsColumn.ReadOnly = true;
            this.colArisingsChipRemove.Visible = true;
            this.colArisingsChipRemove.VisibleIndex = 51;
            this.colArisingsChipRemove.Width = 129;
            // 
            // colArisingsChipOnSite
            // 
            this.colArisingsChipOnSite.Caption = "Arising Chip On Site";
            this.colArisingsChipOnSite.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colArisingsChipOnSite.FieldName = "ArisingsChipOnSite";
            this.colArisingsChipOnSite.Name = "colArisingsChipOnSite";
            this.colArisingsChipOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsChipOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsChipOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsChipOnSite.Visible = true;
            this.colArisingsChipOnSite.VisibleIndex = 52;
            this.colArisingsChipOnSite.Width = 115;
            // 
            // colArisingsStackOnSite
            // 
            this.colArisingsStackOnSite.Caption = "Arising Stack On Site";
            this.colArisingsStackOnSite.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colArisingsStackOnSite.FieldName = "ArisingsStackOnSite";
            this.colArisingsStackOnSite.Name = "colArisingsStackOnSite";
            this.colArisingsStackOnSite.OptionsColumn.AllowEdit = false;
            this.colArisingsStackOnSite.OptionsColumn.AllowFocus = false;
            this.colArisingsStackOnSite.OptionsColumn.ReadOnly = true;
            this.colArisingsStackOnSite.Visible = true;
            this.colArisingsStackOnSite.VisibleIndex = 53;
            this.colArisingsStackOnSite.Width = 120;
            // 
            // colArisingsOther
            // 
            this.colArisingsOther.Caption = "Arising Other";
            this.colArisingsOther.FieldName = "ArisingsOther";
            this.colArisingsOther.Name = "colArisingsOther";
            this.colArisingsOther.OptionsColumn.AllowEdit = false;
            this.colArisingsOther.OptionsColumn.AllowFocus = false;
            this.colArisingsOther.OptionsColumn.ReadOnly = true;
            this.colArisingsOther.Visible = true;
            this.colArisingsOther.VisibleIndex = 54;
            this.colArisingsOther.Width = 84;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colRaisedByName
            // 
            this.colRaisedByName.Caption = "Raised By Name";
            this.colRaisedByName.FieldName = "RaisedByName";
            this.colRaisedByName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRaisedByName.Name = "colRaisedByName";
            this.colRaisedByName.OptionsColumn.AllowEdit = false;
            this.colRaisedByName.OptionsColumn.AllowFocus = false;
            this.colRaisedByName.OptionsColumn.ReadOnly = true;
            this.colRaisedByName.Visible = true;
            this.colRaisedByName.VisibleIndex = 2;
            this.colRaisedByName.Width = 98;
            // 
            // colSentByStaffName
            // 
            this.colSentByStaffName.Caption = "Sent By Staff Name";
            this.colSentByStaffName.FieldName = "SentByStaffName";
            this.colSentByStaffName.Name = "colSentByStaffName";
            this.colSentByStaffName.OptionsColumn.AllowEdit = false;
            this.colSentByStaffName.OptionsColumn.AllowFocus = false;
            this.colSentByStaffName.OptionsColumn.ReadOnly = true;
            this.colSentByStaffName.Visible = true;
            this.colSentByStaffName.VisibleIndex = 55;
            this.colSentByStaffName.Width = 115;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Work Count";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 14;
            this.colLinkedActionCount.Width = 111;
            // 
            // colLinkedToDoActionCount
            // 
            this.colLinkedToDoActionCount.Caption = "Linked To Do Work";
            this.colLinkedToDoActionCount.FieldName = "LinkedToDoActionCount";
            this.colLinkedToDoActionCount.Name = "colLinkedToDoActionCount";
            this.colLinkedToDoActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedToDoActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedToDoActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedToDoActionCount.Visible = true;
            this.colLinkedToDoActionCount.VisibleIndex = 15;
            this.colLinkedToDoActionCount.Width = 110;
            // 
            // colOwnerType
            // 
            this.colOwnerType.Caption = "Owner Type";
            this.colOwnerType.FieldName = "OwnerType";
            this.colOwnerType.Name = "colOwnerType";
            this.colOwnerType.OptionsColumn.AllowEdit = false;
            this.colOwnerType.OptionsColumn.AllowFocus = false;
            this.colOwnerType.OptionsColumn.ReadOnly = true;
            this.colOwnerType.Visible = true;
            this.colOwnerType.VisibleIndex = 10;
            this.colOwnerType.Width = 80;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 3;
            this.colClientName.Width = 97;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.Width = 109;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Width = 120;
            // 
            // colNotPermissionedCount
            // 
            this.colNotPermissionedCount.Caption = "Linked Refused Work Count";
            this.colNotPermissionedCount.FieldName = "NotPermissionedCount";
            this.colNotPermissionedCount.Name = "colNotPermissionedCount";
            this.colNotPermissionedCount.OptionsColumn.AllowEdit = false;
            this.colNotPermissionedCount.OptionsColumn.AllowFocus = false;
            this.colNotPermissionedCount.OptionsColumn.ReadOnly = true;
            this.colNotPermissionedCount.Visible = true;
            this.colNotPermissionedCount.VisibleIndex = 17;
            this.colNotPermissionedCount.Width = 154;
            // 
            // colOnHoldCount
            // 
            this.colOnHoldCount.Caption = "Linked On-Hold Work Count";
            this.colOnHoldCount.FieldName = "OnHoldCount";
            this.colOnHoldCount.Name = "colOnHoldCount";
            this.colOnHoldCount.OptionsColumn.AllowEdit = false;
            this.colOnHoldCount.OptionsColumn.AllowFocus = false;
            this.colOnHoldCount.OptionsColumn.ReadOnly = true;
            this.colOnHoldCount.Visible = true;
            this.colOnHoldCount.VisibleIndex = 16;
            this.colOnHoldCount.Width = 153;
            // 
            // colPermissionedCount
            // 
            this.colPermissionedCount.Caption = "Linked Permissioned Work Count";
            this.colPermissionedCount.FieldName = "PermissionedCount";
            this.colPermissionedCount.Name = "colPermissionedCount";
            this.colPermissionedCount.OptionsColumn.AllowEdit = false;
            this.colPermissionedCount.OptionsColumn.AllowFocus = false;
            this.colPermissionedCount.OptionsColumn.ReadOnly = true;
            this.colPermissionedCount.Visible = true;
            this.colPermissionedCount.VisibleIndex = 18;
            this.colPermissionedCount.Width = 176;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Created By Staff ID";
            this.gridColumn5.FieldName = "CreatedByStaffID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 114;
            // 
            // colLandownerRestrictedCut1
            // 
            this.colLandownerRestrictedCut1.Caption = "Landowner Restricted Cut";
            this.colLandownerRestrictedCut1.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colLandownerRestrictedCut1.FieldName = "LandownerRestrictedCut";
            this.colLandownerRestrictedCut1.Name = "colLandownerRestrictedCut1";
            this.colLandownerRestrictedCut1.OptionsColumn.AllowEdit = false;
            this.colLandownerRestrictedCut1.OptionsColumn.AllowFocus = false;
            this.colLandownerRestrictedCut1.OptionsColumn.ReadOnly = true;
            this.colLandownerRestrictedCut1.Visible = true;
            this.colLandownerRestrictedCut1.VisibleIndex = 27;
            this.colLandownerRestrictedCut1.Width = 144;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.Visible = true;
            this.colPoleNumber1.VisibleIndex = 48;
            this.colPoleNumber1.Width = 116;
            // 
            // repositoryItemTextEditNumeric0DP
            // 
            this.repositoryItemTextEditNumeric0DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric0DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP.Name = "repositoryItemTextEditNumeric0DP";
            // 
            // ContractYearSpinEdit
            // 
            this.ContractYearSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ContractYear", true));
            this.ContractYearSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ContractYearSpinEdit.Location = new System.Drawing.Point(141, 191);
            this.ContractYearSpinEdit.MenuManager = this.barManager1;
            this.ContractYearSpinEdit.Name = "ContractYearSpinEdit";
            this.ContractYearSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ContractYearSpinEdit.Properties.Mask.EditMask = "###0";
            this.ContractYearSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ContractYearSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.ContractYearSpinEdit.Size = new System.Drawing.Size(307, 20);
            this.ContractYearSpinEdit.StyleController = this.dataLayoutControl1;
            this.ContractYearSpinEdit.TabIndex = 4;
            // 
            // CopyAddressButton2
            // 
            this.CopyAddressButton2.Location = new System.Drawing.Point(709, 297);
            this.CopyAddressButton2.Name = "CopyAddressButton2";
            this.CopyAddressButton2.Size = new System.Drawing.Size(154, 22);
            this.CopyAddressButton2.StyleController = this.dataLayoutControl1;
            this.CopyAddressButton2.TabIndex = 80;
            this.CopyAddressButton2.Text = "Copy Address to Reported By";
            this.CopyAddressButton2.Click += new System.EventHandler(this.CopyAddressButton2_Click);
            // 
            // CopyAddressButton1
            // 
            this.CopyAddressButton1.Location = new System.Drawing.Point(297, 297);
            this.CopyAddressButton1.Name = "CopyAddressButton1";
            this.CopyAddressButton1.Size = new System.Drawing.Size(135, 22);
            this.CopyAddressButton1.StyleController = this.dataLayoutControl1;
            this.CopyAddressButton1.TabIndex = 79;
            this.CopyAddressButton1.Text = "Copy Address to Incident";
            this.CopyAddressButton1.Click += new System.EventHandler(this.CopyAddressButton1_Click);
            // 
            // ReactiveCategoryIDGridLookUpEdit
            // 
            this.ReactiveCategoryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReactiveCategoryID", true));
            this.ReactiveCategoryIDGridLookUpEdit.EditValue = "";
            this.ReactiveCategoryIDGridLookUpEdit.Location = new System.Drawing.Point(567, 181);
            this.ReactiveCategoryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ReactiveCategoryIDGridLookUpEdit.Name = "ReactiveCategoryIDGridLookUpEdit";
            this.ReactiveCategoryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ReactiveCategoryIDGridLookUpEdit.Properties.DataSource = this.sp07405UTReactiveCategoryTypesWithBlankBindingSource;
            this.ReactiveCategoryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ReactiveCategoryIDGridLookUpEdit.Properties.NullText = "";
            this.ReactiveCategoryIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.ReactiveCategoryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ReactiveCategoryIDGridLookUpEdit.Size = new System.Drawing.Size(308, 20);
            this.ReactiveCategoryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCategoryIDGridLookUpEdit.TabIndex = 15;
            // 
            // sp07405UTReactiveCategoryTypesWithBlankBindingSource
            // 
            this.sp07405UTReactiveCategoryTypesWithBlankBindingSource.DataMember = "sp07405_UT_Reactive_Category_Types_With_Blank";
            this.sp07405UTReactiveCategoryTypesWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.colOrder});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule6.ApplyToRow = true;
            gridFormatRule6.Column = this.gridColumn2;
            gridFormatRule6.Name = "Format0";
            formatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue6.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue6.Value1 = 0;
            gridFormatRule6.Rule = formatConditionRuleValue6;
            this.gridView4.FormatRules.Add(gridFormatRule6);
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Reactive Category";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // IncidentPostcodeTextEdit
            // 
            this.IncidentPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "IncidentPostcode", true));
            this.IncidentPostcodeTextEdit.Location = new System.Drawing.Point(579, 415);
            this.IncidentPostcodeTextEdit.MenuManager = this.barManager1;
            this.IncidentPostcodeTextEdit.Name = "IncidentPostcodeTextEdit";
            this.IncidentPostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IncidentPostcodeTextEdit, true);
            this.IncidentPostcodeTextEdit.Size = new System.Drawing.Size(284, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IncidentPostcodeTextEdit, optionsSpelling20);
            this.IncidentPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.IncidentPostcodeTextEdit.TabIndex = 0;
            // 
            // IncidentAddressMemoEdit
            // 
            this.IncidentAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "IncidentAddress", true));
            this.IncidentAddressMemoEdit.Location = new System.Drawing.Point(579, 323);
            this.IncidentAddressMemoEdit.MenuManager = this.barManager1;
            this.IncidentAddressMemoEdit.Name = "IncidentAddressMemoEdit";
            this.IncidentAddressMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IncidentAddressMemoEdit, true);
            this.IncidentAddressMemoEdit.Size = new System.Drawing.Size(284, 88);
            this.scSpellChecker.SetSpellCheckerOptions(this.IncidentAddressMemoEdit, optionsSpelling1);
            this.IncidentAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.IncidentAddressMemoEdit.TabIndex = 23;
            // 
            // ReactiveRemarksMemoEdit
            // 
            this.ReactiveRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReactiveRemarks", true));
            this.ReactiveRemarksMemoEdit.Location = new System.Drawing.Point(141, 518);
            this.ReactiveRemarksMemoEdit.MenuManager = this.barManager1;
            this.ReactiveRemarksMemoEdit.Name = "ReactiveRemarksMemoEdit";
            this.ReactiveRemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReactiveRemarksMemoEdit, true);
            this.ReactiveRemarksMemoEdit.Size = new System.Drawing.Size(734, 47);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReactiveRemarksMemoEdit, optionsSpelling2);
            this.ReactiveRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveRemarksMemoEdit.TabIndex = 1;
            // 
            // ReportedByEmailTextEdit
            // 
            this.ReportedByEmailTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReportedByEmail", true));
            this.ReportedByEmailTextEdit.Location = new System.Drawing.Point(153, 462);
            this.ReportedByEmailTextEdit.MenuManager = this.barManager1;
            this.ReportedByEmailTextEdit.Name = "ReportedByEmailTextEdit";
            this.ReportedByEmailTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByEmailTextEdit, true);
            this.ReportedByEmailTextEdit.Size = new System.Drawing.Size(279, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByEmailTextEdit, optionsSpelling3);
            this.ReportedByEmailTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByEmailTextEdit.TabIndex = 22;
            // 
            // ReportedByTelephoneTextEdit
            // 
            this.ReportedByTelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReportedByTelephone", true));
            this.ReportedByTelephoneTextEdit.Location = new System.Drawing.Point(153, 438);
            this.ReportedByTelephoneTextEdit.MenuManager = this.barManager1;
            this.ReportedByTelephoneTextEdit.Name = "ReportedByTelephoneTextEdit";
            this.ReportedByTelephoneTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByTelephoneTextEdit, true);
            this.ReportedByTelephoneTextEdit.Size = new System.Drawing.Size(279, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByTelephoneTextEdit, optionsSpelling4);
            this.ReportedByTelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByTelephoneTextEdit.TabIndex = 21;
            // 
            // ReportedByPostcodeTextEdit
            // 
            this.ReportedByPostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReportedByPostcode", true));
            this.ReportedByPostcodeTextEdit.Location = new System.Drawing.Point(153, 414);
            this.ReportedByPostcodeTextEdit.MenuManager = this.barManager1;
            this.ReportedByPostcodeTextEdit.Name = "ReportedByPostcodeTextEdit";
            this.ReportedByPostcodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByPostcodeTextEdit, true);
            this.ReportedByPostcodeTextEdit.Size = new System.Drawing.Size(279, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByPostcodeTextEdit, optionsSpelling5);
            this.ReportedByPostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByPostcodeTextEdit.TabIndex = 20;
            // 
            // ReportedByAddressMemoEdit
            // 
            this.ReportedByAddressMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReportedByAddress", true));
            this.ReportedByAddressMemoEdit.Location = new System.Drawing.Point(153, 347);
            this.ReportedByAddressMemoEdit.MenuManager = this.barManager1;
            this.ReportedByAddressMemoEdit.Name = "ReportedByAddressMemoEdit";
            this.ReportedByAddressMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByAddressMemoEdit, true);
            this.ReportedByAddressMemoEdit.Size = new System.Drawing.Size(279, 63);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByAddressMemoEdit, optionsSpelling6);
            this.ReportedByAddressMemoEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByAddressMemoEdit.TabIndex = 19;
            // 
            // ReportedByNameTextEdit
            // 
            this.ReportedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ReportedByName", true));
            this.ReportedByNameTextEdit.Location = new System.Drawing.Point(153, 323);
            this.ReportedByNameTextEdit.MenuManager = this.barManager1;
            this.ReportedByNameTextEdit.Name = "ReportedByNameTextEdit";
            this.ReportedByNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportedByNameTextEdit, true);
            this.ReportedByNameTextEdit.Size = new System.Drawing.Size(279, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportedByNameTextEdit, optionsSpelling7);
            this.ReportedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportedByNameTextEdit.TabIndex = 18;
            // 
            // btnCalculateTotals
            // 
            this.btnCalculateTotals.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCalculateTotals.ImageOptions.Image")));
            this.btnCalculateTotals.Location = new System.Drawing.Point(785, 83);
            this.btnCalculateTotals.Name = "btnCalculateTotals";
            this.btnCalculateTotals.Size = new System.Drawing.Size(106, 22);
            this.btnCalculateTotals.StyleController = this.dataLayoutControl1;
            this.btnCalculateTotals.TabIndex = 69;
            this.btnCalculateTotals.Text = "Calculate Total";
            this.btnCalculateTotals.Click += new System.EventHandler(this.btnCalculateTotals_Click);
            // 
            // QuoteApprovedDateDateEdit
            // 
            this.QuoteApprovedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "QuoteApprovedDate", true));
            this.QuoteApprovedDateDateEdit.EditValue = null;
            this.QuoteApprovedDateDateEdit.Location = new System.Drawing.Point(567, 229);
            this.QuoteApprovedDateDateEdit.MenuManager = this.barManager1;
            this.QuoteApprovedDateDateEdit.Name = "QuoteApprovedDateDateEdit";
            this.QuoteApprovedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteApprovedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteApprovedDateDateEdit.Properties.Mask.EditMask = "g";
            this.QuoteApprovedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteApprovedDateDateEdit.Size = new System.Drawing.Size(308, 20);
            this.QuoteApprovedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.QuoteApprovedDateDateEdit.TabIndex = 17;
            // 
            // QuoteReceivedDateDateEdit
            // 
            this.QuoteReceivedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "QuoteReceivedDate", true));
            this.QuoteReceivedDateDateEdit.EditValue = null;
            this.QuoteReceivedDateDateEdit.Location = new System.Drawing.Point(141, 229);
            this.QuoteReceivedDateDateEdit.MenuManager = this.barManager1;
            this.QuoteReceivedDateDateEdit.Name = "QuoteReceivedDateDateEdit";
            this.QuoteReceivedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteReceivedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QuoteReceivedDateDateEdit.Properties.Mask.EditMask = "g";
            this.QuoteReceivedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.QuoteReceivedDateDateEdit.Size = new System.Drawing.Size(309, 20);
            this.QuoteReceivedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.QuoteReceivedDateDateEdit.TabIndex = 14;
            // 
            // ExchequerNumberTextEdit
            // 
            this.ExchequerNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ExchequerNumber", true));
            this.ExchequerNumberTextEdit.Location = new System.Drawing.Point(567, 205);
            this.ExchequerNumberTextEdit.MenuManager = this.barManager1;
            this.ExchequerNumberTextEdit.Name = "ExchequerNumberTextEdit";
            this.ExchequerNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ExchequerNumberTextEdit, true);
            this.ExchequerNumberTextEdit.Size = new System.Drawing.Size(308, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ExchequerNumberTextEdit, optionsSpelling8);
            this.ExchequerNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ExchequerNumberTextEdit.TabIndex = 16;
            // 
            // ClientPONumberTextEdit
            // 
            this.ClientPONumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ClientPONumber", true));
            this.ClientPONumberTextEdit.Location = new System.Drawing.Point(141, 205);
            this.ClientPONumberTextEdit.MenuManager = this.barManager1;
            this.ClientPONumberTextEdit.Name = "ClientPONumberTextEdit";
            this.ClientPONumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientPONumberTextEdit, true);
            this.ClientPONumberTextEdit.Size = new System.Drawing.Size(309, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientPONumberTextEdit, optionsSpelling9);
            this.ClientPONumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientPONumberTextEdit.TabIndex = 13;
            this.ClientPONumberTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientPONumberTextEdit_Validating);
            // 
            // TotalIncomeSpinEdit
            // 
            this.TotalIncomeSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "TotalIncome", true));
            this.TotalIncomeSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalIncomeSpinEdit.Location = new System.Drawing.Point(567, 83);
            this.TotalIncomeSpinEdit.MenuManager = this.barManager1;
            this.TotalIncomeSpinEdit.Name = "TotalIncomeSpinEdit";
            this.TotalIncomeSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalIncomeSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalIncomeSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalIncomeSpinEdit.Properties.ReadOnly = true;
            this.TotalIncomeSpinEdit.Size = new System.Drawing.Size(214, 20);
            this.TotalIncomeSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalIncomeSpinEdit.TabIndex = 5;
            // 
            // TotalCostSpinEdit
            // 
            this.TotalCostSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "TotalCost", true));
            this.TotalCostSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TotalCostSpinEdit.Location = new System.Drawing.Point(125, 83);
            this.TotalCostSpinEdit.MenuManager = this.barManager1;
            this.TotalCostSpinEdit.Name = "TotalCostSpinEdit";
            this.TotalCostSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.TotalCostSpinEdit.Properties.Mask.EditMask = "c";
            this.TotalCostSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostSpinEdit.Properties.ReadOnly = true;
            this.TotalCostSpinEdit.Size = new System.Drawing.Size(325, 20);
            this.TotalCostSpinEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostSpinEdit.TabIndex = 4;
            // 
            // WorkspaceIDTextEdit
            // 
            this.WorkspaceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "WorkspaceID", true));
            this.WorkspaceIDTextEdit.Location = new System.Drawing.Point(158, 243);
            this.WorkspaceIDTextEdit.MenuManager = this.barManager1;
            this.WorkspaceIDTextEdit.Name = "WorkspaceIDTextEdit";
            this.WorkspaceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkspaceIDTextEdit, true);
            this.WorkspaceIDTextEdit.Size = new System.Drawing.Size(702, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkspaceIDTextEdit, optionsSpelling10);
            this.WorkspaceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkspaceIDTextEdit.TabIndex = 59;
            // 
            // LastMapRecordIDTextEdit
            // 
            this.LastMapRecordIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "LastMapRecordID", true));
            this.LastMapRecordIDTextEdit.Location = new System.Drawing.Point(113, 154);
            this.LastMapRecordIDTextEdit.MenuManager = this.barManager1;
            this.LastMapRecordIDTextEdit.Name = "LastMapRecordIDTextEdit";
            this.LastMapRecordIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LastMapRecordIDTextEdit, true);
            this.LastMapRecordIDTextEdit.Size = new System.Drawing.Size(783, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LastMapRecordIDTextEdit, optionsSpelling11);
            this.LastMapRecordIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LastMapRecordIDTextEdit.TabIndex = 57;
            // 
            // btnLoadMapWithSurvey
            // 
            this.btnLoadMapWithSurvey.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadMapWithSurvey.ImageOptions.Image")));
            this.btnLoadMapWithSurvey.Location = new System.Drawing.Point(136, 319);
            this.btnLoadMapWithSurvey.Name = "btnLoadMapWithSurvey";
            this.btnLoadMapWithSurvey.Size = new System.Drawing.Size(134, 22);
            this.btnLoadMapWithSurvey.StyleController = this.dataLayoutControl1;
            this.btnLoadMapWithSurvey.TabIndex = 62;
            this.btnLoadMapWithSurvey.Text = "Load Survey into Map";
            this.btnLoadMapWithSurvey.Click += new System.EventHandler(this.btnLoadMapWithSurvey_Click);
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(28, 319);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(104, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save Changes";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // WorkspaceNameTextEdit
            // 
            this.WorkspaceNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "WorkspaceName", true));
            this.WorkspaceNameTextEdit.Location = new System.Drawing.Point(779, 214);
            this.WorkspaceNameTextEdit.MenuManager = this.barManager1;
            this.WorkspaceNameTextEdit.Name = "WorkspaceNameTextEdit";
            this.WorkspaceNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkspaceNameTextEdit, true);
            this.WorkspaceNameTextEdit.Size = new System.Drawing.Size(96, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkspaceNameTextEdit, optionsSpelling12);
            this.WorkspaceNameTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkspaceNameTextEdit.TabIndex = 9;
            // 
            // LastMapRecordDescriptionButtonEdit
            // 
            this.LastMapRecordDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "LastMapRecordDescription", true));
            this.LastMapRecordDescriptionButtonEdit.Location = new System.Drawing.Point(153, 273);
            this.LastMapRecordDescriptionButtonEdit.MenuManager = this.barManager1;
            this.LastMapRecordDescriptionButtonEdit.Name = "LastMapRecordDescriptionButtonEdit";
            this.LastMapRecordDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LastMapRecordDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LastMapRecordDescriptionButtonEdit.Size = new System.Drawing.Size(283, 20);
            this.LastMapRecordDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.LastMapRecordDescriptionButtonEdit.TabIndex = 7;
            this.LastMapRecordDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LastMapRecordDescriptionButtonEdit_ButtonClick);
            // 
            // MapStartTypeIDGridLookUpEdit
            // 
            this.MapStartTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "MapStartTypeID", true));
            this.MapStartTypeIDGridLookUpEdit.Location = new System.Drawing.Point(153, 249);
            this.MapStartTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.MapStartTypeIDGridLookUpEdit.Name = "MapStartTypeIDGridLookUpEdit";
            this.MapStartTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MapStartTypeIDGridLookUpEdit.Properties.DataSource = this.sp07105UTMapStartTypesBindingSource;
            this.MapStartTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.MapStartTypeIDGridLookUpEdit.Properties.NullText = "";
            this.MapStartTypeIDGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.MapStartTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.MapStartTypeIDGridLookUpEdit.Size = new System.Drawing.Size(283, 20);
            this.MapStartTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.MapStartTypeIDGridLookUpEdit.TabIndex = 6;
            // 
            // sp07105UTMapStartTypesBindingSource
            // 
            this.sp07105UTMapStartTypesBindingSource.DataMember = "sp07105_UT_Map_Start_Types";
            this.sp07105UTMapStartTypesBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colRecordOrder});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Map Start Position Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 193;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // ReactiveCheckEdit
            // 
            this.ReactiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "Reactive", true));
            this.ReactiveCheckEdit.Location = new System.Drawing.Point(141, 181);
            this.ReactiveCheckEdit.MenuManager = this.barManager1;
            this.ReactiveCheckEdit.Name = "ReactiveCheckEdit";
            this.ReactiveCheckEdit.Properties.Caption = "(Tick if Reactive)";
            this.ReactiveCheckEdit.Properties.ValueChecked = 1;
            this.ReactiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ReactiveCheckEdit.Size = new System.Drawing.Size(309, 19);
            this.ReactiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ReactiveCheckEdit.TabIndex = 12;
            this.ReactiveCheckEdit.Validated += new System.EventHandler(this.ReactiveCheckEdit_Validated);
            // 
            // SurveyorIDGridLookUpEdit
            // 
            this.SurveyorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "SurveyorID", true));
            this.SurveyorIDGridLookUpEdit.Location = new System.Drawing.Point(567, 35);
            this.SurveyorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SurveyorIDGridLookUpEdit.Name = "SurveyorIDGridLookUpEdit";
            this.SurveyorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SurveyorIDGridLookUpEdit.Properties.DataSource = this.sp07328UTSurveyorListWithBlankBindingSource;
            this.SurveyorIDGridLookUpEdit.Properties.DisplayMember = "DisplayName";
            this.SurveyorIDGridLookUpEdit.Properties.NullText = "";
            this.SurveyorIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.SurveyorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.SurveyorIDGridLookUpEdit.Properties.ValueMember = "StaffID";
            this.SurveyorIDGridLookUpEdit.Size = new System.Drawing.Size(324, 20);
            this.SurveyorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SurveyorIDGridLookUpEdit.TabIndex = 1;
            this.SurveyorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SurveyorIDGridLookUpEdit_Validating);
            // 
            // sp07328UTSurveyorListWithBlankBindingSource
            // 
            this.sp07328UTSurveyorListWithBlankBindingSource.DataMember = "sp07328_UT_Surveyor_List_With_Blank";
            this.sp07328UTSurveyorListWithBlankBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Active";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn14.FieldName = "Active";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 1;
            this.gridColumn14.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Surveyor Surname: Forename";
            this.gridColumn15.FieldName = "DisplayName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 225;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Email";
            this.gridColumn16.FieldName = "Email";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Width = 192;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Forename";
            this.gridColumn17.FieldName = "Forename";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 178;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Network ID";
            this.gridColumn18.FieldName = "NetworkID";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 180;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Staff ID";
            this.gridColumn19.FieldName = "StaffID";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 59;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Staff Name";
            this.gridColumn20.FieldName = "StaffName";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 231;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Surname";
            this.gridColumn21.FieldName = "Surname";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 191;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "User Type";
            this.gridColumn22.FieldName = "UserType";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Width = 223;
            // 
            // SurveyStatusIDGridLookUpEdit
            // 
            this.SurveyStatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "SurveyStatusID", true));
            this.SurveyStatusIDGridLookUpEdit.Location = new System.Drawing.Point(567, 59);
            this.SurveyStatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SurveyStatusIDGridLookUpEdit.Name = "SurveyStatusIDGridLookUpEdit";
            this.SurveyStatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SurveyStatusIDGridLookUpEdit.Properties.DataSource = this.sp07106UTSurveyStatusesBindingSource;
            this.SurveyStatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SurveyStatusIDGridLookUpEdit.Properties.NullText = "";
            this.SurveyStatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.SurveyStatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SurveyStatusIDGridLookUpEdit.Size = new System.Drawing.Size(324, 20);
            this.SurveyStatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SurveyStatusIDGridLookUpEdit.TabIndex = 3;
            // 
            // sp07106UTSurveyStatusesBindingSource
            // 
            this.sp07106UTSurveyStatusesBindingSource.DataMember = "sp07106_UT_Survey_Statuses";
            this.sp07106UTSurveyStatusesBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.gridColumn1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID2
            // 
            this.colID2.Caption = "ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Survey Status";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Order";
            this.gridColumn1.FieldName = "RecordOrder";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // SurveyDateDateEdit
            // 
            this.SurveyDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "SurveyDate", true));
            this.SurveyDateDateEdit.EditValue = null;
            this.SurveyDateDateEdit.Location = new System.Drawing.Point(125, 59);
            this.SurveyDateDateEdit.MenuManager = this.barManager1;
            this.SurveyDateDateEdit.Name = "SurveyDateDateEdit";
            this.SurveyDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SurveyDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SurveyDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.SurveyDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.SurveyDateDateEdit.Size = new System.Drawing.Size(325, 20);
            this.SurveyDateDateEdit.StyleController = this.dataLayoutControl1;
            this.SurveyDateDateEdit.TabIndex = 2;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(93, 83);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(803, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling13);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 36;
            // 
            // LastMapYTextEdit
            // 
            this.LastMapYTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "LastMapY", true));
            this.LastMapYTextEdit.Location = new System.Drawing.Point(779, 190);
            this.LastMapYTextEdit.MenuManager = this.barManager1;
            this.LastMapYTextEdit.Name = "LastMapYTextEdit";
            this.LastMapYTextEdit.Properties.Mask.EditMask = "n10";
            this.LastMapYTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LastMapYTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LastMapYTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LastMapYTextEdit, true);
            this.LastMapYTextEdit.Size = new System.Drawing.Size(96, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LastMapYTextEdit, optionsSpelling14);
            this.LastMapYTextEdit.StyleController = this.dataLayoutControl1;
            this.LastMapYTextEdit.TabIndex = 42;
            // 
            // LastMapXTextEdit
            // 
            this.LastMapXTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "LastMapX", true));
            this.LastMapXTextEdit.Location = new System.Drawing.Point(558, 191);
            this.LastMapXTextEdit.MenuManager = this.barManager1;
            this.LastMapXTextEdit.Name = "LastMapXTextEdit";
            this.LastMapXTextEdit.Properties.Mask.EditMask = "n10";
            this.LastMapXTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.LastMapXTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.LastMapXTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LastMapXTextEdit, true);
            this.LastMapXTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LastMapXTextEdit, optionsSpelling15);
            this.LastMapXTextEdit.StyleController = this.dataLayoutControl1;
            this.LastMapXTextEdit.TabIndex = 7;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Pole(s) To Survey", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Survey Pole", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Pole(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Refresh Data", "refresh")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(40, 388);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditNumeric2DP,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditShortDate,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(823, 202);
            this.gridControl1.TabIndex = 11;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07107UTSurveyItemLinkedSurveyedPolesBindingSource
            // 
            this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource.DataMember = "sp07107_UT_Survey_Item_Linked_Surveyed_Poles";
            this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks,
            this.colGUID,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.colCircuitNumber,
            this.colCircuitName,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colDeferredUnitDescriptior,
            this.colValue,
            this.colIsBaseClimbable,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colClearanceDistanceUnder,
            this.colSpanResilient,
            this.colIvyOnPole,
            this.colCreatedByStaffID,
            this.colHeldUpType,
            this.colLinearCutLength,
            this.colShutdownHours,
            this.colDeferredEstimatedCuttingHours,
            this.colClearanceDistanceAbove});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowFilterEditor = false;
            this.gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView1.OptionsFilter.AllowMRUFilterList = false;
            this.gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Surveyed Date";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 4;
            this.colSurveyedDate.Width = 103;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "g";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 7;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 8;
            this.colInfestationRate.Width = 100;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 10;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 13;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 14;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance (Side)";
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 18;
            this.colClearanceDistance.Width = 144;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 21;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 22;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 23;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 5 M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 24;
            this.colTreeWithin3Meters.Width = 96;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 25;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 43;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 2;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Visible = true;
            this.colPoleLastInspectionDate.VisibleIndex = 30;
            this.colPoleLastInspectionDate.Width = 94;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Visible = true;
            this.colPoleNextInspectionDate.VisibleIndex = 31;
            this.colPoleNextInspectionDate.Width = 97;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 29;
            this.colIsTransformer.Width = 80;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 1;
            this.colCircuitNumber.Width = 104;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 0;
            this.colCircuitName.Width = 94;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 6;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 3;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID
            // 
            this.colSurveyStatusID.Caption = "Survey Status ID";
            this.colSurveyStatusID.FieldName = "SurveyStatusID";
            this.colSurveyStatusID.Name = "colSurveyStatusID";
            this.colSurveyStatusID.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 15;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 32;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 33;
            this.colDeferredUnits.Width = 91;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 34;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colValue
            // 
            this.colValue.Caption = "Value";
            this.colValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Visible = true;
            this.colValue.VisibleIndex = 5;
            this.colValue.Width = 56;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 26;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 38;
            this.colRevisitDate.Width = 79;
            // 
            // repositoryItemTextEditShortDate
            // 
            this.repositoryItemTextEditShortDate.AutoHeight = false;
            this.repositoryItemTextEditShortDate.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate.Name = "repositoryItemTextEditShortDate";
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.Visible = true;
            this.colMapID.VisibleIndex = 39;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.Visible = true;
            this.colSpanInvoiced.VisibleIndex = 40;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colInvoiceNumber.Visible = true;
            this.colInvoiceNumber.VisibleIndex = 41;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.ColumnEdit = this.repositoryItemTextEditShortDate;
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Visible = true;
            this.colDateInvoiced.VisibleIndex = 42;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 35;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 37;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 16;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 17;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.Visible = true;
            this.colAccessMapPath.VisibleIndex = 44;
            this.colAccessMapPath.Width = 100;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.Visible = true;
            this.colTrafficMapPath.VisibleIndex = 45;
            this.colTrafficMapPath.Width = 100;
            // 
            // colClearanceDistanceUnder
            // 
            this.colClearanceDistanceUnder.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder.Name = "colClearanceDistanceUnder";
            this.colClearanceDistanceUnder.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder.Visible = true;
            this.colClearanceDistanceUnder.VisibleIndex = 19;
            this.colClearanceDistanceUnder.Width = 153;
            // 
            // colSpanResilient
            // 
            this.colSpanResilient.Caption = "Is Resilient";
            this.colSpanResilient.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colSpanResilient.FieldName = "SpanResilient";
            this.colSpanResilient.Name = "colSpanResilient";
            this.colSpanResilient.OptionsColumn.AllowEdit = false;
            this.colSpanResilient.OptionsColumn.AllowFocus = false;
            this.colSpanResilient.OptionsColumn.ReadOnly = true;
            this.colSpanResilient.Visible = true;
            this.colSpanResilient.VisibleIndex = 27;
            // 
            // colIvyOnPole
            // 
            this.colIvyOnPole.Caption = "Ivy on Pole";
            this.colIvyOnPole.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIvyOnPole.FieldName = "IvyOnPole";
            this.colIvyOnPole.Name = "colIvyOnPole";
            this.colIvyOnPole.OptionsColumn.AllowEdit = false;
            this.colIvyOnPole.OptionsColumn.AllowFocus = false;
            this.colIvyOnPole.OptionsColumn.ReadOnly = true;
            this.colIvyOnPole.Visible = true;
            this.colIvyOnPole.VisibleIndex = 28;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 12;
            this.colHeldUpType.Width = 83;
            // 
            // colLinearCutLength
            // 
            this.colLinearCutLength.Caption = "Linear Cut Length";
            this.colLinearCutLength.FieldName = "LinearCutLength";
            this.colLinearCutLength.Name = "colLinearCutLength";
            this.colLinearCutLength.OptionsColumn.AllowEdit = false;
            this.colLinearCutLength.OptionsColumn.AllowFocus = false;
            this.colLinearCutLength.OptionsColumn.ReadOnly = true;
            this.colLinearCutLength.Visible = true;
            this.colLinearCutLength.VisibleIndex = 9;
            this.colLinearCutLength.Width = 104;
            // 
            // colShutdownHours
            // 
            this.colShutdownHours.Caption = "Shutdown Hours";
            this.colShutdownHours.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colShutdownHours.FieldName = "ShutdownHours";
            this.colShutdownHours.Name = "colShutdownHours";
            this.colShutdownHours.OptionsColumn.AllowEdit = false;
            this.colShutdownHours.OptionsColumn.AllowFocus = false;
            this.colShutdownHours.OptionsColumn.ReadOnly = true;
            this.colShutdownHours.Visible = true;
            this.colShutdownHours.VisibleIndex = 11;
            this.colShutdownHours.Width = 98;
            // 
            // colDeferredEstimatedCuttingHours
            // 
            this.colDeferredEstimatedCuttingHours.Caption = "Deferred Est. Cutting Hours";
            this.colDeferredEstimatedCuttingHours.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colDeferredEstimatedCuttingHours.FieldName = "DeferredEstimatedCuttingHours";
            this.colDeferredEstimatedCuttingHours.Name = "colDeferredEstimatedCuttingHours";
            this.colDeferredEstimatedCuttingHours.OptionsColumn.AllowEdit = false;
            this.colDeferredEstimatedCuttingHours.OptionsColumn.AllowFocus = false;
            this.colDeferredEstimatedCuttingHours.OptionsColumn.ReadOnly = true;
            this.colDeferredEstimatedCuttingHours.Visible = true;
            this.colDeferredEstimatedCuttingHours.VisibleIndex = 36;
            this.colDeferredEstimatedCuttingHours.Width = 153;
            // 
            // colClearanceDistanceAbove
            // 
            this.colClearanceDistanceAbove.Caption = "Clearance Distance (Above)";
            this.colClearanceDistanceAbove.FieldName = "ClearanceDistanceAbove";
            this.colClearanceDistanceAbove.Name = "colClearanceDistanceAbove";
            this.colClearanceDistanceAbove.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceAbove.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceAbove.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceAbove.Visible = true;
            this.colClearanceDistanceAbove.VisibleIndex = 20;
            this.colClearanceDistanceAbove.Width = 153;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07103UTSurveyItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(125, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(178, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // SurveyIDTextEdit
            // 
            this.SurveyIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "SurveyID", true));
            this.SurveyIDTextEdit.Location = new System.Drawing.Point(89, 236);
            this.SurveyIDTextEdit.MenuManager = this.barManager1;
            this.SurveyIDTextEdit.Name = "SurveyIDTextEdit";
            this.SurveyIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyIDTextEdit, true);
            this.SurveyIDTextEdit.Size = new System.Drawing.Size(807, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyIDTextEdit, optionsSpelling16);
            this.SurveyIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyIDTextEdit.TabIndex = 4;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(28, 181);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(847, 421);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling17);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 2;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07103UTSurveyItemBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(125, 35);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(325, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 0;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // ItemForSurveyID
            // 
            this.ItemForSurveyID.Control = this.SurveyIDTextEdit;
            this.ItemForSurveyID.CustomizationFormText = "Survey ID:";
            this.ItemForSurveyID.Location = new System.Drawing.Point(0, 224);
            this.ItemForSurveyID.Name = "ItemForSurveyID";
            this.ItemForSurveyID.Size = new System.Drawing.Size(888, 24);
            this.ItemForSurveyID.Text = "Survey ID:";
            this.ItemForSurveyID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLastMapRecordID
            // 
            this.ItemForLastMapRecordID.Control = this.LastMapRecordIDTextEdit;
            this.ItemForLastMapRecordID.CustomizationFormText = "Last Map Record ID:";
            this.ItemForLastMapRecordID.Location = new System.Drawing.Point(0, 142);
            this.ItemForLastMapRecordID.Name = "ItemForLastMapRecordID";
            this.ItemForLastMapRecordID.Size = new System.Drawing.Size(888, 24);
            this.ItemForLastMapRecordID.Text = "Last Map Record ID:";
            this.ItemForLastMapRecordID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 71);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(888, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForWorkspaceID
            // 
            this.ItemForWorkspaceID.Control = this.WorkspaceIDTextEdit;
            this.ItemForWorkspaceID.CustomizationFormText = "Workspace ID:";
            this.ItemForWorkspaceID.Location = new System.Drawing.Point(0, 48);
            this.ItemForWorkspaceID.Name = "ItemForWorkspaceID";
            this.ItemForWorkspaceID.Size = new System.Drawing.Size(816, 24);
            this.ItemForWorkspaceID.Text = "Workspace ID:";
            this.ItemForWorkspaceID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(246, 68);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(294, 26);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLastMapX
            // 
            this.ItemForLastMapX.Control = this.LastMapXTextEdit;
            this.ItemForLastMapX.CustomizationFormText = "Last Map X:";
            this.ItemForLastMapX.Location = new System.Drawing.Point(417, 10);
            this.ItemForLastMapX.Name = "ItemForLastMapX";
            this.ItemForLastMapX.Size = new System.Drawing.Size(208, 24);
            this.ItemForLastMapX.Text = "Last Map X:";
            this.ItemForLastMapX.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForLastMapY
            // 
            this.ItemForLastMapY.Control = this.LastMapYTextEdit;
            this.ItemForLastMapY.CustomizationFormText = "Last Map Y:";
            this.ItemForLastMapY.Location = new System.Drawing.Point(638, 10);
            this.ItemForLastMapY.Name = "ItemForLastMapY";
            this.ItemForLastMapY.Size = new System.Drawing.Size(213, 24);
            this.ItemForLastMapY.Text = "Last Map Y:";
            this.ItemForLastMapY.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForWorkspaceName
            // 
            this.ItemForWorkspaceName.Control = this.WorkspaceNameTextEdit;
            this.ItemForWorkspaceName.CustomizationFormText = "Workspace Name:";
            this.ItemForWorkspaceName.Location = new System.Drawing.Point(638, 34);
            this.ItemForWorkspaceName.Name = "ItemForWorkspaceName";
            this.ItemForWorkspaceName.Size = new System.Drawing.Size(213, 24);
            this.ItemForWorkspaceName.Text = "Workspace Name:";
            this.ItemForWorkspaceName.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(903, 630);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForPoleName,
            this.ItemForSurveyDate,
            this.ItemForSurveyorID,
            this.ItemForSurveyStatusID,
            this.emptySpaceItem4,
            this.ItemForTotalCost,
            this.ItemForTotalIncome,
            this.layoutControlItem4,
            this.emptySpaceItem15});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(883, 610);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(113, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(113, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(113, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(113, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(182, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 107);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(883, 503);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup7;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(867, 465);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Mapping";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.emptySpaceItem6,
            this.tabbedControlGroup2,
            this.layoutControlGroup12,
            this.ItemForContractYear,
            this.layoutControlGroup11,
            this.ItemForContractTypeID,
            this.emptySpaceItem3,
            this.emptySpaceItem5,
            this.ItemForLoadSurveyIntoMap,
            this.layoutControlItem3});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(851, 425);
            this.layoutControlGroup7.Text = "Survey";
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(851, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 164);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(851, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 174);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup3;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(851, 251);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.layoutControlGroup10});
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Surveyed Poles";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(827, 206);
            this.layoutControlGroup3.Text = "Surveyed Poles";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.CustomizationFormText = "Linked Species:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(827, 206);
            this.layoutControlItem2.Text = "Linked Species:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(827, 206);
            this.layoutControlGroup10.Text = "Surveyed Pole Permissions";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl7;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(827, 206);
            this.layoutControlItem5.Text = "Surveyed Pole Permissions Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForMapStartTypeID,
            this.ItemForLastMapRecordDescription});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 34);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(424, 94);
            this.layoutControlGroup12.Text = "Mapping";
            // 
            // ItemForMapStartTypeID
            // 
            this.ItemForMapStartTypeID.Control = this.MapStartTypeIDGridLookUpEdit;
            this.ItemForMapStartTypeID.CustomizationFormText = "Map Start Type:";
            this.ItemForMapStartTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForMapStartTypeID.Name = "ItemForMapStartTypeID";
            this.ItemForMapStartTypeID.Size = new System.Drawing.Size(400, 24);
            this.ItemForMapStartTypeID.Text = "Map Start Type:";
            this.ItemForMapStartTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForLastMapRecordDescription
            // 
            this.ItemForLastMapRecordDescription.Control = this.LastMapRecordDescriptionButtonEdit;
            this.ItemForLastMapRecordDescription.CustomizationFormText = "Map Start Record:";
            this.ItemForLastMapRecordDescription.Location = new System.Drawing.Point(0, 24);
            this.ItemForLastMapRecordDescription.Name = "ItemForLastMapRecordDescription";
            this.ItemForLastMapRecordDescription.Size = new System.Drawing.Size(400, 24);
            this.ItemForLastMapRecordDescription.Text = "Map Start Record:";
            this.ItemForLastMapRecordDescription.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForContractYear
            // 
            this.ItemForContractYear.Control = this.ContractYearSpinEdit;
            this.ItemForContractYear.CustomizationFormText = "Contract Year:";
            this.ItemForContractYear.Location = new System.Drawing.Point(0, 10);
            this.ItemForContractYear.Name = "ItemForContractYear";
            this.ItemForContractYear.Size = new System.Drawing.Size(424, 24);
            this.ItemForContractYear.Text = "Contract Year:";
            this.ItemForContractYear.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTargetSurveyCompletionDate,
            this.ItemForTargetPermissionCompletionDate,
            this.ItemForTargetWorkCompletionDate});
            this.layoutControlGroup11.Location = new System.Drawing.Point(424, 34);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(427, 130);
            this.layoutControlGroup11.Text = "Target Dates";
            // 
            // ItemForTargetSurveyCompletionDate
            // 
            this.ItemForTargetSurveyCompletionDate.Control = this.TargetSurveyCompletionDateDateEdit;
            this.ItemForTargetSurveyCompletionDate.CustomizationFormText = "Target Survey Completion:";
            this.ItemForTargetSurveyCompletionDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForTargetSurveyCompletionDate.Name = "ItemForTargetSurveyCompletionDate";
            this.ItemForTargetSurveyCompletionDate.Size = new System.Drawing.Size(403, 24);
            this.ItemForTargetSurveyCompletionDate.Text = "Survey Completion:";
            this.ItemForTargetSurveyCompletionDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTargetPermissionCompletionDate
            // 
            this.ItemForTargetPermissionCompletionDate.Control = this.TargetPermissionCompletionDateDateEdit;
            this.ItemForTargetPermissionCompletionDate.CustomizationFormText = "Target Permission Completion:";
            this.ItemForTargetPermissionCompletionDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForTargetPermissionCompletionDate.Name = "ItemForTargetPermissionCompletionDate";
            this.ItemForTargetPermissionCompletionDate.Size = new System.Drawing.Size(403, 24);
            this.ItemForTargetPermissionCompletionDate.Text = "Permission Completion:";
            this.ItemForTargetPermissionCompletionDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTargetWorkCompletionDate
            // 
            this.ItemForTargetWorkCompletionDate.Control = this.TargetWorkCompletionDateDateEdit;
            this.ItemForTargetWorkCompletionDate.CustomizationFormText = "Target Work Completion:";
            this.ItemForTargetWorkCompletionDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForTargetWorkCompletionDate.Name = "ItemForTargetWorkCompletionDate";
            this.ItemForTargetWorkCompletionDate.Size = new System.Drawing.Size(403, 36);
            this.ItemForTargetWorkCompletionDate.Text = "Work Completion:";
            this.ItemForTargetWorkCompletionDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForContractTypeID
            // 
            this.ItemForContractTypeID.Control = this.ContractTypeIDGridLookUpEdit;
            this.ItemForContractTypeID.Location = new System.Drawing.Point(424, 10);
            this.ItemForContractTypeID.Name = "ItemForContractTypeID";
            this.ItemForContractTypeID.Size = new System.Drawing.Size(427, 24);
            this.ItemForContractTypeID.Text = "Contract Type:";
            this.ItemForContractTypeID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 128);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(424, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(246, 138);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(178, 26);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLoadSurveyIntoMap
            // 
            this.ItemForLoadSurveyIntoMap.Control = this.btnLoadMapWithSurvey;
            this.ItemForLoadSurveyIntoMap.CustomizationFormText = "Load Survey into Map:";
            this.ItemForLoadSurveyIntoMap.Location = new System.Drawing.Point(108, 138);
            this.ItemForLoadSurveyIntoMap.MaxSize = new System.Drawing.Size(138, 26);
            this.ItemForLoadSurveyIntoMap.MinSize = new System.Drawing.Size(138, 26);
            this.ItemForLoadSurveyIntoMap.Name = "ItemForLoadSurveyIntoMap";
            this.ItemForLoadSurveyIntoMap.Size = new System.Drawing.Size(138, 26);
            this.ItemForLoadSurveyIntoMap.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLoadSurveyIntoMap.Text = "Load Survey into Map:";
            this.ItemForLoadSurveyIntoMap.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForLoadSurveyIntoMap.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "Save Button:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(108, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "Save Button:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Reactive";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReactive,
            this.ItemForClientPONumber,
            this.ItemForExchequerNumber,
            this.ItemForQuoteReceivedDate,
            this.ItemForQuoteApprovedDate,
            this.emptySpaceItem8,
            this.layoutControlGroup5,
            this.ItemForReactiveRemarks,
            this.emptySpaceItem9,
            this.layoutControlGroup9,
            this.splitterItem1,
            this.ItemForReactiveCategoryID,
            this.emptySpaceItem14});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(851, 425);
            this.layoutControlGroup8.Text = "Reactive";
            // 
            // ItemForReactive
            // 
            this.ItemForReactive.Control = this.ReactiveCheckEdit;
            this.ItemForReactive.CustomizationFormText = "Reactive:";
            this.ItemForReactive.Location = new System.Drawing.Point(0, 0);
            this.ItemForReactive.Name = "ItemForReactive";
            this.ItemForReactive.Size = new System.Drawing.Size(426, 24);
            this.ItemForReactive.Text = "Reactive:";
            this.ItemForReactive.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForClientPONumber
            // 
            this.ItemForClientPONumber.Control = this.ClientPONumberTextEdit;
            this.ItemForClientPONumber.CustomizationFormText = "Client PO Number:";
            this.ItemForClientPONumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForClientPONumber.Name = "ItemForClientPONumber";
            this.ItemForClientPONumber.Size = new System.Drawing.Size(426, 24);
            this.ItemForClientPONumber.Text = "Client PO Number:";
            this.ItemForClientPONumber.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForExchequerNumber
            // 
            this.ItemForExchequerNumber.Control = this.ExchequerNumberTextEdit;
            this.ItemForExchequerNumber.CustomizationFormText = "Exchequer Number:";
            this.ItemForExchequerNumber.Location = new System.Drawing.Point(426, 24);
            this.ItemForExchequerNumber.Name = "ItemForExchequerNumber";
            this.ItemForExchequerNumber.Size = new System.Drawing.Size(425, 24);
            this.ItemForExchequerNumber.Text = "Exchequer Number:";
            this.ItemForExchequerNumber.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForQuoteReceivedDate
            // 
            this.ItemForQuoteReceivedDate.Control = this.QuoteReceivedDateDateEdit;
            this.ItemForQuoteReceivedDate.CustomizationFormText = "Quote Received Date:";
            this.ItemForQuoteReceivedDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForQuoteReceivedDate.Name = "ItemForQuoteReceivedDate";
            this.ItemForQuoteReceivedDate.Size = new System.Drawing.Size(426, 24);
            this.ItemForQuoteReceivedDate.Text = "Quote Received Date:";
            this.ItemForQuoteReceivedDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForQuoteApprovedDate
            // 
            this.ItemForQuoteApprovedDate.Control = this.QuoteApprovedDateDateEdit;
            this.ItemForQuoteApprovedDate.CustomizationFormText = "Quote Approved Date:";
            this.ItemForQuoteApprovedDate.Location = new System.Drawing.Point(426, 48);
            this.ItemForQuoteApprovedDate.Name = "ItemForQuoteApprovedDate";
            this.ItemForQuoteApprovedDate.Size = new System.Drawing.Size(425, 24);
            this.ItemForQuoteApprovedDate.Text = "Quote Approved Date:";
            this.ItemForQuoteApprovedDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 327);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(851, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Reported By";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReportedByName,
            this.ItemForReportedByAddress,
            this.ItemForReportedByPostcode,
            this.ItemForReportedByTelephone,
            this.ItemForReportedByEmail,
            this.emptySpaceItem11,
            this.ItemForCopyAddressButton1,
            this.emptySpaceItem12});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 82);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(420, 245);
            this.layoutControlGroup5.Text = "Reported By";
            // 
            // ItemForReportedByName
            // 
            this.ItemForReportedByName.Control = this.ReportedByNameTextEdit;
            this.ItemForReportedByName.CustomizationFormText = "Reported By Name:";
            this.ItemForReportedByName.Location = new System.Drawing.Point(0, 26);
            this.ItemForReportedByName.Name = "ItemForReportedByName";
            this.ItemForReportedByName.Size = new System.Drawing.Size(396, 24);
            this.ItemForReportedByName.Text = "Name:";
            this.ItemForReportedByName.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForReportedByAddress
            // 
            this.ItemForReportedByAddress.Control = this.ReportedByAddressMemoEdit;
            this.ItemForReportedByAddress.CustomizationFormText = "Reported By Address:";
            this.ItemForReportedByAddress.Location = new System.Drawing.Point(0, 50);
            this.ItemForReportedByAddress.MaxSize = new System.Drawing.Size(0, 67);
            this.ItemForReportedByAddress.MinSize = new System.Drawing.Size(127, 67);
            this.ItemForReportedByAddress.Name = "ItemForReportedByAddress";
            this.ItemForReportedByAddress.Size = new System.Drawing.Size(396, 67);
            this.ItemForReportedByAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReportedByAddress.Text = "Address:";
            this.ItemForReportedByAddress.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForReportedByPostcode
            // 
            this.ItemForReportedByPostcode.Control = this.ReportedByPostcodeTextEdit;
            this.ItemForReportedByPostcode.CustomizationFormText = "Reported By Postcode:";
            this.ItemForReportedByPostcode.Location = new System.Drawing.Point(0, 117);
            this.ItemForReportedByPostcode.Name = "ItemForReportedByPostcode";
            this.ItemForReportedByPostcode.Size = new System.Drawing.Size(396, 24);
            this.ItemForReportedByPostcode.Text = "Postcode:";
            this.ItemForReportedByPostcode.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForReportedByTelephone
            // 
            this.ItemForReportedByTelephone.Control = this.ReportedByTelephoneTextEdit;
            this.ItemForReportedByTelephone.CustomizationFormText = "Reported By Telephone:";
            this.ItemForReportedByTelephone.Location = new System.Drawing.Point(0, 141);
            this.ItemForReportedByTelephone.Name = "ItemForReportedByTelephone";
            this.ItemForReportedByTelephone.Size = new System.Drawing.Size(396, 24);
            this.ItemForReportedByTelephone.Text = "Telephone:";
            this.ItemForReportedByTelephone.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForReportedByEmail
            // 
            this.ItemForReportedByEmail.Control = this.ReportedByEmailTextEdit;
            this.ItemForReportedByEmail.CustomizationFormText = "Reported By Email:";
            this.ItemForReportedByEmail.Location = new System.Drawing.Point(0, 165);
            this.ItemForReportedByEmail.Name = "ItemForReportedByEmail";
            this.ItemForReportedByEmail.Size = new System.Drawing.Size(396, 24);
            this.ItemForReportedByEmail.Text = "Email:";
            this.ItemForReportedByEmail.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 189);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(396, 10);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCopyAddressButton1
            // 
            this.ItemForCopyAddressButton1.Control = this.CopyAddressButton1;
            this.ItemForCopyAddressButton1.CustomizationFormText = "Copy Reported By Address Button";
            this.ItemForCopyAddressButton1.Location = new System.Drawing.Point(257, 0);
            this.ItemForCopyAddressButton1.MaxSize = new System.Drawing.Size(139, 26);
            this.ItemForCopyAddressButton1.MinSize = new System.Drawing.Size(139, 26);
            this.ItemForCopyAddressButton1.Name = "ItemForCopyAddressButton1";
            this.ItemForCopyAddressButton1.Size = new System.Drawing.Size(139, 26);
            this.ItemForCopyAddressButton1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCopyAddressButton1.Text = "Copy Reported By Address Button";
            this.ItemForCopyAddressButton1.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCopyAddressButton1.TextVisible = false;
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(257, 26);
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForReactiveRemarks
            // 
            this.ItemForReactiveRemarks.Control = this.ReactiveRemarksMemoEdit;
            this.ItemForReactiveRemarks.CustomizationFormText = "Reactive Remarks:";
            this.ItemForReactiveRemarks.Location = new System.Drawing.Point(0, 337);
            this.ItemForReactiveRemarks.MaxSize = new System.Drawing.Size(0, 51);
            this.ItemForReactiveRemarks.MinSize = new System.Drawing.Size(127, 51);
            this.ItemForReactiveRemarks.Name = "ItemForReactiveRemarks";
            this.ItemForReactiveRemarks.Size = new System.Drawing.Size(851, 51);
            this.ItemForReactiveRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForReactiveRemarks.Text = "Reactive Remarks:";
            this.ItemForReactiveRemarks.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(851, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Incident";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIncidentAddress,
            this.emptySpaceItem10,
            this.ItemForIncidentPostcode,
            this.ItemForCopyAddressButton2,
            this.emptySpaceItem13});
            this.layoutControlGroup9.Location = new System.Drawing.Point(426, 82);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(425, 245);
            this.layoutControlGroup9.Text = "Incident";
            // 
            // ItemForIncidentAddress
            // 
            this.ItemForIncidentAddress.Control = this.IncidentAddressMemoEdit;
            this.ItemForIncidentAddress.CustomizationFormText = "Incident Address:";
            this.ItemForIncidentAddress.Location = new System.Drawing.Point(0, 26);
            this.ItemForIncidentAddress.MaxSize = new System.Drawing.Size(0, 92);
            this.ItemForIncidentAddress.MinSize = new System.Drawing.Size(127, 92);
            this.ItemForIncidentAddress.Name = "ItemForIncidentAddress";
            this.ItemForIncidentAddress.Size = new System.Drawing.Size(401, 92);
            this.ItemForIncidentAddress.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForIncidentAddress.Text = "Address:";
            this.ItemForIncidentAddress.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 142);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(401, 57);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForIncidentPostcode
            // 
            this.ItemForIncidentPostcode.Control = this.IncidentPostcodeTextEdit;
            this.ItemForIncidentPostcode.CustomizationFormText = "Incident Postcode:";
            this.ItemForIncidentPostcode.Location = new System.Drawing.Point(0, 118);
            this.ItemForIncidentPostcode.Name = "ItemForIncidentPostcode";
            this.ItemForIncidentPostcode.Size = new System.Drawing.Size(401, 24);
            this.ItemForIncidentPostcode.Text = "Postcode:";
            this.ItemForIncidentPostcode.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForCopyAddressButton2
            // 
            this.ItemForCopyAddressButton2.Control = this.CopyAddressButton2;
            this.ItemForCopyAddressButton2.CustomizationFormText = "Copy Incident Address Button";
            this.ItemForCopyAddressButton2.Location = new System.Drawing.Point(243, 0);
            this.ItemForCopyAddressButton2.MaxSize = new System.Drawing.Size(158, 26);
            this.ItemForCopyAddressButton2.MinSize = new System.Drawing.Size(158, 26);
            this.ItemForCopyAddressButton2.Name = "ItemForCopyAddressButton2";
            this.ItemForCopyAddressButton2.Size = new System.Drawing.Size(158, 26);
            this.ItemForCopyAddressButton2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCopyAddressButton2.Text = "Copy Incident Address Button";
            this.ItemForCopyAddressButton2.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCopyAddressButton2.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(243, 26);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(420, 82);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 245);
            // 
            // ItemForReactiveCategoryID
            // 
            this.ItemForReactiveCategoryID.Control = this.ReactiveCategoryIDGridLookUpEdit;
            this.ItemForReactiveCategoryID.CustomizationFormText = "Reactive Category:";
            this.ItemForReactiveCategoryID.Location = new System.Drawing.Point(426, 0);
            this.ItemForReactiveCategoryID.Name = "ItemForReactiveCategoryID";
            this.ItemForReactiveCategoryID.Size = new System.Drawing.Size(425, 24);
            this.ItemForReactiveCategoryID.Text = "Category:";
            this.ItemForReactiveCategoryID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 388);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(851, 37);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImageOptions.Image")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(851, 425);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(851, 425);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // ItemForPoleName
            // 
            this.ItemForPoleName.AllowHide = false;
            this.ItemForPoleName.Control = this.ClientNameButtonEdit;
            this.ItemForPoleName.CustomizationFormText = "Client Name:";
            this.ItemForPoleName.Location = new System.Drawing.Point(0, 23);
            this.ItemForPoleName.Name = "ItemForPoleName";
            this.ItemForPoleName.Size = new System.Drawing.Size(442, 24);
            this.ItemForPoleName.Text = "Client Name:";
            this.ItemForPoleName.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSurveyDate
            // 
            this.ItemForSurveyDate.Control = this.SurveyDateDateEdit;
            this.ItemForSurveyDate.CustomizationFormText = "Survey Date:";
            this.ItemForSurveyDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForSurveyDate.Name = "ItemForSurveyDate";
            this.ItemForSurveyDate.Size = new System.Drawing.Size(442, 24);
            this.ItemForSurveyDate.Text = "Survey Date:";
            this.ItemForSurveyDate.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSurveyorID
            // 
            this.ItemForSurveyorID.Control = this.SurveyorIDGridLookUpEdit;
            this.ItemForSurveyorID.CustomizationFormText = "Surveyor:";
            this.ItemForSurveyorID.Location = new System.Drawing.Point(442, 23);
            this.ItemForSurveyorID.Name = "ItemForSurveyorID";
            this.ItemForSurveyorID.Size = new System.Drawing.Size(441, 24);
            this.ItemForSurveyorID.Text = "Surveyor:";
            this.ItemForSurveyorID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForSurveyStatusID
            // 
            this.ItemForSurveyStatusID.Control = this.SurveyStatusIDGridLookUpEdit;
            this.ItemForSurveyStatusID.CustomizationFormText = "Survey Status:";
            this.ItemForSurveyStatusID.Location = new System.Drawing.Point(442, 47);
            this.ItemForSurveyStatusID.Name = "ItemForSurveyStatusID";
            this.ItemForSurveyStatusID.Size = new System.Drawing.Size(441, 24);
            this.ItemForSurveyStatusID.Text = "Survey Status:";
            this.ItemForSurveyStatusID.TextSize = new System.Drawing.Size(110, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(883, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostSpinEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 71);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(442, 26);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(110, 13);
            // 
            // ItemForTotalIncome
            // 
            this.ItemForTotalIncome.Control = this.TotalIncomeSpinEdit;
            this.ItemForTotalIncome.CustomizationFormText = "Total Income:";
            this.ItemForTotalIncome.Location = new System.Drawing.Point(442, 71);
            this.ItemForTotalIncome.Name = "ItemForTotalIncome";
            this.ItemForTotalIncome.Size = new System.Drawing.Size(331, 26);
            this.ItemForTotalIncome.Text = "Total Income:";
            this.ItemForTotalIncome.TextSize = new System.Drawing.Size(110, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnCalculateTotals;
            this.layoutControlItem4.CustomizationFormText = "Calculate Totals button:";
            this.layoutControlItem4.Location = new System.Drawing.Point(773, 71);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(110, 26);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(110, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(110, 26);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "Calculate Totals button:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(295, 0);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(588, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07060UTTreeSpeciesLinkedToTreeBindingSource
            // 
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataMember = "sp07060_UT_Tree_Species_Linked_To_Tree";
            this.sp07060UTTreeSpeciesLinkedToTreeBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter
            // 
            this.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter.ClearBeforeFill = true;
            // 
            // sp07103_UT_Survey_ItemTableAdapter
            // 
            this.sp07103_UT_Survey_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07105_UT_Map_Start_TypesTableAdapter
            // 
            this.sp07105_UT_Map_Start_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07106_UT_Survey_StatusesTableAdapter
            // 
            this.sp07106_UT_Survey_StatusesTableAdapter.ClearBeforeFill = true;
            // 
            // sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter
            // 
            this.sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(908, 656);
            this.xtraTabControl1.TabIndex = 13;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(903, 630);
            this.xtraTabPage1.Text = "Survey";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(903, 630);
            this.xtraTabPage2.Text = "Survey Costings";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Costing Headers";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Costing Items";
            this.splitContainerControl1.Size = new System.Drawing.Size(903, 630);
            this.splitContainerControl1.SplitterPosition = 124;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07220UTSurveyCostingHeadersBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Clone Selected Record", "clone")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemCheckEdit3});
            this.gridControl2.Size = new System.Drawing.Size(878, 121);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07220UTSurveyCostingHeadersBindingSource
            // 
            this.sp07220UTSurveyCostingHeadersBindingSource.DataMember = "sp07220_UT_Survey_Costing_Headers";
            this.sp07220UTSurveyCostingHeadersBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyCostingHeaderID,
            this.colSurveyID1,
            this.colLive,
            this.colDateCreated,
            this.colCreatedByID,
            this.colDescription1,
            this.colTotalCost,
            this.colRemarks1,
            this.colSurveyDate,
            this.colTotalIncome,
            this.colCreatedBy});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsCustomization.AllowFilter = false;
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLive, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView2_FocusedRowChanged);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSurveyCostingHeaderID
            // 
            this.colSurveyCostingHeaderID.Caption = "Survey Costing Header ID";
            this.colSurveyCostingHeaderID.FieldName = "SurveyCostingHeaderID";
            this.colSurveyCostingHeaderID.Name = "colSurveyCostingHeaderID";
            this.colSurveyCostingHeaderID.OptionsColumn.AllowEdit = false;
            this.colSurveyCostingHeaderID.OptionsColumn.AllowFocus = false;
            this.colSurveyCostingHeaderID.OptionsColumn.ReadOnly = true;
            this.colSurveyCostingHeaderID.Width = 146;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colLive
            // 
            this.colLive.Caption = "Live";
            this.colLive.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colLive.FieldName = "Live";
            this.colLive.Name = "colLive";
            this.colLive.OptionsColumn.AllowEdit = false;
            this.colLive.OptionsColumn.AllowFocus = false;
            this.colLive.OptionsColumn.ReadOnly = true;
            this.colLive.Visible = true;
            this.colLive.VisibleIndex = 0;
            this.colLive.Width = 53;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 1;
            this.colDateCreated.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Created By ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 89;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 3;
            this.colDescription1.Width = 286;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 4;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colSurveyDate
            // 
            this.colSurveyDate.Caption = "Survey Date";
            this.colSurveyDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colSurveyDate.FieldName = "SurveyDate";
            this.colSurveyDate.Name = "colSurveyDate";
            this.colSurveyDate.OptionsColumn.AllowEdit = false;
            this.colSurveyDate.OptionsColumn.AllowFocus = false;
            this.colSurveyDate.OptionsColumn.ReadOnly = true;
            this.colSurveyDate.Visible = true;
            this.colSurveyDate.VisibleIndex = 6;
            this.colSurveyDate.Width = 100;
            // 
            // colTotalIncome
            // 
            this.colTotalIncome.Caption = "Total Income";
            this.colTotalIncome.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTotalIncome.FieldName = "TotalIncome";
            this.colTotalIncome.Name = "colTotalIncome";
            this.colTotalIncome.OptionsColumn.AllowEdit = false;
            this.colTotalIncome.OptionsColumn.AllowFocus = false;
            this.colTotalIncome.OptionsColumn.ReadOnly = true;
            this.colTotalIncome.Visible = true;
            this.colTotalIncome.VisibleIndex = 5;
            this.colTotalIncome.Width = 83;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 2;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl3);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl6);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(878, 497);
            this.splitContainerControl2.SplitterPosition = 120;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07221UTSurveyCostingPoleBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.bandedGridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemMemoExEdit3,
            this.repositoryItemMemoEditPoleNumbers});
            this.gridControl3.Size = new System.Drawing.Size(878, 120);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView3});
            // 
            // sp07221UTSurveyCostingPoleBindingSource
            // 
            this.sp07221UTSurveyCostingPoleBindingSource.DataMember = "sp07221_UT_Survey_Costing_Pole";
            this.sp07221UTSurveyCostingPoleBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // bandedGridView3
            // 
            this.bandedGridView3.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand17,
            this.gridBand18,
            this.gridBand19,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3,
            this.gridBand4,
            this.gridBand5,
            this.gridBand6,
            this.gridBand7,
            this.gridBand8,
            this.gridBand9,
            this.gridBand10,
            this.gridBand11,
            this.gridBand12,
            this.gridBand13,
            this.gridBand14,
            this.gridBand15,
            this.gridBand20});
            this.bandedGridView3.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colSurveyCostingPoleID,
            this.colSurveyCostingHeaderID1,
            this.colLiveWorkDate,
            this.colLiveWorkPoleIDs,
            this.colLiveWorkPoles,
            this.colSD1Date,
            this.colSD1PoleIDs,
            this.colSD1Poles,
            this.colSD2Date,
            this.colSD2PoleIDs,
            this.colSD2Poles,
            this.colSD3Date,
            this.colSD3PoleIDs,
            this.colSD3Poles,
            this.colSD4Date,
            this.colSD4PoleIDs,
            this.colSD4Poles,
            this.colSD5Date,
            this.colSD5PoleIDs,
            this.colSD5Poles,
            this.colSD6Date,
            this.colSD6PoleIDs,
            this.colSD6Poles,
            this.colYear3DeferralDate,
            this.colYear3DeferralPoleIDs,
            this.colYear3DeferralPoles,
            this.colYear5DeferralDate,
            this.colYear5DeferralPoleIDs,
            this.colYear5DeferralPoles,
            this.colYear1RevisitDate,
            this.colYear1RevisitPoleIDs,
            this.colYear1RevisitPoles,
            this.colYear2RevisitDate,
            this.colYear2RevisitPoleIDs,
            this.colYear2RevisitPoles,
            this.colYear3RevisitDate,
            this.colYear3RevisitPoleIDs,
            this.colYear3RevisitPoles,
            this.colYear4RevisitDate,
            this.colYear4RevisitPoleIDs,
            this.colYear4RevisitPoles,
            this.colYear5RevisitDate,
            this.colYear5RevisitPoleIDs,
            this.colYear5RevisitPoles,
            this.colRemarks2});
            this.bandedGridView3.GridControl = this.gridControl3;
            this.bandedGridView3.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.bandedGridView3.Name = "bandedGridView3";
            this.bandedGridView3.OptionsCustomization.AllowBandMoving = false;
            this.bandedGridView3.OptionsCustomization.AllowBandResizing = false;
            this.bandedGridView3.OptionsCustomization.AllowColumnMoving = false;
            this.bandedGridView3.OptionsCustomization.AllowColumnResizing = false;
            this.bandedGridView3.OptionsCustomization.AllowFilter = false;
            this.bandedGridView3.OptionsCustomization.AllowGroup = false;
            this.bandedGridView3.OptionsCustomization.AllowQuickHideColumns = false;
            this.bandedGridView3.OptionsCustomization.AllowRowSizing = true;
            this.bandedGridView3.OptionsCustomization.AllowSort = false;
            this.bandedGridView3.OptionsFilter.AllowFilterEditor = false;
            this.bandedGridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.bandedGridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.bandedGridView3.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.bandedGridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.bandedGridView3.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView3.OptionsView.ShowGroupPanel = false;
            this.bandedGridView3.RowHeight = 75;
            this.bandedGridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.bandedGridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bandedGridView3_MouseUp);
            this.bandedGridView3.GotFocus += new System.EventHandler(this.bandedGridView3_GotFocus);
            // 
            // gridBand17
            // 
            this.gridBand17.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand17.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand17.Caption = "Resource";
            this.gridBand17.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand17.Name = "gridBand17";
            this.gridBand17.VisibleIndex = 0;
            this.gridBand17.Width = 180;
            // 
            // gridBand18
            // 
            this.gridBand18.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand18.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand18.Caption = "Rate";
            this.gridBand18.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand18.Name = "gridBand18";
            this.gridBand18.VisibleIndex = 1;
            this.gridBand18.Width = 60;
            // 
            // gridBand19
            // 
            this.gridBand19.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand19.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand19.Caption = "Unit";
            this.gridBand19.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand19.Name = "gridBand19";
            this.gridBand19.VisibleIndex = 2;
            this.gridBand19.Width = 60;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Live Work";
            this.gridBand1.Columns.Add(this.colSurveyCostingPoleID);
            this.gridBand1.Columns.Add(this.colSurveyCostingHeaderID1);
            this.gridBand1.Columns.Add(this.colLiveWorkDate);
            this.gridBand1.Columns.Add(this.colLiveWorkPoles);
            this.gridBand1.Columns.Add(this.colLiveWorkPoleIDs);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 3;
            this.gridBand1.Width = 175;
            // 
            // colSurveyCostingPoleID
            // 
            this.colSurveyCostingPoleID.Caption = "Survey Costing Pole ID";
            this.colSurveyCostingPoleID.FieldName = "SurveyCostingPoleID";
            this.colSurveyCostingPoleID.Name = "colSurveyCostingPoleID";
            this.colSurveyCostingPoleID.Width = 131;
            // 
            // colSurveyCostingHeaderID1
            // 
            this.colSurveyCostingHeaderID1.Caption = "Survey Costing Header ID";
            this.colSurveyCostingHeaderID1.FieldName = "SurveyCostingHeaderID";
            this.colSurveyCostingHeaderID1.Name = "colSurveyCostingHeaderID1";
            this.colSurveyCostingHeaderID1.Width = 146;
            // 
            // colLiveWorkDate
            // 
            this.colLiveWorkDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colLiveWorkDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colLiveWorkDate.AppearanceCell.Options.UseBackColor = true;
            this.colLiveWorkDate.AppearanceCell.Options.UseForeColor = true;
            this.colLiveWorkDate.AppearanceCell.Options.UseTextOptions = true;
            this.colLiveWorkDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colLiveWorkDate.Caption = "Date";
            this.colLiveWorkDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colLiveWorkDate.CustomizationCaption = "Live Work Date";
            this.colLiveWorkDate.FieldName = "LiveWorkDate";
            this.colLiveWorkDate.Name = "colLiveWorkDate";
            this.colLiveWorkDate.OptionsColumn.AllowShowHide = false;
            this.colLiveWorkDate.Visible = true;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colLiveWorkPoles
            // 
            this.colLiveWorkPoles.Caption = "Site Numbers";
            this.colLiveWorkPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colLiveWorkPoles.CustomizationCaption = "Live Work Site Numbers";
            this.colLiveWorkPoles.FieldName = "LiveWorkPoles";
            this.colLiveWorkPoles.Name = "colLiveWorkPoles";
            this.colLiveWorkPoles.OptionsColumn.AllowShowHide = false;
            this.colLiveWorkPoles.OptionsColumn.ReadOnly = true;
            this.colLiveWorkPoles.Visible = true;
            this.colLiveWorkPoles.Width = 100;
            // 
            // repositoryItemMemoEditPoleNumbers
            // 
            this.repositoryItemMemoEditPoleNumbers.Name = "repositoryItemMemoEditPoleNumbers";
            // 
            // colLiveWorkPoleIDs
            // 
            this.colLiveWorkPoleIDs.Caption = "Live Work Pole IDs";
            this.colLiveWorkPoleIDs.FieldName = "LiveWorkPoleIDs";
            this.colLiveWorkPoleIDs.Name = "colLiveWorkPoleIDs";
            this.colLiveWorkPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colLiveWorkPoleIDs.Width = 110;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "SD1";
            this.gridBand2.Columns.Add(this.colSD1Date);
            this.gridBand2.Columns.Add(this.colSD1PoleIDs);
            this.gridBand2.Columns.Add(this.colSD1Poles);
            this.gridBand2.Columns.Add(this.colSD2PoleIDs);
            this.gridBand2.Columns.Add(this.colSD3PoleIDs);
            this.gridBand2.Columns.Add(this.colSD4PoleIDs);
            this.gridBand2.Columns.Add(this.colSD5PoleIDs);
            this.gridBand2.Columns.Add(this.colSD6PoleIDs);
            this.gridBand2.Columns.Add(this.colYear3DeferralPoleIDs);
            this.gridBand2.Columns.Add(this.colYear5DeferralPoleIDs);
            this.gridBand2.Columns.Add(this.colYear1RevisitPoleIDs);
            this.gridBand2.Columns.Add(this.colYear2RevisitPoleIDs);
            this.gridBand2.Columns.Add(this.colYear3RevisitPoleIDs);
            this.gridBand2.Columns.Add(this.colYear4RevisitPoleIDs);
            this.gridBand2.Columns.Add(this.colYear5RevisitPoleIDs);
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 4;
            this.gridBand2.Width = 175;
            // 
            // colSD1Date
            // 
            this.colSD1Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD1Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD1Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD1Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD1Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD1Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD1Date.Caption = "Date";
            this.colSD1Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD1Date.CustomizationCaption = "SD 1 Date";
            this.colSD1Date.FieldName = "SD1Date";
            this.colSD1Date.Name = "colSD1Date";
            this.colSD1Date.OptionsColumn.AllowShowHide = false;
            this.colSD1Date.Visible = true;
            // 
            // colSD1PoleIDs
            // 
            this.colSD1PoleIDs.Caption = "SD 1 Pole IDs";
            this.colSD1PoleIDs.FieldName = "SD1PoleIDs";
            this.colSD1PoleIDs.Name = "colSD1PoleIDs";
            this.colSD1PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD1PoleIDs.Width = 79;
            // 
            // colSD1Poles
            // 
            this.colSD1Poles.Caption = "Site Numbers";
            this.colSD1Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD1Poles.CustomizationCaption = "SD1 Site Numbers";
            this.colSD1Poles.FieldName = "SD1Poles";
            this.colSD1Poles.Name = "colSD1Poles";
            this.colSD1Poles.OptionsColumn.AllowShowHide = false;
            this.colSD1Poles.OptionsColumn.ReadOnly = true;
            this.colSD1Poles.Visible = true;
            this.colSD1Poles.Width = 100;
            // 
            // colSD2PoleIDs
            // 
            this.colSD2PoleIDs.Caption = "SD 2 Pole IDs";
            this.colSD2PoleIDs.FieldName = "SD2PoleIDs";
            this.colSD2PoleIDs.Name = "colSD2PoleIDs";
            this.colSD2PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD2PoleIDs.Width = 79;
            // 
            // colSD3PoleIDs
            // 
            this.colSD3PoleIDs.Caption = "SD 3 Pole IDs";
            this.colSD3PoleIDs.FieldName = "SD3PoleIDs";
            this.colSD3PoleIDs.Name = "colSD3PoleIDs";
            this.colSD3PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD3PoleIDs.Width = 79;
            // 
            // colSD4PoleIDs
            // 
            this.colSD4PoleIDs.Caption = "SD 4 Pole IDs";
            this.colSD4PoleIDs.FieldName = "SD4PoleIDs";
            this.colSD4PoleIDs.Name = "colSD4PoleIDs";
            this.colSD4PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD4PoleIDs.Width = 79;
            // 
            // colSD5PoleIDs
            // 
            this.colSD5PoleIDs.Caption = "SD 5 Pole IDs";
            this.colSD5PoleIDs.FieldName = "SD5PoleIDs";
            this.colSD5PoleIDs.Name = "colSD5PoleIDs";
            this.colSD5PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD5PoleIDs.Width = 79;
            // 
            // colSD6PoleIDs
            // 
            this.colSD6PoleIDs.Caption = "SD 6 Pole IDs";
            this.colSD6PoleIDs.FieldName = "SD6PoleIDs";
            this.colSD6PoleIDs.Name = "colSD6PoleIDs";
            this.colSD6PoleIDs.OptionsColumn.AllowShowHide = false;
            this.colSD6PoleIDs.Width = 79;
            // 
            // colYear3DeferralPoleIDs
            // 
            this.colYear3DeferralPoleIDs.Caption = "Year 3 Deferral Pole IDs";
            this.colYear3DeferralPoleIDs.FieldName = "Year3DeferralPoleIDs";
            this.colYear3DeferralPoleIDs.Name = "colYear3DeferralPoleIDs";
            this.colYear3DeferralPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear3DeferralPoleIDs.Width = 130;
            // 
            // colYear5DeferralPoleIDs
            // 
            this.colYear5DeferralPoleIDs.Caption = "Year 5 Deferral Pole IDs";
            this.colYear5DeferralPoleIDs.FieldName = "Year5DeferralPoleIDs";
            this.colYear5DeferralPoleIDs.Name = "colYear5DeferralPoleIDs";
            this.colYear5DeferralPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear5DeferralPoleIDs.Width = 130;
            // 
            // colYear1RevisitPoleIDs
            // 
            this.colYear1RevisitPoleIDs.Caption = "Year 1 Revisit Pole IDs";
            this.colYear1RevisitPoleIDs.FieldName = "Year1RevisitPoleIDs";
            this.colYear1RevisitPoleIDs.Name = "colYear1RevisitPoleIDs";
            this.colYear1RevisitPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear1RevisitPoleIDs.Width = 123;
            // 
            // colYear2RevisitPoleIDs
            // 
            this.colYear2RevisitPoleIDs.Caption = "Year 2 Revisit Pole IDs";
            this.colYear2RevisitPoleIDs.FieldName = "Year2RevisitPoleIDs";
            this.colYear2RevisitPoleIDs.Name = "colYear2RevisitPoleIDs";
            this.colYear2RevisitPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear2RevisitPoleIDs.Width = 123;
            // 
            // colYear3RevisitPoleIDs
            // 
            this.colYear3RevisitPoleIDs.Caption = "Year 3 Revisit Pole IDs";
            this.colYear3RevisitPoleIDs.FieldName = "Year3RevisitPoleIDs";
            this.colYear3RevisitPoleIDs.Name = "colYear3RevisitPoleIDs";
            this.colYear3RevisitPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear3RevisitPoleIDs.Width = 123;
            // 
            // colYear4RevisitPoleIDs
            // 
            this.colYear4RevisitPoleIDs.Caption = "Year4 Revisit Pole IDs";
            this.colYear4RevisitPoleIDs.FieldName = "Year4RevisitPoleIDs";
            this.colYear4RevisitPoleIDs.Name = "colYear4RevisitPoleIDs";
            this.colYear4RevisitPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear4RevisitPoleIDs.Width = 123;
            // 
            // colYear5RevisitPoleIDs
            // 
            this.colYear5RevisitPoleIDs.Caption = "Year 5 Revisit Pole IDs";
            this.colYear5RevisitPoleIDs.FieldName = "Year5RevisitPoleIDs";
            this.colYear5RevisitPoleIDs.Name = "colYear5RevisitPoleIDs";
            this.colYear5RevisitPoleIDs.OptionsColumn.AllowShowHide = false;
            this.colYear5RevisitPoleIDs.Width = 123;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "SD 2";
            this.gridBand3.Columns.Add(this.colSD2Date);
            this.gridBand3.Columns.Add(this.colSD2Poles);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 5;
            this.gridBand3.Width = 175;
            // 
            // colSD2Date
            // 
            this.colSD2Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD2Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD2Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD2Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD2Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD2Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD2Date.Caption = "Date";
            this.colSD2Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD2Date.CustomizationCaption = "SD 2 Date";
            this.colSD2Date.FieldName = "SD2Date";
            this.colSD2Date.Name = "colSD2Date";
            this.colSD2Date.OptionsColumn.AllowShowHide = false;
            this.colSD2Date.Visible = true;
            // 
            // colSD2Poles
            // 
            this.colSD2Poles.Caption = "Site Numbers";
            this.colSD2Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD2Poles.CustomizationCaption = "SD 3 Site Numbers";
            this.colSD2Poles.FieldName = "SD2Poles";
            this.colSD2Poles.Name = "colSD2Poles";
            this.colSD2Poles.OptionsColumn.AllowShowHide = false;
            this.colSD2Poles.OptionsColumn.ReadOnly = true;
            this.colSD2Poles.Visible = true;
            this.colSD2Poles.Width = 100;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "SD 3";
            this.gridBand4.Columns.Add(this.colSD3Date);
            this.gridBand4.Columns.Add(this.colSD3Poles);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 6;
            this.gridBand4.Width = 175;
            // 
            // colSD3Date
            // 
            this.colSD3Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD3Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD3Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD3Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD3Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD3Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD3Date.Caption = "Date";
            this.colSD3Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD3Date.CustomizationCaption = "SD 3 Date";
            this.colSD3Date.FieldName = "SD3Date";
            this.colSD3Date.Name = "colSD3Date";
            this.colSD3Date.OptionsColumn.AllowShowHide = false;
            this.colSD3Date.Visible = true;
            // 
            // colSD3Poles
            // 
            this.colSD3Poles.Caption = "Site Numbers";
            this.colSD3Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD3Poles.CustomizationCaption = "SD 3 Site Numbers";
            this.colSD3Poles.FieldName = "SD3Poles";
            this.colSD3Poles.Name = "colSD3Poles";
            this.colSD3Poles.OptionsColumn.AllowShowHide = false;
            this.colSD3Poles.OptionsColumn.ReadOnly = true;
            this.colSD3Poles.Visible = true;
            this.colSD3Poles.Width = 100;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "SD 4";
            this.gridBand5.Columns.Add(this.colSD4Date);
            this.gridBand5.Columns.Add(this.colSD4Poles);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 7;
            this.gridBand5.Width = 175;
            // 
            // colSD4Date
            // 
            this.colSD4Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD4Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD4Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD4Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD4Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD4Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD4Date.Caption = "Date";
            this.colSD4Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD4Date.CustomizationCaption = "Sd 4 Date";
            this.colSD4Date.FieldName = "SD4Date";
            this.colSD4Date.Name = "colSD4Date";
            this.colSD4Date.OptionsColumn.AllowShowHide = false;
            this.colSD4Date.Visible = true;
            // 
            // colSD4Poles
            // 
            this.colSD4Poles.Caption = "Site Numbers";
            this.colSD4Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD4Poles.CustomizationCaption = "SD 4 Site Numbers";
            this.colSD4Poles.FieldName = "SD4Poles";
            this.colSD4Poles.Name = "colSD4Poles";
            this.colSD4Poles.OptionsColumn.AllowShowHide = false;
            this.colSD4Poles.OptionsColumn.ReadOnly = true;
            this.colSD4Poles.Visible = true;
            this.colSD4Poles.Width = 100;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "SD 5";
            this.gridBand6.Columns.Add(this.colSD5Date);
            this.gridBand6.Columns.Add(this.colSD5Poles);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 8;
            this.gridBand6.Width = 175;
            // 
            // colSD5Date
            // 
            this.colSD5Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD5Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD5Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD5Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD5Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD5Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD5Date.Caption = "Date";
            this.colSD5Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD5Date.CustomizationCaption = "SD 5 Date";
            this.colSD5Date.FieldName = "SD5Date";
            this.colSD5Date.Name = "colSD5Date";
            this.colSD5Date.OptionsColumn.AllowShowHide = false;
            this.colSD5Date.Visible = true;
            // 
            // colSD5Poles
            // 
            this.colSD5Poles.Caption = "Site Numbers";
            this.colSD5Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD5Poles.CustomizationCaption = "SD 5 Site Numbers";
            this.colSD5Poles.FieldName = "SD5Poles";
            this.colSD5Poles.Name = "colSD5Poles";
            this.colSD5Poles.OptionsColumn.AllowShowHide = false;
            this.colSD5Poles.OptionsColumn.ReadOnly = true;
            this.colSD5Poles.Visible = true;
            this.colSD5Poles.Width = 100;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "SD 6";
            this.gridBand7.Columns.Add(this.colSD6Date);
            this.gridBand7.Columns.Add(this.colSD6Poles);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 9;
            this.gridBand7.Width = 175;
            // 
            // colSD6Date
            // 
            this.colSD6Date.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD6Date.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD6Date.AppearanceCell.Options.UseBackColor = true;
            this.colSD6Date.AppearanceCell.Options.UseForeColor = true;
            this.colSD6Date.AppearanceCell.Options.UseTextOptions = true;
            this.colSD6Date.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colSD6Date.Caption = "Date";
            this.colSD6Date.ColumnEdit = this.repositoryItemDateEdit1;
            this.colSD6Date.CustomizationCaption = "SD 6 Date";
            this.colSD6Date.FieldName = "SD6Date";
            this.colSD6Date.Name = "colSD6Date";
            this.colSD6Date.OptionsColumn.AllowShowHide = false;
            this.colSD6Date.Visible = true;
            // 
            // colSD6Poles
            // 
            this.colSD6Poles.Caption = "Site Numbers";
            this.colSD6Poles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colSD6Poles.CustomizationCaption = "SD 6 Site Numbers";
            this.colSD6Poles.FieldName = "SD6Poles";
            this.colSD6Poles.Name = "colSD6Poles";
            this.colSD6Poles.OptionsColumn.AllowShowHide = false;
            this.colSD6Poles.OptionsColumn.ReadOnly = true;
            this.colSD6Poles.Visible = true;
            this.colSD6Poles.Width = 100;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "3 Year Deferrals";
            this.gridBand8.Columns.Add(this.colYear3DeferralDate);
            this.gridBand8.Columns.Add(this.colYear3DeferralPoles);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 10;
            this.gridBand8.Width = 175;
            // 
            // colYear3DeferralDate
            // 
            this.colYear3DeferralDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear3DeferralDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear3DeferralDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear3DeferralDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear3DeferralDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear3DeferralDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear3DeferralDate.Caption = "Date";
            this.colYear3DeferralDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear3DeferralDate.CustomizationCaption = "Year 3 Deferral Date";
            this.colYear3DeferralDate.FieldName = "Year3DeferralDate";
            this.colYear3DeferralDate.Name = "colYear3DeferralDate";
            this.colYear3DeferralDate.OptionsColumn.AllowShowHide = false;
            this.colYear3DeferralDate.Visible = true;
            // 
            // colYear3DeferralPoles
            // 
            this.colYear3DeferralPoles.Caption = "Site Numbers";
            this.colYear3DeferralPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear3DeferralPoles.CustomizationCaption = "Year 3 Deferral Site Numbers";
            this.colYear3DeferralPoles.FieldName = "Year3DeferralPoles";
            this.colYear3DeferralPoles.Name = "colYear3DeferralPoles";
            this.colYear3DeferralPoles.OptionsColumn.AllowShowHide = false;
            this.colYear3DeferralPoles.OptionsColumn.ReadOnly = true;
            this.colYear3DeferralPoles.Visible = true;
            this.colYear3DeferralPoles.Width = 100;
            // 
            // gridBand9
            // 
            this.gridBand9.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand9.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand9.Caption = "5 Year Deferrals";
            this.gridBand9.Columns.Add(this.colYear5DeferralDate);
            this.gridBand9.Columns.Add(this.colYear5DeferralPoles);
            this.gridBand9.Name = "gridBand9";
            this.gridBand9.VisibleIndex = 11;
            this.gridBand9.Width = 175;
            // 
            // colYear5DeferralDate
            // 
            this.colYear5DeferralDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear5DeferralDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear5DeferralDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear5DeferralDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear5DeferralDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear5DeferralDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear5DeferralDate.Caption = "Date";
            this.colYear5DeferralDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear5DeferralDate.CustomizationCaption = "Year 5 Deferral Date";
            this.colYear5DeferralDate.FieldName = "Year5DeferralDate";
            this.colYear5DeferralDate.Name = "colYear5DeferralDate";
            this.colYear5DeferralDate.OptionsColumn.AllowShowHide = false;
            this.colYear5DeferralDate.Visible = true;
            // 
            // colYear5DeferralPoles
            // 
            this.colYear5DeferralPoles.Caption = "Site Numbers";
            this.colYear5DeferralPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear5DeferralPoles.CustomizationCaption = "Year 5 Deferral Site Numbers";
            this.colYear5DeferralPoles.FieldName = "Year5DeferralPoles";
            this.colYear5DeferralPoles.Name = "colYear5DeferralPoles";
            this.colYear5DeferralPoles.OptionsColumn.AllowShowHide = false;
            this.colYear5DeferralPoles.OptionsColumn.ReadOnly = true;
            this.colYear5DeferralPoles.Visible = true;
            this.colYear5DeferralPoles.Width = 100;
            // 
            // gridBand10
            // 
            this.gridBand10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand10.Caption = "1 Year Revisits";
            this.gridBand10.Columns.Add(this.colYear1RevisitDate);
            this.gridBand10.Columns.Add(this.colYear1RevisitPoles);
            this.gridBand10.Name = "gridBand10";
            this.gridBand10.VisibleIndex = 12;
            this.gridBand10.Width = 175;
            // 
            // colYear1RevisitDate
            // 
            this.colYear1RevisitDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear1RevisitDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear1RevisitDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear1RevisitDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear1RevisitDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear1RevisitDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear1RevisitDate.Caption = "Date";
            this.colYear1RevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear1RevisitDate.FieldName = "Year1RevisitDate";
            this.colYear1RevisitDate.Name = "colYear1RevisitDate";
            this.colYear1RevisitDate.OptionsColumn.AllowShowHide = false;
            this.colYear1RevisitDate.Visible = true;
            // 
            // colYear1RevisitPoles
            // 
            this.colYear1RevisitPoles.Caption = "Site Numbers";
            this.colYear1RevisitPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear1RevisitPoles.CustomizationCaption = "Year 1 Revisit Site Numbers";
            this.colYear1RevisitPoles.FieldName = "Year1RevisitPoles";
            this.colYear1RevisitPoles.Name = "colYear1RevisitPoles";
            this.colYear1RevisitPoles.OptionsColumn.AllowShowHide = false;
            this.colYear1RevisitPoles.OptionsColumn.ReadOnly = true;
            this.colYear1RevisitPoles.Visible = true;
            this.colYear1RevisitPoles.Width = 100;
            // 
            // gridBand11
            // 
            this.gridBand11.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand11.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand11.Caption = "2 Year Revisits";
            this.gridBand11.Columns.Add(this.colYear2RevisitDate);
            this.gridBand11.Columns.Add(this.colYear2RevisitPoles);
            this.gridBand11.Name = "gridBand11";
            this.gridBand11.VisibleIndex = 13;
            this.gridBand11.Width = 175;
            // 
            // colYear2RevisitDate
            // 
            this.colYear2RevisitDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear2RevisitDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear2RevisitDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear2RevisitDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear2RevisitDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear2RevisitDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear2RevisitDate.Caption = "Date";
            this.colYear2RevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear2RevisitDate.FieldName = "Year2RevisitDate";
            this.colYear2RevisitDate.Name = "colYear2RevisitDate";
            this.colYear2RevisitDate.OptionsColumn.AllowShowHide = false;
            this.colYear2RevisitDate.Visible = true;
            // 
            // colYear2RevisitPoles
            // 
            this.colYear2RevisitPoles.Caption = "Site Numbers";
            this.colYear2RevisitPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear2RevisitPoles.CustomizationCaption = "Year 2 Revisit Site Numbers";
            this.colYear2RevisitPoles.FieldName = "Year2RevisitPoles";
            this.colYear2RevisitPoles.Name = "colYear2RevisitPoles";
            this.colYear2RevisitPoles.OptionsColumn.AllowShowHide = false;
            this.colYear2RevisitPoles.OptionsColumn.ReadOnly = true;
            this.colYear2RevisitPoles.Visible = true;
            this.colYear2RevisitPoles.Width = 100;
            // 
            // gridBand12
            // 
            this.gridBand12.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand12.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand12.Caption = "3 Year Revisits";
            this.gridBand12.Columns.Add(this.colYear3RevisitDate);
            this.gridBand12.Columns.Add(this.colYear3RevisitPoles);
            this.gridBand12.Name = "gridBand12";
            this.gridBand12.VisibleIndex = 14;
            this.gridBand12.Width = 175;
            // 
            // colYear3RevisitDate
            // 
            this.colYear3RevisitDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear3RevisitDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear3RevisitDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear3RevisitDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear3RevisitDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear3RevisitDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear3RevisitDate.Caption = "Date";
            this.colYear3RevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear3RevisitDate.FieldName = "Year3RevisitDate";
            this.colYear3RevisitDate.Name = "colYear3RevisitDate";
            this.colYear3RevisitDate.OptionsColumn.AllowShowHide = false;
            this.colYear3RevisitDate.Visible = true;
            // 
            // colYear3RevisitPoles
            // 
            this.colYear3RevisitPoles.Caption = "Site Numbers";
            this.colYear3RevisitPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear3RevisitPoles.CustomizationCaption = "Year 3 Revisit Site Numbers";
            this.colYear3RevisitPoles.FieldName = "Year3RevisitPoles";
            this.colYear3RevisitPoles.Name = "colYear3RevisitPoles";
            this.colYear3RevisitPoles.OptionsColumn.AllowShowHide = false;
            this.colYear3RevisitPoles.OptionsColumn.ReadOnly = true;
            this.colYear3RevisitPoles.Visible = true;
            this.colYear3RevisitPoles.Width = 100;
            // 
            // gridBand13
            // 
            this.gridBand13.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand13.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand13.Caption = "4 Year Revisits";
            this.gridBand13.Columns.Add(this.colYear4RevisitDate);
            this.gridBand13.Columns.Add(this.colYear4RevisitPoles);
            this.gridBand13.Name = "gridBand13";
            this.gridBand13.VisibleIndex = 15;
            this.gridBand13.Width = 175;
            // 
            // colYear4RevisitDate
            // 
            this.colYear4RevisitDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear4RevisitDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear4RevisitDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear4RevisitDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear4RevisitDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear4RevisitDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear4RevisitDate.Caption = "Date";
            this.colYear4RevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear4RevisitDate.FieldName = "Year4RevisitDate";
            this.colYear4RevisitDate.Name = "colYear4RevisitDate";
            this.colYear4RevisitDate.OptionsColumn.AllowShowHide = false;
            this.colYear4RevisitDate.Visible = true;
            // 
            // colYear4RevisitPoles
            // 
            this.colYear4RevisitPoles.Caption = "Site Numbers";
            this.colYear4RevisitPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear4RevisitPoles.CustomizationCaption = "Year 4 Revisit Site Numbers";
            this.colYear4RevisitPoles.FieldName = "Year4RevisitPoles";
            this.colYear4RevisitPoles.Name = "colYear4RevisitPoles";
            this.colYear4RevisitPoles.OptionsColumn.AllowShowHide = false;
            this.colYear4RevisitPoles.OptionsColumn.ReadOnly = true;
            this.colYear4RevisitPoles.Visible = true;
            this.colYear4RevisitPoles.Width = 100;
            // 
            // gridBand14
            // 
            this.gridBand14.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand14.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand14.Caption = "5 Year Revisit";
            this.gridBand14.Columns.Add(this.colYear5RevisitDate);
            this.gridBand14.Columns.Add(this.colYear5RevisitPoles);
            this.gridBand14.Name = "gridBand14";
            this.gridBand14.VisibleIndex = 16;
            this.gridBand14.Width = 175;
            // 
            // colYear5RevisitDate
            // 
            this.colYear5RevisitDate.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear5RevisitDate.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear5RevisitDate.AppearanceCell.Options.UseBackColor = true;
            this.colYear5RevisitDate.AppearanceCell.Options.UseForeColor = true;
            this.colYear5RevisitDate.AppearanceCell.Options.UseTextOptions = true;
            this.colYear5RevisitDate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.colYear5RevisitDate.Caption = "Date";
            this.colYear5RevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colYear5RevisitDate.FieldName = "Year5RevisitDate";
            this.colYear5RevisitDate.Name = "colYear5RevisitDate";
            this.colYear5RevisitDate.OptionsColumn.AllowShowHide = false;
            this.colYear5RevisitDate.Visible = true;
            // 
            // colYear5RevisitPoles
            // 
            this.colYear5RevisitPoles.Caption = "Site Numbers";
            this.colYear5RevisitPoles.ColumnEdit = this.repositoryItemMemoEditPoleNumbers;
            this.colYear5RevisitPoles.CustomizationCaption = "Year 5 Revisit Site Numbers";
            this.colYear5RevisitPoles.FieldName = "Year5RevisitPoles";
            this.colYear5RevisitPoles.Name = "colYear5RevisitPoles";
            this.colYear5RevisitPoles.OptionsColumn.AllowShowHide = false;
            this.colYear5RevisitPoles.OptionsColumn.ReadOnly = true;
            this.colYear5RevisitPoles.Visible = true;
            this.colYear5RevisitPoles.Width = 100;
            // 
            // gridBand15
            // 
            this.gridBand15.Columns.Add(this.colRemarks2);
            this.gridBand15.Name = "gridBand15";
            this.gridBand15.VisibleIndex = 17;
            this.gridBand15.Width = 75;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.AllowShowHide = false;
            this.colRemarks2.Visible = true;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // gridBand20
            // 
            this.gridBand20.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand20.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand20.Caption = "TOTAL";
            this.gridBand20.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridBand20.Name = "gridBand20";
            this.gridBand20.VisibleIndex = 18;
            this.gridBand20.Width = 192;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp07222UTSurveyCostingItemBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEditUnits,
            this.repositoryItemSpinEditCost,
            this.repositoryItemTextEditTotalUnits,
            this.repositoryItemTextEditTotalCost,
            this.repositoryItemGridLookUpEditUnits,
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEditResource,
            this.repositoryItemMemoExEdit4});
            this.gridControl6.Size = new System.Drawing.Size(878, 371);
            this.gridControl6.TabIndex = 2;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07222UTSurveyCostingItemBindingSource
            // 
            this.sp07222UTSurveyCostingItemBindingSource.DataMember = "sp07222_UT_Survey_Costing_Item";
            this.sp07222UTSurveyCostingItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyCostingItemID,
            this.colSurveyCostingHeaderID2,
            this.colCostingTypeID,
            this.colDescription3,
            this.colRate,
            this.colUnitDescriptorID,
            this.colSortOrder,
            this.colLiveWorkUnits,
            this.colLiveWorkCost,
            this.colSD1Units,
            this.colSD1Cost,
            this.colSD2Units,
            this.colSD2Cost,
            this.colSD3Units,
            this.colSD3Cost,
            this.colSD4Units,
            this.colSD4Cost,
            this.colSD5Units,
            this.colSD5Cost,
            this.colSD6Units,
            this.colSD6Cost,
            this.colYear3DeferralUnits,
            this.colYear3DeferralCost,
            this.colYear5DeferralUnits,
            this.colYear5DeferralCost,
            this.colYear1RevisitUnits,
            this.colYear1RevisitCost,
            this.colYear2RevisitUnits,
            this.colYear2RevisitCost,
            this.colYear3RevisitUnits,
            this.colYear3RevisitCost,
            this.colYear4RevisitUnits,
            this.colYear4RevisitCost,
            this.colYear5RevisitUnits,
            this.colYear5RevisitCost,
            this.colTotalUnits,
            this.colTotalCost1,
            this.colRemarks3,
            this.colTypeDescription,
            this.colTypeOrder});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 1;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsCustomization.AllowColumnMoving = false;
            this.gridView6.OptionsCustomization.AllowColumnResizing = false;
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsCustomization.AllowGroup = false;
            this.gridView6.OptionsCustomization.AllowQuickHideColumns = false;
            this.gridView6.OptionsCustomization.AllowSort = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFooter = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            this.gridView6.LeftCoordChanged += new System.EventHandler(this.gridView6_LeftCoordChanged);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView6_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView6_ShowingEditor);
            this.gridView6.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridView6_CustomColumnSort);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colSurveyCostingItemID
            // 
            this.colSurveyCostingItemID.Caption = "Survey Costing Item ID";
            this.colSurveyCostingItemID.FieldName = "SurveyCostingItemID";
            this.colSurveyCostingItemID.Name = "colSurveyCostingItemID";
            this.colSurveyCostingItemID.Width = 133;
            // 
            // colSurveyCostingHeaderID2
            // 
            this.colSurveyCostingHeaderID2.Caption = "Survey Costing Header ID";
            this.colSurveyCostingHeaderID2.FieldName = "SurveyCostingHeaderID";
            this.colSurveyCostingHeaderID2.Name = "colSurveyCostingHeaderID2";
            this.colSurveyCostingHeaderID2.Width = 146;
            // 
            // colCostingTypeID
            // 
            this.colCostingTypeID.Caption = "Costing Type ID";
            this.colCostingTypeID.FieldName = "CostingTypeID";
            this.colCostingTypeID.Name = "colCostingTypeID";
            this.colCostingTypeID.OptionsColumn.AllowShowHide = false;
            this.colCostingTypeID.Width = 98;
            // 
            // colDescription3
            // 
            this.colDescription3.Caption = "Resource";
            this.colDescription3.ColumnEdit = this.repositoryItemTextEditResource;
            this.colDescription3.FieldName = "Description";
            this.colDescription3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDescription3.Name = "colDescription3";
            this.colDescription3.OptionsColumn.AllowShowHide = false;
            this.colDescription3.Visible = true;
            this.colDescription3.VisibleIndex = 0;
            this.colDescription3.Width = 180;
            // 
            // repositoryItemTextEditResource
            // 
            this.repositoryItemTextEditResource.AutoHeight = false;
            this.repositoryItemTextEditResource.MaxLength = 50;
            this.repositoryItemTextEditResource.Name = "repositoryItemTextEditResource";
            // 
            // colRate
            // 
            this.colRate.Caption = "Rate";
            this.colRate.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colRate.FieldName = "Rate";
            this.colRate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRate.Name = "colRate";
            this.colRate.OptionsColumn.AllowShowHide = false;
            this.colRate.Visible = true;
            this.colRate.VisibleIndex = 1;
            this.colRate.Width = 60;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colUnitDescriptorID
            // 
            this.colUnitDescriptorID.AppearanceCell.BackColor = System.Drawing.Color.LightBlue;
            this.colUnitDescriptorID.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colUnitDescriptorID.AppearanceCell.Options.UseBackColor = true;
            this.colUnitDescriptorID.AppearanceCell.Options.UseForeColor = true;
            this.colUnitDescriptorID.Caption = "Unit";
            this.colUnitDescriptorID.ColumnEdit = this.repositoryItemGridLookUpEditUnits;
            this.colUnitDescriptorID.FieldName = "UnitDescriptorID";
            this.colUnitDescriptorID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colUnitDescriptorID.Name = "colUnitDescriptorID";
            this.colUnitDescriptorID.OptionsColumn.AllowShowHide = false;
            this.colUnitDescriptorID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "UnitDescriptorID", "TOTALS:")});
            this.colUnitDescriptorID.Visible = true;
            this.colUnitDescriptorID.VisibleIndex = 2;
            this.colUnitDescriptorID.Width = 60;
            // 
            // repositoryItemGridLookUpEditUnits
            // 
            this.repositoryItemGridLookUpEditUnits.AutoHeight = false;
            this.repositoryItemGridLookUpEditUnits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditUnits.DataSource = this.sp07138UTUnitDescriptorsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditUnits.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditUnits.Name = "repositoryItemGridLookUpEditUnits";
            this.repositoryItemGridLookUpEditUnits.NullText = "";
            this.repositoryItemGridLookUpEditUnits.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditUnits.ValueMember = "ID";
            // 
            // sp07138UTUnitDescriptorsWithBlankBindingSource
            // 
            this.sp07138UTUnitDescriptorsWithBlankBindingSource.DataMember = "sp07138_UT_Unit_Descriptors_With_Blank";
            this.sp07138UTUnitDescriptorsWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription4,
            this.colID1,
            this.colRecordOrder1});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colID1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.repositoryItemGridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription4
            // 
            this.colDescription4.Caption = "Unit Descriptor";
            this.colDescription4.FieldName = "Description";
            this.colDescription4.Name = "colDescription4";
            this.colDescription4.OptionsColumn.AllowEdit = false;
            this.colDescription4.OptionsColumn.AllowFocus = false;
            this.colDescription4.OptionsColumn.ReadOnly = true;
            this.colDescription4.Visible = true;
            this.colDescription4.VisibleIndex = 0;
            this.colDescription4.Width = 131;
            // 
            // colRecordOrder1
            // 
            this.colRecordOrder1.Caption = "Order";
            this.colRecordOrder1.FieldName = "RecordOrder";
            this.colRecordOrder1.Name = "colRecordOrder1";
            this.colRecordOrder1.OptionsColumn.AllowEdit = false;
            this.colRecordOrder1.OptionsColumn.AllowFocus = false;
            this.colRecordOrder1.OptionsColumn.ReadOnly = true;
            // 
            // colSortOrder
            // 
            this.colSortOrder.Caption = "Sort Order";
            this.colSortOrder.FieldName = "SortOrder";
            this.colSortOrder.Name = "colSortOrder";
            this.colSortOrder.OptionsColumn.AllowShowHide = false;
            this.colSortOrder.Width = 84;
            // 
            // colLiveWorkUnits
            // 
            this.colLiveWorkUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colLiveWorkUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colLiveWorkUnits.AppearanceCell.Options.UseBackColor = true;
            this.colLiveWorkUnits.AppearanceCell.Options.UseForeColor = true;
            this.colLiveWorkUnits.Caption = "Number";
            this.colLiveWorkUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colLiveWorkUnits.CustomizationCaption = "Live Work Number";
            this.colLiveWorkUnits.FieldName = "LiveWorkUnits";
            this.colLiveWorkUnits.Name = "colLiveWorkUnits";
            this.colLiveWorkUnits.OptionsColumn.AllowShowHide = false;
            this.colLiveWorkUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LiveWorkUnits", "{0:f0}")});
            this.colLiveWorkUnits.Visible = true;
            this.colLiveWorkUnits.VisibleIndex = 3;
            // 
            // repositoryItemSpinEditUnits
            // 
            this.repositoryItemSpinEditUnits.AutoHeight = false;
            this.repositoryItemSpinEditUnits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditUnits.IsFloatValue = false;
            this.repositoryItemSpinEditUnits.Mask.EditMask = "N00";
            this.repositoryItemSpinEditUnits.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditUnits.Name = "repositoryItemSpinEditUnits";
            this.repositoryItemSpinEditUnits.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditUnits_EditValueChanged);
            this.repositoryItemSpinEditUnits.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditUnits_Validating);
            // 
            // colLiveWorkCost
            // 
            this.colLiveWorkCost.Caption = "Cost";
            this.colLiveWorkCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colLiveWorkCost.CustomizationCaption = "Live Work Cost";
            this.colLiveWorkCost.FieldName = "LiveWorkCost";
            this.colLiveWorkCost.Name = "colLiveWorkCost";
            this.colLiveWorkCost.OptionsColumn.AllowShowHide = false;
            this.colLiveWorkCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "LiveWorkCost", "{0:c}")});
            this.colLiveWorkCost.Visible = true;
            this.colLiveWorkCost.VisibleIndex = 4;
            this.colLiveWorkCost.Width = 100;
            // 
            // repositoryItemSpinEditCost
            // 
            this.repositoryItemSpinEditCost.AutoHeight = false;
            this.repositoryItemSpinEditCost.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditCost.Mask.EditMask = "c";
            this.repositoryItemSpinEditCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditCost.Name = "repositoryItemSpinEditCost";
            this.repositoryItemSpinEditCost.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditCost_EditValueChanged);
            this.repositoryItemSpinEditCost.Validating += new System.ComponentModel.CancelEventHandler(this.repositoryItemSpinEditCost_Validating);
            // 
            // colSD1Units
            // 
            this.colSD1Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD1Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD1Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD1Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD1Units.Caption = "Number";
            this.colSD1Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD1Units.CustomizationCaption = "SD 1 Number";
            this.colSD1Units.FieldName = "SD1Units";
            this.colSD1Units.Name = "colSD1Units";
            this.colSD1Units.OptionsColumn.AllowShowHide = false;
            this.colSD1Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD1Units", "{0:f0}")});
            this.colSD1Units.Visible = true;
            this.colSD1Units.VisibleIndex = 5;
            // 
            // colSD1Cost
            // 
            this.colSD1Cost.Caption = "Cost";
            this.colSD1Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD1Cost.CustomizationCaption = "SD 1 Cost";
            this.colSD1Cost.FieldName = "SD1Cost";
            this.colSD1Cost.Name = "colSD1Cost";
            this.colSD1Cost.OptionsColumn.AllowShowHide = false;
            this.colSD1Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD1Cost", "{0:c}")});
            this.colSD1Cost.Visible = true;
            this.colSD1Cost.VisibleIndex = 6;
            this.colSD1Cost.Width = 100;
            // 
            // colSD2Units
            // 
            this.colSD2Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD2Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD2Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD2Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD2Units.Caption = "Number";
            this.colSD2Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD2Units.CustomizationCaption = "SD  Number";
            this.colSD2Units.FieldName = "SD2Units";
            this.colSD2Units.Name = "colSD2Units";
            this.colSD2Units.OptionsColumn.AllowShowHide = false;
            this.colSD2Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD2Units", "{0:f0}")});
            this.colSD2Units.Visible = true;
            this.colSD2Units.VisibleIndex = 7;
            // 
            // colSD2Cost
            // 
            this.colSD2Cost.Caption = "Cost";
            this.colSD2Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD2Cost.CustomizationCaption = "SD 2 Cost";
            this.colSD2Cost.FieldName = "SD2Cost";
            this.colSD2Cost.Name = "colSD2Cost";
            this.colSD2Cost.OptionsColumn.AllowShowHide = false;
            this.colSD2Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD2Cost", "{0:c}")});
            this.colSD2Cost.Visible = true;
            this.colSD2Cost.VisibleIndex = 8;
            this.colSD2Cost.Width = 100;
            // 
            // colSD3Units
            // 
            this.colSD3Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD3Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD3Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD3Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD3Units.Caption = "Number";
            this.colSD3Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD3Units.CustomizationCaption = "SD 3 Number";
            this.colSD3Units.FieldName = "SD3Units";
            this.colSD3Units.Name = "colSD3Units";
            this.colSD3Units.OptionsColumn.AllowShowHide = false;
            this.colSD3Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD3Units", "{0:f0}")});
            this.colSD3Units.Visible = true;
            this.colSD3Units.VisibleIndex = 9;
            // 
            // colSD3Cost
            // 
            this.colSD3Cost.Caption = "Cost";
            this.colSD3Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD3Cost.CustomizationCaption = "SD 3 Cost";
            this.colSD3Cost.FieldName = "SD3Cost";
            this.colSD3Cost.Name = "colSD3Cost";
            this.colSD3Cost.OptionsColumn.AllowShowHide = false;
            this.colSD3Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD3Cost", "{0:c}")});
            this.colSD3Cost.Visible = true;
            this.colSD3Cost.VisibleIndex = 10;
            this.colSD3Cost.Width = 100;
            // 
            // colSD4Units
            // 
            this.colSD4Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD4Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD4Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD4Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD4Units.Caption = "Number";
            this.colSD4Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD4Units.CustomizationCaption = "SD 4 Number";
            this.colSD4Units.FieldName = "SD4Units";
            this.colSD4Units.Name = "colSD4Units";
            this.colSD4Units.OptionsColumn.AllowShowHide = false;
            this.colSD4Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD4Units", "{0:f0}")});
            this.colSD4Units.Visible = true;
            this.colSD4Units.VisibleIndex = 11;
            // 
            // colSD4Cost
            // 
            this.colSD4Cost.Caption = "Cost";
            this.colSD4Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD4Cost.CustomizationCaption = "SD 4 Cost";
            this.colSD4Cost.FieldName = "SD4Cost";
            this.colSD4Cost.Name = "colSD4Cost";
            this.colSD4Cost.OptionsColumn.AllowShowHide = false;
            this.colSD4Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD4Cost", "{0:c}")});
            this.colSD4Cost.Visible = true;
            this.colSD4Cost.VisibleIndex = 12;
            this.colSD4Cost.Width = 100;
            // 
            // colSD5Units
            // 
            this.colSD5Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD5Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD5Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD5Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD5Units.Caption = "Number";
            this.colSD5Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD5Units.CustomizationCaption = "SD 5 Number";
            this.colSD5Units.FieldName = "SD5Units";
            this.colSD5Units.Name = "colSD5Units";
            this.colSD5Units.OptionsColumn.AllowShowHide = false;
            this.colSD5Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD5Units", "{0:f0}")});
            this.colSD5Units.Visible = true;
            this.colSD5Units.VisibleIndex = 13;
            // 
            // colSD5Cost
            // 
            this.colSD5Cost.Caption = "Cost";
            this.colSD5Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD5Cost.CustomizationCaption = "SD 5 Cost";
            this.colSD5Cost.FieldName = "SD5Cost";
            this.colSD5Cost.Name = "colSD5Cost";
            this.colSD5Cost.OptionsColumn.AllowShowHide = false;
            this.colSD5Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD5Cost", "{0:c}")});
            this.colSD5Cost.Visible = true;
            this.colSD5Cost.VisibleIndex = 14;
            this.colSD5Cost.Width = 100;
            // 
            // colSD6Units
            // 
            this.colSD6Units.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colSD6Units.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSD6Units.AppearanceCell.Options.UseBackColor = true;
            this.colSD6Units.AppearanceCell.Options.UseForeColor = true;
            this.colSD6Units.Caption = "Number";
            this.colSD6Units.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colSD6Units.CustomizationCaption = "SD 6 Number";
            this.colSD6Units.FieldName = "SD6Units";
            this.colSD6Units.Name = "colSD6Units";
            this.colSD6Units.OptionsColumn.AllowShowHide = false;
            this.colSD6Units.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD6Units", "{0:f0}")});
            this.colSD6Units.Visible = true;
            this.colSD6Units.VisibleIndex = 15;
            // 
            // colSD6Cost
            // 
            this.colSD6Cost.Caption = "Cost";
            this.colSD6Cost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colSD6Cost.CustomizationCaption = "SD 6 Cost";
            this.colSD6Cost.FieldName = "SD6Cost";
            this.colSD6Cost.Name = "colSD6Cost";
            this.colSD6Cost.OptionsColumn.AllowShowHide = false;
            this.colSD6Cost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SD6Cost", "{0:c}")});
            this.colSD6Cost.Visible = true;
            this.colSD6Cost.VisibleIndex = 16;
            this.colSD6Cost.Width = 100;
            // 
            // colYear3DeferralUnits
            // 
            this.colYear3DeferralUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear3DeferralUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear3DeferralUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear3DeferralUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear3DeferralUnits.Caption = "Number";
            this.colYear3DeferralUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear3DeferralUnits.CustomizationCaption = "Year 3 Deferral Number";
            this.colYear3DeferralUnits.FieldName = "Year3DeferralUnits";
            this.colYear3DeferralUnits.Name = "colYear3DeferralUnits";
            this.colYear3DeferralUnits.OptionsColumn.AllowShowHide = false;
            this.colYear3DeferralUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year3DeferralUnits", "{0:f0}")});
            this.colYear3DeferralUnits.Visible = true;
            this.colYear3DeferralUnits.VisibleIndex = 17;
            // 
            // colYear3DeferralCost
            // 
            this.colYear3DeferralCost.Caption = "Cost";
            this.colYear3DeferralCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear3DeferralCost.CustomizationCaption = "Year 3 Deferral Cost";
            this.colYear3DeferralCost.FieldName = "Year3DeferralCost";
            this.colYear3DeferralCost.Name = "colYear3DeferralCost";
            this.colYear3DeferralCost.OptionsColumn.AllowShowHide = false;
            this.colYear3DeferralCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year3DeferralCost", "{0:c}")});
            this.colYear3DeferralCost.Visible = true;
            this.colYear3DeferralCost.VisibleIndex = 18;
            this.colYear3DeferralCost.Width = 100;
            // 
            // colYear5DeferralUnits
            // 
            this.colYear5DeferralUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear5DeferralUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear5DeferralUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear5DeferralUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear5DeferralUnits.Caption = "Number";
            this.colYear5DeferralUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear5DeferralUnits.CustomizationCaption = "Year 5 Deferral Number";
            this.colYear5DeferralUnits.FieldName = "Year5DeferralUnits";
            this.colYear5DeferralUnits.Name = "colYear5DeferralUnits";
            this.colYear5DeferralUnits.OptionsColumn.AllowShowHide = false;
            this.colYear5DeferralUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year5DeferralUnits", "{0:f0}")});
            this.colYear5DeferralUnits.Visible = true;
            this.colYear5DeferralUnits.VisibleIndex = 19;
            // 
            // colYear5DeferralCost
            // 
            this.colYear5DeferralCost.Caption = "Cost";
            this.colYear5DeferralCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear5DeferralCost.CustomizationCaption = "Year 5 Deferral Cost";
            this.colYear5DeferralCost.FieldName = "Year5DeferralCost";
            this.colYear5DeferralCost.Name = "colYear5DeferralCost";
            this.colYear5DeferralCost.OptionsColumn.AllowShowHide = false;
            this.colYear5DeferralCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year5DeferralCost", "{0:c}")});
            this.colYear5DeferralCost.Visible = true;
            this.colYear5DeferralCost.VisibleIndex = 20;
            this.colYear5DeferralCost.Width = 100;
            // 
            // colYear1RevisitUnits
            // 
            this.colYear1RevisitUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear1RevisitUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear1RevisitUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear1RevisitUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear1RevisitUnits.Caption = "Number";
            this.colYear1RevisitUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear1RevisitUnits.CustomizationCaption = "Year 1 Revisit Number";
            this.colYear1RevisitUnits.FieldName = "Year1RevisitUnits";
            this.colYear1RevisitUnits.Name = "colYear1RevisitUnits";
            this.colYear1RevisitUnits.OptionsColumn.AllowShowHide = false;
            this.colYear1RevisitUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year1RevisitUnits", "{0:f0}")});
            this.colYear1RevisitUnits.Visible = true;
            this.colYear1RevisitUnits.VisibleIndex = 21;
            // 
            // colYear1RevisitCost
            // 
            this.colYear1RevisitCost.Caption = "Cost";
            this.colYear1RevisitCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear1RevisitCost.CustomizationCaption = "Year 1 Revisit Cost";
            this.colYear1RevisitCost.FieldName = "Year1RevisitCost";
            this.colYear1RevisitCost.Name = "colYear1RevisitCost";
            this.colYear1RevisitCost.OptionsColumn.AllowShowHide = false;
            this.colYear1RevisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year1RevisitCost", "{0:c}")});
            this.colYear1RevisitCost.Visible = true;
            this.colYear1RevisitCost.VisibleIndex = 22;
            this.colYear1RevisitCost.Width = 100;
            // 
            // colYear2RevisitUnits
            // 
            this.colYear2RevisitUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear2RevisitUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear2RevisitUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear2RevisitUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear2RevisitUnits.Caption = "Number";
            this.colYear2RevisitUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear2RevisitUnits.CustomizationCaption = "Year 2 Revisit Number";
            this.colYear2RevisitUnits.FieldName = "Year2RevisitUnits";
            this.colYear2RevisitUnits.Name = "colYear2RevisitUnits";
            this.colYear2RevisitUnits.OptionsColumn.AllowShowHide = false;
            this.colYear2RevisitUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year2RevisitUnits", "{0:f0}")});
            this.colYear2RevisitUnits.Visible = true;
            this.colYear2RevisitUnits.VisibleIndex = 23;
            // 
            // colYear2RevisitCost
            // 
            this.colYear2RevisitCost.Caption = "Cost";
            this.colYear2RevisitCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear2RevisitCost.CustomizationCaption = "Year 2 Revisit Cost";
            this.colYear2RevisitCost.FieldName = "Year2RevisitCost";
            this.colYear2RevisitCost.Name = "colYear2RevisitCost";
            this.colYear2RevisitCost.OptionsColumn.AllowShowHide = false;
            this.colYear2RevisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year2RevisitCost", "{0:c}")});
            this.colYear2RevisitCost.Visible = true;
            this.colYear2RevisitCost.VisibleIndex = 24;
            this.colYear2RevisitCost.Width = 100;
            // 
            // colYear3RevisitUnits
            // 
            this.colYear3RevisitUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear3RevisitUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear3RevisitUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear3RevisitUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear3RevisitUnits.Caption = "Number";
            this.colYear3RevisitUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear3RevisitUnits.CustomizationCaption = "Year 3 Revisit Number";
            this.colYear3RevisitUnits.FieldName = "Year3RevisitUnits";
            this.colYear3RevisitUnits.Name = "colYear3RevisitUnits";
            this.colYear3RevisitUnits.OptionsColumn.AllowShowHide = false;
            this.colYear3RevisitUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year3RevisitUnits", "{0:f0}")});
            this.colYear3RevisitUnits.Visible = true;
            this.colYear3RevisitUnits.VisibleIndex = 25;
            // 
            // colYear3RevisitCost
            // 
            this.colYear3RevisitCost.Caption = "Cost";
            this.colYear3RevisitCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear3RevisitCost.CustomizationCaption = "Year 3 Revisit Cost";
            this.colYear3RevisitCost.FieldName = "Year3RevisitCost";
            this.colYear3RevisitCost.Name = "colYear3RevisitCost";
            this.colYear3RevisitCost.OptionsColumn.AllowShowHide = false;
            this.colYear3RevisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year3RevisitCost", "{0:c}")});
            this.colYear3RevisitCost.Visible = true;
            this.colYear3RevisitCost.VisibleIndex = 26;
            this.colYear3RevisitCost.Width = 100;
            // 
            // colYear4RevisitUnits
            // 
            this.colYear4RevisitUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear4RevisitUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear4RevisitUnits.Caption = "Number";
            this.colYear4RevisitUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear4RevisitUnits.CustomizationCaption = "Year 4 Revisit Number";
            this.colYear4RevisitUnits.FieldName = "Year4RevisitUnits";
            this.colYear4RevisitUnits.Name = "colYear4RevisitUnits";
            this.colYear4RevisitUnits.OptionsColumn.AllowShowHide = false;
            this.colYear4RevisitUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year4RevisitUnits", "{0:f0}")});
            this.colYear4RevisitUnits.Visible = true;
            this.colYear4RevisitUnits.VisibleIndex = 27;
            // 
            // colYear4RevisitCost
            // 
            this.colYear4RevisitCost.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear4RevisitCost.AppearanceCell.Options.UseForeColor = true;
            this.colYear4RevisitCost.Caption = "Cost";
            this.colYear4RevisitCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear4RevisitCost.CustomizationCaption = "Year 4 Revisit Cost";
            this.colYear4RevisitCost.FieldName = "Year4RevisitCost";
            this.colYear4RevisitCost.Name = "colYear4RevisitCost";
            this.colYear4RevisitCost.OptionsColumn.AllowShowHide = false;
            this.colYear4RevisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year4RevisitCost", "{0:c}")});
            this.colYear4RevisitCost.Visible = true;
            this.colYear4RevisitCost.VisibleIndex = 28;
            this.colYear4RevisitCost.Width = 100;
            // 
            // colYear5RevisitUnits
            // 
            this.colYear5RevisitUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colYear5RevisitUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colYear5RevisitUnits.AppearanceCell.Options.UseBackColor = true;
            this.colYear5RevisitUnits.AppearanceCell.Options.UseForeColor = true;
            this.colYear5RevisitUnits.Caption = "Number";
            this.colYear5RevisitUnits.ColumnEdit = this.repositoryItemSpinEditUnits;
            this.colYear5RevisitUnits.CustomizationCaption = "Year 5 Revisit Number";
            this.colYear5RevisitUnits.FieldName = "Year5RevisitUnits";
            this.colYear5RevisitUnits.Name = "colYear5RevisitUnits";
            this.colYear5RevisitUnits.OptionsColumn.AllowShowHide = false;
            this.colYear5RevisitUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year5RevisitUnits", "{0:f0}")});
            this.colYear5RevisitUnits.Visible = true;
            this.colYear5RevisitUnits.VisibleIndex = 29;
            // 
            // colYear5RevisitCost
            // 
            this.colYear5RevisitCost.Caption = "Cost";
            this.colYear5RevisitCost.ColumnEdit = this.repositoryItemSpinEditCost;
            this.colYear5RevisitCost.CustomizationCaption = "Year 5 Revisit Cost";
            this.colYear5RevisitCost.FieldName = "Year5RevisitCost";
            this.colYear5RevisitCost.Name = "colYear5RevisitCost";
            this.colYear5RevisitCost.OptionsColumn.AllowShowHide = false;
            this.colYear5RevisitCost.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Year5RevisitCost", "{0:c}")});
            this.colYear5RevisitCost.Visible = true;
            this.colYear5RevisitCost.VisibleIndex = 30;
            this.colYear5RevisitCost.Width = 100;
            // 
            // colTotalUnits
            // 
            this.colTotalUnits.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.colTotalUnits.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colTotalUnits.AppearanceCell.Options.UseBackColor = true;
            this.colTotalUnits.AppearanceCell.Options.UseForeColor = true;
            this.colTotalUnits.Caption = "Total Number";
            this.colTotalUnits.ColumnEdit = this.repositoryItemTextEditTotalUnits;
            this.colTotalUnits.FieldName = "TotalUnits";
            this.colTotalUnits.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colTotalUnits.Name = "colTotalUnits";
            this.colTotalUnits.OptionsColumn.AllowEdit = false;
            this.colTotalUnits.OptionsColumn.AllowFocus = false;
            this.colTotalUnits.OptionsColumn.AllowShowHide = false;
            this.colTotalUnits.OptionsColumn.ReadOnly = true;
            this.colTotalUnits.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalUnits", "{0:f0}")});
            this.colTotalUnits.Visible = true;
            this.colTotalUnits.VisibleIndex = 32;
            // 
            // repositoryItemTextEditTotalUnits
            // 
            this.repositoryItemTextEditTotalUnits.AutoHeight = false;
            this.repositoryItemTextEditTotalUnits.Mask.EditMask = "n0";
            this.repositoryItemTextEditTotalUnits.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditTotalUnits.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTotalUnits.Name = "repositoryItemTextEditTotalUnits";
            // 
            // colTotalCost1
            // 
            this.colTotalCost1.Caption = "Total Cost";
            this.colTotalCost1.ColumnEdit = this.repositoryItemTextEditTotalCost;
            this.colTotalCost1.FieldName = "TotalCost";
            this.colTotalCost1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colTotalCost1.Name = "colTotalCost1";
            this.colTotalCost1.OptionsColumn.AllowEdit = false;
            this.colTotalCost1.OptionsColumn.AllowFocus = false;
            this.colTotalCost1.OptionsColumn.AllowShowHide = false;
            this.colTotalCost1.OptionsColumn.ReadOnly = true;
            this.colTotalCost1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalCost", "{0:c}")});
            this.colTotalCost1.Visible = true;
            this.colTotalCost1.VisibleIndex = 33;
            this.colTotalCost1.Width = 100;
            // 
            // repositoryItemTextEditTotalCost
            // 
            this.repositoryItemTextEditTotalCost.AutoHeight = false;
            this.repositoryItemTextEditTotalCost.Mask.EditMask = "c";
            this.repositoryItemTextEditTotalCost.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditTotalCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTotalCost.Name = "repositoryItemTextEditTotalCost";
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.AllowShowHide = false;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 31;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowShowHide = false;
            this.colTypeDescription.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 0;
            this.colTypeDescription.Width = 70;
            // 
            // colTypeOrder
            // 
            this.colTypeOrder.Caption = "Type Order";
            this.colTypeOrder.FieldName = "TypeOrder";
            this.colTypeOrder.Name = "colTypeOrder";
            this.colTypeOrder.OptionsColumn.AllowShowHide = false;
            this.colTypeOrder.Width = 111;
            // 
            // sp07220_UT_Survey_Costing_HeadersTableAdapter
            // 
            this.sp07220_UT_Survey_Costing_HeadersTableAdapter.ClearBeforeFill = true;
            // 
            // sp07221_UT_Survey_Costing_PoleTableAdapter
            // 
            this.sp07221_UT_Survey_Costing_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // sp07222_UT_Survey_Costing_ItemTableAdapter
            // 
            this.sp07222_UT_Survey_Costing_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl6;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp07138_UT_Unit_Descriptors_With_BlankTableAdapter
            // 
            this.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // pmSurveyPoles
            // 
            this.pmSurveyPoles.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddPoles),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearPoles)});
            this.pmSurveyPoles.Manager = this.barManager1;
            this.pmSurveyPoles.MenuCaption = "Survey Sites";
            this.pmSurveyPoles.Name = "pmSurveyPoles";
            // 
            // bbiAddPoles
            // 
            this.bbiAddPoles.Caption = "Add Poles";
            this.bbiAddPoles.Id = 27;
            this.bbiAddPoles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddPoles.ImageOptions.Image")));
            this.bbiAddPoles.Name = "bbiAddPoles";
            this.bbiAddPoles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddPoles_ItemClick);
            // 
            // bbiClearPoles
            // 
            this.bbiClearPoles.Caption = "Clear Poles";
            this.bbiClearPoles.Id = 28;
            this.bbiClearPoles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClearPoles.ImageOptions.Image")));
            this.bbiClearPoles.Name = "bbiClearPoles";
            this.bbiClearPoles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearPoles_ItemClick);
            // 
            // sp07328_UT_Surveyor_List_With_BlankTableAdapter
            // 
            this.sp07328_UT_Surveyor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter
            // 
            this.sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter
            // 
            this.sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // sp07468_UT_Contract_Types_With_BlankTableAdapter
            // 
            this.sp07468_UT_Contract_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Survey_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(908, 712);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Survey_Edit";
            this.Text = "Edit Survey";
            this.Activated += new System.EventHandler(this.frm_UT_Survey_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Survey_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Survey_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditEditTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ContractTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07103UTSurveyItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07468UTContractTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetWorkCompletionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetWorkCompletionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetPermissionCompletionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetPermissionCompletionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetSurveyCompletionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetSurveyCompletionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07466UTPermissionDocumentsForSurveyedPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractYearSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCategoryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07405UTReactiveCategoryTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByTelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByPostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByAddressMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteApprovedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteApprovedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteReceivedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuoteReceivedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExchequerNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientPONumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalIncomeSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapRecordIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkspaceNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapRecordDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MapStartTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07105UTMapStartTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReactiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07328UTSurveyorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyStatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07106UTSurveyStatusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapYTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LastMapXTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07107UTSurveyItemLinkedSurveyedPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapRecordID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkspaceName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMapStartTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLastMapRecordDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetSurveyCompletionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetPermissionCompletionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTargetWorkCompletionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLoadSurveyIntoMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientPONumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExchequerNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteReceivedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteApprovedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportedByEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyAddressButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactiveRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIncidentPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCopyAddressButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReactiveCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalIncome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07060UTTreeSpeciesLinkedToTreeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07220UTSurveyCostingHeadersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07221UTSurveyCostingPoleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEditPoleNumbers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07222UTSurveyCostingItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditResource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07138UTUnitDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmSurveyPoles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit SurveyIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoleName;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DataSet_UT dataSet_UT;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.TextEdit LastMapXTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastMapX;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.TextEdit LastMapYTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastMapY;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp07060UTTreeSpeciesLinkedToTreeBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_UT_EditTableAdapters.sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter sp07060_UT_Tree_Species_Linked_To_TreeTableAdapter;
        private System.Windows.Forms.BindingSource sp07103UTSurveyItemBindingSource;
        private DataSet_UT_EditTableAdapters.sp07103_UT_Survey_ItemTableAdapter sp07103_UT_Survey_ItemTableAdapter;
        private DevExpress.XtraEditors.DateEdit SurveyDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyDate;
        private DevExpress.XtraEditors.GridLookUpEdit SurveyStatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyStatusID;
        private DevExpress.XtraEditors.GridLookUpEdit SurveyorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyorID;
        private DevExpress.XtraEditors.CheckEdit ReactiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactive;
        private DevExpress.XtraEditors.GridLookUpEdit MapStartTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMapStartTypeID;
        private DevExpress.XtraEditors.TextEdit LastMapRecordIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastMapRecordID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.BindingSource sp07105UTMapStartTypesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DataSet_UT_EditTableAdapters.sp07105_UT_Map_Start_TypesTableAdapter sp07105_UT_Map_Start_TypesTableAdapter;
        private System.Windows.Forms.BindingSource sp07106UTSurveyStatusesBindingSource;
        private DataSet_UT_EditTableAdapters.sp07106_UT_Survey_StatusesTableAdapter sp07106_UT_Survey_StatusesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private System.Windows.Forms.BindingSource sp07107UTSurveyItemLinkedSurveyedPolesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DataSet_UT_EditTableAdapters.sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter sp07107_UT_Survey_Item_Linked_Surveyed_PolesTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit LastMapRecordDescriptionButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLastMapRecordDescription;
        private DevExpress.XtraEditors.TextEdit WorkspaceIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkspaceID;
        private DevExpress.XtraEditors.TextEdit WorkspaceNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkspaceName;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SimpleButton btnLoadMapWithSurvey;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLoadSurveyIntoMap;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraEditors.SpinEdit TotalCostSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraEditors.SpinEdit TotalIncomeSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalIncome;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit ExchequerNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit ClientPONumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientPONumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private System.Windows.Forms.BindingSource sp07220UTSurveyCostingHeadersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyCostingHeaderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLive;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalIncome;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DataSet_UTTableAdapters.sp07220_UT_Survey_Costing_HeadersTableAdapter sp07220_UT_Survey_Costing_HeadersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private System.Windows.Forms.BindingSource sp07221UTSurveyCostingPoleBindingSource;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView3;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurveyCostingPoleID;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurveyCostingHeaderID1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLiveWorkDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLiveWorkPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colLiveWorkPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD1Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD1PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD1Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD2PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD3PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD4PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD5PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD6PoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3DeferralPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5DeferralPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear1RevisitPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear2RevisitPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3RevisitPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear4RevisitPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5RevisitPoleIDs;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRemarks2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD2Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD2Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD3Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD3Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD4Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD4Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD5Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD5Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD6Date;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSD6Poles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3DeferralDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3DeferralPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5DeferralDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5DeferralPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear1RevisitDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear1RevisitPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear2RevisitDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear2RevisitPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3RevisitDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear3RevisitPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear4RevisitDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear4RevisitPoles;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5RevisitDate;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colYear5RevisitPoles;
        private DataSet_UT_EditTableAdapters.sp07221_UT_Survey_Costing_PoleTableAdapter sp07221_UT_Survey_Costing_PoleTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEditPoleNumbers;
        private System.Windows.Forms.BindingSource sp07222UTSurveyCostingItemBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyCostingItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyCostingHeaderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colCostingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription3;
        private DevExpress.XtraGrid.Columns.GridColumn colRate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colLiveWorkUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colLiveWorkCost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD1Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD1Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD2Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD2Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD3Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD3Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD4Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD4Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD5Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD5Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colSD6Units;
        private DevExpress.XtraGrid.Columns.GridColumn colSD6Cost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear3DeferralUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear3DeferralCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear5DeferralUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear5DeferralCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear1RevisitUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear1RevisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear2RevisitUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear2RevisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear3RevisitUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear3RevisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear4RevisitUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear4RevisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colYear5RevisitUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colYear5RevisitCost;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOrder;
        private DataSet_UT_EditTableAdapters.sp07222_UT_Survey_Costing_ItemTableAdapter sp07222_UT_Survey_Costing_ItemTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTotalUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTotalCost;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditUnits;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditResource;
        private System.Windows.Forms.BindingSource sp07138UTUnitDescriptorsWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07138_UT_Unit_Descriptors_With_BlankTableAdapter sp07138_UT_Unit_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription4;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand17;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand18;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand19;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand9;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand10;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand11;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand12;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand13;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand14;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand15;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand20;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarCheckItem barCheckItemSurveyDetails;
        private DevExpress.XtraBars.BarCheckItem barCheckItemSurveyCostings;
        private DevExpress.XtraBars.BarEditItem barEditItemEditTotals;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditEditTotals;
        private DevExpress.XtraBars.BarCheckItem barCheckItemEnableTotals;
        private DevExpress.XtraBars.BarButtonItem bbiAddPoles;
        private DevExpress.XtraBars.PopupMenu pmSurveyPoles;
        private DevExpress.XtraBars.BarButtonItem bbiClearPoles;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private System.Windows.Forms.BindingSource sp07328UTSurveyorListWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07328_UT_Surveyor_List_With_BlankTableAdapter sp07328_UT_Surveyor_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.DateEdit QuoteApprovedDateDateEdit;
        private DevExpress.XtraEditors.DateEdit QuoteReceivedDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteReceivedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteApprovedDate;
        private DevExpress.XtraEditors.SimpleButton btnCalculateTotals;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.MemoEdit ReportedByAddressMemoEdit;
        private DevExpress.XtraEditors.TextEdit ReportedByNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByAddress;
        private DevExpress.XtraEditors.TextEdit ReportedByTelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit ReportedByPostcodeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByPostcode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByTelephone;
        private DevExpress.XtraEditors.MemoEdit ReactiveRemarksMemoEdit;
        private DevExpress.XtraEditors.TextEdit ReportedByEmailTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportedByEmail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactiveRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.TextEdit IncidentPostcodeTextEdit;
        private DevExpress.XtraEditors.MemoEdit IncidentAddressMemoEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentAddress;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIncidentPostcode;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.GridLookUpEdit ReactiveCategoryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReactiveCategoryID;
        private System.Windows.Forms.BindingSource sp07405UTReactiveCategoryTypesWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter sp07405_UT_Reactive_Category_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.SimpleButton CopyAddressButton2;
        private DevExpress.XtraEditors.SimpleButton CopyAddressButton1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCopyAddressButton1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCopyAddressButton2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanResilient;
        private DevExpress.XtraGrid.Columns.GridColumn colIvyOnPole;
        private DevExpress.XtraEditors.SpinEdit ContractYearSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractYear;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinearCutLength;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private System.Windows.Forms.BindingSource sp07466UTPermissionDocumentsForSurveyedPoleBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter sp07466_UT_Permission_Documents_For_Surveyed_PoleTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureFile;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPDFFile;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyAccess;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestTelephonePoint;
        private DevExpress.XtraGrid.Columns.GridColumn colGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestAandE;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveAccessAvailable;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessAgreedWithLandOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessDetailsOnMap;
        private DevExpress.XtraGrid.Columns.GridColumn colRoadsideAccessOnly;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTPOTree;
        private DevExpress.XtraGrid.Columns.GridColumn colPlanningConservation;
        private DevExpress.XtraGrid.Columns.GridColumn colWildlifeDesignation;
        private DevExpress.XtraGrid.Columns.GridColumn colSpecialInstructions;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimarySubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLineNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubName;
        private DevExpress.XtraGrid.Columns.GridColumn colLVSubNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colManagedUnitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colEnvironmentalRANumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLandscapeImpactNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteGridReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colPostageRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByPost;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerSalutation;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipRemove;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsChipOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsStackOnSite;
        private DevExpress.XtraGrid.Columns.GridColumn colArisingsOther;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToDoActionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colNotPermissionedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldCount;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedCount;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colLandownerRestrictedCut1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraEditors.DateEdit TargetSurveyCompletionDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetSurveyCompletionDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraEditors.DateEdit TargetWorkCompletionDateDateEdit;
        private DevExpress.XtraEditors.DateEdit TargetPermissionCompletionDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetPermissionCompletionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTargetWorkCompletionDate;
        private DevExpress.XtraEditors.GridLookUpEdit ContractTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractTypeID;
        private System.Windows.Forms.BindingSource sp07468UTContractTypesWithBlankBindingSource;
        private DataSet_UTTableAdapters.sp07468_UT_Contract_Types_With_BlankTableAdapter sp07468_UT_Contract_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredEstimatedCuttingHours;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceAbove;
    }
}

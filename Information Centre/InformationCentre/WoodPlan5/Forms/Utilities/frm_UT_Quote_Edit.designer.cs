﻿namespace WoodPlan5
{
    partial class frm_UT_Quote_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Quote_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Doughnut3DSeriesLabel doughnut3DSeriesLabel1 = new DevExpress.XtraCharts.Doughnut3DSeriesLabel();
            DevExpress.XtraCharts.Doughnut3DSeriesView doughnut3DSeriesView1 = new DevExpress.XtraCharts.Doughnut3DSeriesView();
            DevExpress.XtraCharts.Doughnut3DSeriesLabel doughnut3DSeriesLabel2 = new DevExpress.XtraCharts.Doughnut3DSeriesLabel();
            DevExpress.XtraCharts.Doughnut3DSeriesView doughnut3DSeriesView2 = new DevExpress.XtraCharts.Doughnut3DSeriesView();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D2 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Doughnut3DSeriesLabel doughnut3DSeriesLabel3 = new DevExpress.XtraCharts.Doughnut3DSeriesLabel();
            DevExpress.XtraCharts.Doughnut3DSeriesView doughnut3DSeriesView3 = new DevExpress.XtraCharts.Doughnut3DSeriesView();
            DevExpress.XtraCharts.Doughnut3DSeriesLabel doughnut3DSeriesLabel4 = new DevExpress.XtraCharts.Doughnut3DSeriesLabel();
            DevExpress.XtraCharts.Doughnut3DSeriesView doughnut3DSeriesView4 = new DevExpress.XtraCharts.Doughnut3DSeriesView();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView2 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGauges.Core.Model.ArcScaleRange arcScaleRange1 = new DevExpress.XtraGauges.Core.Model.ArcScaleRange();
            DevExpress.XtraGauges.Core.Model.ArcScaleRange arcScaleRange2 = new DevExpress.XtraGauges.Core.Model.ArcScaleRange();
            DevExpress.XtraGauges.Core.Model.ArcScaleRange arcScaleRange3 = new DevExpress.XtraGauges.Core.Model.ArcScaleRange();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.sp07421_UT_Quote_Item_ListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiSaveReview = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormAddAnother = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.sp07418_UT_Quote_ListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtTotalNetPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalNetCost = new DevExpress.XtraEditors.TextEdit();
            this.txtTotalNetProfit = new DevExpress.XtraEditors.TextEdit();
            this.chartControl2 = new DevExpress.XtraCharts.ChartControl();
            this.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1 = new WoodPlan5.DataSet_AT_TreePickerTableAdapters.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter();
            this.chartControl3 = new DevExpress.XtraCharts.ChartControl();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.gaugeControl1 = new DevExpress.XtraGauges.Win.GaugeControl();
            this.linearGauge1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearGauge();
            this.labelComponent1 = new DevExpress.XtraGauges.Win.Base.LabelComponent();
            this.linearScaleMarkerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleMarkerComponent();
            this.linearScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent();
            this.linearScaleRangeBarComponent1 = new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleRangeBarComponent();
            this.lueAnalysisCodeID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07446UTPLAnalysisCodeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueFeederContractID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07453UTFeederContractListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueCircuitID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07452UTCircuitListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueSubContractorID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07450UTSubContractorListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07421_UT_Quote_Item_ListGridControl = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQuoteItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRateVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellRateVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellRateTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRateTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFreeTextRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRating = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.quoteItemDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.spnBuyRateVAT = new DevExpress.XtraEditors.SpinEdit();
            this.spnSellRateVAT = new DevExpress.XtraEditors.SpinEdit();
            this.lueClientID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07427_UT_Client_ListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueQuoteCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07424UTQuoteCategoryItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memDescription = new DevExpress.XtraEditors.MemoEdit();
            this.spnSellRate = new DevExpress.XtraEditors.SpinEdit();
            this.spnSellRateTotal = new DevExpress.XtraEditors.SpinEdit();
            this.spnBuyRate = new DevExpress.XtraEditors.SpinEdit();
            this.spnBuyRateTotal = new DevExpress.XtraEditors.SpinEdit();
            this.spnFreeTextRate = new DevExpress.XtraEditors.SpinEdit();
            this.spnNetPrice = new DevExpress.XtraEditors.SpinEdit();
            this.spnNetCost = new DevExpress.XtraEditors.SpinEdit();
            this.spnMarkUp = new DevExpress.XtraEditors.SpinEdit();
            this.spnQuantity = new DevExpress.XtraEditors.SpinEdit();
            this.lueItemUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07435UTItemUnitsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueRating = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07446UTPicklistByHeaderIDBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbResourceRateID = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.sp07438_UT_Resource_Rate_ListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbQuoteCategoryID = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.sp07424_UT_Quote_Category_Item_AddModeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtReference = new DevExpress.XtraEditors.TextEdit();
            this.txtPaddedWorkOrderID = new DevExpress.XtraEditors.TextEdit();
            this.lueClientContractTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07431UTClientContractTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ceIsMainQuote = new DevExpress.XtraEditors.CheckEdit();
            this.ceTransferredExchequer = new DevExpress.XtraEditors.CheckEdit();
            this.txtPONumber = new DevExpress.XtraEditors.TextEdit();
            this.memNotes = new DevExpress.XtraEditors.MemoEdit();
            this.lueBillingCentreCodeID = new DevExpress.XtraEditors.SearchLookUpEdit();
            this.spAS11038BillingCentreCodeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.searchLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBillingCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.tcgDetail = new DevExpress.XtraLayout.TabbedControlGroup();
            this.lcgMain = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgQuote = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciReference = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciClient = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciProfit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgQuoteItemBlockAdd = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcgQuoteItem = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciFreeTextRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup8 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lciQuoteItemList = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tcgClient = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciSellRateTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.tcgFieldTeam = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciBuyRateTotal = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lciMarkUp = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgQuotePerformance = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07418_UT_Quote_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07418_UT_Quote_ListTableAdapter();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_UT_QuoteTableAdapters.TableAdapterManager();
            this.sp07421_UT_Quote_Item_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07421_UT_Quote_Item_ListTableAdapter();
            this.sp07424_UT_Quote_Category_Item_AddModeTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_Item_AddModeTableAdapter();
            this.sp07424_UT_Quote_Category_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_ItemTableAdapter();
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter();
            this.sp07438_UT_Resource_Rate_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter();
            this.sp07427_UT_Client_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07427_UT_Client_ListTableAdapter();
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter();
            this.sp07446_UT_Picklist_By_Header_IDTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07446_UT_Picklist_By_Header_IDTableAdapter();
            this.sp07435_UT_Item_Units_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07435_UT_Item_Units_ListTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp07450_UT_SubContractor_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07450_UT_SubContractor_ListTableAdapter();
            this.sp07452_UT_Circuit_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07452_UT_Circuit_ListTableAdapter();
            this.sp07453_UT_FeederContract_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07453_UT_FeederContract_ListTableAdapter();
            this.sp07446_UT_PL_AnalysisCodeTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07446_UT_PL_AnalysisCodeTableAdapter();
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.sp07009_UT_Region_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07009_UT_Region_ItemTableAdapter();
            this.arcScaleSpindleCapComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent();
            this.arcScaleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleMarkerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleMarkerComponent();
            this.arcScaleNeedleComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleBackgroundLayerComponent1 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleBackgroundLayerComponent3 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent();
            this.arcScaleComponent3 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent();
            this.arcScaleNeedleComponent3 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent();
            this.arcScaleSpindleCapComponent3 = new DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07421_UT_Quote_Item_ListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07418_UT_Quote_ListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetProfit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleMarkerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleRangeBarComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAnalysisCodeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07446UTPLAnalysisCodeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFeederContractID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07453UTFeederContractListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCircuitID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07452UTCircuitListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSubContractorID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07450UTSubContractorListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07421_UT_Quote_Item_ListGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRateVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRateVAT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07427_UT_Client_ListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQuoteCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424UTQuoteCategoryItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRateTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRateTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnFreeTextRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMarkUp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueItemUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07435UTItemUnitsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRating.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07446UTPicklistByHeaderIDBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbResourceRateID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438_UT_Resource_Rate_ListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuoteCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424_UT_Quote_Category_Item_AddModeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaddedWorkOrderID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientContractTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsMainQuote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTransferredExchequer.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPONumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBillingCentreCodeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProfit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuoteItemBlockAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuoteItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFreeTextRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQuoteItemList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgClient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSellRateTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgFieldTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBuyRateTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMarkUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuotePerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleMarkerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1096, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1096, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1096, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation,
            this.bbiFormAddAnother,
            this.bbiSaveReview});
            this.barManager1.MaxItemId = 37;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // sp07421_UT_Quote_Item_ListBindingSource
            // 
            this.sp07421_UT_Quote_Item_ListBindingSource.DataMember = "sp07421_UT_Quote_Item_List";
            this.sp07421_UT_Quote_Item_ListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.barAndDockingController1.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveReview),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormAddAnother, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiSaveReview
            // 
            this.bbiSaveReview.Caption = "Save && Review";
            this.bbiSaveReview.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveReview.Glyph")));
            this.bbiSaveReview.Id = 36;
            this.bbiSaveReview.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveReview.LargeGlyph")));
            this.bbiSaveReview.Name = "bbiSaveReview";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Save and Review quote</b>.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiSaveReview.SuperTip = superToolTip2;
            this.bbiSaveReview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveReview_ItemClick);
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Text = "Save Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormSave.SuperTip = superToolTip3;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormAddAnother
            // 
            this.bbiFormAddAnother.Caption = "Add Quote Item";
            this.bbiFormAddAnother.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormAddAnother.Glyph")));
            this.bbiFormAddAnother.Id = 35;
            this.bbiFormAddAnother.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormAddAnother.LargeGlyph")));
            this.bbiFormAddAnother.Name = "bbiFormAddAnother";
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Save & Add Button - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to save all outstanding changes and <b>add another quote item</b>.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiFormAddAnother.SuperTip = superToolTip4;
            this.bbiFormAddAnother.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormAddAnother_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Text = "Cancel Button - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiFormCancel.SuperTip = superToolTip5;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.tcgDetail});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1079, 1228);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 1198);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1059, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.editFormDataNavigator;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(422, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.sp07418_UT_Quote_ListBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(434, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(173, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 124;
            this.editFormDataNavigator.Text = "quoteDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.editFormDataNavigator.TextStringFormat = "Quote {0} of {1}";
            // 
            // sp07418_UT_Quote_ListBindingSource
            // 
            this.sp07418_UT_Quote_ListBindingSource.DataMember = "sp07418_UT_Quote_List";
            this.sp07418_UT_Quote_ListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtTotalNetPrice);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtTotalNetCost);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtTotalNetProfit);
            this.editFormDataLayoutControlGroup.Controls.Add(this.chartControl2);
            this.editFormDataLayoutControlGroup.Controls.Add(this.chartControl3);
            this.editFormDataLayoutControlGroup.Controls.Add(this.chartControl1);
            this.editFormDataLayoutControlGroup.Controls.Add(this.gaugeControl1);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueAnalysisCodeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueFeederContractID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueCircuitID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueSubContractorID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.sp07421_UT_Quote_Item_ListGridControl);
            this.editFormDataLayoutControlGroup.Controls.Add(this.quoteItemDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnBuyRateVAT);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnSellRateVAT);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueClientID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueQuoteCategoryID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memDescription);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnSellRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnSellRateTotal);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnBuyRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnBuyRateTotal);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnFreeTextRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnNetPrice);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnNetCost);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnMarkUp);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnQuantity);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueItemUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueRating);
            this.editFormDataLayoutControlGroup.Controls.Add(this.cmbResourceRateID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.cmbQuoteCategoryID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtReference);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPaddedWorkOrderID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueClientContractTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceIsMainQuote);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceTransferredExchequer);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtPONumber);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memNotes);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueBillingCentreCodeID);
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.layoutControlItem20});
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(790, 164, 513, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1096, 692);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtTotalNetPrice
            // 
            this.txtTotalNetPrice.Location = new System.Drawing.Point(684, 254);
            this.txtTotalNetPrice.MenuManager = this.barManager1;
            this.txtTotalNetPrice.Name = "txtTotalNetPrice";
            this.txtTotalNetPrice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalNetPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txtTotalNetPrice.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalNetPrice.Properties.Appearance.Options.UseFont = true;
            this.txtTotalNetPrice.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtTotalNetPrice, true);
            this.txtTotalNetPrice.Size = new System.Drawing.Size(347, 22);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtTotalNetPrice, optionsSpelling1);
            this.txtTotalNetPrice.StyleController = this.editFormDataLayoutControlGroup;
            this.txtTotalNetPrice.TabIndex = 175;
            // 
            // txtTotalNetCost
            // 
            this.txtTotalNetCost.Location = new System.Drawing.Point(684, 280);
            this.txtTotalNetCost.MenuManager = this.barManager1;
            this.txtTotalNetCost.Name = "txtTotalNetCost";
            this.txtTotalNetCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalNetCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txtTotalNetCost.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalNetCost.Properties.Appearance.Options.UseFont = true;
            this.txtTotalNetCost.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtTotalNetCost, true);
            this.txtTotalNetCost.Size = new System.Drawing.Size(347, 22);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtTotalNetCost, optionsSpelling2);
            this.txtTotalNetCost.StyleController = this.editFormDataLayoutControlGroup;
            this.txtTotalNetCost.TabIndex = 174;
            // 
            // txtTotalNetProfit
            // 
            this.txtTotalNetProfit.Location = new System.Drawing.Point(684, 306);
            this.txtTotalNetProfit.MenuManager = this.barManager1;
            this.txtTotalNetProfit.Name = "txtTotalNetProfit";
            this.txtTotalNetProfit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtTotalNetProfit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txtTotalNetProfit.Properties.Appearance.Options.UseBackColor = true;
            this.txtTotalNetProfit.Properties.Appearance.Options.UseFont = true;
            this.txtTotalNetProfit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtTotalNetProfit, true);
            this.txtTotalNetProfit.Size = new System.Drawing.Size(347, 22);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtTotalNetProfit, optionsSpelling3);
            this.txtTotalNetProfit.StyleController = this.editFormDataLayoutControlGroup;
            this.txtTotalNetProfit.TabIndex = 173;
            // 
            // chartControl2
            // 
            this.chartControl2.DataAdapter = this.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1;
            simpleDiagram3D1.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            this.chartControl2.Diagram = simpleDiagram3D1;
            this.chartControl2.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl2.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Bottom;
            this.chartControl2.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl2.Legend.EquallySpacedItems = false;
            this.chartControl2.Location = new System.Drawing.Point(550, 778);
            this.chartControl2.Name = "chartControl2";
            series1.ArgumentDataMember = "Description";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series1.DataSource = this.sp07421_UT_Quote_Item_ListBindingSource;
            doughnut3DSeriesLabel1.TextPattern = "{VP:P2}";
            series1.Label = doughnut3DSeriesLabel1;
            series1.LegendTextPattern = "{A}";
            series1.Name = "Series 1";
            series1.ValueDataMembersSerializable = "NetCost";
            doughnut3DSeriesView1.SizeAsPercentage = 100D;
            doughnut3DSeriesView1.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            series1.View = doughnut3DSeriesView1;
            this.chartControl2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            doughnut3DSeriesLabel2.TextPattern = "{VP:G4}";
            this.chartControl2.SeriesTemplate.Label = doughnut3DSeriesLabel2;
            doughnut3DSeriesView2.SizeAsPercentage = 100D;
            doughnut3DSeriesView2.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            this.chartControl2.SeriesTemplate.View = doughnut3DSeriesView2;
            this.chartControl2.Size = new System.Drawing.Size(435, 356);
            this.chartControl2.TabIndex = 169;
            // 
            // sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1
            // 
            this.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1.ClearBeforeFill = true;
            // 
            // chartControl3
            // 
            this.chartControl3.AppearanceNameSerializable = "Chameleon";
            this.chartControl3.DataAdapter = this.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1;
            simpleDiagram3D2.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            this.chartControl3.Diagram = simpleDiagram3D2;
            this.chartControl3.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl3.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Bottom;
            this.chartControl3.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl3.Legend.EquallySpacedItems = false;
            this.chartControl3.Location = new System.Drawing.Point(94, 778);
            this.chartControl3.Name = "chartControl3";
            series2.ArgumentDataMember = "Description";
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series2.DataSource = this.sp07421_UT_Quote_Item_ListBindingSource;
            doughnut3DSeriesLabel3.TextPattern = "{VP:P0}";
            series2.Label = doughnut3DSeriesLabel3;
            series2.LegendTextPattern = "{A}";
            series2.Name = "Series 1";
            series2.ValueDataMembersSerializable = "NetPrice";
            doughnut3DSeriesView3.HoleRadiusPercent = 70;
            doughnut3DSeriesView3.SizeAsPercentage = 100D;
            doughnut3DSeriesView3.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            series2.View = doughnut3DSeriesView3;
            this.chartControl3.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            doughnut3DSeriesLabel4.TextPattern = "{VP:G4}";
            this.chartControl3.SeriesTemplate.Label = doughnut3DSeriesLabel4;
            doughnut3DSeriesView4.SizeAsPercentage = 100D;
            doughnut3DSeriesView4.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise;
            this.chartControl3.SeriesTemplate.View = doughnut3DSeriesView4;
            this.chartControl3.Size = new System.Drawing.Size(394, 356);
            this.chartControl3.TabIndex = 168;
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1;
            this.chartControl1.DataSource = this.sp07421_UT_Quote_Item_ListBindingSource;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = xyDiagram1;
            this.chartControl1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.Location = new System.Drawing.Point(60, 402);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesDataMember = "PaddedWorkOrderID";
            series3.ArgumentDataMember = "Description";
            series3.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series3.Name = "Net Cost";
            series3.ValueDataMembersSerializable = "NetCost";
            sideBySideBarSeriesView1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            sideBySideBarSeriesView1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series3.View = sideBySideBarSeriesView1;
            series4.ArgumentDataMember = "Description";
            series4.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series4.Name = "Net Price";
            series4.ValueDataMembersSerializable = "NetPrice";
            sideBySideBarSeriesView2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))));
            sideBySideBarSeriesView2.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series4.View = sideBySideBarSeriesView2;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series3,
        series4};
            this.chartControl1.SeriesTemplate.ArgumentDataMember = "Description";
            this.chartControl1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            this.chartControl1.Size = new System.Drawing.Size(949, 259);
            this.chartControl1.TabIndex = 161;
            // 
            // gaugeControl1
            // 
            this.gaugeControl1.AutoLayout = false;
            this.gaugeControl1.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.linearGauge1});
            this.gaugeControl1.LayoutPadding = new DevExpress.XtraGauges.Core.Base.Thickness(31);
            this.gaugeControl1.Location = new System.Drawing.Point(70, 167);
            this.gaugeControl1.Name = "gaugeControl1";
            this.gaugeControl1.Size = new System.Drawing.Size(949, 186);
            this.gaugeControl1.TabIndex = 166;
            // 
            // linearGauge1
            // 
            this.linearGauge1.AutoSize = DevExpress.Utils.DefaultBoolean.False;
            this.linearGauge1.Bounds = new System.Drawing.Rectangle(31, 31, 887, 125);
            this.linearGauge1.Labels.AddRange(new DevExpress.XtraGauges.Win.Base.LabelComponent[] {
            this.labelComponent1});
            this.linearGauge1.Markers.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleMarkerComponent[] {
            this.linearScaleMarkerComponent1});
            this.linearGauge1.Name = "linearGauge1";
            this.linearGauge1.Orientation = DevExpress.XtraGauges.Core.Model.ScaleOrientation.Horizontal;
            this.linearGauge1.RangeBars.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleRangeBarComponent[] {
            this.linearScaleRangeBarComponent1});
            this.linearGauge1.Scales.AddRange(new DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent[] {
            this.linearScaleComponent1});
            // 
            // labelComponent1
            // 
            this.labelComponent1.AppearanceText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelComponent1.AppearanceText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LimeGreen");
            this.labelComponent1.Name = "linearGauge1_Label1";
            this.labelComponent1.Position = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 100F);
            this.labelComponent1.Size = new System.Drawing.SizeF(200F, 25F);
            this.labelComponent1.Text = "Net Profit:";
            this.labelComponent1.ZOrder = -1001;
            // 
            // linearScaleMarkerComponent1
            // 
            this.linearScaleMarkerComponent1.LinearScale = this.linearScaleComponent1;
            this.linearScaleMarkerComponent1.Name = "linearGauge1_Marker1";
            this.linearScaleMarkerComponent1.ShapeOffset = 15F;
            this.linearScaleMarkerComponent1.ZOrder = -150;
            // 
            // linearScaleComponent1
            // 
            this.linearScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.linearScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.linearScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.linearScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.linearScaleComponent1.AppearanceScale.Brush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#999999");
            this.linearScaleComponent1.AppearanceScale.Width = 4F;
            this.linearScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#999999");
            this.linearScaleComponent1.AutoRescaling = true;
            this.linearScaleComponent1.CustomLogarithmicBase = 2F;
            this.linearScaleComponent1.EndPoint = new DevExpress.XtraGauges.Core.Base.PointF2D(62.5F, -200F);
            this.linearScaleComponent1.MajorTickCount = 6;
            this.linearScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.linearScaleComponent1.MajorTickmark.ShapeOffset = -7F;
            this.linearScaleComponent1.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(1.1F, 1F);
            this.linearScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style27_1;
            this.linearScaleComponent1.MajorTickmark.TextOffset = -20F;
            this.linearScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.BottomToTop;
            this.linearScaleComponent1.MaxValue = 50F;
            this.linearScaleComponent1.MinorTickCount = 4;
            this.linearScaleComponent1.MinorTickmark.ShapeOffset = -14F;
            this.linearScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style27_1;
            this.linearScaleComponent1.MinorTickmark.ShowTick = false;
            this.linearScaleComponent1.Name = "scale2";
            this.linearScaleComponent1.StartPoint = new DevExpress.XtraGauges.Core.Base.PointF2D(62.5F, 500F);
            this.linearScaleComponent1.Value = 20F;
            // 
            // linearScaleRangeBarComponent1
            // 
            this.linearScaleRangeBarComponent1.AppearanceRangeBar.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.linearScaleRangeBarComponent1.AppearanceRangeBar.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:LimeGreen");
            this.linearScaleRangeBarComponent1.EndOffset = 8F;
            this.linearScaleRangeBarComponent1.LinearScale = this.linearScaleComponent1;
            this.linearScaleRangeBarComponent1.Name = "linearGauge2_RangeBar1";
            this.linearScaleRangeBarComponent1.StartOffset = 4F;
            this.linearScaleRangeBarComponent1.Value = 30F;
            this.linearScaleRangeBarComponent1.ZOrder = -100;
            // 
            // lueAnalysisCodeID
            // 
            this.lueAnalysisCodeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "AnalysisCodeID", true));
            this.lueAnalysisCodeID.Location = new System.Drawing.Point(845, 555);
            this.lueAnalysisCodeID.MenuManager = this.barManager1;
            this.lueAnalysisCodeID.Name = "lueAnalysisCodeID";
            this.lueAnalysisCodeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueAnalysisCodeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Analysis Code", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueAnalysisCodeID.Properties.DataSource = this.sp07446UTPLAnalysisCodeBindingSource;
            this.lueAnalysisCodeID.Properties.DisplayMember = "Value";
            this.lueAnalysisCodeID.Properties.NullText = "";
            this.lueAnalysisCodeID.Properties.NullValuePrompt = "-- Please Select Analysis Code -- ";
            this.lueAnalysisCodeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueAnalysisCodeID.Properties.ValueMember = "PickListID";
            this.lueAnalysisCodeID.Size = new System.Drawing.Size(198, 20);
            this.lueAnalysisCodeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueAnalysisCodeID.TabIndex = 160;
            this.lueAnalysisCodeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07446UTPLAnalysisCodeBindingSource
            // 
            this.sp07446UTPLAnalysisCodeBindingSource.DataMember = "sp07446_UT_PL_AnalysisCode";
            this.sp07446UTPLAnalysisCodeBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueFeederContractID
            // 
            this.lueFeederContractID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "FeederContractID", true));
            this.lueFeederContractID.Location = new System.Drawing.Point(154, 221);
            this.lueFeederContractID.MenuManager = this.barManager1;
            this.lueFeederContractID.Name = "lueFeederContractID";
            this.lueFeederContractID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueFeederContractID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FeederContract", "Feeder Contract", 89, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueFeederContractID.Properties.DataSource = this.sp07453UTFeederContractListBindingSource;
            this.lueFeederContractID.Properties.DisplayMember = "FeederContract";
            this.lueFeederContractID.Properties.NullText = "";
            this.lueFeederContractID.Properties.NullValuePrompt = "-- Please Select Feeder Contract --";
            this.lueFeederContractID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueFeederContractID.Properties.ValueMember = "FeederContractID";
            this.lueFeederContractID.Size = new System.Drawing.Size(396, 20);
            this.lueFeederContractID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueFeederContractID.TabIndex = 159;
            this.lueFeederContractID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07453UTFeederContractListBindingSource
            // 
            this.sp07453UTFeederContractListBindingSource.DataMember = "sp07453_UT_FeederContract_List";
            this.sp07453UTFeederContractListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueCircuitID
            // 
            this.lueCircuitID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "CircuitID", true));
            this.lueCircuitID.Location = new System.Drawing.Point(154, 197);
            this.lueCircuitID.MenuManager = this.barManager1;
            this.lueCircuitID.Name = "lueCircuitID";
            this.lueCircuitID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCircuitID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CircuitName", "Circuit Name", 70, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CircuitNumber", "Circuit Number", 80, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCircuitID.Properties.DataSource = this.sp07452UTCircuitListBindingSource;
            this.lueCircuitID.Properties.DisplayMember = "CircuitName";
            this.lueCircuitID.Properties.NullText = "";
            this.lueCircuitID.Properties.NullValuePrompt = "-- Please Select Circuit --";
            this.lueCircuitID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueCircuitID.Properties.ValueMember = "CircuitID";
            this.lueCircuitID.Size = new System.Drawing.Size(396, 20);
            this.lueCircuitID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueCircuitID.TabIndex = 158;
            this.lueCircuitID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07452UTCircuitListBindingSource
            // 
            this.sp07452UTCircuitListBindingSource.DataMember = "sp07452_UT_Circuit_List";
            this.sp07452UTCircuitListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueSubContractorID
            // 
            this.lueSubContractorID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "SubContractorID", true));
            this.lueSubContractorID.Location = new System.Drawing.Point(154, 173);
            this.lueSubContractorID.MenuManager = this.barManager1;
            this.lueSubContractorID.Name = "lueSubContractorID";
            this.lueSubContractorID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueSubContractorID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("SupplierCode", "Supplier Code", 76, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 37, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueSubContractorID.Properties.DataSource = this.sp07450UTSubContractorListBindingSource;
            this.lueSubContractorID.Properties.DisplayMember = "Name";
            this.lueSubContractorID.Properties.NullText = "";
            this.lueSubContractorID.Properties.NullValuePrompt = "-- Please Select Sub Contractor --";
            this.lueSubContractorID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueSubContractorID.Properties.ValueMember = "SubContractorID";
            this.lueSubContractorID.Size = new System.Drawing.Size(396, 20);
            this.lueSubContractorID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueSubContractorID.TabIndex = 157;
            this.lueSubContractorID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07450UTSubContractorListBindingSource
            // 
            this.sp07450UTSubContractorListBindingSource.DataMember = "sp07450_UT_SubContractor_List";
            this.sp07450UTSubContractorListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp07421_UT_Quote_Item_ListGridControl
            // 
            this.sp07421_UT_Quote_Item_ListGridControl.DataSource = this.sp07421_UT_Quote_Item_ListBindingSource;
            this.sp07421_UT_Quote_Item_ListGridControl.Location = new System.Drawing.Point(48, 517);
            this.sp07421_UT_Quote_Item_ListGridControl.MainView = this.gridView1;
            this.sp07421_UT_Quote_Item_ListGridControl.MenuManager = this.barManager1;
            this.sp07421_UT_Quote_Item_ListGridControl.Name = "sp07421_UT_Quote_Item_ListGridControl";
            this.sp07421_UT_Quote_Item_ListGridControl.Size = new System.Drawing.Size(393, 213);
            this.sp07421_UT_Quote_Item_ListGridControl.TabIndex = 156;
            this.sp07421_UT_Quote_Item_ListGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQuoteItemID,
            this.colQuoteID,
            this.colWorkOrderID,
            this.colPaddedWorkOrderID,
            this.colQuoteCategoryID,
            this.colQuoteCategory,
            this.colDescription,
            this.colBuyRateVAT,
            this.colSellRate,
            this.colSellRateVAT,
            this.colSellRateTotal,
            this.colBuyRate,
            this.colBuyRateTotal,
            this.colFreeTextRate,
            this.colNetPrice,
            this.colNetCost,
            this.colMarkUp,
            this.colQuantity,
            this.colItemUnitsID,
            this.colUnits,
            this.colRating,
            this.colCreationDate,
            this.colLastUpdatedDate,
            this.colLastUpdatedBy,
            this.colMode,
            this.colRecordID});
            this.gridView1.GridControl = this.sp07421_UT_Quote_Item_ListGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuoteCategoryID, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colQuoteItemID
            // 
            this.colQuoteItemID.FieldName = "QuoteItemID";
            this.colQuoteItemID.Name = "colQuoteItemID";
            this.colQuoteItemID.OptionsColumn.AllowEdit = false;
            this.colQuoteItemID.OptionsColumn.AllowFocus = false;
            this.colQuoteItemID.OptionsColumn.ReadOnly = true;
            // 
            // colQuoteID
            // 
            this.colQuoteID.FieldName = "QuoteID";
            this.colQuoteID.Name = "colQuoteID";
            this.colQuoteID.OptionsColumn.AllowEdit = false;
            this.colQuoteID.OptionsColumn.AllowFocus = false;
            this.colQuoteID.OptionsColumn.ReadOnly = true;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colQuoteCategoryID
            // 
            this.colQuoteCategoryID.FieldName = "QuoteCategoryID";
            this.colQuoteCategoryID.Name = "colQuoteCategoryID";
            this.colQuoteCategoryID.OptionsColumn.AllowEdit = false;
            this.colQuoteCategoryID.OptionsColumn.AllowFocus = false;
            this.colQuoteCategoryID.OptionsColumn.ReadOnly = true;
            // 
            // colQuoteCategory
            // 
            this.colQuoteCategory.FieldName = "QuoteCategory";
            this.colQuoteCategory.Name = "colQuoteCategory";
            this.colQuoteCategory.OptionsColumn.AllowEdit = false;
            this.colQuoteCategory.OptionsColumn.AllowFocus = false;
            this.colQuoteCategory.OptionsColumn.ReadOnly = true;
            this.colQuoteCategory.Visible = true;
            this.colQuoteCategory.VisibleIndex = 0;
            this.colQuoteCategory.Width = 89;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 236;
            // 
            // colBuyRateVAT
            // 
            this.colBuyRateVAT.FieldName = "BuyRateVAT";
            this.colBuyRateVAT.Name = "colBuyRateVAT";
            this.colBuyRateVAT.OptionsColumn.AllowEdit = false;
            this.colBuyRateVAT.OptionsColumn.AllowFocus = false;
            this.colBuyRateVAT.OptionsColumn.ReadOnly = true;
            // 
            // colSellRate
            // 
            this.colSellRate.FieldName = "SellRate";
            this.colSellRate.Name = "colSellRate";
            this.colSellRate.OptionsColumn.AllowEdit = false;
            this.colSellRate.OptionsColumn.AllowFocus = false;
            this.colSellRate.OptionsColumn.ReadOnly = true;
            // 
            // colSellRateVAT
            // 
            this.colSellRateVAT.FieldName = "SellRateVAT";
            this.colSellRateVAT.Name = "colSellRateVAT";
            this.colSellRateVAT.OptionsColumn.AllowEdit = false;
            this.colSellRateVAT.OptionsColumn.AllowFocus = false;
            this.colSellRateVAT.OptionsColumn.ReadOnly = true;
            // 
            // colSellRateTotal
            // 
            this.colSellRateTotal.FieldName = "SellRateTotal";
            this.colSellRateTotal.Name = "colSellRateTotal";
            this.colSellRateTotal.OptionsColumn.AllowEdit = false;
            this.colSellRateTotal.OptionsColumn.AllowFocus = false;
            this.colSellRateTotal.OptionsColumn.ReadOnly = true;
            // 
            // colBuyRate
            // 
            this.colBuyRate.FieldName = "BuyRate";
            this.colBuyRate.Name = "colBuyRate";
            this.colBuyRate.OptionsColumn.AllowEdit = false;
            this.colBuyRate.OptionsColumn.AllowFocus = false;
            this.colBuyRate.OptionsColumn.ReadOnly = true;
            // 
            // colBuyRateTotal
            // 
            this.colBuyRateTotal.FieldName = "BuyRateTotal";
            this.colBuyRateTotal.Name = "colBuyRateTotal";
            this.colBuyRateTotal.OptionsColumn.AllowEdit = false;
            this.colBuyRateTotal.OptionsColumn.AllowFocus = false;
            this.colBuyRateTotal.OptionsColumn.ReadOnly = true;
            // 
            // colFreeTextRate
            // 
            this.colFreeTextRate.FieldName = "FreeTextRate";
            this.colFreeTextRate.Name = "colFreeTextRate";
            this.colFreeTextRate.OptionsColumn.AllowEdit = false;
            this.colFreeTextRate.OptionsColumn.AllowFocus = false;
            this.colFreeTextRate.OptionsColumn.ReadOnly = true;
            // 
            // colNetPrice
            // 
            this.colNetPrice.FieldName = "NetPrice";
            this.colNetPrice.Name = "colNetPrice";
            this.colNetPrice.OptionsColumn.AllowEdit = false;
            this.colNetPrice.OptionsColumn.AllowFocus = false;
            this.colNetPrice.OptionsColumn.ReadOnly = true;
            // 
            // colNetCost
            // 
            this.colNetCost.FieldName = "NetCost";
            this.colNetCost.Name = "colNetCost";
            this.colNetCost.OptionsColumn.AllowEdit = false;
            this.colNetCost.OptionsColumn.AllowFocus = false;
            this.colNetCost.OptionsColumn.ReadOnly = true;
            // 
            // colMarkUp
            // 
            this.colMarkUp.FieldName = "MarkUp";
            this.colMarkUp.Name = "colMarkUp";
            this.colMarkUp.OptionsColumn.AllowEdit = false;
            this.colMarkUp.OptionsColumn.AllowFocus = false;
            this.colMarkUp.OptionsColumn.ReadOnly = true;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowFocus = false;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 2;
            this.colQuantity.Width = 54;
            // 
            // colItemUnitsID
            // 
            this.colItemUnitsID.FieldName = "ItemUnitsID";
            this.colItemUnitsID.Name = "colItemUnitsID";
            this.colItemUnitsID.OptionsColumn.AllowEdit = false;
            this.colItemUnitsID.OptionsColumn.AllowFocus = false;
            this.colItemUnitsID.OptionsColumn.ReadOnly = true;
            // 
            // colUnits
            // 
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.OptionsColumn.AllowEdit = false;
            this.colUnits.OptionsColumn.AllowFocus = false;
            this.colUnits.OptionsColumn.ReadOnly = true;
            this.colUnits.Visible = true;
            this.colUnits.VisibleIndex = 3;
            this.colUnits.Width = 89;
            // 
            // colRating
            // 
            this.colRating.FieldName = "Rating";
            this.colRating.Name = "colRating";
            this.colRating.OptionsColumn.AllowEdit = false;
            this.colRating.OptionsColumn.AllowFocus = false;
            this.colRating.OptionsColumn.ReadOnly = true;
            // 
            // colCreationDate
            // 
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.OptionsColumn.AllowEdit = false;
            this.colCreationDate.OptionsColumn.AllowFocus = false;
            this.colCreationDate.OptionsColumn.ReadOnly = true;
            // 
            // colLastUpdatedDate
            // 
            this.colLastUpdatedDate.FieldName = "LastUpdatedDate";
            this.colLastUpdatedDate.Name = "colLastUpdatedDate";
            this.colLastUpdatedDate.OptionsColumn.AllowEdit = false;
            this.colLastUpdatedDate.OptionsColumn.AllowFocus = false;
            this.colLastUpdatedDate.OptionsColumn.ReadOnly = true;
            // 
            // colLastUpdatedBy
            // 
            this.colLastUpdatedBy.FieldName = "LastUpdatedBy";
            this.colLastUpdatedBy.Name = "colLastUpdatedBy";
            this.colLastUpdatedBy.OptionsColumn.AllowEdit = false;
            this.colLastUpdatedBy.OptionsColumn.AllowFocus = false;
            this.colLastUpdatedBy.OptionsColumn.ReadOnly = true;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // quoteItemDataNavigator
            // 
            this.quoteItemDataNavigator.Buttons.Append.Visible = false;
            this.quoteItemDataNavigator.Buttons.CancelEdit.Visible = false;
            this.quoteItemDataNavigator.Buttons.EndEdit.Visible = false;
            this.quoteItemDataNavigator.Buttons.Remove.Visible = false;
            this.quoteItemDataNavigator.DataSource = this.sp07421_UT_Quote_Item_ListBindingSource;
            this.quoteItemDataNavigator.Location = new System.Drawing.Point(540, 484);
            this.quoteItemDataNavigator.Name = "quoteItemDataNavigator";
            this.quoteItemDataNavigator.Size = new System.Drawing.Size(194, 19);
            this.quoteItemDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.quoteItemDataNavigator.TabIndex = 125;
            this.quoteItemDataNavigator.Text = "quoteItemDataNavigator";
            this.quoteItemDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.quoteItemDataNavigator.TextStringFormat = "Quote Item {0} of {1}";
            this.quoteItemDataNavigator.PositionChanged += new System.EventHandler(this.quoteItemDataNavigator_PositionChanged);
            // 
            // spnBuyRateVAT
            // 
            this.spnBuyRateVAT.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "BuyRateVAT", true));
            this.spnBuyRateVAT.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnBuyRateVAT.Location = new System.Drawing.Point(702, 556);
            this.spnBuyRateVAT.MenuManager = this.barManager1;
            this.spnBuyRateVAT.Name = "spnBuyRateVAT";
            this.spnBuyRateVAT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnBuyRateVAT.Properties.DisplayFormat.FormatString = "c2";
            this.spnBuyRateVAT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRateVAT.Properties.EditFormat.FormatString = "c2";
            this.spnBuyRateVAT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRateVAT.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spnBuyRateVAT.Size = new System.Drawing.Size(237, 20);
            this.spnBuyRateVAT.StyleController = this.editFormDataLayoutControlGroup;
            this.spnBuyRateVAT.TabIndex = 26;
            // 
            // spnSellRateVAT
            // 
            this.spnSellRateVAT.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "SellRateVAT", true));
            this.spnSellRateVAT.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnSellRateVAT.Location = new System.Drawing.Point(702, 556);
            this.spnSellRateVAT.MenuManager = this.barManager1;
            this.spnSellRateVAT.Name = "spnSellRateVAT";
            this.spnSellRateVAT.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnSellRateVAT.Properties.DisplayFormat.FormatString = "c2";
            this.spnSellRateVAT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRateVAT.Properties.EditFormat.FormatString = "c2";
            this.spnSellRateVAT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRateVAT.Size = new System.Drawing.Size(237, 20);
            this.spnSellRateVAT.StyleController = this.editFormDataLayoutControlGroup;
            this.spnSellRateVAT.TabIndex = 30;
            // 
            // lueClientID
            // 
            this.lueClientID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07427_UT_Client_ListBindingSource, "ClientID", true));
            this.lueClientID.Location = new System.Drawing.Point(154, 149);
            this.lueClientID.MenuManager = this.barManager1;
            this.lueClientID.Name = "lueClientID";
            this.lueClientID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueClientID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ClientName", "Client Name", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueClientID.Properties.DataSource = this.sp07427_UT_Client_ListBindingSource;
            this.lueClientID.Properties.DisplayMember = "ClientName";
            this.lueClientID.Properties.NullText = "";
            this.lueClientID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueClientID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueClientID.Properties.ValueMember = "ClientID";
            this.lueClientID.Size = new System.Drawing.Size(396, 20);
            this.lueClientID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueClientID.TabIndex = 51;
            this.lueClientID.EditValueChanged += new System.EventHandler(this.lueClientID_EditValueChanged);
            this.lueClientID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07427_UT_Client_ListBindingSource
            // 
            this.sp07427_UT_Client_ListBindingSource.DataMember = "sp07427_UT_Client_List";
            this.sp07427_UT_Client_ListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueQuoteCategoryID
            // 
            this.lueQuoteCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "QuoteCategoryID", true));
            this.lueQuoteCategoryID.Location = new System.Drawing.Point(575, 507);
            this.lueQuoteCategoryID.MenuManager = this.barManager1;
            this.lueQuoteCategoryID.Name = "lueQuoteCategoryID";
            this.lueQuoteCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQuoteCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("QuoteCategory", "Quote Category", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueQuoteCategoryID.Properties.DataSource = this.sp07424UTQuoteCategoryItemBindingSource;
            this.lueQuoteCategoryID.Properties.DisplayMember = "QuoteCategory";
            this.lueQuoteCategoryID.Properties.NullText = "";
            this.lueQuoteCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueQuoteCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueQuoteCategoryID.Properties.ValueMember = "QuoteCategoryID";
            this.lueQuoteCategoryID.Size = new System.Drawing.Size(147, 20);
            this.lueQuoteCategoryID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueQuoteCategoryID.TabIndex = 22;
            this.lueQuoteCategoryID.EditValueChanged += new System.EventHandler(this.lueQuoteCategoryID_EditValueChanged);
            this.lueQuoteCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07424UTQuoteCategoryItemBindingSource
            // 
            this.sp07424UTQuoteCategoryItemBindingSource.DataMember = "sp07424_UT_Quote_Category_Item";
            this.sp07424UTQuoteCategoryItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // memDescription
            // 
            this.memDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "Description", true));
            this.memDescription.Location = new System.Drawing.Point(575, 531);
            this.memDescription.MenuManager = this.barManager1;
            this.memDescription.Name = "memDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memDescription, true);
            this.memDescription.Size = new System.Drawing.Size(148, 70);
            this.scSpellChecker.SetSpellCheckerOptions(this.memDescription, optionsSpelling4);
            this.memDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.memDescription.TabIndex = 24;
            this.memDescription.Validating += new System.ComponentModel.CancelEventHandler(this.memDescription_Validating);
            // 
            // spnSellRate
            // 
            this.spnSellRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "SellRate", true));
            this.spnSellRate.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnSellRate.Location = new System.Drawing.Point(888, 638);
            this.spnSellRate.MenuManager = this.barManager1;
            this.spnSellRate.Name = "spnSellRate";
            this.spnSellRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnSellRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnSellRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRate.Properties.MaxValue = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.spnSellRate.Size = new System.Drawing.Size(143, 20);
            this.spnSellRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnSellRate.TabIndex = 28;
            this.spnSellRate.EditValueChanged += new System.EventHandler(this.spnSellRate_EditValueChanged);
            this.spnSellRate.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnSellRateTotal
            // 
            this.spnSellRateTotal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "SellRateTotal", true));
            this.spnSellRateTotal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnSellRateTotal.Location = new System.Drawing.Point(888, 662);
            this.spnSellRateTotal.MenuManager = this.barManager1;
            this.spnSellRateTotal.Name = "spnSellRateTotal";
            this.spnSellRateTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnSellRateTotal.Properties.DisplayFormat.FormatString = "c2";
            this.spnSellRateTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRateTotal.Properties.EditFormat.FormatString = "c2";
            this.spnSellRateTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRateTotal.Properties.ReadOnly = true;
            this.spnSellRateTotal.Size = new System.Drawing.Size(143, 20);
            this.spnSellRateTotal.StyleController = this.editFormDataLayoutControlGroup;
            this.spnSellRateTotal.TabIndex = 32;
            this.spnSellRateTotal.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnBuyRate
            // 
            this.spnBuyRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "BuyRate", true));
            this.spnBuyRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnBuyRate.Location = new System.Drawing.Point(587, 638);
            this.spnBuyRate.MenuManager = this.barManager1;
            this.spnBuyRate.Name = "spnBuyRate";
            this.spnBuyRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnBuyRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnBuyRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRate.Properties.EditFormat.FormatString = "c2";
            this.spnBuyRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRate.Properties.MaxValue = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.spnBuyRate.Size = new System.Drawing.Size(155, 20);
            this.spnBuyRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnBuyRate.TabIndex = 34;
            this.spnBuyRate.EditValueChanged += new System.EventHandler(this.spnBuyRate_EditValueChanged);
            this.spnBuyRate.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnBuyRateTotal
            // 
            this.spnBuyRateTotal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "BuyRateTotal", true));
            this.spnBuyRateTotal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnBuyRateTotal.Location = new System.Drawing.Point(587, 662);
            this.spnBuyRateTotal.MenuManager = this.barManager1;
            this.spnBuyRateTotal.Name = "spnBuyRateTotal";
            this.spnBuyRateTotal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnBuyRateTotal.Properties.DisplayFormat.FormatString = "c2";
            this.spnBuyRateTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRateTotal.Properties.EditFormat.FormatString = "c2";
            this.spnBuyRateTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRateTotal.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spnBuyRateTotal.Properties.ReadOnly = true;
            this.spnBuyRateTotal.Size = new System.Drawing.Size(155, 20);
            this.spnBuyRateTotal.StyleController = this.editFormDataLayoutControlGroup;
            this.spnBuyRateTotal.TabIndex = 36;
            this.spnBuyRateTotal.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnFreeTextRate
            // 
            this.spnFreeTextRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "FreeTextRate", true));
            this.spnFreeTextRate.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnFreeTextRate.Location = new System.Drawing.Point(575, 722);
            this.spnFreeTextRate.MenuManager = this.barManager1;
            this.spnFreeTextRate.Name = "spnFreeTextRate";
            this.spnFreeTextRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnFreeTextRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnFreeTextRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnFreeTextRate.Properties.EditFormat.FormatString = "c2";
            this.spnFreeTextRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnFreeTextRate.Size = new System.Drawing.Size(183, 20);
            this.spnFreeTextRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnFreeTextRate.TabIndex = 38;
            this.spnFreeTextRate.EditValueChanged += new System.EventHandler(this.spnFreeTextRate_EditValueChanged);
            this.spnFreeTextRate.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnNetPrice
            // 
            this.spnNetPrice.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "NetPrice", true));
            this.spnNetPrice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnNetPrice.Location = new System.Drawing.Point(880, 698);
            this.spnNetPrice.MenuManager = this.barManager1;
            this.spnNetPrice.Name = "spnNetPrice";
            this.spnNetPrice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnNetPrice.Properties.DisplayFormat.FormatString = "c2";
            this.spnNetPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnNetPrice.Properties.EditFormat.FormatString = "c2";
            this.spnNetPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnNetPrice.Size = new System.Drawing.Size(163, 20);
            this.spnNetPrice.StyleController = this.editFormDataLayoutControlGroup;
            this.spnNetPrice.TabIndex = 40;
            this.spnNetPrice.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnNetCost
            // 
            this.spnNetCost.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "NetCost", true));
            this.spnNetCost.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnNetCost.Location = new System.Drawing.Point(575, 698);
            this.spnNetCost.MenuManager = this.barManager1;
            this.spnNetCost.Name = "spnNetCost";
            this.spnNetCost.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnNetCost.Properties.DisplayFormat.FormatString = "c2";
            this.spnNetCost.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnNetCost.Properties.EditFormat.FormatString = "c2";
            this.spnNetCost.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnNetCost.Size = new System.Drawing.Size(183, 20);
            this.spnNetCost.StyleController = this.editFormDataLayoutControlGroup;
            this.spnNetCost.TabIndex = 42;
            this.spnNetCost.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnMarkUp
            // 
            this.spnMarkUp.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "MarkUp", true));
            this.spnMarkUp.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spnMarkUp.Location = new System.Drawing.Point(880, 722);
            this.spnMarkUp.MenuManager = this.barManager1;
            this.spnMarkUp.Name = "spnMarkUp";
            this.spnMarkUp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnMarkUp.Properties.DisplayFormat.FormatString = "p";
            this.spnMarkUp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnMarkUp.Properties.EditFormat.FormatString = "c2";
            this.spnMarkUp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnMarkUp.Size = new System.Drawing.Size(163, 20);
            this.spnMarkUp.StyleController = this.editFormDataLayoutControlGroup;
            this.spnMarkUp.TabIndex = 44;
            this.spnMarkUp.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // spnQuantity
            // 
            this.spnQuantity.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "Quantity", true));
            this.spnQuantity.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnQuantity.Location = new System.Drawing.Point(844, 507);
            this.spnQuantity.MenuManager = this.barManager1;
            this.spnQuantity.Name = "spnQuantity";
            this.spnQuantity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnQuantity.Properties.MaxValue = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.spnQuantity.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnQuantity.Size = new System.Drawing.Size(199, 20);
            this.spnQuantity.StyleController = this.editFormDataLayoutControlGroup;
            this.spnQuantity.TabIndex = 46;
            this.spnQuantity.EditValueChanged += new System.EventHandler(this.spnQuantity_EditValueChanged);
            this.spnQuantity.Validating += new System.ComponentModel.CancelEventHandler(this.spinEdit_Validating);
            // 
            // lueItemUnitsID
            // 
            this.lueItemUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "ItemUnitsID", true));
            this.lueItemUnitsID.Location = new System.Drawing.Point(845, 579);
            this.lueItemUnitsID.MenuManager = this.barManager1;
            this.lueItemUnitsID.Name = "lueItemUnitsID";
            this.lueItemUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueItemUnitsID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueItemUnitsID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "refresh", null, true)});
            this.lueItemUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("QuoteCategory", "Quote Category", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Units", "Units", 34, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueItemUnitsID.Properties.DataSource = this.sp07435UTItemUnitsListBindingSource;
            this.lueItemUnitsID.Properties.DisplayMember = "Units";
            this.lueItemUnitsID.Properties.NullText = "";
            this.lueItemUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueItemUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueItemUnitsID.Properties.ValueMember = "ItemUnitsID";
            this.lueItemUnitsID.Size = new System.Drawing.Size(198, 22);
            this.lueItemUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueItemUnitsID.TabIndex = 48;
            this.lueItemUnitsID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueItemUnitsID_ButtonPressed);
            this.lueItemUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07435UTItemUnitsListBindingSource
            // 
            this.sp07435UTItemUnitsListBindingSource.DataMember = "sp07435_UT_Item_Units_List";
            this.sp07435UTItemUnitsListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueRating
            // 
            this.lueRating.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07421_UT_Quote_Item_ListBindingSource, "RatingID", true));
            this.lueRating.Location = new System.Drawing.Point(845, 531);
            this.lueRating.MenuManager = this.barManager1;
            this.lueRating.Name = "lueRating";
            this.lueRating.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueRating.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Value", "Rating", 36, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueRating.Properties.DataSource = this.sp07446UTPicklistByHeaderIDBindingSource;
            this.lueRating.Properties.DisplayMember = "Value";
            this.lueRating.Properties.NullText = "";
            this.lueRating.Properties.NullValuePrompt = "-- Please Select --";
            this.lueRating.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueRating.Properties.ValueMember = "PickListID";
            this.lueRating.Size = new System.Drawing.Size(198, 20);
            this.lueRating.StyleController = this.editFormDataLayoutControlGroup;
            this.lueRating.TabIndex = 50;
            this.lueRating.EditValueChanged += new System.EventHandler(this.lueRating_EditValueChanged);
            this.lueRating.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07446UTPicklistByHeaderIDBindingSource
            // 
            this.sp07446UTPicklistByHeaderIDBindingSource.DataMember = "sp07446_UT_Picklist_By_Header_ID";
            this.sp07446UTPicklistByHeaderIDBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // cmbResourceRateID
            // 
            this.cmbResourceRateID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438_UT_Resource_Rate_ListBindingSource, "ResourceRateID", true));
            this.cmbResourceRateID.Location = new System.Drawing.Point(154, 413);
            this.cmbResourceRateID.MenuManager = this.barManager1;
            this.cmbResourceRateID.Name = "cmbResourceRateID";
            this.cmbResourceRateID.Properties.AllowMultiSelect = true;
            this.cmbResourceRateID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("cmbResourceRateID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Add Resource", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("cmbResourceRateID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "", "refresh", null, true)});
            this.cmbResourceRateID.Properties.DataSource = this.sp07438_UT_Resource_Rate_ListBindingSource;
            this.cmbResourceRateID.Properties.DisplayMember = "Description";
            this.cmbResourceRateID.Properties.ValueMember = "ResourceRateID";
            this.cmbResourceRateID.Size = new System.Drawing.Size(889, 22);
            this.cmbResourceRateID.StyleController = this.editFormDataLayoutControlGroup;
            this.cmbResourceRateID.TabIndex = 21;
            this.cmbResourceRateID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.cmbResourceRateID_ButtonPressed);
            this.cmbResourceRateID.Enter += new System.EventHandler(this.cmbResourceRateID_Enter);
            this.cmbResourceRateID.Validating += new System.ComponentModel.CancelEventHandler(this.checkedComboBox_Validating);
            // 
            // sp07438_UT_Resource_Rate_ListBindingSource
            // 
            this.sp07438_UT_Resource_Rate_ListBindingSource.DataMember = "sp07438_UT_Resource_Rate_List";
            this.sp07438_UT_Resource_Rate_ListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // cmbQuoteCategoryID
            // 
            this.cmbQuoteCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07424_UT_Quote_Category_Item_AddModeBindingSource, "QuoteCategoryID", true));
            this.cmbQuoteCategoryID.Location = new System.Drawing.Point(154, 389);
            this.cmbQuoteCategoryID.MenuManager = this.barManager1;
            this.cmbQuoteCategoryID.Name = "cmbQuoteCategoryID";
            this.cmbQuoteCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbQuoteCategoryID.Properties.DataSource = this.sp07424_UT_Quote_Category_Item_AddModeBindingSource;
            this.cmbQuoteCategoryID.Properties.DisplayMember = "QuoteCategory";
            this.cmbQuoteCategoryID.Properties.ValueMember = "QuoteCategoryID";
            this.cmbQuoteCategoryID.Size = new System.Drawing.Size(889, 20);
            this.cmbQuoteCategoryID.StyleController = this.editFormDataLayoutControlGroup;
            this.cmbQuoteCategoryID.TabIndex = 20;
            this.cmbQuoteCategoryID.EditValueChanged += new System.EventHandler(this.cmbQuoteCategoryID_EditValueChanged);
            this.cmbQuoteCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.checkedComboBox_Validating);
            this.cmbQuoteCategoryID.Validated += new System.EventHandler(this.cmbQuoteCategoryID_Validated);
            // 
            // sp07424_UT_Quote_Category_Item_AddModeBindingSource
            // 
            this.sp07424_UT_Quote_Category_Item_AddModeBindingSource.DataMember = "sp07424_UT_Quote_Category_Item_AddMode";
            this.sp07424_UT_Quote_Category_Item_AddModeBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // txtReference
            // 
            this.txtReference.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "Reference", true));
            this.txtReference.Location = new System.Drawing.Point(154, 101);
            this.txtReference.MenuManager = this.barManager1;
            this.txtReference.Name = "txtReference";
            this.txtReference.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtReference, true);
            this.txtReference.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtReference, optionsSpelling5);
            this.txtReference.StyleController = this.editFormDataLayoutControlGroup;
            this.txtReference.TabIndex = 5;
            // 
            // txtPaddedWorkOrderID
            // 
            this.txtPaddedWorkOrderID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "PaddedWorkOrderID", true));
            this.txtPaddedWorkOrderID.Location = new System.Drawing.Point(154, 125);
            this.txtPaddedWorkOrderID.MenuManager = this.barManager1;
            this.txtPaddedWorkOrderID.Name = "txtPaddedWorkOrderID";
            this.txtPaddedWorkOrderID.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPaddedWorkOrderID, true);
            this.txtPaddedWorkOrderID.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPaddedWorkOrderID, optionsSpelling6);
            this.txtPaddedWorkOrderID.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPaddedWorkOrderID.TabIndex = 7;
            // 
            // lueClientContractTypeID
            // 
            this.lueClientContractTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "ClientContractTypeID", true));
            this.lueClientContractTypeID.Location = new System.Drawing.Point(154, 245);
            this.lueClientContractTypeID.MenuManager = this.barManager1;
            this.lueClientContractTypeID.Name = "lueClientContractTypeID";
            this.lueClientContractTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueClientContractTypeID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueClientContractTypeID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "", "refresh", null, true)});
            this.lueClientContractTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContractType", "Contract Type", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueClientContractTypeID.Properties.DataSource = this.sp07431UTClientContractTypeListBindingSource;
            this.lueClientContractTypeID.Properties.DisplayMember = "ContractType";
            this.lueClientContractTypeID.Properties.NullText = "";
            this.lueClientContractTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueClientContractTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueClientContractTypeID.Properties.ValueMember = "ClientContractTypeID";
            this.lueClientContractTypeID.Size = new System.Drawing.Size(396, 22);
            this.lueClientContractTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueClientContractTypeID.TabIndex = 9;
            this.lueClientContractTypeID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueClientContractTypeID_ButtonPressed);
            this.lueClientContractTypeID.EditValueChanged += new System.EventHandler(this.lueClientContractTypeID_EditValueChanged);
            this.lueClientContractTypeID.Enter += new System.EventHandler(this.lueClientContractTypeID_Enter);
            this.lueClientContractTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07431UTClientContractTypeListBindingSource
            // 
            this.sp07431UTClientContractTypeListBindingSource.DataMember = "sp07431_UT_Client_Contract_Type_List";
            this.sp07431UTClientContractTypeListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // ceIsMainQuote
            // 
            this.ceIsMainQuote.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "IsMainQuote", true));
            this.ceIsMainQuote.Location = new System.Drawing.Point(477, 297);
            this.ceIsMainQuote.MenuManager = this.barManager1;
            this.ceIsMainQuote.Name = "ceIsMainQuote";
            this.ceIsMainQuote.Properties.Caption = "";
            this.ceIsMainQuote.Size = new System.Drawing.Size(73, 19);
            this.ceIsMainQuote.StyleController = this.editFormDataLayoutControlGroup;
            this.ceIsMainQuote.TabIndex = 11;
            // 
            // ceTransferredExchequer
            // 
            this.ceTransferredExchequer.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "TransferredExchequer", true));
            this.ceTransferredExchequer.Location = new System.Drawing.Point(154, 297);
            this.ceTransferredExchequer.MenuManager = this.barManager1;
            this.ceTransferredExchequer.Name = "ceTransferredExchequer";
            this.ceTransferredExchequer.Properties.Caption = "";
            this.ceTransferredExchequer.Properties.ReadOnly = true;
            this.ceTransferredExchequer.Size = new System.Drawing.Size(201, 19);
            this.ceTransferredExchequer.StyleController = this.editFormDataLayoutControlGroup;
            this.ceTransferredExchequer.TabIndex = 13;
            // 
            // txtPONumber
            // 
            this.txtPONumber.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "PONumber", true));
            this.txtPONumber.Location = new System.Drawing.Point(154, 320);
            this.txtPONumber.MenuManager = this.barManager1;
            this.txtPONumber.Name = "txtPONumber";
            this.txtPONumber.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtPONumber, true);
            this.txtPONumber.Size = new System.Drawing.Size(396, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtPONumber, optionsSpelling7);
            this.txtPONumber.StyleController = this.editFormDataLayoutControlGroup;
            this.txtPONumber.TabIndex = 15;
            this.txtPONumber.Validating += new System.ComponentModel.CancelEventHandler(this.textEdit_Validating);
            // 
            // memNotes
            // 
            this.memNotes.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "Notes", true));
            this.memNotes.Location = new System.Drawing.Point(566, 134);
            this.memNotes.MenuManager = this.barManager1;
            this.memNotes.Name = "memNotes";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memNotes, true);
            this.memNotes.Size = new System.Drawing.Size(465, 116);
            this.scSpellChecker.SetSpellCheckerOptions(this.memNotes, optionsSpelling8);
            this.memNotes.StyleController = this.editFormDataLayoutControlGroup;
            this.memNotes.TabIndex = 19;
            // 
            // lueBillingCentreCodeID
            // 
            this.lueBillingCentreCodeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07418_UT_Quote_ListBindingSource, "BillingCentreCodeID", true));
            this.lueBillingCentreCodeID.Location = new System.Drawing.Point(154, 271);
            this.lueBillingCentreCodeID.MenuManager = this.barManager1;
            this.lueBillingCentreCodeID.Name = "lueBillingCentreCodeID";
            toolTipTitleItem6.Text = "Billing Centre Code";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Load new form to Add Billing Centre Code";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            toolTipTitleItem7.Text = "Billing Centre Code";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Refresh Billing Centre Code";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.lueBillingCentreCodeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueBillingCentreCodeID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "", "add", superToolTip6, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueBillingCentreCodeID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, "", "refresh", superToolTip7, true)});
            this.lueBillingCentreCodeID.Properties.DataSource = this.spAS11038BillingCentreCodeItemBindingSource;
            this.lueBillingCentreCodeID.Properties.DisplayMember = "BillingCentreCode";
            this.lueBillingCentreCodeID.Properties.NullText = "";
            this.lueBillingCentreCodeID.Properties.NullValuePrompt = "-- Please Select Billing Centre Code --";
            this.lueBillingCentreCodeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueBillingCentreCodeID.Properties.ValueMember = "BillingCentreCodeID";
            this.lueBillingCentreCodeID.Properties.View = this.searchLookUpEdit1View;
            this.lueBillingCentreCodeID.Size = new System.Drawing.Size(396, 22);
            this.lueBillingCentreCodeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueBillingCentreCodeID.TabIndex = 17;
            this.lueBillingCentreCodeID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueBillingCentreCodeID_ButtonPressed);
            this.lueBillingCentreCodeID.EditValueChanged += new System.EventHandler(this.lueBillingCentreCodeID_EditValueChanged);
            this.lueBillingCentreCodeID.Validating += new System.ComponentModel.CancelEventHandler(this.searchLookup_Validating);
            // 
            // spAS11038BillingCentreCodeItemBindingSource
            // 
            this.spAS11038BillingCentreCodeItemBindingSource.DataMember = "sp_AS_11038_Billing_Centre_Code_Item";
            this.spAS11038BillingCentreCodeItemBindingSource.DataSource = this.dataSet_AS_DataEntry;
            // 
            // searchLookUpEdit1View
            // 
            this.searchLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBillingCentreCode,
            this.colCompany,
            this.colCompanyCode,
            this.colDepartment,
            this.colDepartmentCode,
            this.colCostCentre,
            this.colCostCentreCode});
            this.searchLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.searchLookUpEdit1View.Name = "searchLookUpEdit1View";
            this.searchLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.searchLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colBillingCentreCode
            // 
            this.colBillingCentreCode.FieldName = "BillingCentreCode";
            this.colBillingCentreCode.Name = "colBillingCentreCode";
            this.colBillingCentreCode.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCode.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCode.Visible = true;
            this.colBillingCentreCode.VisibleIndex = 0;
            // 
            // colCompany
            // 
            this.colCompany.FieldName = "Company";
            this.colCompany.Name = "colCompany";
            this.colCompany.OptionsColumn.AllowEdit = false;
            this.colCompany.OptionsColumn.ReadOnly = true;
            this.colCompany.Visible = true;
            this.colCompany.VisibleIndex = 1;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.AllowEdit = false;
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 2;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            // 
            // colCostCentre
            // 
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.AllowEdit = false;
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 3;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.spnBuyRateVAT;
            this.layoutControlItem15.CustomizationFormText = "Buy Rate VAT:";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(359, 24);
            this.layoutControlItem15.Text = "Buy Rate VAT:";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.spnSellRateVAT;
            this.layoutControlItem20.CustomizationFormText = "Sell Rate VAT:";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(359, 24);
            this.layoutControlItem20.Text = "Sell Rate VAT:";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(422, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(599, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(460, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // tcgDetail
            // 
            this.tcgDetail.CustomizationFormText = "tcgDetail";
            this.tcgDetail.Location = new System.Drawing.Point(0, 23);
            this.tcgDetail.Name = "tcgDetail";
            this.tcgDetail.SelectedTabPage = this.lcgMain;
            this.tcgDetail.SelectedTabPageIndex = 0;
            this.tcgDetail.Size = new System.Drawing.Size(1059, 1175);
            this.tcgDetail.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgMain,
            this.lcgQuotePerformance});
            // 
            // lcgMain
            // 
            this.lcgMain.CustomizationFormText = "Create Quote";
            this.lcgMain.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgQuote,
            this.lcgQuoteItemBlockAdd,
            this.lcgQuoteItem,
            this.emptySpaceItem9});
            this.lcgMain.Location = new System.Drawing.Point(0, 0);
            this.lcgMain.Name = "lcgMain";
            this.lcgMain.Size = new System.Drawing.Size(1035, 1130);
            this.lcgMain.Text = "Create Quote";
            // 
            // lcgQuote
            // 
            this.lcgQuote.CustomizationFormText = "Quote";
            this.lcgQuote.ExpandButtonVisible = true;
            this.lcgQuote.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciReference,
            this.layoutControlItem6,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.lciClient,
            this.layoutControlItem8,
            this.tabbedControlGroup1,
            this.layoutControlItem11,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem4});
            this.lcgQuote.Location = new System.Drawing.Point(0, 0);
            this.lcgQuote.Name = "lcgQuote";
            this.lcgQuote.Size = new System.Drawing.Size(1035, 288);
            this.lcgQuote.Text = "Quote";
            // 
            // lciReference
            // 
            this.lciReference.Control = this.txtReference;
            this.lciReference.CustomizationFormText = "Reference:";
            this.lciReference.Location = new System.Drawing.Point(0, 0);
            this.lciReference.Name = "lciReference";
            this.lciReference.Size = new System.Drawing.Size(518, 24);
            this.lciReference.Text = "Reference:";
            this.lciReference.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.lueClientContractTypeID;
            this.layoutControlItem6.CustomizationFormText = "Contract Type:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(518, 26);
            this.layoutControlItem6.Text = "Contract Type:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.ceTransferredExchequer;
            this.layoutControlItem10.CustomizationFormText = "Transferred Exchequer:";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 196);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(323, 23);
            this.layoutControlItem10.Text = "Transferred Exchequer:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtPONumber;
            this.layoutControlItem12.CustomizationFormText = "PO Number:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 219);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem12.Text = "PO Number:";
            this.layoutControlItem12.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.lueBillingCentreCodeID;
            this.layoutControlItem14.CustomizationFormText = "Billing Centre Code:";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 170);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(518, 26);
            this.layoutControlItem14.Text = "Billing Centre Code:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lciClient
            // 
            this.lciClient.Control = this.lueClientID;
            this.lciClient.CustomizationFormText = "Client:";
            this.lciClient.Location = new System.Drawing.Point(0, 48);
            this.lciClient.Name = "lciClient";
            this.lciClient.Size = new System.Drawing.Size(518, 24);
            this.lciClient.Text = "Client:";
            this.lciClient.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.ceIsMainQuote;
            this.layoutControlItem8.CustomizationFormText = "Is Main Quote:";
            this.layoutControlItem8.Location = new System.Drawing.Point(323, 196);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(195, 23);
            this.layoutControlItem8.Text = "Is Main Quote:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(518, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(493, 243);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Notes";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.lciProfit,
            this.layoutControlItem37,
            this.layoutControlItem39});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(469, 198);
            this.layoutControlGroup2.Text = "Notes";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.memNotes;
            this.layoutControlItem16.CustomizationFormText = "Notes:";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(469, 120);
            this.layoutControlItem16.Text = "Notes:";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // lciProfit
            // 
            this.lciProfit.Control = this.txtTotalNetProfit;
            this.lciProfit.CustomizationFormText = "Net Profit:";
            this.lciProfit.Location = new System.Drawing.Point(0, 172);
            this.lciProfit.Name = "lciProfit";
            this.lciProfit.Size = new System.Drawing.Size(469, 26);
            this.lciProfit.Text = "Net Profit:";
            this.lciProfit.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.txtTotalNetCost;
            this.layoutControlItem37.CustomizationFormText = "Total Net Cost: ";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(469, 26);
            this.layoutControlItem37.Text = "Total Net Cost: ";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.txtTotalNetPrice;
            this.layoutControlItem39.CustomizationFormText = "Total Net Price: ";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(469, 26);
            this.layoutControlItem39.Text = "Total Net Price: ";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.lueSubContractorID;
            this.layoutControlItem11.CustomizationFormText = "Sub Contractor:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem11.Text = "Sub Contractor:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.lueCircuitID;
            this.layoutControlItem17.CustomizationFormText = "Circuit:";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem17.Text = "Circuit:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.lueFeederContractID;
            this.layoutControlItem19.CustomizationFormText = "Feeder Contract:";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem19.Text = "Feeder Contract:";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtPaddedWorkOrderID;
            this.layoutControlItem4.CustomizationFormText = "Padded Work Order ID:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(518, 24);
            this.layoutControlItem4.Text = "Padded Work Order ID:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lcgQuoteItemBlockAdd
            // 
            this.lcgQuoteItemBlockAdd.CustomizationFormText = "Quote Item(s)";
            this.lcgQuoteItemBlockAdd.ExpandButtonVisible = true;
            this.lcgQuoteItemBlockAdd.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.lcgQuoteItemBlockAdd.Location = new System.Drawing.Point(0, 288);
            this.lcgQuoteItemBlockAdd.Name = "lcgQuoteItemBlockAdd";
            this.lcgQuoteItemBlockAdd.Size = new System.Drawing.Size(1035, 95);
            this.lcgQuoteItemBlockAdd.Text = "Quote Item(s)";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cmbQuoteCategoryID;
            this.layoutControlItem3.CustomizationFormText = "Quote Category:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1011, 24);
            this.layoutControlItem3.Text = "Quote Category:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.cmbResourceRateID;
            this.layoutControlItem5.CustomizationFormText = "Resource Rate:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1011, 26);
            this.layoutControlItem5.Text = "Resource Rate:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lcgQuoteItem
            // 
            this.lcgQuoteItem.CustomizationFormText = "Quote Item(s)";
            this.lcgQuoteItem.ExpandButtonVisible = true;
            this.lcgQuoteItem.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciFreeTextRate,
            this.layoutControlItem13,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem7,
            this.tabbedControlGroup8,
            this.layoutControlItem40,
            this.layoutControlItem38,
            this.layoutControlItem1,
            this.layoutControlItem21,
            this.tcgClient,
            this.tcgFieldTeam,
            this.layoutControlItem36,
            this.layoutControlItem32,
            this.layoutControlItem30,
            this.lciMarkUp});
            this.lcgQuoteItem.Location = new System.Drawing.Point(0, 383);
            this.lcgQuoteItem.Name = "lcgQuoteItem";
            this.lcgQuoteItem.Size = new System.Drawing.Size(1035, 307);
            this.lcgQuoteItem.Text = "Quote Item(s)";
            // 
            // lciFreeTextRate
            // 
            this.lciFreeTextRate.Control = this.spnFreeTextRate;
            this.lciFreeTextRate.CustomizationFormText = "Free Text Rate:";
            this.lciFreeTextRate.Location = new System.Drawing.Point(421, 238);
            this.lciFreeTextRate.Name = "lciFreeTextRate";
            this.lciFreeTextRate.Size = new System.Drawing.Size(305, 24);
            this.lciFreeTextRate.Text = "Free Text Rate:";
            this.lciFreeTextRate.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.quoteItemDataNavigator;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(504, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(198, 23);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(421, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(83, 23);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(702, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(309, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lueQuoteCategoryID;
            this.layoutControlItem7.CustomizationFormText = "Quote Category:";
            this.layoutControlItem7.Location = new System.Drawing.Point(421, 23);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem7.Text = "Quote Category:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tabbedControlGroup8
            // 
            this.tabbedControlGroup8.CustomizationFormText = "tabbedControlGroup8";
            this.tabbedControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup8.Name = "tabbedControlGroup8";
            this.tabbedControlGroup8.SelectedTabPage = this.layoutControlGroup1;
            this.tabbedControlGroup8.SelectedTabPageIndex = 0;
            this.tabbedControlGroup8.Size = new System.Drawing.Size(421, 262);
            this.tabbedControlGroup8.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1});
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Item List";
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lciQuoteItemList});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(397, 217);
            this.layoutControlGroup1.Text = "Item List";
            // 
            // lciQuoteItemList
            // 
            this.lciQuoteItemList.Control = this.sp07421_UT_Quote_Item_ListGridControl;
            this.lciQuoteItemList.CustomizationFormText = "lciQuoteItemList";
            this.lciQuoteItemList.Location = new System.Drawing.Point(0, 0);
            this.lciQuoteItemList.Name = "lciQuoteItemList";
            this.lciQuoteItemList.Size = new System.Drawing.Size(397, 217);
            this.lciQuoteItemList.TextSize = new System.Drawing.Size(0, 0);
            this.lciQuoteItemList.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.lueRating;
            this.layoutControlItem40.CustomizationFormText = "Rating:";
            this.layoutControlItem40.Location = new System.Drawing.Point(691, 47);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem40.Text = "Rating:";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.lueItemUnitsID;
            this.layoutControlItem38.CustomizationFormText = "Item Units:";
            this.layoutControlItem38.Location = new System.Drawing.Point(691, 95);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(320, 26);
            this.layoutControlItem38.Text = "Item Units:";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.memDescription;
            this.layoutControlItem1.CustomizationFormText = "Description:";
            this.layoutControlItem1.Location = new System.Drawing.Point(421, 47);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(270, 74);
            this.layoutControlItem1.Text = "Description:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.lueAnalysisCodeID;
            this.layoutControlItem21.CustomizationFormText = "Analysis Code:";
            this.layoutControlItem21.Location = new System.Drawing.Point(691, 71);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(320, 24);
            this.layoutControlItem21.Text = "Analysis Code:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tcgClient
            // 
            this.tcgClient.CustomizationFormText = "tcgClient";
            this.tcgClient.Location = new System.Drawing.Point(722, 121);
            this.tcgClient.Name = "tcgClient";
            this.tcgClient.SelectedTabPage = this.layoutControlGroup5;
            this.tcgClient.SelectedTabPageIndex = 0;
            this.tcgClient.Size = new System.Drawing.Size(289, 93);
            this.tcgClient.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Field Team Rate ";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.lciSellRateTotal});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(265, 48);
            this.layoutControlGroup5.Text = "Client Rate";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.spnSellRate;
            this.layoutControlItem18.CustomizationFormText = "Sell Rate:";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(265, 24);
            this.layoutControlItem18.Text = "Sell Rate:";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lciSellRateTotal
            // 
            this.lciSellRateTotal.Control = this.spnSellRateTotal;
            this.lciSellRateTotal.CustomizationFormText = "Sell Rate Total:";
            this.lciSellRateTotal.Location = new System.Drawing.Point(0, 24);
            this.lciSellRateTotal.Name = "lciSellRateTotal";
            this.lciSellRateTotal.Size = new System.Drawing.Size(265, 24);
            this.lciSellRateTotal.Text = "Sell Rate Total:";
            this.lciSellRateTotal.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tcgFieldTeam
            // 
            this.tcgFieldTeam.CustomizationFormText = "tcgFieldTeam";
            this.tcgFieldTeam.Location = new System.Drawing.Point(421, 121);
            this.tcgFieldTeam.Name = "tcgFieldTeam";
            this.tcgFieldTeam.SelectedTabPage = this.layoutControlGroup6;
            this.tcgFieldTeam.SelectedTabPageIndex = 0;
            this.tcgFieldTeam.Size = new System.Drawing.Size(301, 93);
            this.tcgFieldTeam.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Client Rate";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.lciBuyRateTotal});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(277, 48);
            this.layoutControlGroup6.Text = "Field Team Rate";
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.spnBuyRate;
            this.layoutControlItem24.CustomizationFormText = "Buy Rate:";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem24.Text = "Buy Rate:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lciBuyRateTotal
            // 
            this.lciBuyRateTotal.Control = this.spnBuyRateTotal;
            this.lciBuyRateTotal.CustomizationFormText = "Buy Rate Total:";
            this.lciBuyRateTotal.Location = new System.Drawing.Point(0, 24);
            this.lciBuyRateTotal.Name = "lciBuyRateTotal";
            this.lciBuyRateTotal.Size = new System.Drawing.Size(277, 24);
            this.lciBuyRateTotal.Text = "Buy Rate Total:";
            this.lciBuyRateTotal.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.spnQuantity;
            this.layoutControlItem36.CustomizationFormText = "Quantity:";
            this.layoutControlItem36.Location = new System.Drawing.Point(690, 23);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem36.Text = "Quantity:";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.spnNetCost;
            this.layoutControlItem32.CustomizationFormText = "Net Cost:";
            this.layoutControlItem32.Location = new System.Drawing.Point(421, 214);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(305, 24);
            this.layoutControlItem32.Text = "Net Cost:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.spnNetPrice;
            this.layoutControlItem30.CustomizationFormText = "Net Price:";
            this.layoutControlItem30.Location = new System.Drawing.Point(726, 214);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(285, 24);
            this.layoutControlItem30.Text = "Net Price:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(115, 13);
            // 
            // lciMarkUp
            // 
            this.lciMarkUp.Control = this.spnMarkUp;
            this.lciMarkUp.CustomizationFormText = "Mark Up:";
            this.lciMarkUp.Location = new System.Drawing.Point(726, 238);
            this.lciMarkUp.Name = "lciMarkUp";
            this.lciMarkUp.Size = new System.Drawing.Size(285, 24);
            this.lciMarkUp.Text = "Mark Up:";
            this.lciMarkUp.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 690);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(1035, 440);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgQuotePerformance
            // 
            this.lcgQuotePerformance.CustomizationFormText = "Quote Perfomance";
            this.lcgQuotePerformance.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8});
            this.lcgQuotePerformance.Location = new System.Drawing.Point(0, 0);
            this.lcgQuotePerformance.Name = "lcgQuotePerformance";
            this.lcgQuotePerformance.Size = new System.Drawing.Size(1035, 1130);
            this.lcgQuotePerformance.Text = "Quote Perfomance";
            this.lcgQuotePerformance.Shown += new System.EventHandler(this.layoutControlGroup9_Shown);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Summary";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.ExpandOnDoubleClick = true;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1035, 1130);
            this.layoutControlGroup8.Text = "Quote Summary";
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Quote Perfomance";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup11,
            this.layoutControlGroup3,
            this.layoutControlGroup12});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1011, 1085);
            this.layoutControlGroup7.Text = "Quote Perfomance";
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Profit or Loss";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.layoutControlItem22});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(987, 235);
            this.layoutControlGroup11.Text = "Profit or Loss";
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(10, 190);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 190);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(10, 190);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.gaugeControl1;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(10, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(953, 190);
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Quote Item Summary";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6,
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 235);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(987, 308);
            this.layoutControlGroup3.Text = "Quote Item Summary";
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(953, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(10, 263);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 263);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(10, 263);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.chartControl1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(953, 263);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "More Breakdowns";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup10});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 543);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(987, 497);
            this.layoutControlGroup12.Text = "More Breakdowns";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Total Quote Net Price";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(456, 452);
            this.layoutControlGroup4.Text = "Total Quote Net Price";
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Net Price Breakdown";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem31,
            this.emptySpaceItem8});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(432, 403);
            this.layoutControlGroup13.Text = "Net Price Breakdown";
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.chartControl3;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(10, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(398, 360);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(10, 360);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 360);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(10, 360);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup10.CaptionImage")));
            this.layoutControlGroup10.CustomizationFormText = "Total Quote Net Cost";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem13,
            this.emptySpaceItem12,
            this.layoutControlGroup14});
            this.layoutControlGroup10.Location = new System.Drawing.Point(456, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(507, 452);
            this.layoutControlGroup10.Text = "Total Quote Net Cost";
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(0, 300);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(10, 300);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(10, 403);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(473, 0);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 403);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 403);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(10, 403);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "Net Cost Breakdown";
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26});
            this.layoutControlGroup14.Location = new System.Drawing.Point(10, 0);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(463, 403);
            this.layoutControlGroup14.Text = "Net Cost Breakdown";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.chartControl2;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(439, 360);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // sp07418_UT_Quote_ListTableAdapter
            // 
            this.sp07418_UT_Quote_ListTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager1.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager1.sp07418_UT_Quote_ListTableAdapter = this.sp07418_UT_Quote_ListTableAdapter;
            this.tableAdapterManager1.sp07421_UT_Quote_Item_ListTableAdapter = this.sp07421_UT_Quote_Item_ListTableAdapter;
            this.tableAdapterManager1.sp07424_UT_Quote_Category_Item_AddModeTableAdapter = this.sp07424_UT_Quote_Category_Item_AddModeTableAdapter;
            this.tableAdapterManager1.sp07424_UT_Quote_Category_ItemTableAdapter = this.sp07424_UT_Quote_Category_ItemTableAdapter;
            this.tableAdapterManager1.sp07428_UT_Contract_Type_ListTableAdapter = null;
            this.tableAdapterManager1.sp07431_UT_Client_Contract_Type_ListTableAdapter = this.sp07431_UT_Client_Contract_Type_ListTableAdapter;
            this.tableAdapterManager1.sp07435_UT_Item_Units_ListTableAdapter = null;
            this.tableAdapterManager1.sp07438_UT_Resource_Rate_ListTableAdapter = this.sp07438_UT_Resource_Rate_ListTableAdapter;
            this.tableAdapterManager1.sp07442_UT_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp07448_UT_POTransactionTableAdapter = null;
            this.tableAdapterManager1.sp07459_UT_Quote_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp07463_UT_Item_Units_ManagerTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_UT_QuoteTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp07421_UT_Quote_Item_ListTableAdapter
            // 
            this.sp07421_UT_Quote_Item_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07424_UT_Quote_Category_Item_AddModeTableAdapter
            // 
            this.sp07424_UT_Quote_Category_Item_AddModeTableAdapter.ClearBeforeFill = true;
            // 
            // sp07424_UT_Quote_Category_ItemTableAdapter
            // 
            this.sp07424_UT_Quote_Category_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07431_UT_Client_Contract_Type_ListTableAdapter
            // 
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07438_UT_Resource_Rate_ListTableAdapter
            // 
            this.sp07438_UT_Resource_Rate_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07427_UT_Client_ListTableAdapter
            // 
            this.sp07427_UT_Client_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_AS_11038_Billing_Centre_Code_ItemTableAdapter
            // 
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07446_UT_Picklist_By_Header_IDTableAdapter
            // 
            this.sp07446_UT_Picklist_By_Header_IDTableAdapter.ClearBeforeFill = true;
            // 
            // sp07435_UT_Item_Units_ListTableAdapter
            // 
            this.sp07435_UT_Item_Units_ListTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("add_16x16.png", "images/actions/add_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/add_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "add_16x16.png");
            this.imageCollection1.InsertGalleryImage("edit_16x16.png", "images/edit/edit_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/edit_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "edit_16x16.png");
            this.imageCollection1.InsertGalleryImage("delete_16x16.png", "images/edit/delete_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/delete_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "delete_16x16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 3);
            this.imageCollection1.Images.SetKeyName(3, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("country_16x16.png", "images/miscellaneous/country_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/miscellaneous/country_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "country_16x16.png");
            this.imageCollection1.InsertGalleryImage("insert_16x16.png", "images/actions/insert_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/insert_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "insert_16x16.png");
            // 
            // sp07450_UT_SubContractor_ListTableAdapter
            // 
            this.sp07450_UT_SubContractor_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07452_UT_Circuit_ListTableAdapter
            // 
            this.sp07452_UT_Circuit_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07453_UT_FeederContract_ListTableAdapter
            // 
            this.sp07453_UT_FeederContract_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07446_UT_PL_AnalysisCodeTableAdapter
            // 
            this.sp07446_UT_PL_AnalysisCodeTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp07009_UT_Region_ItemTableAdapter
            // 
            this.sp07009_UT_Region_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // arcScaleSpindleCapComponent1
            // 
            this.arcScaleSpindleCapComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleSpindleCapComponent1.Name = "cGauge1_SpindleCap1";
            this.arcScaleSpindleCapComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.SpindleCapShapeType.CircularFull_Style20;
            this.arcScaleSpindleCapComponent1.Size = new System.Drawing.SizeF(10F, 10F);
            this.arcScaleSpindleCapComponent1.ZOrder = -100;
            // 
            // arcScaleComponent1
            // 
            this.arcScaleComponent1.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent1.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 9F);
            this.arcScaleComponent1.AppearanceTickmarkText.Spacing = new DevExpress.XtraGauges.Core.Base.TextSpacing(2, 2, 2, 2);
            this.arcScaleComponent1.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#3A3832");
            this.arcScaleComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponent1.EndAngle = 60F;
            this.arcScaleComponent1.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent1.MajorTickmark.ShapeOffset = -10F;
            this.arcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style20_1;
            this.arcScaleComponent1.MajorTickmark.TextOffset = -18F;
            this.arcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent1.MaxValue = 100F;
            this.arcScaleComponent1.MinorTickCount = 4;
            this.arcScaleComponent1.MinorTickmark.ShapeOffset = -6F;
            this.arcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style20_2;
            this.arcScaleComponent1.Name = "scale1";
            this.arcScaleComponent1.RadiusX = 97F;
            this.arcScaleComponent1.RadiusY = 97F;
            arcScaleRange1.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#96C562");
            arcScaleRange1.EndThickness = 5F;
            arcScaleRange1.EndValue = 33F;
            arcScaleRange1.Name = "Range0";
            arcScaleRange1.ShapeOffset = 6F;
            arcScaleRange1.StartThickness = 5F;
            arcScaleRange2.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#FCD66B");
            arcScaleRange2.EndThickness = 5F;
            arcScaleRange2.EndValue = 66F;
            arcScaleRange2.Name = "Range1";
            arcScaleRange2.ShapeOffset = 6F;
            arcScaleRange2.StartThickness = 5F;
            arcScaleRange2.StartValue = 33F;
            arcScaleRange3.AppearanceRange.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#EA836D");
            arcScaleRange3.EndThickness = 5F;
            arcScaleRange3.EndValue = 100F;
            arcScaleRange3.Name = "Range2";
            arcScaleRange3.ShapeOffset = 6F;
            arcScaleRange3.StartThickness = 5F;
            arcScaleRange3.StartValue = 66F;
            this.arcScaleComponent1.Ranges.AddRange(new DevExpress.XtraGauges.Core.Model.IRange[] {
            arcScaleRange1,
            arcScaleRange2,
            arcScaleRange3});
            this.arcScaleComponent1.StartAngle = -240F;
            this.arcScaleComponent1.Value = 80F;
            // 
            // arcScaleMarkerComponent1
            // 
            this.arcScaleMarkerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleMarkerComponent1.Name = "circularGauge1_Marker1";
            this.arcScaleMarkerComponent1.Value = 80F;
            this.arcScaleMarkerComponent1.ZOrder = -100;
            // 
            // arcScaleNeedleComponent1
            // 
            this.arcScaleNeedleComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleNeedleComponent1.EndOffset = -2F;
            this.arcScaleNeedleComponent1.Name = "needle";
            this.arcScaleNeedleComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style20;
            this.arcScaleNeedleComponent1.StartOffset = -39.5F;
            this.arcScaleNeedleComponent1.ZOrder = -50;
            // 
            // arcScaleBackgroundLayerComponent1
            // 
            this.arcScaleBackgroundLayerComponent1.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent1.Name = "bg";
            this.arcScaleBackgroundLayerComponent1.ShapeType = DevExpress.XtraGauges.Core.Model.BackgroundLayerShapeType.CircularFull_Style20;
            this.arcScaleBackgroundLayerComponent1.ZOrder = 1000;
            // 
            // arcScaleBackgroundLayerComponent3
            // 
            this.arcScaleBackgroundLayerComponent3.ArcScale = this.arcScaleComponent1;
            this.arcScaleBackgroundLayerComponent3.Name = "circularGauge1_BackgroundLayer2";
            this.arcScaleBackgroundLayerComponent3.Size = new System.Drawing.SizeF(242.5F, 242.5F);
            this.arcScaleBackgroundLayerComponent3.ZOrder = 1000;
            // 
            // arcScaleComponent3
            // 
            this.arcScaleComponent3.AppearanceMajorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent3.AppearanceMajorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent3.AppearanceMinorTickmark.BorderBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent3.AppearanceMinorTickmark.ContentBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White");
            this.arcScaleComponent3.AppearanceTickmarkText.Font = new System.Drawing.Font("Tahoma", 12F);
            this.arcScaleComponent3.AppearanceTickmarkText.TextBrush = new DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:Black");
            this.arcScaleComponent3.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.arcScaleComponent3.EndAngle = 60F;
            this.arcScaleComponent3.MajorTickmark.FormatString = "{0:F0}";
            this.arcScaleComponent3.MajorTickmark.ShapeOffset = -12F;
            this.arcScaleComponent3.MajorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 0.8F);
            this.arcScaleComponent3.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_2;
            this.arcScaleComponent3.MajorTickmark.TextOffset = -27F;
            this.arcScaleComponent3.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight;
            this.arcScaleComponent3.MaxValue = 100F;
            this.arcScaleComponent3.MinorTickCount = 4;
            this.arcScaleComponent3.MinorTickmark.ShapeOffset = -5F;
            this.arcScaleComponent3.MinorTickmark.ShapeScale = new DevExpress.XtraGauges.Core.Base.FactorF2D(0.6F, 1F);
            this.arcScaleComponent3.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style4_1;
            this.arcScaleComponent3.Name = "scale1";
            this.arcScaleComponent3.RadiusX = 122F;
            this.arcScaleComponent3.RadiusY = 122F;
            this.arcScaleComponent3.StartAngle = -240F;
            // 
            // arcScaleNeedleComponent3
            // 
            this.arcScaleNeedleComponent3.ArcScale = this.arcScaleComponent3;
            this.arcScaleNeedleComponent3.EndOffset = 5F;
            this.arcScaleNeedleComponent3.Name = "needle1";
            this.arcScaleNeedleComponent3.ShapeType = DevExpress.XtraGauges.Core.Model.NeedleShapeType.CircularFull_Style4;
            this.arcScaleNeedleComponent3.StartOffset = -21F;
            this.arcScaleNeedleComponent3.ZOrder = -50;
            // 
            // arcScaleSpindleCapComponent3
            // 
            this.arcScaleSpindleCapComponent3.ArcScale = this.arcScaleComponent3;
            this.arcScaleSpindleCapComponent3.Name = "cap1";
            this.arcScaleSpindleCapComponent3.ShapeType = DevExpress.XtraGauges.Core.Model.SpindleCapShapeType.CircularFull_Style4;
            this.arcScaleSpindleCapComponent3.Size = new System.Drawing.SizeF(16F, 16F);
            this.arcScaleSpindleCapComponent3.ZOrder = -100;
            // 
            // textEdit1
            // 
            this.textEdit1.EditValue = "textEdit1";
            this.textEdit1.Location = new System.Drawing.Point(1141, 284);
            this.textEdit1.Name = "textEdit1";
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(166, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling9);
            this.textEdit1.StyleController = this.editFormDataLayoutControlGroup;
            this.textEdit1.TabIndex = 174;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.sp07421_UT_Quote_Item_ListGridControl;
            // 
            // frm_UT_Quote_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1096, 748);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Quote_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Quote";
            this.Activated += new System.EventHandler(this.frm_UT_Quote_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Quote_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Quote_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07421_UT_Quote_Item_ListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07418_UT_Quote_ListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalNetProfit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(doughnut3DSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleMarkerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linearScaleRangeBarComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueAnalysisCodeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07446UTPLAnalysisCodeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueFeederContractID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07453UTFeederContractListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCircuitID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07452UTCircuitListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueSubContractorID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07450UTSubContractorListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07421_UT_Quote_Item_ListGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRateVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRateVAT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07427_UT_Client_ListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueQuoteCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424UTQuoteCategoryItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRateTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRateTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnFreeTextRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnNetCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnMarkUp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnQuantity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueItemUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07435UTItemUnitsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueRating.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07446UTPicklistByHeaderIDBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbResourceRateID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438_UT_Resource_Rate_ListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbQuoteCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424_UT_Quote_Category_Item_AddModeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaddedWorkOrderID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientContractTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceIsMainQuote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceTransferredExchequer.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPONumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memNotes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueBillingCentreCodeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.searchLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciProfit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuoteItemBlockAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuoteItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciFreeTextRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciQuoteItemList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgClient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciSellRateTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tcgFieldTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciBuyRateTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lciMarkUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgQuotePerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleMarkerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleBackgroundLayerComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleNeedleComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.arcScaleSpindleCapComponent3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private System.Windows.Forms.BindingSource sp07418_UT_Quote_ListBindingSource;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private DataSet_UT_QuoteTableAdapters.sp07418_UT_Quote_ListTableAdapter sp07418_UT_Quote_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.TableAdapterManager tableAdapterManager1;
        private DevExpress.XtraEditors.TextEdit txtReference;
        private DevExpress.XtraEditors.TextEdit txtPaddedWorkOrderID;
        private DevExpress.XtraEditors.LookUpEdit lueClientContractTypeID;
        private DevExpress.XtraEditors.CheckEdit ceIsMainQuote;
        private DevExpress.XtraEditors.CheckEdit ceTransferredExchequer;
        private DevExpress.XtraEditors.TextEdit txtPONumber;
        private DevExpress.XtraEditors.MemoEdit memNotes;
        private DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_Item_AddModeTableAdapter sp07424_UT_Quote_Category_Item_AddModeTableAdapter;
        private System.Windows.Forms.BindingSource sp07424_UT_Quote_Category_Item_AddModeBindingSource;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbQuoteCategoryID;
        private DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter sp07438_UT_Resource_Rate_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07438_UT_Resource_Rate_ListBindingSource;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbResourceRateID;
        private DataSet_UT_QuoteTableAdapters.sp07421_UT_Quote_Item_ListTableAdapter sp07421_UT_Quote_Item_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07421_UT_Quote_Item_ListBindingSource;
        private DevExpress.XtraEditors.LookUpEdit lueQuoteCategoryID;
        private DevExpress.XtraEditors.MemoEdit memDescription;
        private DevExpress.XtraEditors.SpinEdit spnBuyRateVAT;
        private DevExpress.XtraEditors.SpinEdit spnSellRate;
        private DevExpress.XtraEditors.SpinEdit spnSellRateVAT;
        private DevExpress.XtraEditors.SpinEdit spnSellRateTotal;
        private DevExpress.XtraEditors.SpinEdit spnBuyRate;
        private DevExpress.XtraEditors.SpinEdit spnBuyRateTotal;
        private DevExpress.XtraEditors.SpinEdit spnFreeTextRate;
        private DevExpress.XtraEditors.SpinEdit spnNetPrice;
        private DevExpress.XtraEditors.SpinEdit spnNetCost;
        private DevExpress.XtraEditors.SpinEdit spnMarkUp;
        private DevExpress.XtraEditors.SpinEdit spnQuantity;
        private DevExpress.XtraEditors.LookUpEdit lueItemUnitsID;
        private DevExpress.XtraEditors.LookUpEdit lueRating;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.BindingSource sp07427_UT_Client_ListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07427_UT_Client_ListTableAdapter sp07427_UT_Client_ListTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueClientID;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.DataNavigator quoteItemDataNavigator;
        private System.Windows.Forms.BindingSource sp07424UTQuoteCategoryItemBindingSource;
        private System.Windows.Forms.BindingSource sp07431UTClientContractTypeListBindingSource;
        private System.Windows.Forms.BindingSource spAS11038BillingCentreCodeItemBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_ItemTableAdapter sp07424_UT_Quote_Category_ItemTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter sp07431_UT_Client_Contract_Type_ListTableAdapter;
        private DataSet_AS_DataEntryTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter sp_AS_11038_Billing_Centre_Code_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp07435UTItemUnitsListBindingSource;
        private System.Windows.Forms.BindingSource sp07446UTPicklistByHeaderIDBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07446_UT_Picklist_By_Header_IDTableAdapter sp07446_UT_Picklist_By_Header_IDTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07435_UT_Item_Units_ListTableAdapter sp07435_UT_Item_Units_ListTableAdapter;
        private DevExpress.XtraBars.BarButtonItem bbiFormAddAnother;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.TabbedControlGroup tcgDetail;
        private DevExpress.XtraLayout.LayoutControlGroup lcgMain;
        private DevExpress.XtraGrid.GridControl sp07421_UT_Quote_Item_ListGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRateVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRateVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colFreeTextRate;
        private DevExpress.XtraGrid.Columns.GridColumn colNetPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colNetCost;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkUp;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colItemUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colRating;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraBars.BarButtonItem bbiSaveReview;
        private DevExpress.XtraEditors.LookUpEdit lueFeederContractID;
        private DevExpress.XtraEditors.LookUpEdit lueCircuitID;
        private DevExpress.XtraEditors.LookUpEdit lueSubContractorID;
        private System.Windows.Forms.BindingSource sp07453UTFeederContractListBindingSource;
        private System.Windows.Forms.BindingSource sp07452UTCircuitListBindingSource;
        private System.Windows.Forms.BindingSource sp07450UTSubContractorListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07450_UT_SubContractor_ListTableAdapter sp07450_UT_SubContractor_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07452_UT_Circuit_ListTableAdapter sp07452_UT_Circuit_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07453_UT_FeederContract_ListTableAdapter sp07453_UT_FeederContract_ListTableAdapter;
        private DevExpress.XtraEditors.LookUpEdit lueAnalysisCodeID;
        private System.Windows.Forms.BindingSource sp07446UTPLAnalysisCodeBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07446_UT_PL_AnalysisCodeTableAdapter sp07446_UT_PL_AnalysisCodeTableAdapter;
        private DevExpress.XtraEditors.SearchLookUpEdit lueBillingCentreCodeID;
        private DevExpress.XtraGrid.Views.Grid.GridView searchLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT_EditTableAdapters.sp07009_UT_Region_ItemTableAdapter sp07009_UT_Region_ItemTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup lcgQuote;
        private DevExpress.XtraLayout.LayoutControlItem lciReference;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem lciClient;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup lcgQuotePerformance;
        private DevExpress.XtraLayout.LayoutControlGroup lcgQuoteItemBlockAdd;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup lcgQuoteItem;
        private DevExpress.XtraLayout.LayoutControlItem lciFreeTextRate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem lciMarkUp;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem lciQuoteItemList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.TabbedControlGroup tcgClient;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem lciSellRateTotal;
        private DevExpress.XtraLayout.TabbedControlGroup tcgFieldTeam;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem lciBuyRateTotal;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent arcScaleSpindleCapComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleMarkerComponent arcScaleMarkerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleBackgroundLayerComponent arcScaleBackgroundLayerComponent3;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleNeedleComponent arcScaleNeedleComponent3;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent arcScaleComponent3;
        private DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleSpindleCapComponent arcScaleSpindleCapComponent3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraGauges.Win.GaugeControl gaugeControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraCharts.ChartControl chartControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraCharts.ChartControl chartControl2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DataSet_AT_TreePickerTableAdapters.sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter sp01253_AT_Tree_Picker_columns_for_labelsTableAdapter1;
        private DevExpress.XtraEditors.TextEdit txtTotalNetPrice;
        private DevExpress.XtraEditors.TextEdit txtTotalNetCost;
        private DevExpress.XtraEditors.TextEdit txtTotalNetProfit;
        private DevExpress.XtraLayout.LayoutControlItem lciProfit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearGauge linearGauge1;
        private DevExpress.XtraGauges.Win.Base.LabelComponent labelComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleMarkerComponent linearScaleMarkerComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleComponent linearScaleComponent1;
        private DevExpress.XtraGauges.Win.Gauges.Linear.LinearScaleRangeBarComponent linearScaleRangeBarComponent1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;


    }
}

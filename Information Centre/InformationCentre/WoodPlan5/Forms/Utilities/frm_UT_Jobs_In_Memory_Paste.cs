using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_Jobs_In_Memory_Paste : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        public string _PassedInSurveyedPoleIDs = "";
        private string _SelectedSurveyedTreeIDs = "";
        private string _SelectedActionIDs = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";

        #endregion

        public frm_UT_Jobs_In_Memory_Paste()
        {
            InitializeComponent();
        }

        private void frm_UT_Jobs_In_Memory_Paste_Load(object sender, EventArgs e)
        {
            this.FormID = 50007900;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07383_UT_Surveyed_Trees_Paste_Jobs_ToTableAdapter.Fill(dataSet_UT_Edit.sp07383_UT_Surveyed_Trees_Paste_Jobs_To, _PassedInSurveyedPoleIDs);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while loading the Surveyed Poles list.\n\nPlease close the screen and try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                
                sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07384_UT_Copy_jobs_from_Memory_Actions_ListTableAdapter.Fill(dataSet_UT_Edit.sp07384_UT_Copy_jobs_from_Memory_Actions_List, (string.IsNullOrEmpty(GlobalSettings.CopiedUtilityJobIDs) ? "" : GlobalSettings.CopiedUtilityJobIDs));
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + ex.Message + "] while loading the jobs in memory.\n\nPlease close the screen and try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            gridControl1.BeginUpdate();
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            GridColumn gc = view.Columns["CheckMarkSelection"];
            gc.Fixed = FixedStyle.Left;
            view.ExpandAllGroups();
            selection1.SelectAll();
            gridControl1.EndUpdate();

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            gridControl2.BeginUpdate();
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            gc = view.Columns["CheckMarkSelection"];
            gc.Fixed = FixedStyle.Left;
            view.ExpandAllGroups();
            selection2.SelectAll();
            gridControl2.EndUpdate();

            Application.DoEvents();  // Allow Form time to repaint itself //

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
        }

        public override void PostLoadView(object objParameter)
        {
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(500025, this.GlobalSettings.UserID, strConnectionString) == 1)  // Value hardcoded to Edit Action screen where the setting to be retrieved is set most of the time //
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("ReferenceNumberPrefix");
                buttonEdit1.EditValue = _strLastUsedReferencePrefix;
            }
        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Surveyed Poles Available";
                    break;
                case "gridView2":
                    message = "No Jobs in Memory to Paste";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void buttonEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "UT_Action";
                fChildForm.PassedInFieldName = "ReferenceNumber";
                //this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    buttonEdit1.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix = fChildForm.SelectedSequence;
                }
            }
        }

        private void buttonEdit1_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(buttonEdit1, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(buttonEdit1, "");
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a Job Reference Prefix [sequence] for the Job Reference Number before proceeding.", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GetSelectedDetails();
            if (string.IsNullOrEmpty(_SelectedSurveyedTreeIDs) || string.IsNullOrEmpty(_SelectedActionIDs))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more surveyed poles and one or more actions to paste by ticking them before proceeding.", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Paste Jobs //
            int intAddEquipment = (checkEditCopyEquipment.Checked ? 1 : 0);
            int intAddMaterials = (checkEditCopyMaterials.Checked ? 1 : 0);
            GridView view = (GridView)gridControl2.MainView;

            DataSet_UT_EditTableAdapters.QueriesTableAdapter PasteRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            PasteRecords.ChangeConnectionString(strConnectionString);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Pasting Action(s)...");

            try
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        int intActionID = Convert.ToInt32(view.GetRowCellValue(i, "ActionID"));
                        PasteRecords.sp07386_UT_Paste_Jobs_Against_Surveyed_Trees(_SelectedSurveyedTreeIDs, intActionID, intAddEquipment, intAddMaterials, _strLastUsedReferencePrefix);
                        view.SetRowCellValue(i, "CheckMarkSelection", false);
                    }
                }
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while pasting the actions [" + ex.Message + "].\n\nPlease try again - if the problem persist, contact Technical Support.", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show("Record(s) pasted Successfully.", "Paste Actions", MessageBoxButtons.OK, MessageBoxIcon.Information);

            if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix))
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(500025, this.GlobalSettings.UserID, "ReferenceNumberPrefix", _strLastUsedReferencePrefix);  // Value hardcoded to Edit Action screen where the setting to be stored is set most of the time //
                default_screen_settings.SaveDefaultScreenSettings();
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            _SelectedSurveyedTreeIDs = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                _SelectedSurveyedTreeIDs = "";
                return;

            }
            else if (selection1.SelectedCount <= 0)
            {
                _SelectedSurveyedTreeIDs = "";
                return;
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _SelectedSurveyedTreeIDs += Convert.ToString(view.GetRowCellValue(i, "SurveyedTreeID")) + ",";
                    }
                }
            }

            _SelectedActionIDs = "";  // Reset any prior values first //
            view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                _SelectedActionIDs = "";
                return;

            }
            else if (selection2.SelectedCount <= 0)
            {
                _SelectedActionIDs = "";
                return;
            }
            else
            {
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _SelectedActionIDs += Convert.ToString(view.GetRowCellValue(i, "ActionID")) + ",";
                    }
                }
            }
            return;
        }




    }
}


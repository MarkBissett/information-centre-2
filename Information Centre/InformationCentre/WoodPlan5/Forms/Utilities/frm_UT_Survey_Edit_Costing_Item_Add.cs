using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_UT_Survey_Edit_Costing_Item_Add : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intTypeID = 0;
        public int intEquipmentID = 0;
        public string strTypeDescription = "";
        public string strEquipmentDescription = "";
        public decimal decEquipmentRate = (decimal)0.00;
        #endregion

        public frm_UT_Survey_Edit_Costing_Item_Add()
        {
            InitializeComponent();
        }

        private void frm_UT_Survey_Edit_Costing_Item_Add_Load(object sender, EventArgs e)
        {
            this.FormID = 500053;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
           
            sp07231_UT_Survey_Costing_Cost_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07231_UT_Survey_Costing_Cost_TypesTableAdapter.Fill(dataSet_UT.sp07231_UT_Survey_Costing_Cost_Types);

            sp07148_UT_Equipment_Master_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07148_UT_Equipment_Master_SelectTableAdapter.Fill(dataSet_UT.sp07148_UT_Equipment_Master_Select);

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
 
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\nTip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Clone Contracts\\Apply Rate Uplifts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (string.IsNullOrEmpty(gridLookUpEditCostingType.EditValue.ToString()) || gridLookUpEditCostingType.EditValue.ToString() == "0")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Cost Type before proceeding.", "Add Costing Item", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else
            {
                intTypeID = Convert.ToInt32(gridLookUpEditCostingType.EditValue);
                strTypeDescription = gridLookUpEditCostingType.Text.ToString();
            }

            if (gridLookUpEditCostingType.EditValue.ToString() == "3" && (gridLookUpEditEquipmentType.EditValue == null || string.IsNullOrEmpty(gridLookUpEditEquipmentType.EditValue.ToString()) || gridLookUpEditEquipmentType.EditValue.ToString() == "0"))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Kit Type before proceeding.", "Add Costing Item", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (gridLookUpEditCostingType.EditValue.ToString() == "3")
            {
                intEquipmentID = Convert.ToInt32(gridLookUpEditEquipmentType.EditValue);
                strEquipmentDescription = gridLookUpEditEquipmentType.Text.ToString();
                
                GridView view = gridLookUpEditEquipmentType.Properties.View;
                int intFoundRow = 0;
                intFoundRow = (gridLookUpEditEquipmentType.EditValue == DBNull.Value ? GridControl.InvalidRowHandle : view.LocateByValue(0, view.Columns["EquipmentID"], Convert.ToInt32(gridLookUpEditEquipmentType.EditValue)));
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    decEquipmentRate = Convert.ToDecimal(view.GetRowCellValue(intFoundRow, "CostPerHour"));
                }
             }
   
            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void gridLookUpEditCostingType_EditValueChanged(object sender, EventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() != "3")
            {
                labelControlEquipmentType.Enabled = false;
                gridLookUpEditEquipmentType.Enabled = false;
            }
            else
            {
                labelControlEquipmentType.Enabled = true;
                gridLookUpEditEquipmentType.Enabled = true;
            }
        }

        private void gridLookUpEditCostingType_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (glue.EditValue == null || string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0")
            {
                dxErrorProvider1.SetError(gridLookUpEditCostingType, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(gridLookUpEditCostingType, "");
            }
        }

        private void gridLookUpEditEquipmentType_Validating(object sender, CancelEventArgs e)
        {         
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (gridLookUpEditCostingType.EditValue.ToString() == "3" && (glue.EditValue == null || string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(gridLookUpEditEquipmentType, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(gridLookUpEditEquipmentType, "");
            }
        }





    
    }
}


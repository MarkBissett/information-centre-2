namespace WoodPlan5
{
    partial class frm_UT_Team_WorkOrder_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Team_WorkOrder_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barCheckItemWorkOrder = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemRPSheet = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemRiskAssessment = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItemPrint = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPrintRPSheet = new DevExpress.XtraBars.BarButtonItem();
            this.pmRPSheet = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiEditRPSheetLayout = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.SurveyDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07247UTWorkOrderEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.btnShowMap = new DevExpress.XtraEditors.SimpleButton();
            this.StatusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPDFFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPermissionEmailed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandownerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnEditRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditRecord = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07299UTTeamWorkOrderActionPicturesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyPictureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShortLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkOrderPDFHyperLinkEdit = new DevExpress.XtraEditors.HyperLinkEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTakenByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnButton = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.CreatedByTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalCostTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PaddedWorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.WorkOrderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateSubContractorSentDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colDateScheduled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApproved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colApprovedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelfBillingInvoiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceSystemBillingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTeamRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnButtonStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditStartJob = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnButtonComplete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditCompleteJob = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnButtonOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditOnHoldJob = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colTreeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeSpeciesList = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemSpinEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemTextEditHoursDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemMemoExEditTeamRemarksDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.DateCompletedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ReferenceNumberButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DateCreatedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ItemForWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateSubContractorSent = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalCost = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforCreatedBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSurveyDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup3 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPaddedWorkOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateCompleted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkOrderPDF = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07247_UT_Work_Order_EditTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07247_UT_Work_Order_EditTableAdapter();
            this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter();
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter();
            this.sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWorkOrder = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPageRPSheet = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompletionSheetID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colFinalClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit2DPMetres = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCompleteFitToBill = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTreesFelledCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemSpinEdit0DP = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.colStumpToGrindCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpsTreatedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEcoPlugsAppliedCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colFurtherWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFurtherWorkRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedByTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletedActions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalActions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobsDoneStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnButtonStart2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditStart2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnButtonComplete2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditComplete2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEditDateTimeDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2DPMetresDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit0DPDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompletionSheetQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompletionSheetID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamAnswer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditAnswer = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colAuditAnswer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuditRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuditedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasterQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAuditedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemGridLookUpEditDiabled = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditDisabled = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn40 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn44 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn45 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.xtraTabPageRiskAssessment = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskAssessmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colPersonCompletingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colElectricalAssessmentVerified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorkingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermitHolderName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNRSWAOpeningNoticeNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMEWPArialRescuers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyKitLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBanksMan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyServicesMeetPoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobilePhoneSignalBars = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestAandE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestLandLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending7 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter();
            this.sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter();
            this.sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter();
            this.sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmRPSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07247UTWorkOrderEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditRecord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07299UTTeamWorkOrderActionPicturesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFHyperLinkEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaddedWorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditStartJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCompleteJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditOnHoldJob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditTeamRemarksDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateSubContractorSent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaddedWorkOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWorkOrder.SuspendLayout();
            this.xtraTabPageRPSheet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DPMetres)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit0DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditStart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditComplete2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetresDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DPDisabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAnswer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDiabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditDisabled)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).BeginInit();
            this.xtraTabPageRiskAssessment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1095, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1095, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1095, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiEditRPSheetLayout});
            this.barManager1.MaxItemId = 28;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3,
            this.bar2});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.barButtonItemPrint,
            this.barCheckItemWorkOrder,
            this.barCheckItemRPSheet,
            this.barCheckItemRiskAssessment,
            this.barButtonItemPrintRPSheet});
            this.barManager2.MaxItemId = 32;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemWorkOrder, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemRPSheet),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemRiskAssessment),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrint, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPrintRPSheet, true)});
            this.bar2.Text = "Work Order Document";
            // 
            // barCheckItemWorkOrder
            // 
            this.barCheckItemWorkOrder.BindableChecked = true;
            this.barCheckItemWorkOrder.Caption = "Work Order";
            this.barCheckItemWorkOrder.Checked = true;
            this.barCheckItemWorkOrder.GroupIndex = 1;
            this.barCheckItemWorkOrder.Id = 28;
            this.barCheckItemWorkOrder.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemWorkOrder.ImageOptions.Image")));
            this.barCheckItemWorkOrder.Name = "barCheckItemWorkOrder";
            this.barCheckItemWorkOrder.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barCheckItemWorkOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemWorkOrder_ItemClick);
            // 
            // barCheckItemRPSheet
            // 
            this.barCheckItemRPSheet.Caption = "RP Sheet";
            this.barCheckItemRPSheet.GroupIndex = 1;
            this.barCheckItemRPSheet.Id = 29;
            this.barCheckItemRPSheet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemRPSheet.ImageOptions.Image")));
            this.barCheckItemRPSheet.Name = "barCheckItemRPSheet";
            this.barCheckItemRPSheet.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barCheckItemRPSheet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemRPSheet_ItemClick);
            // 
            // barCheckItemRiskAssessment
            // 
            this.barCheckItemRiskAssessment.Caption = "Risk Assessment";
            this.barCheckItemRiskAssessment.GroupIndex = 1;
            this.barCheckItemRiskAssessment.Id = 30;
            this.barCheckItemRiskAssessment.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barCheckItemRiskAssessment.ImageOptions.Image")));
            this.barCheckItemRiskAssessment.Name = "barCheckItemRiskAssessment";
            this.barCheckItemRiskAssessment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barCheckItemRiskAssessment.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemRiskAssessment_ItemClick);
            // 
            // barButtonItemPrint
            // 
            this.barButtonItemPrint.Caption = "View Work Order Document";
            this.barButtonItemPrint.Id = 27;
            this.barButtonItemPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrint.ImageOptions.Image")));
            this.barButtonItemPrint.Name = "barButtonItemPrint";
            this.barButtonItemPrint.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItemPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrint_ItemClick);
            // 
            // barButtonItemPrintRPSheet
            // 
            this.barButtonItemPrintRPSheet.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemPrintRPSheet.Caption = "View RP Sheet";
            this.barButtonItemPrintRPSheet.DropDownControl = this.pmRPSheet;
            this.barButtonItemPrintRPSheet.Enabled = false;
            this.barButtonItemPrintRPSheet.Id = 31;
            this.barButtonItemPrintRPSheet.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemPrintRPSheet.ImageOptions.Image")));
            this.barButtonItemPrintRPSheet.Name = "barButtonItemPrintRPSheet";
            this.barButtonItemPrintRPSheet.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barButtonItemPrintRPSheet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemPrintRPSheet_ItemClick);
            // 
            // pmRPSheet
            // 
            this.pmRPSheet.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiEditRPSheetLayout)});
            this.pmRPSheet.Manager = this.barManager1;
            this.pmRPSheet.MenuCaption = "RP Sheet Menu";
            this.pmRPSheet.Name = "pmRPSheet";
            this.pmRPSheet.ShowCaption = true;
            // 
            // bbiEditRPSheetLayout
            // 
            this.bbiEditRPSheetLayout.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiEditRPSheetLayout.Caption = "<b>Edit</b> RP Sheet Layout";
            this.bbiEditRPSheetLayout.Id = 27;
            this.bbiEditRPSheetLayout.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiEditRPSheetLayout.ImageOptions.Image")));
            this.bbiEditRPSheetLayout.Name = "bbiEditRPSheetLayout";
            this.bbiEditRPSheetLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEditRPSheetLayout_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1095, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 718);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1095, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1095, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 692);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(8, "Refresh_16x16.png");
            this.imageCollection1.Images.SetKeyName(9, "show_map_16.png");
            this.imageCollection1.InsertGalleryImage("refresh2_16x16.png", "images/actions/refresh2_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh2_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "refresh2_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SurveyDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnShowMap);
            this.dataLayoutControl1.Controls.Add(this.StatusIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl11);
            this.dataLayoutControl1.Controls.Add(this.gridControl4);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderPDFHyperLinkEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl3);
            this.dataLayoutControl1.Controls.Add(this.gridControl2);
            this.dataLayoutControl1.Controls.Add(this.CreatedByTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalCostTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PaddedWorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.WorkOrderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateSubContractorSentDateEdit);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.DateCompletedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCreatedDateEdit);
            this.dataLayoutControl1.DataSource = this.sp07247UTWorkOrderEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForWorkOrderID,
            this.ItemForSubContractorID,
            this.ItemForCreatedByStaffID,
            this.ItemForDateSubContractorSent,
            this.ItemForTotalCost,
            this.ItemForDateCreated,
            this.ItemforCreatedBy,
            this.ItemForStatusID,
            this.ItemForSurveyDate});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(830, 170, 477, 525);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1090, 666);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SurveyDateTextEdit
            // 
            this.SurveyDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "SurveyDate", true));
            this.SurveyDateTextEdit.Location = new System.Drawing.Point(136, 266);
            this.SurveyDateTextEdit.MenuManager = this.barManager1;
            this.SurveyDateTextEdit.Name = "SurveyDateTextEdit";
            this.SurveyDateTextEdit.Properties.Mask.EditMask = "d";
            this.SurveyDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.SurveyDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SurveyDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SurveyDateTextEdit, true);
            this.SurveyDateTextEdit.Size = new System.Drawing.Size(915, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SurveyDateTextEdit, optionsSpelling1);
            this.SurveyDateTextEdit.StyleController = this.dataLayoutControl1;
            this.SurveyDateTextEdit.TabIndex = 54;
            // 
            // sp07247UTWorkOrderEditBindingSource
            // 
            this.sp07247UTWorkOrderEditBindingSource.DataMember = "sp07247_UT_Work_Order_Edit";
            this.sp07247UTWorkOrderEditBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnShowMap
            // 
            this.btnShowMap.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnShowMap.Appearance.Options.UseFont = true;
            this.btnShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowMap.ImageOptions.Image")));
            this.btnShowMap.Location = new System.Drawing.Point(257, 12);
            this.btnShowMap.Name = "btnShowMap";
            this.btnShowMap.Size = new System.Drawing.Size(142, 22);
            this.btnShowMap.StyleController = this.dataLayoutControl1;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Show Work On Map - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to load the all jobs on the Work Order into the Map.\r\n\r\n<color=green><b>" +
    "Tip:</b>  Select one or more job records from the Work grid first to highlight t" +
    "hese on the Map.</color>";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.btnShowMap.SuperTip = superToolTip4;
            this.btnShowMap.TabIndex = 53;
            this.btnShowMap.Text = "Show Work on Map";
            this.btnShowMap.Click += new System.EventHandler(this.btnShowMap_Click);
            // 
            // StatusIDTextEdit
            // 
            this.StatusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "StatusID", true));
            this.StatusIDTextEdit.Location = new System.Drawing.Point(133, 164);
            this.StatusIDTextEdit.MenuManager = this.barManager1;
            this.StatusIDTextEdit.Name = "StatusIDTextEdit";
            this.StatusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StatusIDTextEdit, true);
            this.StatusIDTextEdit.Size = new System.Drawing.Size(921, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StatusIDTextEdit, optionsSpelling2);
            this.StatusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDTextEdit.TabIndex = 51;
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl11.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl11_EmbeddedNavigator_ButtonClick);
            this.gridControl11.Location = new System.Drawing.Point(36, 392);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit9,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextEdit1,
            this.repositoryItemCheckEdit7,
            this.repositoryItemButtonEditRecord});
            this.gridControl11.Size = new System.Drawing.Size(1018, 238);
            this.gridControl11.TabIndex = 52;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource
            // 
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource.DataMember = "sp07352_UT_Work_Order_Linked_Permission_Documents";
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.colDateRaised1,
            this.colPDFFile,
            this.colPermissionEmailed,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.gridColumn9,
            this.colLandownerName,
            this.colDataEntryScreenName,
            this.colReportLayoutName,
            this.colClientID,
            this.gridColumnEditRecord});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLandownerName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView11_CustomRowCellEdit);
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView11_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView11_ShowingEditor);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.DoubleClick += new System.EventHandler(this.gridView11_DoubleClick);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // colDateRaised1
            // 
            this.colDateRaised1.Caption = "Date Raised";
            this.colDateRaised1.ColumnEdit = this.repositoryItemTextEdit1;
            this.colDateRaised1.FieldName = "DateRaised";
            this.colDateRaised1.Name = "colDateRaised1";
            this.colDateRaised1.OptionsColumn.AllowEdit = false;
            this.colDateRaised1.OptionsColumn.AllowFocus = false;
            this.colDateRaised1.OptionsColumn.ReadOnly = true;
            this.colDateRaised1.Visible = true;
            this.colDateRaised1.VisibleIndex = 0;
            this.colDateRaised1.Width = 109;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "g";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colPDFFile
            // 
            this.colPDFFile.Caption = "PDF File";
            this.colPDFFile.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colPDFFile.FieldName = "PDFFile";
            this.colPDFFile.Name = "colPDFFile";
            this.colPDFFile.OptionsColumn.ReadOnly = true;
            this.colPDFFile.Visible = true;
            this.colPDFFile.VisibleIndex = 2;
            this.colPDFFile.Width = 270;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // colPermissionEmailed
            // 
            this.colPermissionEmailed.Caption = "Permission Emailed";
            this.colPermissionEmailed.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colPermissionEmailed.FieldName = "PermissionEmailed";
            this.colPermissionEmailed.Name = "colPermissionEmailed";
            this.colPermissionEmailed.OptionsColumn.AllowEdit = false;
            this.colPermissionEmailed.OptionsColumn.AllowFocus = false;
            this.colPermissionEmailed.OptionsColumn.ReadOnly = true;
            this.colPermissionEmailed.Visible = true;
            this.colPermissionEmailed.VisibleIndex = 3;
            this.colPermissionEmailed.Width = 110;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "Emailed To Client";
            this.colEmailedToClientDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.Visible = true;
            this.colEmailedToClientDate.VisibleIndex = 4;
            this.colEmailedToClientDate.Width = 102;
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "Emailed To Customer";
            this.colEmailedToCustomerDate.ColumnEdit = this.repositoryItemTextEdit1;
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.Visible = true;
            this.colEmailedToCustomerDate.VisibleIndex = 5;
            this.colEmailedToCustomerDate.Width = 121;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Remarks";
            this.gridColumn9.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.gridColumn9.FieldName = "Remarks";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colLandownerName
            // 
            this.colLandownerName.Caption = "Landowner Name";
            this.colLandownerName.FieldName = "LandownerName";
            this.colLandownerName.Name = "colLandownerName";
            this.colLandownerName.OptionsColumn.AllowEdit = false;
            this.colLandownerName.OptionsColumn.AllowFocus = false;
            this.colLandownerName.OptionsColumn.ReadOnly = true;
            this.colLandownerName.Visible = true;
            this.colLandownerName.VisibleIndex = 1;
            this.colLandownerName.Width = 184;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen Name";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.Width = 139;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout Name";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.Width = 120;
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumnEditRecord
            // 
            this.gridColumnEditRecord.Caption = "Edit Record";
            this.gridColumnEditRecord.ColumnEdit = this.repositoryItemButtonEditRecord;
            this.gridColumnEditRecord.CustomizationCaption = "Edit Button";
            this.gridColumnEditRecord.FieldName = "gridColumnEditRecord";
            this.gridColumnEditRecord.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnEditRecord.Name = "gridColumnEditRecord";
            this.gridColumnEditRecord.OptionsColumn.AllowSize = false;
            this.gridColumnEditRecord.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnEditRecord.OptionsColumn.FixedWidth = true;
            this.gridColumnEditRecord.OptionsColumn.ReadOnly = true;
            this.gridColumnEditRecord.OptionsColumn.ShowCaption = false;
            this.gridColumnEditRecord.OptionsColumn.ShowInExpressionEditor = false;
            this.gridColumnEditRecord.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnEditRecord.OptionsFilter.AllowFilter = false;
            this.gridColumnEditRecord.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumnEditRecord.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnEditRecord.Visible = true;
            this.gridColumnEditRecord.VisibleIndex = 7;
            // 
            // repositoryItemButtonEditRecord
            // 
            this.repositoryItemButtonEditRecord.AutoHeight = false;
            this.repositoryItemButtonEditRecord.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit Record", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Edit Permission Document", "edit", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditRecord.Name = "repositoryItemButtonEditRecord";
            this.repositoryItemButtonEditRecord.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditRecord.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditRecord_ButtonClick);
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07299UTTeamWorkOrderActionPicturesListBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Refresh List", "refresh")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(36, 392);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime1,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemMemoExEdit4});
            this.gridControl4.Size = new System.Drawing.Size(1018, 238);
            this.gridControl4.TabIndex = 50;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07299UTTeamWorkOrderActionPicturesListBindingSource
            // 
            this.sp07299UTTeamWorkOrderActionPicturesListBindingSource.DataMember = "sp07299_UT_Team_Work_Order_Action_Pictures_List";
            this.sp07299UTTeamWorkOrderActionPicturesListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyPictureID,
            this.gridColumn1,
            this.colLinkedToRecordTypeID,
            this.colPictureTypeID,
            this.colPicturePath,
            this.colDateTimeTaken,
            this.colAddedByStaffID,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colAddedByStaffName,
            this.colShortLinkedRecordDescription,
            this.colPictureType,
            this.colPictureTypeDescription});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeTaken, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView4_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colSurveyPictureID
            // 
            this.colSurveyPictureID.Caption = "Survey Picture ID";
            this.colSurveyPictureID.FieldName = "SurveyPictureID";
            this.colSurveyPictureID.Name = "colSurveyPictureID";
            this.colSurveyPictureID.OptionsColumn.AllowEdit = false;
            this.colSurveyPictureID.OptionsColumn.AllowFocus = false;
            this.colSurveyPictureID.OptionsColumn.ReadOnly = true;
            this.colSurveyPictureID.Width = 105;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Linked To Record ID";
            this.gridColumn1.FieldName = "LinkedToRecordID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 117;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colPictureTypeID
            // 
            this.colPictureTypeID.Caption = "Picture Type ID";
            this.colPictureTypeID.FieldName = "PictureTypeID";
            this.colPictureTypeID.Name = "colPictureTypeID";
            this.colPictureTypeID.OptionsColumn.AllowEdit = false;
            this.colPictureTypeID.OptionsColumn.AllowFocus = false;
            this.colPictureTypeID.OptionsColumn.ReadOnly = true;
            this.colPictureTypeID.Width = 95;
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture Path";
            this.colPicturePath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 4;
            this.colPicturePath.Width = 526;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.Caption = "Date Taken";
            this.colDateTimeTaken.ColumnEdit = this.repositoryItemTextEditDateTime1;
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.OptionsColumn.AllowEdit = false;
            this.colDateTimeTaken.OptionsColumn.AllowFocus = false;
            this.colDateTimeTaken.OptionsColumn.ReadOnly = true;
            this.colDateTimeTaken.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 0;
            this.colDateTimeTaken.Width = 99;
            // 
            // repositoryItemTextEditDateTime1
            // 
            this.repositoryItemTextEditDateTime1.AutoHeight = false;
            this.repositoryItemTextEditDateTime1.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime1.Name = "repositoryItemTextEditDateTime1";
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "GUID";
            this.gridColumn2.FieldName = "GUID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Remarks";
            this.gridColumn3.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn3.FieldName = "Remarks";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 5;
            this.gridColumn3.Width = 182;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Linked To";
            this.gridColumn4.FieldName = "LinkedRecordDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 103;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 1;
            this.colAddedByStaffName.Width = 142;
            // 
            // colShortLinkedRecordDescription
            // 
            this.colShortLinkedRecordDescription.Caption = "Linked To (Short)";
            this.colShortLinkedRecordDescription.FieldName = "ShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.Name = "colShortLinkedRecordDescription";
            this.colShortLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colShortLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colShortLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colShortLinkedRecordDescription.Width = 366;
            // 
            // colPictureType
            // 
            this.colPictureType.Caption = "Picture Type";
            this.colPictureType.FieldName = "PictureType";
            this.colPictureType.Name = "colPictureType";
            this.colPictureType.OptionsColumn.AllowEdit = false;
            this.colPictureType.OptionsColumn.AllowFocus = false;
            this.colPictureType.OptionsColumn.ReadOnly = true;
            this.colPictureType.Visible = true;
            this.colPictureType.VisibleIndex = 2;
            this.colPictureType.Width = 112;
            // 
            // colPictureTypeDescription
            // 
            this.colPictureTypeDescription.Caption = "Status";
            this.colPictureTypeDescription.FieldName = "PictureTypeDescription";
            this.colPictureTypeDescription.Name = "colPictureTypeDescription";
            this.colPictureTypeDescription.OptionsColumn.AllowEdit = false;
            this.colPictureTypeDescription.OptionsColumn.AllowFocus = false;
            this.colPictureTypeDescription.OptionsColumn.ReadOnly = true;
            this.colPictureTypeDescription.Visible = true;
            this.colPictureTypeDescription.VisibleIndex = 3;
            this.colPictureTypeDescription.Width = 109;
            // 
            // WorkOrderPDFHyperLinkEdit
            // 
            this.WorkOrderPDFHyperLinkEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "WorkOrderPDF", true));
            this.WorkOrderPDFHyperLinkEdit.Location = new System.Drawing.Point(430, 134);
            this.WorkOrderPDFHyperLinkEdit.MenuManager = this.barManager1;
            this.WorkOrderPDFHyperLinkEdit.Name = "WorkOrderPDFHyperLinkEdit";
            this.WorkOrderPDFHyperLinkEdit.Properties.ReadOnly = true;
            this.WorkOrderPDFHyperLinkEdit.Size = new System.Drawing.Size(624, 20);
            this.WorkOrderPDFHyperLinkEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderPDFHyperLinkEdit.TabIndex = 49;
            this.WorkOrderPDFHyperLinkEdit.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.WorkOrderPDFHyperLinkEdit_OpenLink);
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(138, 137);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(969, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling3);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 47;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07272UTWorkOrderEditLinkedMapsBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(36, 392);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl3.Size = new System.Drawing.Size(1018, 238);
            this.gridControl3.TabIndex = 41;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07272UTWorkOrderEditLinkedMapsBindingSource
            // 
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.DataMember = "sp07272_UT_Work_Order_Edit_Linked_Maps";
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDateTimeCreated,
            this.colDescription,
            this.colLinkedMapID,
            this.colLinkedToRecordID,
            this.colMap,
            this.colMapOrder,
            this.colPaddedWorkOrderID3,
            this.colPictureTakenByName,
            this.colRemarks});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDateTimeCreated
            // 
            this.colDateTimeCreated.Caption = "Created On";
            this.colDateTimeCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateTimeCreated.FieldName = "DateTimeCreated";
            this.colDateTimeCreated.Name = "colDateTimeCreated";
            this.colDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colDateTimeCreated.Visible = true;
            this.colDateTimeCreated.VisibleIndex = 5;
            this.colDateTimeCreated.Width = 97;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 288;
            // 
            // colLinkedMapID
            // 
            this.colLinkedMapID.Caption = "Linked Map ID";
            this.colLinkedMapID.FieldName = "LinkedMapID";
            this.colLinkedMapID.Name = "colLinkedMapID";
            this.colLinkedMapID.OptionsColumn.AllowEdit = false;
            this.colLinkedMapID.OptionsColumn.AllowFocus = false;
            this.colLinkedMapID.OptionsColumn.ReadOnly = true;
            this.colLinkedMapID.Width = 88;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked to Work Order ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 137;
            // 
            // colMap
            // 
            this.colMap.Caption = "Map";
            this.colMap.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colMap.FieldName = "Map";
            this.colMap.Name = "colMap";
            this.colMap.OptionsColumn.ReadOnly = true;
            this.colMap.Visible = true;
            this.colMap.VisibleIndex = 3;
            this.colMap.Width = 307;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.OptionsFilter.AllowAutoFilter = false;
            this.colMapOrder.OptionsFilter.AllowFilter = false;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 52;
            // 
            // colPaddedWorkOrderID3
            // 
            this.colPaddedWorkOrderID3.Caption = "Work Order #";
            this.colPaddedWorkOrderID3.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID3.Name = "colPaddedWorkOrderID3";
            this.colPaddedWorkOrderID3.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID3.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID3.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID3.Width = 101;
            // 
            // colPictureTakenByName
            // 
            this.colPictureTakenByName.Caption = "Created By";
            this.colPictureTakenByName.FieldName = "PictureTakenByName";
            this.colPictureTakenByName.Name = "colPictureTakenByName";
            this.colPictureTakenByName.OptionsColumn.AllowEdit = false;
            this.colPictureTakenByName.OptionsColumn.AllowFocus = false;
            this.colPictureTakenByName.OptionsColumn.ReadOnly = true;
            this.colPictureTakenByName.Visible = true;
            this.colPictureTakenByName.VisibleIndex = 2;
            this.colPictureTakenByName.Width = 91;
            // 
            // colRemarks
            // 
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 4;
            this.colRemarks.Width = 122;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            this.repositoryItemMemoExEdit3.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit3_EditValueChanged);
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(39, 193);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemButtonEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1012, 93);
            this.gridControl2.TabIndex = 40;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource
            // 
            this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource.DataMember = "sp07294_UT_Team_Work_Order_Linked_SubContractors";
            this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderTeamID,
            this.colWorkOrderID1,
            this.colSubContractorID,
            this.colRemarks1,
            this.colGUID1,
            this.colSubContractorName,
            this.colInternalContractor,
            this.colMobileTel,
            this.colTelephone1,
            this.colTelephone2,
            this.colPostcode,
            this.colPaddedWorkOrderID2,
            this.colTeamMemberCount,
            this.gridColumnButton});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.ViewCaption = "Team(s) Assigned To";
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colWorkOrderTeamID
            // 
            this.colWorkOrderTeamID.Caption = "Work Order Team ID";
            this.colWorkOrderTeamID.FieldName = "WorkOrderTeamID";
            this.colWorkOrderTeamID.Name = "colWorkOrderTeamID";
            this.colWorkOrderTeamID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderTeamID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderTeamID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderTeamID.Width = 120;
            // 
            // colWorkOrderID1
            // 
            this.colWorkOrderID1.Caption = "Work Order ID";
            this.colWorkOrderID1.FieldName = "WorkOrderID";
            this.colWorkOrderID1.Name = "colWorkOrderID1";
            this.colWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID1.Width = 91;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 3;
            this.colRemarks1.Width = 116;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            this.repositoryItemMemoExEdit1.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit1_EditValueChanged_1);
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Sub-Contractor Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 0;
            this.colSubContractorName.Width = 288;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 61;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Telephone";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 1;
            this.colMobileTel.Width = 117;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 2;
            this.colTelephone1.Width = 97;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 98;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            // 
            // colPaddedWorkOrderID2
            // 
            this.colPaddedWorkOrderID2.Caption = "Work Order #";
            this.colPaddedWorkOrderID2.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID2.Name = "colPaddedWorkOrderID2";
            this.colPaddedWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID2.Width = 88;
            // 
            // colTeamMemberCount
            // 
            this.colTeamMemberCount.Caption = "Team Members on Site";
            this.colTeamMemberCount.FieldName = "TeamMemberCount";
            this.colTeamMemberCount.Name = "colTeamMemberCount";
            this.colTeamMemberCount.OptionsColumn.AllowEdit = false;
            this.colTeamMemberCount.OptionsColumn.AllowFocus = false;
            this.colTeamMemberCount.OptionsColumn.ReadOnly = true;
            this.colTeamMemberCount.Visible = true;
            this.colTeamMemberCount.VisibleIndex = 4;
            this.colTeamMemberCount.Width = 129;
            // 
            // gridColumnButton
            // 
            this.gridColumnButton.Caption = "Team Members Present";
            this.gridColumnButton.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumnButton.FieldName = "gridColumnButton";
            this.gridColumnButton.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnButton.Name = "gridColumnButton";
            this.gridColumnButton.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButton.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButton.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButton.OptionsColumn.AllowMove = false;
            this.gridColumnButton.OptionsColumn.AllowShowHide = false;
            this.gridColumnButton.OptionsColumn.AllowSize = false;
            this.gridColumnButton.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButton.OptionsColumn.FixedWidth = true;
            this.gridColumnButton.OptionsColumn.ReadOnly = true;
            this.gridColumnButton.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButton.OptionsFilter.AllowFilter = false;
            this.gridColumnButton.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButton.Visible = true;
            this.gridColumnButton.VisibleIndex = 5;
            this.gridColumnButton.Width = 133;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Record Team Members - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to record Team Members on Site.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Record Team Members", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "RecordTeamMembers", superToolTip5, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            this.repositoryItemButtonEdit1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
            // 
            // CreatedByTextEdit
            // 
            this.CreatedByTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "CreatedBy", true));
            this.CreatedByTextEdit.Location = new System.Drawing.Point(145, 197);
            this.CreatedByTextEdit.MenuManager = this.barManager1;
            this.CreatedByTextEdit.Name = "CreatedByTextEdit";
            this.CreatedByTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByTextEdit, true);
            this.CreatedByTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByTextEdit, optionsSpelling4);
            this.CreatedByTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByTextEdit.TabIndex = 48;
            // 
            // TotalCostTextEdit
            // 
            this.TotalCostTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "TotalCost", true));
            this.TotalCostTextEdit.Location = new System.Drawing.Point(145, 197);
            this.TotalCostTextEdit.MenuManager = this.barManager1;
            this.TotalCostTextEdit.Name = "TotalCostTextEdit";
            this.TotalCostTextEdit.Properties.Mask.EditMask = "c";
            this.TotalCostTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalCostTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalCostTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalCostTextEdit, true);
            this.TotalCostTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalCostTextEdit, optionsSpelling5);
            this.TotalCostTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalCostTextEdit.TabIndex = 46;
            // 
            // SubContractorIDTextEdit
            // 
            this.SubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "SubContractorID", true));
            this.SubContractorIDTextEdit.Location = new System.Drawing.Point(145, 269);
            this.SubContractorIDTextEdit.MenuManager = this.barManager1;
            this.SubContractorIDTextEdit.Name = "SubContractorIDTextEdit";
            this.SubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorIDTextEdit, true);
            this.SubContractorIDTextEdit.Size = new System.Drawing.Size(950, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorIDTextEdit, optionsSpelling6);
            this.SubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDTextEdit.TabIndex = 45;
            // 
            // PaddedWorkOrderIDTextEdit
            // 
            this.PaddedWorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "PaddedWorkOrderID", true));
            this.PaddedWorkOrderIDTextEdit.Location = new System.Drawing.Point(133, 110);
            this.PaddedWorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.PaddedWorkOrderIDTextEdit.Name = "PaddedWorkOrderIDTextEdit";
            this.PaddedWorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PaddedWorkOrderIDTextEdit, true);
            this.PaddedWorkOrderIDTextEdit.Size = new System.Drawing.Size(196, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PaddedWorkOrderIDTextEdit, optionsSpelling7);
            this.PaddedWorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PaddedWorkOrderIDTextEdit.TabIndex = 43;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07247UTWorkOrderEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(241, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 40;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // WorkOrderIDTextEdit
            // 
            this.WorkOrderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "WorkOrderID", true));
            this.WorkOrderIDTextEdit.Location = new System.Drawing.Point(121, 94);
            this.WorkOrderIDTextEdit.MenuManager = this.barManager1;
            this.WorkOrderIDTextEdit.Name = "WorkOrderIDTextEdit";
            this.WorkOrderIDTextEdit.Properties.Mask.EditMask = "0000000";
            this.WorkOrderIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WorkOrderIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WorkOrderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkOrderIDTextEdit, true);
            this.WorkOrderIDTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkOrderIDTextEdit, optionsSpelling8);
            this.WorkOrderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkOrderIDTextEdit.TabIndex = 4;
            // 
            // DateSubContractorSentDateEdit
            // 
            this.DateSubContractorSentDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateSubContractorSent", true));
            this.DateSubContractorSentDateEdit.EditValue = null;
            this.DateSubContractorSentDateEdit.Location = new System.Drawing.Point(145, 173);
            this.DateSubContractorSentDateEdit.MenuManager = this.barManager1;
            this.DateSubContractorSentDateEdit.Name = "DateSubContractorSentDateEdit";
            this.DateSubContractorSentDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateSubContractorSentDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateSubContractorSentDateEdit.Properties.Mask.EditMask = "g";
            this.DateSubContractorSentDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateSubContractorSentDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateSubContractorSentDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateSubContractorSentDateEdit.Properties.ReadOnly = true;
            this.DateSubContractorSentDateEdit.Size = new System.Drawing.Size(950, 20);
            this.DateSubContractorSentDateEdit.StyleController = this.dataLayoutControl1;
            this.DateSubContractorSentDateEdit.TabIndex = 9;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 392);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1018, 238);
            this.gridSplitContainer1.TabIndex = 42;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "View Work Order on Map", "map_view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemDateEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemButtonEditCompleteJob,
            this.repositoryItemSpinEditHours,
            this.repositoryItemButtonEditStartJob,
            this.repositoryItemTextEditHoursDisabled,
            this.repositoryItemMemoExEditTeamRemarksDisabled,
            this.repositoryItemButtonEditOnHoldJob});
            this.gridControl1.Size = new System.Drawing.Size(1018, 238);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07249UTWorkOrderManagerLinkedActionsEditBindingSource
            // 
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.DataMember = "sp07249_UT_Work_Order_Manager_Linked_Actions_Edit";
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.colSurveyedTreeID,
            this.colJobTypeID,
            this.colReferenceNumber,
            this.colDateRaised,
            this.colDateScheduled,
            this.colDateCompleted,
            this.colApproved,
            this.colApprovedByStaffID,
            this.colWorkOrderID,
            this.colSelfBillingInvoiceID,
            this.colFinanceSystemBillingID,
            this.colRemarks2,
            this.colGUID,
            this.colLinkedRecordDescription,
            this.colJobDescription,
            this.colPaddedWorkOrderID,
            this.colWorkOrderSortOrder,
            this.colLastStartTime,
            this.colTotalTimeTaken,
            this.colTeamRemarks,
            this.colActionStatusID,
            this.colActionStatus,
            this.gridColumnButtonStart,
            this.gridColumnButtonComplete,
            this.gridColumnButtonOnHold,
            this.colTreeID,
            this.colSurveyedPoleID1,
            this.colCircuitName1,
            this.colPoleNumber1,
            this.colTreeReferenceNumber,
            this.colPoleID,
            this.colSurveyID,
            this.colActualHours,
            this.colEstimatedHours,
            this.colPossibleLiveWork,
            this.colTeamOnHold,
            this.colTreeSpeciesList,
            this.colHeldUpType,
            this.colIsShutdownRequired});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colActionID
            // 
            this.colActionID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colActionID.AppearanceCell.Options.UseForeColor = true;
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedTreeID
            // 
            this.colSurveyedTreeID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colSurveyedTreeID.AppearanceCell.Options.UseForeColor = true;
            this.colSurveyedTreeID.Caption = "Surveyed Tree ID";
            this.colSurveyedTreeID.FieldName = "SurveyedTreeID";
            this.colSurveyedTreeID.Name = "colSurveyedTreeID";
            this.colSurveyedTreeID.OptionsColumn.AllowEdit = false;
            this.colSurveyedTreeID.OptionsColumn.AllowFocus = false;
            this.colSurveyedTreeID.OptionsColumn.ReadOnly = true;
            this.colSurveyedTreeID.Width = 106;
            // 
            // colJobTypeID
            // 
            this.colJobTypeID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colJobTypeID.AppearanceCell.Options.UseForeColor = true;
            this.colJobTypeID.Caption = "Job Type ID";
            this.colJobTypeID.FieldName = "JobTypeID";
            this.colJobTypeID.Name = "colJobTypeID";
            this.colJobTypeID.OptionsColumn.AllowEdit = false;
            this.colJobTypeID.OptionsColumn.AllowFocus = false;
            this.colJobTypeID.OptionsColumn.ReadOnly = true;
            this.colJobTypeID.Width = 79;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colReferenceNumber.AppearanceCell.Options.UseForeColor = true;
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 6;
            this.colReferenceNumber.Width = 112;
            // 
            // colDateRaised
            // 
            this.colDateRaised.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colDateRaised.AppearanceCell.Options.UseForeColor = true;
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Width = 99;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colDateScheduled
            // 
            this.colDateScheduled.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colDateScheduled.AppearanceCell.Options.UseForeColor = true;
            this.colDateScheduled.Caption = "Date Scheduled";
            this.colDateScheduled.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateScheduled.FieldName = "DateScheduled";
            this.colDateScheduled.Name = "colDateScheduled";
            this.colDateScheduled.OptionsColumn.AllowEdit = false;
            this.colDateScheduled.OptionsColumn.AllowFocus = false;
            this.colDateScheduled.OptionsColumn.ReadOnly = true;
            this.colDateScheduled.Visible = true;
            this.colDateScheduled.VisibleIndex = 8;
            this.colDateScheduled.Width = 96;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colDateCompleted.AppearanceCell.Options.UseForeColor = true;
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 10;
            this.colDateCompleted.Width = 98;
            // 
            // colApproved
            // 
            this.colApproved.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colApproved.AppearanceCell.Options.UseForeColor = true;
            this.colApproved.Caption = "Approved";
            this.colApproved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colApproved.FieldName = "Approved";
            this.colApproved.Name = "colApproved";
            this.colApproved.OptionsColumn.AllowEdit = false;
            this.colApproved.OptionsColumn.AllowFocus = false;
            this.colApproved.OptionsColumn.ReadOnly = true;
            this.colApproved.Visible = true;
            this.colApproved.VisibleIndex = 18;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colApprovedByStaffID
            // 
            this.colApprovedByStaffID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colApprovedByStaffID.AppearanceCell.Options.UseForeColor = true;
            this.colApprovedByStaffID.Caption = "Apporved By Staff ID";
            this.colApprovedByStaffID.FieldName = "ApprovedByStaffID";
            this.colApprovedByStaffID.Name = "colApprovedByStaffID";
            this.colApprovedByStaffID.OptionsColumn.AllowEdit = false;
            this.colApprovedByStaffID.OptionsColumn.AllowFocus = false;
            this.colApprovedByStaffID.OptionsColumn.ReadOnly = true;
            this.colApprovedByStaffID.Width = 124;
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colWorkOrderID.AppearanceCell.Options.UseForeColor = true;
            this.colWorkOrderID.Caption = "Work Order";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 77;
            // 
            // colSelfBillingInvoiceID
            // 
            this.colSelfBillingInvoiceID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colSelfBillingInvoiceID.AppearanceCell.Options.UseForeColor = true;
            this.colSelfBillingInvoiceID.Caption = "Self Billing ID";
            this.colSelfBillingInvoiceID.FieldName = "SelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.Name = "colSelfBillingInvoiceID";
            this.colSelfBillingInvoiceID.OptionsColumn.AllowEdit = false;
            this.colSelfBillingInvoiceID.OptionsColumn.AllowFocus = false;
            this.colSelfBillingInvoiceID.OptionsColumn.ReadOnly = true;
            this.colSelfBillingInvoiceID.Width = 82;
            // 
            // colFinanceSystemBillingID
            // 
            this.colFinanceSystemBillingID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colFinanceSystemBillingID.AppearanceCell.Options.UseForeColor = true;
            this.colFinanceSystemBillingID.Caption = "Finance Billing ID";
            this.colFinanceSystemBillingID.FieldName = "FinanceSystemBillingID";
            this.colFinanceSystemBillingID.Name = "colFinanceSystemBillingID";
            this.colFinanceSystemBillingID.OptionsColumn.AllowEdit = false;
            this.colFinanceSystemBillingID.OptionsColumn.AllowFocus = false;
            this.colFinanceSystemBillingID.OptionsColumn.ReadOnly = true;
            this.colFinanceSystemBillingID.Width = 101;
            // 
            // colRemarks2
            // 
            this.colRemarks2.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colRemarks2.AppearanceCell.Options.UseForeColor = true;
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 11;
            this.colRemarks2.Width = 119;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colGUID.AppearanceCell.Options.UseForeColor = true;
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colLinkedRecordDescription.AppearanceCell.Options.UseForeColor = true;
            this.colLinkedRecordDescription.Caption = "Linked To Tree";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 226;
            // 
            // colJobDescription
            // 
            this.colJobDescription.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colJobDescription.AppearanceCell.Options.UseForeColor = true;
            this.colJobDescription.Caption = "Job Description";
            this.colJobDescription.FieldName = "JobDescription";
            this.colJobDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colJobDescription.Name = "colJobDescription";
            this.colJobDescription.OptionsColumn.AllowEdit = false;
            this.colJobDescription.OptionsColumn.AllowFocus = false;
            this.colJobDescription.OptionsColumn.ReadOnly = true;
            this.colJobDescription.Visible = true;
            this.colJobDescription.VisibleIndex = 5;
            this.colJobDescription.Width = 119;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colPaddedWorkOrderID.AppearanceCell.Options.UseForeColor = true;
            this.colPaddedWorkOrderID.Caption = "Linked To Work Order";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Width = 125;
            // 
            // colWorkOrderSortOrder
            // 
            this.colWorkOrderSortOrder.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colWorkOrderSortOrder.AppearanceCell.Options.UseForeColor = true;
            this.colWorkOrderSortOrder.Caption = "Order";
            this.colWorkOrderSortOrder.FieldName = "WorkOrderSortOrder";
            this.colWorkOrderSortOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colWorkOrderSortOrder.Name = "colWorkOrderSortOrder";
            this.colWorkOrderSortOrder.OptionsColumn.AllowEdit = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowFocus = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colWorkOrderSortOrder.OptionsColumn.ReadOnly = true;
            this.colWorkOrderSortOrder.OptionsFilter.AllowAutoFilter = false;
            this.colWorkOrderSortOrder.OptionsFilter.AllowFilter = false;
            this.colWorkOrderSortOrder.Visible = true;
            this.colWorkOrderSortOrder.VisibleIndex = 1;
            this.colWorkOrderSortOrder.Width = 50;
            // 
            // colLastStartTime
            // 
            this.colLastStartTime.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colLastStartTime.AppearanceCell.Options.UseForeColor = true;
            this.colLastStartTime.Caption = "Last Start Time";
            this.colLastStartTime.ColumnEdit = this.repositoryItemDateEdit2;
            this.colLastStartTime.FieldName = "LastStartTime";
            this.colLastStartTime.Name = "colLastStartTime";
            this.colLastStartTime.OptionsColumn.AllowEdit = false;
            this.colLastStartTime.OptionsColumn.AllowFocus = false;
            this.colLastStartTime.OptionsColumn.ReadOnly = true;
            this.colLastStartTime.Width = 93;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colTotalTimeTaken.AppearanceCell.Options.UseForeColor = true;
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.Width = 102;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colTeamRemarks
            // 
            this.colTeamRemarks.Caption = "Team Remarks";
            this.colTeamRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTeamRemarks.FieldName = "TeamRemarks";
            this.colTeamRemarks.Name = "colTeamRemarks";
            this.colTeamRemarks.Visible = true;
            this.colTeamRemarks.VisibleIndex = 12;
            this.colTeamRemarks.Width = 118;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colActionStatusID.AppearanceCell.Options.UseForeColor = true;
            this.colActionStatusID.Caption = "Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colActionStatus
            // 
            this.colActionStatus.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colActionStatus.AppearanceCell.Options.UseForeColor = true;
            this.colActionStatus.Caption = "Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 9;
            this.colActionStatus.Width = 76;
            // 
            // gridColumnButtonStart
            // 
            this.gridColumnButtonStart.Caption = "Start";
            this.gridColumnButtonStart.ColumnEdit = this.repositoryItemButtonEditStartJob;
            this.gridColumnButtonStart.FieldName = "gridColumnButtonStart";
            this.gridColumnButtonStart.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumnButtonStart.Name = "gridColumnButtonStart";
            this.gridColumnButtonStart.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButtonStart.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart.OptionsColumn.AllowMove = false;
            this.gridColumnButtonStart.OptionsColumn.AllowShowHide = false;
            this.gridColumnButtonStart.OptionsColumn.AllowSize = false;
            this.gridColumnButtonStart.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart.OptionsColumn.FixedWidth = true;
            this.gridColumnButtonStart.OptionsColumn.ReadOnly = true;
            this.gridColumnButtonStart.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButtonStart.OptionsFilter.AllowFilter = false;
            this.gridColumnButtonStart.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButtonStart.Visible = true;
            this.gridColumnButtonStart.VisibleIndex = 0;
            this.gridColumnButtonStart.Width = 36;
            // 
            // repositoryItemButtonEditStartJob
            // 
            this.repositoryItemButtonEditStartJob.AutoHeight = false;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem6.Text = "Start - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = "Click me to Start the job. Once the job is started, the record can be edited.\r\n\r\n" +
    "Click the Completed button once the job is finished or the On-Hold button if the" +
    " job cannot be completed.";
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.repositoryItemButtonEditStartJob.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Start", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "", "start", superToolTip6, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditStartJob.Name = "repositoryItemButtonEditStartJob";
            this.repositoryItemButtonEditStartJob.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditStartJob.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditStartJob_ButtonClick);
            // 
            // gridColumnButtonComplete
            // 
            this.gridColumnButtonComplete.Caption = "Completed";
            this.gridColumnButtonComplete.ColumnEdit = this.repositoryItemButtonEditCompleteJob;
            this.gridColumnButtonComplete.FieldName = "gridColumnButtonComplete";
            this.gridColumnButtonComplete.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnButtonComplete.Name = "gridColumnButtonComplete";
            this.gridColumnButtonComplete.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButtonComplete.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete.OptionsColumn.AllowMove = false;
            this.gridColumnButtonComplete.OptionsColumn.AllowShowHide = false;
            this.gridColumnButtonComplete.OptionsColumn.AllowSize = false;
            this.gridColumnButtonComplete.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete.OptionsColumn.FixedWidth = true;
            this.gridColumnButtonComplete.OptionsColumn.ReadOnly = true;
            this.gridColumnButtonComplete.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButtonComplete.OptionsFilter.AllowFilter = false;
            this.gridColumnButtonComplete.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButtonComplete.Visible = true;
            this.gridColumnButtonComplete.VisibleIndex = 20;
            this.gridColumnButtonComplete.Width = 63;
            // 
            // repositoryItemButtonEditCompleteJob
            // 
            this.repositoryItemButtonEditCompleteJob.AutoHeight = false;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem7.Text = "Set Completed - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "Click me to set Job as Completed.";
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.repositoryItemButtonEditCompleteJob.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Completed", -1, true, true, false, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "", "Completed", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditCompleteJob.Name = "repositoryItemButtonEditCompleteJob";
            this.repositoryItemButtonEditCompleteJob.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditCompleteJob.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditCompleteJob_ButtonClick);
            // 
            // gridColumnButtonOnHold
            // 
            this.gridColumnButtonOnHold.Caption = "On-Hold";
            this.gridColumnButtonOnHold.ColumnEdit = this.repositoryItemButtonEditOnHoldJob;
            this.gridColumnButtonOnHold.FieldName = "gridColumnButtonOnHold";
            this.gridColumnButtonOnHold.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnButtonOnHold.Name = "gridColumnButtonOnHold";
            this.gridColumnButtonOnHold.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonOnHold.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButtonOnHold.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonOnHold.OptionsColumn.AllowMove = false;
            this.gridColumnButtonOnHold.OptionsColumn.AllowShowHide = false;
            this.gridColumnButtonOnHold.OptionsColumn.AllowSize = false;
            this.gridColumnButtonOnHold.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonOnHold.OptionsColumn.FixedWidth = true;
            this.gridColumnButtonOnHold.OptionsColumn.ReadOnly = true;
            this.gridColumnButtonOnHold.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButtonOnHold.OptionsFilter.AllowFilter = false;
            this.gridColumnButtonOnHold.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButtonOnHold.Visible = true;
            this.gridColumnButtonOnHold.VisibleIndex = 19;
            this.gridColumnButtonOnHold.Width = 50;
            // 
            // repositoryItemButtonEditOnHoldJob
            // 
            this.repositoryItemButtonEditOnHoldJob.AutoHeight = false;
            toolTipTitleItem8.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            toolTipTitleItem8.Appearance.Options.UseImage = true;
            toolTipTitleItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            toolTipTitleItem8.Text = "Set On-Hold - Information";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "Click me to set Job as On-Hold.";
            superToolTip8.Items.Add(toolTipTitleItem8);
            superToolTip8.Items.Add(toolTipItem8);
            this.repositoryItemButtonEditOnHoldJob.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "On-Hold", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "", "OnHold", superToolTip8, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditOnHoldJob.Name = "repositoryItemButtonEditOnHoldJob";
            this.repositoryItemButtonEditOnHoldJob.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditOnHoldJob.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditOnHoldJob_ButtonClick);
            // 
            // colTreeID
            // 
            this.colTreeID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colTreeID.AppearanceCell.Options.UseForeColor = true;
            this.colTreeID.FieldName = "TreeID";
            this.colTreeID.Name = "colTreeID";
            this.colTreeID.OptionsColumn.AllowEdit = false;
            this.colTreeID.OptionsColumn.AllowFocus = false;
            this.colTreeID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyedPoleID1
            // 
            this.colSurveyedPoleID1.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colSurveyedPoleID1.AppearanceCell.Options.UseForeColor = true;
            this.colSurveyedPoleID1.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID1.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID1.Name = "colSurveyedPoleID1";
            this.colSurveyedPoleID1.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID1.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID1.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID1.Width = 104;
            // 
            // colCircuitName1
            // 
            this.colCircuitName1.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCircuitName1.AppearanceCell.Options.UseForeColor = true;
            this.colCircuitName1.Caption = "Circuit";
            this.colCircuitName1.FieldName = "CircuitName";
            this.colCircuitName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName1.Name = "colCircuitName1";
            this.colCircuitName1.OptionsColumn.AllowEdit = false;
            this.colCircuitName1.OptionsColumn.AllowFocus = false;
            this.colCircuitName1.OptionsColumn.ReadOnly = true;
            this.colCircuitName1.Visible = true;
            this.colCircuitName1.VisibleIndex = 2;
            // 
            // colPoleNumber1
            // 
            this.colPoleNumber1.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colPoleNumber1.AppearanceCell.Options.UseForeColor = true;
            this.colPoleNumber1.Caption = "Pole Number";
            this.colPoleNumber1.FieldName = "PoleNumber";
            this.colPoleNumber1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber1.Name = "colPoleNumber1";
            this.colPoleNumber1.OptionsColumn.AllowEdit = false;
            this.colPoleNumber1.OptionsColumn.AllowFocus = false;
            this.colPoleNumber1.OptionsColumn.ReadOnly = true;
            this.colPoleNumber1.Visible = true;
            this.colPoleNumber1.VisibleIndex = 3;
            this.colPoleNumber1.Width = 81;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colTreeReferenceNumber.AppearanceCell.Options.UseForeColor = true;
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 4;
            this.colTreeReferenceNumber.Width = 96;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colSurveyID
            // 
            this.colSurveyID.Caption = "Survey ID";
            this.colSurveyID.FieldName = "SurveyID";
            this.colSurveyID.Name = "colSurveyID";
            this.colSurveyID.OptionsColumn.AllowEdit = false;
            this.colSurveyID.OptionsColumn.AllowFocus = false;
            this.colSurveyID.OptionsColumn.ReadOnly = true;
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ActualHours", "{0:######0.00 Hours}")});
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 17;
            this.colActualHours.Width = 93;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EstimatedHours", "{0:######0.00 Hours}")});
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 16;
            this.colEstimatedHours.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.Visible = true;
            this.colTeamOnHold.VisibleIndex = 15;
            this.colTeamOnHold.Width = 87;
            // 
            // colTreeSpeciesList
            // 
            this.colTreeSpeciesList.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colTreeSpeciesList.AppearanceCell.Options.UseForeColor = true;
            this.colTreeSpeciesList.Caption = "Tree Species";
            this.colTreeSpeciesList.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTreeSpeciesList.FieldName = "TreeSpeciesList";
            this.colTreeSpeciesList.Name = "colTreeSpeciesList";
            this.colTreeSpeciesList.OptionsColumn.ReadOnly = true;
            this.colTreeSpeciesList.Visible = true;
            this.colTreeSpeciesList.VisibleIndex = 7;
            this.colTreeSpeciesList.Width = 80;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colHeldUpType.AppearanceCell.Options.UseForeColor = true;
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 13;
            this.colHeldUpType.Width = 104;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colIsShutdownRequired.AppearanceCell.Options.UseForeColor = true;
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 14;
            this.colIsShutdownRequired.Width = 113;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // repositoryItemSpinEditHours
            // 
            this.repositoryItemSpinEditHours.AutoHeight = false;
            this.repositoryItemSpinEditHours.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemSpinEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEditHours.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEditHours.Name = "repositoryItemSpinEditHours";
            this.repositoryItemSpinEditHours.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEditHours_EditValueChanged);
            // 
            // repositoryItemTextEditHoursDisabled
            // 
            this.repositoryItemTextEditHoursDisabled.AutoHeight = false;
            this.repositoryItemTextEditHoursDisabled.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHoursDisabled.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHoursDisabled.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHoursDisabled.Name = "repositoryItemTextEditHoursDisabled";
            this.repositoryItemTextEditHoursDisabled.ReadOnly = true;
            // 
            // repositoryItemMemoExEditTeamRemarksDisabled
            // 
            this.repositoryItemMemoExEditTeamRemarksDisabled.AutoHeight = false;
            this.repositoryItemMemoExEditTeamRemarksDisabled.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditTeamRemarksDisabled.Name = "repositoryItemMemoExEditTeamRemarksDisabled";
            this.repositoryItemMemoExEditTeamRemarksDisabled.ReadOnly = true;
            this.repositoryItemMemoExEditTeamRemarksDisabled.ShowIcon = false;
            // 
            // DateCompletedDateEdit
            // 
            this.DateCompletedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateCompleted", true));
            this.DateCompletedDateEdit.EditValue = null;
            this.DateCompletedDateEdit.Location = new System.Drawing.Point(133, 134);
            this.DateCompletedDateEdit.MenuManager = this.barManager1;
            this.DateCompletedDateEdit.Name = "DateCompletedDateEdit";
            this.DateCompletedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateCompletedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCompletedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCompletedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCompletedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCompletedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateCompletedDateEdit.Properties.ReadOnly = true;
            this.DateCompletedDateEdit.Size = new System.Drawing.Size(196, 20);
            this.DateCompletedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCompletedDateEdit.TabIndex = 24;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 110);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(1018, 179);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling9);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 26;
            // 
            // ReferenceNumberButtonEdit
            // 
            this.ReferenceNumberButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberButtonEdit.Location = new System.Drawing.Point(430, 110);
            this.ReferenceNumberButtonEdit.MenuManager = this.barManager1;
            this.ReferenceNumberButtonEdit.Name = "ReferenceNumberButtonEdit";
            this.ReferenceNumberButtonEdit.Properties.MaxLength = 20;
            this.ReferenceNumberButtonEdit.Properties.ReadOnly = true;
            this.ReferenceNumberButtonEdit.Size = new System.Drawing.Size(624, 20);
            this.ReferenceNumberButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberButtonEdit.TabIndex = 31;
            // 
            // DateCreatedDateEdit
            // 
            this.DateCreatedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07247UTWorkOrderEditBindingSource, "DateCreated", true));
            this.DateCreatedDateEdit.EditValue = null;
            this.DateCreatedDateEdit.Location = new System.Drawing.Point(145, 197);
            this.DateCreatedDateEdit.MenuManager = this.barManager1;
            this.DateCreatedDateEdit.Name = "DateCreatedDateEdit";
            this.DateCreatedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCreatedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCreatedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCreatedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCreatedDateEdit.Properties.ReadOnly = true;
            this.DateCreatedDateEdit.Size = new System.Drawing.Size(950, 20);
            this.DateCreatedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCreatedDateEdit.TabIndex = 36;
            // 
            // ItemForWorkOrderID
            // 
            this.ItemForWorkOrderID.Control = this.WorkOrderIDTextEdit;
            this.ItemForWorkOrderID.CustomizationFormText = "Work Order ID:";
            this.ItemForWorkOrderID.Location = new System.Drawing.Point(0, 24);
            this.ItemForWorkOrderID.Name = "ItemForWorkOrderID";
            this.ItemForWorkOrderID.Size = new System.Drawing.Size(528, 24);
            this.ItemForWorkOrderID.Text = "Work Order ID:";
            this.ItemForWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDTextEdit;
            this.ItemForSubContractorID.CustomizationFormText = "SubContractor ID:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 96);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(1051, 24);
            this.ItemForSubContractorID.Text = "SubContractor ID:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(1075, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForDateSubContractorSent
            // 
            this.ItemForDateSubContractorSent.Control = this.DateSubContractorSentDateEdit;
            this.ItemForDateSubContractorSent.CustomizationFormText = "Issue Date:";
            this.ItemForDateSubContractorSent.Location = new System.Drawing.Point(0, 0);
            this.ItemForDateSubContractorSent.Name = "ItemForDateSubContractorSent";
            this.ItemForDateSubContractorSent.Size = new System.Drawing.Size(1051, 24);
            this.ItemForDateSubContractorSent.Text = "Issue Date:";
            this.ItemForDateSubContractorSent.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForTotalCost
            // 
            this.ItemForTotalCost.Control = this.TotalCostTextEdit;
            this.ItemForTotalCost.CustomizationFormText = "Total Cost:";
            this.ItemForTotalCost.Location = new System.Drawing.Point(0, 0);
            this.ItemForTotalCost.Name = "ItemForTotalCost";
            this.ItemForTotalCost.Size = new System.Drawing.Size(1051, 24);
            this.ItemForTotalCost.Text = "Total Cost:";
            this.ItemForTotalCost.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForDateCreated
            // 
            this.ItemForDateCreated.Control = this.DateCreatedDateEdit;
            this.ItemForDateCreated.CustomizationFormText = "Date Created:";
            this.ItemForDateCreated.Location = new System.Drawing.Point(0, 0);
            this.ItemForDateCreated.Name = "ItemForDateCreated";
            this.ItemForDateCreated.Size = new System.Drawing.Size(1051, 24);
            this.ItemForDateCreated.Text = "Date Created:";
            this.ItemForDateCreated.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemforCreatedBy
            // 
            this.ItemforCreatedBy.Control = this.CreatedByTextEdit;
            this.ItemforCreatedBy.CustomizationFormText = "Created By:";
            this.ItemforCreatedBy.Location = new System.Drawing.Point(0, 0);
            this.ItemforCreatedBy.Name = "ItemforCreatedBy";
            this.ItemforCreatedBy.Size = new System.Drawing.Size(1051, 24);
            this.ItemforCreatedBy.Text = "Created By:";
            this.ItemforCreatedBy.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDTextEdit;
            this.ItemForStatusID.CustomizationFormText = "Status ID:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 58);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(1022, 24);
            this.ItemForStatusID.Text = "Status ID:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSurveyDate
            // 
            this.ItemForSurveyDate.Control = this.SurveyDateTextEdit;
            this.ItemForSurveyDate.Location = new System.Drawing.Point(0, 75);
            this.ItemForSurveyDate.Name = "ItemForSurveyDate";
            this.ItemForSurveyDate.Size = new System.Drawing.Size(1016, 24);
            this.ItemForSurveyDate.Text = "Survey Date:";
            this.ItemForSurveyDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1090, 666);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.simpleSeparator6,
            this.splitterItem1,
            this.layoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1070, 311);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Work Order Header";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup3});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 28);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1070, 277);
            this.layoutControlGroup13.Text = "Work Order Header";
            // 
            // tabbedControlGroup3
            // 
            this.tabbedControlGroup3.CustomizationFormText = "tabbedControlGroup3";
            this.tabbedControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup3.Name = "tabbedControlGroup3";
            this.tabbedControlGroup3.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup3.SelectedTabPageIndex = 0;
            this.tabbedControlGroup3.Size = new System.Drawing.Size(1046, 231);
            this.tabbedControlGroup3.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPaddedWorkOrderID,
            this.ItemForReferenceNumber,
            this.ItemForDateCompleted,
            this.ItemForWorkOrderPDF,
            this.emptySpaceItem1,
            this.layoutControlGroup9});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1022, 183);
            this.layoutControlGroup4.Text = "Details";
            // 
            // ItemForPaddedWorkOrderID
            // 
            this.ItemForPaddedWorkOrderID.Control = this.PaddedWorkOrderIDTextEdit;
            this.ItemForPaddedWorkOrderID.CustomizationFormText = "Work Order #:";
            this.ItemForPaddedWorkOrderID.Location = new System.Drawing.Point(0, 0);
            this.ItemForPaddedWorkOrderID.Name = "ItemForPaddedWorkOrderID";
            this.ItemForPaddedWorkOrderID.Size = new System.Drawing.Size(297, 24);
            this.ItemForPaddedWorkOrderID.Text = "Work Order #:";
            this.ItemForPaddedWorkOrderID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.Control = this.ReferenceNumberButtonEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(297, 0);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(725, 24);
            this.ItemForReferenceNumber.Text = "Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForDateCompleted
            // 
            this.ItemForDateCompleted.Control = this.DateCompletedDateEdit;
            this.ItemForDateCompleted.CustomizationFormText = "Completed Date:";
            this.ItemForDateCompleted.Location = new System.Drawing.Point(0, 24);
            this.ItemForDateCompleted.Name = "ItemForDateCompleted";
            this.ItemForDateCompleted.Size = new System.Drawing.Size(297, 24);
            this.ItemForDateCompleted.Text = "Completed Date:";
            this.ItemForDateCompleted.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForWorkOrderPDF
            // 
            this.ItemForWorkOrderPDF.Control = this.WorkOrderPDFHyperLinkEdit;
            this.ItemForWorkOrderPDF.CustomizationFormText = "Work Order PDF:";
            this.ItemForWorkOrderPDF.Location = new System.Drawing.Point(297, 24);
            this.ItemForWorkOrderPDF.Name = "ItemForWorkOrderPDF";
            this.ItemForWorkOrderPDF.Size = new System.Drawing.Size(725, 24);
            this.ItemForWorkOrderPDF.Text = "Work Order PDF:";
            this.ItemForWorkOrderPDF.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1022, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Team(s) Assigned To  -  Click Record Team Members button to set Employees on Site" +
    "";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 58);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(1022, 125);
            this.layoutControlGroup9.Text = "Team(s) Assigned  -  Click Record Team Members button to set Employees on Site";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "Contractor Grid:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1016, 97);
            this.layoutControlItem3.Text = "Contractor Grid:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup8.CaptionImageOptions.Image")));
            this.layoutControlGroup8.CustomizationFormText = "Remarks";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1022, 183);
            this.layoutControlGroup8.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(1022, 183);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(245, 26);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(391, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(679, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 26);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(1070, 2);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 305);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1070, 6);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnShowMap;
            this.layoutControlItem7.CustomizationFormText = "Show Map Button:";
            this.layoutControlItem7.Location = new System.Drawing.Point(245, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(146, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(146, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(146, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "Show Map Button:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.simpleSeparator1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 311);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1070, 335);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Work";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(1070, 333);
            this.layoutControlGroup12.Text = "Linked Records";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1046, 287);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup6,
            this.layoutControlGroup10});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Work";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlGroup5.Text = "Work";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridSplitContainer1;
            this.layoutControlItem1.CustomizationFormText = "Work Grid:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlItem1.Text = "Work Grid:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Work Maps";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlGroup7.Text = "Work Maps";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl3;
            this.layoutControlItem4.CustomizationFormText = "Work Maps Grid:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlItem4.Text = "Work Maps Grid:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Linked Pictures";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlGroup6.Text = "Linked Pictures";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.gridControl4;
            this.layoutControlItem5.CustomizationFormText = "Linked Pictures Grid:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlItem5.Text = "Linked Pictures Grid:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Linked Permission Documents";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlGroup10.Text = "Linked Permission Documents";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gridControl11;
            this.layoutControlItem6.CustomizationFormText = "Permission Documents Grid:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(1022, 242);
            this.layoutControlItem6.Text = "Permission Documents Grid:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1070, 2);
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07247_UT_Work_Order_EditTableAdapter
            // 
            this.sp07247_UT_Work_Order_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter
            // 
            this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter
            // 
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter
            // 
            this.sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter
            // 
            this.sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWorkOrder;
            this.xtraTabControl1.Size = new System.Drawing.Size(1095, 692);
            this.xtraTabControl1.TabIndex = 13;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWorkOrder,
            this.xtraTabPageRPSheet,
            this.xtraTabPageRiskAssessment});
            // 
            // xtraTabPageWorkOrder
            // 
            this.xtraTabPageWorkOrder.Controls.Add(this.dataLayoutControl1);
            this.xtraTabPageWorkOrder.Name = "xtraTabPageWorkOrder";
            this.xtraTabPageWorkOrder.Size = new System.Drawing.Size(1090, 666);
            this.xtraTabPageWorkOrder.Text = "Work Order";
            // 
            // xtraTabPageRPSheet
            // 
            this.xtraTabPageRPSheet.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageRPSheet.Name = "xtraTabPageRPSheet";
            this.xtraTabPageRPSheet.Size = new System.Drawing.Size(1090, 666);
            this.xtraTabPageRPSheet.Text = "RP Sheet";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl5);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1090, 666);
            this.splitContainerControl1.SplitterPosition = 261;
            this.splitContainerControl1.TabIndex = 42;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Linked Question", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Create Linked Questions  [Only use if Linked Questions are Missing]", "create")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit3,
            this.repositoryItemSpinEdit2DPMetres,
            this.repositoryItemSpinEdit0DP,
            this.repositoryItemButtonEditStart2,
            this.repositoryItemButtonEditComplete2,
            this.repositoryItemCheckEditDisabled,
            this.repositoryItemTextEditDateTimeDisabled,
            this.repositoryItemTextEdit2DPMetresDisabled,
            this.repositoryItemTextEdit0DPDisabled,
            this.repositoryItemDateEdit1});
            this.gridControl5.Size = new System.Drawing.Size(1090, 261);
            this.gridControl5.TabIndex = 41;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource
            // 
            this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource.DataMember = "sp07304_UT_Team_Work_Order_Site_Completion_Sheets_List";
            this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompletionSheetID,
            this.colSurveyedPoleID,
            this.colCompletedByID,
            this.colDateTimeCompleted,
            this.colFinalClearanceDistance,
            this.colRevisitDate,
            this.colSiteCompleteFitToBill,
            this.colTreesFelledCount,
            this.colStumpToGrindCount,
            this.colStumpsTreatedCount,
            this.colEcoPlugsAppliedCount,
            this.colRemarks3,
            this.colFurtherWorkRequired,
            this.colFurtherWorkRemarks,
            this.colGUID2,
            this.colStatusID,
            this.colCompletedByTeamName,
            this.colClientName,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colPoleNumber,
            this.colStatusDescription,
            this.colCompletedActions,
            this.colTotalActions,
            this.colJobsDoneStatus,
            this.gridColumnButtonStart2,
            this.gridColumnButtonComplete2,
            this.colClientID1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.ViewCaption = "Team(s) Assigned To";
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView5_FocusedRowChanged);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseDown);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            this.gridView5.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView5_ValidatingEditor);
            // 
            // colCompletionSheetID
            // 
            this.colCompletionSheetID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletionSheetID.AppearanceCell.Options.UseForeColor = true;
            this.colCompletionSheetID.Caption = "RP Sheet ID";
            this.colCompletionSheetID.FieldName = "CompletionSheetID";
            this.colCompletionSheetID.Name = "colCompletionSheetID";
            this.colCompletionSheetID.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetID.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetID.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetID.Width = 79;
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colSurveyedPoleID.AppearanceCell.Options.UseForeColor = true;
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colCompletedByID
            // 
            this.colCompletedByID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletedByID.AppearanceCell.Options.UseForeColor = true;
            this.colCompletedByID.Caption = "Completed By ID";
            this.colCompletedByID.FieldName = "CompletedByID";
            this.colCompletedByID.Name = "colCompletedByID";
            this.colCompletedByID.OptionsColumn.AllowEdit = false;
            this.colCompletedByID.OptionsColumn.AllowFocus = false;
            this.colCompletedByID.OptionsColumn.ReadOnly = true;
            this.colCompletedByID.Width = 101;
            // 
            // colDateTimeCompleted
            // 
            this.colDateTimeCompleted.Caption = "Date Completed";
            this.colDateTimeCompleted.ColumnEdit = this.repositoryItemDateEdit1;
            this.colDateTimeCompleted.FieldName = "DateTimeCompleted";
            this.colDateTimeCompleted.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeCompleted.Name = "colDateTimeCompleted";
            this.colDateTimeCompleted.OptionsColumn.AllowEdit = false;
            this.colDateTimeCompleted.OptionsColumn.AllowFocus = false;
            this.colDateTimeCompleted.OptionsColumn.ReadOnly = true;
            this.colDateTimeCompleted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeCompleted.Visible = true;
            this.colDateTimeCompleted.VisibleIndex = 5;
            this.colDateTimeCompleted.Width = 98;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colFinalClearanceDistance
            // 
            this.colFinalClearanceDistance.Caption = "Final Clearance Distance";
            this.colFinalClearanceDistance.ColumnEdit = this.repositoryItemSpinEdit2DPMetres;
            this.colFinalClearanceDistance.FieldName = "FinalClearanceDistance";
            this.colFinalClearanceDistance.Name = "colFinalClearanceDistance";
            this.colFinalClearanceDistance.Visible = true;
            this.colFinalClearanceDistance.VisibleIndex = 6;
            this.colFinalClearanceDistance.Width = 138;
            // 
            // repositoryItemSpinEdit2DPMetres
            // 
            this.repositoryItemSpinEdit2DPMetres.AutoHeight = false;
            this.repositoryItemSpinEdit2DPMetres.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit2DPMetres.Mask.EditMask = "######0.00 Metres";
            this.repositoryItemSpinEdit2DPMetres.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit2DPMetres.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.repositoryItemSpinEdit2DPMetres.Name = "repositoryItemSpinEdit2DPMetres";
            this.repositoryItemSpinEdit2DPMetres.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit2DPMetres_EditValueChanged);
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemDateEdit1;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 7;
            this.colRevisitDate.Width = 96;
            // 
            // colSiteCompleteFitToBill
            // 
            this.colSiteCompleteFitToBill.Caption = "Site Fit To Bill";
            this.colSiteCompleteFitToBill.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colSiteCompleteFitToBill.FieldName = "SiteCompleteFitToBill";
            this.colSiteCompleteFitToBill.Name = "colSiteCompleteFitToBill";
            this.colSiteCompleteFitToBill.Visible = true;
            this.colSiteCompleteFitToBill.VisibleIndex = 8;
            this.colSiteCompleteFitToBill.Width = 84;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            this.repositoryItemCheckEdit3.EditValueChanged += new System.EventHandler(this.repositoryItemCheckEdit3_EditValueChanged);
            // 
            // colTreesFelledCount
            // 
            this.colTreesFelledCount.Caption = "Trees Felled";
            this.colTreesFelledCount.ColumnEdit = this.repositoryItemSpinEdit0DP;
            this.colTreesFelledCount.FieldName = "TreesFelledCount";
            this.colTreesFelledCount.Name = "colTreesFelledCount";
            this.colTreesFelledCount.Visible = true;
            this.colTreesFelledCount.VisibleIndex = 9;
            this.colTreesFelledCount.Width = 79;
            // 
            // repositoryItemSpinEdit0DP
            // 
            this.repositoryItemSpinEdit0DP.AutoHeight = false;
            this.repositoryItemSpinEdit0DP.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemSpinEdit0DP.IsFloatValue = false;
            this.repositoryItemSpinEdit0DP.Mask.EditMask = "n0";
            this.repositoryItemSpinEdit0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemSpinEdit0DP.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.repositoryItemSpinEdit0DP.Name = "repositoryItemSpinEdit0DP";
            this.repositoryItemSpinEdit0DP.EditValueChanged += new System.EventHandler(this.repositoryItemSpinEdit0DP_EditValueChanged);
            // 
            // colStumpToGrindCount
            // 
            this.colStumpToGrindCount.Caption = "Stumps Ground";
            this.colStumpToGrindCount.ColumnEdit = this.repositoryItemSpinEdit0DP;
            this.colStumpToGrindCount.FieldName = "StumpToGrindCount";
            this.colStumpToGrindCount.Name = "colStumpToGrindCount";
            this.colStumpToGrindCount.Visible = true;
            this.colStumpToGrindCount.VisibleIndex = 10;
            this.colStumpToGrindCount.Width = 94;
            // 
            // colStumpsTreatedCount
            // 
            this.colStumpsTreatedCount.Caption = "Stumps Treated";
            this.colStumpsTreatedCount.ColumnEdit = this.repositoryItemSpinEdit0DP;
            this.colStumpsTreatedCount.FieldName = "StumpsTreatedCount";
            this.colStumpsTreatedCount.Name = "colStumpsTreatedCount";
            this.colStumpsTreatedCount.Visible = true;
            this.colStumpsTreatedCount.VisibleIndex = 11;
            this.colStumpsTreatedCount.Width = 97;
            // 
            // colEcoPlugsAppliedCount
            // 
            this.colEcoPlugsAppliedCount.Caption = "Ecoplugs Applied";
            this.colEcoPlugsAppliedCount.ColumnEdit = this.repositoryItemSpinEdit0DP;
            this.colEcoPlugsAppliedCount.FieldName = "EcoPlugsAppliedCount";
            this.colEcoPlugsAppliedCount.Name = "colEcoPlugsAppliedCount";
            this.colEcoPlugsAppliedCount.Visible = true;
            this.colEcoPlugsAppliedCount.VisibleIndex = 12;
            this.colEcoPlugsAppliedCount.Width = 101;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            this.repositoryItemMemoExEdit5.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit5_EditValueChanged);
            // 
            // colFurtherWorkRequired
            // 
            this.colFurtherWorkRequired.Caption = "Further Work Required";
            this.colFurtherWorkRequired.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colFurtherWorkRequired.FieldName = "FurtherWorkRequired";
            this.colFurtherWorkRequired.Name = "colFurtherWorkRequired";
            this.colFurtherWorkRequired.Visible = true;
            this.colFurtherWorkRequired.VisibleIndex = 14;
            this.colFurtherWorkRequired.Width = 131;
            // 
            // colFurtherWorkRemarks
            // 
            this.colFurtherWorkRemarks.Caption = "Further Work Remarks";
            this.colFurtherWorkRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colFurtherWorkRemarks.FieldName = "FurtherWorkRemarks";
            this.colFurtherWorkRemarks.Name = "colFurtherWorkRemarks";
            this.colFurtherWorkRemarks.Visible = true;
            this.colFurtherWorkRemarks.VisibleIndex = 15;
            this.colFurtherWorkRemarks.Width = 129;
            // 
            // colGUID2
            // 
            this.colGUID2.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colGUID2.AppearanceCell.Options.UseForeColor = true;
            this.colGUID2.Caption = "GUID";
            this.colGUID2.FieldName = "GUID";
            this.colGUID2.Name = "colGUID2";
            this.colGUID2.OptionsColumn.AllowEdit = false;
            this.colGUID2.OptionsColumn.AllowFocus = false;
            this.colGUID2.OptionsColumn.ReadOnly = true;
            // 
            // colStatusID
            // 
            this.colStatusID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colStatusID.AppearanceCell.Options.UseForeColor = true;
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCompletedByTeamName
            // 
            this.colCompletedByTeamName.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletedByTeamName.AppearanceCell.Options.UseForeColor = true;
            this.colCompletedByTeamName.Caption = "Completed By";
            this.colCompletedByTeamName.FieldName = "CompletedByTeamName";
            this.colCompletedByTeamName.Name = "colCompletedByTeamName";
            this.colCompletedByTeamName.OptionsColumn.AllowEdit = false;
            this.colCompletedByTeamName.OptionsColumn.AllowFocus = false;
            this.colCompletedByTeamName.OptionsColumn.ReadOnly = true;
            this.colCompletedByTeamName.Visible = true;
            this.colCompletedByTeamName.VisibleIndex = 16;
            this.colCompletedByTeamName.Width = 128;
            // 
            // colClientName
            // 
            this.colClientName.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colClientName.AppearanceCell.Options.UseForeColor = true;
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 133;
            // 
            // colCircuitName
            // 
            this.colCircuitName.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCircuitName.AppearanceCell.Options.UseForeColor = true;
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Width = 154;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCircuitNumber.AppearanceCell.Options.UseForeColor = true;
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 1;
            this.colCircuitNumber.Width = 104;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colPoleNumber.AppearanceCell.Options.UseForeColor = true;
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 2;
            this.colPoleNumber.Width = 94;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colStatusDescription.AppearanceCell.Options.UseForeColor = true;
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 3;
            this.colStatusDescription.Width = 80;
            // 
            // colCompletedActions
            // 
            this.colCompletedActions.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletedActions.AppearanceCell.Options.UseForeColor = true;
            this.colCompletedActions.Caption = "Completed Actions";
            this.colCompletedActions.FieldName = "CompletedActions";
            this.colCompletedActions.Name = "colCompletedActions";
            this.colCompletedActions.OptionsColumn.AllowEdit = false;
            this.colCompletedActions.OptionsColumn.AllowFocus = false;
            this.colCompletedActions.OptionsColumn.ReadOnly = true;
            this.colCompletedActions.Width = 110;
            // 
            // colTotalActions
            // 
            this.colTotalActions.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colTotalActions.AppearanceCell.Options.UseForeColor = true;
            this.colTotalActions.Caption = "Total Actions";
            this.colTotalActions.FieldName = "TotalActions";
            this.colTotalActions.Name = "colTotalActions";
            this.colTotalActions.OptionsColumn.AllowEdit = false;
            this.colTotalActions.OptionsColumn.AllowFocus = false;
            this.colTotalActions.OptionsColumn.ReadOnly = true;
            this.colTotalActions.Width = 83;
            // 
            // colJobsDoneStatus
            // 
            this.colJobsDoneStatus.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colJobsDoneStatus.AppearanceCell.Options.UseForeColor = true;
            this.colJobsDoneStatus.Caption = "Jobs Completed";
            this.colJobsDoneStatus.FieldName = "JobsDoneStatus";
            this.colJobsDoneStatus.Name = "colJobsDoneStatus";
            this.colJobsDoneStatus.OptionsColumn.AllowEdit = false;
            this.colJobsDoneStatus.OptionsColumn.AllowFocus = false;
            this.colJobsDoneStatus.OptionsColumn.ReadOnly = true;
            this.colJobsDoneStatus.Visible = true;
            this.colJobsDoneStatus.VisibleIndex = 4;
            this.colJobsDoneStatus.Width = 97;
            // 
            // gridColumnButtonStart2
            // 
            this.gridColumnButtonStart2.Caption = "Start";
            this.gridColumnButtonStart2.ColumnEdit = this.repositoryItemButtonEditStart2;
            this.gridColumnButtonStart2.FieldName = "gridColumnButtonStart2";
            this.gridColumnButtonStart2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumnButtonStart2.Name = "gridColumnButtonStart2";
            this.gridColumnButtonStart2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart2.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButtonStart2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart2.OptionsColumn.AllowMove = false;
            this.gridColumnButtonStart2.OptionsColumn.AllowShowHide = false;
            this.gridColumnButtonStart2.OptionsColumn.AllowSize = false;
            this.gridColumnButtonStart2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart2.OptionsColumn.FixedWidth = true;
            this.gridColumnButtonStart2.OptionsColumn.ReadOnly = true;
            this.gridColumnButtonStart2.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButtonStart2.OptionsFilter.AllowFilter = false;
            this.gridColumnButtonStart2.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonStart2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButtonStart2.Visible = true;
            this.gridColumnButtonStart2.VisibleIndex = 0;
            this.gridColumnButtonStart2.Width = 35;
            // 
            // repositoryItemButtonEditStart2
            // 
            this.repositoryItemButtonEditStart2.AutoHeight = false;
            superToolTip9.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem9.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image14")));
            toolTipTitleItem9.Appearance.Options.UseImage = true;
            toolTipTitleItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image15")));
            toolTipTitleItem9.Text = "Start Button - Information";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = resources.GetString("toolTipItem9.Text");
            superToolTip9.Items.Add(toolTipTitleItem9);
            superToolTip9.Items.Add(toolTipItem9);
            this.repositoryItemButtonEditStart2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Start", -1, true, true, false, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "start", superToolTip9, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditStart2.Name = "repositoryItemButtonEditStart2";
            this.repositoryItemButtonEditStart2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditStart2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditStart2_ButtonClick);
            // 
            // gridColumnButtonComplete2
            // 
            this.gridColumnButtonComplete2.Caption = "Completed";
            this.gridColumnButtonComplete2.ColumnEdit = this.repositoryItemButtonEditComplete2;
            this.gridColumnButtonComplete2.FieldName = "gridColumnButtonComplete2";
            this.gridColumnButtonComplete2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumnButtonComplete2.Name = "gridColumnButtonComplete2";
            this.gridColumnButtonComplete2.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete2.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumnButtonComplete2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete2.OptionsColumn.AllowMove = false;
            this.gridColumnButtonComplete2.OptionsColumn.AllowShowHide = false;
            this.gridColumnButtonComplete2.OptionsColumn.AllowSize = false;
            this.gridColumnButtonComplete2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete2.OptionsColumn.FixedWidth = true;
            this.gridColumnButtonComplete2.OptionsColumn.ReadOnly = true;
            this.gridColumnButtonComplete2.OptionsFilter.AllowAutoFilter = false;
            this.gridColumnButtonComplete2.OptionsFilter.AllowFilter = false;
            this.gridColumnButtonComplete2.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumnButtonComplete2.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumnButtonComplete2.Visible = true;
            this.gridColumnButtonComplete2.VisibleIndex = 17;
            this.gridColumnButtonComplete2.Width = 62;
            // 
            // repositoryItemButtonEditComplete2
            // 
            this.repositoryItemButtonEditComplete2.AutoHeight = false;
            toolTipTitleItem10.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image16")));
            toolTipTitleItem10.Appearance.Options.UseImage = true;
            toolTipTitleItem10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image17")));
            toolTipTitleItem10.Text = "Completed Button - information";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = "Click me to set the RP Sheet as Completed.";
            superToolTip10.Items.Add(toolTipTitleItem10);
            superToolTip10.Items.Add(toolTipItem10);
            this.repositoryItemButtonEditComplete2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Completed", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "", null, superToolTip10, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditComplete2.Name = "repositoryItemButtonEditComplete2";
            this.repositoryItemButtonEditComplete2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditComplete2.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditComplete2_ButtonClick);
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEditDisabled
            // 
            this.repositoryItemCheckEditDisabled.AutoHeight = false;
            this.repositoryItemCheckEditDisabled.Caption = "Check";
            this.repositoryItemCheckEditDisabled.Name = "repositoryItemCheckEditDisabled";
            this.repositoryItemCheckEditDisabled.ReadOnly = true;
            this.repositoryItemCheckEditDisabled.ValueChecked = 1;
            this.repositoryItemCheckEditDisabled.ValueUnchecked = 0;
            // 
            // repositoryItemTextEditDateTimeDisabled
            // 
            this.repositoryItemTextEditDateTimeDisabled.AutoHeight = false;
            this.repositoryItemTextEditDateTimeDisabled.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTimeDisabled.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTimeDisabled.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTimeDisabled.Name = "repositoryItemTextEditDateTimeDisabled";
            this.repositoryItemTextEditDateTimeDisabled.ReadOnly = true;
            // 
            // repositoryItemTextEdit2DPMetresDisabled
            // 
            this.repositoryItemTextEdit2DPMetresDisabled.AutoHeight = false;
            this.repositoryItemTextEdit2DPMetresDisabled.Mask.EditMask = "#####0.00 Metres";
            this.repositoryItemTextEdit2DPMetresDisabled.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DPMetresDisabled.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DPMetresDisabled.Name = "repositoryItemTextEdit2DPMetresDisabled";
            this.repositoryItemTextEdit2DPMetresDisabled.ReadOnly = true;
            // 
            // repositoryItemTextEdit0DPDisabled
            // 
            this.repositoryItemTextEdit0DPDisabled.AutoHeight = false;
            this.repositoryItemTextEdit0DPDisabled.Mask.EditMask = "n0";
            this.repositoryItemTextEdit0DPDisabled.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit0DPDisabled.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit0DPDisabled.Name = "repositoryItemTextEdit0DPDisabled";
            this.repositoryItemTextEdit0DPDisabled.ReadOnly = true;
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl2.Size = new System.Drawing.Size(1090, 399);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl6);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1085, 373);
            this.xtraTabPage1.Text = "Linked Questions";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemGridLookUpEditAnswer,
            this.repositoryItemGridLookUpEditDiabled,
            this.repositoryItemMemoExEditDisabled});
            this.gridControl6.Size = new System.Drawing.Size(1085, 373);
            this.gridControl6.TabIndex = 42;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07307UTTeamWorkOrderCompletionSheetEditBindingSource
            // 
            this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource.DataMember = "sp07307_UT_Team_Work_Order_Completion_Sheet_Edit";
            this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompletionSheetQuestionID,
            this.colCompletionSheetID1,
            this.colQuestionDescription,
            this.colQuestionNotes,
            this.colQuestionOrder,
            this.colTeamAnswer,
            this.colTeamRemarks1,
            this.colAuditAnswer,
            this.colAuditRemarks,
            this.colAuditedByID,
            this.colMasterQuestionID,
            this.colAuditedByName});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsCustomization.AllowFilter = false;
            this.gridView6.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowFilterEditor = false;
            this.gridView6.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView6.OptionsFilter.AllowMRUFilterList = false;
            this.gridView6.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuestionOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView6.ViewCaption = "Team(s) Assigned To";
            this.gridView6.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView6_CustomDrawCell);
            this.gridView6.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView6_CustomRowCellEdit);
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView6_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView6_ShowingEditor);
            this.gridView6.HiddenEditor += new System.EventHandler(this.gridView6_HiddenEditor);
            this.gridView6.ShownEditor += new System.EventHandler(this.gridView6_ShownEditor);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colCompletionSheetQuestionID
            // 
            this.colCompletionSheetQuestionID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletionSheetQuestionID.AppearanceCell.Options.UseForeColor = true;
            this.colCompletionSheetQuestionID.Caption = "Question ID";
            this.colCompletionSheetQuestionID.FieldName = "CompletionSheetQuestionID";
            this.colCompletionSheetQuestionID.Name = "colCompletionSheetQuestionID";
            this.colCompletionSheetQuestionID.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetQuestionID.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetQuestionID.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetQuestionID.OptionsFilter.AllowAutoFilter = false;
            this.colCompletionSheetQuestionID.OptionsFilter.AllowFilter = false;
            this.colCompletionSheetQuestionID.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colCompletionSheetID1
            // 
            this.colCompletionSheetID1.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colCompletionSheetID1.AppearanceCell.Options.UseForeColor = true;
            this.colCompletionSheetID1.Caption = "Completion Sheet ID";
            this.colCompletionSheetID1.FieldName = "CompletionSheetID";
            this.colCompletionSheetID1.Name = "colCompletionSheetID1";
            this.colCompletionSheetID1.OptionsColumn.AllowEdit = false;
            this.colCompletionSheetID1.OptionsColumn.AllowFocus = false;
            this.colCompletionSheetID1.OptionsColumn.ReadOnly = true;
            this.colCompletionSheetID1.OptionsFilter.AllowAutoFilter = false;
            this.colCompletionSheetID1.OptionsFilter.AllowFilter = false;
            this.colCompletionSheetID1.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colCompletionSheetID1.Width = 109;
            // 
            // colQuestionDescription
            // 
            this.colQuestionDescription.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colQuestionDescription.AppearanceCell.Options.UseForeColor = true;
            this.colQuestionDescription.Caption = "Question";
            this.colQuestionDescription.FieldName = "QuestionDescription";
            this.colQuestionDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionDescription.Name = "colQuestionDescription";
            this.colQuestionDescription.OptionsColumn.AllowEdit = false;
            this.colQuestionDescription.OptionsColumn.AllowFocus = false;
            this.colQuestionDescription.OptionsColumn.ReadOnly = true;
            this.colQuestionDescription.OptionsFilter.AllowAutoFilter = false;
            this.colQuestionDescription.OptionsFilter.AllowFilter = false;
            this.colQuestionDescription.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colQuestionDescription.Visible = true;
            this.colQuestionDescription.VisibleIndex = 1;
            this.colQuestionDescription.Width = 472;
            // 
            // colQuestionNotes
            // 
            this.colQuestionNotes.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colQuestionNotes.AppearanceCell.Options.UseForeColor = true;
            this.colQuestionNotes.Caption = "Notes";
            this.colQuestionNotes.FieldName = "QuestionNotes";
            this.colQuestionNotes.Name = "colQuestionNotes";
            this.colQuestionNotes.OptionsColumn.AllowEdit = false;
            this.colQuestionNotes.OptionsColumn.AllowFocus = false;
            this.colQuestionNotes.OptionsColumn.ReadOnly = true;
            this.colQuestionNotes.OptionsFilter.AllowAutoFilter = false;
            this.colQuestionNotes.OptionsFilter.AllowFilter = false;
            this.colQuestionNotes.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colQuestionNotes.Visible = true;
            this.colQuestionNotes.VisibleIndex = 2;
            this.colQuestionNotes.Width = 144;
            // 
            // colQuestionOrder
            // 
            this.colQuestionOrder.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colQuestionOrder.AppearanceCell.Options.UseForeColor = true;
            this.colQuestionOrder.Caption = "#";
            this.colQuestionOrder.FieldName = "QuestionOrder";
            this.colQuestionOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionOrder.Name = "colQuestionOrder";
            this.colQuestionOrder.OptionsColumn.AllowEdit = false;
            this.colQuestionOrder.OptionsColumn.AllowFocus = false;
            this.colQuestionOrder.OptionsColumn.ReadOnly = true;
            this.colQuestionOrder.OptionsFilter.AllowAutoFilter = false;
            this.colQuestionOrder.OptionsFilter.AllowFilter = false;
            this.colQuestionOrder.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colQuestionOrder.Visible = true;
            this.colQuestionOrder.VisibleIndex = 0;
            this.colQuestionOrder.Width = 32;
            // 
            // colTeamAnswer
            // 
            this.colTeamAnswer.Caption = "Team Answer";
            this.colTeamAnswer.ColumnEdit = this.repositoryItemGridLookUpEditAnswer;
            this.colTeamAnswer.FieldName = "TeamAnswer";
            this.colTeamAnswer.Name = "colTeamAnswer";
            this.colTeamAnswer.OptionsFilter.AllowAutoFilter = false;
            this.colTeamAnswer.OptionsFilter.AllowFilter = false;
            this.colTeamAnswer.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colTeamAnswer.Visible = true;
            this.colTeamAnswer.VisibleIndex = 3;
            this.colTeamAnswer.Width = 76;
            // 
            // repositoryItemGridLookUpEditAnswer
            // 
            this.repositoryItemGridLookUpEditAnswer.AutoHeight = false;
            this.repositoryItemGridLookUpEditAnswer.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditAnswer.DataSource = this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource;
            this.repositoryItemGridLookUpEditAnswer.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditAnswer.Name = "repositoryItemGridLookUpEditAnswer";
            this.repositoryItemGridLookUpEditAnswer.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.repositoryItemGridLookUpEditAnswer.ValueMember = "ID";
            this.repositoryItemGridLookUpEditAnswer.EditValueChanged += new System.EventHandler(this.repositoryItemGridLookUpEditAnswer_EditValueChanged);
            // 
            // sp07262UTRiskAssessmentQuestionAnswersListBindingSource
            // 
            this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource.DataMember = "sp07262_UT_Risk_Assessment_Question_Answers_List";
            this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder});
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Answer";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 147;
            // 
            // colID
            // 
            this.colID.Caption = "Answer ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // colTeamRemarks1
            // 
            this.colTeamRemarks1.Caption = "Team Remarks";
            this.colTeamRemarks1.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colTeamRemarks1.FieldName = "TeamRemarks";
            this.colTeamRemarks1.Name = "colTeamRemarks1";
            this.colTeamRemarks1.OptionsFilter.AllowAutoFilter = false;
            this.colTeamRemarks1.OptionsFilter.AllowFilter = false;
            this.colTeamRemarks1.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colTeamRemarks1.Visible = true;
            this.colTeamRemarks1.VisibleIndex = 4;
            this.colTeamRemarks1.Width = 148;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            this.repositoryItemMemoExEdit6.EditValueChanged += new System.EventHandler(this.repositoryItemMemoExEdit6_EditValueChanged_1);
            // 
            // colAuditAnswer
            // 
            this.colAuditAnswer.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colAuditAnswer.AppearanceCell.Options.UseForeColor = true;
            this.colAuditAnswer.Caption = "Audit Answer";
            this.colAuditAnswer.ColumnEdit = this.repositoryItemGridLookUpEditAnswer;
            this.colAuditAnswer.FieldName = "AuditAnswer";
            this.colAuditAnswer.Name = "colAuditAnswer";
            this.colAuditAnswer.OptionsColumn.AllowEdit = false;
            this.colAuditAnswer.OptionsColumn.AllowFocus = false;
            this.colAuditAnswer.OptionsColumn.ReadOnly = true;
            this.colAuditAnswer.OptionsFilter.AllowAutoFilter = false;
            this.colAuditAnswer.OptionsFilter.AllowFilter = false;
            this.colAuditAnswer.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colAuditAnswer.Visible = true;
            this.colAuditAnswer.VisibleIndex = 5;
            // 
            // colAuditRemarks
            // 
            this.colAuditRemarks.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colAuditRemarks.AppearanceCell.Options.UseForeColor = true;
            this.colAuditRemarks.Caption = "Audit Remarks";
            this.colAuditRemarks.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colAuditRemarks.FieldName = "AuditRemarks";
            this.colAuditRemarks.Name = "colAuditRemarks";
            this.colAuditRemarks.OptionsColumn.AllowEdit = false;
            this.colAuditRemarks.OptionsColumn.AllowFocus = false;
            this.colAuditRemarks.OptionsColumn.ReadOnly = true;
            this.colAuditRemarks.OptionsFilter.AllowAutoFilter = false;
            this.colAuditRemarks.OptionsFilter.AllowFilter = false;
            this.colAuditRemarks.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colAuditRemarks.Visible = true;
            this.colAuditRemarks.VisibleIndex = 6;
            this.colAuditRemarks.Width = 137;
            // 
            // colAuditedByID
            // 
            this.colAuditedByID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colAuditedByID.AppearanceCell.Options.UseForeColor = true;
            this.colAuditedByID.Caption = "Audited By ID";
            this.colAuditedByID.FieldName = "AuditedByID";
            this.colAuditedByID.Name = "colAuditedByID";
            this.colAuditedByID.OptionsColumn.AllowEdit = false;
            this.colAuditedByID.OptionsColumn.AllowFocus = false;
            this.colAuditedByID.OptionsColumn.ReadOnly = true;
            this.colAuditedByID.OptionsFilter.AllowAutoFilter = false;
            this.colAuditedByID.OptionsFilter.AllowFilter = false;
            this.colAuditedByID.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colAuditedByID.Width = 77;
            // 
            // colMasterQuestionID
            // 
            this.colMasterQuestionID.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colMasterQuestionID.AppearanceCell.Options.UseForeColor = true;
            this.colMasterQuestionID.Caption = "Master Question ID";
            this.colMasterQuestionID.FieldName = "MasterQuestionID";
            this.colMasterQuestionID.Name = "colMasterQuestionID";
            this.colMasterQuestionID.OptionsColumn.AllowEdit = false;
            this.colMasterQuestionID.OptionsColumn.AllowFocus = false;
            this.colMasterQuestionID.OptionsColumn.ReadOnly = true;
            this.colMasterQuestionID.OptionsFilter.AllowAutoFilter = false;
            this.colMasterQuestionID.OptionsFilter.AllowFilter = false;
            this.colMasterQuestionID.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colMasterQuestionID.Width = 104;
            // 
            // colAuditedByName
            // 
            this.colAuditedByName.AppearanceCell.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.colAuditedByName.AppearanceCell.Options.UseForeColor = true;
            this.colAuditedByName.Caption = "Audited By";
            this.colAuditedByName.FieldName = "AuditedByName";
            this.colAuditedByName.Name = "colAuditedByName";
            this.colAuditedByName.OptionsColumn.AllowEdit = false;
            this.colAuditedByName.OptionsColumn.AllowFocus = false;
            this.colAuditedByName.OptionsColumn.ReadOnly = true;
            this.colAuditedByName.OptionsFilter.AllowAutoFilter = false;
            this.colAuditedByName.OptionsFilter.AllowFilter = false;
            this.colAuditedByName.OptionsFilter.AllowFilterModeChanging = DevExpress.Utils.DefaultBoolean.False;
            this.colAuditedByName.Visible = true;
            this.colAuditedByName.VisibleIndex = 7;
            this.colAuditedByName.Width = 123;
            // 
            // repositoryItemGridLookUpEditDiabled
            // 
            this.repositoryItemGridLookUpEditDiabled.AutoHeight = false;
            this.repositoryItemGridLookUpEditDiabled.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditDiabled.DataSource = this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource;
            this.repositoryItemGridLookUpEditDiabled.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditDiabled.Name = "repositoryItemGridLookUpEditDiabled";
            this.repositoryItemGridLookUpEditDiabled.PopupView = this.gridView8;
            this.repositoryItemGridLookUpEditDiabled.ReadOnly = true;
            this.repositoryItemGridLookUpEditDiabled.ValueMember = "ID";
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView8.OptionsView.ShowIndicator = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Answer";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 147;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Answer ID";
            this.gridColumn6.FieldName = "ID";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Order";
            this.gridColumn7.FieldName = "RecordOrder";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEditDisabled
            // 
            this.repositoryItemMemoExEditDisabled.AutoHeight = false;
            this.repositoryItemMemoExEditDisabled.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditDisabled.Name = "repositoryItemMemoExEditDisabled";
            this.repositoryItemMemoExEditDisabled.ReadOnly = true;
            this.repositoryItemMemoExEditDisabled.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditDisabled.ShowIcon = false;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl7);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1085, 373);
            this.xtraTabPage2.Text = "Linked Work";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemCheckEdit5,
            this.repositoryItemButtonEdit4});
            this.gridControl7.Size = new System.Drawing.Size(1085, 373);
            this.gridControl7.TabIndex = 42;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.gridColumn40,
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn44,
            this.gridColumn45,
            this.gridColumn46});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn38, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.ViewCaption = "Team(s) Assigned To";
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Work Order Team ID";
            this.gridColumn33.FieldName = "WorkOrderTeamID";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Width = 120;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Work Order ID";
            this.gridColumn34.FieldName = "WorkOrderID";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Width = 91;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Sub-Contractor ID";
            this.gridColumn35.FieldName = "SubContractorID";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Width = 109;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Remarks";
            this.gridColumn36.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn36.FieldName = "Remarks";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 3;
            this.gridColumn36.Width = 116;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "GUID";
            this.gridColumn37.FieldName = "GUID";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Sub-Contractor Name";
            this.gridColumn38.FieldName = "SubContractorName";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 0;
            this.gridColumn38.Width = 288;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Internal";
            this.gridColumn39.ColumnEdit = this.repositoryItemCheckEdit5;
            this.gridColumn39.FieldName = "InternalContractor";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 61;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // gridColumn40
            // 
            this.gridColumn40.Caption = "Mobile Telephone";
            this.gridColumn40.FieldName = "MobileTel";
            this.gridColumn40.Name = "gridColumn40";
            this.gridColumn40.OptionsColumn.AllowEdit = false;
            this.gridColumn40.OptionsColumn.AllowFocus = false;
            this.gridColumn40.OptionsColumn.ReadOnly = true;
            this.gridColumn40.Visible = true;
            this.gridColumn40.VisibleIndex = 1;
            this.gridColumn40.Width = 117;
            // 
            // gridColumn41
            // 
            this.gridColumn41.Caption = "Telephone 1";
            this.gridColumn41.FieldName = "Telephone1";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.AllowEdit = false;
            this.gridColumn41.OptionsColumn.AllowFocus = false;
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 2;
            this.gridColumn41.Width = 97;
            // 
            // gridColumn42
            // 
            this.gridColumn42.Caption = "Telephone 2";
            this.gridColumn42.FieldName = "Telephone2";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.AllowEdit = false;
            this.gridColumn42.OptionsColumn.AllowFocus = false;
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Width = 98;
            // 
            // gridColumn43
            // 
            this.gridColumn43.Caption = "Postcode";
            this.gridColumn43.FieldName = "Postcode";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.AllowEdit = false;
            this.gridColumn43.OptionsColumn.AllowFocus = false;
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn44
            // 
            this.gridColumn44.Caption = "Work Order #";
            this.gridColumn44.FieldName = "PaddedWorkOrderID";
            this.gridColumn44.Name = "gridColumn44";
            this.gridColumn44.OptionsColumn.AllowEdit = false;
            this.gridColumn44.OptionsColumn.AllowFocus = false;
            this.gridColumn44.OptionsColumn.ReadOnly = true;
            this.gridColumn44.Width = 88;
            // 
            // gridColumn45
            // 
            this.gridColumn45.Caption = "Team Members on Site";
            this.gridColumn45.FieldName = "TeamMemberCount";
            this.gridColumn45.Name = "gridColumn45";
            this.gridColumn45.OptionsColumn.AllowEdit = false;
            this.gridColumn45.OptionsColumn.AllowFocus = false;
            this.gridColumn45.OptionsColumn.ReadOnly = true;
            this.gridColumn45.Visible = true;
            this.gridColumn45.VisibleIndex = 4;
            this.gridColumn45.Width = 129;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Team Members Present";
            this.gridColumn46.ColumnEdit = this.repositoryItemButtonEdit4;
            this.gridColumn46.FieldName = "gridColumnButton";
            this.gridColumn46.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn46.OptionsColumn.AllowIncrementalSearch = false;
            this.gridColumn46.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn46.OptionsColumn.AllowMove = false;
            this.gridColumn46.OptionsColumn.AllowShowHide = false;
            this.gridColumn46.OptionsColumn.AllowSize = false;
            this.gridColumn46.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn46.OptionsColumn.FixedWidth = true;
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.OptionsFilter.AllowAutoFilter = false;
            this.gridColumn46.OptionsFilter.AllowFilter = false;
            this.gridColumn46.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 5;
            this.gridColumn46.Width = 133;
            // 
            // repositoryItemButtonEdit4
            // 
            this.repositoryItemButtonEdit4.AutoHeight = false;
            toolTipTitleItem11.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image18")));
            toolTipTitleItem11.Appearance.Options.UseImage = true;
            toolTipTitleItem11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image19")));
            toolTipTitleItem11.Text = "Record Team Members - Information";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = "Click me to record Team Members on Site.";
            superToolTip11.Items.Add(toolTipTitleItem11);
            superToolTip11.Items.Add(toolTipItem11);
            this.repositoryItemButtonEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Record Team Members", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "", "RecordTeamMembers", superToolTip11, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit4.Name = "repositoryItemButtonEdit4";
            this.repositoryItemButtonEdit4.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // xtraTabPageRiskAssessment
            // 
            this.xtraTabPageRiskAssessment.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageRiskAssessment.Name = "xtraTabPageRiskAssessment";
            this.xtraTabPageRiskAssessment.Size = new System.Drawing.Size(1090, 666);
            this.xtraTabPageRiskAssessment.Text = "Risk Assessments";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl9);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Locations on Work Order  [Select one or more to see Risk Assessments]";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl10);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Linked Risk Assessments";
            this.splitContainerControl2.Size = new System.Drawing.Size(1090, 666);
            this.splitContainerControl2.SplitterPosition = 242;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemCheckEdit4,
            this.repositoryItemButtonEdit2});
            this.gridControl9.Size = new System.Drawing.Size(1086, 218);
            this.gridControl9.TabIndex = 41;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp07309UTTeamWorkOrderRiskLocationsListBindingSource
            // 
            this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource.DataMember = "sp07309_UT_Team_Work_Order_Risk_Locations_List";
            this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID2,
            this.colClientName1,
            this.colCircuitName2,
            this.colCircuitNumber1,
            this.colPoleNumber2,
            this.colVoltage,
            this.colRegionName,
            this.colRegionNumber,
            this.colSubAreaName,
            this.colSubAreaNumber,
            this.colPrimaryName,
            this.colPrimaryNumber,
            this.colFeederName,
            this.colFeederNumber});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView9.ViewCaption = "Team(s) Assigned To";
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView9_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // colSurveyedPoleID2
            // 
            this.colSurveyedPoleID2.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID2.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID2.Name = "colSurveyedPoleID2";
            this.colSurveyedPoleID2.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID2.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID2.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID2.Width = 104;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 0;
            this.colClientName1.Width = 175;
            // 
            // colCircuitName2
            // 
            this.colCircuitName2.Caption = "Circuit Name";
            this.colCircuitName2.FieldName = "CircuitName";
            this.colCircuitName2.Name = "colCircuitName2";
            this.colCircuitName2.OptionsColumn.AllowEdit = false;
            this.colCircuitName2.OptionsColumn.AllowFocus = false;
            this.colCircuitName2.OptionsColumn.ReadOnly = true;
            this.colCircuitName2.Width = 175;
            // 
            // colCircuitNumber1
            // 
            this.colCircuitNumber1.Caption = "Circuit Number";
            this.colCircuitNumber1.FieldName = "CircuitNumber";
            this.colCircuitNumber1.Name = "colCircuitNumber1";
            this.colCircuitNumber1.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber1.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber1.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber1.Visible = true;
            this.colCircuitNumber1.VisibleIndex = 2;
            this.colCircuitNumber1.Width = 175;
            // 
            // colPoleNumber2
            // 
            this.colPoleNumber2.Caption = "Pole Number";
            this.colPoleNumber2.FieldName = "PoleNumber";
            this.colPoleNumber2.Name = "colPoleNumber2";
            this.colPoleNumber2.OptionsColumn.AllowEdit = false;
            this.colPoleNumber2.OptionsColumn.AllowFocus = false;
            this.colPoleNumber2.OptionsColumn.ReadOnly = true;
            this.colPoleNumber2.Visible = true;
            this.colPoleNumber2.VisibleIndex = 3;
            this.colPoleNumber2.Width = 175;
            // 
            // colVoltage
            // 
            this.colVoltage.Caption = "Voltage";
            this.colVoltage.FieldName = "Voltage";
            this.colVoltage.Name = "colVoltage";
            this.colVoltage.OptionsColumn.AllowEdit = false;
            this.colVoltage.OptionsColumn.AllowFocus = false;
            this.colVoltage.OptionsColumn.ReadOnly = true;
            this.colVoltage.Visible = true;
            this.colVoltage.VisibleIndex = 4;
            this.colVoltage.Width = 175;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Width = 175;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region Number";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.Width = 175;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 1;
            this.colSubAreaName.Width = 175;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area Number";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.Width = 175;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Width = 175;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary Number";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.Width = 175;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Width = 175;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder Number";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Width = 175;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            toolTipTitleItem12.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image20")));
            toolTipTitleItem12.Appearance.Options.UseImage = true;
            toolTipTitleItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image21")));
            toolTipTitleItem12.Text = "Record Team Members - Information";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "Click me to record Team Members on Site.";
            superToolTip12.Items.Add(toolTipTitleItem12);
            superToolTip12.Items.Add(toolTipItem12);
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Record Team Members", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "", "RecordTeamMembers", superToolTip12, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            this.repositoryItemButtonEdit2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Site Specific Risk Assessment", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Refresh Data", "refresh")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit3,
            this.repositoryItemMemoExEdit21,
            this.repositoryItemCheckEdit6});
            this.gridControl10.Size = new System.Drawing.Size(1086, 394);
            this.gridControl10.TabIndex = 25;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp07310UTTeamWorkOrderRiskAssessmentListBindingSource
            // 
            this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource.DataMember = "sp07310_UT_Team_Work_Order_Risk_Assessment_List";
            this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskAssessmentID,
            this.gridColumn8,
            this.colDateTimeRecorded,
            this.colPersonCompletingTypeID,
            this.colPersonCompletingID,
            this.colPermissionChecked,
            this.colElectricalAssessmentVerified,
            this.colElectricalRiskCategoryID,
            this.colTypeOfWorkingID,
            this.colPermitHolderName,
            this.colNRSWAOpeningNoticeNo,
            this.colRemarks6,
            this.colGUID6,
            this.colRiskAssessmentTypeID,
            this.colLastUpdated,
            this.colElectricalRiskCategory,
            this.colTypeOfWorking,
            this.colPersonCompletingType,
            this.colPersonCompletingName,
            this.colRiskAssessmentType,
            this.colSiteAddress,
            this.colMEWPArialRescuers,
            this.colEmergencyKitLocation,
            this.colAirAmbulanceLat,
            this.colAirAmbulanceLong,
            this.colBanksMan,
            this.colEmergencyServicesMeetPoint,
            this.colMobilePhoneSignalBars,
            this.colNearestAandE,
            this.colNearestLandLine,
            this.colLinkedTo,
            this.colCircuitName3,
            this.colCircuitNumber2,
            this.colPoleNumber3});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedTo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeRecorded, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView10_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.DoubleClick += new System.EventHandler(this.gridView10_DoubleClick);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colRiskAssessmentID
            // 
            this.colRiskAssessmentID.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID.Name = "colRiskAssessmentID";
            this.colRiskAssessmentID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID.Width = 114;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Surveyed Pole ID";
            this.gridColumn8.FieldName = "SurveyedPoleID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 104;
            // 
            // colDateTimeRecorded
            // 
            this.colDateTimeRecorded.Caption = "Created On";
            this.colDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit3;
            this.colDateTimeRecorded.FieldName = "DateTimeRecorded";
            this.colDateTimeRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeRecorded.Name = "colDateTimeRecorded";
            this.colDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeRecorded.Visible = true;
            this.colDateTimeRecorded.VisibleIndex = 2;
            this.colDateTimeRecorded.Width = 102;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit3.Mask.EditMask = "g";
            this.repositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // colPersonCompletingTypeID
            // 
            this.colPersonCompletingTypeID.Caption = "Person Completing Type ID";
            this.colPersonCompletingTypeID.FieldName = "PersonCompletingTypeID";
            this.colPersonCompletingTypeID.Name = "colPersonCompletingTypeID";
            this.colPersonCompletingTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingTypeID.Width = 151;
            // 
            // colPersonCompletingID
            // 
            this.colPersonCompletingID.Caption = "Person Completing ID";
            this.colPersonCompletingID.FieldName = "PersonCompletingID";
            this.colPersonCompletingID.Name = "colPersonCompletingID";
            this.colPersonCompletingID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingID.Width = 124;
            // 
            // colPermissionChecked
            // 
            this.colPermissionChecked.Caption = "Permission Checked";
            this.colPermissionChecked.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colPermissionChecked.FieldName = "PermissionChecked";
            this.colPermissionChecked.Name = "colPermissionChecked";
            this.colPermissionChecked.OptionsColumn.AllowEdit = false;
            this.colPermissionChecked.OptionsColumn.AllowFocus = false;
            this.colPermissionChecked.OptionsColumn.ReadOnly = true;
            this.colPermissionChecked.Visible = true;
            this.colPermissionChecked.VisibleIndex = 6;
            this.colPermissionChecked.Width = 115;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colElectricalAssessmentVerified
            // 
            this.colElectricalAssessmentVerified.Caption = "Electrical Assessment Verified";
            this.colElectricalAssessmentVerified.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colElectricalAssessmentVerified.FieldName = "ElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.Name = "colElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.OptionsColumn.AllowEdit = false;
            this.colElectricalAssessmentVerified.OptionsColumn.AllowFocus = false;
            this.colElectricalAssessmentVerified.OptionsColumn.ReadOnly = true;
            this.colElectricalAssessmentVerified.Visible = true;
            this.colElectricalAssessmentVerified.VisibleIndex = 7;
            this.colElectricalAssessmentVerified.Width = 162;
            // 
            // colElectricalRiskCategoryID
            // 
            this.colElectricalRiskCategoryID.Caption = "Electrical Risk Category ID";
            this.colElectricalRiskCategoryID.FieldName = "ElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.Name = "colElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategoryID.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategoryID.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategoryID.Width = 147;
            // 
            // colTypeOfWorkingID
            // 
            this.colTypeOfWorkingID.Caption = "Type Of Working ID";
            this.colTypeOfWorkingID.FieldName = "TypeOfWorkingID";
            this.colTypeOfWorkingID.Name = "colTypeOfWorkingID";
            this.colTypeOfWorkingID.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorkingID.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorkingID.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorkingID.Width = 116;
            // 
            // colPermitHolderName
            // 
            this.colPermitHolderName.Caption = "Permit Holder Name";
            this.colPermitHolderName.FieldName = "PermitHolderName";
            this.colPermitHolderName.Name = "colPermitHolderName";
            this.colPermitHolderName.OptionsColumn.AllowEdit = false;
            this.colPermitHolderName.OptionsColumn.AllowFocus = false;
            this.colPermitHolderName.OptionsColumn.ReadOnly = true;
            this.colPermitHolderName.Visible = true;
            this.colPermitHolderName.VisibleIndex = 10;
            this.colPermitHolderName.Width = 115;
            // 
            // colNRSWAOpeningNoticeNo
            // 
            this.colNRSWAOpeningNoticeNo.Caption = "NRSWA Opening Notice";
            this.colNRSWAOpeningNoticeNo.FieldName = "NRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.Name = "colNRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowEdit = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowFocus = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.ReadOnly = true;
            this.colNRSWAOpeningNoticeNo.Visible = true;
            this.colNRSWAOpeningNoticeNo.VisibleIndex = 11;
            this.colNRSWAOpeningNoticeNo.Width = 134;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit21
            // 
            this.repositoryItemMemoExEdit21.AutoHeight = false;
            this.repositoryItemMemoExEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit21.Name = "repositoryItemMemoExEdit21";
            this.repositoryItemMemoExEdit21.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit21.ShowIcon = false;
            // 
            // colGUID6
            // 
            this.colGUID6.Caption = "GUID";
            this.colGUID6.FieldName = "GUID";
            this.colGUID6.Name = "colGUID6";
            this.colGUID6.OptionsColumn.AllowEdit = false;
            this.colGUID6.OptionsColumn.AllowFocus = false;
            this.colGUID6.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentTypeID
            // 
            this.colRiskAssessmentTypeID.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.Name = "colRiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID.Width = 141;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.Caption = "Last Updated";
            this.colLastUpdated.ColumnEdit = this.repositoryItemDateEdit3;
            this.colLastUpdated.FieldName = "LastUpdated";
            this.colLastUpdated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastUpdated.Name = "colLastUpdated";
            this.colLastUpdated.OptionsColumn.AllowEdit = false;
            this.colLastUpdated.OptionsColumn.AllowFocus = false;
            this.colLastUpdated.OptionsColumn.ReadOnly = true;
            this.colLastUpdated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastUpdated.Visible = true;
            this.colLastUpdated.VisibleIndex = 12;
            this.colLastUpdated.Width = 85;
            // 
            // colElectricalRiskCategory
            // 
            this.colElectricalRiskCategory.Caption = "Electrical Risk Category";
            this.colElectricalRiskCategory.FieldName = "ElectricalRiskCategory";
            this.colElectricalRiskCategory.Name = "colElectricalRiskCategory";
            this.colElectricalRiskCategory.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategory.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategory.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategory.Visible = true;
            this.colElectricalRiskCategory.VisibleIndex = 8;
            this.colElectricalRiskCategory.Width = 133;
            // 
            // colTypeOfWorking
            // 
            this.colTypeOfWorking.Caption = "Type of Working";
            this.colTypeOfWorking.FieldName = "TypeOfWorking";
            this.colTypeOfWorking.Name = "colTypeOfWorking";
            this.colTypeOfWorking.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorking.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorking.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorking.Visible = true;
            this.colTypeOfWorking.VisibleIndex = 9;
            this.colTypeOfWorking.Width = 100;
            // 
            // colPersonCompletingType
            // 
            this.colPersonCompletingType.Caption = "Person Type";
            this.colPersonCompletingType.FieldName = "PersonCompletingType";
            this.colPersonCompletingType.Name = "colPersonCompletingType";
            this.colPersonCompletingType.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingType.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingType.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingType.Visible = true;
            this.colPersonCompletingType.VisibleIndex = 5;
            this.colPersonCompletingType.Width = 81;
            // 
            // colPersonCompletingName
            // 
            this.colPersonCompletingName.Caption = "Person Completing Name";
            this.colPersonCompletingName.FieldName = "PersonCompletingName";
            this.colPersonCompletingName.Name = "colPersonCompletingName";
            this.colPersonCompletingName.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingName.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingName.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingName.Visible = true;
            this.colPersonCompletingName.VisibleIndex = 4;
            this.colPersonCompletingName.Width = 140;
            // 
            // colRiskAssessmentType
            // 
            this.colRiskAssessmentType.Caption = "Risk Assessment Type";
            this.colRiskAssessmentType.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType.Name = "colRiskAssessmentType";
            this.colRiskAssessmentType.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType.Visible = true;
            this.colRiskAssessmentType.VisibleIndex = 3;
            this.colRiskAssessmentType.Width = 127;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address and Access";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 14;
            this.colSiteAddress.Width = 138;
            // 
            // colMEWPArialRescuers
            // 
            this.colMEWPArialRescuers.Caption = "MEWP \\ Aerial Rescuers";
            this.colMEWPArialRescuers.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colMEWPArialRescuers.FieldName = "MEWPArialRescuers";
            this.colMEWPArialRescuers.Name = "colMEWPArialRescuers";
            this.colMEWPArialRescuers.OptionsColumn.ReadOnly = true;
            this.colMEWPArialRescuers.Visible = true;
            this.colMEWPArialRescuers.VisibleIndex = 15;
            this.colMEWPArialRescuers.Width = 135;
            // 
            // colEmergencyKitLocation
            // 
            this.colEmergencyKitLocation.Caption = "Emergency Kit Location";
            this.colEmergencyKitLocation.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyKitLocation.FieldName = "EmergencyKitLocation";
            this.colEmergencyKitLocation.Name = "colEmergencyKitLocation";
            this.colEmergencyKitLocation.OptionsColumn.ReadOnly = true;
            this.colEmergencyKitLocation.Visible = true;
            this.colEmergencyKitLocation.VisibleIndex = 16;
            this.colEmergencyKitLocation.Width = 132;
            // 
            // colAirAmbulanceLat
            // 
            this.colAirAmbulanceLat.Caption = "Air Ambulance Lat";
            this.colAirAmbulanceLat.FieldName = "AirAmbulanceLat";
            this.colAirAmbulanceLat.Name = "colAirAmbulanceLat";
            this.colAirAmbulanceLat.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLat.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLat.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLat.Visible = true;
            this.colAirAmbulanceLat.VisibleIndex = 17;
            this.colAirAmbulanceLat.Width = 107;
            // 
            // colAirAmbulanceLong
            // 
            this.colAirAmbulanceLong.Caption = "Air Ambulance Long";
            this.colAirAmbulanceLong.FieldName = "AirAmbulanceLong";
            this.colAirAmbulanceLong.Name = "colAirAmbulanceLong";
            this.colAirAmbulanceLong.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLong.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLong.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLong.Visible = true;
            this.colAirAmbulanceLong.VisibleIndex = 18;
            this.colAirAmbulanceLong.Width = 115;
            // 
            // colBanksMan
            // 
            this.colBanksMan.Caption = "Banksman";
            this.colBanksMan.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colBanksMan.FieldName = "BanksMan";
            this.colBanksMan.Name = "colBanksMan";
            this.colBanksMan.OptionsColumn.ReadOnly = true;
            this.colBanksMan.Visible = true;
            this.colBanksMan.VisibleIndex = 19;
            // 
            // colEmergencyServicesMeetPoint
            // 
            this.colEmergencyServicesMeetPoint.Caption = "Emergency Service Meet Point";
            this.colEmergencyServicesMeetPoint.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyServicesMeetPoint.FieldName = "EmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.Name = "colEmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.OptionsColumn.ReadOnly = true;
            this.colEmergencyServicesMeetPoint.Visible = true;
            this.colEmergencyServicesMeetPoint.VisibleIndex = 20;
            this.colEmergencyServicesMeetPoint.Width = 166;
            // 
            // colMobilePhoneSignalBars
            // 
            this.colMobilePhoneSignalBars.Caption = "Phone Signal Bars";
            this.colMobilePhoneSignalBars.FieldName = "MobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.Name = "colMobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.OptionsColumn.AllowEdit = false;
            this.colMobilePhoneSignalBars.OptionsColumn.AllowFocus = false;
            this.colMobilePhoneSignalBars.OptionsColumn.ReadOnly = true;
            this.colMobilePhoneSignalBars.Visible = true;
            this.colMobilePhoneSignalBars.VisibleIndex = 21;
            this.colMobilePhoneSignalBars.Width = 106;
            // 
            // colNearestAandE
            // 
            this.colNearestAandE.Caption = "Nearest A & E";
            this.colNearestAandE.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestAandE.FieldName = "NearestAandE";
            this.colNearestAandE.Name = "colNearestAandE";
            this.colNearestAandE.OptionsColumn.ReadOnly = true;
            this.colNearestAandE.Visible = true;
            this.colNearestAandE.VisibleIndex = 22;
            this.colNearestAandE.Width = 88;
            // 
            // colNearestLandLine
            // 
            this.colNearestLandLine.Caption = "Nearest Landline";
            this.colNearestLandLine.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestLandLine.FieldName = "NearestLandLine";
            this.colNearestLandLine.Name = "colNearestLandLine";
            this.colNearestLandLine.OptionsColumn.ReadOnly = true;
            this.colNearestLandLine.Visible = true;
            this.colNearestLandLine.VisibleIndex = 23;
            this.colNearestLandLine.Width = 101;
            // 
            // colLinkedTo
            // 
            this.colLinkedTo.FieldName = "LinkedTo";
            this.colLinkedTo.Name = "colLinkedTo";
            this.colLinkedTo.OptionsColumn.AllowEdit = false;
            this.colLinkedTo.OptionsColumn.AllowFocus = false;
            this.colLinkedTo.OptionsColumn.ReadOnly = true;
            this.colLinkedTo.Visible = true;
            this.colLinkedTo.VisibleIndex = 22;
            this.colLinkedTo.Width = 164;
            // 
            // colCircuitName3
            // 
            this.colCircuitName3.FieldName = "CircuitName";
            this.colCircuitName3.Name = "colCircuitName3";
            this.colCircuitName3.OptionsColumn.AllowEdit = false;
            this.colCircuitName3.OptionsColumn.AllowFocus = false;
            this.colCircuitName3.OptionsColumn.ReadOnly = true;
            this.colCircuitName3.Width = 124;
            // 
            // colCircuitNumber2
            // 
            this.colCircuitNumber2.FieldName = "CircuitNumber";
            this.colCircuitNumber2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitNumber2.Name = "colCircuitNumber2";
            this.colCircuitNumber2.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber2.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber2.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber2.Visible = true;
            this.colCircuitNumber2.VisibleIndex = 0;
            this.colCircuitNumber2.Width = 110;
            // 
            // colPoleNumber3
            // 
            this.colPoleNumber3.FieldName = "PoleNumber";
            this.colPoleNumber3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber3.Name = "colPoleNumber3";
            this.colPoleNumber3.OptionsColumn.AllowEdit = false;
            this.colPoleNumber3.OptionsColumn.AllowFocus = false;
            this.colPoleNumber3.OptionsColumn.ReadOnly = true;
            this.colPoleNumber3.Visible = true;
            this.colPoleNumber3.VisibleIndex = 1;
            this.colPoleNumber3.Width = 110;
            // 
            // sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter
            // 
            this.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // xtraGridBlending7
            // 
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending7.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending7.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending7.GridControl = this.gridControl7;
            // 
            // sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter
            // 
            this.sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter
            // 
            this.sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter
            // 
            this.sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter
            // 
            this.sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter
            // 
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Team_WorkOrder_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1095, 748);
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Team_WorkOrder_Edit";
            this.ShowIcon = true;
            this.Text = "Edit Team Work Order";
            this.Activated += new System.EventHandler(this.frm_UT_Team_WorkOrder_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Team_WorkOrder_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Team_WorkOrder_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmRPSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SurveyDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07247UTWorkOrderEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditRecord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07299UTTeamWorkOrderActionPicturesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderPDFHyperLinkEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalCostTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaddedWorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkOrderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateSubContractorSentDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditStartJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCompleteJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditOnHoldJob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditTeamRemarksDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCompletedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateSubContractorSent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSurveyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaddedWorkOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCompleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkOrderPDF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWorkOrder.ResumeLayout(false);
            this.xtraTabPageRPSheet.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit2DPMetres)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit0DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditStart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditComplete2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPMetresDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DPDisabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditAnswer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07262UTRiskAssessmentQuestionAnswersListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditDiabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditDisabled)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit4)).EndInit();
            this.xtraTabPageRiskAssessment.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07309UTTeamWorkOrderRiskLocationsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07310UTTeamWorkOrderRiskAssessmentListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit WorkOrderIDTextEdit;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.DateEdit DateSubContractorSentDateEdit;
        private DevExpress.XtraEditors.DateEdit DateCompletedDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit ReferenceNumberButtonEdit;
        private DevExpress.XtraEditors.DateEdit DateCreatedDateEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateSubContractorSent;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCompleted;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCreated;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrint;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp07247UTWorkOrderEditBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07247_UT_Work_Order_EditTableAdapter sp07247_UT_Work_Order_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit PaddedWorkOrderIDTextEdit;
        private DevExpress.XtraEditors.TextEdit SubContractorIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraEditors.TextEdit TotalCostTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalCost;
        private DevExpress.XtraEditors.TextEdit CreatedByTextEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.LayoutControlItem ItemforCreatedBy;
        private System.Windows.Forms.BindingSource sp07249UTWorkOrderManagerLinkedActionsEditBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedTreeID;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colDateScheduled;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colApproved;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colSelfBillingInvoiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceSystemBillingID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colJobDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private System.Windows.Forms.BindingSource sp07272UTWorkOrderEditLinkedMapsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMap;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTakenByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderSortOrder;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.HyperLinkEdit WorkOrderPDFHyperLinkEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkOrderPDF;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaddedWorkOrderID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private System.Windows.Forms.BindingSource sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberCount;
        private DataSet_UT_WorkOrderTableAdapters.sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButton;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButtonComplete;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditCompleteJob;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEditHours;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButtonStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditStartJob;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource sp07299UTTeamWorkOrderActionPicturesListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyPictureID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeTaken;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colShortLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DataSet_UT_WorkOrderTableAdapters.sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeID;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTypeDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHoursDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditTeamRemarksDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButtonOnHold;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditOnHoldJob;
        private DevExpress.XtraBars.BarCheckItem barCheckItemWorkOrder;
        private DevExpress.XtraBars.BarCheckItem barCheckItemRPSheet;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWorkOrder;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRPSheet;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn40;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn44;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn45;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit4;
        private System.Windows.Forms.BindingSource sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colFinalClearanceDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit2DPMetres;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCompleteFitToBill;
        private DevExpress.XtraGrid.Columns.GridColumn colTreesFelledCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit0DP;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpToGrindCount;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpsTreatedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colEcoPlugsAppliedCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colFurtherWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colFurtherWorkRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID2;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedByTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DataSet_UT_WorkOrderTableAdapters.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedActions;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalActions;
        private DevExpress.XtraGrid.Columns.GridColumn colJobsDoneStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButtonStart2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditStart2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnButtonComplete2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditComplete2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTimeDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DPMetresDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit0DPDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private System.Windows.Forms.BindingSource sp07307UTTeamWorkOrderCompletionSheetEditBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletionSheetID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colAuditAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn colAuditRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colAuditedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colAuditedByName;
        private DataSet_UT_WorkOrderTableAdapters.sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditAnswer;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07262UTRiskAssessmentQuestionAnswersListBindingSource;
        private DataSet_UTTableAdapters.sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditDiabled;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditDisabled;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.BarCheckItem barCheckItemRiskAssessment;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageRiskAssessment;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private System.Windows.Forms.BindingSource sp07309UTTeamWorkOrderRiskLocationsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName2;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltage;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DataSet_UT_WorkOrderTableAdapters.sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeRecorded;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionChecked;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalAssessmentVerified;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorkingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermitHolderName;
        private DevExpress.XtraGrid.Columns.GridColumn colNRSWAOpeningNoticeNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit21;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID6;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorking;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingType;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingName;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colMEWPArialRescuers;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyKitLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLat;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLong;
        private DevExpress.XtraGrid.Columns.GridColumn colBanksMan;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyServicesMeetPoint;
        private DevExpress.XtraGrid.Columns.GridColumn colMobilePhoneSignalBars;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestAandE;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestLandLine;
        private System.Windows.Forms.BindingSource sp07310UTTeamWorkOrderRiskAssessmentListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedTo;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName3;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber3;
        private DataSet_UT_WorkOrderTableAdapters.sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPrintRPSheet;
        private DevExpress.XtraBars.BarButtonItem bbiEditRPSheetLayout;
        private DevExpress.XtraBars.PopupMenu pmRPSheet;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.TextEdit StatusIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDFFile;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionEmailed;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLandownerName;
        private DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID;
        private DevExpress.XtraEditors.SimpleButton btnShowMap;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnEditRecord;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraEditors.TextEdit SurveyDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSurveyDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeSpeciesList;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
    }
}

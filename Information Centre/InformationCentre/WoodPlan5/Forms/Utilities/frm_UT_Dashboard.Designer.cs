﻿namespace WoodPlan5
{
    partial class frm_UT_Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Dashboard));
            DevExpress.XtraEditors.RangeControlRange rangeControlRange1 = new DevExpress.XtraEditors.RangeControlRange();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraCharts.XYDiagram xyDiagram3 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView1 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle3 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D1 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel1 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView1 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel2 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView2 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle4 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.XYDiagram xyDiagram4 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView2 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series3 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView3 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series4 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView4 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series5 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView5 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView6 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle5 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.SimpleDiagram3D simpleDiagram3D2 = new DevExpress.XtraCharts.SimpleDiagram3D();
            DevExpress.XtraCharts.Series series6 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel3 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView3 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.Pie3DSeriesLabel pie3DSeriesLabel4 = new DevExpress.XtraCharts.Pie3DSeriesLabel();
            DevExpress.XtraCharts.Pie3DSeriesView pie3DSeriesView4 = new DevExpress.XtraCharts.Pie3DSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle6 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.XYDiagram xyDiagram2 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.AreaSeriesView areaSeriesView1 = new DevExpress.XtraCharts.AreaSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle2 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.XYDiagram xyDiagram5 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series7 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView7 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series8 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView8 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series9 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView9 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.Series series10 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView10 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.SideBySideStackedBarSeriesView sideBySideStackedBarSeriesView11 = new DevExpress.XtraCharts.SideBySideStackedBarSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle7 = new DevExpress.XtraCharts.ChartTitle();
            this.rangeControl1 = new DevExpress.XtraEditors.RangeControl();
            this.chartControl6 = new DevExpress.XtraCharts.ChartControl();
            this.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter();
            this.sp07340UTReportingSurveyorMetricsByDateBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Reporting = new WoodPlan5.DataSet_UT_Reporting();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiPrintPreviewScreen = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemChartSetup = new DevExpress.XtraBars.BarButtonItem();
            this.popupControlContainerChartSetup = new DevExpress.XtraBars.PopupControlContainer(this.components);
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart7ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart7ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart6ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart6ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart5ShowTarget = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart5ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart5ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart4ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart4ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart2ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart2ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart3ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart3ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkEditChart1ShowLabels = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditChart1ShowLegend = new DevExpress.XtraEditors.CheckEdit();
            this.buttonEditFilterCircuits = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemButtonEditFilterCircuits = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.barEditItemPopupVoltageFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditVoltageFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlVoltageFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.VoltageFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07327UTVoltagesFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colintOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barEditItemPopupDateFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnOK1 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.spinEditYear = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.barEditItemPopupSurveyorFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditSurveyorFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlSurveyorFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.SurveyorFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07329UTSurveyorListNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDisplayName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barEditItemPopupTeamFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditTeamFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlTeamFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.TeamFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07330UTTeamListNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barEditItemSurveyorPerformance = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditSurveyorPerformance = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlSurveyorPerformance = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnOk_5 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditDateDivision2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditMetric2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditDateDivision1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEditMetric1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemPopupContainerEditChartSetup = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter();
            this.chartControl3 = new DevExpress.XtraCharts.ChartControl();
            this.chartControl2 = new DevExpress.XtraCharts.ChartControl();
            this.sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter();
            this.chartControl4 = new DevExpress.XtraCharts.ChartControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateEditGroupingEndDate = new DevExpress.XtraEditors.DateEdit();
            this.spinEditDaysGrouping = new DevExpress.XtraEditors.SpinEdit();
            this.dateEditYTDStart = new DevExpress.XtraEditors.DateEdit();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07349UTReportingSurveyormetrics2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bandedGridView1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colStaffID2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colForename2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSurname2 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSurveyorName1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekSurveyedSpanCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEditInteger = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSurveyedSpanCount1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand5 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekPermissionedSpanCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colPermissionedSpanCount1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekCompletedSpanCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colCompletedSpanCount1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand8 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekReactiveSpanCompleteCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colReactiveSpanCompleteCount = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekSpansNeedingWork = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSpansNeedingWork1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekSurveyedSpanClear = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colSurveyedSpanClear1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colWeekAverageInfestation = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAverageInfestation1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.chartControl7 = new DevExpress.XtraCharts.ChartControl();
            this.sp07340UTReportingSurveyorMetricsByDate2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl7 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl8 = new DevExpress.XtraEditors.SplitContainerControl();
            this.chartControl5 = new DevExpress.XtraCharts.ChartControl();
            this.sp07335UTReportingClearSpanTargetVActualBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07339UTReportingSurveyormetricsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStaffID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedSpanCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageInfestation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCompletedSpanCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionedSpanCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageSpanCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSpansNeedingWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp07327_UT_Voltages_Filter_ListTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07327_UT_Voltages_Filter_ListTableAdapter();
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter();
            this.sp07330_UT_Team_List_No_BlankTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07330_UT_Team_List_No_BlankTableAdapter();
            this.sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter();
            this.sp07339_UT_Reporting_Surveyor_metricsTableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07339_UT_Reporting_Surveyor_metricsTableAdapter();
            this.sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter();
            this.sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter = new WoodPlan5.DataSet_UT_ReportingTableAdapters.sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07340UTReportingSurveyorMetricsByDateBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainerChartSetup)).BeginInit();
            this.popupControlContainerChartSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart7ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart7ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart6ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart6ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowTarget.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart4ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart4ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart2ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart2ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart3ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart3ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart1ShowLabels.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart1ShowLegend.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditVoltageFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVoltageFilter)).BeginInit();
            this.popupContainerControlVoltageFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07327UTVoltagesFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFilter)).BeginInit();
            this.popupContainerControlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSurveyorFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyorFilter)).BeginInit();
            this.popupContainerControlSurveyorFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditTeamFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTeamFilter)).BeginInit();
            this.popupContainerControlTeamFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07330UTTeamListNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSurveyorPerformance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyorPerformance)).BeginInit();
            this.popupContainerControlSurveyorPerformance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDateDivision2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMetric2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDateDivision1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMetric1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditChartSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGroupingEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGroupingEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysGrouping.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditYTDStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditYTDStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07349UTReportingSurveyormetrics2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(areaSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07340UTReportingSurveyorMetricsByDate2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).BeginInit();
            this.splitContainerControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).BeginInit();
            this.splitContainerControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07335UTReportingClearSpanTargetVActualBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07339UTReportingSurveyormetricsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 34);
            this.barDockControlTop.Size = new System.Drawing.Size(1113, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 552);
            this.barDockControlBottom.Size = new System.Drawing.Size(1113, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 518);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1113, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 518);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // rangeControl1
            // 
            this.rangeControl1.Client = this.chartControl6;
            this.rangeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rangeControl1.Location = new System.Drawing.Point(0, 0);
            this.rangeControl1.Name = "rangeControl1";
            rangeControlRange1.Maximum = "E";
            rangeControlRange1.Minimum = "A";
            rangeControlRange1.Owner = this.rangeControl1;
            this.rangeControl1.SelectedRange = rangeControlRange1;
            this.rangeControl1.Size = new System.Drawing.Size(1103, 58);
            this.rangeControl1.TabIndex = 0;
            this.rangeControl1.Text = "rangeControl1";
            // 
            // chartControl6
            // 
            this.chartControl6.DataAdapter = this.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter;
            this.chartControl6.DataSource = this.sp07340UTReportingSurveyorMetricsByDateBindingSource;
            xyDiagram1.AxisX.DateTimeScaleOptions.AutoGrid = false;
            xyDiagram1.AxisX.Label.Angle = 270;
            xyDiagram1.AxisX.NumericScaleOptions.AutoGrid = false;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisX.VisualRange.Auto = false;
            xyDiagram1.AxisX.VisualRange.MaxValueSerializable = "E";
            xyDiagram1.AxisX.VisualRange.MinValueSerializable = "A";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXScrolling = true;
            xyDiagram1.EnableAxisXZooming = true;
            this.chartControl6.Diagram = xyDiagram1;
            this.chartControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl6.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl6.EmptyChartText.Text = "No Data To Chart";
            this.chartControl6.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center;
            this.chartControl6.Legend.Name = "Default Legend";
            this.chartControl6.Location = new System.Drawing.Point(0, 0);
            this.chartControl6.Name = "chartControl6";
            this.chartControl6.RuntimeHitTesting = true;
            this.chartControl6.SelectionMode = DevExpress.XtraCharts.ElementSelectionMode.Single;
            this.chartControl6.SeriesDataMember = "SurveyorName";
            this.chartControl6.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl6.SeriesTemplate.ArgumentDataMember = "DateDescription";
            this.chartControl6.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            pointSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl6.SeriesTemplate.Label = pointSeriesLabel1;
            this.chartControl6.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl6.SeriesTemplate.LegendTextPattern = "{A}: {V:G}";
            this.chartControl6.SeriesTemplate.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl6.SeriesTemplate.ValueDataMembersSerializable = "SurveyedTotal";
            this.chartControl6.SeriesTemplate.View = lineSeriesView1;
            this.chartControl6.Size = new System.Drawing.Size(1103, 222);
            this.chartControl6.TabIndex = 18;
            chartTitle1.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle1.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle1.Indent = 0;
            chartTitle1.Text = "Chart 1";
            this.chartControl6.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1});
            this.chartControl6.ObjectSelected += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl6_ObjectSelected);
            this.chartControl6.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl6_ObjectHotTracked);
            this.chartControl6.Enter += new System.EventHandler(this.chartControl6_Enter);
            // 
            // sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter
            // 
            this.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter.ClearBeforeFill = true;
            // 
            // sp07340UTReportingSurveyorMetricsByDateBindingSource
            // 
            this.sp07340UTReportingSurveyorMetricsByDateBindingSource.DataMember = "sp07340_UT_Reporting_Surveyor_Metrics_By_Date";
            this.sp07340UTReportingSurveyorMetricsByDateBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // dataSet_UT_Reporting
            // 
            this.dataSet_UT_Reporting.DataSetName = "DataSet_UT_Reporting";
            this.dataSet_UT_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 52;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 2;
            this.colActive1.Width = 52;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // sp07319UTReportingSurveyorsbySpansSurveyedBindingSource
            // 
            this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource.DataMember = "sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed";
            this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // sp07331UTReportingTeamsbySpansWorkedOnBindingSource
            // 
            this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource.DataMember = "sp07331_UT_Reporting_Teams_by_Spans_Worked_On";
            this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemPopupDateFilter,
            this.bbiRefresh,
            this.bbiPrintPreviewScreen,
            this.buttonEditFilterCircuits,
            this.barEditItemPopupVoltageFilter,
            this.barEditItemPopupSurveyorFilter,
            this.barEditItemPopupTeamFilter,
            this.barButtonItemChartSetup,
            this.barEditItemSurveyorPerformance});
            this.barManager2.MaxItemId = 11;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateFilter,
            this.repositoryItemButtonEditFilterCircuits,
            this.repositoryItemPopupContainerEditVoltageFilter,
            this.repositoryItemPopupContainerEditSurveyorFilter,
            this.repositoryItemPopupContainerEditTeamFilter,
            this.repositoryItemPopupContainerEditChartSetup,
            this.repositoryItemPopupContainerEditSurveyorPerformance});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Control";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrintPreviewScreen, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemChartSetup, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.buttonEditFilterCircuits, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPopupVoltageFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPopupDateFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPopupSurveyorFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemPopupTeamFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemSurveyorPerformance),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // bbiPrintPreviewScreen
            // 
            this.bbiPrintPreviewScreen.Caption = "Print";
            this.bbiPrintPreviewScreen.Id = 2;
            this.bbiPrintPreviewScreen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPrintPreviewScreen.ImageOptions.Image")));
            this.bbiPrintPreviewScreen.Name = "bbiPrintPreviewScreen";
            this.bbiPrintPreviewScreen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintPreviewScreen_ItemClick);
            // 
            // barButtonItemChartSetup
            // 
            this.barButtonItemChartSetup.ActAsDropDown = true;
            this.barButtonItemChartSetup.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItemChartSetup.Caption = "Charts Setup";
            this.barButtonItemChartSetup.DropDownControl = this.popupControlContainerChartSetup;
            this.barButtonItemChartSetup.Id = 9;
            this.barButtonItemChartSetup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItemChartSetup.ImageOptions.Image")));
            this.barButtonItemChartSetup.Name = "barButtonItemChartSetup";
            // 
            // popupControlContainerChartSetup
            // 
            this.popupControlContainerChartSetup.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl8);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl7);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl6);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl5);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl4);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl3);
            this.popupControlContainerChartSetup.Controls.Add(this.groupControl2);
            this.popupControlContainerChartSetup.Location = new System.Drawing.Point(73, 3);
            this.popupControlContainerChartSetup.Manager = this.barManager1;
            this.popupControlContainerChartSetup.Name = "popupControlContainerChartSetup";
            this.popupControlContainerChartSetup.Size = new System.Drawing.Size(172, 497);
            this.popupControlContainerChartSetup.TabIndex = 19;
            this.popupControlContainerChartSetup.Visible = false;
            // 
            // groupControl8
            // 
            this.groupControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl8.Controls.Add(this.checkEditChart7ShowLabels);
            this.groupControl8.Controls.Add(this.checkEditChart7ShowLegend);
            this.groupControl8.Location = new System.Drawing.Point(2, 431);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(168, 65);
            this.groupControl8.TabIndex = 10;
            this.groupControl8.Text = "Surveyor Accumulated Totals";
            // 
            // checkEditChart7ShowLabels
            // 
            this.checkEditChart7ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart7ShowLabels.EditValue = 1;
            this.checkEditChart7ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart7ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart7ShowLabels.Name = "checkEditChart7ShowLabels";
            this.checkEditChart7ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart7ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart7ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart7ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart7ShowLabels.TabIndex = 1;
            // 
            // checkEditChart7ShowLegend
            // 
            this.checkEditChart7ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart7ShowLegend.EditValue = 1;
            this.checkEditChart7ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart7ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart7ShowLegend.Name = "checkEditChart7ShowLegend";
            this.checkEditChart7ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart7ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart7ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart7ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart7ShowLegend.TabIndex = 0;
            // 
            // groupControl7
            // 
            this.groupControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl7.Controls.Add(this.checkEditChart6ShowLabels);
            this.groupControl7.Controls.Add(this.checkEditChart6ShowLegend);
            this.groupControl7.Location = new System.Drawing.Point(2, 363);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(168, 65);
            this.groupControl7.TabIndex = 9;
            this.groupControl7.Text = "Surveyor Totals";
            // 
            // checkEditChart6ShowLabels
            // 
            this.checkEditChart6ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart6ShowLabels.EditValue = 1;
            this.checkEditChart6ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart6ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart6ShowLabels.Name = "checkEditChart6ShowLabels";
            this.checkEditChart6ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart6ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart6ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart6ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart6ShowLabels.TabIndex = 1;
            // 
            // checkEditChart6ShowLegend
            // 
            this.checkEditChart6ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart6ShowLegend.EditValue = 1;
            this.checkEditChart6ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart6ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart6ShowLegend.Name = "checkEditChart6ShowLegend";
            this.checkEditChart6ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart6ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart6ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart6ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart6ShowLegend.TabIndex = 0;
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.checkEditChart5ShowTarget);
            this.groupControl6.Controls.Add(this.checkEditChart5ShowLabels);
            this.groupControl6.Controls.Add(this.checkEditChart5ShowLegend);
            this.groupControl6.Location = new System.Drawing.Point(2, 274);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(168, 87);
            this.groupControl6.TabIndex = 8;
            this.groupControl6.Text = "Clear Span Target V Actual";
            // 
            // checkEditChart5ShowTarget
            // 
            this.checkEditChart5ShowTarget.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart5ShowTarget.EditValue = 1;
            this.checkEditChart5ShowTarget.Location = new System.Drawing.Point(5, 65);
            this.checkEditChart5ShowTarget.MenuManager = this.barManager1;
            this.checkEditChart5ShowTarget.Name = "checkEditChart5ShowTarget";
            this.checkEditChart5ShowTarget.Properties.Caption = "Show Target Line";
            this.checkEditChart5ShowTarget.Properties.ValueChecked = 1;
            this.checkEditChart5ShowTarget.Properties.ValueUnchecked = 0;
            this.checkEditChart5ShowTarget.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart5ShowTarget.TabIndex = 2;
            // 
            // checkEditChart5ShowLabels
            // 
            this.checkEditChart5ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart5ShowLabels.EditValue = 1;
            this.checkEditChart5ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart5ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart5ShowLabels.Name = "checkEditChart5ShowLabels";
            this.checkEditChart5ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart5ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart5ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart5ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart5ShowLabels.TabIndex = 1;
            // 
            // checkEditChart5ShowLegend
            // 
            this.checkEditChart5ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart5ShowLegend.EditValue = 1;
            this.checkEditChart5ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart5ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart5ShowLegend.Name = "checkEditChart5ShowLegend";
            this.checkEditChart5ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart5ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart5ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart5ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart5ShowLegend.TabIndex = 0;
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Controls.Add(this.checkEditChart4ShowLabels);
            this.groupControl5.Controls.Add(this.checkEditChart4ShowLegend);
            this.groupControl5.Location = new System.Drawing.Point(2, 206);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(168, 65);
            this.groupControl5.TabIndex = 7;
            this.groupControl5.Text = "Job Breakdown By Team";
            // 
            // checkEditChart4ShowLabels
            // 
            this.checkEditChart4ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart4ShowLabels.EditValue = 1;
            this.checkEditChart4ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart4ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart4ShowLabels.Name = "checkEditChart4ShowLabels";
            this.checkEditChart4ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart4ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart4ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart4ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart4ShowLabels.TabIndex = 1;
            // 
            // checkEditChart4ShowLegend
            // 
            this.checkEditChart4ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart4ShowLegend.EditValue = 1;
            this.checkEditChart4ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart4ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart4ShowLegend.Name = "checkEditChart4ShowLegend";
            this.checkEditChart4ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart4ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart4ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart4ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart4ShowLegend.TabIndex = 0;
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.checkEditChart2ShowLabels);
            this.groupControl4.Controls.Add(this.checkEditChart2ShowLegend);
            this.groupControl4.Location = new System.Drawing.Point(2, 138);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(168, 65);
            this.groupControl4.TabIndex = 6;
            this.groupControl4.Text = "Jobs By Team";
            // 
            // checkEditChart2ShowLabels
            // 
            this.checkEditChart2ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart2ShowLabels.EditValue = 1;
            this.checkEditChart2ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart2ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart2ShowLabels.Name = "checkEditChart2ShowLabels";
            this.checkEditChart2ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart2ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart2ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart2ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart2ShowLabels.TabIndex = 1;
            // 
            // checkEditChart2ShowLegend
            // 
            this.checkEditChart2ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart2ShowLegend.EditValue = 1;
            this.checkEditChart2ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart2ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart2ShowLegend.Name = "checkEditChart2ShowLegend";
            this.checkEditChart2ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart2ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart2ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart2ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart2ShowLegend.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.checkEditChart3ShowLabels);
            this.groupControl3.Controls.Add(this.checkEditChart3ShowLegend);
            this.groupControl3.Location = new System.Drawing.Point(2, 70);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(168, 65);
            this.groupControl3.TabIndex = 5;
            this.groupControl3.Text = "Spans Surveyed Breakdown";
            // 
            // checkEditChart3ShowLabels
            // 
            this.checkEditChart3ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart3ShowLabels.EditValue = 1;
            this.checkEditChart3ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart3ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart3ShowLabels.Name = "checkEditChart3ShowLabels";
            this.checkEditChart3ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart3ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart3ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart3ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart3ShowLabels.TabIndex = 1;
            // 
            // checkEditChart3ShowLegend
            // 
            this.checkEditChart3ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart3ShowLegend.EditValue = 1;
            this.checkEditChart3ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart3ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart3ShowLegend.Name = "checkEditChart3ShowLegend";
            this.checkEditChart3ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart3ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart3ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart3ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart3ShowLegend.TabIndex = 0;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.checkEditChart1ShowLabels);
            this.groupControl2.Controls.Add(this.checkEditChart1ShowLegend);
            this.groupControl2.Location = new System.Drawing.Point(2, 2);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(168, 65);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Spans Surveyed By Surveyor";
            // 
            // checkEditChart1ShowLabels
            // 
            this.checkEditChart1ShowLabels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart1ShowLabels.EditValue = 1;
            this.checkEditChart1ShowLabels.Location = new System.Drawing.Point(5, 43);
            this.checkEditChart1ShowLabels.MenuManager = this.barManager1;
            this.checkEditChart1ShowLabels.Name = "checkEditChart1ShowLabels";
            this.checkEditChart1ShowLabels.Properties.Caption = "Show Labels";
            this.checkEditChart1ShowLabels.Properties.ValueChecked = 1;
            this.checkEditChart1ShowLabels.Properties.ValueUnchecked = 0;
            this.checkEditChart1ShowLabels.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart1ShowLabels.TabIndex = 1;
            // 
            // checkEditChart1ShowLegend
            // 
            this.checkEditChart1ShowLegend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditChart1ShowLegend.EditValue = 1;
            this.checkEditChart1ShowLegend.Location = new System.Drawing.Point(5, 22);
            this.checkEditChart1ShowLegend.MenuManager = this.barManager1;
            this.checkEditChart1ShowLegend.Name = "checkEditChart1ShowLegend";
            this.checkEditChart1ShowLegend.Properties.Caption = "Show Legend";
            this.checkEditChart1ShowLegend.Properties.ValueChecked = 1;
            this.checkEditChart1ShowLegend.Properties.ValueUnchecked = 0;
            this.checkEditChart1ShowLegend.Size = new System.Drawing.Size(158, 19);
            this.checkEditChart1ShowLegend.TabIndex = 0;
            // 
            // buttonEditFilterCircuits
            // 
            this.buttonEditFilterCircuits.Caption = "Circuit Filter";
            this.buttonEditFilterCircuits.Edit = this.repositoryItemButtonEditFilterCircuits;
            this.buttonEditFilterCircuits.EditValue = "No Circuit Filter";
            this.buttonEditFilterCircuits.EditWidth = 201;
            this.buttonEditFilterCircuits.Id = 3;
            this.buttonEditFilterCircuits.Name = "buttonEditFilterCircuits";
            // 
            // repositoryItemButtonEditFilterCircuits
            // 
            this.repositoryItemButtonEditFilterCircuits.AutoHeight = false;
            this.repositoryItemButtonEditFilterCircuits.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditFilterCircuits.Name = "repositoryItemButtonEditFilterCircuits";
            this.repositoryItemButtonEditFilterCircuits.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.repositoryItemButtonEditFilterCircuits.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditFilterCircuits_ButtonClick);
            // 
            // barEditItemPopupVoltageFilter
            // 
            this.barEditItemPopupVoltageFilter.Caption = "Voltage Filter";
            this.barEditItemPopupVoltageFilter.Edit = this.repositoryItemPopupContainerEditVoltageFilter;
            this.barEditItemPopupVoltageFilter.EditValue = "No Voltage Filter";
            this.barEditItemPopupVoltageFilter.EditWidth = 107;
            this.barEditItemPopupVoltageFilter.Id = 4;
            this.barEditItemPopupVoltageFilter.Name = "barEditItemPopupVoltageFilter";
            // 
            // repositoryItemPopupContainerEditVoltageFilter
            // 
            this.repositoryItemPopupContainerEditVoltageFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditVoltageFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditVoltageFilter.Name = "repositoryItemPopupContainerEditVoltageFilter";
            this.repositoryItemPopupContainerEditVoltageFilter.PopupControl = this.popupContainerControlVoltageFilter;
            this.repositoryItemPopupContainerEditVoltageFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditVoltageFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditVoltageFilter_QueryResultValue);
            // 
            // popupContainerControlVoltageFilter
            // 
            this.popupContainerControlVoltageFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlVoltageFilter.Controls.Add(this.VoltageFilterOK);
            this.popupContainerControlVoltageFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlVoltageFilter.Location = new System.Drawing.Point(240, 31);
            this.popupContainerControlVoltageFilter.Name = "popupContainerControlVoltageFilter";
            this.popupContainerControlVoltageFilter.Size = new System.Drawing.Size(223, 206);
            this.popupContainerControlVoltageFilter.TabIndex = 37;
            // 
            // VoltageFilterOK
            // 
            this.VoltageFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.VoltageFilterOK.Location = new System.Drawing.Point(3, 181);
            this.VoltageFilterOK.Name = "VoltageFilterOK";
            this.VoltageFilterOK.Size = new System.Drawing.Size(30, 22);
            this.VoltageFilterOK.TabIndex = 19;
            this.VoltageFilterOK.Text = "OK";
            this.VoltageFilterOK.Click += new System.EventHandler(this.VoltageFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp07327UTVoltagesFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(217, 176);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07327UTVoltagesFilterListBindingSource
            // 
            this.sp07327UTVoltagesFilterListBindingSource.DataMember = "sp07327_UT_Voltages_Filter_List";
            this.sp07327UTVoltagesFilterListBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription,
            this.colID,
            this.colintOrder});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colintOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Voltage";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 184;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 72;
            // 
            // colintOrder
            // 
            this.colintOrder.Caption = "Order";
            this.colintOrder.FieldName = "intOrder";
            this.colintOrder.Name = "colintOrder";
            this.colintOrder.OptionsColumn.AllowEdit = false;
            this.colintOrder.OptionsColumn.AllowFocus = false;
            this.colintOrder.OptionsColumn.ReadOnly = true;
            // 
            // barEditItemPopupDateFilter
            // 
            this.barEditItemPopupDateFilter.Caption = "Date Filter";
            this.barEditItemPopupDateFilter.Edit = this.repositoryItemPopupContainerEditDateFilter;
            this.barEditItemPopupDateFilter.EditValue = "No Date Filter";
            this.barEditItemPopupDateFilter.EditWidth = 209;
            this.barEditItemPopupDateFilter.Id = 0;
            this.barEditItemPopupDateFilter.Name = "barEditItemPopupDateFilter";
            // 
            // repositoryItemPopupContainerEditDateFilter
            // 
            this.repositoryItemPopupContainerEditDateFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateFilter.Name = "repositoryItemPopupContainerEditDateFilter";
            this.repositoryItemPopupContainerEditDateFilter.PopupControl = this.popupContainerControlFilter;
            this.repositoryItemPopupContainerEditDateFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateFilter_QueryResultValue);
            // 
            // popupContainerControlFilter
            // 
            this.popupContainerControlFilter.Controls.Add(this.btnOK1);
            this.popupContainerControlFilter.Controls.Add(this.groupControl1);
            this.popupContainerControlFilter.Location = new System.Drawing.Point(38, 9);
            this.popupContainerControlFilter.Name = "popupContainerControlFilter";
            this.popupContainerControlFilter.Size = new System.Drawing.Size(183, 134);
            this.popupContainerControlFilter.TabIndex = 18;
            // 
            // btnOK1
            // 
            this.btnOK1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK1.Location = new System.Drawing.Point(3, 108);
            this.btnOK1.Name = "btnOK1";
            this.btnOK1.Size = new System.Drawing.Size(30, 23);
            this.btnOK1.TabIndex = 4;
            this.btnOK1.Text = "Ok";
            this.btnOK1.Click += new System.EventHandler(this.btnOK1_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.spinEditYear);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.dateEditTo);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFrom);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(177, 103);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Date Filter";
            // 
            // spinEditYear
            // 
            this.spinEditYear.EditValue = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.spinEditYear.Location = new System.Drawing.Point(40, 77);
            this.spinEditYear.MenuManager = this.barManager1;
            this.spinEditYear.Name = "spinEditYear";
            this.spinEditYear.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditYear.Properties.IsFloatValue = false;
            this.spinEditYear.Properties.Mask.EditMask = "f0";
            this.spinEditYear.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEditYear.Properties.MaxValue = new decimal(new int[] {
            2200,
            0,
            0,
            0});
            this.spinEditYear.Properties.MinValue = new decimal(new int[] {
            2010,
            0,
            0,
            0});
            this.spinEditYear.Size = new System.Drawing.Size(52, 20);
            this.spinEditYear.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(8, 80);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(26, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Year:";
            // 
            // dateEditTo
            // 
            this.dateEditTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(40, 51);
            this.dateEditTo.MenuManager = this.barManager1;
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Properties.Mask.EditMask = "g";
            this.dateEditTo.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditTo.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditTo.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditTo.Size = new System.Drawing.Size(132, 20);
            this.dateEditTo.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 54);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 28);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(40, 25);
            this.dateEditFrom.MenuManager = this.barManager1;
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Properties.Mask.EditMask = "g";
            this.dateEditFrom.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFrom.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFrom.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFrom.Size = new System.Drawing.Size(132, 20);
            this.dateEditFrom.TabIndex = 0;
            // 
            // barEditItemPopupSurveyorFilter
            // 
            this.barEditItemPopupSurveyorFilter.Caption = "Surveyor Filter";
            this.barEditItemPopupSurveyorFilter.Edit = this.repositoryItemPopupContainerEditSurveyorFilter;
            this.barEditItemPopupSurveyorFilter.EditValue = "No Surveyor Filter";
            this.barEditItemPopupSurveyorFilter.EditWidth = 126;
            this.barEditItemPopupSurveyorFilter.Id = 5;
            this.barEditItemPopupSurveyorFilter.Name = "barEditItemPopupSurveyorFilter";
            // 
            // repositoryItemPopupContainerEditSurveyorFilter
            // 
            this.repositoryItemPopupContainerEditSurveyorFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditSurveyorFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditSurveyorFilter.Name = "repositoryItemPopupContainerEditSurveyorFilter";
            this.repositoryItemPopupContainerEditSurveyorFilter.PopupControl = this.popupContainerControlSurveyorFilter;
            this.repositoryItemPopupContainerEditSurveyorFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditSurveyorFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditSurveyorFilter_QueryResultValue);
            // 
            // popupContainerControlSurveyorFilter
            // 
            this.popupContainerControlSurveyorFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlSurveyorFilter.Controls.Add(this.SurveyorFilterOK);
            this.popupContainerControlSurveyorFilter.Controls.Add(this.gridControl1);
            this.popupContainerControlSurveyorFilter.Location = new System.Drawing.Point(202, 31);
            this.popupContainerControlSurveyorFilter.Name = "popupContainerControlSurveyorFilter";
            this.popupContainerControlSurveyorFilter.Size = new System.Drawing.Size(222, 275);
            this.popupContainerControlSurveyorFilter.TabIndex = 38;
            // 
            // SurveyorFilterOK
            // 
            this.SurveyorFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SurveyorFilterOK.Location = new System.Drawing.Point(3, 250);
            this.SurveyorFilterOK.Name = "SurveyorFilterOK";
            this.SurveyorFilterOK.Size = new System.Drawing.Size(30, 22);
            this.SurveyorFilterOK.TabIndex = 19;
            this.SurveyorFilterOK.Text = "OK";
            this.SurveyorFilterOK.Click += new System.EventHandler(this.SurveyorFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp07329UTSurveyorListNoBlankBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(217, 245);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07329UTSurveyorListNoBlankBindingSource
            // 
            this.sp07329UTSurveyorListNoBlankBindingSource.DataMember = "sp07329_UT_Surveyor_List_No_Blank";
            this.sp07329UTSurveyorListNoBlankBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDisplayName,
            this.colStaffID,
            this.colForename,
            this.colSurname,
            this.colStaffName,
            this.colActive});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDisplayName
            // 
            this.colDisplayName.Caption = "Surveyor Surname: Forename";
            this.colDisplayName.FieldName = "DisplayName";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.OptionsColumn.AllowEdit = false;
            this.colDisplayName.OptionsColumn.AllowFocus = false;
            this.colDisplayName.OptionsColumn.ReadOnly = true;
            this.colDisplayName.Visible = true;
            this.colDisplayName.VisibleIndex = 0;
            this.colDisplayName.Width = 183;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Surveyor ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 80;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Width = 97;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Width = 109;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 76;
            // 
            // barEditItemPopupTeamFilter
            // 
            this.barEditItemPopupTeamFilter.Caption = "Team Filter";
            this.barEditItemPopupTeamFilter.Edit = this.repositoryItemPopupContainerEditTeamFilter;
            this.barEditItemPopupTeamFilter.EditValue = "No Team Filter";
            this.barEditItemPopupTeamFilter.EditWidth = 118;
            this.barEditItemPopupTeamFilter.Id = 6;
            this.barEditItemPopupTeamFilter.Name = "barEditItemPopupTeamFilter";
            // 
            // repositoryItemPopupContainerEditTeamFilter
            // 
            this.repositoryItemPopupContainerEditTeamFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditTeamFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditTeamFilter.Name = "repositoryItemPopupContainerEditTeamFilter";
            this.repositoryItemPopupContainerEditTeamFilter.PopupControl = this.popupContainerControlTeamFilter;
            this.repositoryItemPopupContainerEditTeamFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditTeamFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditTeamFilter_QueryResultValue);
            // 
            // popupContainerControlTeamFilter
            // 
            this.popupContainerControlTeamFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlTeamFilter.Controls.Add(this.TeamFilterOK);
            this.popupContainerControlTeamFilter.Controls.Add(this.gridControl3);
            this.popupContainerControlTeamFilter.Location = new System.Drawing.Point(3, 31);
            this.popupContainerControlTeamFilter.Name = "popupContainerControlTeamFilter";
            this.popupContainerControlTeamFilter.Size = new System.Drawing.Size(231, 252);
            this.popupContainerControlTeamFilter.TabIndex = 39;
            // 
            // TeamFilterOK
            // 
            this.TeamFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TeamFilterOK.Location = new System.Drawing.Point(3, 227);
            this.TeamFilterOK.Name = "TeamFilterOK";
            this.TeamFilterOK.Size = new System.Drawing.Size(30, 22);
            this.TeamFilterOK.TabIndex = 19;
            this.TeamFilterOK.Text = "OK";
            this.TeamFilterOK.Click += new System.EventHandler(this.TeamFilterOK_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp07330UTTeamListNoBlankBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(3, 3);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.gridControl3.Size = new System.Drawing.Size(225, 222);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07330UTTeamListNoBlankBindingSource
            // 
            this.sp07330UTTeamListNoBlankBindingSource.DataMember = "sp07330_UT_Team_List_No_Blank";
            this.sp07330UTTeamListNoBlankBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTeamID,
            this.colTeamName,
            this.colInternalTeam,
            this.colActive1});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colActive1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTeamID
            // 
            this.colTeamID.Caption = "Team ID";
            this.colTeamID.FieldName = "TeamID";
            this.colTeamID.Name = "colTeamID";
            this.colTeamID.OptionsColumn.AllowEdit = false;
            this.colTeamID.OptionsColumn.AllowFocus = false;
            this.colTeamID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 0;
            this.colTeamName.Width = 195;
            // 
            // colInternalTeam
            // 
            this.colInternalTeam.Caption = "Internal Team";
            this.colInternalTeam.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalTeam.FieldName = "InternalTeam";
            this.colInternalTeam.Name = "colInternalTeam";
            this.colInternalTeam.OptionsColumn.AllowEdit = false;
            this.colInternalTeam.OptionsColumn.AllowFocus = false;
            this.colInternalTeam.OptionsColumn.ReadOnly = true;
            this.colInternalTeam.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInternalTeam.Visible = true;
            this.colInternalTeam.VisibleIndex = 1;
            this.colInternalTeam.Width = 89;
            // 
            // barEditItemSurveyorPerformance
            // 
            this.barEditItemSurveyorPerformance.Caption = "Surveyor Perfomance Setup";
            this.barEditItemSurveyorPerformance.Edit = this.repositoryItemPopupContainerEditSurveyorPerformance;
            this.barEditItemSurveyorPerformance.EditValue = "Spans Surveyed Total By Date, Spans Surveyed Total Accumulated By Date";
            this.barEditItemSurveyorPerformance.EditWidth = 128;
            this.barEditItemSurveyorPerformance.Id = 10;
            this.barEditItemSurveyorPerformance.Name = "barEditItemSurveyorPerformance";
            // 
            // repositoryItemPopupContainerEditSurveyorPerformance
            // 
            this.repositoryItemPopupContainerEditSurveyorPerformance.AutoHeight = false;
            this.repositoryItemPopupContainerEditSurveyorPerformance.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditSurveyorPerformance.Name = "repositoryItemPopupContainerEditSurveyorPerformance";
            this.repositoryItemPopupContainerEditSurveyorPerformance.PopupControl = this.popupContainerControlSurveyorPerformance;
            this.repositoryItemPopupContainerEditSurveyorPerformance.PopupSizeable = false;
            this.repositoryItemPopupContainerEditSurveyorPerformance.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditSurveyorPerformance.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditSurveyorPerformance_QueryResultValue);
            // 
            // popupContainerControlSurveyorPerformance
            // 
            this.popupContainerControlSurveyorPerformance.Controls.Add(this.btnOk_5);
            this.popupContainerControlSurveyorPerformance.Controls.Add(this.groupControl9);
            this.popupContainerControlSurveyorPerformance.Controls.Add(this.groupControl10);
            this.popupContainerControlSurveyorPerformance.Location = new System.Drawing.Point(679, 17);
            this.popupContainerControlSurveyorPerformance.Name = "popupContainerControlSurveyorPerformance";
            this.popupContainerControlSurveyorPerformance.Size = new System.Drawing.Size(297, 185);
            this.popupContainerControlSurveyorPerformance.TabIndex = 21;
            // 
            // btnOk_5
            // 
            this.btnOk_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk_5.Location = new System.Drawing.Point(3, 159);
            this.btnOk_5.Name = "btnOk_5";
            this.btnOk_5.Size = new System.Drawing.Size(30, 23);
            this.btnOk_5.TabIndex = 4;
            this.btnOk_5.Text = "Ok";
            this.btnOk_5.Click += new System.EventHandler(this.btnOk_5_Click);
            // 
            // groupControl9
            // 
            this.groupControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl9.Controls.Add(this.labelControl6);
            this.groupControl9.Controls.Add(this.labelControl7);
            this.groupControl9.Controls.Add(this.comboBoxEditDateDivision2);
            this.groupControl9.Controls.Add(this.comboBoxEditMetric2);
            this.groupControl9.Location = new System.Drawing.Point(2, 81);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(292, 76);
            this.groupControl9.TabIndex = 6;
            this.groupControl9.Text = "Surveyor Performance Chart 2";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(6, 53);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(49, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Timespan:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(6, 27);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(77, 13);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Metric Graphed:";
            // 
            // comboBoxEditDateDivision2
            // 
            this.comboBoxEditDateDivision2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditDateDivision2.EditValue = "Quarter";
            this.comboBoxEditDateDivision2.Location = new System.Drawing.Point(89, 50);
            this.comboBoxEditDateDivision2.MenuManager = this.barManager1;
            this.comboBoxEditDateDivision2.Name = "comboBoxEditDateDivision2";
            this.comboBoxEditDateDivision2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDateDivision2.Properties.Items.AddRange(new object[] {
            "Date",
            "Week",
            "Month",
            "Quarter"});
            this.comboBoxEditDateDivision2.Size = new System.Drawing.Size(198, 20);
            this.comboBoxEditDateDivision2.TabIndex = 1;
            // 
            // comboBoxEditMetric2
            // 
            this.comboBoxEditMetric2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditMetric2.EditValue = "Spans Surveyed Total Accumulated";
            this.comboBoxEditMetric2.Location = new System.Drawing.Point(89, 24);
            this.comboBoxEditMetric2.MenuManager = this.barManager1;
            this.comboBoxEditMetric2.Name = "comboBoxEditMetric2";
            this.comboBoxEditMetric2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMetric2.Properties.Items.AddRange(new object[] {
            "Spans Surveyed Total",
            "Spans Surveyed Total Accumulated",
            "Spans Permissioned Total",
            "Spans Permissioned Total Accumulated",
            "Spans Completed Total",
            "Spans Completed Total Accumulated",
            "Spans Invoiced Total",
            "Spans Invoiced Total Accumulated",
            "Spans Not Invoiced Total",
            "Spans Not Invoiced Total Accumulated"});
            this.comboBoxEditMetric2.Size = new System.Drawing.Size(198, 20);
            this.comboBoxEditMetric2.TabIndex = 0;
            // 
            // groupControl10
            // 
            this.groupControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl10.Controls.Add(this.labelControl5);
            this.groupControl10.Controls.Add(this.labelControl4);
            this.groupControl10.Controls.Add(this.comboBoxEditDateDivision1);
            this.groupControl10.Controls.Add(this.comboBoxEditMetric1);
            this.groupControl10.Location = new System.Drawing.Point(3, 3);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(292, 76);
            this.groupControl10.TabIndex = 5;
            this.groupControl10.Text = "Surveyor Performance Chart 1";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(6, 53);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(49, 13);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "Timespan:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 27);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(77, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Metric Graphed:";
            // 
            // comboBoxEditDateDivision1
            // 
            this.comboBoxEditDateDivision1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditDateDivision1.EditValue = "Quarter";
            this.comboBoxEditDateDivision1.Location = new System.Drawing.Point(89, 50);
            this.comboBoxEditDateDivision1.MenuManager = this.barManager1;
            this.comboBoxEditDateDivision1.Name = "comboBoxEditDateDivision1";
            this.comboBoxEditDateDivision1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditDateDivision1.Properties.Items.AddRange(new object[] {
            "Date",
            "Week",
            "Month",
            "Quarter"});
            this.comboBoxEditDateDivision1.Size = new System.Drawing.Size(198, 20);
            this.comboBoxEditDateDivision1.TabIndex = 1;
            // 
            // comboBoxEditMetric1
            // 
            this.comboBoxEditMetric1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxEditMetric1.EditValue = "Spans Surveyed Total";
            this.comboBoxEditMetric1.Location = new System.Drawing.Point(89, 24);
            this.comboBoxEditMetric1.MenuManager = this.barManager1;
            this.comboBoxEditMetric1.Name = "comboBoxEditMetric1";
            this.comboBoxEditMetric1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMetric1.Properties.Items.AddRange(new object[] {
            "Spans Surveyed Total",
            "Spans Surveyed Total Accumulated",
            "Spans Permissioned Total",
            "Spans Permissioned Total Accumulated",
            "Spans Completed Total",
            "Spans Completed Total Accumulated",
            "Spans Invoiced Total",
            "Spans Invoiced Total Accumulated",
            "Spans Not Invoiced Total",
            "Spans Not Invoiced Total Accumulated"});
            this.comboBoxEditMetric1.Size = new System.Drawing.Size(198, 20);
            this.comboBoxEditMetric1.TabIndex = 0;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 1;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1113, 34);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 552);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1113, 22);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 34);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 518);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1113, 34);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 518);
            // 
            // repositoryItemPopupContainerEditChartSetup
            // 
            this.repositoryItemPopupContainerEditChartSetup.AutoHeight = false;
            this.repositoryItemPopupContainerEditChartSetup.Name = "repositoryItemPopupContainerEditChartSetup";
            this.repositoryItemPopupContainerEditChartSetup.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditChartSetup.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // chartControl1
            // 
            this.chartControl1.DataAdapter = this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter;
            this.chartControl1.DataSource = this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource;
            xyDiagram3.AxisX.Label.Angle = 315;
            xyDiagram3.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram3.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram3.EnableAxisXScrolling = true;
            xyDiagram3.EnableAxisXZooming = true;
            xyDiagram3.EnableAxisYScrolling = true;
            xyDiagram3.EnableAxisYZooming = true;
            this.chartControl1.Diagram = xyDiagram3;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl1.EmptyChartText.Text = "No Data To Chart";
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Legend.EquallySpacedItems = false;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.RuntimeHitTesting = true;
            this.chartControl1.SelectionMode = DevExpress.XtraCharts.ElementSelectionMode.Single;
            this.chartControl1.SeriesDataMember = "SurveyedSpanCount";
            this.chartControl1.SeriesNameTemplate.BeginText = "Spans Surveyed: ";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.SeriesTemplate.ArgumentDataMember = "SurveyorName";
            this.chartControl1.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl1.SeriesTemplate.LegendTextPattern = "{A}: {V:G}";
            this.chartControl1.SeriesTemplate.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl1.SeriesTemplate.ValueDataMembersSerializable = "SurveyedSpanCount";
            this.chartControl1.SeriesTemplate.View = sideBySideStackedBarSeriesView1;
            this.chartControl1.Size = new System.Drawing.Size(427, 331);
            this.chartControl1.TabIndex = 0;
            chartTitle3.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle3.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle3.Indent = 0;
            chartTitle3.Text = "Spans Surveyed By Surveyor";
            this.chartControl1.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle3});
            this.chartControl1.ObjectSelected += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl1_ObjectSelected);
            this.chartControl1.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl1_ObjectHotTracked);
            // 
            // sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter
            // 
            this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter.ClearBeforeFill = true;
            // 
            // chartControl3
            // 
            this.chartControl3.DataAdapter = this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter;
            this.chartControl3.DataSource = this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed;
            simpleDiagram3D1.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            simpleDiagram3D1.RuntimeRotation = true;
            simpleDiagram3D1.RuntimeZooming = true;
            this.chartControl3.Diagram = simpleDiagram3D1;
            this.chartControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl3.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl3.EmptyChartText.Text = "No Data To Chart";
            this.chartControl3.Legend.Name = "Default Legend";
            this.chartControl3.Location = new System.Drawing.Point(0, 0);
            this.chartControl3.Name = "chartControl3";
            series1.ArgumentDataMember = "SurveyorName";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series1.DataSource = this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource;
            pie3DSeriesLabel1.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel1.TextPattern = "{VP:P0}";
            series1.Label = pie3DSeriesLabel1;
            series1.LegendTextPattern = "{A}: {VP:P0}";
            series1.Name = "Series 1";
            series1.ValueDataMembersSerializable = "SurveyedSpanCount";
            series1.View = pie3DSeriesView1;
            this.chartControl3.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1};
            this.chartControl3.SeriesSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl3.SeriesTemplate.ArgumentDataMember = "SurveyorName";
            this.chartControl3.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            pie3DSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel2.TextPattern = "{VP:G4}";
            this.chartControl3.SeriesTemplate.Label = pie3DSeriesLabel2;
            this.chartControl3.SeriesTemplate.View = pie3DSeriesView2;
            this.chartControl3.Size = new System.Drawing.Size(427, 150);
            this.chartControl3.TabIndex = 0;
            chartTitle4.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle4.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle4.Indent = 0;
            chartTitle4.Text = "Spans Surveyed Breakdown by Surveyor";
            this.chartControl3.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle4});
            this.chartControl3.CustomDrawSeriesPoint += new DevExpress.XtraCharts.CustomDrawSeriesPointEventHandler(this.chartControl3_CustomDrawSeriesPoint);
            // 
            // chartControl2
            // 
            this.chartControl2.DataAdapter = this.sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter;
            this.chartControl2.DataSource = this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource;
            xyDiagram4.AxisX.Label.Angle = 315;
            xyDiagram4.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram4.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl2.Diagram = xyDiagram4;
            this.chartControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl2.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl2.EmptyChartText.Text = "No Data To Chart";
            this.chartControl2.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl2.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl2.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl2.Legend.EquallySpacedItems = false;
            this.chartControl2.Legend.Name = "Default Legend";
            this.chartControl2.Location = new System.Drawing.Point(0, 0);
            this.chartControl2.Name = "chartControl2";
            this.chartControl2.RuntimeHitTesting = true;
            this.chartControl2.SelectionMode = DevExpress.XtraCharts.ElementSelectionMode.Single;
            this.chartControl2.SeriesNameTemplate.BeginText = "Spans Surveyed: ";
            series2.Name = "Series 1";
            sideBySideStackedBarSeriesView2.StackedGroupSerializable = "Group 1";
            series2.View = sideBySideStackedBarSeriesView2;
            series3.Name = "Series 2";
            sideBySideStackedBarSeriesView3.StackedGroupSerializable = "Group 1";
            series3.View = sideBySideStackedBarSeriesView3;
            series4.Name = "Series 3";
            sideBySideStackedBarSeriesView4.StackedGroupSerializable = "Group 2";
            series4.View = sideBySideStackedBarSeriesView4;
            series5.Name = "Series 4";
            sideBySideStackedBarSeriesView5.StackedGroupSerializable = "Group 2";
            series5.View = sideBySideStackedBarSeriesView5;
            this.chartControl2.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2,
        series3,
        series4,
        series5};
            this.chartControl2.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl2.SeriesTemplate.LegendTextPattern = "{A:G}: {V:G}";
            this.chartControl2.SeriesTemplate.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl2.SeriesTemplate.View = sideBySideStackedBarSeriesView6;
            this.chartControl2.Size = new System.Drawing.Size(463, 330);
            this.chartControl2.TabIndex = 1;
            chartTitle5.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle5.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle5.Indent = 0;
            chartTitle5.Text = "Jobs By Team";
            this.chartControl2.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle5});
            this.chartControl2.ObjectSelected += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl2_ObjectSelected);
            this.chartControl2.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl2_ObjectHotTracked);
            // 
            // sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter
            // 
            this.sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter.ClearBeforeFill = true;
            // 
            // chartControl4
            // 
            this.chartControl4.DataAdapter = this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter;
            this.chartControl4.DataSource = this.dataSet_UT_Reporting.sp07319_UT_Reporting_Surveyors_by_Spans_Surveyed;
            simpleDiagram3D2.RotationMatrixSerializable = "1;0;0;0;0;0.5;-0.866025403784439;0;0;0.866025403784439;0.5;0;0;0;0;1";
            simpleDiagram3D2.RuntimeRotation = true;
            simpleDiagram3D2.RuntimeZooming = true;
            this.chartControl4.Diagram = simpleDiagram3D2;
            this.chartControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl4.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl4.EmptyChartText.Text = "No Data To Chart";
            this.chartControl4.Legend.Name = "Default Legend";
            this.chartControl4.Location = new System.Drawing.Point(0, 0);
            this.chartControl4.Name = "chartControl4";
            series6.ArgumentDataMember = "TeamName";
            series6.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            series6.DataSource = this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource;
            pie3DSeriesLabel3.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel3.TextPattern = "{VP:P0}";
            series6.Label = pie3DSeriesLabel3;
            series6.LegendTextPattern = "{A}: {VP:P0}";
            series6.Name = "Series 1";
            series6.ValueDataMembersSerializable = "WorkedOnSpanCount";
            series6.View = pie3DSeriesView3;
            this.chartControl4.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series6};
            this.chartControl4.SeriesSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl4.SeriesTemplate.ArgumentDataMember = "TeamName";
            this.chartControl4.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            pie3DSeriesLabel4.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            pie3DSeriesLabel4.TextPattern = "{VP:G4}";
            this.chartControl4.SeriesTemplate.Label = pie3DSeriesLabel4;
            this.chartControl4.SeriesTemplate.ValueDataMembersSerializable = "WorkedOnSpanCount";
            this.chartControl4.SeriesTemplate.View = pie3DSeriesView4;
            this.chartControl4.Size = new System.Drawing.Size(463, 151);
            this.chartControl4.TabIndex = 19;
            chartTitle6.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle6.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle6.Indent = 0;
            chartTitle6.Text = "Job Breakdown By Team";
            this.chartControl4.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle6});
            // 
            // layoutControl1
            // 
            this.layoutControl1.AllowCustomization = false;
            this.layoutControl1.Controls.Add(this.dateEditGroupingEndDate);
            this.layoutControl1.Controls.Add(this.spinEditDaysGrouping);
            this.layoutControl1.Controls.Add(this.dateEditYTDStart);
            this.layoutControl1.Controls.Add(this.gridControl5);
            this.layoutControl1.Controls.Add(this.splitContainerControl2);
            this.layoutControl1.Controls.Add(this.splitContainerControl4);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 34);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(253, 312, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(1113, 518);
            this.layoutControl1.TabIndex = 13;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateEditGroupingEndDate
            // 
            this.dateEditGroupingEndDate.EditValue = null;
            this.dateEditGroupingEndDate.Location = new System.Drawing.Point(345, 56);
            this.dateEditGroupingEndDate.MenuManager = this.barManager1;
            this.dateEditGroupingEndDate.Name = "dateEditGroupingEndDate";
            this.dateEditGroupingEndDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditGroupingEndDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditGroupingEndDate.Properties.Mask.EditMask = "g";
            this.dateEditGroupingEndDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditGroupingEndDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditGroupingEndDate.Properties.MinValue = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            this.dateEditGroupingEndDate.Size = new System.Drawing.Size(120, 20);
            this.dateEditGroupingEndDate.StyleController = this.layoutControl1;
            this.dateEditGroupingEndDate.TabIndex = 23;
            // 
            // spinEditDaysGrouping
            // 
            this.spinEditDaysGrouping.EditValue = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.spinEditDaysGrouping.Location = new System.Drawing.Point(566, 56);
            this.spinEditDaysGrouping.MenuManager = this.barManager1;
            this.spinEditDaysGrouping.Name = "spinEditDaysGrouping";
            this.spinEditDaysGrouping.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditDaysGrouping.Properties.IsFloatValue = false;
            this.spinEditDaysGrouping.Properties.Mask.EditMask = "N00";
            this.spinEditDaysGrouping.Properties.MaxValue = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.spinEditDaysGrouping.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditDaysGrouping.Size = new System.Drawing.Size(75, 20);
            this.spinEditDaysGrouping.StyleController = this.layoutControl1;
            this.spinEditDaysGrouping.TabIndex = 22;
            this.spinEditDaysGrouping.EditValueChanged += new System.EventHandler(this.spinEditDaysGrouping_EditValueChanged);
            // 
            // dateEditYTDStart
            // 
            this.dateEditYTDStart.EditValue = null;
            this.dateEditYTDStart.Location = new System.Drawing.Point(110, 56);
            this.dateEditYTDStart.MenuManager = this.barManager1;
            this.dateEditYTDStart.Name = "dateEditYTDStart";
            this.dateEditYTDStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditYTDStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditYTDStart.Properties.Mask.EditMask = "g";
            this.dateEditYTDStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditYTDStart.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditYTDStart.Properties.MinValue = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            this.dateEditYTDStart.Size = new System.Drawing.Size(134, 20);
            this.dateEditYTDStart.StyleController = this.layoutControl1;
            this.dateEditYTDStart.TabIndex = 21;
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07349UTReportingSurveyormetrics2BindingSource;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.Location = new System.Drawing.Point(5, 88);
            this.gridControl5.MainView = this.bandedGridView1;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditInteger,
            this.repositoryItemTextEditPercentage2});
            this.gridControl5.Size = new System.Drawing.Size(1103, 425);
            this.gridControl5.TabIndex = 18;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.bandedGridView1});
            // 
            // sp07349UTReportingSurveyormetrics2BindingSource
            // 
            this.sp07349UTReportingSurveyormetrics2BindingSource.DataMember = "sp07349_UT_Reporting_Surveyor_metrics_2";
            this.sp07349UTReportingSurveyormetrics2BindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // bandedGridView1
            // 
            this.bandedGridView1.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand2,
            this.gridBand3,
            this.gridBand5,
            this.gridBand4,
            this.gridBand8,
            this.gridBand6,
            this.gridBand7,
            this.gridBand1});
            this.bandedGridView1.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colStaffID2,
            this.colForename2,
            this.colSurname2,
            this.colSurveyorName1,
            this.colSurveyedSpanCount1,
            this.colWeekSurveyedSpanCount,
            this.colAverageInfestation1,
            this.colWeekAverageInfestation,
            this.colCompletedSpanCount1,
            this.colWeekCompletedSpanCount,
            this.colPermissionedSpanCount1,
            this.colWeekPermissionedSpanCount,
            this.colSpansNeedingWork1,
            this.colWeekSpansNeedingWork,
            this.colSurveyedSpanClear1,
            this.colWeekSurveyedSpanClear,
            this.colReactiveSpanCompleteCount,
            this.colWeekReactiveSpanCompleteCount});
            this.bandedGridView1.GridControl = this.gridControl5;
            this.bandedGridView1.Name = "bandedGridView1";
            this.bandedGridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.bandedGridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.bandedGridView1.OptionsLayout.StoreAppearance = true;
            this.bandedGridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.bandedGridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.bandedGridView1.OptionsSelection.MultiSelect = true;
            this.bandedGridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.bandedGridView1.OptionsView.ColumnAutoWidth = false;
            this.bandedGridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.bandedGridView1.OptionsView.ShowFooter = true;
            this.bandedGridView1.OptionsView.ShowGroupPanel = false;
            this.bandedGridView1.OptionsView.ShowIndicator = false;
            this.bandedGridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCompletedSpanCount1, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridBand2
            // 
            this.gridBand2.Columns.Add(this.colStaffID2);
            this.gridBand2.Columns.Add(this.colForename2);
            this.gridBand2.Columns.Add(this.colSurname2);
            this.gridBand2.Columns.Add(this.colSurveyorName1);
            this.gridBand2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 0;
            this.gridBand2.Width = 157;
            // 
            // colStaffID2
            // 
            this.colStaffID2.Caption = "Staff ID";
            this.colStaffID2.FieldName = "StaffID";
            this.colStaffID2.Name = "colStaffID2";
            this.colStaffID2.OptionsColumn.AllowEdit = false;
            this.colStaffID2.OptionsColumn.AllowFocus = false;
            this.colStaffID2.OptionsColumn.ReadOnly = true;
            this.colStaffID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colForename2
            // 
            this.colForename2.Caption = "Forename";
            this.colForename2.FieldName = "Forename";
            this.colForename2.Name = "colForename2";
            this.colForename2.OptionsColumn.AllowEdit = false;
            this.colForename2.OptionsColumn.AllowFocus = false;
            this.colForename2.OptionsColumn.ReadOnly = true;
            this.colForename2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSurname2
            // 
            this.colSurname2.Caption = "Surname";
            this.colSurname2.FieldName = "Surname";
            this.colSurname2.Name = "colSurname2";
            this.colSurname2.OptionsColumn.AllowEdit = false;
            this.colSurname2.OptionsColumn.AllowFocus = false;
            this.colSurname2.OptionsColumn.ReadOnly = true;
            this.colSurname2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSurveyorName1
            // 
            this.colSurveyorName1.Caption = "Surveyor Name";
            this.colSurveyorName1.FieldName = "SurveyorName";
            this.colSurveyorName1.Name = "colSurveyorName1";
            this.colSurveyorName1.OptionsColumn.AllowEdit = false;
            this.colSurveyorName1.OptionsColumn.AllowFocus = false;
            this.colSurveyorName1.OptionsColumn.ReadOnly = true;
            this.colSurveyorName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyorName1.Visible = true;
            this.colSurveyorName1.Width = 157;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Surveyed Spans";
            this.gridBand3.Columns.Add(this.colWeekSurveyedSpanCount);
            this.gridBand3.Columns.Add(this.colSurveyedSpanCount1);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 1;
            this.gridBand3.Width = 161;
            // 
            // colWeekSurveyedSpanCount
            // 
            this.colWeekSurveyedSpanCount.Caption = "X Days";
            this.colWeekSurveyedSpanCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekSurveyedSpanCount.CustomizationCaption = "X Days Surveyed Spans";
            this.colWeekSurveyedSpanCount.FieldName = "WeekSurveyedSpanCount";
            this.colWeekSurveyedSpanCount.Name = "colWeekSurveyedSpanCount";
            this.colWeekSurveyedSpanCount.OptionsColumn.AllowEdit = false;
            this.colWeekSurveyedSpanCount.OptionsColumn.AllowFocus = false;
            this.colWeekSurveyedSpanCount.OptionsColumn.ReadOnly = true;
            this.colWeekSurveyedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekSurveyedSpanCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekSurveyedSpanCount.Visible = true;
            this.colWeekSurveyedSpanCount.Width = 81;
            // 
            // repositoryItemTextEditInteger
            // 
            this.repositoryItemTextEditInteger.AutoHeight = false;
            this.repositoryItemTextEditInteger.Mask.EditMask = "f0";
            this.repositoryItemTextEditInteger.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger.Name = "repositoryItemTextEditInteger";
            // 
            // colSurveyedSpanCount1
            // 
            this.colSurveyedSpanCount1.Caption = "YTD";
            this.colSurveyedSpanCount1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colSurveyedSpanCount1.CustomizationCaption = "YTD Surveyed Spans";
            this.colSurveyedSpanCount1.FieldName = "SurveyedSpanCount";
            this.colSurveyedSpanCount1.Name = "colSurveyedSpanCount1";
            this.colSurveyedSpanCount1.OptionsColumn.AllowEdit = false;
            this.colSurveyedSpanCount1.OptionsColumn.AllowFocus = false;
            this.colSurveyedSpanCount1.OptionsColumn.ReadOnly = true;
            this.colSurveyedSpanCount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedSpanCount1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colSurveyedSpanCount1.Visible = true;
            this.colSurveyedSpanCount1.Width = 80;
            // 
            // gridBand5
            // 
            this.gridBand5.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand5.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand5.Caption = "Permissioned Spans";
            this.gridBand5.Columns.Add(this.colWeekPermissionedSpanCount);
            this.gridBand5.Columns.Add(this.colPermissionedSpanCount1);
            this.gridBand5.Name = "gridBand5";
            this.gridBand5.VisibleIndex = 2;
            this.gridBand5.Width = 161;
            // 
            // colWeekPermissionedSpanCount
            // 
            this.colWeekPermissionedSpanCount.Caption = "X Days";
            this.colWeekPermissionedSpanCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekPermissionedSpanCount.CustomizationCaption = "X Days Permissioned Spans";
            this.colWeekPermissionedSpanCount.FieldName = "WeekPermissionedSpanCount";
            this.colWeekPermissionedSpanCount.Name = "colWeekPermissionedSpanCount";
            this.colWeekPermissionedSpanCount.OptionsColumn.AllowEdit = false;
            this.colWeekPermissionedSpanCount.OptionsColumn.AllowFocus = false;
            this.colWeekPermissionedSpanCount.OptionsColumn.ReadOnly = true;
            this.colWeekPermissionedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekPermissionedSpanCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekPermissionedSpanCount.Visible = true;
            this.colWeekPermissionedSpanCount.Width = 81;
            // 
            // colPermissionedSpanCount1
            // 
            this.colPermissionedSpanCount1.Caption = "YTD";
            this.colPermissionedSpanCount1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colPermissionedSpanCount1.CustomizationCaption = "YTD Permissioned Spans";
            this.colPermissionedSpanCount1.FieldName = "PermissionedSpanCount";
            this.colPermissionedSpanCount1.Name = "colPermissionedSpanCount1";
            this.colPermissionedSpanCount1.OptionsColumn.AllowEdit = false;
            this.colPermissionedSpanCount1.OptionsColumn.AllowFocus = false;
            this.colPermissionedSpanCount1.OptionsColumn.ReadOnly = true;
            this.colPermissionedSpanCount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPermissionedSpanCount1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colPermissionedSpanCount1.Visible = true;
            this.colPermissionedSpanCount1.Width = 80;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "Cut Spans";
            this.gridBand4.Columns.Add(this.colWeekCompletedSpanCount);
            this.gridBand4.Columns.Add(this.colCompletedSpanCount1);
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 3;
            this.gridBand4.Width = 161;
            // 
            // colWeekCompletedSpanCount
            // 
            this.colWeekCompletedSpanCount.Caption = "X Days";
            this.colWeekCompletedSpanCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekCompletedSpanCount.CustomizationCaption = "X Days Cut Spans";
            this.colWeekCompletedSpanCount.FieldName = "WeekCompletedSpanCount";
            this.colWeekCompletedSpanCount.Name = "colWeekCompletedSpanCount";
            this.colWeekCompletedSpanCount.OptionsColumn.AllowEdit = false;
            this.colWeekCompletedSpanCount.OptionsColumn.AllowFocus = false;
            this.colWeekCompletedSpanCount.OptionsColumn.ReadOnly = true;
            this.colWeekCompletedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekCompletedSpanCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekCompletedSpanCount.Visible = true;
            this.colWeekCompletedSpanCount.Width = 83;
            // 
            // colCompletedSpanCount1
            // 
            this.colCompletedSpanCount1.Caption = "YTD";
            this.colCompletedSpanCount1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colCompletedSpanCount1.CustomizationCaption = "YTD Cut Spans";
            this.colCompletedSpanCount1.FieldName = "CompletedSpanCount";
            this.colCompletedSpanCount1.Name = "colCompletedSpanCount1";
            this.colCompletedSpanCount1.OptionsColumn.AllowEdit = false;
            this.colCompletedSpanCount1.OptionsColumn.AllowFocus = false;
            this.colCompletedSpanCount1.OptionsColumn.ReadOnly = true;
            this.colCompletedSpanCount1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedSpanCount1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colCompletedSpanCount1.Visible = true;
            this.colCompletedSpanCount1.Width = 78;
            // 
            // gridBand8
            // 
            this.gridBand8.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand8.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand8.Caption = "Reactive Spans Complete";
            this.gridBand8.Columns.Add(this.colWeekReactiveSpanCompleteCount);
            this.gridBand8.Columns.Add(this.colReactiveSpanCompleteCount);
            this.gridBand8.Name = "gridBand8";
            this.gridBand8.VisibleIndex = 4;
            this.gridBand8.Width = 161;
            // 
            // colWeekReactiveSpanCompleteCount
            // 
            this.colWeekReactiveSpanCompleteCount.Caption = "YTD";
            this.colWeekReactiveSpanCompleteCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekReactiveSpanCompleteCount.CustomizationCaption = "YTD Reactive Spans Complete";
            this.colWeekReactiveSpanCompleteCount.FieldName = "WeekReactiveSpanCompleteCount";
            this.colWeekReactiveSpanCompleteCount.Name = "colWeekReactiveSpanCompleteCount";
            this.colWeekReactiveSpanCompleteCount.OptionsColumn.AllowEdit = false;
            this.colWeekReactiveSpanCompleteCount.OptionsColumn.AllowFocus = false;
            this.colWeekReactiveSpanCompleteCount.OptionsColumn.ReadOnly = true;
            this.colWeekReactiveSpanCompleteCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekReactiveSpanCompleteCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekReactiveSpanCompleteCount.Visible = true;
            this.colWeekReactiveSpanCompleteCount.Width = 80;
            // 
            // colReactiveSpanCompleteCount
            // 
            this.colReactiveSpanCompleteCount.Caption = "X  Days";
            this.colReactiveSpanCompleteCount.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colReactiveSpanCompleteCount.CustomizationCaption = "X Days Reactive Spans Complete";
            this.colReactiveSpanCompleteCount.FieldName = "ReactiveSpanCompleteCount";
            this.colReactiveSpanCompleteCount.Name = "colReactiveSpanCompleteCount";
            this.colReactiveSpanCompleteCount.OptionsColumn.AllowEdit = false;
            this.colReactiveSpanCompleteCount.OptionsColumn.AllowFocus = false;
            this.colReactiveSpanCompleteCount.OptionsColumn.ReadOnly = true;
            this.colReactiveSpanCompleteCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReactiveSpanCompleteCount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colReactiveSpanCompleteCount.Visible = true;
            this.colReactiveSpanCompleteCount.Width = 81;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "Spans Requiring Work";
            this.gridBand6.Columns.Add(this.colWeekSpansNeedingWork);
            this.gridBand6.Columns.Add(this.colSpansNeedingWork1);
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 5;
            this.gridBand6.Width = 161;
            // 
            // colWeekSpansNeedingWork
            // 
            this.colWeekSpansNeedingWork.Caption = "X Days";
            this.colWeekSpansNeedingWork.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekSpansNeedingWork.CustomizationCaption = "X Days Spans Needing Work";
            this.colWeekSpansNeedingWork.FieldName = "WeekSpansNeedingWork";
            this.colWeekSpansNeedingWork.Name = "colWeekSpansNeedingWork";
            this.colWeekSpansNeedingWork.OptionsColumn.AllowEdit = false;
            this.colWeekSpansNeedingWork.OptionsColumn.AllowFocus = false;
            this.colWeekSpansNeedingWork.OptionsColumn.ReadOnly = true;
            this.colWeekSpansNeedingWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekSpansNeedingWork.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekSpansNeedingWork.Visible = true;
            this.colWeekSpansNeedingWork.Width = 82;
            // 
            // colSpansNeedingWork1
            // 
            this.colSpansNeedingWork1.Caption = "YTD";
            this.colSpansNeedingWork1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colSpansNeedingWork1.CustomizationCaption = "YTD Spans Needing Work";
            this.colSpansNeedingWork1.FieldName = "SpansNeedingWork";
            this.colSpansNeedingWork1.Name = "colSpansNeedingWork1";
            this.colSpansNeedingWork1.OptionsColumn.AllowEdit = false;
            this.colSpansNeedingWork1.OptionsColumn.AllowFocus = false;
            this.colSpansNeedingWork1.OptionsColumn.ReadOnly = true;
            this.colSpansNeedingWork1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpansNeedingWork1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colSpansNeedingWork1.Visible = true;
            this.colSpansNeedingWork1.Width = 79;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "Clear Spans";
            this.gridBand7.Columns.Add(this.colWeekSurveyedSpanClear);
            this.gridBand7.Columns.Add(this.colSurveyedSpanClear1);
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 6;
            this.gridBand7.Width = 161;
            // 
            // colWeekSurveyedSpanClear
            // 
            this.colWeekSurveyedSpanClear.Caption = "X Days";
            this.colWeekSurveyedSpanClear.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colWeekSurveyedSpanClear.CustomizationCaption = "X Days Clear Spans";
            this.colWeekSurveyedSpanClear.FieldName = "WeekSurveyedSpanClear";
            this.colWeekSurveyedSpanClear.Name = "colWeekSurveyedSpanClear";
            this.colWeekSurveyedSpanClear.OptionsColumn.AllowEdit = false;
            this.colWeekSurveyedSpanClear.OptionsColumn.AllowFocus = false;
            this.colWeekSurveyedSpanClear.OptionsColumn.ReadOnly = true;
            this.colWeekSurveyedSpanClear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekSurveyedSpanClear.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colWeekSurveyedSpanClear.Visible = true;
            this.colWeekSurveyedSpanClear.Width = 83;
            // 
            // colSurveyedSpanClear1
            // 
            this.colSurveyedSpanClear1.Caption = "YTD";
            this.colSurveyedSpanClear1.ColumnEdit = this.repositoryItemTextEditInteger;
            this.colSurveyedSpanClear1.CustomizationCaption = "YTD Clear Spans";
            this.colSurveyedSpanClear1.FieldName = "SurveyedSpanClear";
            this.colSurveyedSpanClear1.Name = "colSurveyedSpanClear1";
            this.colSurveyedSpanClear1.OptionsColumn.AllowEdit = false;
            this.colSurveyedSpanClear1.OptionsColumn.AllowFocus = false;
            this.colSurveyedSpanClear1.OptionsColumn.ReadOnly = true;
            this.colSurveyedSpanClear1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedSpanClear1.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.colSurveyedSpanClear1.Visible = true;
            this.colSurveyedSpanClear1.Width = 78;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Average Infestation Rate";
            this.gridBand1.Columns.Add(this.colWeekAverageInfestation);
            this.gridBand1.Columns.Add(this.colAverageInfestation1);
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 7;
            this.gridBand1.Width = 161;
            // 
            // colWeekAverageInfestation
            // 
            this.colWeekAverageInfestation.Caption = "X Days";
            this.colWeekAverageInfestation.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colWeekAverageInfestation.CustomizationCaption = "X Days Avg. Infestation";
            this.colWeekAverageInfestation.FieldName = "WeekAverageInfestation";
            this.colWeekAverageInfestation.Name = "colWeekAverageInfestation";
            this.colWeekAverageInfestation.OptionsColumn.AllowEdit = false;
            this.colWeekAverageInfestation.OptionsColumn.AllowFocus = false;
            this.colWeekAverageInfestation.OptionsColumn.ReadOnly = true;
            this.colWeekAverageInfestation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWeekAverageInfestation.Visible = true;
            this.colWeekAverageInfestation.Width = 82;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colAverageInfestation1
            // 
            this.colAverageInfestation1.Caption = "YTD";
            this.colAverageInfestation1.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colAverageInfestation1.CustomizationCaption = "YTD Avg Infestation";
            this.colAverageInfestation1.FieldName = "AverageInfestation";
            this.colAverageInfestation1.Name = "colAverageInfestation1";
            this.colAverageInfestation1.OptionsColumn.AllowEdit = false;
            this.colAverageInfestation1.OptionsColumn.AllowFocus = false;
            this.colAverageInfestation1.OptionsColumn.ReadOnly = true;
            this.colAverageInfestation1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageInfestation1.Visible = true;
            this.colAverageInfestation1.Width = 79;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(5, 26);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.rangeControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1103, 487);
            this.splitContainerControl2.SplitterPosition = 58;
            this.splitContainerControl2.TabIndex = 20;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlSurveyorPerformance);
            this.splitContainerControl1.Panel1.Controls.Add(this.chartControl6);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.chartControl7);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1103, 423);
            this.splitContainerControl1.SplitterPosition = 222;
            this.splitContainerControl1.TabIndex = 19;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // chartControl7
            // 
            this.chartControl7.DataAdapter = this.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter;
            this.chartControl7.DataSource = this.sp07340UTReportingSurveyorMetricsByDate2BindingSource;
            xyDiagram2.AxisX.Label.Angle = 270;
            xyDiagram2.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram2.AxisX.WholeRange.AutoSideMargins = false;
            xyDiagram2.AxisX.WholeRange.SideMarginsValue = 0D;
            xyDiagram2.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram2.EnableAxisXScrolling = true;
            xyDiagram2.EnableAxisXZooming = true;
            this.chartControl7.Diagram = xyDiagram2;
            this.chartControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl7.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl7.EmptyChartText.Text = "No Data To Chart";
            this.chartControl7.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.Center;
            this.chartControl7.Legend.EquallySpacedItems = false;
            this.chartControl7.Legend.Name = "Default Legend";
            this.chartControl7.Location = new System.Drawing.Point(0, 0);
            this.chartControl7.Name = "chartControl7";
            this.chartControl7.RuntimeHitTesting = true;
            this.chartControl7.SelectionMode = DevExpress.XtraCharts.ElementSelectionMode.Single;
            this.chartControl7.SeriesDataMember = "SurveyorName";
            this.chartControl7.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl7.SeriesTemplate.ArgumentDataMember = "DateRange";
            this.chartControl7.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.LineVisibility = DevExpress.Utils.DefaultBoolean.True;
            this.chartControl7.SeriesTemplate.Label = pointSeriesLabel2;
            this.chartControl7.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl7.SeriesTemplate.LegendTextPattern = "{A:d}: {V:G}";
            this.chartControl7.SeriesTemplate.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl7.SeriesTemplate.ValueDataMembersSerializable = "RunningTotal";
            areaSeriesView1.RangeControlOptions.ViewType = DevExpress.XtraCharts.RangeControlViewType.Area;
            this.chartControl7.SeriesTemplate.View = areaSeriesView1;
            this.chartControl7.Size = new System.Drawing.Size(1103, 195);
            this.chartControl7.TabIndex = 19;
            chartTitle2.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle2.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle2.Indent = 0;
            chartTitle2.Text = "Chart 2";
            this.chartControl7.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle2});
            this.chartControl7.ObjectSelected += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl7_ObjectSelected);
            this.chartControl7.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl7_ObjectHotTracked);
            this.chartControl7.Enter += new System.EventHandler(this.chartControl7_Enter);
            // 
            // sp07340UTReportingSurveyorMetricsByDate2BindingSource
            // 
            this.sp07340UTReportingSurveyorMetricsByDate2BindingSource.DataMember = "sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2";
            this.sp07340UTReportingSurveyorMetricsByDate2BindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Location = new System.Drawing.Point(5, 26);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.splitContainerControl6);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.splitContainerControl5);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1103, 487);
            this.splitContainerControl4.SplitterPosition = 427;
            this.splitContainerControl4.TabIndex = 9;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.popupControlContainerChartSetup);
            this.splitContainerControl6.Panel1.Controls.Add(this.popupContainerControlSurveyorFilter);
            this.splitContainerControl6.Panel1.Controls.Add(this.chartControl1);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.chartControl3);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(427, 487);
            this.splitContainerControl6.SplitterPosition = 331;
            this.splitContainerControl6.TabIndex = 0;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.splitContainerControl7);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.splitContainerControl8);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(670, 487);
            this.splitContainerControl5.SplitterPosition = 463;
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // splitContainerControl7
            // 
            this.splitContainerControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl7.Horizontal = false;
            this.splitContainerControl7.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl7.Name = "splitContainerControl7";
            this.splitContainerControl7.Panel1.Controls.Add(this.popupContainerControlVoltageFilter);
            this.splitContainerControl7.Panel1.Controls.Add(this.popupContainerControlTeamFilter);
            this.splitContainerControl7.Panel1.Controls.Add(this.chartControl2);
            this.splitContainerControl7.Panel1.Text = "Panel1";
            this.splitContainerControl7.Panel2.Controls.Add(this.popupContainerControlFilter);
            this.splitContainerControl7.Panel2.Controls.Add(this.chartControl4);
            this.splitContainerControl7.Panel2.Text = "Panel2";
            this.splitContainerControl7.Size = new System.Drawing.Size(463, 487);
            this.splitContainerControl7.SplitterPosition = 330;
            this.splitContainerControl7.TabIndex = 0;
            this.splitContainerControl7.Text = "splitContainerControl7";
            // 
            // splitContainerControl8
            // 
            this.splitContainerControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl8.Horizontal = false;
            this.splitContainerControl8.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl8.Name = "splitContainerControl8";
            this.splitContainerControl8.Panel1.Controls.Add(this.chartControl5);
            this.splitContainerControl8.Panel1.Text = "Panel1";
            this.splitContainerControl8.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl8.Panel2.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl8.Panel2.ShowCaption = true;
            this.splitContainerControl8.Panel2.Text = "Surveyor Metrics";
            this.splitContainerControl8.Size = new System.Drawing.Size(201, 487);
            this.splitContainerControl8.SplitterPosition = 326;
            this.splitContainerControl8.TabIndex = 0;
            this.splitContainerControl8.Text = "splitContainerControl8";
            // 
            // chartControl5
            // 
            this.chartControl5.DataAdapter = this.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter;
            this.chartControl5.DataSource = this.sp07335UTReportingClearSpanTargetVActualBindingSource;
            xyDiagram5.AxisX.Label.Angle = 315;
            xyDiagram5.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram5.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl5.Diagram = xyDiagram5;
            this.chartControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl5.EmptyChartText.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.chartControl5.EmptyChartText.Text = "No Data To Chart";
            this.chartControl5.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Center;
            this.chartControl5.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl5.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl5.Legend.EquallySpacedItems = false;
            this.chartControl5.Legend.Name = "Default Legend";
            this.chartControl5.Location = new System.Drawing.Point(0, 0);
            this.chartControl5.Name = "chartControl5";
            this.chartControl5.RuntimeHitTesting = true;
            this.chartControl5.SelectionMode = DevExpress.XtraCharts.ElementSelectionMode.Single;
            this.chartControl5.SeriesNameTemplate.BeginText = "Spans Surveyed: ";
            series7.Name = "Series 1";
            sideBySideStackedBarSeriesView7.StackedGroupSerializable = "Group 1";
            series7.View = sideBySideStackedBarSeriesView7;
            series8.Name = "Series 2";
            sideBySideStackedBarSeriesView8.StackedGroupSerializable = "Group 1";
            series8.View = sideBySideStackedBarSeriesView8;
            series9.Name = "Series 3";
            sideBySideStackedBarSeriesView9.StackedGroupSerializable = "Group 2";
            series9.View = sideBySideStackedBarSeriesView9;
            series10.Name = "Series 4";
            sideBySideStackedBarSeriesView10.StackedGroupSerializable = "Group 2";
            series10.View = sideBySideStackedBarSeriesView10;
            this.chartControl5.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series7,
        series8,
        series9,
        series10};
            this.chartControl5.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            this.chartControl5.SeriesTemplate.LegendTextPattern = "{A:G}: {V:G}";
            this.chartControl5.SeriesTemplate.SeriesPointsSorting = DevExpress.XtraCharts.SortingMode.Ascending;
            this.chartControl5.SeriesTemplate.View = sideBySideStackedBarSeriesView11;
            this.chartControl5.Size = new System.Drawing.Size(201, 326);
            this.chartControl5.TabIndex = 1;
            chartTitle7.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.False;
            chartTitle7.Font = new System.Drawing.Font("Tahoma", 10F);
            chartTitle7.Indent = 0;
            chartTitle7.Text = "Clear Span Target v Actual";
            this.chartControl5.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle7});
            this.chartControl5.ObjectSelected += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl5_ObjectSelected);
            this.chartControl5.ObjectHotTracked += new DevExpress.XtraCharts.HotTrackEventHandler(this.chartControl5_ObjectHotTracked);
            // 
            // sp07335UTReportingClearSpanTargetVActualBindingSource
            // 
            this.sp07335UTReportingClearSpanTargetVActualBindingSource.DataMember = "sp07335_UT_Reporting_Clear_Span_Target_V_Actual";
            this.sp07335UTReportingClearSpanTargetVActualBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl4;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer1.Size = new System.Drawing.Size(197, 131);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07339UTReportingSurveyormetricsBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditMoney});
            this.gridControl4.Size = new System.Drawing.Size(197, 131);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07339UTReportingSurveyormetricsBindingSource
            // 
            this.sp07339UTReportingSurveyormetricsBindingSource.DataMember = "sp07339_UT_Reporting_Surveyor_metrics";
            this.sp07339UTReportingSurveyormetricsBindingSource.DataSource = this.dataSet_UT_Reporting;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStaffID1,
            this.colForename1,
            this.colSurname1,
            this.colSurveyorName,
            this.colSurveyedSpanCount,
            this.colAverageInfestation,
            this.colCompletedSpanCount,
            this.colPermissionedSpanCount,
            this.colAverageSpanCost,
            this.colSpansNeedingWork,
            this.colSurveyedSpanClear});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyedSpanCount, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView4_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView4_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView4_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView4_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView4_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            // 
            // colStaffID1
            // 
            this.colStaffID1.Caption = "Staff ID";
            this.colStaffID1.FieldName = "StaffID";
            this.colStaffID1.Name = "colStaffID1";
            this.colStaffID1.OptionsColumn.AllowEdit = false;
            this.colStaffID1.OptionsColumn.AllowFocus = false;
            this.colStaffID1.OptionsColumn.ReadOnly = true;
            this.colStaffID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colForename1
            // 
            this.colForename1.Caption = "Forename";
            this.colForename1.FieldName = "Forename";
            this.colForename1.Name = "colForename1";
            this.colForename1.OptionsColumn.AllowEdit = false;
            this.colForename1.OptionsColumn.AllowFocus = false;
            this.colForename1.OptionsColumn.ReadOnly = true;
            this.colForename1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colForename1.Width = 131;
            // 
            // colSurname1
            // 
            this.colSurname1.Caption = "Surname";
            this.colSurname1.FieldName = "Surname";
            this.colSurname1.Name = "colSurname1";
            this.colSurname1.OptionsColumn.AllowEdit = false;
            this.colSurname1.OptionsColumn.AllowFocus = false;
            this.colSurname1.OptionsColumn.ReadOnly = true;
            this.colSurname1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurname1.Width = 130;
            // 
            // colSurveyorName
            // 
            this.colSurveyorName.Caption = "Surveyor Name";
            this.colSurveyorName.FieldName = "SurveyorName";
            this.colSurveyorName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSurveyorName.Name = "colSurveyorName";
            this.colSurveyorName.OptionsColumn.AllowEdit = false;
            this.colSurveyorName.OptionsColumn.AllowFocus = false;
            this.colSurveyorName.OptionsColumn.ReadOnly = true;
            this.colSurveyorName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyorName.Visible = true;
            this.colSurveyorName.VisibleIndex = 0;
            this.colSurveyorName.Width = 162;
            // 
            // colSurveyedSpanCount
            // 
            this.colSurveyedSpanCount.Caption = "Surveyed";
            this.colSurveyedSpanCount.FieldName = "SurveyedSpanCount";
            this.colSurveyedSpanCount.Name = "colSurveyedSpanCount";
            this.colSurveyedSpanCount.OptionsColumn.AllowEdit = false;
            this.colSurveyedSpanCount.OptionsColumn.AllowFocus = false;
            this.colSurveyedSpanCount.OptionsColumn.ReadOnly = true;
            this.colSurveyedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedSpanCount.Visible = true;
            this.colSurveyedSpanCount.VisibleIndex = 1;
            this.colSurveyedSpanCount.Width = 80;
            // 
            // colAverageInfestation
            // 
            this.colAverageInfestation.Caption = "Avg. Infestation";
            this.colAverageInfestation.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colAverageInfestation.FieldName = "AverageInfestation";
            this.colAverageInfestation.Name = "colAverageInfestation";
            this.colAverageInfestation.OptionsColumn.AllowEdit = false;
            this.colAverageInfestation.OptionsColumn.AllowFocus = false;
            this.colAverageInfestation.OptionsColumn.ReadOnly = true;
            this.colAverageInfestation.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageInfestation.Visible = true;
            this.colAverageInfestation.VisibleIndex = 4;
            this.colAverageInfestation.Width = 100;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P0";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colCompletedSpanCount
            // 
            this.colCompletedSpanCount.Caption = "Completed";
            this.colCompletedSpanCount.FieldName = "CompletedSpanCount";
            this.colCompletedSpanCount.Name = "colCompletedSpanCount";
            this.colCompletedSpanCount.OptionsColumn.AllowEdit = false;
            this.colCompletedSpanCount.OptionsColumn.AllowFocus = false;
            this.colCompletedSpanCount.OptionsColumn.ReadOnly = true;
            this.colCompletedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompletedSpanCount.Visible = true;
            this.colCompletedSpanCount.VisibleIndex = 6;
            this.colCompletedSpanCount.Width = 72;
            // 
            // colPermissionedSpanCount
            // 
            this.colPermissionedSpanCount.Caption = "Permissioned";
            this.colPermissionedSpanCount.FieldName = "PermissionedSpanCount";
            this.colPermissionedSpanCount.Name = "colPermissionedSpanCount";
            this.colPermissionedSpanCount.OptionsColumn.AllowEdit = false;
            this.colPermissionedSpanCount.OptionsColumn.AllowFocus = false;
            this.colPermissionedSpanCount.OptionsColumn.ReadOnly = true;
            this.colPermissionedSpanCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPermissionedSpanCount.Visible = true;
            this.colPermissionedSpanCount.VisibleIndex = 5;
            this.colPermissionedSpanCount.Width = 83;
            // 
            // colAverageSpanCost
            // 
            this.colAverageSpanCost.Caption = "Avg. Cost";
            this.colAverageSpanCost.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colAverageSpanCost.FieldName = "AverageSpanCost";
            this.colAverageSpanCost.Name = "colAverageSpanCost";
            this.colAverageSpanCost.OptionsColumn.AllowEdit = false;
            this.colAverageSpanCost.OptionsColumn.AllowFocus = false;
            this.colAverageSpanCost.OptionsColumn.ReadOnly = true;
            this.colAverageSpanCost.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAverageSpanCost.Visible = true;
            this.colAverageSpanCost.VisibleIndex = 7;
            this.colAverageSpanCost.Width = 85;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colSpansNeedingWork
            // 
            this.colSpansNeedingWork.Caption = "Work Required";
            this.colSpansNeedingWork.FieldName = "SpansNeedingWork";
            this.colSpansNeedingWork.Name = "colSpansNeedingWork";
            this.colSpansNeedingWork.OptionsColumn.AllowEdit = false;
            this.colSpansNeedingWork.OptionsColumn.AllowFocus = false;
            this.colSpansNeedingWork.OptionsColumn.ReadOnly = true;
            this.colSpansNeedingWork.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpansNeedingWork.Visible = true;
            this.colSpansNeedingWork.VisibleIndex = 3;
            this.colSpansNeedingWork.Width = 92;
            // 
            // colSurveyedSpanClear
            // 
            this.colSurveyedSpanClear.Caption = "Clear";
            this.colSurveyedSpanClear.FieldName = "SurveyedSpanClear";
            this.colSurveyedSpanClear.Name = "colSurveyedSpanClear";
            this.colSurveyedSpanClear.OptionsColumn.AllowEdit = false;
            this.colSurveyedSpanClear.OptionsColumn.AllowFocus = false;
            this.colSurveyedSpanClear.OptionsColumn.ReadOnly = true;
            this.colSurveyedSpanClear.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyedSpanClear.Visible = true;
            this.colSurveyedSpanClear.VisibleIndex = 2;
            this.colSurveyedSpanClear.Width = 61;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(1113, 518);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1113, 518);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Page 1";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1107, 491);
            this.layoutControlGroup2.Text = "Page 1";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.splitContainerControl4;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1107, 491);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Page 2";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1107, 491);
            this.layoutControlGroup3.Text = "Page 2";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.splitContainerControl2;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1107, 491);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Page3";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1107, 491);
            this.layoutControlGroup4.Text = "Page3";
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Criteria";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup5.Size = new System.Drawing.Size(1107, 62);
            this.layoutControlGroup5.Text = "Criteria";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(632, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(459, 24);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEditYTDStart;
            this.layoutControlItem4.CustomizationFormText = "YTD Start Date:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem4.Text = "YTD Start Date:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.spinEditDaysGrouping;
            this.layoutControlItem5.CustomizationFormText = "Grouping Days:";
            this.layoutControlItem5.Location = new System.Drawing.Point(456, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(176, 24);
            this.layoutControlItem5.Text = "Grouping Days:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.dateEditGroupingEndDate;
            this.layoutControlItem6.CustomizationFormText = "Grouping End Date:";
            this.layoutControlItem6.Location = new System.Drawing.Point(235, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem6.Text = "Grouping End Date:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl5;
            this.layoutControlItem3.CustomizationFormText = "Surveyor Metrics Grid 2";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 62);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1107, 429);
            this.layoutControlItem3.Text = "Surveyor Metrics Grid 2";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // sp07327_UT_Voltages_Filter_ListTableAdapter
            // 
            this.sp07327_UT_Voltages_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07329_UT_Surveyor_List_No_BlankTableAdapter
            // 
            this.sp07329_UT_Surveyor_List_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07330_UT_Team_List_No_BlankTableAdapter
            // 
            this.sp07330_UT_Team_List_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter
            // 
            this.sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter.ClearBeforeFill = true;
            // 
            // sp07339_UT_Reporting_Surveyor_metricsTableAdapter
            // 
            this.sp07339_UT_Reporting_Surveyor_metricsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter
            // 
            this.sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter.ClearBeforeFill = true;
            // 
            // sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter
            // 
            this.sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // frm_UT_Dashboard
            // 
            this.ClientSize = new System.Drawing.Size(1113, 574);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Dashboard";
            this.Text = "Utilities Dashboard";
            this.Activated += new System.EventHandler(this.frm_UT_Dashboard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Dashboard_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Dashboard_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07340UTReportingSurveyorMetricsByDateBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07319UTReportingSurveyorsbySpansSurveyedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07331UTReportingTeamsbySpansWorkedOnBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupControlContainerChartSetup)).EndInit();
            this.popupControlContainerChartSetup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart7ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart7ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart6ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart6ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowTarget.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart5ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart4ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart4ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart2ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart2ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart3ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart3ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart1ShowLabels.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditChart1ShowLegend.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditFilterCircuits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditVoltageFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlVoltageFilter)).EndInit();
            this.popupContainerControlVoltageFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07327UTVoltagesFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlFilter)).EndInit();
            this.popupContainerControlFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSurveyorFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyorFilter)).EndInit();
            this.popupContainerControlSurveyorFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07329UTSurveyorListNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditTeamFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTeamFilter)).EndInit();
            this.popupContainerControlTeamFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07330UTTeamListNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditSurveyorPerformance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlSurveyorPerformance)).EndInit();
            this.popupContainerControlSurveyorPerformance.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupControl9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDateDivision2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMetric2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditDateDivision1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMetric1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditChartSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram3D2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pie3DSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGroupingEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditGroupingEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditDaysGrouping.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditYTDStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditYTDStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07349UTReportingSurveyormetrics2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bandedGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(areaSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07340UTReportingSurveyorMetricsByDate2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl7)).EndInit();
            this.splitContainerControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl8)).EndInit();
            this.splitContainerControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideStackedBarSeriesView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07335UTReportingClearSpanTargetVActualBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07339UTReportingSurveyormetricsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem barEditItemPopupDateFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateFilter;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarButtonItem bbiPrintPreviewScreen;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraCharts.ChartControl chartControl3;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnOK1;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DataSet_UT_ReportingTableAdapters.sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter sp07319_UT_Reporting_Surveyors_by_Spans_SurveyedTableAdapter;
        private System.Windows.Forms.BindingSource sp07319UTReportingSurveyorsbySpansSurveyedBindingSource;
        private DataSet_UT_Reporting dataSet_UT_Reporting;
        private DevExpress.XtraBars.BarEditItem buttonEditFilterCircuits;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditFilterCircuits;
        private DevExpress.XtraBars.BarEditItem barEditItemPopupVoltageFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditVoltageFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlVoltageFilter;
        private DevExpress.XtraEditors.SimpleButton VoltageFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource sp07327UTVoltagesFilterListBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07327_UT_Voltages_Filter_ListTableAdapter sp07327_UT_Voltages_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colintOrder;
        private DevExpress.XtraBars.BarEditItem barEditItemPopupSurveyorFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditSurveyorFilter;
        private DevExpress.XtraBars.BarEditItem barEditItemPopupTeamFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditTeamFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSurveyorFilter;
        private DevExpress.XtraEditors.SimpleButton SurveyorFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTeamFilter;
        private DevExpress.XtraEditors.SimpleButton TeamFilterOK;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp07329UTSurveyorListNoBlankBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07329_UT_Surveyor_List_No_BlankTableAdapter sp07329_UT_Surveyor_List_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDisplayName;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp07330UTTeamListNoBlankBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07330_UT_Team_List_No_BlankTableAdapter sp07330_UT_Team_List_No_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalTeam;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit checkEditChart1ShowLegend;
        private DevExpress.XtraEditors.CheckEdit checkEditChart1ShowLabels;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.CheckEdit checkEditChart3ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart3ShowLegend;
        private DevExpress.XtraBars.BarButtonItem barButtonItemChartSetup;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditChartSetup;
        private DevExpress.XtraBars.PopupControlContainer popupControlContainerChartSetup;
        private DataSet_UT_ReportingTableAdapters.sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter sp07331_UT_Reporting_Teams_by_Spans_Worked_OnTableAdapter;
        private System.Windows.Forms.BindingSource sp07331UTReportingTeamsbySpansWorkedOnBindingSource;
        private DevExpress.XtraCharts.ChartControl chartControl2;
        private DevExpress.XtraCharts.ChartControl chartControl4;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.CheckEdit checkEditChart4ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart4ShowLegend;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.CheckEdit checkEditChart2ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart2ShowLegend;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraCharts.ChartControl chartControl5;
        private DevExpress.XtraEditors.SpinEdit spinEditYear;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private System.Windows.Forms.BindingSource sp07335UTReportingClearSpanTargetVActualBindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter sp07335_UT_Reporting_Clear_Span_Target_V_ActualTableAdapter;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.CheckEdit checkEditChart5ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart5ShowLegend;
        private DevExpress.XtraEditors.CheckEdit checkEditChart5ShowTarget;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource sp07339UTReportingSurveyormetricsBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID1;
        private DevExpress.XtraGrid.Columns.GridColumn colForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyorName;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedSpanCount;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageInfestation;
        private DevExpress.XtraGrid.Columns.GridColumn colCompletedSpanCount;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionedSpanCount;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageSpanCost;
        private DataSet_UT_ReportingTableAdapters.sp07339_UT_Reporting_Surveyor_metricsTableAdapter sp07339_UT_Reporting_Surveyor_metricsTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraGrid.Columns.GridColumn colSpansNeedingWork;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedSpanClear;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraCharts.ChartControl chartControl6;
        private DataSet_UT_ReportingTableAdapters.sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter sp07340_UT_Reporting_Surveyor_Metrics_By_DateTableAdapter;
        private System.Windows.Forms.BindingSource sp07340UTReportingSurveyorMetricsByDateBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraCharts.ChartControl chartControl7;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraEditors.RangeControl rangeControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.CheckEdit checkEditChart6ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart6ShowLegend;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private DevExpress.XtraEditors.CheckEdit checkEditChart7ShowLabels;
        private DevExpress.XtraEditors.CheckEdit checkEditChart7ShowLegend;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlSurveyorPerformance;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDateDivision1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMetric1;
        private DevExpress.XtraEditors.SimpleButton btnOk_5;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditDateDivision2;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMetric2;
        private DevExpress.XtraBars.BarEditItem barEditItemSurveyorPerformance;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditSurveyorPerformance;
        private System.Windows.Forms.BindingSource sp07340UTReportingSurveyorMetricsByDate2BindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter sp07340_UT_Reporting_Surveyor_Metrics_By_Date_2TableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.SpinEdit spinEditDaysGrouping;
        private DevExpress.XtraEditors.DateEdit dateEditYTDStart;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.DateEdit dateEditGroupingEndDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource sp07349UTReportingSurveyormetrics2BindingSource;
        private DataSet_UT_ReportingTableAdapters.sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter sp07349_UT_Reporting_Surveyor_metrics_2TableAdapter;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView bandedGridView1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colStaffID2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colForename2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurname2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurveyorName1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurveyedSpanCount1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekSurveyedSpanCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colAverageInfestation1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekAverageInfestation;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCompletedSpanCount1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekCompletedSpanCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colPermissionedSpanCount1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekPermissionedSpanCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSpansNeedingWork1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekSpansNeedingWork;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colSurveyedSpanClear1;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekSurveyedSpanClear;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colWeekReactiveSpanCompleteCount;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colReactiveSpanCompleteCount;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand5;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand8;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
    }
}

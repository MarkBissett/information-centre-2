using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;
using DevExpress.XtraEditors.Repository;  // Required by emptyEditor //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_WorkOrder_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private string strDefaultMapPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPicturePath = "";  // Path for the Picture files //
        private string strDefaultWorkOrderDocumentPath = "";  // Path for the Picture files //
        private string strPermissionFilePath = "";  // Path for the Picture files //

        #endregion

        public frm_UT_WorkOrder_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_WorkOrder_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500060;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            emptyEditor = new RepositoryItem();

            sp07318_UT_Work_Order_Status_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07318_UT_Work_Order_Status_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07318_UT_Work_Order_Status_List);

            sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ActionID");

            sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "WorkOrderTeamID");

            sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedMapID");

            sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "PermissionDocumentID");
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultWorkOrderDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Work Order Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Work Order Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPermissionFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Permission Document Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            sp07247_UT_Work_Order_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["DateCreated"] = DateTime.Now;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedBy"] = (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : GlobalSettings.UserSurname) + (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : ": ") + (string.IsNullOrEmpty(GlobalSettings.UserForename) ? "" : GlobalSettings.UserForename);
                        drNewRow["PaddedWorkOrderID"] = "Set on First Save";
                        drNewRow["StatusID"] = 0;
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Add(drNewRow);
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Add(drNewRow);
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07247_UT_Work_Order_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    if (strFormMode == "edit")
                    {
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.ReadOnly = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl3.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.ReadOnly = false;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = true;

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl3.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ReferenceNumberButtonEdit.Focus();

                        ReferenceNumberButtonEdit.Properties.ReadOnly = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[0].Enabled = true;
                        ReferenceNumberButtonEdit.Properties.Buttons[1].Enabled = false;

                        barButtonItemPrint.Enabled = false;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                        gridControl3.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                barButtonItemPrint.Enabled = false;
                gridControl1.Enabled = false;
                gridControl2.Enabled = false;
                gridControl3.Enabled = false;
                gridControl4.Enabled = false;
            }
            SetMenuStatus();  // Just in case any default layout has permissions set wrongly //
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.EndEdit();
                dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                    bbiSave.Enabled = false;
                    bbiFormSave.Enabled = false;
                    return false;
                }
            }
        }


        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

 
            if (!iBool_AllowEdit || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intWorkOrderID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            DateTime dtDateCompleted = Convert.ToDateTime("01/01/1900");
            if (currentRow != null)
            {
                intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                dtDateCompleted = (currentRow["DateCompleted"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(currentRow["DateCompleted"]));
            }

            if (i_int_FocusedGrid == 1)  // Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 2)  // Contractors //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Maps //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length == 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDelete)
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowEdit && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);  // Move Up Btn //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowEdit && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);  // Move Down Btn //

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            if (iBool_AllowDelete)
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl2.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            if (iBool_AllowDelete)
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view" ? true : false);
            }
            else
            {
                gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl4.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl4.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0 ? true : false);
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }


        private void frm_UT_WorkOrder_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus == 99)  // Refresh Top Level data //
            {
                Load_Top_Level();
            }
            else if (UpdateRefreshStatus == 1)
            {
                LoadLinkedActions();
            }
            else if (UpdateRefreshStatus == 2)
            {
                LoadLinkedContractors();
            }
            else if (UpdateRefreshStatus == 3)
            {
                LoadLinkedMaps();
            }
            SetMenuStatus();
        }

        private void frm_UT_WorkOrder_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            // Commit any outstanding changes within the grid control //
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            // Save Work Order Header //
            this.sp07247UTWorkOrderEditBindingSource.EndEdit();
            try
            {
                this.sp07247_UT_Work_Order_EditTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Actions //
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.EndEdit();
            try
            {
                this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked action changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Sub-Contractors //
            this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource.EndEdit();
            try
            {
                this.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked sub-contractor changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Maps //
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.EndEdit();
            try
            {
                this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked map changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["WorkOrderID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_WorkOrder_Manager")
                    {
                        var fParentForm = (frm_UT_WorkOrder_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intLinkedRecordNew = 0;
            int intLinkedRecordModified = 0;
            int intLinkedRecordDeleted = 0;

            int intLinkedRecordNew2 = 0;
            int intLinkedRecordModified2 = 0;
            int intLinkedRecordDeleted2 = 0;

            int intLinkedRecordNew3 = 0;
            int intLinkedRecordModified3 = 0;
            int intLinkedRecordDeleted3 = 0;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted++;
                        break;
                }
            }

            view = (GridView)gridControl2.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Sub-Contractor Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07271_UT_Work_Order_Edit_Linked_SubContractors.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07271_UT_Work_Order_Edit_Linked_SubContractors.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew2++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified2++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted2++;
                        break;
                }
            }

            view = (GridView)gridControl3.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Map Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew3++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified3++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted3++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedRecordNew > 0 || intLinkedRecordModified > 0 || intLinkedRecordDeleted > 0 || intLinkedRecordNew2 > 0 || intLinkedRecordModified2 > 0 || intLinkedRecordDeleted2 > 0 || intLinkedRecordNew3 > 0 || intLinkedRecordModified3 > 0 || intLinkedRecordDeleted3 > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Work Order record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Work Order record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Work Order record(s)\n";
                if (intLinkedRecordNew > 0) strMessage += Convert.ToString(intLinkedRecordNew) + " New Linked Action record(s)\n";
                if (intLinkedRecordModified > 0) strMessage += Convert.ToString(intLinkedRecordModified) + " Updated Linked Action record(s)\n";
                if (intLinkedRecordDeleted > 0) strMessage += Convert.ToString(intLinkedRecordDeleted) + " Deleted Linked Action record(s)\n";
                if (intLinkedRecordNew2 > 0) strMessage += Convert.ToString(intLinkedRecordNew2) + " New Linked Sub-Contractor record(s)\n";
                if (intLinkedRecordModified2 > 0) strMessage += Convert.ToString(intLinkedRecordModified2) + " Updated Linked Sub-Contractor record(s)\n";
                if (intLinkedRecordDeleted2 > 0) strMessage += Convert.ToString(intLinkedRecordDeleted2) + " Deleted Linked Sub-Contractor record(s)\n";
                if (intLinkedRecordNew3 > 0) strMessage += Convert.ToString(intLinkedRecordNew3) + " New Linked Map record(s)\n";
                if (intLinkedRecordModified3 > 0) strMessage += Convert.ToString(intLinkedRecordModified3) + " Updated Linked Map record(s)\n";
                if (intLinkedRecordDeleted3 > 0) strMessage += Convert.ToString(intLinkedRecordDeleted3) + " Deleted Linked Map record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            if (intWorkOrderID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before creating a Work Order Document File.", "Create Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strPDFFile = (currentRow["WorkOrderPDF"] == null ? "" : currentRow["WorkOrderPDF"].ToString());

            // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before creating the Work Order Document File!\n\nSave Changes?", "Create Work Order Document", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (boolProceed)
            {
                string strDocumentDescription = currentRow["PaddedWorkOrderID"].ToString();

                frm_UT_WorkOrder_Create_Document fChildForm = new frm_UT_WorkOrder_Create_Document();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.strWorkOrderDocumentPath = strDefaultWorkOrderDocumentPath;
                fChildForm.strPicturePath = strDefaultPicturePath;
                fChildForm.strMapPath = strDefaultMapPath;
                fChildForm._strExistingPDFFileName = strPDFFile;
                fChildForm._intWorkOrderID = intWorkOrderID;
                fChildForm._boolViewOnly = false;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

       }

        public void Load_Top_Level()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            // Refresh main data since it has been changed by the Create Permission Document screen [may have a signature file and a physical PDF document] //
            try
            {
                sp07247_UT_Work_Order_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit, strRecordIDs, strFormMode);
            }
            catch (Exception)
            {
            }
        }

        private void LoadLinkedActions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit, strWorkOrderID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }


            // Load Any Linked Permission Document (Linked via Actions) //
            string strActionIDs = "";
            foreach (DataRow dr in dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.Rows)
            {
                strActionIDs += dr["ActionID"].ToString() + ",";
            }
            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
            gridControl4.BeginUpdate();
            sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07352_UT_Work_Order_Linked_Permission_Documents, strActionIDs);
            this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl4.EndUpdate();
        }

        private void LoadLinkedContractors()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            gridControl2.BeginUpdate();
            sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07271_UT_Work_Order_Edit_Linked_SubContractors, strWorkOrderID);
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WorkOrderTeamID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }

        private void LoadLinkedMaps()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl3.BeginUpdate();
            sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps, strWorkOrderID, strDefaultMapPath);
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedMapID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Actions Linked - Click Add button to link Actions";
                    break;
                case "gridView2":
                    message = "No Sub-Contractors Linked - Click Add button to link Sub-Contractors";
                    break;
                case "gridView3":
                    message = "No Maps Linked - Click Add button to link Maps";
                    break;
                case "gridView4":
                    message = "No Permission Documents Linked to Actions - Use the Permission Document Manager to Link Actions";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "WorkOrderSortOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "WorkOrderSortOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        // Clean Up any holes in the Sort Order Sequence... //
                        int intCorrectOrder = 0;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            intCorrectOrder++;
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                        }
                        view.EndDataUpdate();
                        view.EndSort();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "WorkOrderSortOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "WorkOrderSortOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "WorkOrderSortOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();

                        // Clean Up any holes in the Sort Order Sequence... //
                        int intCorrectOrder = 0;
                        view.BeginDataUpdate();
                        view.BeginSort();
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            intCorrectOrder++;
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                        }
                        view.EndDataUpdate();
                        view.EndSort();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CostCentreCode":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("intOwnershipID")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        #endregion


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                 default:
                    break;
            }
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged_1(object sender, EventArgs e)
        {
            gridView2.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }

        #endregion

        #endregion


        #region GridView3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "CostCentreCode":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "intOwnershipID")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView3_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView3;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "MapOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "MapOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit3_EditValueChanged(object sender, EventArgs e)
        {
            gridView3.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }

        #endregion

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 4;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView4_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PDFFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView4_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PDFFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colPDFFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PDFFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strPermissionFilePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    //System.Diagnostics.Process.Start(strFilePath);

                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFilePath;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveFirst();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveFirst();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveFirst();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //
                                        bs.MovePrevious();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MovePrevious();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MovePrevious();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveNext();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveNext();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveNext();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveLast();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveLast();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveLast();
                        }
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else  // Load in Past Inspections... //
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadLinkedActions();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();

                        LoadLinkedContractors();
                        view = (GridView)gridControl2.MainView;
                        view.ExpandAllGroups();

                        LoadLinkedMaps();
                        view = (GridView)gridControl3.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void ReferenceNumberButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            string strTableName = "ut_work_order";
            string strFieldName = "ReferenceNumber";
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = strTableName;
                fChildForm.PassedInFieldName = strFieldName;
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    ReferenceNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    //i_strLastUsedSequencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                // Get next value in sequence //
                string strSequence = ReferenceNumberButtonEdit.EditValue.ToString() ?? "";

                SequenceNumberGetNext sequence = new SequenceNumberGetNext();
                string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, strFieldName, strTableName, strSequence);  // Calculate Next in Sequence //

                if (strNextNumber == "")
                {
                    return;
                }
                if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

                int intRequiredLength = strNextNumber.Length;
                int intNextNumber = Convert.ToInt32(strNextNumber);
                int intIncrement = 0;
                string strTempNumber = "";


                // Check if value is already present within dataset //
                string strCurrentWorkOrderReference = ReferenceNumberButtonEdit.EditValue.ToString();
                if (string.IsNullOrEmpty(strCurrentWorkOrderReference)) strCurrentWorkOrderReference = "";
                bool boolUniqueValueFound = false;
                bool boolDuplicateFound = false;
                do
                {
                    boolDuplicateFound = false;
                    strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                    if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    else
                    {
                        strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                    }
                    foreach (DataRow dr in this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows)
                    {
                        if (dr["ReferenceNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                        {
                            intIncrement++;
                            boolDuplicateFound = true;
                            break;
                        }
                    }
                    if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
                } while (!boolUniqueValueFound);
                ReferenceNumberButtonEdit.EditValue = strSequence + strTempNumber;
            }
        }

        private void DateCompletedDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off for DBH Field //
        }
        private void DateCompletedDateEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;

            DateEdit de = (DateEdit)sender;
            if (de.DateTime != Convert.ToDateTime("01/01/0001"))
            {
                // Set all outstanding actions to complete //
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (string.IsNullOrEmpty(view.GetRowCellValue(i, "DateCompleted").ToString())) view.SetRowCellValue(i, "DateCompleted", de.DateTime);
                }
            }
            SetMenuStatus();  // Make Sure Add Action button is enabled if appropriate //
        }

        private void WorkOrderPDFHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strFile = WorkOrderPDFHyperLinkEdit.Text.ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Work Order Document Linked - unable to proceed.\n\nTry clicking the Create Work Order Document button.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strFilePath = strDefaultWorkOrderDocumentPath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                //System.Diagnostics.Process.Start(strFilePath);

                frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                fChildForm.strPDFFile = strFilePath;
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.Show();

            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Work Order Document: " + strFile + ".\n\nThe Work Order may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        public void Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Tree Work //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Actions.", "Add Linked Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        int intMaxOrder = 0;
                        string strExcludeActions = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludeActions += view.GetRowCellValue(i, "ActionID").ToString() + ";";
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder"));
                        }

                        frm_UT_WorkOrder_Add_Actions fChildForm = new frm_UT_WorkOrder_Add_Actions();
                        fChildForm.strExcludedActionIDs = strExcludeActions;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Linking Actions...");

                            view.BeginUpdate();
                            view.BeginSort();
                            DataTable dtNew = fChildForm.dtSelectedActions;
                            // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                            //dtNew.Columns.Remove("ActionPriority");
                            
                            foreach (DataRow dr in dtNew.Rows)
                            {
                                intMaxOrder++;
                                dr["WorkOrderSortOrder"] = intMaxOrder;
                                dr["WorkOrderID"] = intWorkOrderID;
                                this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.ImportRow(dr);
                            }
                            view.EndSort();
                            view.EndUpdate();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                        }
                    }
                    break;
                case 2:     // Teams //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Teams.", "Add Linked Team(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        string strExcludeTeams = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludeTeams += view.GetRowCellValue(i, "SubContractorID").ToString() + ";";
                        }

                        frm_UT_WorkOrder_Add_Teams fChildForm = new frm_UT_WorkOrder_Add_Teams();
                        fChildForm.strExcludedTeamIDs = strExcludeTeams;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Linking Teams...");

                            view.BeginUpdate();
                            view.BeginSort();
                            DataTable dtNew = fChildForm.dtSelectedTeams;
                            // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                            //dtNew.Columns.Remove("ActionPriority");

                            foreach (DataRow dr in dtNew.Rows)
                            {
                                dr["WorkOrderID"] = intWorkOrderID;
                                this.dataSet_UT_WorkOrder.sp07271_UT_Work_Order_Edit_Linked_SubContractors.ImportRow(dr);
                            }
                            view.EndSort();
                            view.EndUpdate();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                        }
                    }
                    break;
                case 3:     // Maps //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Maps.", "Add Linked Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
                        bool boolProceed = true;
                        string strMessage = CheckForPendingSave();
                        if (!string.IsNullOrEmpty(strMessage))
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before opening the mapping!\n\nSave Changes?", "Add Linked Map(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                            }
                            else
                            {
                                boolProceed = false;  // The user said no to save changes //
                            }
                        }
                        else  // No pending changes in screen //
                        {
                            boolProceed = true;
                        }
                        if (!boolProceed) return;

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        // Check for any outstanding changes in the linked Maps Grid //

                        OpenMapping();
                    }
                    break;

            }
        }

        public void Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Tree Work //
                    {
                        // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
                        bool boolProceed = true;
                        string strMessage = CheckForPendingSave();
                        if (!string.IsNullOrEmpty(strMessage))
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before editing an action!\n\nSave Changes?", "Edit Action", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                            }
                            else
                            {
                                boolProceed = false;  // The user said no to save changes //
                            }
                        }
                        else  // No pending changes in screen //
                        {
                            boolProceed = true;
                        }
                        if (!boolProceed) return;

                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPicturePath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Work Orders //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Actions to remove from the Work Order by clicking on them then try again.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Action" : Convert.ToString(intRowHandles.Length) + " Linked Actions") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Action" : "these Linked Actions") + " will be removed from the work order. They will then be available for linking to a different work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            // Clean Up any holes in the Sort Order Sequence... //
                            int intCorrectOrder = 0;
                            view.BeginDataUpdate();
                            view.BeginSort();
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                intCorrectOrder++;
                                if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                            }
                            view.EndDataUpdate();
                            view.EndSort();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:  // Contractors //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Sub-Contractors to remove from the Work Order by clicking on them then try again.", "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Sub-Contractor" : Convert.ToString(intRowHandles.Length) + " Linked Sub-Contractors") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Sub-Contractor" : "these Linked Sub-Contractors") + " will be removed from the work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Maps //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Maps to remove from the Work Order by clicking on them then try again.", "Remove Linked Maps from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Map" : Convert.ToString(intRowHandles.Length) + " Linked Maps") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Map" : "these Linked Maps") + " will be removed from the work order. They will then be available for linking to a different work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }

                            // Clean Up any holes in the Sort Order Sequence... //
                            int intCorrectOrder = 0;
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                intCorrectOrder++;
                                if (Convert.ToInt32(view.GetRowCellValue(i, "MapOrder")) != intCorrectOrder) view.SetRowCellValue(i, "MapOrder", intCorrectOrder);
                            }

                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Maps from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 4:  // Permission Documents //
                    view = (GridView)gridControl4.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;

                    string strLayout = "";
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                        strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                    }
                    this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //

                    if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                    {
                        frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                    {
                        frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }                   
                    break;
                default:
                    break;
            }
        }


        public Boolean Action_Adding_Allowed(ref string strWorkOrderIDs)
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // strWorkOrderIDs passed by reference so original value is updated if there is a WorkOrderID present on the form //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return false;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            if (intWorkOrderID == 0) return false;

            DateTime dt = (currentRow["DateCompleted"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["DateCompleted"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                strWorkOrderIDs += intWorkOrderID.ToString() + ",";
                return true;
            }
            else
            {
                return false;
            }
        }

        public int Return_ActionsID_To_Calling_Form()
        {
            // Called by Tree Picker - Transfer Actions to Work Order process //
            // Returns the WorkOrderID if the work order is not completed. If completed it returns 0 //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //          
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return 0;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            if (intWorkOrderID == 0) return 0;

            DateTime dt = (currentRow["DateCompleted"] == DBNull.Value ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(currentRow["DateCompleted"]));
            if (dt == Convert.ToDateTime("01/01/0001"))
            {
                return intWorkOrderID;
            }
            else
            {
                return 0;
            }
        }


        private void Calculate_Total_Cost()
        {

        }

        private void OpenMapping()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            string strDocumentDescription = currentRow["PaddedWorkOrderID"].ToString();
            if (intWorkOrderID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Maps.", "Add Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = (GridView)gridControl1.MainView;
            int intRowCount = view.RowCount;  // Not using DataRowCount as we have a filtered grid and we don't want any other permission document's work //
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You must add at least one piece of work before proceeding.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get Unique list of Survey IDs //
            string strSurveyIDs = ",";
            string strCurrentID = "";
            string strTreeIDs = ",";
            string strTreeID = "";
            string strPoleIDs = ",";
            string strPoleID = "";
            for (int i = 0; i < view.RowCount; i++)
            {
                strCurrentID = view.GetRowCellValue(i, "SurveyID").ToString();
                if (!(string.IsNullOrEmpty(strCurrentID) || strCurrentID == "0"))
                {
                    if (!strSurveyIDs.Contains("," + strCurrentID + ",")) strSurveyIDs += strCurrentID + ",";
                }
                strTreeID = view.GetRowCellValue(i, "TreeID").ToString();
                if (!(string.IsNullOrEmpty(strTreeID) || strTreeID == "0"))
                {
                    if (!strTreeIDs.Contains("," + strTreeID + ",")) strTreeIDs += strTreeID + ",";
                }
                strPoleID = view.GetRowCellValue(i, "PoleID").ToString();
                if (!(string.IsNullOrEmpty(strPoleID) || strPoleID == "0"))
                {
                    if (!strPoleIDs.Contains("," + strPoleID + ",")) strPoleIDs += strPoleID + ",";
                }
            }
            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strSurveyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Survey - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strTreeIDs = strTreeIDs.TrimStart(',');
            if (strTreeIDs.Length < 2)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Tree - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intSelectedSurveyID = Convert.ToInt32(strArray[0]);

            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(Convert.ToInt32(strArray[0])).ToString();
                delimiters = new char[] { '|' };
                string[] strArray2 = null;
                strArray2 = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray2.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray2[0]);
                    strWorkspaceName = strArray2[1].ToString();
                    intRegionID = Convert.ToInt32(strArray2[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is linked to a Survey, but the survey is not linked to a region with a Mapping Workspace - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Close any open instance of the mapping //
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Close();
                    }
                }
            }
            catch (Exception)
            {
            }

            // Open Map and set mode to Map Snapshot and load in trees with work on this Permission Document //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSelectedSurveyID;
            frmInstance._SurveyMode = "PermissionDocumentMap";
            frmInstance._PassedInRecordType = 2;  // 1 = Permision Document, 2 = Work Order //
            frmInstance._PassedInTreeIDs = strTreeIDs;
            frmInstance._PassedInPoleIDs = strPoleIDs;
            frmInstance._PassedInPermissionDocumentID = intWorkOrderID;
            frmInstance._PassedInPermissionDocumentDescription = strDocumentDescription;
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
            frmInstance.Scale_Map_To_Visible_Objects(false);
        }

 


    }
}


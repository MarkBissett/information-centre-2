namespace WoodPlan5
{
    partial class frm_UT_Shutdown_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Shutdown_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.ContractorNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.sp07375UTShutdownEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.ContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EngineerNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ReferenceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07377UTShutdownStatusListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CreatedByTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ShutdownIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSurveyedPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colIsSpanClear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colInfestationRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHotGloveRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinesmanPossible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrafficManagementResolved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFiveYearClearanceAchieved = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeWithin3Meters = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExchequerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoWorkRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colG55CategoryDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferred = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptior = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnitDescriptiorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferredUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsBaseClimbable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRevisitDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpanInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInvoiced = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeferralRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colTrafficMapPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearanceDistanceUnder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsiblePerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShutdownHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DoneDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.DateCreatedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShutdownID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForReferenceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDoneDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEngineerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateCreated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforCreatedBy = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter();
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter();
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter();
            this.sp07375_UT_Shutdown_EditTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07375_UT_Shutdown_EditTableAdapter();
            this.sp07377_UT_Shutdown_Status_ListTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07377_UT_Shutdown_Status_ListTableAdapter();
            this.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07375UTShutdownEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineerNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07377UTShutdownStatusListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoneDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoneDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoneDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 718);
            this.barDockControlBottom.Size = new System.Drawing.Size(1143, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1143, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 692);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 28;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(487, 172);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(1143, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 718);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(1143, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 692);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1143, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 692);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(7, "arrow_down_blue_round.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ContractorNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EngineerNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ReferenceNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.CreatedByTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ShutdownIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.DueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DoneDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.DateCreatedDateEdit);
            this.dataLayoutControl1.DataSource = this.sp07375UTShutdownEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCreatedByStaffID,
            this.ItemForShutdownID,
            this.ItemForContractorID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(223, 308, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1143, 692);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ContractorNameButtonEdit
            // 
            this.ContractorNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "ContractorName", true));
            this.ContractorNameButtonEdit.EditValue = "";
            this.ContractorNameButtonEdit.Location = new System.Drawing.Point(133, 227);
            this.ContractorNameButtonEdit.MenuManager = this.barManager1;
            this.ContractorNameButtonEdit.Name = "ContractorNameButtonEdit";
            this.ContractorNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Select Client screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ContractorNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ContractorNameButtonEdit.Size = new System.Drawing.Size(974, 20);
            this.ContractorNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ContractorNameButtonEdit.TabIndex = 13;
            this.ContractorNameButtonEdit.TabStop = false;
            this.ContractorNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ContractorNameButtonEdit_ButtonClick);
            // 
            // sp07375UTShutdownEditBindingSource
            // 
            this.sp07375UTShutdownEditBindingSource.DataMember = "sp07375_UT_Shutdown_Edit";
            this.sp07375UTShutdownEditBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ContractorIDTextEdit
            // 
            this.ContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "ContractorID", true));
            this.ContractorIDTextEdit.Location = new System.Drawing.Point(133, 226);
            this.ContractorIDTextEdit.MenuManager = this.barManager1;
            this.ContractorIDTextEdit.Name = "ContractorIDTextEdit";
            this.ContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ContractorIDTextEdit, true);
            this.ContractorIDTextEdit.Size = new System.Drawing.Size(974, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ContractorIDTextEdit, optionsSpelling1);
            this.ContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ContractorIDTextEdit.TabIndex = 53;
            // 
            // EngineerNameTextEdit
            // 
            this.EngineerNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "EngineerName", true));
            this.EngineerNameTextEdit.Location = new System.Drawing.Point(133, 203);
            this.EngineerNameTextEdit.MenuManager = this.barManager1;
            this.EngineerNameTextEdit.Name = "EngineerNameTextEdit";
            this.EngineerNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EngineerNameTextEdit, true);
            this.EngineerNameTextEdit.Size = new System.Drawing.Size(974, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EngineerNameTextEdit, optionsSpelling2);
            this.EngineerNameTextEdit.StyleController = this.dataLayoutControl1;
            this.EngineerNameTextEdit.TabIndex = 52;
            // 
            // ReferenceNumberTextEdit
            // 
            this.ReferenceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "ReferenceNumber", true));
            this.ReferenceNumberTextEdit.Location = new System.Drawing.Point(121, 71);
            this.ReferenceNumberTextEdit.MenuManager = this.barManager1;
            this.ReferenceNumberTextEdit.Name = "ReferenceNumberTextEdit";
            this.ReferenceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReferenceNumberTextEdit, true);
            this.ReferenceNumberTextEdit.Size = new System.Drawing.Size(998, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReferenceNumberTextEdit, optionsSpelling3);
            this.ReferenceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.ReferenceNumberTextEdit.TabIndex = 51;
            this.ReferenceNumberTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ReferenceNumberTextEdit_Validating);
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(126, 70);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(443, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling4);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 47;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(133, 131);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp07377UTShutdownStatusListBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(974, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 50;
            // 
            // sp07377UTShutdownStatusListBindingSource
            // 
            this.sp07377UTShutdownStatusListBindingSource.DataMember = "sp07377_UT_Shutdown_Status_List";
            this.sp07377UTShutdownStatusListBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescription1,
            this.colID,
            this.colRecordOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 230;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // CreatedByTextEdit
            // 
            this.CreatedByTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "CreatedBy", true));
            this.CreatedByTextEdit.Location = new System.Drawing.Point(133, 155);
            this.CreatedByTextEdit.MenuManager = this.barManager1;
            this.CreatedByTextEdit.Name = "CreatedByTextEdit";
            this.CreatedByTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByTextEdit, true);
            this.CreatedByTextEdit.Size = new System.Drawing.Size(974, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByTextEdit, optionsSpelling5);
            this.CreatedByTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByTextEdit.TabIndex = 48;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07375UTShutdownEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(12, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(241, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 40;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ShutdownIDTextEdit
            // 
            this.ShutdownIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "ShutdownID", true));
            this.ShutdownIDTextEdit.Location = new System.Drawing.Point(121, 70);
            this.ShutdownIDTextEdit.MenuManager = this.barManager1;
            this.ShutdownIDTextEdit.Name = "ShutdownIDTextEdit";
            this.ShutdownIDTextEdit.Properties.Mask.EditMask = "0000000";
            this.ShutdownIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.ShutdownIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ShutdownIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ShutdownIDTextEdit, true);
            this.ShutdownIDTextEdit.Size = new System.Drawing.Size(448, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ShutdownIDTextEdit, optionsSpelling6);
            this.ShutdownIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ShutdownIDTextEdit.TabIndex = 4;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Actions to Work Order", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Remove Selected Linked Action(s) from Work Order", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Move Item Down", "down")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(24, 311);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemDateEdit2,
            this.repositoryItemCheckEdit1,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1095, 357);
            this.gridControl1.TabIndex = 39;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07378UTShutdownEditLinkedSurveyedPolesBindingSource
            // 
            this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource.DataMember = "sp07378_UT_Shutdown_Edit_Linked_Surveyed_Poles";
            this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSurveyedPoleID,
            this.colSurveyID1,
            this.colPoleID,
            this.colFeederContractID,
            this.colSurveyedDate,
            this.colIsSpanClear,
            this.colInfestationRate,
            this.colIsShutdownRequired,
            this.colHotGloveRequired,
            this.colLinesmanPossible,
            this.colClearanceDistance,
            this.colTrafficManagementRequired,
            this.colTrafficManagementResolved,
            this.colFiveYearClearanceAchieved,
            this.colTreeWithin3Meters,
            this.colTreeClimbable,
            this.colRemarks2,
            this.colGUID1,
            this.colClientName2,
            this.colSurveyDate1,
            this.colSurveyDescription,
            this.colSurveyor1,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colExchequerNumber,
            this.colContractValue,
            this.colPoleNumber,
            this.colPoleLastInspectionDate,
            this.colPoleNextInspectionDate,
            this.colIsTransformer,
            this.colCircuitNumber,
            this.colCircuitName,
            this.colNoWorkRequired,
            this.colSurveyStatus,
            this.colSurveyStatusID1,
            this.colG55CategoryID,
            this.colG55CategoryDescription,
            this.colDeferred,
            this.colDeferredUnitDescriptior,
            this.colDeferredUnitDescriptiorID,
            this.colDeferredUnits,
            this.colIsBaseClimbable,
            this.colRevisitDate,
            this.colMapID,
            this.colSpanInvoiced,
            this.colInvoiceNumber,
            this.colDateInvoiced,
            this.colDeferralReason,
            this.colDeferralRemarks,
            this.colEstimatedHours,
            this.colActualHours,
            this.colAccessMapPath,
            this.colTrafficMapPath,
            this.colShutdownID,
            this.colClearanceDistanceUnder,
            this.colHeldUpType,
            this.colHeldUpTypeID,
            this.colRegionName,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colPrimaryName,
            this.colResponsiblePerson,
            this.colSubAreaName,
            this.colShutdownHours});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurveyDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubAreaName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPrimaryName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFeederName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // colSurveyedPoleID
            // 
            this.colSurveyedPoleID.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID.Name = "colSurveyedPoleID";
            this.colSurveyedPoleID.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID.Width = 104;
            // 
            // colSurveyID1
            // 
            this.colSurveyID1.Caption = "Survey ID";
            this.colSurveyID1.FieldName = "SurveyID";
            this.colSurveyID1.Name = "colSurveyID1";
            this.colSurveyID1.OptionsColumn.AllowEdit = false;
            this.colSurveyID1.OptionsColumn.AllowFocus = false;
            this.colSurveyID1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.Caption = "Feeder Contract ID";
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.Width = 110;
            // 
            // colSurveyedDate
            // 
            this.colSurveyedDate.Caption = "Date Surveyed";
            this.colSurveyedDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colSurveyedDate.FieldName = "SurveyedDate";
            this.colSurveyedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyedDate.Name = "colSurveyedDate";
            this.colSurveyedDate.OptionsColumn.AllowEdit = false;
            this.colSurveyedDate.OptionsColumn.AllowFocus = false;
            this.colSurveyedDate.OptionsColumn.ReadOnly = true;
            this.colSurveyedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyedDate.Visible = true;
            this.colSurveyedDate.VisibleIndex = 13;
            this.colSurveyedDate.Width = 93;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colIsSpanClear
            // 
            this.colIsSpanClear.Caption = "Span Clear";
            this.colIsSpanClear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSpanClear.FieldName = "IsSpanClear";
            this.colIsSpanClear.Name = "colIsSpanClear";
            this.colIsSpanClear.OptionsColumn.AllowEdit = false;
            this.colIsSpanClear.OptionsColumn.AllowFocus = false;
            this.colIsSpanClear.OptionsColumn.ReadOnly = true;
            this.colIsSpanClear.Visible = true;
            this.colIsSpanClear.VisibleIndex = 15;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colInfestationRate
            // 
            this.colInfestationRate.Caption = "Infestation Rate";
            this.colInfestationRate.FieldName = "InfestationRate";
            this.colInfestationRate.Name = "colInfestationRate";
            this.colInfestationRate.OptionsColumn.AllowEdit = false;
            this.colInfestationRate.OptionsColumn.AllowFocus = false;
            this.colInfestationRate.OptionsColumn.ReadOnly = true;
            this.colInfestationRate.Visible = true;
            this.colInfestationRate.VisibleIndex = 16;
            this.colInfestationRate.Width = 100;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 17;
            this.colIsShutdownRequired.Width = 115;
            // 
            // colHotGloveRequired
            // 
            this.colHotGloveRequired.Caption = "Hot Glove";
            this.colHotGloveRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colHotGloveRequired.FieldName = "HotGloveRequired";
            this.colHotGloveRequired.Name = "colHotGloveRequired";
            this.colHotGloveRequired.OptionsColumn.AllowEdit = false;
            this.colHotGloveRequired.OptionsColumn.AllowFocus = false;
            this.colHotGloveRequired.OptionsColumn.ReadOnly = true;
            this.colHotGloveRequired.Visible = true;
            this.colHotGloveRequired.VisibleIndex = 19;
            // 
            // colLinesmanPossible
            // 
            this.colLinesmanPossible.Caption = "Linesman";
            this.colLinesmanPossible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLinesmanPossible.FieldName = "LinesmanPossible";
            this.colLinesmanPossible.Name = "colLinesmanPossible";
            this.colLinesmanPossible.OptionsColumn.AllowEdit = false;
            this.colLinesmanPossible.OptionsColumn.AllowFocus = false;
            this.colLinesmanPossible.OptionsColumn.ReadOnly = true;
            this.colLinesmanPossible.Visible = true;
            this.colLinesmanPossible.VisibleIndex = 20;
            // 
            // colClearanceDistance
            // 
            this.colClearanceDistance.Caption = "Clearance Distance (Side)";
            this.colClearanceDistance.FieldName = "ClearanceDistance";
            this.colClearanceDistance.Name = "colClearanceDistance";
            this.colClearanceDistance.OptionsColumn.AllowEdit = false;
            this.colClearanceDistance.OptionsColumn.AllowFocus = false;
            this.colClearanceDistance.OptionsColumn.ReadOnly = true;
            this.colClearanceDistance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClearanceDistance.Visible = true;
            this.colClearanceDistance.VisibleIndex = 24;
            this.colClearanceDistance.Width = 144;
            // 
            // colTrafficManagementRequired
            // 
            this.colTrafficManagementRequired.Caption = "Traffic Management Req.";
            this.colTrafficManagementRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementRequired.FieldName = "TrafficManagementRequired";
            this.colTrafficManagementRequired.Name = "colTrafficManagementRequired";
            this.colTrafficManagementRequired.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementRequired.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementRequired.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementRequired.Visible = true;
            this.colTrafficManagementRequired.VisibleIndex = 26;
            this.colTrafficManagementRequired.Width = 143;
            // 
            // colTrafficManagementResolved
            // 
            this.colTrafficManagementResolved.Caption = "Traffic Management Resolved";
            this.colTrafficManagementResolved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTrafficManagementResolved.FieldName = "TrafficManagementResolved";
            this.colTrafficManagementResolved.Name = "colTrafficManagementResolved";
            this.colTrafficManagementResolved.OptionsColumn.AllowEdit = false;
            this.colTrafficManagementResolved.OptionsColumn.AllowFocus = false;
            this.colTrafficManagementResolved.OptionsColumn.ReadOnly = true;
            this.colTrafficManagementResolved.Visible = true;
            this.colTrafficManagementResolved.VisibleIndex = 27;
            this.colTrafficManagementResolved.Width = 164;
            // 
            // colFiveYearClearanceAchieved
            // 
            this.colFiveYearClearanceAchieved.Caption = "5 Year Clearance Achieved";
            this.colFiveYearClearanceAchieved.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colFiveYearClearanceAchieved.FieldName = "FiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.Name = "colFiveYearClearanceAchieved";
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowEdit = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.AllowFocus = false;
            this.colFiveYearClearanceAchieved.OptionsColumn.ReadOnly = true;
            this.colFiveYearClearanceAchieved.Visible = true;
            this.colFiveYearClearanceAchieved.VisibleIndex = 28;
            this.colFiveYearClearanceAchieved.Width = 150;
            // 
            // colTreeWithin3Meters
            // 
            this.colTreeWithin3Meters.Caption = "Tree Within 3M";
            this.colTreeWithin3Meters.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeWithin3Meters.FieldName = "TreeWithin3Meters";
            this.colTreeWithin3Meters.Name = "colTreeWithin3Meters";
            this.colTreeWithin3Meters.OptionsColumn.AllowEdit = false;
            this.colTreeWithin3Meters.OptionsColumn.AllowFocus = false;
            this.colTreeWithin3Meters.OptionsColumn.ReadOnly = true;
            this.colTreeWithin3Meters.Visible = true;
            this.colTreeWithin3Meters.VisibleIndex = 29;
            this.colTreeWithin3Meters.Width = 93;
            // 
            // colTreeClimbable
            // 
            this.colTreeClimbable.Caption = "Tree Climbable";
            this.colTreeClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeClimbable.FieldName = "TreeClimbable";
            this.colTreeClimbable.Name = "colTreeClimbable";
            this.colTreeClimbable.OptionsColumn.AllowEdit = false;
            this.colTreeClimbable.OptionsColumn.AllowFocus = false;
            this.colTreeClimbable.OptionsColumn.ReadOnly = true;
            this.colTreeClimbable.Visible = true;
            this.colTreeClimbable.VisibleIndex = 30;
            this.colTreeClimbable.Width = 91;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 32;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClientName2.Width = 224;
            // 
            // colSurveyDate1
            // 
            this.colSurveyDate1.Caption = "Survey - Date";
            this.colSurveyDate1.ColumnEdit = this.repositoryItemDateEdit2;
            this.colSurveyDate1.FieldName = "SurveyDate";
            this.colSurveyDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colSurveyDate1.Name = "colSurveyDate1";
            this.colSurveyDate1.OptionsColumn.AllowEdit = false;
            this.colSurveyDate1.OptionsColumn.AllowFocus = false;
            this.colSurveyDate1.OptionsColumn.ReadOnly = true;
            this.colSurveyDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colSurveyDate1.Width = 88;
            // 
            // colSurveyDescription
            // 
            this.colSurveyDescription.Caption = "Survey - Description";
            this.colSurveyDescription.FieldName = "SurveyDescription";
            this.colSurveyDescription.Name = "colSurveyDescription";
            this.colSurveyDescription.OptionsColumn.AllowEdit = false;
            this.colSurveyDescription.OptionsColumn.AllowFocus = false;
            this.colSurveyDescription.OptionsColumn.ReadOnly = true;
            this.colSurveyDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyDescription.Visible = true;
            this.colSurveyDescription.VisibleIndex = 18;
            this.colSurveyDescription.Width = 268;
            // 
            // colSurveyor1
            // 
            this.colSurveyor1.Caption = "Surveyor";
            this.colSurveyor1.FieldName = "Surveyor";
            this.colSurveyor1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Right;
            this.colSurveyor1.Name = "colSurveyor1";
            this.colSurveyor1.OptionsColumn.AllowEdit = false;
            this.colSurveyor1.OptionsColumn.AllowFocus = false;
            this.colSurveyor1.OptionsColumn.ReadOnly = true;
            this.colSurveyor1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyor1.Visible = true;
            this.colSurveyor1.VisibleIndex = 42;
            this.colSurveyor1.Width = 120;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start Date";
            this.colContractStartDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Width = 116;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End Date";
            this.colContractEndDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Width = 110;
            // 
            // colExchequerNumber
            // 
            this.colExchequerNumber.Caption = "Exchequer Number";
            this.colExchequerNumber.FieldName = "ExchequerNumber";
            this.colExchequerNumber.Name = "colExchequerNumber";
            this.colExchequerNumber.OptionsColumn.AllowEdit = false;
            this.colExchequerNumber.OptionsColumn.AllowFocus = false;
            this.colExchequerNumber.OptionsColumn.ReadOnly = true;
            this.colExchequerNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExchequerNumber.Width = 112;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Span Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContractValue.Visible = true;
            this.colContractValue.VisibleIndex = 11;
            this.colContractValue.Width = 74;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 0;
            this.colPoleNumber.Width = 105;
            // 
            // colPoleLastInspectionDate
            // 
            this.colPoleLastInspectionDate.Caption = "Pole Last Inspection";
            this.colPoleLastInspectionDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colPoleLastInspectionDate.FieldName = "PoleLastInspectionDate";
            this.colPoleLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleLastInspectionDate.Name = "colPoleLastInspectionDate";
            this.colPoleLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleLastInspectionDate.Visible = true;
            this.colPoleLastInspectionDate.VisibleIndex = 9;
            this.colPoleLastInspectionDate.Width = 117;
            // 
            // colPoleNextInspectionDate
            // 
            this.colPoleNextInspectionDate.Caption = "Pole Next Inspection";
            this.colPoleNextInspectionDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colPoleNextInspectionDate.FieldName = "PoleNextInspectionDate";
            this.colPoleNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colPoleNextInspectionDate.Name = "colPoleNextInspectionDate";
            this.colPoleNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colPoleNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colPoleNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colPoleNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colPoleNextInspectionDate.Visible = true;
            this.colPoleNextInspectionDate.VisibleIndex = 10;
            this.colPoleNextInspectionDate.Width = 120;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Tranformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 33;
            this.colIsTransformer.Width = 87;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 5;
            this.colCircuitNumber.Width = 111;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCircuitName.Width = 81;
            // 
            // colNoWorkRequired
            // 
            this.colNoWorkRequired.Caption = "No Work Required";
            this.colNoWorkRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colNoWorkRequired.FieldName = "NoWorkRequired";
            this.colNoWorkRequired.Name = "colNoWorkRequired";
            this.colNoWorkRequired.OptionsColumn.AllowEdit = false;
            this.colNoWorkRequired.OptionsColumn.AllowFocus = false;
            this.colNoWorkRequired.OptionsColumn.ReadOnly = true;
            this.colNoWorkRequired.Visible = true;
            this.colNoWorkRequired.VisibleIndex = 14;
            this.colNoWorkRequired.Width = 108;
            // 
            // colSurveyStatus
            // 
            this.colSurveyStatus.Caption = "Survey Status";
            this.colSurveyStatus.FieldName = "SurveyStatus";
            this.colSurveyStatus.Name = "colSurveyStatus";
            this.colSurveyStatus.OptionsColumn.AllowEdit = false;
            this.colSurveyStatus.OptionsColumn.AllowFocus = false;
            this.colSurveyStatus.OptionsColumn.ReadOnly = true;
            this.colSurveyStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyStatus.Visible = true;
            this.colSurveyStatus.VisibleIndex = 12;
            this.colSurveyStatus.Width = 96;
            // 
            // colSurveyStatusID1
            // 
            this.colSurveyStatusID1.Caption = "Survey Status ID";
            this.colSurveyStatusID1.FieldName = "SurveyStatusID";
            this.colSurveyStatusID1.Name = "colSurveyStatusID1";
            this.colSurveyStatusID1.OptionsColumn.AllowEdit = false;
            this.colSurveyStatusID1.OptionsColumn.AllowFocus = false;
            this.colSurveyStatusID1.OptionsColumn.ReadOnly = true;
            this.colSurveyStatusID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurveyStatusID1.Width = 103;
            // 
            // colG55CategoryID
            // 
            this.colG55CategoryID.Caption = "G55 Category ID";
            this.colG55CategoryID.FieldName = "G55CategoryID";
            this.colG55CategoryID.Name = "colG55CategoryID";
            this.colG55CategoryID.OptionsColumn.AllowEdit = false;
            this.colG55CategoryID.OptionsColumn.AllowFocus = false;
            this.colG55CategoryID.OptionsColumn.ReadOnly = true;
            this.colG55CategoryID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colG55CategoryID.Width = 102;
            // 
            // colG55CategoryDescription
            // 
            this.colG55CategoryDescription.Caption = "G55 Category";
            this.colG55CategoryDescription.FieldName = "G55CategoryDescription";
            this.colG55CategoryDescription.Name = "colG55CategoryDescription";
            this.colG55CategoryDescription.OptionsColumn.AllowEdit = false;
            this.colG55CategoryDescription.OptionsColumn.AllowFocus = false;
            this.colG55CategoryDescription.OptionsColumn.ReadOnly = true;
            this.colG55CategoryDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colG55CategoryDescription.Visible = true;
            this.colG55CategoryDescription.VisibleIndex = 21;
            this.colG55CategoryDescription.Width = 88;
            // 
            // colDeferred
            // 
            this.colDeferred.Caption = "Deferred";
            this.colDeferred.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDeferred.FieldName = "Deferred";
            this.colDeferred.Name = "colDeferred";
            this.colDeferred.OptionsColumn.AllowEdit = false;
            this.colDeferred.OptionsColumn.AllowFocus = false;
            this.colDeferred.OptionsColumn.ReadOnly = true;
            this.colDeferred.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferred.Visible = true;
            this.colDeferred.VisibleIndex = 34;
            this.colDeferred.Width = 64;
            // 
            // colDeferredUnitDescriptior
            // 
            this.colDeferredUnitDescriptior.Caption = "Deferred Unit Descriptor";
            this.colDeferredUnitDescriptior.FieldName = "DeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.Name = "colDeferredUnitDescriptior";
            this.colDeferredUnitDescriptior.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptior.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptior.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptior.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnitDescriptior.Visible = true;
            this.colDeferredUnitDescriptior.VisibleIndex = 36;
            this.colDeferredUnitDescriptior.Width = 138;
            // 
            // colDeferredUnitDescriptiorID
            // 
            this.colDeferredUnitDescriptiorID.Caption = "Deferred Unit Descriptor ID";
            this.colDeferredUnitDescriptiorID.FieldName = "DeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.Name = "colDeferredUnitDescriptiorID";
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowEdit = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.AllowFocus = false;
            this.colDeferredUnitDescriptiorID.OptionsColumn.ReadOnly = true;
            this.colDeferredUnitDescriptiorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnitDescriptiorID.Width = 152;
            // 
            // colDeferredUnits
            // 
            this.colDeferredUnits.Caption = "Deferred Units";
            this.colDeferredUnits.FieldName = "DeferredUnits";
            this.colDeferredUnits.Name = "colDeferredUnits";
            this.colDeferredUnits.OptionsColumn.AllowEdit = false;
            this.colDeferredUnits.OptionsColumn.AllowFocus = false;
            this.colDeferredUnits.OptionsColumn.ReadOnly = true;
            this.colDeferredUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferredUnits.Visible = true;
            this.colDeferredUnits.VisibleIndex = 35;
            this.colDeferredUnits.Width = 91;
            // 
            // colIsBaseClimbable
            // 
            this.colIsBaseClimbable.Caption = "Is Base Climbable";
            this.colIsBaseClimbable.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsBaseClimbable.FieldName = "IsBaseClimbable";
            this.colIsBaseClimbable.Name = "colIsBaseClimbable";
            this.colIsBaseClimbable.OptionsColumn.AllowEdit = false;
            this.colIsBaseClimbable.OptionsColumn.AllowFocus = false;
            this.colIsBaseClimbable.OptionsColumn.ReadOnly = true;
            this.colIsBaseClimbable.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIsBaseClimbable.Visible = true;
            this.colIsBaseClimbable.VisibleIndex = 31;
            this.colIsBaseClimbable.Width = 104;
            // 
            // colRevisitDate
            // 
            this.colRevisitDate.Caption = "Revisit Date";
            this.colRevisitDate.ColumnEdit = this.repositoryItemDateEdit2;
            this.colRevisitDate.FieldName = "RevisitDate";
            this.colRevisitDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRevisitDate.Name = "colRevisitDate";
            this.colRevisitDate.OptionsColumn.AllowEdit = false;
            this.colRevisitDate.OptionsColumn.AllowFocus = false;
            this.colRevisitDate.OptionsColumn.ReadOnly = true;
            this.colRevisitDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRevisitDate.Visible = true;
            this.colRevisitDate.VisibleIndex = 39;
            this.colRevisitDate.Width = 79;
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            this.colMapID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSpanInvoiced
            // 
            this.colSpanInvoiced.Caption = "Invoiced";
            this.colSpanInvoiced.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSpanInvoiced.FieldName = "SpanInvoiced";
            this.colSpanInvoiced.Name = "colSpanInvoiced";
            this.colSpanInvoiced.OptionsColumn.AllowEdit = false;
            this.colSpanInvoiced.OptionsColumn.AllowFocus = false;
            this.colSpanInvoiced.OptionsColumn.ReadOnly = true;
            this.colSpanInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSpanInvoiced.Width = 62;
            // 
            // colInvoiceNumber
            // 
            this.colInvoiceNumber.Caption = "Invoice #";
            this.colInvoiceNumber.FieldName = "InvoiceNumber";
            this.colInvoiceNumber.Name = "colInvoiceNumber";
            this.colInvoiceNumber.OptionsColumn.AllowEdit = false;
            this.colInvoiceNumber.OptionsColumn.AllowFocus = false;
            this.colInvoiceNumber.OptionsColumn.ReadOnly = true;
            this.colInvoiceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDateInvoiced
            // 
            this.colDateInvoiced.Caption = "Date Invoiced";
            this.colDateInvoiced.FieldName = "DateInvoiced";
            this.colDateInvoiced.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateInvoiced.Name = "colDateInvoiced";
            this.colDateInvoiced.OptionsColumn.AllowEdit = false;
            this.colDateInvoiced.OptionsColumn.AllowFocus = false;
            this.colDateInvoiced.OptionsColumn.ReadOnly = true;
            this.colDateInvoiced.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateInvoiced.Width = 88;
            // 
            // colDeferralReason
            // 
            this.colDeferralReason.Caption = "Deferral Reason";
            this.colDeferralReason.FieldName = "DeferralReason";
            this.colDeferralReason.Name = "colDeferralReason";
            this.colDeferralReason.OptionsColumn.AllowEdit = false;
            this.colDeferralReason.OptionsColumn.AllowFocus = false;
            this.colDeferralReason.OptionsColumn.ReadOnly = true;
            this.colDeferralReason.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferralReason.Visible = true;
            this.colDeferralReason.VisibleIndex = 37;
            this.colDeferralReason.Width = 99;
            // 
            // colDeferralRemarks
            // 
            this.colDeferralRemarks.Caption = "Deferral Remarks";
            this.colDeferralRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDeferralRemarks.FieldName = "DeferralRemarks";
            this.colDeferralRemarks.Name = "colDeferralRemarks";
            this.colDeferralRemarks.OptionsColumn.AllowEdit = false;
            this.colDeferralRemarks.OptionsColumn.AllowFocus = false;
            this.colDeferralRemarks.OptionsColumn.ReadOnly = true;
            this.colDeferralRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDeferralRemarks.Visible = true;
            this.colDeferralRemarks.VisibleIndex = 38;
            this.colDeferralRemarks.Width = 104;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 22;
            this.colEstimatedHours.Width = 99;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 23;
            this.colActualHours.Width = 82;
            // 
            // colAccessMapPath
            // 
            this.colAccessMapPath.Caption = "Access Map";
            this.colAccessMapPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colAccessMapPath.FieldName = "AccessMapPath";
            this.colAccessMapPath.Name = "colAccessMapPath";
            this.colAccessMapPath.OptionsColumn.ReadOnly = true;
            this.colAccessMapPath.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAccessMapPath.Visible = true;
            this.colAccessMapPath.VisibleIndex = 40;
            this.colAccessMapPath.Width = 100;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colTrafficMapPath
            // 
            this.colTrafficMapPath.Caption = "Traffic Map";
            this.colTrafficMapPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colTrafficMapPath.FieldName = "TrafficMapPath";
            this.colTrafficMapPath.Name = "colTrafficMapPath";
            this.colTrafficMapPath.OptionsColumn.ReadOnly = true;
            this.colTrafficMapPath.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTrafficMapPath.Visible = true;
            this.colTrafficMapPath.VisibleIndex = 41;
            this.colTrafficMapPath.Width = 100;
            // 
            // colShutdownID
            // 
            this.colShutdownID.Caption = "Shutdown ID";
            this.colShutdownID.FieldName = "ShutdownID";
            this.colShutdownID.Name = "colShutdownID";
            this.colShutdownID.OptionsColumn.AllowEdit = false;
            this.colShutdownID.OptionsColumn.AllowFocus = false;
            this.colShutdownID.OptionsColumn.ReadOnly = true;
            this.colShutdownID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShutdownID.Width = 74;
            // 
            // colClearanceDistanceUnder
            // 
            this.colClearanceDistanceUnder.Caption = "Clearance Distance (Under)";
            this.colClearanceDistanceUnder.FieldName = "ClearanceDistanceUnder";
            this.colClearanceDistanceUnder.Name = "colClearanceDistanceUnder";
            this.colClearanceDistanceUnder.OptionsColumn.AllowEdit = false;
            this.colClearanceDistanceUnder.OptionsColumn.AllowFocus = false;
            this.colClearanceDistanceUnder.OptionsColumn.ReadOnly = true;
            this.colClearanceDistanceUnder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClearanceDistanceUnder.Visible = true;
            this.colClearanceDistanceUnder.VisibleIndex = 25;
            this.colClearanceDistanceUnder.Width = 153;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 8;
            this.colHeldUpType.Width = 106;
            // 
            // colHeldUpTypeID
            // 
            this.colHeldUpTypeID.Caption = "Held-Up Type ID";
            this.colHeldUpTypeID.FieldName = "HeldUpTypeID";
            this.colHeldUpTypeID.Name = "colHeldUpTypeID";
            this.colHeldUpTypeID.OptionsColumn.AllowEdit = false;
            this.colHeldUpTypeID.OptionsColumn.AllowFocus = false;
            this.colHeldUpTypeID.OptionsColumn.ReadOnly = true;
            this.colHeldUpTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeldUpTypeID.Width = 98;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 1;
            this.colRegionName.Width = 111;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            this.colVoltageID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageID.Width = 69;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 6;
            this.colVoltageType.Width = 99;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 4;
            this.colFeederName.Width = 111;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 3;
            this.colPrimaryName.Width = 111;
            // 
            // colResponsiblePerson
            // 
            this.colResponsiblePerson.Caption = "Responsible Person";
            this.colResponsiblePerson.FieldName = "ResponsiblePerson";
            this.colResponsiblePerson.Name = "colResponsiblePerson";
            this.colResponsiblePerson.OptionsColumn.AllowEdit = false;
            this.colResponsiblePerson.OptionsColumn.AllowFocus = false;
            this.colResponsiblePerson.OptionsColumn.ReadOnly = true;
            this.colResponsiblePerson.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colResponsiblePerson.Visible = true;
            this.colResponsiblePerson.VisibleIndex = 7;
            this.colResponsiblePerson.Width = 112;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 2;
            this.colSubAreaName.Width = 111;
            // 
            // colShutdownHours
            // 
            this.colShutdownHours.Caption = "Shutdown Hours";
            this.colShutdownHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colShutdownHours.FieldName = "ShutdownHours";
            this.colShutdownHours.Name = "colShutdownHours";
            this.colShutdownHours.OptionsColumn.AllowEdit = false;
            this.colShutdownHours.OptionsColumn.AllowFocus = false;
            this.colShutdownHours.OptionsColumn.ReadOnly = true;
            this.colShutdownHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShutdownHours.Visible = true;
            this.colShutdownHours.VisibleIndex = 18;
            this.colShutdownHours.Width = 98;
            // 
            // DueDateDateEdit
            // 
            this.DueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "DueDate", true));
            this.DueDateDateEdit.EditValue = null;
            this.DueDateDateEdit.Location = new System.Drawing.Point(133, 155);
            this.DueDateDateEdit.MenuManager = this.barManager1;
            this.DueDateDateEdit.Name = "DueDateDateEdit";
            this.DueDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DueDateDateEdit.Properties.Mask.EditMask = "g";
            this.DueDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DueDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DueDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DueDateDateEdit.Size = new System.Drawing.Size(974, 20);
            this.DueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.DueDateDateEdit.TabIndex = 9;
            // 
            // DoneDateDateEdit
            // 
            this.DoneDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "DoneDate", true));
            this.DoneDateDateEdit.EditValue = null;
            this.DoneDateDateEdit.Location = new System.Drawing.Point(133, 179);
            this.DoneDateDateEdit.MenuManager = this.barManager1;
            this.DoneDateDateEdit.Name = "DoneDateDateEdit";
            this.DoneDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DoneDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DoneDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DoneDateDateEdit.Properties.Mask.EditMask = "g";
            this.DoneDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DoneDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DoneDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DoneDateDateEdit.Size = new System.Drawing.Size(974, 20);
            this.DoneDateDateEdit.StyleController = this.dataLayoutControl1;
            this.DoneDateDateEdit.TabIndex = 24;
            this.DoneDateDateEdit.EditValueChanged += new System.EventHandler(this.DoneDateDateEdit_EditValueChanged);
            this.DoneDateDateEdit.Validated += new System.EventHandler(this.DoneDateDateEdit_Validated);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 131);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(1071, 116);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling7);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 26;
            // 
            // DateCreatedDateEdit
            // 
            this.DateCreatedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07375UTShutdownEditBindingSource, "DateCreated", true));
            this.DateCreatedDateEdit.EditValue = null;
            this.DateCreatedDateEdit.Location = new System.Drawing.Point(133, 131);
            this.DateCreatedDateEdit.MenuManager = this.barManager1;
            this.DateCreatedDateEdit.Name = "DateCreatedDateEdit";
            this.DateCreatedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateCreatedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateCreatedDateEdit.Properties.Mask.EditMask = "g";
            this.DateCreatedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateCreatedDateEdit.Properties.ReadOnly = true;
            this.DateCreatedDateEdit.Size = new System.Drawing.Size(974, 20);
            this.DateCreatedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateCreatedDateEdit.TabIndex = 36;
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.CustomizationFormText = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 0);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(549, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForShutdownID
            // 
            this.ItemForShutdownID.Control = this.ShutdownIDTextEdit;
            this.ItemForShutdownID.CustomizationFormText = "Shutdown ID:";
            this.ItemForShutdownID.Location = new System.Drawing.Point(0, 0);
            this.ItemForShutdownID.Name = "ItemForShutdownID";
            this.ItemForShutdownID.Size = new System.Drawing.Size(549, 24);
            this.ItemForShutdownID.Text = "Shutdown ID:";
            this.ItemForShutdownID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForContractorID
            // 
            this.ItemForContractorID.Control = this.ContractorIDTextEdit;
            this.ItemForContractorID.Location = new System.Drawing.Point(0, 96);
            this.ItemForContractorID.Name = "ItemForContractorID";
            this.ItemForContractorID.Size = new System.Drawing.Size(1075, 24);
            this.ItemForContractorID.Text = "Contractor ID:";
            this.ItemForContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1143, 692);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup13,
            this.layoutControlItem2,
            this.emptySpaceItem3,
            this.simpleSeparator6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1123, 263);
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "Held-Up Details";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForReferenceNumber,
            this.tabbedControlGroup1});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 25);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Size = new System.Drawing.Size(1123, 238);
            this.layoutControlGroup13.Text = "Held-Up Details";
            // 
            // ItemForReferenceNumber
            // 
            this.ItemForReferenceNumber.Control = this.ReferenceNumberTextEdit;
            this.ItemForReferenceNumber.CustomizationFormText = "Reference Number:";
            this.ItemForReferenceNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForReferenceNumber.Name = "ItemForReferenceNumber";
            this.ItemForReferenceNumber.Size = new System.Drawing.Size(1099, 24);
            this.ItemForReferenceNumber.Text = "Reference Number:";
            this.ItemForReferenceNumber.TextSize = new System.Drawing.Size(94, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup16;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1099, 168);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup16,
            this.layoutControlGroup10,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "Details";
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDueDate,
            this.ItemForDoneDate,
            this.ItemForStatusID,
            this.ItemForEngineerName,
            this.ItemForContractorName});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(1075, 120);
            this.layoutControlGroup16.Text = "Details";
            // 
            // ItemForDueDate
            // 
            this.ItemForDueDate.Control = this.DueDateDateEdit;
            this.ItemForDueDate.CustomizationFormText = "Due Date:";
            this.ItemForDueDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForDueDate.Name = "ItemForDueDate";
            this.ItemForDueDate.Size = new System.Drawing.Size(1075, 24);
            this.ItemForDueDate.Text = "Due Date:";
            this.ItemForDueDate.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForDoneDate
            // 
            this.ItemForDoneDate.Control = this.DoneDateDateEdit;
            this.ItemForDoneDate.CustomizationFormText = "Done Date:";
            this.ItemForDoneDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForDoneDate.Name = "ItemForDoneDate";
            this.ItemForDoneDate.Size = new System.Drawing.Size(1075, 24);
            this.ItemForDoneDate.Text = "Done Date:";
            this.ItemForDoneDate.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 0);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(1075, 24);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForEngineerName
            // 
            this.ItemForEngineerName.Control = this.EngineerNameTextEdit;
            this.ItemForEngineerName.Location = new System.Drawing.Point(0, 72);
            this.ItemForEngineerName.Name = "ItemForEngineerName";
            this.ItemForEngineerName.Size = new System.Drawing.Size(1075, 24);
            this.ItemForEngineerName.Text = "Engineer Name:";
            this.ItemForEngineerName.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemForContractorName
            // 
            this.ItemForContractorName.Control = this.ContractorNameButtonEdit;
            this.ItemForContractorName.Location = new System.Drawing.Point(0, 96);
            this.ItemForContractorName.Name = "ItemForContractorName";
            this.ItemForContractorName.Size = new System.Drawing.Size(1075, 24);
            this.ItemForContractorName.Text = "Contractor Name:";
            this.ItemForContractorName.TextSize = new System.Drawing.Size(94, 13);
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup10.CaptionImageOptions.Image")));
            this.layoutControlGroup10.CustomizationFormText = "Remarks";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(1075, 120);
            this.layoutControlGroup10.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(1075, 120);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Creation Info";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateCreated,
            this.ItemforCreatedBy,
            this.emptySpaceItem1});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1075, 120);
            this.layoutControlGroup4.Text = "Creation Info";
            // 
            // ItemForDateCreated
            // 
            this.ItemForDateCreated.Control = this.DateCreatedDateEdit;
            this.ItemForDateCreated.CustomizationFormText = "Date Created:";
            this.ItemForDateCreated.Location = new System.Drawing.Point(0, 0);
            this.ItemForDateCreated.Name = "ItemForDateCreated";
            this.ItemForDateCreated.Size = new System.Drawing.Size(1075, 24);
            this.ItemForDateCreated.Text = "Date Created:";
            this.ItemForDateCreated.TextSize = new System.Drawing.Size(94, 13);
            // 
            // ItemforCreatedBy
            // 
            this.ItemforCreatedBy.Control = this.CreatedByTextEdit;
            this.ItemforCreatedBy.CustomizationFormText = "Created By:";
            this.ItemforCreatedBy.Location = new System.Drawing.Point(0, 24);
            this.ItemforCreatedBy.Name = "ItemforCreatedBy";
            this.ItemforCreatedBy.Size = new System.Drawing.Size(1075, 24);
            this.ItemforCreatedBy.Text = "Created By:";
            this.ItemforCreatedBy.TextSize = new System.Drawing.Size(94, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1075, 72);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dataNavigator1;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(245, 23);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(245, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(878, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(0, 23);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(1123, 2);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.simpleSeparator1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 263);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1123, 409);
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "Work";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 2);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(1123, 407);
            this.layoutControlGroup12.Text = "Linked Surveyed Poles";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl1;
            this.layoutControlItem3.CustomizationFormText = "Linked Surveyed Spans:";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1099, 361);
            this.layoutControlItem3.Text = "Linked Surveyed Spans:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(1123, 2);
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter
            // 
            this.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter
            // 
            this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter
            // 
            this.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07375_UT_Shutdown_EditTableAdapter
            // 
            this.sp07375_UT_Shutdown_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp07377_UT_Shutdown_Status_ListTableAdapter
            // 
            this.sp07377_UT_Shutdown_Status_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter
            // 
            this.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_UT_Shutdown_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1143, 748);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Shutdown_Edit";
            this.Text = "Edit Held-Up";
            this.Activated += new System.EventHandler(this.frm_UT_Shutdown_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Shutdown_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Shutdown_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ContractorNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07375UTShutdownEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EngineerNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReferenceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07377UTShutdownStatusListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShutdownIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07378UTShutdownEditLinkedSurveyedPolesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoneDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DoneDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCreatedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShutdownID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReferenceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDoneDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEngineerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateCreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforCreatedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07272UTWorkOrderEditLinkedMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07271UTWorkOrderEditLinkedSubContractorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit ShutdownIDTextEdit;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.DateEdit DueDateDateEdit;
        private DevExpress.XtraEditors.DateEdit DoneDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraEditors.DateEdit DateCreatedDateEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShutdownID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDueDate;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDoneDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateCreated;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private WoodPlanDataSet woodPlanDataSet;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit CreatedByTextEdit;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemforCreatedBy;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private System.Windows.Forms.BindingSource sp07272UTWorkOrderEditLinkedMapsBindingSource;
        private System.Windows.Forms.BindingSource sp07271UTWorkOrderEditLinkedSubContractorsBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter sp07271_UT_Work_Order_Edit_Linked_SubContractorsTableAdapter;
        private DataSet_UT_WorkOrderTableAdapters.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private System.Windows.Forms.BindingSource sp07352UTWorkOrderLinkedPermissionDocumentsBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource sp07375UTShutdownEditBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT_EditTableAdapters.sp07375_UT_Shutdown_EditTableAdapter sp07375_UT_Shutdown_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit ReferenceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReferenceNumber;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07377UTShutdownStatusListBindingSource;
        private DataSet_UTTableAdapters.sp07377_UT_Shutdown_Status_ListTableAdapter sp07377_UT_Shutdown_Status_ListTableAdapter;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private System.Windows.Forms.BindingSource sp07378UTShutdownEditLinkedSurveyedPolesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSpanClear;
        private DevExpress.XtraGrid.Columns.GridColumn colInfestationRate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colHotGloveRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colLinesmanPossible;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficManagementResolved;
        private DevExpress.XtraGrid.Columns.GridColumn colFiveYearClearanceAchieved;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeWithin3Meters;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyor1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExchequerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleLastInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colNoWorkRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colG55CategoryDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferred;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptior;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnitDescriptiorID;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferredUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colIsBaseClimbable;
        private DevExpress.XtraGrid.Columns.GridColumn colRevisitDate;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpanInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInvoiced;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralReason;
        private DevExpress.XtraGrid.Columns.GridColumn colDeferralRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessMapPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTrafficMapPath;
        private DataSet_UT_EditTableAdapters.sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter sp07378_UT_Shutdown_Edit_Linked_Surveyed_PolesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownID;
        private DevExpress.XtraGrid.Columns.GridColumn colClearanceDistanceUnder;
        private DevExpress.XtraEditors.TextEdit ContractorIDTextEdit;
        private DevExpress.XtraEditors.TextEdit EngineerNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEngineerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractorID;
        private DevExpress.XtraEditors.ButtonEdit ContractorNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsiblePerson;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colShutdownHours;
    }
}

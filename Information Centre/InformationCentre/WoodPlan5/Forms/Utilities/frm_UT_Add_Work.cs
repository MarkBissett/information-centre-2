﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Add_Work : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        ArrayList ArrayListFilteredPicklists;  // Holds all the picklists bound to sp01372 - used for iterating round to bind and unbind generic Enter, Leave and ButtonClick events //

        #endregion

        public frm_UT_Add_Work()
        {
            InitializeComponent();
        }

        private void frm_UT_Add_Work_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;

            ArrayListFilteredPicklists = new ArrayList();  // Note: Each GridLookUpEdit within the array should have it's tag value set to it's value in the SQL PicklistHeader Table //
            ArrayListFilteredPicklists.Add(intEnvironmentalIssues);

            sp00214_Master_Job_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00214_Master_Job_List_With_BlankTableAdapter.Fill(this.dataSet_AT.sp00214_Master_Job_List_With_Blank);

            sp00190_Contractor_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00190_Contractor_List_With_BlankTableAdapter.Fill(this.woodPlanDataSet.sp00190_Contractor_List_With_Blank);

            dateEditDateRaised.DateTime = DateTime.Now;
        }

        public override void PostOpen(object objParameter)
        {
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void intEnvironmentalIssues_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 902, "Job Rates");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00214_Master_Job_List_With_BlankTableAdapter.Fill(this.dataSet_AT.sp00214_Master_Job_List_With_Blank);
                }
            }
        }

        private void gridLookUpEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 1, "Contractors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp00190_Contractor_List_With_BlankTableAdapter.Fill(woodPlanDataSet.sp00190_Contractor_List_With_Blank);
                }
            }
        }








    }
}

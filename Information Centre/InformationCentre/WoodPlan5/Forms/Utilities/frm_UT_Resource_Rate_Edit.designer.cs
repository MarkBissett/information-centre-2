﻿namespace WoodPlan5
{
    partial class frm_UT_Resource_Rate_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Resource_Rate_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.dataSet_AS_DataEntry = new WoodPlan5.DataSet_AS_DataEntry();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.sp07438UTResourceRateListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lueQuoteCategoryID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07424UTQuoteCategoryItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueResourceID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07447UTResourceListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lueClientContractTypeID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07431UTClientContractTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.memDescription = new DevExpress.XtraEditors.MemoEdit();
            this.spnBuyRate = new DevExpress.XtraEditors.SpinEdit();
            this.spnSellRate = new DevExpress.XtraEditors.SpinEdit();
            this.lueItemUnitsID = new DevExpress.XtraEditors.LookUpEdit();
            this.sp07435UTItemUnitsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ceAllowEditBuyRate = new DevExpress.XtraEditors.CheckEdit();
            this.ceAllowEditSellRate = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForQuoteCategoryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBuyRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSellRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForItemUnitsID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientContractTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForResourceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllowEditBuyRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllowEditSellRate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp07438_UT_Resource_Rate_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter();
            this.sp07424_UT_Quote_Category_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_ItemTableAdapter();
            this.sp07447_UT_Resource_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07447_UT_Resource_ListTableAdapter();
            this.sp07428UTContractTypeListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07428_UT_Contract_Type_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07428_UT_Contract_Type_ListTableAdapter();
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter();
            this.sp07435_UT_Item_Units_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07435_UT_Item_Units_ListTableAdapter();
            this.tableAdapterManager1 = new WoodPlan5.DataSet_UT_QuoteTableAdapters.TableAdapterManager();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438UTResourceRateListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lueQuoteCategoryID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424UTQuoteCategoryItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueResourceID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07447UTResourceListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientContractTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueItemUnitsID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07435UTItemUnitsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowEditBuyRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowEditSellRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteCategoryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBuyRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemUnitsID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResourceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllowEditBuyRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllowEditSellRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07428UTContractTypeListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1384, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 725);
            this.barDockControlBottom.Size = new System.Drawing.Size(1384, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 699);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1384, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 699);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // dataSet_AS_DataEntry
            // 
            this.dataSet_AS_DataEntry.DataSetName = "dataSet_AS_DataEntry";
            this.dataSet_AS_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1384, 699);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(491, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.DataSource = this.sp07438UTResourceRateListBindingSource;
            this.editFormDataNavigator.Location = new System.Drawing.Point(503, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // sp07438UTResourceRateListBindingSource
            // 
            this.sp07438UTResourceRateListBindingSource.DataMember = "sp07438_UT_Resource_Rate_List";
            this.sp07438UTResourceRateListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueQuoteCategoryID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueResourceID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueClientContractTypeID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.memDescription);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnBuyRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.spnSellRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueItemUnitsID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceAllowEditBuyRate);
            this.editFormDataLayoutControlGroup.Controls.Add(this.ceAllowEditSellRate);
            this.editFormDataLayoutControlGroup.DataSource = this.sp07438UTResourceRateListBindingSource;
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1935, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1384, 699);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // lueQuoteCategoryID
            // 
            this.lueQuoteCategoryID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "QuoteCategoryID", true));
            this.lueQuoteCategoryID.Location = new System.Drawing.Point(118, 35);
            this.lueQuoteCategoryID.MenuManager = this.barManager1;
            this.lueQuoteCategoryID.Name = "lueQuoteCategoryID";
            this.lueQuoteCategoryID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueQuoteCategoryID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("QuoteCategory", "Quote Category", 88, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueQuoteCategoryID.Properties.DataSource = this.sp07424UTQuoteCategoryItemBindingSource;
            this.lueQuoteCategoryID.Properties.DisplayMember = "QuoteCategory";
            this.lueQuoteCategoryID.Properties.NullText = "";
            this.lueQuoteCategoryID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueQuoteCategoryID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueQuoteCategoryID.Properties.ValueMember = "QuoteCategoryID";
            this.lueQuoteCategoryID.Size = new System.Drawing.Size(570, 20);
            this.lueQuoteCategoryID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueQuoteCategoryID.TabIndex = 123;
            this.lueQuoteCategoryID.Tag = "Quote Category";
            this.lueQuoteCategoryID.EditValueChanged += new System.EventHandler(this.lueQuoteCategoryID_EditValueChanged);
            this.lueQuoteCategoryID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07424UTQuoteCategoryItemBindingSource
            // 
            this.sp07424UTQuoteCategoryItemBindingSource.DataMember = "sp07424_UT_Quote_Category_Item";
            this.sp07424UTQuoteCategoryItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueResourceID
            // 
            this.lueResourceID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "ResourceID", true));
            this.lueResourceID.Location = new System.Drawing.Point(798, 35);
            this.lueResourceID.MenuManager = this.barManager1;
            this.lueResourceID.Name = "lueResourceID";
            this.lueResourceID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueResourceID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ResourceDescription", "Resource Description", 111, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueResourceID.Properties.DataSource = this.sp07447UTResourceListBindingSource;
            this.lueResourceID.Properties.DisplayMember = "ResourceDescription";
            this.lueResourceID.Properties.NullText = "";
            this.lueResourceID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueResourceID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueResourceID.Properties.ValueMember = "ResourceID";
            this.lueResourceID.Size = new System.Drawing.Size(574, 20);
            this.lueResourceID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueResourceID.TabIndex = 124;
            this.lueResourceID.Tag = "Resource";
            this.lueResourceID.EditValueChanged += new System.EventHandler(this.lueResourceID_EditValueChanged);
            this.lueResourceID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07447UTResourceListBindingSource
            // 
            this.sp07447UTResourceListBindingSource.DataMember = "sp07447_UT_Resource_List";
            this.sp07447UTResourceListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // lueClientContractTypeID
            // 
            this.lueClientContractTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "ClientContractTypeID", true));
            this.lueClientContractTypeID.Location = new System.Drawing.Point(798, 59);
            this.lueClientContractTypeID.MenuManager = this.barManager1;
            this.lueClientContractTypeID.Name = "lueClientContractTypeID";
            this.lueClientContractTypeID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueClientContractTypeID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ClientName", "Client Name", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ContractType", "Contract Type", 79, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueClientContractTypeID.Properties.DataSource = this.sp07431UTClientContractTypeListBindingSource;
            this.lueClientContractTypeID.Properties.DisplayMember = "ContractType";
            this.lueClientContractTypeID.Properties.NullText = "";
            this.lueClientContractTypeID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueClientContractTypeID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueClientContractTypeID.Properties.ValueMember = "ClientContractTypeID";
            this.lueClientContractTypeID.Size = new System.Drawing.Size(574, 20);
            this.lueClientContractTypeID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueClientContractTypeID.TabIndex = 125;
            this.lueClientContractTypeID.Tag = "Client Contract Type";
            this.lueClientContractTypeID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07431UTClientContractTypeListBindingSource
            // 
            this.sp07431UTClientContractTypeListBindingSource.DataMember = "sp07431_UT_Client_Contract_Type_List";
            this.sp07431UTClientContractTypeListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // memDescription
            // 
            this.memDescription.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "Description", true));
            this.memDescription.Location = new System.Drawing.Point(24, 92);
            this.memDescription.MenuManager = this.barManager1;
            this.memDescription.Name = "memDescription";
            this.scSpellChecker.SetShowSpellCheckMenu(this.memDescription, true);
            this.memDescription.Size = new System.Drawing.Size(652, 95);
            this.scSpellChecker.SetSpellCheckerOptions(this.memDescription, optionsSpelling1);
            this.memDescription.StyleController = this.editFormDataLayoutControlGroup;
            this.memDescription.TabIndex = 126;
            this.memDescription.Tag = "Description";
            // 
            // spnBuyRate
            // 
            this.spnBuyRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "BuyRate", true));
            this.spnBuyRate.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnBuyRate.Location = new System.Drawing.Point(798, 109);
            this.spnBuyRate.MenuManager = this.barManager1;
            this.spnBuyRate.Name = "spnBuyRate";
            this.spnBuyRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnBuyRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnBuyRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnBuyRate.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnBuyRate.Size = new System.Drawing.Size(574, 20);
            this.spnBuyRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnBuyRate.TabIndex = 127;
            this.spnBuyRate.Tag = "Buy Rate";
            // 
            // spnSellRate
            // 
            this.spnSellRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "SellRate", true));
            this.spnSellRate.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spnSellRate.Location = new System.Drawing.Point(798, 133);
            this.spnSellRate.MenuManager = this.barManager1;
            this.spnSellRate.Name = "spnSellRate";
            this.spnSellRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spnSellRate.Properties.DisplayFormat.FormatString = "c2";
            this.spnSellRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.spnSellRate.Properties.MaxValue = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.spnSellRate.Size = new System.Drawing.Size(574, 20);
            this.spnSellRate.StyleController = this.editFormDataLayoutControlGroup;
            this.spnSellRate.TabIndex = 128;
            this.spnSellRate.Tag = "Sell Rate";
            // 
            // lueItemUnitsID
            // 
            this.lueItemUnitsID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "ItemUnitsID", true));
            this.lueItemUnitsID.Location = new System.Drawing.Point(798, 83);
            this.lueItemUnitsID.MenuManager = this.barManager1;
            this.lueItemUnitsID.Name = "lueItemUnitsID";
            this.lueItemUnitsID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueItemUnitsID.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", "add", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("lueItemUnitsID.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", "refresh", null, true)});
            this.lueItemUnitsID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Units", "Units", 34, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueItemUnitsID.Properties.DataSource = this.sp07435UTItemUnitsListBindingSource;
            this.lueItemUnitsID.Properties.DisplayMember = "Units";
            this.lueItemUnitsID.Properties.NullText = "";
            this.lueItemUnitsID.Properties.NullValuePrompt = "-- Please Select --";
            this.lueItemUnitsID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueItemUnitsID.Properties.ValueMember = "ItemUnitsID";
            this.lueItemUnitsID.Size = new System.Drawing.Size(574, 22);
            this.lueItemUnitsID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueItemUnitsID.TabIndex = 129;
            this.lueItemUnitsID.Tag = "Item Units";
            this.lueItemUnitsID.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.lueItemUnitsID_Properties_ButtonPressed);
            this.lueItemUnitsID.Validating += new System.ComponentModel.CancelEventHandler(this.lookUpEdit_Validating);
            // 
            // sp07435UTItemUnitsListBindingSource
            // 
            this.sp07435UTItemUnitsListBindingSource.DataMember = "sp07435_UT_Item_Units_List";
            this.sp07435UTItemUnitsListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // ceAllowEditBuyRate
            // 
            this.ceAllowEditBuyRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "AllowEditBuyRate", true));
            this.ceAllowEditBuyRate.Location = new System.Drawing.Point(798, 157);
            this.ceAllowEditBuyRate.MenuManager = this.barManager1;
            this.ceAllowEditBuyRate.Name = "ceAllowEditBuyRate";
            this.ceAllowEditBuyRate.Properties.Caption = "";
            this.ceAllowEditBuyRate.Size = new System.Drawing.Size(574, 19);
            this.ceAllowEditBuyRate.StyleController = this.editFormDataLayoutControlGroup;
            this.ceAllowEditBuyRate.TabIndex = 131;
            // 
            // ceAllowEditSellRate
            // 
            this.ceAllowEditSellRate.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07438UTResourceRateListBindingSource, "AllowEditSellRate", true));
            this.ceAllowEditSellRate.Location = new System.Drawing.Point(798, 180);
            this.ceAllowEditSellRate.MenuManager = this.barManager1;
            this.ceAllowEditSellRate.Name = "ceAllowEditSellRate";
            this.ceAllowEditSellRate.Properties.Caption = "";
            this.ceAllowEditSellRate.Size = new System.Drawing.Size(574, 19);
            this.ceAllowEditSellRate.StyleController = this.editFormDataLayoutControlGroup;
            this.ceAllowEditSellRate.TabIndex = 132;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQuoteCategoryID,
            this.emptySpaceItem1,
            this.ItemForBuyRate,
            this.ItemForSellRate,
            this.ItemForItemUnitsID,
            this.ItemForClientContractTypeID,
            this.ItemForResourceID,
            this.tabbedControlGroup1,
            this.ItemForAllowEditBuyRate,
            this.ItemForAllowEditSellRate});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1364, 656);
            // 
            // ItemForQuoteCategoryID
            // 
            this.ItemForQuoteCategoryID.Control = this.lueQuoteCategoryID;
            this.ItemForQuoteCategoryID.CustomizationFormText = "Quote Category:";
            this.ItemForQuoteCategoryID.Location = new System.Drawing.Point(0, 0);
            this.ItemForQuoteCategoryID.Name = "ItemForQuoteCategoryID";
            this.ItemForQuoteCategoryID.Size = new System.Drawing.Size(680, 24);
            this.ItemForQuoteCategoryID.Text = "Quote Category:";
            this.ItemForQuoteCategoryID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 168);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1364, 488);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBuyRate
            // 
            this.ItemForBuyRate.Control = this.spnBuyRate;
            this.ItemForBuyRate.CustomizationFormText = "Buy Rate:";
            this.ItemForBuyRate.Location = new System.Drawing.Point(680, 74);
            this.ItemForBuyRate.Name = "ItemForBuyRate";
            this.ItemForBuyRate.Size = new System.Drawing.Size(684, 24);
            this.ItemForBuyRate.Text = "Buy Rate:";
            this.ItemForBuyRate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForSellRate
            // 
            this.ItemForSellRate.Control = this.spnSellRate;
            this.ItemForSellRate.CustomizationFormText = "Sell Rate:";
            this.ItemForSellRate.Location = new System.Drawing.Point(680, 98);
            this.ItemForSellRate.Name = "ItemForSellRate";
            this.ItemForSellRate.Size = new System.Drawing.Size(684, 24);
            this.ItemForSellRate.Text = "Sell Rate:";
            this.ItemForSellRate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForItemUnitsID
            // 
            this.ItemForItemUnitsID.Control = this.lueItemUnitsID;
            this.ItemForItemUnitsID.CustomizationFormText = "Item Units:";
            this.ItemForItemUnitsID.Location = new System.Drawing.Point(680, 48);
            this.ItemForItemUnitsID.Name = "ItemForItemUnitsID";
            this.ItemForItemUnitsID.Size = new System.Drawing.Size(684, 26);
            this.ItemForItemUnitsID.Text = "Item Units:";
            this.ItemForItemUnitsID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForClientContractTypeID
            // 
            this.ItemForClientContractTypeID.Control = this.lueClientContractTypeID;
            this.ItemForClientContractTypeID.CustomizationFormText = "Client Contract Type:";
            this.ItemForClientContractTypeID.Location = new System.Drawing.Point(680, 24);
            this.ItemForClientContractTypeID.Name = "ItemForClientContractTypeID";
            this.ItemForClientContractTypeID.Size = new System.Drawing.Size(684, 24);
            this.ItemForClientContractTypeID.Text = "Client Contract Type:";
            this.ItemForClientContractTypeID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForResourceID
            // 
            this.ItemForResourceID.Control = this.lueResourceID;
            this.ItemForResourceID.CustomizationFormText = "Resource:";
            this.ItemForResourceID.Location = new System.Drawing.Point(680, 0);
            this.ItemForResourceID.Name = "ItemForResourceID";
            this.ItemForResourceID.Size = new System.Drawing.Size(684, 24);
            this.ItemForResourceID.Text = "Resource:";
            this.ItemForResourceID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup2;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(680, 144);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Description";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(656, 99);
            this.layoutControlGroup2.Text = "Description";
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.Control = this.memDescription;
            this.ItemForDescription.CustomizationFormText = "Description";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(656, 99);
            this.ItemForDescription.Text = "Description";
            this.ItemForDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForDescription.TextVisible = false;
            // 
            // ItemForAllowEditBuyRate
            // 
            this.ItemForAllowEditBuyRate.Control = this.ceAllowEditBuyRate;
            this.ItemForAllowEditBuyRate.CustomizationFormText = "Allow Edit Buy Rate :";
            this.ItemForAllowEditBuyRate.Location = new System.Drawing.Point(680, 122);
            this.ItemForAllowEditBuyRate.Name = "ItemForAllowEditBuyRate";
            this.ItemForAllowEditBuyRate.Size = new System.Drawing.Size(684, 23);
            this.ItemForAllowEditBuyRate.Text = "Allow Edit Buy Rate :";
            this.ItemForAllowEditBuyRate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForAllowEditSellRate
            // 
            this.ItemForAllowEditSellRate.Control = this.ceAllowEditSellRate;
            this.ItemForAllowEditSellRate.CustomizationFormText = "Allow Edit Sell Rate:";
            this.ItemForAllowEditSellRate.Location = new System.Drawing.Point(680, 145);
            this.ItemForAllowEditSellRate.Name = "ItemForAllowEditSellRate";
            this.ItemForAllowEditSellRate.Size = new System.Drawing.Size(684, 23);
            this.ItemForAllowEditSellRate.Text = "Allow Edit Sell Rate:";
            this.ItemForAllowEditSellRate.TextSize = new System.Drawing.Size(103, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(491, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(754, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(610, 23);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp07438_UT_Resource_Rate_ListTableAdapter
            // 
            this.sp07438_UT_Resource_Rate_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07424_UT_Quote_Category_ItemTableAdapter
            // 
            this.sp07424_UT_Quote_Category_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07447_UT_Resource_ListTableAdapter
            // 
            this.sp07447_UT_Resource_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07428UTContractTypeListBindingSource
            // 
            this.sp07428UTContractTypeListBindingSource.DataMember = "sp07428_UT_Contract_Type_List";
            this.sp07428UTContractTypeListBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp07428_UT_Contract_Type_ListTableAdapter
            // 
            this.sp07428_UT_Contract_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07431_UT_Client_Contract_Type_ListTableAdapter
            // 
            this.sp07431_UT_Client_Contract_Type_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07435_UT_Item_Units_ListTableAdapter
            // 
            this.sp07435_UT_Item_Units_ListTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager1
            // 
            this.tableAdapterManager1.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager1.sp07418_UT_Quote_ListTableAdapter = null;
            this.tableAdapterManager1.sp07421_UT_Quote_Item_ListTableAdapter = null;
            this.tableAdapterManager1.sp07424_UT_Quote_Category_Item_AddModeTableAdapter = null;
            this.tableAdapterManager1.sp07424_UT_Quote_Category_ItemTableAdapter = this.sp07424_UT_Quote_Category_ItemTableAdapter;
            this.tableAdapterManager1.sp07428_UT_Contract_Type_ListTableAdapter = this.sp07428_UT_Contract_Type_ListTableAdapter;
            this.tableAdapterManager1.sp07431_UT_Client_Contract_Type_ListTableAdapter = this.sp07431_UT_Client_Contract_Type_ListTableAdapter;
            this.tableAdapterManager1.sp07435_UT_Item_Units_ListTableAdapter = this.sp07435_UT_Item_Units_ListTableAdapter;
            this.tableAdapterManager1.sp07438_UT_Resource_Rate_ListTableAdapter = this.sp07438_UT_Resource_Rate_ListTableAdapter;
            this.tableAdapterManager1.sp07442_UT_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp07448_UT_POTransactionTableAdapter = null;
            this.tableAdapterManager1.sp07459_UT_Quote_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager1.sp07463_UT_Item_Units_ManagerTableAdapter = null;
            this.tableAdapterManager1.UpdateOrder = WoodPlan5.DataSet_UT_QuoteTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // frm_UT_Resource_Rate_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1384, 755);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Resource_Rate_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Resource Rates";
            this.Activated += new System.EventHandler(this.frm_UT_Resource_Rate_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Resource_Rate_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Resource_Rate_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AS_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07438UTResourceRateListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lueQuoteCategoryID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07424UTQuoteCategoryItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueResourceID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07447UTResourceListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueClientContractTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07431UTClientContractTypeListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnBuyRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spnSellRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueItemUnitsID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07435UTItemUnitsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowEditBuyRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceAllowEditSellRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQuoteCategoryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBuyRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSellRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForItemUnitsID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientContractTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForResourceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllowEditBuyRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllowEditSellRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07428UTContractTypeListBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntry dataSet_AS_DataEntry;
        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit lueQuoteCategoryID;
        private System.Windows.Forms.BindingSource sp07438UTResourceRateListBindingSource;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private DevExpress.XtraEditors.LookUpEdit lueResourceID;
        private DevExpress.XtraEditors.LookUpEdit lueClientContractTypeID;
        private DevExpress.XtraEditors.MemoEdit memDescription;
        private DevExpress.XtraEditors.SpinEdit spnBuyRate;
        private DevExpress.XtraEditors.SpinEdit spnSellRate;
        private DevExpress.XtraEditors.LookUpEdit lueItemUnitsID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQuoteCategoryID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForResourceID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientContractTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBuyRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSellRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForItemUnitsID;
        private DataSet_UT_QuoteTableAdapters.sp07438_UT_Resource_Rate_ListTableAdapter sp07438_UT_Resource_Rate_ListTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private System.Windows.Forms.BindingSource sp07424UTQuoteCategoryItemBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07424_UT_Quote_Category_ItemTableAdapter sp07424_UT_Quote_Category_ItemTableAdapter;
        private System.Windows.Forms.BindingSource sp07447UTResourceListBindingSource;
        private System.Windows.Forms.BindingSource sp07431UTClientContractTypeListBindingSource;
        private System.Windows.Forms.BindingSource sp07435UTItemUnitsListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07447_UT_Resource_ListTableAdapter sp07447_UT_Resource_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07428UTContractTypeListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07428_UT_Contract_Type_ListTableAdapter sp07428_UT_Contract_Type_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07431_UT_Client_Contract_Type_ListTableAdapter sp07431_UT_Client_Contract_Type_ListTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07435_UT_Item_Units_ListTableAdapter sp07435_UT_Item_Units_ListTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.CheckEdit ceAllowEditBuyRate;
        private DevExpress.XtraEditors.CheckEdit ceAllowEditSellRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllowEditBuyRate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllowEditSellRate;
        private DataSet_UT_QuoteTableAdapters.TableAdapterManager tableAdapterManager1;


    }
}

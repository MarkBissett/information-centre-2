using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_UT_WorkOrder_Add_Actions : WoodPlan5.frmBase_Modal
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        BaseObjects.GridCheckMarksSelection selection1;
        public string strExcludedActionIDs = "";
        public DataTable dtSelectedActions;
        public string strPassedInTreeIDsFromMap = "";  // Optional only filled in when this screen is called from Edit Work Order via the Tree Picker //

        #endregion
        
        public frm_UT_WorkOrder_Add_Actions()
        {
            InitializeComponent();
        }

        private void frm_UT_WorkOrder_Add_Actions_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500063;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            if (!string.IsNullOrEmpty(strPassedInTreeIDsFromMap)) this.Text = "Work Order - Link Actions [Activated From Map]";
            
            sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter.Connection.ConnectionString = strConnectionString;

            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            //dateEditDateFrom.DateTime = this.GlobalSettings.LiveStartDate;
            //dateEditDateTo.DateTime = this.GlobalSettings.LiveEndDate;

            gridControl1.ForceInitialize();  // Must be before LoadData() //
            LoadData();
            if (!string.IsNullOrEmpty(strPassedInTreeIDsFromMap))
            {
                GridView view = (GridView)gridControl1.MainView;
                view.ExpandAllGroups();
                selection1.SelectAll();
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            DateTime dtStart = dateEditDateFrom.DateTime;
            DateTime dtEnd = dateEditDateTo.DateTime;
            int intRetrieveType = 0;
            if (checkEdit2.Checked)
            {
                intRetrieveType = 1;
            }
            else if (checkEdit3.Checked)
            {
                intRetrieveType = 2;
            }
            
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp07275_UT_Work_Order_Actions_Available_For_AddingTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07275_UT_Work_Order_Actions_Available_For_Adding, intRetrieveType, dtStart, dtEnd);
            // Remove any rows already presnt in the current work Order //
            if (strExcludedActionIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = strExcludedActionIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.DeleteRow(intRowHandle);
                    }
                }
            }
            view.EndUpdate();
        }

        private void dateEditDateFrom_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditDateFrom.EditValue = null;
                }
            }
        }

        private void dateEditDateTo_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Delete)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to clear the entered date.\n\nAre you sure you wish to proceed?", "Clear Date Filter", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    dateEditDateTo.EditValue = null;
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Tick one or more actions to link before proceeding!", "Link Actions to Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Copy selected rows only into Public DataSet so it can be picked up by the calling screen. //
            dtSelectedActions = this.dataSet_UT_WorkOrder.sp07275_UT_Work_Order_Actions_Available_For_Adding.Clone();  // Copy structure to public dataset //
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1) dtSelectedActions.ImportRow(view.GetDataRow(i));
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Actions - adjust parameters then click Refresh button");
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        #endregion






    }
}


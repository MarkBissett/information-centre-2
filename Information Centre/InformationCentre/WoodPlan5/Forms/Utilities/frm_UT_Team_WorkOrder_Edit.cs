using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Skins;
using DevExpress.XtraEditors.Repository;  // Required by emptyEditor //
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using Utilities;  // Used by Datasets //
using LocusEffects;
using WoodPlan5.Reports;

using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;

using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;

namespace WoodPlan5
{
    public partial class frm_UT_Team_WorkOrder_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState2;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState7;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState9;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState10;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState11;  // Used by Grid View State Facilities //
        private int i_int_FocusedGrid = 1;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs7 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs9 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDs10 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
         
        private string strDefaultMapPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strDefaultPicturePath = "";  // Path for the Picture files //
        private string strDefaultWorkOrderDocumentPath = "";  // Path for the Picture files //
        private string strPermissionFilePath = "";  // Path for the Picture files //

        rpt_UT_Report_Layout_RP_Sheet rptReport;
        public string _ReportLayoutFolder = "";
        WaitDialogForm loadingForm = null;
        bool iBool_EditCompletionSheetLayoutEnabled = false;
        #endregion

        public frm_UT_Team_WorkOrder_Edit()
        {
            InitializeComponent();
        }

        private void frm_UT_Team_WorkOrder_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500067;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            xtraTabControl1.ShowTabHeader = DefaultBoolean.False;  // Hide main pagframe headers //

            emptyEditor = new RepositoryItem();

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 10018, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07262_UT_Risk_Assessment_Question_Answers_ListTableAdapter.Fill(dataSet_UT.sp07262_UT_Risk_Assessment_Question_Answers_List);

            sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "ActionID");
            gridControl1.ForceInitialize();

            sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState2 = new RefreshGridState(gridView2, "WorkOrderTeamID");

            sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "LinkedMapID");

            sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "SurveyPictureID");

            sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "CompletionSheetID");
            gridControl5.ForceInitialize();

            sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "CompletionSheetQuestionID");
            gridControl6.ForceInitialize();

            sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState9 = new RefreshGridState(gridView9, "SurveyedPoleID");
            gridControl9.ForceInitialize();

            sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState10 = new RefreshGridState(gridView10, "RiskAssessmentID");
            gridControl10.ForceInitialize();

            sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState11 = new RefreshGridState(gridView11, "PermissionDocumentID");

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultWorkOrderDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedWorkOrderDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Saved Work Order Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Work Order Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _ReportLayoutFolder = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "UtilitiesSavedReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for RP Sheet Report Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get RP Sheet Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!_ReportLayoutFolder.EndsWith("\\")) _ReportLayoutFolder += "\\";  // Add Backslash to end //
            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strPermissionFilePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Permission Document Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Saved Permission Document Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            sp07247_UT_Work_Order_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["DateCreated"] = DateTime.Now;
                        drNewRow["CreatedByStaffID"] = GlobalSettings.UserID;
                        drNewRow["CreatedBy"] = (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : GlobalSettings.UserSurname) + (string.IsNullOrEmpty(GlobalSettings.UserSurname) ? "" : ": ") + (string.IsNullOrEmpty(GlobalSettings.UserForename) ? "" : GlobalSettings.UserForename);
                        drNewRow["PaddedWorkOrderID"] = "Set on First Save";
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Add(drNewRow);
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Add(drNewRow);
                        this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp07247_UT_Work_Order_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    if (strFormMode == "edit")
                    {
                        iBool_AllowDelete = true;
                        iBool_AllowAdd = true;
                        iBool_AllowEdit = true;
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event //
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberButtonEdit.Focus();

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl3.Enabled = true;
                        gridControl4.Enabled = true;
                        gridControl11.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ReferenceNumberButtonEdit.Focus();

                        barButtonItemPrint.Enabled = true;
                        gridControl1.Enabled = true;
                        gridControl2.Enabled = true;
                        gridControl3.Enabled = true;
                        gridControl4.Enabled = true;
                        gridControl11.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ReferenceNumberButtonEdit.Focus();

                        barButtonItemPrint.Enabled = false;
                        gridControl1.Enabled = false;
                        gridControl2.Enabled = false;
                        gridControl3.Enabled = false;
                        gridControl4.Enabled = false;
                        gridControl11.Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                barButtonItemPrint.Enabled = false;
                gridControl1.Enabled = false;
                gridControl2.Enabled = false;
                gridControl3.Enabled = false;
                gridControl4.Enabled = false;
                gridControl11.Enabled = false;
            }
            SetMenuStatus();  // Just in case any default layout has permissions set wrongly //
        }

        private void SetEditorButtons()
        {
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.EndEdit();
                dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
                if (dsChanges != null)
                {
                    barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                    bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                    bbiFormSave.Enabled = true;
                    return true;
                }
                else
                {
                    this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource.EndEdit();
                    dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
                    if (dsChanges != null)
                    {
                        barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                        bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                        bbiFormSave.Enabled = true;
                        return true;
                    }
                    else
                    {
                        this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource.EndEdit();
                        dsChanges = this.dataSet_UT_WorkOrder.GetChanges();
                        if (dsChanges != null)
                        {
                            barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                            bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                            bbiFormSave.Enabled = true;
                            return true;
                        }
                        else
                        {
                            barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                            bbiSave.Enabled = false;
                            bbiFormSave.Enabled = false;
                            return false;
                        }
                    }
                }
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 1:  // Edit RP Completion Sheet Layout //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_EditCompletionSheetLayoutEnabled = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");
 
            //gridControl1.Enabled = (!iBool_AllowEdit || strFormMode == "blockedit" ? false : true);

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intWorkOrderID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            DateTime dtDateCompleted = Convert.ToDateTime("01/01/1900");
            if (currentRow != null)
            {
                intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                dtDateCompleted = (currentRow["DateCompleted"] == DBNull.Value ? Convert.ToDateTime("01/01/1900") : Convert.ToDateTime(currentRow["DateCompleted"]));
            }

            if (i_int_FocusedGrid == 1)  // Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                /*if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length == 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }*/
            }
            else if (i_int_FocusedGrid == 2)  // Contractors //
            {
                view = (GridView)gridControl2.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowEdit && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Maps //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                /*if (iBool_AllowAdd && intWorkOrderID > 0 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view")
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }*/
            }
            else if (i_int_FocusedGrid == 10)  // Linked Risk Assesments //
            {
                view = (GridView)gridControl10.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 11)  // Linked Pemission Documents //
            {
                view = (GridView)gridControl11.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && strFormMode != "view" && view.DataRowCount > 0);
            btnShowMap.Enabled = (iBool_AllowEdit && strFormMode != "view" && view.DataRowCount > 0);

            // Set enabled status of GridView2 navigator custom buttons //
            view = (GridView)gridControl2.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl2.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && intRowHandles.Length >= 1 && dtDateCompleted == Convert.ToDateTime("01/01/1900") && strFormMode != "view");

            // Set enabled status of GridView10 navigator custom buttons //
            view = (GridView)gridControl10.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length == 1);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            gridControl10.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length == 1);

            // Set enabled status of GridView4 navigator custom buttons //
            view = (GridView)gridControl11.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl11.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length > 0);
            gridControl11.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0);

            // Set enabled status of GridView5 navigator custom buttons //
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length == 1);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length == 1);

            // Set Enabled Status of Main Toolbar Buttons //
            bbiEditRPSheetLayout.Enabled = iBool_EditCompletionSheetLayoutEnabled;
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs10)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs10 != "") i_str_AddedRecordIDs10 = strNewIDs10;
        }


        private void frm_UT_Team_WorkOrder_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus == 99)  // Refresh Top Level data //
            {
                Load_Top_Level();
            }
            else if (UpdateRefreshStatus == 1)
            {
                LoadLinkedActions();
            }
            else if (UpdateRefreshStatus == 2)
            {
                LoadLinkedContractors();
            }
            else if (UpdateRefreshStatus == 3)
            {
                LoadLinkedMaps();
            }
            else if (UpdateRefreshStatus == 4)
            {
                Load_Linked_Pictures();
            }
            else if (UpdateRefreshStatus == 5)
            {
                Load_Site_Completion_Sheets();
            }
            else if (UpdateRefreshStatus == 6)
            {
                Load_Completion_Questions();
            }
            else if (UpdateRefreshStatus == 9)
            {
                Load_Risk_Locations();
            }
            else if (UpdateRefreshStatus == 10)
            {
                LoadLinkedRiskAssessments();
            }
            else if (UpdateRefreshStatus == 11)
            {
                LoadLinkedPermissionDocuments();
            }
            SetMenuStatus();
        }

        private void frm_UT_Team_WorkOrder_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Detach Validating Event from all Editors to track changes...  Attached on Form Load Event //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private bool Check_Operatives_Recorded()
        {
            // Check that each work order loaded has at least one operative recorded on site //
            GridView view = (GridView)gridControl2.MainView;
            int intInvalidWorkOrderCount = 0;
            int intOperativeCount = 0;
            //string strWorkOrderID = "";
            //DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            //if (currentRow != null)
            //{
            //    strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            //}
            //this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            //gridControl2.BeginUpdate();
            //foreach (DataRow dr in dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows)
            //{
            //    intOperativeCount = 0;
            //    try
            //    {
            //        sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors, dr["WorkOrderID"].ToString() + ",");

                    for (int i = 0; i < view.DataRowCount; i++)
                    {
                        intOperativeCount += Convert.ToInt32(view.GetRowCellValue(i, "TeamMemberCount"));
                    }
                    if (intOperativeCount <= 0) intInvalidWorkOrderCount++;
            //    }
            //    catch (Exception) { }
            //}
            //// Put Work Order grid back to original state //
            //sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors, strWorkOrderID);
            //this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            //gridControl2.EndUpdate();
            if (intInvalidWorkOrderCount > 0)
            {
                //fProgress.Close();
                //fProgress.Dispose();
                //string strErrorMessage = (intInvalidWorkOrderCount == 1 ? "1 loaded Work Order has" : intInvalidWorkOrderCount.ToString() + " loaded Work Orders have") + " no operatives signed in to the site - Please sign in at least one operative for " + (intInvalidWorkOrderCount == 1 ? "this site" : intInvalidWorkOrderCount.ToString() + "these sites") + " before proceeding.";
                //XtraMessageBox.Show(strErrorMessage, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                XtraMessageBox.Show("Record at least one operative on site before proceeding.", "Check Operatives on Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            return true;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            // Commit any outstanding changes within the grid control //
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            } 

            ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            // Save Work Order Header //
            this.sp07247UTWorkOrderEditBindingSource.EndEdit();
            try
            {
                this.sp07247_UT_Work_Order_EditTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Actions //
            this.sp07249UTWorkOrderManagerLinkedActionsEditBindingSource.EndEdit();
            try
            {
                this.sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked action changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked Sub-Contractors //
            //this.sp07294UTTeamWorkOrderLinkedSubContractorsBindingSource.EndEdit();
            //try
            //{
            //    this.sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            //}
            //catch (System.Exception ex)
            //{
            //    fProgress.Close();
            //    fProgress.Dispose();
            //    XtraMessageBox.Show("An error occurred while saving the linked sub-contractor changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            //    return "Error";
            //}

            // Save Linked Maps //
            this.sp07272UTWorkOrderEditLinkedMapsBindingSource.EndEdit();
            try
            {
                this.sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked map changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked RP Sheets //
            this.sp07304UTTeamWorkOrderSiteCompletionSheetsListBindingSource.EndEdit();
            try
            {
                this.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked RP Sheet changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Save Linked RP Sheet Questions //
            this.sp07307UTTeamWorkOrderCompletionSheetEditBindingSource.EndEdit();
            try
            {
                this.sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter.Update(dataSet_UT_WorkOrder);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the linked RP Sheet Question changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["WorkOrderID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            try
            {
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frm_UT_Team_WorkOrder_Manager")
                        {
                            var fParentForm = (frm_UT_Team_WorkOrder_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "", "", "", "", "", "");
                        }
                    }
                }
            }
            catch (Exception) { }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;

            int intLinkedRecordNew = 0;
            int intLinkedRecordModified = 0;
            int intLinkedRecordDeleted = 0;

            //int intLinkedRecordNew2 = 0;
            //int intLinkedRecordModified2 = 0;
            //int intLinkedRecordDeleted2 = 0;

            int intLinkedRecordNew3 = 0;
            int intLinkedRecordModified3 = 0;
            int intLinkedRecordDeleted3 = 0;

            int intLinkedRecordNew5 = 0;
            int intLinkedRecordModified5 = 0;
            int intLinkedRecordDeleted5 = 0;
            
            int intLinkedRecordNew6 = 0;
            int intLinkedRecordModified6 = 0;
            int intLinkedRecordDeleted6 = 0;
            
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Action Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted++;
                        break;
                }
            }

            /*view = (GridView)gridControl2.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Sub-Contractor Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew2++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified2++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted2++;
                        break;
                }
            }*/

            view = (GridView)gridControl3.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked Map Item Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew3++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified3++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted3++;
                        break;
                }
            }

            view = (GridView)gridControl5.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked RP Sheet Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_List.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_List.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew5++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified5++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted5++;
                        break;
                }
            }

            view = (GridView)gridControl6.MainView;
            view.PostEditor();
            view.CloseEditor();
            if (!view.UpdateCurrentRow())
            {
                XtraMessageBox.Show("Column Error on current row in Linked RP Sheet Questions Grid - Correct before procceeding.", "Check for Pending Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            for (int i = 0; i < this.dataSet_UT_WorkOrder.sp07307_UT_Team_Work_Order_Completion_Sheet_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_UT_WorkOrder.sp07307_UT_Team_Work_Order_Completion_Sheet_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intLinkedRecordNew6++;
                        break;
                    case DataRowState.Modified:
                        intLinkedRecordModified6++;
                        break;
                    case DataRowState.Deleted:
                        intLinkedRecordDeleted6++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intLinkedRecordNew > 0 || intLinkedRecordModified > 0 || intLinkedRecordDeleted > 0 || intLinkedRecordNew3 > 0 || intLinkedRecordModified3 > 0 || intLinkedRecordDeleted3 > 0 || intLinkedRecordNew5 > 0 || intLinkedRecordModified5 > 0 || intLinkedRecordDeleted5 > 0 || intLinkedRecordNew6 > 0 || intLinkedRecordModified6 > 0 || intLinkedRecordDeleted6 > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Work Order record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Work Order record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Work Order record(s)\n";
                if (intLinkedRecordNew > 0) strMessage += Convert.ToString(intLinkedRecordNew) + " New Linked Action record(s)\n";
                if (intLinkedRecordModified > 0) strMessage += Convert.ToString(intLinkedRecordModified) + " Updated Linked Action record(s)\n";
                if (intLinkedRecordDeleted > 0) strMessage += Convert.ToString(intLinkedRecordDeleted) + " Deleted Linked Action record(s)\n";
                if (intLinkedRecordNew3 > 0) strMessage += Convert.ToString(intLinkedRecordNew3) + " New Linked Map record(s)\n";
                if (intLinkedRecordModified3 > 0) strMessage += Convert.ToString(intLinkedRecordModified3) + " Updated Linked Map record(s)\n";
                if (intLinkedRecordDeleted3 > 0) strMessage += Convert.ToString(intLinkedRecordDeleted3) + " Deleted Linked Map record(s)\n";
                if (intLinkedRecordNew5 > 0) strMessage += Convert.ToString(intLinkedRecordNew5) + " New Linked RP Sheet record(s)\n";
                if (intLinkedRecordModified5 > 0) strMessage += Convert.ToString(intLinkedRecordModified5) + " Updated Linked RP Sheet record(s)\n";
                if (intLinkedRecordDeleted5 > 0) strMessage += Convert.ToString(intLinkedRecordDeleted5) + " Deleted Linked RP Sheet record(s)\n";
                if (intLinkedRecordNew6 > 0) strMessage += Convert.ToString(intLinkedRecordNew6) + " New Linked RP Sheet Question record(s)\n";
                if (intLinkedRecordModified6 > 0) strMessage += Convert.ToString(intLinkedRecordModified6) + " Updated Linked RP Sheet Question record(s)\n";
                if (intLinkedRecordDeleted6 > 0) strMessage += Convert.ToString(intLinkedRecordDeleted6) + " Deleted Linked RP Sheet Question record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonItemPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            if (intWorkOrderID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before creating a Work Order Document File.", "Create Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strPDFFile = (currentRow["WorkOrderPDF"] == null ? "" : currentRow["WorkOrderPDF"].ToString());

            // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
            bool boolProceed = true;
            string strMessage = CheckForPendingSave();
            if (!string.IsNullOrEmpty(strMessage))
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before creating the Work Order Document File!\n\nSave Changes?", "Create Work Order Document", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                }
                else
                {
                    boolProceed = false;  // The user said no to save changes //
                }
            }
            else  // No pending changes in screen //
            {
                boolProceed = true;
            }
            if (boolProceed)
            {
                string strDocumentDescription = currentRow["PaddedWorkOrderID"].ToString();

                frm_UT_WorkOrder_Create_Document fChildForm = new frm_UT_WorkOrder_Create_Document();
                fChildForm.MdiParent = this.MdiParent;
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm.splashScreenManager = splashScreenManager1;
                fChildForm.splashScreenManager.ShowWaitForm();
                fChildForm.strWorkOrderDocumentPath = strDefaultWorkOrderDocumentPath;
                fChildForm.strPicturePath = strDefaultPicturePath;
                fChildForm.strMapPath = strDefaultMapPath;
                fChildForm._strExistingPDFFileName = strPDFFile;
                fChildForm._intWorkOrderID = intWorkOrderID;
                fChildForm._boolViewOnly = true;
                fChildForm.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm, new object[] { null });
            }

       }

        public void Load_Top_Level()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            // Refresh main data since it has been changed by the Create Permission Document screen [may have a signature file and a physical PDF document] //
            try
            {
                sp07247_UT_Work_Order_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07247_UT_Work_Order_Edit, strRecordIDs, strFormMode);
            }
            catch (Exception)
            {
            }
        }

        private void LoadLinkedActions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp07249_UT_Work_Order_Manager_Linked_Actions_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit, strWorkOrderID);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void LoadLinkedContractors()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState2.SaveViewInfo();  // Store Grid View State //
            gridControl2.BeginUpdate();
            sp07294_UT_Team_Work_Order_Linked_SubContractorsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors, strWorkOrderID);
            this.RefreshGridViewState2.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl2.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs2 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs2.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl2.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WorkOrderTeamID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs2 = "";
            }
        }

        private void LoadLinkedMaps()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }

            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
            gridControl3.BeginUpdate();
            sp07272_UT_Work_Order_Edit_Linked_MapsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07272_UT_Work_Order_Edit_Linked_Maps, strWorkOrderID, strDefaultMapPath);
            this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl3.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedMapID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }

        private void Load_Linked_Pictures()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount == 0)
            {
                gridControl4.MainView.BeginUpdate(); 
                dataSet_UT_WorkOrder.sp07299_UT_Team_Work_Order_Action_Pictures_List.Rows.Clear();
                gridControl4.MainView.EndUpdate();
                i_str_AddedRecordIDs4 = "";
                return;
            }
            string strSelectedActionIDs = "";
            string strTreeIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedActionIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
                strTreeIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
            }

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState4.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07299_UT_Team_Work_Order_Action_Pictures_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07299_UT_Team_Work_Order_Action_Pictures_List, strSelectedActionIDs, strTreeIDs, strDefaultPicturePath);
                this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl4.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs4 != "")
            {
                strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs4 = "";
            }
        }

        private void Load_Site_Completion_Sheets()
        {
            // Get list of locations from Actions list //
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            string strRecordIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                gridControl5.MainView.BeginUpdate();
                dataSet_UT_WorkOrder.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_List.Rows.Clear();
                gridControl5.MainView.EndUpdate();
                i_str_AddedRecordIDs5 = "";
                return;
            }
            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
            for (int i = 0; i < intCount; i++)
            {
                strRecordIDs += view.GetRowCellValue(i, "SurveyedPoleID").ToString() + ',';
            }

            gridControl5.BeginUpdate();
            sp07304_UT_Team_Work_Order_Site_Completion_Sheets_ListTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07304_UT_Team_Work_Order_Site_Completion_Sheets_List, strRecordIDs);
            this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl5.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs5 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl5.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["CompletionSheetID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs5 = "";
            }
            Load_Completion_Questions();  // Load Related Question Answers //
        }

        private void Load_Completion_Questions()
        {
            // Get list of completion sheet IDs from parent list //
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            string strRecordIDs = "";
            GridView view = (GridView)gridControl5.MainView;
            int intCount = view.DataRowCount;
            if (intCount <= 0)
            {
                gridControl6.MainView.BeginUpdate();
                dataSet_UT_WorkOrder.sp07307_UT_Team_Work_Order_Completion_Sheet_Edit.Rows.Clear();
                gridControl6.MainView.EndUpdate();
                i_str_AddedRecordIDs6 = "";
                return;
            }
            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
            for (int i = 0; i < intCount; i++)
            {
                strRecordIDs += view.GetRowCellValue(i, "CompletionSheetID").ToString() + ',';
            }
            
            gridControl6.BeginUpdate();
            sp07307_UT_Team_Work_Order_Completion_Sheet_EditTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07307_UT_Team_Work_Order_Completion_Sheet_Edit, strRecordIDs);
            this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl6.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs6 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["CompletionSheetQuestionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }

        private void Load_Risk_Locations()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            
            string strWorkOrderID = "";
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                strWorkOrderID = (currentRow["WorkOrderID"] == null ? "" : currentRow["WorkOrderID"].ToString() + ",");
            }
            
            this.RefreshGridViewState9.SaveViewInfo();  // Store Grid View State //
            gridControl9.BeginUpdate();
            sp07309_UT_Team_Work_Order_Risk_Locations_ListTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07309_UT_Team_Work_Order_Risk_Locations_List, strWorkOrderID);
            this.RefreshGridViewState9.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl9.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs9 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs9.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl9.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyedPoleID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs9 = "";
            }
        }

        private void LoadLinkedRiskAssessments()
        {
            GridView view = (GridView)gridControl9.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount == 0)
            {
                gridControl10.MainView.BeginUpdate();
                dataSet_UT_WorkOrder.sp07310_UT_Team_Work_Order_Risk_Assessment_List.Rows.Clear();
                gridControl10.MainView.EndUpdate();
                i_str_AddedRecordIDs10 = "";
                return;
            }
            string strSurveyedPoleIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSurveyedPoleIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyedPoleID"])) + ',';
            }
            
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            view = null;
            gridControl10.MainView.BeginUpdate();
            this.RefreshGridViewState10.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07310_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.Fill(dataSet_UT_WorkOrder.sp07310_UT_Team_Work_Order_Risk_Assessment_List, strSurveyedPoleIDs);
                this.RefreshGridViewState10.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl10.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs10 != "")
            {
                strArray = i_str_AddedRecordIDs10.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl10.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskAssessmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs10 = "";
            }
        }

        private void LoadLinkedPermissionDocuments()
        {
            // Load Any Linked Permission Document (Linked via Actions) //
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
            }
            this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
            gridControl11.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07352_UT_Work_Order_Linked_Permission_Documents.Clear();
            }
            else
            {
                sp07352_UT_Work_Order_Linked_Permission_DocumentsTableAdapter.Fill(this.dataSet_UT_WorkOrder.sp07352_UT_Work_Order_Linked_Permission_Documents, strSelectedIDs);
                this.RefreshGridViewState11.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl11.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Actions Linked";
                    break;
                case "gridView2":
                    message = "No Sub-Contractors Linked";
                    break;
                case "gridView3":
                    message = "No Maps Linked available";
                    break;
                case "gridView4":
                    message = "No Work Pictures available - Use Start and Completed buttons on Work List to add pictures";
                    break;
                case "gridView5":
                    message = "No RP Sheets Linked";
                    break;
                case "gridView6":
                    message = "No RP Sheet Questions Linked";
                    break;
                case "gridView9":
                    message = "No Risk Locations available";
                    break;
                case "gridView10":
                    message = "No Linked Risk Assessments";
                    break;
                case "gridView11":
                    message = "No Permission Documents Linked to Actions - Use the Permission Document Manager to Link Actions";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();

            Load_Linked_Pictures();
            GridView view = (GridView)gridControl4.MainView;
            view.ExpandAllGroups();

            LoadLinkedPermissionDocuments();
            view = (GridView)gridControl11.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("map_view".Equals(e.Button.Tag))
                    {
                        View_Map();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "gridColumnButtonStart":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) == 1 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) == 3) e.RepositoryItem = emptyEditor;
                    break;
                case "gridColumnButtonOnHold":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) != 1) e.RepositoryItem = emptyEditor;
                    break;
                case "gridColumnButtonComplete":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) != 1) e.RepositoryItem = emptyEditor;
                    break;
                case "EstimatedHours":
                case "ActualHours":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) != 1) e.RepositoryItem = repositoryItemTextEditHoursDisabled;
                    break;
                case "TeamRemarks":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID")) != 1) e.RepositoryItem = repositoryItemMemoExEditTeamRemarksDisabled;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "gridColumnButtonStart":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) == 1 || Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) == 3) e.Cancel = true;
                    break;
                case "gridColumnButtonOnHold":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) != 1) e.Cancel = true;
                    break;
                case "gridColumnButtonComplete":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) != 1) e.Cancel = true;
                    break;
                /*case "EstimatedHours":
                case "ActualHours":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) != 1) e.Cancel = true;
                    break;
                case "TeamRemarks":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ActionStatusID")) != 1) e.Cancel = true;
                    break;*/
                default:
                    break;
            }
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = (view.DataRowCount >= 1 ? true : false);
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
             GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "EstimatedHours":
                case "ActualHours":
                case "TeamRemarks":
                    {
                        int intStatusID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ActionStatusID"));
                        if (intStatusID != 1) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
                        break;
                    }
            }
        }


        private void repositoryItemButtonEditStartJob_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (!Check_Operatives_Recorded()) return;  // Check that each work order loaded has at least one operative recorded on site - abort if not //

            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intActionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ActionID"));

            int intPictureCount = 0;  // Get linked start picture count //
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetPictureCount = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            GetPictureCount.ChangeConnectionString(strConnectionString);
            try
            {
                intPictureCount = Convert.ToInt32(GetPictureCount.sp07300_UT_Team_Work_Order_Get_Picture_Count(intActionID, 1));
            }
            catch (Exception) { return; }
            bool boolGetPicture = true;
            bool boolStartJob = true;
            if (intPictureCount > 0)  // At least one picture, see if user wants to add more //
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Would you like to take one or more starting pictures?", "Start Action", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) boolGetPicture = false;
            }
            if (boolGetPicture)  // Add more pictures or add first picture //
            {
                frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                fChildForm.strCaller = this.Name;
                fChildForm.strConnectionString = strConnectionString;
                fChildForm.strFormMode = "add";
                fChildForm.intAddToRecordID = intActionID;
                fChildForm.intAddToRecordTypeID = 4;  // 4 = Work Done By Team Action //
                fChildForm.intPictureTypeID = 1; // 1 = Start Picture, 2 = Completed Picture //
                fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                fChildForm.ShowDialog();
                int intNewPictureID = fChildForm._AddedPolePictureID;
                if (intNewPictureID != 0)  // At least 1 picture added so refresh picture grid and set flag to allow job to start //
                {
                    i_str_AddedRecordIDs4 = intNewPictureID.ToString() + ";";
                    this.RefreshGridViewState4.SaveViewInfo();
                    Load_Linked_Pictures();
                    boolStartJob = true;
                }
                else if (intPictureCount <= 0) // No pictures, so don't start job //
                {
                    boolStartJob = false;
                }
            }
            if (boolStartJob)
            {
                // Update status of Action to Started //
                view.SetFocusedRowCellValue("ActionStatusID", 1);  // 0 = To Be Started, 1 = Started, 2 = On-hold, 3 = Completed //
                view.SetFocusedRowCellValue("ActionStatus", "Started");
                view.SetFocusedRowCellValue("LastStartTime", DateTime.Now);
                view.UpdateCurrentRow();
                string.IsNullOrEmpty(SaveChanges(true));  // Save change - status change will toggle editors to allow editing for this row //
                SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //
            }

        }

        private void repositoryItemButtonEditOnHoldJob_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (!Check_Operatives_Recorded()) return;  // Check that each work order loaded has at least one operative recorded on site - abort if not //

            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intActionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ActionID"));
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to put this job on hold?", "Set Action On-Hold", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            
            // Update status of Action to On-Hold //
            view.SetFocusedRowCellValue("ActionStatusID", 2);  // 0 = To Be Started, 1 = Started, 2 = On-hold, 3 = Completed //
            view.SetFocusedRowCellValue("ActionStatus", "On-Hold");

            // Calculate elapsed time so far //
            DateTime StartTime = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("LastStartTime").ToString()) ? DateTime.MinValue : Convert.ToDateTime(view.GetFocusedRowCellValue("LastStartTime")));
            if (StartTime != DateTime.MinValue)
            {
                TimeSpan elapsedTime = DateTime.Now.Subtract(StartTime);
                decimal decTimeElapsed = (decimal)view.GetFocusedRowCellValue("TotalTimeTaken");
                decimal decExistingElapsedTime = (decimal)elapsedTime.TotalMinutes / 60;
                view.SetFocusedRowCellValue("TotalTimeTaken", decExistingElapsedTime + decTimeElapsed);
            }
            view.UpdateCurrentRow();
            string.IsNullOrEmpty(SaveChanges(true));  // Save change - status change will toggle editors to allow editing for this row //
        }

        private void repositoryItemButtonEditCompleteJob_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (!Check_Operatives_Recorded()) return;  // Check that each work order loaded has at least one operative recorded on site - abort if not //
            
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intActionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ActionID"));
            //if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to mark this job as Completed?\n\nIf you proceed you will also need to specify if you have achieved a 5 year clearance on this job and a revisit date if clearance has not been achieved.", "Complete Action", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            int intSurveyedPoleID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SurveyedPoleID"));
            
            // Get any existing Revisit Date //
            DateTime? dtRevisitDate = null;
            DataSet_UT_EditTableAdapters.QueriesTableAdapter GetDate = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            GetDate.ChangeConnectionString(strConnectionString);
            try
            {
                dtRevisitDate = Convert.ToDateTime(GetDate.sp07345_UT_Get_Surveyed_Pole_Revisit_Date(intSurveyedPoleID)); 
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while getting the Revisit Date for the Surveyed Pole - error [" + ex.Message + "]\n\nPlease try again - if the problem persists, please contact Technical Support.", "Complete Action", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DateTime dtSurveyDate = DateTime.MinValue;
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow != null)
            {
                try
                {
                    dtSurveyDate = Convert.ToDateTime(currentRow["SurveyDate"]);
                }
                catch (Exception) {}
            }

            frm_UT_Team_WorkOrder_Edit_Clearance_Achieved fChild = new frm_UT_Team_WorkOrder_Edit_Clearance_Achieved();
            fChild.GlobalSettings = this.GlobalSettings;
            fChild._dtRevisitDate = dtRevisitDate;
            fChild._dtSurveyDate = dtSurveyDate;
            if (fChild.ShowDialog() != DialogResult.OK)  // User Clicked OK on child form //
            {
                return;
            }
            int intAction = fChild._intClearanceAchieved;
            if (intAction != 1)  // Store the Revisit Date //
            {
                DataSet_UT_EditTableAdapters.QueriesTableAdapter SetDate = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                SetDate.ChangeConnectionString(strConnectionString);
                try
                {
                    SetDate.sp07346_UT_Set_Surveyed_Pole_Revisit_Date(intSurveyedPoleID, fChild._dtRevisitDate, intActionID);
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while setting the Revisit Date for the Surveyed Pole - error [" + ex.Message + "]\n\nPlease try again - if the problem persists, please contact Technical Support.", "Complete Action", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            int intPictureCount = 0;  // Get linked start picture count //
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetPictureCount = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            GetPictureCount.ChangeConnectionString(strConnectionString);
            try
            {
                intPictureCount = Convert.ToInt32(GetPictureCount.sp07300_UT_Team_Work_Order_Get_Picture_Count(intActionID, 2));
            }
            catch (Exception) { return; }
            bool boolGetPicture = true;
            bool boolCompleteJob = true;
            if (intPictureCount > 0)  // At least one picture, see if user wants to add more //
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Would you like to take one or more completion pictures?", "Complete Action", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) boolGetPicture = false;
            }
            if (boolGetPicture)  // Add more pictures or add first picture //
            {
                frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                fChildForm.strCaller = this.Name;
                fChildForm.strConnectionString = strConnectionString;
                fChildForm.strFormMode = "add";
                fChildForm.intAddToRecordID = intActionID;
                fChildForm.intAddToRecordTypeID = 4;  // 4 = Work Done By Team Action //
                fChildForm.intPictureTypeID = 2; // 1 = Start Picture, 2 = Completed Picture //
                fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                fChildForm.ShowDialog();
                int intNewPictureID = fChildForm._AddedPolePictureID;
                if (intNewPictureID != 0)  // At least 1 picture added so refresh picture grid and set flag to allow job to start //
                {
                    i_str_AddedRecordIDs4 = intNewPictureID.ToString() + ";";
                    this.RefreshGridViewState4.SaveViewInfo();
                    Load_Linked_Pictures();
                    boolCompleteJob = true;
                }
                else if (intPictureCount <= 0) // No pictures, so don't start job //
                {
                    boolCompleteJob = false;
                }
            }
            if (boolCompleteJob)
            {
                // Update status of Action to Completed //
                view.SetFocusedRowCellValue("ActionStatusID", 3);  // 0 = To Be Started, 1 = Started, 2 = On-hold, 3 = Completed //
                view.SetFocusedRowCellValue("ActionStatus", "Completed");
                view.SetFocusedRowCellValue("DateCompleted", DateTime.Now);
                // Calculate elapsed time so far //
                DateTime StartTime = (string.IsNullOrEmpty(view.GetFocusedRowCellValue("LastStartTime").ToString()) ? DateTime.MinValue : Convert.ToDateTime(view.GetFocusedRowCellValue("LastStartTime")));
                if (StartTime != DateTime.MinValue)
                {
                    TimeSpan elapsedTime = DateTime.Now.Subtract(StartTime);
                    decimal decTimeElapsed = (decimal)view.GetFocusedRowCellValue("TotalTimeTaken");
                    decimal decExistingElapsedTime = (decimal)elapsedTime.TotalMinutes / 60;
                    view.SetFocusedRowCellValue("TotalTimeTaken", decExistingElapsedTime + decTimeElapsed);
                }
                view.UpdateCurrentRow();

                // Check if all jobs on Work Order are completed so we can set status of work order to completed //
                if (currentRow != null)
                {
                    if (view.RowCount > 0)
                    {
                        bool boolAllCompleted = true;
                        for (int i = 0; i < view.RowCount; i++)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(i, "ActionStatusID")) != 3)
                            {
                                boolAllCompleted = false;
                                break;
                            }
                        }
                        if (boolAllCompleted) currentRow["StatusID"] = 3;  // 3 = Work Order Completed //
                    }
                }

                string.IsNullOrEmpty(SaveChanges(true));  // Save change - status change will toggle editors to allow editing for this row //
                SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

                // Reload Completion Sheets as the number of completed jobs for a specific sheet may now equal the total sheets so the Completions Sheet should be available for Starting //
                Load_Site_Completion_Sheets();
 
                // Check if all jobs at this location are complete so we can activate the Completion Sheet page //
                if (intSurveyedPoleID > 0)
                {
                    int intCompletionSheetID = 0;
                    DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetCompletionSheet = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                    GetCompletionSheet.ChangeConnectionString(strConnectionString);
                    try
                    {
                        intCompletionSheetID = Convert.ToInt32(GetCompletionSheet.sp07306_UT_Team_Work_Order_Get_Completion_Sheet_Status(intSurveyedPoleID));
                    }
                    catch (Exception) { return; }
                    if (intCompletionSheetID > 0) // All jobs at location finished so activate Completions Sheet page //
                    {
                        // Highlight correct Completion Sheet Row //
                        view = (GridView)gridControl5.MainView;
                        int intFoundRow = view.LocateByValue(0, view.Columns["CompletionSheetID"], intCompletionSheetID);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.FocusedRowHandle = intFoundRow;
                            view.MakeRowVisible(intFoundRow, false);
                        }
                        view.FocusedColumn = gridColumnButtonStart2;

                        barCheckItemRPSheet.Checked = true;  // Activate Completion Sheet page //
                        xtraTabControl1.SelectedTabPage = xtraTabPageRPSheet;
                        Application.DoEvents();

                        LocusEffectsProvider locusEffectsProvider1 = new LocusEffectsProvider();
                        locusEffectsProvider1.Initialize();
                        locusEffectsProvider1.FramesPerSecond = 30;
                        ArrowLocusEffect m_customArrowLocusEffect1 = new ArrowLocusEffect();
                        m_customArrowLocusEffect1.Name = "CustomeArrow1";
                        m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                        m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                        m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                        m_customArrowLocusEffect1.MovementCycles = 20;
                        m_customArrowLocusEffect1.MovementAmplitude = 200;
                        m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                        m_customArrowLocusEffect1.LeadInTime = 0; //msec
                        m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                        m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                        locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                        // Get Cells position on screen so Locus Effect Arrow can point to it //
                        GridViewInfo info = view.GetViewInfo() as GridViewInfo;
                        GridCellInfo cellInfo = info.GetGridCellInfo(view.FocusedRowHandle, view.FocusedColumn);
                        System.Drawing.Rectangle rect = RectangleToScreen(cellInfo.Bounds);
                        if (rect.X > 0 && rect.Y > 0)
                        {
                            // Draw Locus Effect on object so user can see it //
                            System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, xtraTabControl1.Location.Y + rect.Y + rect.Height - 10);
                            locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                        }
                    }
                }
            }
        }

        #region Editor EditValueChanged
        
        private void repositoryItemMemoExEdit1_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEditHours_EditValueChanged(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        public override void OnShowMapEvent(object sender, EventArgs e)
        {
            View_Map();
        }

        private void View_Map()
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (view.DataRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load Work Order into Mapping - the current Work Order has no work.", "Load Work Order into Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            // Get Highlight Colour //
            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            int intHighlightColour = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesWorkOrderMapTreeHighlightColour"));

            // Get Unique list of Survey IDs //
            string strSurveyIDs = ",";
            string strCurrentID = "";
            string strTreeIDs = ",";
            string strTreeID = "";
            string strPoleIDs = ",";
            string strPoleID = "";
            string strSelectedTreeIDs = ",";
            string strSelectedPoleIDs = ",";
            for (int i = 0; i < view.RowCount; i++)
            {
                strCurrentID = view.GetRowCellValue(i, "SurveyID").ToString();
                if (!(string.IsNullOrEmpty(strCurrentID) || strCurrentID == "0"))
                {
                    if (!strSurveyIDs.Contains("," + strCurrentID + ",")) strSurveyIDs += strCurrentID + ",";
                }
                strTreeID = view.GetRowCellValue(i, "TreeID").ToString();
                if (!(string.IsNullOrEmpty(strTreeID) || strTreeID == "0"))
                {
                    if (!strTreeIDs.Contains("," + strTreeID + ",")) strTreeIDs += strTreeID + ",";
                }
                strPoleID = view.GetRowCellValue(i, "PoleID").ToString();
                if (!(string.IsNullOrEmpty(strPoleID) || strPoleID == "0"))
                {
                    if (!strPoleIDs.Contains("," + strPoleID + ",")) strPoleIDs += strPoleID + ",";
                }
            }
            foreach (int intRowHandle in intRowHandles)
            {
                if (!strSelectedTreeIDs.Contains("," + Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ","))
                {
                    strSelectedTreeIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ",";
                }
                if (!strSelectedPoleIDs.Contains("," + Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ","))
                {
                    strSelectedPoleIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleID")) + ",";
                }
            }

            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strSurveyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Survey - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "View Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strTreeIDs = strTreeIDs.TrimStart(',');
            if (strTreeIDs.Length < 2)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Tree - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "View Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intSelectedSurveyID = Convert.ToInt32(strArray[0]);

            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(Convert.ToInt32(strArray[0])).ToString();
                delimiters = new char[] { '|' };
                string[] strArray2 = null;
                strArray2 = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray2.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray2[0]);
                    strWorkspaceName = strArray2[1].ToString();
                    intRegionID = Convert.ToInt32(strArray2[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is linked to a Survey, but the survey is not linked to a region with a Mapping Workspace - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "View Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Close any open instance of the mapping //
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Close();
                    }
                }
            }
            catch (Exception)
            {
            }

            // Open Map and load in just the poles and trees with work on this work order //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSelectedSurveyID;
            frmInstance._SurveyMode = "WorkOrderMap"; //"PermissionDocumentMap";
            frmInstance._PassedInRecordType = 1;  // 1 = Permision Document, 2 = Work Order //
            frmInstance._PassedInTreeIDs = strTreeIDs;
            frmInstance._PassedInPoleIDs = strPoleIDs;
            frmInstance._PassedInPermissionDocumentID = 0;
            frmInstance._PassedInPermissionDocumentDescription = "";
            frmInstance.strHighlightedAssetIDs = strSelectedTreeIDs;
            frmInstance.intInitialHighlightColour = intHighlightColour;
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
            // frmInstance.Scale_Map_To_Visible_Objects(false);  // Commented out [MB, 07/04/2015] since we don't want map to zoom out to view all loaded objects //
        }

        #endregion


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    break;
                 default:
                    break;
            }
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }


        private void repositoryItemButtonEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            // Set Team on Site button //
            Edit_Record();
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit1_EditValueChanged_1(object sender, EventArgs e)
        {
            gridView2.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }

        #endregion

        #endregion


        #region GridView3

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView3;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("up".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow <= 0) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) - 1);
                        view.SetRowCellValue(intFocusedRow - 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow - 1, "MapOrder")) + 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    else if ("down".Equals(e.Button.Tag))
                    {
                        int intFocusedRow = view.FocusedRowHandle;
                        if (intFocusedRow == GridControl.InvalidRowHandle || intFocusedRow >= view.DataRowCount - 1) return;

                        view.BeginDataUpdate();
                        view.BeginSort();
                        view.SetRowCellValue(intFocusedRow, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow, "MapOrder")) + 1);
                        view.SetRowCellValue(intFocusedRow + 1, "MapOrder", Convert.ToInt32(view.GetRowCellValue(intFocusedRow + 1, "MapOrder")) - 1);
                        view.EndSort();
                        view.EndDataUpdate();
                    }
                    break;
                default:
                    break;
            }
        }

        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit3_EditValueChanged(object sender, EventArgs e)
        {
            gridView3.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }

        }

        #endregion

        #endregion


        #region GridView4

        private void gridView4_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 4;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        Load_Linked_Pictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView5

        // Notes: 09/04/2015, MB - Ability to prevent data entry once RP Sheet completed turned off as a result of change request. //

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "gridColumnButtonStart2":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 0 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CompletedActions")) <= 0 || Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CompletedActions")) != Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "TotalActions"))) e.RepositoryItem = emptyEditor;
                    break;
                case "gridColumnButtonComplete2":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = emptyEditor;
                    break;
              /*  case "SiteCompleteFitToBill":
                case "FurtherWorkRequired":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = repositoryItemCheckEditDisabled;
                    break;
                case "FinalClearanceDistance":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = repositoryItemTextEdit2DPMetresDisabled;
                    break;
                case "TreesFelledCount":
                case "StumpToGrindCount":
                case "StumpsTreatedCount":
                case "EcoPlugsAppliedCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = repositoryItemTextEdit0DPDisabled;
                    break;
                case "Remarks":
                case "FurtherWorkRemarks":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = repositoryItemMemoExEditTeamRemarksDisabled;
                    break;
                case "RevisitDate":
                case "DateTimeCompleted":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID")) != 1) e.RepositoryItem = repositoryItemTextEditDateTimeDisabled;
                    break;*/
                default:
                    break;
                    
            }
        }

        private void gridView5_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "gridColumnButtonStart2":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("StatusID")) != 0 || Convert.ToInt32(view.GetFocusedRowCellValue("CompletedActions")) <= 0 || Convert.ToInt32(view.GetFocusedRowCellValue("CompletedActions")) != Convert.ToInt32(view.GetFocusedRowCellValue("TotalActions"))) e.Cancel = true;
                    break;
                case "gridColumnButtonComplete2":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("StatusID")) != 1) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 5;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView5_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
        }

        private void gridView5_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                /*case "SiteCompleteFitToBill":
                case "FurtherWorkRequired":
                case "FinalClearanceDistance":
                case "TreesFelledCount":
                case "StumpToGrindCount":
                case "StumpsTreatedCount":
                case "EcoPlugsAppliedCount":
                case "Remarks":
                case "FurtherWorkRemarks":
                case "RevisitDate":
                case "DateTimeCompleted":
                    {
                        int intStatusID = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StatusID"));
                        if (intStatusID != 1) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
                        break;
                    }*/
                case "JobsDoneStatus":
                    {
                        int intCompletedActions = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "CompletedActions"));
                        int intTotalActions = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "TotalActions"));
                        if (intCompletedActions < intTotalActions || intTotalActions <= 0) e.Appearance.ForeColor = Color.Red;
                        break;
                    }
            }
        }

        bool internalRowFocusing;
        private void gridView5_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            FilterGrids();
        }

        private void gridView5_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 5;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Linked_Questions();
                    }
                    else if ("create".Equals(e.Button.Tag))
                    {
                        Create_Linked_Questions();
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Linked_Questions()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intCompletionSheetID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetID"));
            if (intCompletionSheetID <= 0) return;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to delete the linked questions - any answers provided will also be deleted!\n\nAre you sure you wish to proceed?", "Delete RP Sheet Questions", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Deleting...");
            
            DataSet_UT_EditTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            RemoveRecords.ChangeConnectionString(strConnectionString);
            try
            {
                RemoveRecords.sp07407_UT_Delete_RP_Sheet_Questions(intCompletionSheetID);
            }
            catch (Exception)
            {
            }
            Load_Completion_Questions();
            
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }

        private void Create_Linked_Questions()
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.SelectedRowsCount != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create RP Sheet questions - select just 1 RP sheet before proceeding.", "Create RP Sheet Questions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView ChildView = (GridView)gridControl6.MainView;
            if (ChildView.DataRowCount > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to create RP Sheet questions - there are existing questions present.\n\nYou must delete these by clicking the Delete button before proceeding.", "Create RP Sheet Questions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intCompletionSheetID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetID"));
            int intClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
            if (intCompletionSheetID <= 0 || intClientID <= 0) return;
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to create linked RP Sheet Questions.\n\nAre you sure you wish to proceed?", "Create RP Sheet Questions", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Creating...");

            DataSet_UT_EditTableAdapters.QueriesTableAdapter CreateRecords = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            CreateRecords.ChangeConnectionString(strConnectionString);
            try
            {
                CreateRecords.sp07408_UT_Create_RP_Sheet_Questions(intCompletionSheetID, intClientID);
            }
            catch (Exception)
            {
            }
            Load_Completion_Questions();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
        }


        private void repositoryItemButtonEditStart2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intCompletionSheetID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetID"));

            // Update status of Completion Sheet to Started //
            view.SetFocusedRowCellValue("StatusID", 1);  // 0 = To Be Started, 1 = Started, 2 = Completed, 3 = Audited //
            view.SetFocusedRowCellValue("StatusDescription","Started");  // 0 = To Be Started, 1 = Started, 2 = Completed, 3 = Audited //
            
            view.UpdateCurrentRow();
            string.IsNullOrEmpty(SaveChanges(true));  // Save change - status change will toggle editors to allow editing for this row //
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //
            view.FocusedColumn = colSiteCompleteFitToBill;

            // Update linked questions grid so editors and colors are updated now they are enabled //
            view = (GridView)gridControl6.MainView;
            view.FocusedColumn = colQuestionNotes;
            gridControl6.Focus();
            gridControl6.Refresh(); 
        }

        private void repositoryItemButtonEditComplete2_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intCompletionSheetID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CompletionSheetID"));

            // Check required fields are present //
            string strErrorMessage = "";
            decimal decFinalClearanceDistance = (decimal)0.00;   
            if (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("FinalClearanceDistance").ToString())) decFinalClearanceDistance = Convert.ToDecimal(view.GetFocusedRowCellValue("FinalClearanceDistance"));
            if (decFinalClearanceDistance == (decimal)0.00) strErrorMessage += "Missing Final Clearance Distance\r\n";

            //DateTime dtRevisitDate = DateTime.MinValue;
            //if (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("RevisitDate").ToString())) dtRevisitDate = Convert.ToDateTime(view.GetFocusedRowCellValue("RevisitDate"));
            //if (dtRevisitDate == DateTime.MinValue) strErrorMessage += "Missing Revisit Date\r\n";

            int intFurtherWorkRequired = 0;
            string strFurtherWorkRemarks = "";
            if (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("FurtherWorkRequired").ToString())) intFurtherWorkRequired = Convert.ToInt32(view.GetFocusedRowCellValue("FurtherWorkRequired"));
            if (!string.IsNullOrEmpty(view.GetFocusedRowCellValue("FurtherWorkRemarks").ToString())) strFurtherWorkRemarks = view.GetFocusedRowCellValue("FurtherWorkRemarks").ToString();
            if (intFurtherWorkRequired > 0 && string.IsNullOrEmpty(strFurtherWorkRemarks)) strErrorMessage += "Further Work Required Ticked but no Description of Work\r\n";

            if (!string.IsNullOrEmpty(strErrorMessage))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to complete RP Sheet - the following <b>errors</b> are present...\r\n\r\n" + strErrorMessage, "Complete RP Sheet", MessageBoxButtons.OK, MessageBoxIcon.Stop, DefaultBoolean.True);
                return;
            }

            // Get count of unknown answers from questions //
            int intUnknownCount = 0;
            GridView questionView = (GridView)gridControl6.MainView;
            int intRowCount = questionView.RowCount;  // RowCount oinstead of DataRowCount as we only want to see the filtered rows // 
            for (int i = 0; i < intRowCount; i++)
            {
                if (string.IsNullOrEmpty(questionView.GetRowCellValue(i, "TeamAnswer").ToString()))
                {
                    intUnknownCount++;
                }
                else if (Convert.ToInt32(questionView.GetRowCellValue(i, "TeamAnswer")) == 0) intUnknownCount++;
            }
            if (intUnknownCount > 0)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have one or more questions answered as <b>Unknown</b> - are you sure you wish to mark the RP Sheet as completed?", "Complete RP Sheet", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.No) return;
            }

            // If we are here - Ok to update status of Completion Sheet to Started //
            view.SetFocusedRowCellValue("StatusID", 2);  // 0 = To Be Started, 1 = Started, 2 = Completed, 3 = Audited //
            view.SetFocusedRowCellValue("StatusDescription", "Completed");  // 0 = To Be Started, 1 = Started, 2 = Completed, 3 = Audited //
            view.SetFocusedRowCellValue("DateTimeCompleted", DateTime.Now);
            view.UpdateCurrentRow();
            string.IsNullOrEmpty(SaveChanges(true));  // Save change - status change will toggle editors to allow editing for this row //
            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //
            view.FocusedColumn = colSiteCompleteFitToBill;

            // Update linked questions grid so editors and colors are updated now they are disabled //
            view = (GridView)gridControl6.MainView;
            view.FocusedColumn = colQuestionNotes;
            gridControl6.Focus();
            gridControl6.Refresh();
        }


        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit5_EditValueChanged(object sender, EventArgs e)
        {
            gridView5.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemCheckEdit3_EditValueChanged(object sender, EventArgs e)
        {
            gridView5.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemTextEditDateTime2_EditValueChanged(object sender, EventArgs e)
        {
            gridView5.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEdit2DPMetres_EditValueChanged(object sender, EventArgs e)
        {
            gridView5.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemSpinEdit0DP_EditValueChanged(object sender, EventArgs e)
        {
            gridView5.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        #endregion


        #region GridView6
        
        // Notes: 09/04/2015, MB - Ability to prevent data entry once RP Sheet completed turned off as a result of change request. //

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 6;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView6_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intStatus = Convert.ToInt32(view.GetFocusedRowCellValue("StatusID"));
            view = (GridView)sender;
            /*switch (e.Column.FieldName)
            {
                case "TeamAnswer":
                    if (intStatus != 1) e.RepositoryItem = repositoryItemGridLookUpEditDiabled;
                    break;
                case "TeamRemarks":
                    if (intStatus != 1) e.RepositoryItem = repositoryItemMemoExEditDisabled;
                    break;
            }*/
        }

        private void gridView6_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            int intStatus = Convert.ToInt32(view.GetFocusedRowCellValue("StatusID"));
            view = (GridView)sender;
            /*switch (view.FocusedColumn.FieldName)
            {
                case "TeamAnswer":
                case "TeamRemarks":
                    if (intStatus != 1) e.Cancel = true;
                    break;
                default:
                    break;
            }*/
        }

        private void gridView6_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            /*switch (e.Column.FieldName)
            {
                case "TeamAnswer":
                case "TeamRemarks":
                    {
                        view = (GridView)gridControl5.MainView;
                        if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                        int intStatus = Convert.ToInt32(view.GetFocusedRowCellValue("StatusID"));
                        if (intStatus != 1) e.Appearance.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
                        break;
                    }
            }*/

        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private DataView clone = null;
        private void gridView6_ShownEditor(object sender, EventArgs e)
        {
            GridView view = (GridView)sender;
            if (view.FocusedColumn.FieldName == "TeamAnswer" || view.FocusedColumn.FieldName == "AuditAnswer")
            {
                GridLookUpEdit glue = (GridLookUpEdit)view.ActiveEditor;
                BindingSource bs = (BindingSource)glue.Properties.DataSource;
                DataTable table = ((DataSet)bs.DataSource).Tables[bs.DataMember];
                clone = new DataView(table);
                DataRow row = view.GetDataRow(view.FocusedRowHandle);
                clone.RowFilter = "[MasterQuestionID] = " + row["MasterQuestionID"].ToString() + " or [MasterQuestionID] = 0";
                glue.Properties.DataSource = new BindingSource(clone, "");
            }
        }

        private void gridView6_HiddenEditor(object sender, EventArgs e)
        {
            if (clone != null)
            {
                clone.Dispose();
                clone = null;
            }
        }


        #region Editor EditValueChanged

        private void repositoryItemMemoExEdit6_EditValueChanged_1(object sender, EventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void repositoryItemGridLookUpEditAnswer_EditValueChanging(object sender, ChangingEventArgs e)
        {
            gridView1.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        #endregion

        #endregion


        #region GridView9

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 9;
            SetMenuStatus();
        }

        private void gridView9_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();

            LoadLinkedRiskAssessments();
            GridView view = (GridView)gridControl10.MainView;
            view.ExpandAllGroups();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 9;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView10

        private void gridView10_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 10;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                // Set any of the Dataset Menu items as appropriate if required //
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView10_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            //LoadLinkedRiskAssessmentQuestions();
            //GridView view = (GridView)gridControl23.MainView;
            //view.ExpandAllGroups();
        }

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 10;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    {
                        if ("add".Equals(e.Button.Tag))
                        {
                            Add_Record();
                        }
                        else if ("edit".Equals(e.Button.Tag))
                        {
                            Edit_Record();
                        }
                        else if ("delete".Equals(e.Button.Tag))
                        {
                            Delete_Record();
                        }
                        else if ("view".Equals(e.Button.Tag))
                        {
                            View_Record();
                        }
                        else if ("refresh".Equals(e.Button.Tag))
                        {
                            LoadLinkedRiskAssessments();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView11

        private void gridView11_DoubleClick(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView11_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 11;
            SetMenuStatus();
        }

        private void gridView11_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView11_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 11;
            SetMenuStatus();
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiShowMap.Enabled = false;
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView11_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PDFFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView11_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PDFFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl11_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridView1;
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditRecord_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "edit")  // Edit Job Button //
            {
                i_int_FocusedGrid = 11;
                Edit_Record();
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colPDFFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PDFFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strPermissionFilePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    //System.Diagnostics.Process.Start(strFilePath);

                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFilePath;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveFirst();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveFirst();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveFirst();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //
                                        bs.MovePrevious();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MovePrevious();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MovePrevious();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveNext();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveNext();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveNext();
                        }
                    }
                    e.Handled = true;
                    break;
                case DevExpress.XtraEditors.NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else  // Check for outstanding linked action changes - if yes see if user wished to save before moving row //
                    {
                        string strMessage = CheckForPendingSave();
                        if (strMessage != "")
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            if (strMessage != "")
                            {
                                strMessage += "\nWould you like to save the change(s) before navigating to another Work Order?";
                                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Work Order Navigation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                                {
                                    case DialogResult.Cancel:  // Abort navigation //                                 
                                        break;
                                    case DialogResult.No:  // Proceed with navigation and disguard changes //                       
                                        bs.MoveLast();
                                        break;
                                    case DialogResult.Yes:  // Fire Save then proceed with navigation //

                                        if (!string.IsNullOrEmpty(SaveChanges(true)))
                                        {
                                            e.Handled = true;  // Save Failed so abort form closing to allow user to correct //
                                            break;
                                        }
                                        bs.MoveLast();
                                        break;
                                }
                            }
                        }
                        else
                        {
                            BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                            bs.MoveLast();
                        }
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    if (this.strFormMode != "blockedit" && this.strFormMode != "blockadd")
                    {
                        LoadLinkedActions();
                        GridView view = (GridView)gridControl1.MainView;
                        view.ExpandAllGroups();

                        LoadLinkedContractors();
                        view = (GridView)gridControl2.MainView;
                        view.ExpandAllGroups();

                        LoadLinkedMaps();
                        view = (GridView)gridControl3.MainView;
                        view.ExpandAllGroups();

                        Load_Linked_Pictures();
                        view = (GridView)gridControl4.MainView;
                        view.ExpandAllGroups();

                        Load_Site_Completion_Sheets();
                        view = (GridView)gridControl5.MainView;
                        view.ExpandAllGroups();

                        FilterGrids();
                        view = (GridView)gridControl6.MainView;
                        view.ExpandAllGroups();

                        Load_Risk_Locations();
                        view = (GridView)gridControl9.MainView;
                        view.ExpandAllGroups();
                    }
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void repositoryItemMemoExEdit6_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void repositoryItemGridLookUpEditAnswer_EditValueChanged(object sender, EventArgs e)
        {
            gridView6.PostEditor();  // Immediately save the edited value to the grid's cell and the underlying column so Changes Pending works correctly //
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }

        private void WorkOrderPDFHyperLinkEdit_OpenLink(object sender, OpenLinkEventArgs e)
        {
            string strFile = WorkOrderPDFHyperLinkEdit.Text.ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Work Order Document Linked - unable to proceed.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                string strFilePath = strDefaultWorkOrderDocumentPath;
                if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                strFilePath += strFile;
                System.Diagnostics.Process.Start(strFilePath);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Work Order Document: " + strFile + ".\n\nThe Work Order may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Work Order Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        public void Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Tree Work //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Actions.", "Add Linked Actions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        int intMaxOrder = 0;
                        string strExcludeActions = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludeActions += view.GetRowCellValue(i, "ActionID").ToString() + ";";
                            if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder"));
                        }

                        frm_UT_WorkOrder_Add_Actions fChildForm = new frm_UT_WorkOrder_Add_Actions();
                        fChildForm.strExcludedActionIDs = strExcludeActions;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Linking Actions...");

                            view.BeginUpdate();
                            view.BeginSort();
                            DataTable dtNew = fChildForm.dtSelectedActions;
                            // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                            //dtNew.Columns.Remove("ActionPriority");
                            
                            foreach (DataRow dr in dtNew.Rows)
                            {
                                intMaxOrder++;
                                dr["WorkOrderSortOrder"] = intMaxOrder;
                                dr["WorkOrderID"] = intWorkOrderID;
                                this.dataSet_UT_WorkOrder.sp07249_UT_Work_Order_Manager_Linked_Actions_Edit.ImportRow(dr);
                            }
                            view.EndSort();
                            view.EndUpdate();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                        }
                    }
                    break;
                case 2:     // Teams //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Teams.", "Add Linked Team(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        string strExcludeTeams = "";
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            strExcludeTeams += view.GetRowCellValue(i, "SubContractorID").ToString() + ";";
                        }

                        frm_UT_WorkOrder_Add_Teams fChildForm = new frm_UT_WorkOrder_Add_Teams();
                        fChildForm.strExcludedTeamIDs = strExcludeTeams;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;

                        if (fChildForm.ShowDialog() == DialogResult.OK)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Linking Teams...");

                            view.BeginUpdate();
                            view.BeginSort();
                            DataTable dtNew = fChildForm.dtSelectedTeams;
                            // Remove columns which don't exist in the Work Orders Linked Actions datatable otherwise the Import statement later in the process won't work (the source and destination must have the same structure) // 
                            //dtNew.Columns.Remove("ActionPriority");

                            foreach (DataRow dr in dtNew.Rows)
                            {
                                dr["WorkOrderID"] = intWorkOrderID;
                                this.dataSet_UT_WorkOrder.sp07294_UT_Team_Work_Order_Linked_SubContractors.ImportRow(dr);
                            }
                            view.EndSort();
                            view.EndUpdate();
                            SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                        }
                    }
                    break;
                case 3:     // Maps //
                    {
                        if (strFormMode == "view") return;
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Maps.", "Add Linked Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
                        bool boolProceed = true;
                        string strMessage = CheckForPendingSave();
                        if (!string.IsNullOrEmpty(strMessage))
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before opening the mapping!\n\nSave Changes?", "Add Linked Map(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                            }
                            else
                            {
                                boolProceed = false;  // The user said no to save changes //
                            }
                        }
                        else  // No pending changes in screen //
                        {
                            boolProceed = true;
                        }
                        if (!boolProceed) return;

                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        if (!iBool_AllowAdd) return;

                        OpenMapping();
                    }
                    break;
                case 10:     // Risk Assessment //
                    {
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        
                        currentRow = (DataRowView)sp07309UTTeamWorkOrderRiskLocationsListBindingSource.Current;
                        if (currentRow == null) return;
                        int intSurveyedPoleID = (string.IsNullOrEmpty(currentRow["SurveyedPoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyedPoleID"]));
                        int intNewRiskAssessmentID = 0;

                        // Get the Risk Assessment Type //
                        int intType = 0;
                        frm_UT_Risk_Assessment_Add_Select_Type fChildForm1 = new frm_UT_Risk_Assessment_Add_Select_Type();
                        switch (fChildForm1.ShowDialog())
                        {
                            case DialogResult.OK:
                                intType = 0;
                                break;
                            case DialogResult.Yes:
                                intType = 1;
                                break;
                            case DialogResult.Cancel:
                                return;
                        }

                        // Add New Risk Assessment (Site Specific) //
                        try
                        {
                            DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                            AddRecord.ChangeConnectionString(strConnectionString);
                            intNewRiskAssessmentID = Convert.ToInt32(AddRecord.sp07260_UT_Risk_Assessment_Create(intSurveyedPoleID, 0, GlobalSettings.UserID, intType));
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to add the Risk Assessment [" + ex.Message + "]. Try again. If the problem persists contact technical support.", "Add Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Inform this screen to reload the list once the screen re-activates after editing the record //
                        UpdateRefreshStatus = 10;
                        i_str_AddedRecordIDs10 = intNewRiskAssessmentID.ToString() + ";";

                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit fChildForm = new frm_UT_Risk_Assessment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = intNewRiskAssessmentID.ToString() + ",";
                        fChildForm.strFormMode = "edit";  // This should be Edit since we already added the record, so now we edit it //
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm._PassedInWorkOrderID = intWorkOrderID;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

            }
        }

        public void Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Tree Work //
                    {
                        // Check if the screen contains pending changes - if yes, prompt user to save and Save changes if the user okays this //
                        bool boolProceed = true;
                        string strMessage = CheckForPendingSave();
                        if (!string.IsNullOrEmpty(strMessage))
                        {
                            if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these changes before editing an action!\n\nSave Changes?", "Edit Action", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                            }
                            else
                            {
                                boolProceed = false;  // The user said no to save changes //
                            }
                        }
                        else  // No pending changes in screen //
                        {
                            boolProceed = true;
                        }
                        if (!boolProceed) return;

                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPicturePath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 2:
                    {
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        if (intWorkOrderID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before setting Team Members on Site.", "Set Team Members on Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        bool boolRowsSelected = false;
                        view = (GridView)gridControl2.MainView;
                        int intRowCount = view.DataRowCount;
                        if (intRowCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("No Teams linked to this work Order - unable to Set Team Members on Site.", "Set Team Members on Site", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length > 0) boolRowsSelected = true;
 
                        if (boolRowsSelected)  // process selected rows only //
                        {
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkOrderTeamID")) + ',';
                            }
                        }
                        else  // No rows so get all them //
                        {
                            for (int intRowHandle = 0; intRowHandle < intRowCount; intRowHandle++)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkOrderTeamID")) + ',';
                            }
                        }
                        if (string.IsNullOrEmpty(strRecordIDs)) return;

                        frm_UT_Team_WorkOrder_Team_Members_On_Site fChildForm = new frm_UT_Team_WorkOrder_Team_Members_On_Site();
                        fChildForm.intWorkOrderID = intWorkOrderID;
                        fChildForm.strWorkOrderTeamIDs = strRecordIDs;
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.ShowDialog();
                        if (fChildForm.boolChangesMade)
                        {
                            LoadLinkedContractors();
                        }                       
                    }
                    break;
                case 4:     // Work Pictures //
                    {
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        if (intLinkedToRecordTypeID != 4)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("You cannot edit a Survey Picture - select a different picture to edit before proceeding.", "Edit Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            view.UnselectRow(intRowHandles[0]);
                            return;
                        }
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.ShowDialog();

                        UpdateRefreshStatus = 4;
                        this.RefreshGridViewState4.SaveViewInfo();
                        Load_Linked_Pictures();
                    }
                    break;
                case 10:     // Risk Assessment //
                    {
                        DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
                        if (currentRow == null) return;
                        int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
                        
                        view = (GridView)gridControl10.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Risk Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance._PassedInWorkOrderID = intWorkOrderID;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 11:     // Permission Documents //
                    {
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strLayout = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            if (!iBool_AllowDelete) return;
            switch (i_int_FocusedGrid)
            {
                case 1:  // Work Orders //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Actions to remove from the Work Order by clicking on them then try again.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Action" : Convert.ToString(intRowHandles.Length) + " Linked Actions") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Action" : "these Linked Actions") + " will be removed from the work order. They will then be available for linking to a different work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            // Clean Up any holes in the Sort Order Sequence... //
                            int intCorrectOrder = 0;
                            view.BeginDataUpdate();
                            view.BeginSort();
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                intCorrectOrder++;
                                if (Convert.ToInt32(view.GetRowCellValue(i, "WorkOrderSortOrder")) != intCorrectOrder) view.SetRowCellValue(i, "WorkOrderSortOrder", intCorrectOrder);
                            }
                            view.EndDataUpdate();
                            view.EndSort();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Actions from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 2:  // Contractors //
                    {
                        view = (GridView)gridControl2.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Sub-Contractors to remove from the Work Order by clicking on them then try again.", "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Sub-Contractor" : Convert.ToString(intRowHandles.Length) + " Linked Sub-Contractors") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Sub-Contractor" : "these Linked Sub-Contractors") + " will be removed from the work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Sub-Contractors from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Maps //
                    {
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Maps to remove from the Work Order by clicking on them then try again.", "Remove Linked Maps from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Map" : Convert.ToString(intRowHandles.Length) + " Linked Maps") + " selected for removing from the current Work Order!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Map" : "these Linked Maps") + " will be removed from the work order. They will then be available for linking to a different work order.";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Actions from Work Order", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            frmProgress fProgress = new frmProgress(20);
                            fProgress.UpdateCaption("Removing...");
                            fProgress.Show();
                            Application.DoEvents();

                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }

                            if (fProgress != null)
                            {
                                fProgress.UpdateProgress(20); // Update Progress Bar //
                                fProgress.Close();
                                fProgress = null;
                            }

                            // Clean Up any holes in the Sort Order Sequence... //
                            int intCorrectOrder = 0;
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                intCorrectOrder++;
                                if (Convert.ToInt32(view.GetRowCellValue(i, "MapOrder")) != intCorrectOrder) view.SetRowCellValue(i, "MapOrder", intCorrectOrder);
                            }

                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Work Order.\n\nIMPORTANT NOTE: You must save changes to this work order before the marked linked records are physically removed from the Work Order.", "Remove Linked Maps from Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 4:  // Work Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl4.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedToRecordTypeID")) != 4) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only delete Work Pictures - select a different picture to delete before proceeding.", "Delete Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                         // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Work Picture" : Convert.ToString(intRowHandles.Length) + " Work Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Work Picture" : "these Work Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Work Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("survey_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            Load_Linked_Pictures();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 10:  // Risk Assessment //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl10.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Risk Assessments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Risk Assessment" : Convert.ToString(intRowHandles.Length) + " Risk Assessments") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Risk Assessment" : "these Risk Assessments") + " will no longer be available for selection and any associated records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "RiskAssessmentID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("risk_assessment", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRiskAssessments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            switch (i_int_FocusedGrid)
            {
                case 10:     // Risk Assessment //
                    {
                        view = (GridView)gridControl10.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState10.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 11:     // Permission Documents //
                    {
                        view = (GridView)gridControl11.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strLayout = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        this.RefreshGridViewState11.SaveViewInfo();  // Store Grid View State //
                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;

                default:
                    break;
            }
        }

        private void OpenMapping()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07247UTWorkOrderEditBindingSource.Current;
            if (currentRow == null) return;
            int intWorkOrderID = (currentRow["WorkOrderID"] == null ? 0 : Convert.ToInt32(currentRow["WorkOrderID"]));
            string strDocumentDescription = currentRow["PaddedWorkOrderID"].ToString();
            if (intWorkOrderID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Work Order must be saved before adding Linked Maps.", "Add Work Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = (GridView)gridControl1.MainView;
            int intRowCount = view.RowCount;  // Not using DataRowCount as we have a filtered grid and we don't want any other permission document's work //
            if (intRowCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You must add at least one piece of work before proceeding.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get Unique list of Survey IDs //
            string strSurveyIDs = ",";
            string strCurrentID = "";
            string strTreeIDs = ",";
            string strTreeID = "";
            string strPoleIDs = ",";
            string strPoleID = "";
            for (int i = 0; i < view.RowCount; i++)
            {
                strCurrentID = view.GetRowCellValue(i, "SurveyID").ToString();
                if (!(string.IsNullOrEmpty(strCurrentID) || strCurrentID == "0"))
                {
                    if (!strSurveyIDs.Contains("," + strCurrentID + ",")) strSurveyIDs += strCurrentID + ",";
                }
                strTreeID = view.GetRowCellValue(i, "TreeID").ToString();
                if (!(string.IsNullOrEmpty(strTreeID) || strTreeID == "0"))
                {
                    if (!strTreeIDs.Contains("," + strTreeID + ",")) strTreeIDs += strTreeID + ",";
                }
                strPoleID = view.GetRowCellValue(i, "PoleID").ToString();
                if (!(string.IsNullOrEmpty(strPoleID) || strPoleID == "0"))
                {
                    if (!strPoleIDs.Contains("," + strPoleID + ",")) strPoleIDs += strPoleID + ",";
                }
            }
            char[] delimiters = new char[] { ',' };
            string[] strArray;
            strArray = strSurveyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            if (strArray.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Survey - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            strTreeIDs = strTreeIDs.TrimStart(',');
            if (strTreeIDs.Length < 2)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is not linked to any Tree - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            int intSelectedSurveyID = Convert.ToInt32(strArray[0]);

            int intWorkSpaceID = 0;
            int intRegionID = 0;
            string strWorkspaceName = "";
            string strReturn = "";
            DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
            getWorkspace.ChangeConnectionString(strConnectionString);
            try
            {
                strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(Convert.ToInt32(strArray[0])).ToString();
                delimiters = new char[] { '|' };
                string[] strArray2 = null;
                strArray2 = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strArray2.Length == 3)
                {
                    intWorkSpaceID = Convert.ToInt32(strArray2[0]);
                    strWorkspaceName = strArray2[1].ToString();
                    intRegionID = Convert.ToInt32(strArray2[2]);
                }
            }
            catch (Exception)
            {
                return;
            }
            if (intWorkSpaceID == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The selected work is linked to a Survey, but the survey is not linked to a region with a Mapping Workspace - unable to open mapping!\n\nYou should manually start the mapping, load in the required trees and click the Map Snapshot button on the mapping toolbar.", "Create Work Map(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Close any open instance of the mapping //
            try
            {
                frmMain2 frmMDI = (frmMain2)this.MdiParent;
                foreach (frmBase frmChild in frmMDI.MdiChildren)
                {
                    if (frmChild.FormID == 10003)
                    {
                        frmChild.Close();
                    }
                }
            }
            catch (Exception)
            {
            }

            // Open Map and set mode to Map Snapshot and load in trees with work on this Permission Document //
            int intWorkspaceOwner = 0;
            Mapping_Functions MapFunctions = new Mapping_Functions();
            intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

            frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            frmInstance.splashScreenManager = splashScreenManager1;
            frmInstance.splashScreenManager.ShowWaitForm();

            frmInstance.GlobalSettings = this.GlobalSettings;
            frmInstance.MdiParent = this.MdiParent;
            frmInstance._SelectedSurveyID = intSelectedSurveyID;
            frmInstance._SurveyMode = "PermissionDocumentMap";
            frmInstance._PassedInRecordType = 2;  // 1 = Permision Document, 2 = Work Order //
            frmInstance._PassedInTreeIDs = strTreeIDs;
            frmInstance._PassedInPoleIDs = strPoleIDs;
            frmInstance._PassedInPermissionDocumentID = intWorkOrderID;
            frmInstance._PassedInPermissionDocumentDescription = strDocumentDescription;
            frmInstance.Show();
            // Invoke Post Open event on Base form //
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null)
            {
                method.Invoke(frmInstance, new object[] { null });
            }
        }

        private void barCheckItemWorkOrder_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (barCheckItemWorkOrder.Checked)
            {
                xtraTabControl1.SelectedTabPage = xtraTabPageWorkOrder;
                barButtonItemPrint.Enabled = true;
                barButtonItemPrintRPSheet.Enabled = false;
            }

        }

        private void barCheckItemRPSheet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (barCheckItemRPSheet.Checked)
            {
                xtraTabControl1.SelectedTabPage = xtraTabPageRPSheet;
                barButtonItemPrint.Enabled = false;
                barButtonItemPrintRPSheet.Enabled = true;
            }
        }

        private void barCheckItemRiskAssessment_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (barCheckItemRiskAssessment.Checked)
            {
                xtraTabControl1.SelectedTabPage = xtraTabPageRiskAssessment;
                barButtonItemPrint.Enabled = false;
                barButtonItemPrintRPSheet.Enabled = false;
            }
        }

        private void FilterGrids()
        {
            string strCompletionSheetID = "";
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            strCompletionSheetID = view.GetFocusedRowCellValue("CompletionSheetID").ToString();
            
            view = (GridView)gridControl6.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[CompletionSheetID] = " + Convert.ToString(strCompletionSheetID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void barButtonItemPrintRPSheet_ItemClick(object sender, ItemClickEventArgs e)
        {
            
            int intCompletionSheetID = 0;
            GridView view = (GridView)gridControl5.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            intCompletionSheetID = Convert.ToInt32(view.GetFocusedRowCellValue("CompletionSheetID"));         
 
            if (!splashScreenManager.IsSplashFormVisible)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                this.splashScreenManager = splashScreenManager1;
                this.splashScreenManager.ShowWaitForm();
            }
            string strReportFileName = "RP_Sheet_Layout.repx";
            rpt_UT_Report_Layout_RP_Sheet rptReport = new rpt_UT_Report_Layout_RP_Sheet(this.GlobalSettings, intCompletionSheetID);
            try
            {
                rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                rptReport.ShowRibbonPreview();
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();   
            
        }

        private void bbiEditRPSheetLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                int intCompletionSheetID = 0;
                GridView view = (GridView)gridControl5.MainView;
                if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                intCompletionSheetID = Convert.ToInt32(view.GetFocusedRowCellValue("CompletionSheetID"));
                string strReportFileName = "RP_Sheet_Layout.repx";

                rpt_UT_Report_Layout_RP_Sheet rptReport = null;
                rptReport = new rpt_UT_Report_Layout_RP_Sheet(this.GlobalSettings, intCompletionSheetID);
                try
                {
                    rptReport.LoadLayout(_ReportLayoutFolder + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (!splashScreenManager.IsSplashFormVisible)
                {
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    this.splashScreenManager = splashScreenManager1;
                    this.splashScreenManager.ShowWaitForm();
                }

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, _ReportLayoutFolder, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void btnShowMap_Click(object sender, EventArgs e)
        {
            View_Map();
        }




        void form_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            //if (loadingForm != null) loadingForm.Close();
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);

            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Data...", "Report Printing");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //

            }
        }

        /*
                public class SaveCommandHandler : ICommandHandler
                {
                    XRDesignPanel panel;

                    public string strFullPath = "";

                    public string strFileName = "";

                    public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
                    {
                        this.panel = panel;
                        this.strFullPath = strFullPath;
                        this.strFileName = strFileName;
                    }

                    public virtual void HandleCommand(ReportCommand command, object[] args, ref bool handled)
                    {
                        if (!CanHandleCommand(command)) return;
                        Save();  // Save report //
                        handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
                    }

                    public virtual bool CanHandleCommand(ReportCommand command)
                    {
                        // This handler is used for SaveFile, SaveFileAs and Closing commands.
                        return command == ReportCommand.SaveFile ||
                            command == ReportCommand.SaveFileAs ||
                            command == ReportCommand.Closing;
                    }

                    void Save()
                    {
                        // Custom Saving Logic //
                        Boolean blSaved = false;
                        panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                        // Update existing file layout //
                        panel.Report.DataSource = null;
                        panel.Report.DataMember = null;
                        panel.Report.DataAdapter = null;
                        try
                        {
                            panel.Report.SaveLayout(strFullPath + strFileName);
                            blSaved = true;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            Console.WriteLine(ex.Message);
                            blSaved = false;
                        }
                        if (blSaved)
                        {
                            panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                        }
                    }
                }
        */
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler, IDisposable
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public void Dispose()
            {
                if (panel != null)
                {
                    panel.Dispose();
                    panel = null;
                }
            }
            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }

 






    }
}


﻿namespace WoodPlan5
{
    partial class frm_UT_Billing_Centre_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Billing_Centre_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.tableAdapterManager = new WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.bsiFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.dxErrorProvider = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.editFormLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.editFormDataNavigator = new DevExpress.XtraEditors.DataNavigator();
            this.editFormDataLayoutControlGroup = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtBillingCentreCode = new DevExpress.XtraEditors.TextEdit();
            this.lueCompanyID = new DevExpress.XtraEditors.LookUpEdit();
            this.lueDepartmentID = new DevExpress.XtraEditors.LookUpEdit();
            this.lueCostCentreID = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBillingCentreCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCompanyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepartmentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.spAS11000DuplicateSearchBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11000_Duplicate_SearchTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter();
            this.spAS11038BillingCentreCodeItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter();
            this.spAS11057CompanyItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11057_Company_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp_AS_11057_Company_ItemTableAdapter();
            this.spAS11060DepartmentItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11060_Department_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp_AS_11060_Department_ItemTableAdapter();
            this.spAS11063CostCentreItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).BeginInit();
            this.editFormDataLayoutControlGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCompanyID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartmentID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCostCentreID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompanyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1059, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 699);
            this.barDockControlBottom.Size = new System.Drawing.Size(1059, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 673);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1059, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 673);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus,
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.bsiFormMode,
            this.barStaticItem1,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemChangesPending,
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 35;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_AS_11002_Equipment_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11005_Vehicle_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11008_Plant_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11011_Equipment_Category_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11014_Make_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11017_Model_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11020_Supplier_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Tracker_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11023_Verilocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11026_Gadget_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11029_Office_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11032_Hardware_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11035_Software_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11041_Transaction_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11044_Keeper_Allocation_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11047_Equipment_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11050_Depreciation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Data_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Interval_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11054_Service_Intervals_ListTableAdapter = null;
            this.tableAdapterManager.sp_AS_11057_Company_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11060_Department_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11063_Cost_Centre_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11066_Depreciation_Settings_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11075_Cover_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11078_Work_Detail_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11081_Incident_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11084_Purpose_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11087_Equipment_Category_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11089_Fuel_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11093_Initial_Billing_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11096_Road_Tax_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11099_Improvement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11102_Rental_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11105_Speeding_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11108_Rental_Details_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11112_Keeper_Email_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11117_Sim_Card_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11120_Additional_FollowUps_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11123_Sim_Allocation_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_AlertTableAdapter = null;
            this.tableAdapterManager.sp_AS_11126_Notification_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11129_Software_Assign_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11133_Picklist_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_AS_11141_Hardware_Specifications_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11157_Email_Schedule_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11163_Service_ScheduleTableAdapter = null;
            this.tableAdapterManager.sp_AS_11166_P11D_ItemTableAdapter = null;
            this.tableAdapterManager.sp_AS_11169_NetBookValue_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_AS_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status Bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiFormMode, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barStaticItemInformation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // bsiFormMode
            // 
            this.bsiFormMode.Caption = "Form Mode: Editing";
            this.bsiFormMode.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.Glyph")));
            this.bsiFormMode.Id = 29;
            this.bsiFormMode.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bsiFormMode.LargeGlyph")));
            this.bsiFormMode.Name = "bsiFormMode";
            toolTipTitleItem1.Text = "Form Mode - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bsiFormMode.SuperTip = superToolTip1;
            this.bsiFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 31;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 32;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 33;
            this.barStaticItemInformation.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.LargeGlyph")));
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 27;
            this.bbiFormSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.LargeGlyph")));
            this.bbiFormSave.Name = "bbiFormSave";
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Text = "Save Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormSave.SuperTip = superToolTip2;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 28;
            this.bbiFormCancel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.LargeGlyph")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Text = "Cancel Button - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiFormCancel.SuperTip = superToolTip3;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 30;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // dxErrorProvider
            // 
            this.dxErrorProvider.ContainerControl = this;
            // 
            // editFormLayoutControlGroup
            // 
            this.editFormLayoutControlGroup.CustomizationFormText = "Main Layout Control Group";
            this.editFormLayoutControlGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.editFormLayoutControlGroup.GroupBordersVisible = false;
            this.editFormLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlGroup1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.editFormLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.editFormLayoutControlGroup.Name = "Root";
            this.editFormLayoutControlGroup.Size = new System.Drawing.Size(1059, 673);
            this.editFormLayoutControlGroup.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.editFormDataNavigator;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(334, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(263, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // editFormDataNavigator
            // 
            this.editFormDataNavigator.Buttons.Append.Visible = false;
            this.editFormDataNavigator.Buttons.CancelEdit.Visible = false;
            this.editFormDataNavigator.Buttons.EndEdit.Visible = false;
            this.editFormDataNavigator.Buttons.Remove.Visible = false;
            this.editFormDataNavigator.Location = new System.Drawing.Point(346, 12);
            this.editFormDataNavigator.Name = "editFormDataNavigator";
            this.editFormDataNavigator.Size = new System.Drawing.Size(259, 19);
            this.editFormDataNavigator.StyleController = this.editFormDataLayoutControlGroup;
            this.editFormDataNavigator.TabIndex = 122;
            this.editFormDataNavigator.Text = "depreciationDataNavigator";
            this.editFormDataNavigator.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            // 
            // editFormDataLayoutControlGroup
            // 
            this.editFormDataLayoutControlGroup.Controls.Add(this.editFormDataNavigator);
            this.editFormDataLayoutControlGroup.Controls.Add(this.txtBillingCentreCode);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueCompanyID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueDepartmentID);
            this.editFormDataLayoutControlGroup.Controls.Add(this.lueCostCentreID);
            this.editFormDataLayoutControlGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editFormDataLayoutControlGroup.Location = new System.Drawing.Point(0, 26);
            this.editFormDataLayoutControlGroup.Name = "editFormDataLayoutControlGroup";
            this.editFormDataLayoutControlGroup.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1825, 238, 250, 350);
            this.editFormDataLayoutControlGroup.Root = this.editFormLayoutControlGroup;
            this.editFormDataLayoutControlGroup.Size = new System.Drawing.Size(1059, 673);
            this.editFormDataLayoutControlGroup.TabIndex = 4;
            this.editFormDataLayoutControlGroup.Text = "dataLayoutControl1";
            // 
            // txtBillingCentreCode
            // 
            this.txtBillingCentreCode.Location = new System.Drawing.Point(112, 35);
            this.txtBillingCentreCode.MenuManager = this.barManager1;
            this.txtBillingCentreCode.Name = "txtBillingCentreCode";
            this.scSpellChecker.SetShowSpellCheckMenu(this.txtBillingCentreCode, true);
            this.txtBillingCentreCode.Size = new System.Drawing.Size(935, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.txtBillingCentreCode, optionsSpelling1);
            this.txtBillingCentreCode.StyleController = this.editFormDataLayoutControlGroup;
            this.txtBillingCentreCode.TabIndex = 123;
            // 
            // lueCompanyID
            // 
            this.lueCompanyID.Location = new System.Drawing.Point(112, 59);
            this.lueCompanyID.MenuManager = this.barManager1;
            this.lueCompanyID.Name = "lueCompanyID";
            this.lueCompanyID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCompanyID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyCode", "Company Code", 83, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Company", "Company", 55, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCompanyID.Properties.DataSource = this.spAS11057CompanyItemBindingSource;
            this.lueCompanyID.Properties.DisplayMember = "CompanyCode";
            this.lueCompanyID.Properties.NullText = "";
            this.lueCompanyID.Properties.NullValuePrompt = "-- Please Select Company --";
            this.lueCompanyID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueCompanyID.Properties.ValueMember = "CompanyID";
            this.lueCompanyID.Size = new System.Drawing.Size(231, 20);
            this.lueCompanyID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueCompanyID.TabIndex = 124;
            this.lueCompanyID.Tag = "Company";
            this.lueCompanyID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // lueDepartmentID
            // 
            this.lueDepartmentID.Location = new System.Drawing.Point(447, 59);
            this.lueDepartmentID.MenuManager = this.barManager1;
            this.lueDepartmentID.Name = "lueDepartmentID";
            this.lueDepartmentID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueDepartmentID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("DepartmentCode", "Department Code", 95, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Department", "Department", 67, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueDepartmentID.Properties.DataSource = this.spAS11060DepartmentItemBindingSource;
            this.lueDepartmentID.Properties.DisplayMember = "DepartmentCode";
            this.lueDepartmentID.Properties.NullText = "";
            this.lueDepartmentID.Properties.NullValuePrompt = "-- Please Select Department --";
            this.lueDepartmentID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueDepartmentID.Properties.ValueMember = "DepartmentID";
            this.lueDepartmentID.Size = new System.Drawing.Size(248, 20);
            this.lueDepartmentID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueDepartmentID.TabIndex = 125;
            this.lueDepartmentID.ToolTipTitle = "Department";
            this.lueDepartmentID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // lueCostCentreID
            // 
            this.lueCostCentreID.Location = new System.Drawing.Point(799, 59);
            this.lueCostCentreID.MenuManager = this.barManager1;
            this.lueCostCentreID.Name = "lueCostCentreID";
            this.lueCostCentreID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueCostCentreID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CostCentreCode", "Cost Centre Code", 96, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CostCentre", "Cost Centre", 68, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lueCostCentreID.Properties.DataSource = this.spAS11063CostCentreItemBindingSource;
            this.lueCostCentreID.Properties.DisplayMember = "CostCentreCode";
            this.lueCostCentreID.Properties.NullText = "";
            this.lueCostCentreID.Properties.NullValuePrompt = "-- Please Select Cost Centre --";
            this.lueCostCentreID.Properties.NullValuePromptShowForEmptyValue = true;
            this.lueCostCentreID.Properties.ValueMember = "CostCentreID";
            this.lueCostCentreID.Size = new System.Drawing.Size(248, 20);
            this.lueCostCentreID.StyleController = this.editFormDataLayoutControlGroup;
            this.lueCostCentreID.TabIndex = 126;
            this.lueCostCentreID.Tag = "Cost Centre";
            this.lueCostCentreID.Validating += new System.ComponentModel.CancelEventHandler(this.lookupedit_Validating);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AllowDrawBackground = false;
            this.layoutControlGroup1.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBillingCentreCode,
            this.ItemForCompanyID,
            this.ItemForDepartmentID,
            this.ItemForCostCentreID});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup1.Name = "autoGeneratedGroup0";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1039, 630);
            // 
            // ItemForBillingCentreCode
            // 
            this.ItemForBillingCentreCode.Control = this.txtBillingCentreCode;
            this.ItemForBillingCentreCode.CustomizationFormText = "Billing Centre Code :";
            this.ItemForBillingCentreCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForBillingCentreCode.Name = "ItemForBillingCentreCode";
            this.ItemForBillingCentreCode.Size = new System.Drawing.Size(1039, 24);
            this.ItemForBillingCentreCode.Text = "Billing Centre Code :";
            this.ItemForBillingCentreCode.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForCompanyID
            // 
            this.ItemForCompanyID.Control = this.lueCompanyID;
            this.ItemForCompanyID.CustomizationFormText = "Company :";
            this.ItemForCompanyID.Location = new System.Drawing.Point(0, 24);
            this.ItemForCompanyID.Name = "ItemForCompanyID";
            this.ItemForCompanyID.Size = new System.Drawing.Size(335, 606);
            this.ItemForCompanyID.Text = "Company :";
            this.ItemForCompanyID.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForDepartmentID
            // 
            this.ItemForDepartmentID.Control = this.lueDepartmentID;
            this.ItemForDepartmentID.CustomizationFormText = "Department :";
            this.ItemForDepartmentID.Location = new System.Drawing.Point(335, 24);
            this.ItemForDepartmentID.Name = "ItemForDepartmentID";
            this.ItemForDepartmentID.Size = new System.Drawing.Size(352, 606);
            this.ItemForDepartmentID.Text = "Department :";
            this.ItemForDepartmentID.TextSize = new System.Drawing.Size(97, 13);
            // 
            // ItemForCostCentreID
            // 
            this.ItemForCostCentreID.Control = this.lueCostCentreID;
            this.ItemForCostCentreID.CustomizationFormText = "Cost Centre :";
            this.ItemForCostCentreID.Location = new System.Drawing.Point(687, 24);
            this.ItemForCostCentreID.Name = "ItemForCostCentreID";
            this.ItemForCostCentreID.Size = new System.Drawing.Size(352, 606);
            this.ItemForCostCentreID.Text = "Cost Centre :";
            this.ItemForCostCentreID.TextSize = new System.Drawing.Size(97, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(334, 23);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(597, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(442, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spAS11000DuplicateSearchBindingSource
            // 
            this.spAS11000DuplicateSearchBindingSource.DataMember = "sp_AS_11000_Duplicate_Search";
            this.spAS11000DuplicateSearchBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp_AS_11000_Duplicate_SearchTableAdapter
            // 
            this.sp_AS_11000_Duplicate_SearchTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11038BillingCentreCodeItemBindingSource
            // 
            this.spAS11038BillingCentreCodeItemBindingSource.DataMember = "sp_AS_11038_Billing_Centre_Code_Item";
            this.spAS11038BillingCentreCodeItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp_AS_11038_Billing_Centre_Code_ItemTableAdapter
            // 
            this.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11057CompanyItemBindingSource
            // 
            this.spAS11057CompanyItemBindingSource.DataMember = "sp_AS_11057_Company_Item";
            this.spAS11057CompanyItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp_AS_11057_Company_ItemTableAdapter
            // 
            this.sp_AS_11057_Company_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11060DepartmentItemBindingSource
            // 
            this.spAS11060DepartmentItemBindingSource.DataMember = "sp_AS_11060_Department_Item";
            this.spAS11060DepartmentItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp_AS_11060_Department_ItemTableAdapter
            // 
            this.sp_AS_11060_Department_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // spAS11063CostCentreItemBindingSource
            // 
            this.spAS11063CostCentreItemBindingSource.DataMember = "sp_AS_11063_Cost_Centre_Item";
            this.spAS11063CostCentreItemBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // sp_AS_11063_Cost_Centre_ItemTableAdapter
            // 
            this.sp_AS_11063_Cost_Centre_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Billing_Centre_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1059, 729);
            this.Controls.Add(this.editFormDataLayoutControlGroup);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1065, 586);
            this.Name = "frm_UT_Billing_Centre_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Billing Centre Codes";
            this.Activated += new System.EventHandler(this.frm_AS_Billing_Centre_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_AS_Billing_Centre_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_AS_Billing_Centre_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.editFormDataLayoutControlGroup, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editFormDataLayoutControlGroup)).EndInit();
            this.editFormDataLayoutControlGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingCentreCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCompanyID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueDepartmentID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueCostCentreID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBillingCentreCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCompanyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11000DuplicateSearchBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11038BillingCentreCodeItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11057CompanyItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11060DepartmentItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAS11063CostCentreItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataSet_AS_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.BarStaticItem bsiFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider;
        private DevExpress.XtraDataLayout.DataLayoutControl editFormDataLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup editFormLayoutControlGroup;
        private DevExpress.XtraEditors.DataNavigator editFormDataNavigator;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.TextEdit txtBillingCentreCode;
        private DevExpress.XtraEditors.LookUpEdit lueCompanyID;
        private DevExpress.XtraEditors.LookUpEdit lueDepartmentID;
        private DevExpress.XtraEditors.LookUpEdit lueCostCentreID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBillingCentreCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCompanyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreID;
        private System.Windows.Forms.BindingSource spAS11057CompanyItemBindingSource;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private System.Windows.Forms.BindingSource spAS11060DepartmentItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11063CostCentreItemBindingSource;
        private System.Windows.Forms.BindingSource spAS11000DuplicateSearchBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp_AS_11000_Duplicate_SearchTableAdapter sp_AS_11000_Duplicate_SearchTableAdapter;
        private System.Windows.Forms.BindingSource spAS11038BillingCentreCodeItemBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp_AS_11038_Billing_Centre_Code_ItemTableAdapter sp_AS_11038_Billing_Centre_Code_ItemTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp_AS_11057_Company_ItemTableAdapter sp_AS_11057_Company_ItemTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp_AS_11060_Department_ItemTableAdapter sp_AS_11060_Department_ItemTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp_AS_11063_Cost_Centre_ItemTableAdapter sp_AS_11063_Cost_Centre_ItemTableAdapter;


    }
}

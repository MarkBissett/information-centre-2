using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // For Path Combine //

using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_UT_Survey_Pole2 : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int _SurveyID = 0;
        public int intRecordCount = 0;
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strLinkedDocumentDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        private string strPermissionDocumentPath = "";
        private string strSignaturePath = "";
        private string strDefaultMapPath = "";

        public int intPassedInSurveyPoleID = 0;

        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private int i_int_FocusedGrid = 1;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormEditingCancelled = false;
        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //
        private bool ibool_FormStillLoading = true;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState3;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState4;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState5;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState6;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState7;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState8;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState9;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState10;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState12;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState13;  // Used by Grid View State Facilities //

        public RefreshGridState RefreshGridViewState16;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState17;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState18;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState19;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState20;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState22;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState23;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewState27;  // Used by Grid View State Facilities //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs4 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs5 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs6 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs8 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs9 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs10 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs12 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs13 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs16 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs17 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs18 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs19 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs20 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs22 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs27 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private Point? _Previous = null;
        private Color _color = Color.Black;
        private Pen _Pen = new Pen(System.Drawing.Color.Black);
        private int _OriginalSurveyStatusID = 0;

        private int ClientUKPN = 3679;  // Used for validation of ClearanceDistanceAboveID field //

        #endregion

        public frm_UT_Survey_Pole2()
        {
            InitializeComponent();
        }

        private void frm_UT_Survey_Pole2_Load(object sender, EventArgs e)
        {
            this.FormID = 500023;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            btnPrior.Enabled = false;
            _Pen.Width = (float)3;

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPictures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            try
            {
                strPermissionDocumentPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedPermissionDocuments").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Picture Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Picture Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPermissionDocumentPath.EndsWith("\\")) strPermissionDocumentPath += "\\";

            try
            {
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedSignatures").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Signature Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Signature Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strSignaturePath.EndsWith("\\")) strSignaturePath += "\\";

            try
            {
                strLinkedDocumentDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                strDefaultMapPath = GetSetting.sp00043_RetrieveSingleSystemSetting(8, "SavedMaps").ToString();
                if (!strDefaultMapPath.EndsWith("\\")) strDefaultMapPath += "\\";
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Map Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Map Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp07186_UT_G55_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07186_UT_G55_Categories_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07186_UT_G55_Categories_With_Blank);

            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07046_UT_Inspection_Cycles_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07046_UT_Inspection_Cycles_With_Blank);

            sp07356_UT_Clearance_Distances_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07356_UT_Clearance_Distances_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07356_UT_Clearance_Distances_With_Blank);

            sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07357_UT_Deferral_Reasons_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07357_UT_Deferral_Reasons_With_Blank);

            sp07465_UT_Held_Up_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07465_UT_Held_Up_Types_With_BlankTableAdapter.Fill(dataSet_UT.sp07465_UT_Held_Up_Types_With_Blank);

            sp07117_UT_Pole_Survey_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl1.ForceInitialize();
            RefreshGridViewState1 = new RefreshGridState(gridView1, "SurveyPictureID");

            // Populate Main Dataset //          
            sp07114_UT_Surveyed_Pole_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "edit":
                    try
                    {
                        sp07114_UT_Surveyed_Pole_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item, strRecordIDs, strFormMode, _SurveyID);
                    }
                    catch (Exception)
                    {
                    }
                    if (strFormMode == "edit")  // Attempt to set survey date if it is missing //
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (intPassedInSurveyPoleID == 0)  // If called from map (always present when called from Edit Survey screen) //
                            {
                                intPassedInSurveyPoleID = Convert.ToInt32(currentRow["SurveyedPoleID"]);
                            }
                            LoadPolePictures();

                            if (Convert.ToInt32(currentRow["SurveyStatusID"]) <= 0)
                            {
                                xtraTabControl1.SelectedTabPage = xtraTabPageWelcome;  // Go to Welcome page //
                                btnNext.Enabled = false;  // Only allow proceeding via the Start Survey button //
                            }
                            else
                            {
                                // Check if at least 1 picture is present - If none abort page change. //
                                GridView view = (GridView)gridControl1.MainView;
                                if (view.DataRowCount <= 0)
                                {
                                    xtraTabControl1.SelectedTabPage = xtraTabPageTakePicture;  // Go to Take Picture page //
                                }
                                else
                                {
                                    xtraTabControl1.SelectedTabPage = xtraTabPageGeneralInfo;  // Go to General Info page //  
                                }
                            }
                            _OriginalSurveyStatusID = Convert.ToInt32(currentRow["SurveyStatusID"]);  // Store original status so we can track if it changes on close and update any calling map if required //
                        }
                    }

                    break;
                case "view":
                    try
                    {
                        LoadPolePictures();
                        
                        sp07114_UT_Surveyed_Pole_ItemTableAdapter.Fill(this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item, strRecordIDs, strFormMode, _SurveyID);
                    }
                    catch (Exception)
                    {
                    }
                    xtraTabControl1.SelectedTabPage = xtraTabPageTakePicture;  // Go to Take Picture page //
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (intPassedInSurveyPoleID == 0)
            {
                // No SurveyPoleIDs [Called by Map] so get them from the Loaded Survey Poles dataset - these are neede for loaded linked records //
                foreach (DataRow dr in this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item.Rows)
                {
                    intPassedInSurveyPoleID += Convert.ToInt32(dr["SurveyedPoleID"]);
                }
            }

            DataRowView currentRow2 = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow2 != null)
            {
                sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp07464_UT_Linear_Cut_Lengths_With_BlankTableAdapter.Fill(dataSet_UT.sp07464_UT_Linear_Cut_Lengths_With_Blank, Convert.ToInt32(currentRow2["ClientID"]));
            }

            sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState16 = new RefreshGridState(gridView16, "PoleAccessIssueID");

            sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState17 = new RefreshGridState(gridView17, "PoleEnvironmentalIssueID");

            sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState18 = new RefreshGridState(gridView18, "PoleSiteHazardID");

            sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState19 = new RefreshGridState(gridView19, "PoleElectricalHazardID");

            sp00220_Linked_Documents_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState20 = new RefreshGridState(gridView20, "LinkedDocumentID");

            sp07401_UT_PD_Linked_To_ActionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState27 = new RefreshGridState(gridView27, "PermissionDocumentID");

            sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07366_UT_Traffic_Management_Items_With_BlankTableAdapter.Fill(dataSet_UT_Edit.sp07366_UT_Traffic_Management_Items_With_Blank);

            sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter.Connection.ConnectionString = strConnectionString;
            LoadTrafficManagementItems();

            sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            LoadHistoricalPolePictures();

            sp07120_UT_Survey_Infestation_Rate_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            try
            {
                sp07120_UT_Survey_Infestation_Rate_ListTableAdapter.Fill(dataSet_UT_Edit.sp07120_UT_Survey_Infestation_Rate_List);
            }
            catch (Exception)
            {
            }

            sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState13 = new RefreshGridState(gridView13, "SurveyPictureID");

            sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState12 = new RefreshGridState(gridView12, "TreeDefectID");

            sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState7 = new RefreshGridState(gridView7, "ActionID");

            sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState10 = new RefreshGridState(gridView10, "ActionMaterialID");

            sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState9 = new RefreshGridState(gridView9, "ActionEquipmentID");

            sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState8 = new RefreshGridState(gridView8, "SurveyPictureID");

            sp07127_UT_Surveyed_Pole_Work_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState6 = new RefreshGridState(gridView6, "ActionID");

            sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState5 = new RefreshGridState(gridView5, "SurveyPictureID");

            sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState4 = new RefreshGridState(gridView4, "TreeSpeciesID");

            sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState23 = new RefreshGridState(gridView23, "RiskQuestionID");

            sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState22 = new RefreshGridState(gridView22, "RiskAssessmentID");
            LoadLinkedRiskAssessments();

            sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState3 = new RefreshGridState(gridView3, "TreeID");
            LoadTrees();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            Attach_EditValueChanged_To_Children(dataLayoutControl1.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //

            emptyEditor = new RepositoryItem();
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Surveyed Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            LoadAccessIssues();
            LoadEnviromentalIssues();
            LoadSiteHazards();
            LoadElectricalHazards();
            LoadLinkedDocuments();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;

            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1 Surveyed Pole";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1 Surveyed Pole";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            if (strFormMode == "view")
            {
                btnFinish.Enabled = false;
                btnOnHold.Enabled = false;
                btnTakePicture.Enabled = false;
            }
            else
            {
                btnFinish.Enabled = true;
                btnOnHold.Enabled = true;
                btnTakePicture.Enabled = true;
                Set_Finish_Survey_Warning();
                Set_Deferred_Enabled_Status(null);
                Set_ShutdownHours_Enabled_Status(null);
            }
            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            DataSet dsChanges = this.dataSet_UT_Edit.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            GridView view = null;
            int[] intRowHandles;
            GridView gvParentTrees = (GridView)gridControl3.MainView;
            int[] intSelectedTreeHandles = gvParentTrees.GetSelectedRows();
            int intSelectedTrees = intSelectedTreeHandles.Length;

            GridView gvParentWork = (GridView)gridControl6.MainView;
            int[] intSelectedWorkHandles = gvParentWork.GetSelectedRows();

            int intTraficManagementRequired = 0;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                intTraficManagementRequired = Convert.ToInt32(currentRow["TrafficManagementRequired"]);
            }

            if (i_int_FocusedGrid == 1)  // Surveyed Pole Pictures //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 3)  // Linked Trees //
            {
                view = (GridView)gridControl3.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 5)  // Linked Tree Pictures //
            {

                view = (GridView)gridControl5.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intSelectedTrees == 1)  // Just 1 parent tree selected so we can add a picture to it //
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 6)  // Linked Work //
            {
                view = (GridView)gridControl6.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intSelectedTreeHandles.Length >= 1)
                {
                    bsiAdd.Enabled = true;
                    alItems.Add("sbiAdd");
                }
                else
                {
                    bsiAdd.Enabled = false;
                }
                if (strFormMode != "view" && intSelectedTreeHandles.Length == 1)
                {
                    alItems.Add("iAdd");
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intSelectedTreeHandles.Length > 1)
                {
                    alItems.Add("iBlockAdd");
                    bbiBlockAdd.Enabled = true;
                }
                else
                {
                    bbiBlockAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 12)  // Linked Tree Defects //
            {
                view = (GridView)gridControl12.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 16)  // Access Issues //
            {
                view = (GridView)gridControl16.MainView;
                intRowHandles = view.GetSelectedRows();
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 17)  // Environmental Issues //
            {
                view = (GridView)gridControl17.MainView;
                intRowHandles = view.GetSelectedRows();
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 18)  // Site Hazards //
            {
                view = (GridView)gridControl18.MainView;
                intRowHandles = view.GetSelectedRows();
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 19)  // Electrical Hazards //
            {
                view = (GridView)gridControl19.MainView;
                intRowHandles = view.GetSelectedRows();
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 20)  // Linked Documents //
            {
                view = (GridView)gridControl20.MainView;
                intRowHandles = view.GetSelectedRows();
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
                if (intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            else if (i_int_FocusedGrid == 22)  // Linked Risk Assesments //
            {
                view = (GridView)gridControl22.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view")
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    //if (intRowHandles.Length >= 2)
                    //{
                    //    alItems.Add("iBlockEdit");
                    //    bbiBlockEdit.Enabled = true;
                    //}
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 26)  // Linked Traffic Management Requirements //
            {
                view = (GridView)gridControl26.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intTraficManagementRequired == 1)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                bbiBlockEdit.Enabled = false;
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = false;
                if (strFormMode != "view" && intRowHandles.Length >= 1 && intTraficManagementRequired == 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            else if (i_int_FocusedGrid == 27)  // Linked Work Permissions //
            {
                view = (GridView)gridControl27.MainView;
                intRowHandles = view.GetSelectedRows();
                if (strFormMode != "view" && intSelectedWorkHandles.Length > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (strFormMode != "view" && intRowHandles.Length == 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                bbiBlockEdit.Enabled = false;
                if (strFormMode != "view" && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView3 navigator custom buttons //
            view = (GridView)gridControl3.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl3.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            
            // Set enabled status of GridView5 navigator custom buttons //
            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intSelectedTrees == 1);  // Enabled only if 1 parent tree selected //
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length == 1);
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            
            // Set enabled status of GridView6 navigator custom buttons //
            view = (GridView)gridControl6.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intSelectedTreeHandles.Length == 1);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intSelectedTreeHandles.Length > 1);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length == 1);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = true;
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[6].Enabled = (strFormMode != "view" && view.DataRowCount > 0);
            gridControl6.EmbeddedNavigator.Buttons.CustomButtons[7].Enabled = (strFormMode != "view" && !string.IsNullOrEmpty(GlobalSettings.CopiedUtilityJobIDs));

            // Set enabled status of GridView12 navigator custom buttons //
            view = (GridView)gridControl12.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl12.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            
            // Set enabled status of GridView7 navigator custom buttons //
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (intRowHandles.Length == 1 ? true : false);

            // Set enabled status of GridView16 navigator custom buttons //
            view = (GridView)gridControl16.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl16.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl16.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl16.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);

            // Set enabled status of GridView17 navigator custom buttons //
            view = (GridView)gridControl17.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl17.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
          
            // Set enabled status of GridView18 navigator custom buttons //
            view = (GridView)gridControl18.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl18.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            
            // Set enabled status of GridView19 navigator custom buttons //
            view = (GridView)gridControl19.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl19.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl19.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl19.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            
            // Set enabled status of GridView20 navigator custom buttons //
            view = (GridView)gridControl20.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl20.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl20.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl20.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
           
            // Set enabled status of GridView22 navigator custom buttons //
            view = (GridView)gridControl22.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl22.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl22.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl22.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl22.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length == 1 ? true : false);

            // Set enabled status of GridView26 navigator custom buttons //
            view = (GridView)gridControl26.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl26.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intTraficManagementRequired == 1);
            gridControl26.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 && intTraficManagementRequired == 1);

            // Set enabled status of GridView27 navigator custom buttons //
            view = (GridView)gridControl27.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl27.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intSelectedWorkHandles.Length > 0);
            gridControl27.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intSelectedWorkHandles.Length > 0);
            gridControl27.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (strFormMode != "view" && intRowHandles.Length == 1 ? true : false);
            gridControl27.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (strFormMode != "view" && intRowHandles.Length > 0 ? true : false);
            gridControl27.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);
        }


        private void frm_UT_Survey_Pole2_Activated(object sender, EventArgs e)
        {
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDs3))
                {
                    LoadTrees();
                }
                else if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDs4))
                {
                    LoadLinkedTreeSpecies();
                }
                else if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedRecordIDs5))
                {
                    LoadLinkedTreePictures();
                }
                else if (UpdateRefreshStatus == 6 || !string.IsNullOrEmpty(i_str_AddedRecordIDs6))
                {
                    LoadLinkedTreeWork();
                    LoadLinkedTreeWorkPictures();
                    LoadLinkedTreeWorkEquiment();
                    LoadLinkedTreeWorkMaterials();
                    LoadLinkedWorkPermissions();
                }
                else if (UpdateRefreshStatus == 8 || !string.IsNullOrEmpty(i_str_AddedRecordIDs8))
                {
                    LoadLinkedTreeWorkPictures();
                }
                else if (UpdateRefreshStatus == 9 || !string.IsNullOrEmpty(i_str_AddedRecordIDs9))
                {
                    LoadLinkedTreeWorkEquiment();
                }
                else if (UpdateRefreshStatus == 10 || !string.IsNullOrEmpty(i_str_AddedRecordIDs10))
                {
                    LoadLinkedTreeWorkMaterials();
                }
                else if (UpdateRefreshStatus == 12 || !string.IsNullOrEmpty(i_str_AddedRecordIDs12))
                {
                    LoadLinkedTreeDefects();
                    LoadLinkedTreeDefectPictures();
                }
                else if (UpdateRefreshStatus == 13 || !string.IsNullOrEmpty(i_str_AddedRecordIDs13))
                {
                   LoadLinkedTreeDefectPictures();
                }
                else if (UpdateRefreshStatus == 16 || !string.IsNullOrEmpty(i_str_AddedRecordIDs16))
                {
                    LoadAccessIssues();
                }
                else if (UpdateRefreshStatus == 17 || !string.IsNullOrEmpty(i_str_AddedRecordIDs17))
                {
                    LoadEnviromentalIssues();
                }
                else if (UpdateRefreshStatus == 18 || !string.IsNullOrEmpty(i_str_AddedRecordIDs18))
                {
                    LoadSiteHazards();
                }
                else if (UpdateRefreshStatus == 19 || !string.IsNullOrEmpty(i_str_AddedRecordIDs19))
                {
                    LoadElectricalHazards();
                }
                else if (UpdateRefreshStatus == 20 || !string.IsNullOrEmpty(i_str_AddedRecordIDs20))
                {
                    LoadLinkedDocuments();
                }
                else if (UpdateRefreshStatus == 27 || !string.IsNullOrEmpty(i_str_AddedRecordIDs27))
                {
                    LoadLinkedWorkPermissions();
                }
                else if (UpdateRefreshStatus == 22 || !string.IsNullOrEmpty(i_str_AddedRecordIDs22))
                {
                    LoadLinkedRiskAssessments();
                }
            }
            SetMenuStatus();
        }

        private void frm_UT_Survey_Pole2_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }

        private void frm_UT_Survey_Pole2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        Detach_EditValuechanged_From_Children(dataLayoutControl1.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs3, string strNewIDs4, string strNewIDs5, string strNewIDs6, string strNewIDs8, string strNewIDs9, string strNewIDs10, string strNewIDs12, string strNewIDs13, string strNewIDs11, string strNewIDs16, string strNewIDs17, string strNewIDs18, string strNewIDs19, string strNewIDs20, string strNewIDs27, string strNewIDs22)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
            if (strNewIDs4 != "") i_str_AddedRecordIDs4 = strNewIDs4;
            if (strNewIDs5 != "") i_str_AddedRecordIDs5 = strNewIDs5;
            if (strNewIDs6 != "") i_str_AddedRecordIDs6 = strNewIDs6;
            if (strNewIDs8 != "") i_str_AddedRecordIDs8 = strNewIDs8;
            if (strNewIDs9 != "") i_str_AddedRecordIDs9 = strNewIDs9;
            if (strNewIDs10 != "") i_str_AddedRecordIDs10 = strNewIDs10;
            if (strNewIDs12 != "") i_str_AddedRecordIDs12 = strNewIDs12;
            if (strNewIDs13 != "") i_str_AddedRecordIDs13 = strNewIDs13;
            if (strNewIDs16 != "") i_str_AddedRecordIDs16 = strNewIDs16;
            if (strNewIDs17 != "") i_str_AddedRecordIDs17 = strNewIDs17;
            if (strNewIDs18 != "") i_str_AddedRecordIDs18 = strNewIDs18;
            if (strNewIDs19 != "") i_str_AddedRecordIDs19 = strNewIDs19;
            if (strNewIDs20 != "") i_str_AddedRecordIDs20 = strNewIDs20;
            if (strNewIDs27 != "") i_str_AddedRecordIDs27 = strNewIDs27;
            if (strNewIDs22 != "") i_str_AddedRecordIDs22 = strNewIDs22;
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07114_UT_Surveyed_Pole_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            int intRecordNewTM = 0;
            int intRecordModifiedTM = 0;
            int intRecordDeletedTM = 0;
            gridControl26.MainView.PostEditor();
            this.sp07367UTTrafficManagementForSurveyedPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
            for (int i = 0; i < this.dataSet_UT_Edit.sp07367_UT_Traffic_Management_For_Surveyed_Pole.Rows.Count; i++)
            {
                switch (this.dataSet_UT_Edit.sp07367_UT_Traffic_Management_For_Surveyed_Pole.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNewTM++;
                        break;
                    case DataRowState.Modified:
                        intRecordModifiedTM++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeletedTM++;
                        break;
                }
            }

            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0 || intRecordNewTM > 0 || intRecordModifiedTM > 0 || intRecordDeletedTM > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New Surveyed Pole record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated Surveyed Pole record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted Surveyed Pole record(s)\n";
                if (intRecordNewTM > 0) strMessage += Convert.ToString(intRecordNewTM) + " New Traffic Management Requirement record(s)\n";
                if (intRecordModifiedTM > 0) strMessage += Convert.ToString(intRecordModifiedTM) + " Updated Traffic Management Requirement record(s)\n";
                if (intRecordDeletedTM > 0) strMessage += Convert.ToString(intRecordDeletedTM) + " Deleted Traffic Management Requirement record(s)\n";
            }
            return strMessage;
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                    xtraTabControl1.SelectedTabPage = xtraTabPageGeneralInfo;  // Make tabe page active //
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp07114UTSurveyedPoleItemBindingSource.EndEdit();
            try
            {
                this.sp07114_UT_Surveyed_Pole_ItemTableAdapter.Update(dataSet_UT_Edit);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            int intPoleID = 0;
            int intSurveyStatus = 0;
            int intShutdown = 0;
            int intTrafficManagement = 0;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (this.strFormMode.ToLower() == "add")
            {
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["SurveyedPoleID"]) + ";";
                }

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            if (currentRow != null)
            {
                // Surveyed Pole Status may have changed due to child triggers firing so pick it up from DB just in case //
                DataSet_UTTableAdapters.QueriesTableAdapter GetStatus = new DataSet_UTTableAdapters.QueriesTableAdapter();
                GetStatus.ChangeConnectionString(strConnectionString);
                intSurveyStatus = Convert.ToInt32(GetStatus.sp07355_UT_Surveyed_Pole_Get_Status(Convert.ToInt32(currentRow["SurveyedPoleID"])));
                if (intSurveyStatus != Convert.ToInt32(currentRow["SurveyStatusID"]))
                {
                    currentRow["SurveyStatusID"] = intSurveyStatus;
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }

                intPoleID = Convert.ToInt32(currentRow["PoleID"]);
                intShutdown = Convert.ToInt32(currentRow["IsShutdownRequired"]);
                intTrafficManagement = Convert.ToInt32(currentRow["TrafficManagementRequired"]);
            }

            try  // Attempt to save any changes to Linked Traffic Management Requirements... //
            {
                this.sp07367UTTrafficManagementForSurveyedPoleBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter.Update(dataSet_UT_Edit);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the Traffic Management Requirement changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Survey_Edit")
                    {
                        var fParentForm = (frm_UT_Survey_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "");
                    }
                    if (frmChild.Name == "frm_UT_Mapping")  // Refresh Map to say pole has been surveyed or not //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.Update_Pole_Survey_Status(intPoleID, intSurveyStatus, intShutdown, intTrafficManagement);
                    }
                    if (frmChild.Name == "frm_UT_Surveyed_Pole_Manager")
                    {
                        var fParentForm = (frm_UT_Surveyed_Pole_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        private void xtraTabControl1_SelectedPageChanging(object sender, DevExpress.XtraTab.TabPageChangingEventArgs e)
        {
            if (ibool_FormStillLoading) return;

            switch (xtraTabControl1.SelectedTabPage.Name.ToString())
            {

                case "xtraTabPageWelcome":
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (Convert.ToInt32(currentRow["SurveyStatusID"]) <= 0)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("Click the Start Survey button to proceed.", "Start Survey", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                btnStart.Focus();
                                e.Cancel = true;
                            }
                            else if (e.Page.Name != "xtraTabPageTakePicture")
                            {
                                // Check if at least 1 picture is present - If none abort page change. //
                                GridView view = (GridView)gridControl1.MainView;
                                if (view.DataRowCount <= 0)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("No Picture has been taken of the pole - unable to proceed.\n\nTake at least one picture before proceeding.", "Mising Picture of Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    e.Cancel = true;
                                    xtraTabControl1.SelectedTabPage = xtraTabPageTakePicture;
                                }
                            }
                        }
                        break;
                    }
                case "xtraTabPageTakePicture":
                    {
                        if (e.Page.Name != "xtraTabPageWelcome")
                        {
                            // Check if at least 1 picture is present - If none abort page change. //
                            GridView view = (GridView)gridControl1.MainView;
                            if (view.DataRowCount <= 0)
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture has been taken of the pole - unable to proceed.\n\nTake at least one picture before proceeding.", "Mising Picture of Pole", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
            }
        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageWelcome":
                    {
                        btnPrior.Enabled = false;
                        btnNext.Enabled = true;
                        labelControlStep.Text = "Step 1 of 6";
                        labelControlStep.ForeColor = Color.Red;
                        break;
                    }
                case "xtraTabPageTakePicture":
                    {
                        btnPrior.Enabled = true;
                        btnNext.Enabled = true;
                        labelControlStep.Text = "Step 2 of 6";
                        labelControlStep.ForeColor = Color.Red;
                        break;
                    }
                case "xtraTabPageGeneralInfo":
                    {
                        btnPrior.Enabled = true;
                        btnNext.Enabled = true;
                        labelControlStep.Text = "Step 3 of 6";
                        labelControlStep.ForeColor = Color.Red;
                        this.ValidateChildren();
                        Check_Clearance_Above(null);
                        break;
                    }
                case "xtraTabPageTrees":
                    {
                        btnPrior.Enabled = true;
                        btnNext.Enabled = true;
                        labelControlStep.Text = "Step 4 of 6";
                        labelControlStep.ForeColor = Color.Red;
                        break;
                    }
                case "xtraTabPageRiskAssessment":
                    {
                        btnPrior.Enabled = true;
                        btnNext.Enabled = true;
                        labelControlStep.Text = "Step 5 of 6";
                        labelControlStep.ForeColor = Color.Red;
                        break;
                    }
                case "xtraTabPageFinish":
                    {
                        btnPrior.Enabled = true;
                        btnNext.Enabled = false;
                        Set_Finish_Survey_Warning();
                        labelControlStep.Text = "Step 6 of 6";
                        labelControlStep.ForeColor = Color.ForestGreen;
                        break;
                    }
            }
        }

        private void btnPrior_Click(object sender, EventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name.ToString())
            {
                case "xtraTabPageWelcome":
                    {
                        break;
                    }
                case "xtraTabPageTakePicture":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageWelcome;
                        break;
                    }
                case "xtraTabPageGeneralInfo":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageTakePicture;
                        break;
                    }
                case "xtraTabPageTrees":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageGeneralInfo;
                        break;
                    }
                case "xtraTabPageRiskAssessment":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageTrees;
                         break;
                    }
                case "xtraTabPageFinish":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageRiskAssessment;
                        break;
                    }
            }
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name.ToString())
            {
                case "xtraTabPageWelcome":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageTakePicture;
                        break;
                    }
                case "xtraTabPageTakePicture":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageGeneralInfo;
                        break;
                    }
                case "xtraTabPageGeneralInfo":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageTrees;
                        break;
                    }
                case "xtraTabPageTrees":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageRiskAssessment;
                        break;
                    }
                case "xtraTabPageRiskAssessment":
                    {
                        xtraTabControl1.SelectedTabPage = xtraTabPageFinish;
                        break;
                    }
                case "xtraTabPageFinish":
                    {
                        break;
                    }
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Survey Pictures - Click Take Picture button to take a picture";
                    break;
                case "gridView2":
                    message = "No Historical Survey Pictures available";
                    break;
                case "gridView26":
                    message = "No Traffic Management Requirements - Click Add button to map Traffic Management Requirements";
                    break;
                case "gridView3":
                    message = "No Linked Trees - Click Add button to map linked trees";
                    break;
                case "gridView4":
                    message = "No Linked Species - Select a Tree to view linked Species";
                    break;
                case "gridView5":
                    message = "No Linked Tree Pictures - Click Add button to take tree pictures";
                    break;
                case "gridView6":
                    message = "No Linked Work - Select one or more Trees to view linked Work";
                    break;
                case "gridView12":
                    message = "No Linked Tree Defects - Select one or more Trees to view linked Defects";
                    break;
                case "gridView13":
                    message = "No Linked Defect Pictures - Select one or more Tree Defects to view linked Defect Pictures";
                    break;
                case "gridView8":
                    message = "No Linked Work Pictures - Select one or more Jobs to view linked Work Pictures";
                    break;
                case "gridView9":
                    message = "No Linked Work Equipment - Select one or more Jobs to view linked Work Equipment";
                    break;
                case "gridView10":
                    message = "No Linked Work Materials - Select one or more Jobs to view linked Work Materials";
                    break;
                case "gridView7":
                    message = "No Linked Historical Work - Select one or more Trees to view linked Historical Work";
                    break;
                case "gridView27":
                    message = "No Permissions - Select one or more work records to see linked Work Permission";
                    break;
                case "gridView22":
                    message = "No Linked Risk Assessments";
                    break;
                case "gridView23":
                    message = "No Linked Risk Assessment Questions - Select one or more Risk Assessments to see Questions";
                    break;
                case "gridView16":
                    message = "No Linked Access Issues Available";
                    break;
                case "gridView17":
                    message = "No Linked Environmental Issues Available";
                    break;
                case "gridView18":
                    message = "No Linked Site Hazards Available";
                    break;
                case "gridView19":
                    message = "No Linked Electrical Hazards Available";
                    break;
                case "gridView20":
                    message = "No Linked Documents Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region Welcome Page

        private void btnStart_Click(object sender, EventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                if (string.IsNullOrEmpty(currentRow["SurveyDate"].ToString()))
                {
                    currentRow["SurveyDate"] = DateTime.Now;
                    currentRow["SurveyStatusID"] = 1;
                    currentRow["SurveyStatus"] = "Survey Started" ;
                    sp07114UTSurveyedPoleItemBindingSource.EndEdit();
                    btnNext.Enabled = true;
                }
            }
            btnNext.PerformClick();  // Move to next page //
        }

        #endregion


        #region Survey Picture(s) Page

        private void btnTakePicture_Click(object sender, EventArgs e)
        {
            frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
            fChildForm.strCaller = this.Name;
            fChildForm.strConnectionString = strConnectionString;
            fChildForm.strFormMode = "add";
            fChildForm.intAddToRecordID = intPassedInSurveyPoleID;
            fChildForm.intAddToRecordTypeID = 0;  // 1 = Surveyed Pole //
            fChildForm._PassedInFileNameSuffix = "";
            fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
            fChildForm.ShowDialog();
            if (fChildForm._AddedPolePictureID != 0)  // At least 1 picture added to refresh picture grid //
            {
                i_str_AddedRecordIDs1 = fChildForm._AddedPolePictureID.ToString() + ";";
                UpdateRefreshStatus = 1;
                this.RefreshGridViewState1.SaveViewInfo();
                LoadPolePictures();
            }
        }


        #region GridView1

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 1;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadPolePictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }


        #endregion


        #region GridView2

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl2_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 2;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadHistoricalPolePictures();
                    }
                    break;
                default:
                    break;
            }
        }


        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        private void LoadPolePictures()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl1.MainView.BeginUpdate();
            this.RefreshGridViewState1.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07117_UT_Pole_Survey_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07117_UT_Pole_Survey_Pictures_List, intPassedInSurveyPoleID, 0, strDefaultPath);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl1.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        private void LoadHistoricalPolePictures()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                int intPoleID = Convert.ToInt32(currentRow["PoleID"]);
                int intSurveyID = Convert.ToInt32(currentRow["SurveyID"]);
                try
                {
                    sp07119_UT_Pole_Survey_Historical_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07119_UT_Pole_Survey_Historical_Pictures_List, intSurveyID, intPoleID, strDefaultPath);
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion


        #region General Info Page 

        private void LoadTrafficManagementItems()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["SurveyedPoleID"].ToString() + ",";

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl26.MainView;
            view.BeginUpdate();
            try
            {
                sp07367_UT_Traffic_Management_For_Surveyed_PoleTableAdapter.Fill(dataSet_UT_Edit.sp07367_UT_Traffic_Management_For_Surveyed_Pole, strSelectedIDs);
            }
            catch (Exception)
            {
            }
            view.EndUpdate();
        }


        #region GridView26

        private void gridView26_DoubleClick(object sender, EventArgs e)
        {
        }

        private void gridView26_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 26;
            SetMenuStatus();
        }

        private void gridView26_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView26_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl26_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 26;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void DeferredCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            Set_Deferred_Enabled_Status((CheckEdit)sender);
        }
        private void DeferredCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //
        }
        private void DeferredCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            CheckEdit ce = (CheckEdit)sender;
            int intValue = Convert.ToInt32(ce.EditValue);

            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;

            currentRow["DeferredUnitDescriptiorID"] = (intValue == 1 ? 4 : 0);
            sp07114UTSurveyedPoleItemBindingSource.EndEdit();

            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(DeferralReasonIDGridLookUpEdit.EditValue.ToString()) || DeferralReasonIDGridLookUpEdit.EditValue.ToString() == "0") && ce.Checked)
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "Select a value.");
            }
            else
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "");
            }

            Check_Revisit_Date();
        }

        private void Set_Deferred_Enabled_Status(CheckEdit ce)
        {
            if (strFormMode == "view") return;
            if (ce == null)
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                if (Convert.ToInt32(currentRow["DeferredUnits"]) != 0)
                {
                    DeferredUnitsSpinEdit.Properties.ReadOnly = false;
                    DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ReadOnly = false;
                    DeferralReasonIDGridLookUpEdit.Properties.ReadOnly = false;
                    DeferralRemarksMemoEdit.Properties.ReadOnly = false;
                    DeferredEstimatedCuttingHoursSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    DeferredUnitsSpinEdit.Properties.ReadOnly = true;
                    DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ReadOnly = true;
                    DeferralReasonIDGridLookUpEdit.Properties.ReadOnly = true;
                    DeferralRemarksMemoEdit.Properties.ReadOnly = true;
                    DeferredEstimatedCuttingHoursSpinEdit.Properties.ReadOnly = true;
                }
            }
            else
            {
                if (ce.Checked)
                {
                    DeferredUnitsSpinEdit.Properties.ReadOnly = false;
                    DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ReadOnly = false;
                    DeferralReasonIDGridLookUpEdit.Properties.ReadOnly = false;
                    DeferralRemarksMemoEdit.Properties.ReadOnly = false;
                    DeferredEstimatedCuttingHoursSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    DeferredUnitsSpinEdit.Properties.ReadOnly = true;
                    DeferredUnitDescriptiorIDGridLookUpEdit.Properties.ReadOnly = true;
                    DeferralReasonIDGridLookUpEdit.Properties.ReadOnly = true;
                    DeferralRemarksMemoEdit.Properties.ReadOnly = true;
                    DeferredEstimatedCuttingHoursSpinEdit.Properties.ReadOnly = true;
                    if (!ibool_FormStillLoading)
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            if (Convert.ToInt32(currentRow["DeferredUnits"]) != 0) currentRow["DeferredUnits"] = 0;  // Clear as this pole is not deferred //
                            if (Convert.ToInt32(currentRow["DeferredUnitDescriptiorID"]) != 0) currentRow["DeferredUnitDescriptiorID"] = 0;  // Clear as this pole is not deferred //
                            if (Convert.ToInt32(currentRow["DeferralReasonID"]) != 0) currentRow["DeferralReasonID"] = 0;  // Clear as this pole is not deferred //
                            if (!string.IsNullOrEmpty(currentRow["DeferralRemarks"].ToString())) currentRow["DeferralRemarks"] = "";  // Clear as this pole is not deferred //
                            if (Convert.ToDecimal(currentRow["DeferredEstimatedCuttingHours"]) > (decimal)0.00) currentRow["DeferredEstimatedCuttingHours"] = (decimal)0.00;  // Clear as this pole is not deferred //
                            sp07114UTSurveyedPoleItemBindingSource.EndEdit();
                        }
                    }
                }
            }
        }

        private void DeferredUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //
        }
        private void DeferredUnitsSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            Calculate_Revisit_Date();
        }

        private void DeferredUnitDescriptiorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //
        }
        private void DeferredUnitDescriptiorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            Calculate_Revisit_Date();
        }

        private void RevisitDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
        }
        private void RevisitDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            Check_Revisit_Date();
        }


        private void Calculate_Revisit_Date()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;

            int DeferredUnits = Convert.ToInt32(DeferredUnitsSpinEdit.EditValue);
            int DeferredUnitDescriptiorID = Convert.ToInt32(DeferredUnitDescriptiorIDGridLookUpEdit.EditValue);

            DateTime CurrentDate = DateTime.Today;
            DateTime NextDate = DateTime.MinValue;
            if (DeferredUnits > 0 && DeferredUnitDescriptiorID > 0)
            {
                switch (DeferredUnitDescriptiorID)
                {
                    case 1:  // Days //
                        NextDate = CurrentDate.AddDays((double)DeferredUnits);
                        break;
                    case 2:  // Weeks //
                        NextDate = CurrentDate.AddDays((double)DeferredUnits * 7);
                        break;
                    case 3:  // Months //
                        NextDate = CurrentDate.AddMonths(DeferredUnits);
                        break;
                    case 4:  // Years //
                        NextDate = CurrentDate.AddYears(DeferredUnits);
                        break;
                    default:
                        break;
                }
                if (NextDate != DateTime.MinValue)
                {
                    currentRow["RevisitDate"] = NextDate;
                }
                else
                {
                    currentRow["RevisitDate"] = DBNull.Value;
                }
            }
            else
            {
                currentRow["RevisitDate"] = DBNull.Value;
            }
            sp07114UTSurveyedPoleItemBindingSource.EndEdit();

            Check_Revisit_Date();
        }
        private void Check_Revisit_Date()
        {
            if (this.strFormMode == "blockedit") return;
            bool boolErrorFound = false;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;

            int intDeffered = Convert.ToInt32(currentRow["Deferred"]);
            DateTime dtSurveyDate = DateTime.MinValue;
            try { dtSurveyDate = Convert.ToDateTime(currentRow["SurveySurveyDate"]); }
            catch (Exception) { }
            if ((RevisitDateDateEdit.EditValue == null || string.IsNullOrEmpty(RevisitDateDateEdit.EditValue.ToString())) && intDeffered == 1)
            {
                boolErrorFound = true;
            }
            else if (!(RevisitDateDateEdit.EditValue == null || string.IsNullOrEmpty(RevisitDateDateEdit.EditValue.ToString())) && RevisitDateDateEdit.DateTime.Year - dtSurveyDate.Year < 1)
            {
                boolErrorFound = true;
            }
            if (boolErrorFound)
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "Enter a Date at least a year later than the Survey Date [" + dtSurveyDate.ToString("dd/MM/yyyy") + "].");
            }
            else
            {
                dxErrorProvider1.SetError(RevisitDateDateEdit, "");
            }

        }


        private void DeferralReasonIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0") && DeferredCheckEdit.Checked)
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DeferralReasonIDGridLookUpEdit, "");
            }
        }

        private void G55CategoryIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            /*GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(G55CategoryIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(G55CategoryIDGridLookUpEdit, "");
            }*/
        }

        private void IsSpanClearCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)IsSpanClearCheckEdit;
            if (ce.Checked)
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                currentRow["NoWorkRequired"] = 1;
            }
        }

        private void ClearanceDistanceGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ClearanceDistanceGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = Convert.ToInt32(glue.EditValue);
            
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intValue2 = Convert.ToInt32(currentRow["ClearanceDistanceUnderID"]);
            int intValue3 = Convert.ToInt32(currentRow["ClearanceDistanceAboveID"]);
            currentRow["TreeWithin3Meters"] = (intValue > 0 && (intValue <= 7 || intValue2 <= 7 || intValue3 <= 7) ? 1 : 0);
            sp07114UTSurveyedPoleItemBindingSource.EndEdit();
        }

        private void ClearanceDistanceUnderIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void ClearanceDistanceUnderIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = Convert.ToInt32(glue.EditValue);
            
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intValue2 = Convert.ToInt32(currentRow["ClearanceDistance"]);
            int intValue3 = Convert.ToInt32(currentRow["ClearanceDistanceAboveID"]);
            currentRow["TreeWithin3Meters"] = (intValue > 0 && (intValue <= 7 || intValue2 <= 7 || intValue3 <= 7) ? 1 : 0);
            sp07114UTSurveyedPoleItemBindingSource.EndEdit();
        }

        private void HeldUpTypeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //       
        }
        private void HeldUpTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = true;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = Convert.ToInt32(glue.EditValue);

            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intValue2 = Convert.ToInt32(currentRow["IsShutdownRequired"]);
            if (intValue == 1 && intValue2 != 1)  // Shutdown as Held Up Type and Is Shutdown Required not ticked //
            {
                currentRow["IsShutdownRequired"] = 1;
                Set_ShutdownHours_Enabled_Status(null);
                sp07114UTSurveyedPoleItemBindingSource.EndEdit();
            }
        }

        private void IsShutdownRequiredCheckEdit_Validated(object sender, EventArgs e)
        {
            Set_ShutdownHours_Enabled_Status((CheckEdit)sender);
        }

        private void ShutdownHoursSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(se.EditValue.ToString()) || se.EditValue.ToString() == "0.00") && IsShutdownRequiredCheckEdit.Checked)
            {
                dxErrorProvider1.SetError(ShutdownHoursSpinEdit, "Enter a value when 'Is Shutdown Required' ticked.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ShutdownHoursSpinEdit, "");
            }
        }
        private void Set_ShutdownHours_Enabled_Status(CheckEdit ce)
        {
            bool boolIsShutdownRequired = false;
            if (strFormMode == "view") return;
            if (ce == null)
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                boolIsShutdownRequired = (Convert.ToInt32(currentRow["IsShutdownRequired"]) == 0 ? false : true);
            }
            else
            {
                boolIsShutdownRequired = ce.Checked;
            }
            if (!boolIsShutdownRequired)
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                ShutdownHoursSpinEdit.Properties.ReadOnly = true;
                dxErrorProvider1.SetError(ShutdownHoursSpinEdit, "");

                if (!ibool_FormStillLoading)
                {
                    if (!string.IsNullOrEmpty(currentRow["ShutdownHours"].ToString())) currentRow["ShutdownHours"] = "0.00";  // Clear as this is not a shutdown //
                    sp07114UTSurveyedPoleItemBindingSource.EndEdit();
                }
            }
            else
            {
                ShutdownHoursSpinEdit.Properties.ReadOnly = false;

                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow != null)
                {
                    if (this.strFormMode != "blockedit" && (Convert.ToDecimal(ShutdownHoursSpinEdit.EditValue) <= (decimal)0.00)) dxErrorProvider1.SetError(ShutdownHoursSpinEdit, "Enter a value when 'Is Shutdown Required' ticked.");
                }
            }
        }

        private void ClearanceDistanceAboveIDFGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //
        }
        private void ClearanceDistanceAboveIDFGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_FormStillLoading || ibool_ignoreValidation) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int intValue = Convert.ToInt32(glue.EditValue);
           
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            int intValue2 = Convert.ToInt32(currentRow["ClearanceDistance"]);
            int intValue3 = Convert.ToInt32(currentRow["ClearanceDistanceUnderID"]);
            currentRow["TreeWithin3Meters"] = (intValue > 0 && (intValue <= 7 || intValue2 <= 7 || intValue3 <= 7) ? 1 : 0);
            sp07114UTSurveyedPoleItemBindingSource.EndEdit();

            Check_Clearance_Above(glue);
        }
        private void Check_Clearance_Above(GridLookUpEdit glue)
        {
            bool boolError = false;
            if (strFormMode == "view") return;
            if (glue == null)
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                boolError = (Convert.ToInt32(currentRow["ClearanceDistanceAboveID"]) > 0 || Convert.ToInt32(currentRow["ClientID"]) != ClientUKPN ? false : true);
            }
            else
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                boolError = (Convert.ToInt32(glue.EditValue) > 0 || Convert.ToInt32(currentRow["ClientID"]) != ClientUKPN ? false : true);
            }
            if (!boolError)
            {
                dxErrorProvider1.SetError(ClearanceDistanceAboveIDFGridLookUpEdit, "");
            }
            else
            {
                dxErrorProvider1.SetError(ClearanceDistanceAboveIDFGridLookUpEdit, "Select a value [Required for UKPN].");
            }
        }

        private void TrafficManagementRequiredCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)TrafficManagementRequiredCheckEdit;
            if (ce.Checked)
            {
                gridControl26.Enabled = true;
            }
            else  // TM not ticked //
            {
                Clear_Traffic_Management_Requirements();
                gridControl26.Enabled = false;
            }
            SetMenuStatus();
            Check_Traffic_Management();
        }
        private void TrafficManagementRequiredCheckEdit_Validating(object sender, CancelEventArgs e)
        {
        }
        private void Clear_Traffic_Management_Requirements()
        {
            // Clear any TM Requirements //
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControl26.MainView;
            if (view.DataRowCount > 0)
            {
                view.BeginUpdate();
                for (int i = view.DataRowCount - 1; i >= 0; i--)
                {
                    view.DeleteRow(i);
                }
                view.EndUpdate();
            }
        }
        private void Check_Traffic_Management()
        {
            if (strFormMode == "blockedit" || strFormMode == "view") return;
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            bool boolErrorFound = false;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;

            int intTrafficManagementRequired = Convert.ToInt32(currentRow["TrafficManagementRequired"]);
            if (intTrafficManagementRequired == 1)
            {
                GridView view = (GridView)gridControl26.MainView;
                if (view.DataRowCount <= 0) boolErrorFound = true;
            }
            if (boolErrorFound)
            {
                dxErrorProvider1.SetError(TrafficManagementRequiredCheckEdit, "Add at least one Traffic Management Requirement record to the Traffic Management Requirement Grid.");
            }
            else
            {
                dxErrorProvider1.SetError(TrafficManagementRequiredCheckEdit, "");
            }

        }

        private void AccessMapPathButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "view")
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                string strMap = (string.IsNullOrEmpty(currentRow["AccessMapPath"].ToString()) ? "" : currentRow["AccessMapPath"].ToString());
                if (string.IsNullOrEmpty(strMap))
                {
                    XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                strMap += ".gif";
                string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                frm_preview.strImage = strImagePath;
                frm_preview.ShowDialog();
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Clear the Linked Map.\n\nAre you sure you wish to procced?", "Clear Linked Map", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
                currentRow["AccessMapPath"] = null;
                sp07114UTSurveyedPoleItemBindingSource.EndEdit();
            }
        }
 
        private void TrafficMapPathButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "view")
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                string strMap = (string.IsNullOrEmpty(currentRow["TrafficMapPath"].ToString()) ? "" : currentRow["TrafficMapPath"].ToString());
                if (string.IsNullOrEmpty(strMap))
                {
                    XtraMessageBox.Show("No Map Linked for Viewing.", "View Linked Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                strMap += ".gif";
                string strImagePath = Path.Combine(strDefaultMapPath, strMap);

                frm_AT_Mapping_WorkOrder_Map_Preview frm_preview = new frm_AT_Mapping_WorkOrder_Map_Preview();
                frm_preview.strImage = strImagePath;
                frm_preview.ShowDialog();
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow == null) return;
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to Clear the Linked Map.\n\nAre you sure you wish to procced?", "Clear Linked Map", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;
                currentRow["TrafficMapPath"] = null;
                sp07114UTSurveyedPoleItemBindingSource.EndEdit();
            }
        }

        public void Update_Linked_Map_Path(int intMapType, string strSurveyedPoleIDs, string strMapPath)
        {
            // Called by UT_Mapping screen //
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSurveyedPoleID = (string.IsNullOrEmpty(currentRow["SurveyedPoleID"].ToString()) ? "" : currentRow["SurveyedPoleID"].ToString());

            if (string.IsNullOrEmpty(strSurveyedPoleID)) return;
            strSurveyedPoleID = "," + strSurveyedPoleID + ",";
            strSurveyedPoleIDs = "," + strSurveyedPoleIDs;
            if (strSurveyedPoleIDs.Contains(strSurveyedPoleID))
            {
                if (intMapType == 3)  // Access Map //
                {
                    currentRow["AccessMapPath"] = strMapPath;
                }
                else if (intMapType == 4)  // Traffic Map //
                {
                    currentRow["TrafficMapPath"] = strMapPath;
                }
                sp07114UTSurveyedPoleItemBindingSource.EndEdit();
            }
        }

        #endregion


        #region Tree Page

        private void LoadTrees()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl3.MainView.BeginUpdate();
            this.RefreshGridViewState3.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07121_UT_Surveyed_Pole_Linked_TreesTableAdapter.Fill(dataSet_UT_Edit.sp07121_UT_Surveyed_Pole_Linked_Trees, intPassedInSurveyPoleID, strFormMode);
                this.RefreshGridViewState3.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl3.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs3 != "")
            {
                strArray = i_str_AddedRecordIDs3.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl3.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["TreeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs3 = "";
            }
        }

        private void LoadLinkedTreeSpecies()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
            }
            gridControl4.MainView.BeginUpdate();
            this.RefreshGridViewState4.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07122_UT_Surveyed_Pole_Species_Linked_To_Tree.Clear();
            }
            else
            {
                try
                {
                    sp07122_UT_Surveyed_Pole_Species_Linked_To_TreeTableAdapter.Fill(dataSet_UT_Edit.sp07122_UT_Surveyed_Pole_Species_Linked_To_Tree, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState4.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl4.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs4 != "")
            {
                strArray = i_str_AddedRecordIDs4.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl4.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["TreeSpeciesID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs4 = "";
            }
        }

        private void LoadLinkedTreePictures()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
            }
            gridControl5.MainView.BeginUpdate();
            this.RefreshGridViewState5.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07123_UT_Surveyed_Pole_Tree_Pictures_List.Clear();
            }
            else
            {
                try
                {
                    sp07123_UT_Surveyed_Pole_Tree_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07123_UT_Surveyed_Pole_Tree_Pictures_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath);
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState5.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl5.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs5 != "")
            {
                strArray = i_str_AddedRecordIDs5.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl5.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs5 = "";
            }
        }

        private void LoadLinkedTreeWork()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyedTreeID"])) + ',';
            }
            gridControl6.MainView.BeginUpdate();
            this.RefreshGridViewState6.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07127_UT_Surveyed_Pole_Work_List.Clear();
            }
            else
            {
                try
                {
                    sp07127_UT_Surveyed_Pole_Work_ListTableAdapter.Fill(dataSet_UT_Edit.sp07127_UT_Surveyed_Pole_Work_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState6.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl6.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs6 != "")
            {
                strArray = i_str_AddedRecordIDs6.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl6.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs6 = "";
            }
        }

        private void LoadLinkedTreeWorkPictures()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
            }
            gridControl8.MainView.BeginUpdate();
            this.RefreshGridViewState8.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07130_UT_Surveyed_Pole_Work_Pictures_List.Clear();
            }
            else
            {
                try
                {
                    sp07130_UT_Surveyed_Pole_Work_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07130_UT_Surveyed_Pole_Work_Pictures_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath);
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState8.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl8.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs8 != "")
            {
                strArray = i_str_AddedRecordIDs8.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl8.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs8 = "";
            }
        }

        private void LoadLinkedTreeWorkEquiment()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
            }
            gridControl9.MainView.BeginUpdate();
            this.RefreshGridViewState9.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_Only.Clear();
            }
            else
            {
                try
                {
                    sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_OnlyTableAdapter.Fill(dataSet_UT_Edit.sp07139_UT_Surveyed_Tree_Work_Linked_Equipment_Read_Only, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState9.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl9.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs9 != "")
            {
                strArray = i_str_AddedRecordIDs9.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl9.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionEquipmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs9 = "";
            }
        }

        private void LoadLinkedTreeWorkMaterials()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
            }
            gridControl10.MainView.BeginUpdate();
            this.RefreshGridViewState10.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_Only.Clear();
            }
            else
            {
                try
                {
                    sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_OnlyTableAdapter.Fill(dataSet_UT_Edit.sp07140_UT_Surveyed_Tree_Work_Linked_Materials_Read_Only, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState10.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl10.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs10 != "")
            {
                strArray = i_str_AddedRecordIDs10.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl10.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ActionMaterialID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs10 = "";
            }
        }

        private void LoadLinkedTreeWorkHistorical()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strTreeIDs = "";
            string strSurveyedTreeIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strTreeIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeID"])) + ',';
                strSurveyedTreeIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyedTreeID"])) + ',';
            }
            gridControl7.MainView.BeginUpdate();
            this.RefreshGridViewState7.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07149_UT_Surveyed_Pole_Work_Historical_List.Clear();
            }
            else
            {
                try
                {
                    sp07149_UT_Surveyed_Pole_Work_Historical_ListTableAdapter.Fill(dataSet_UT_Edit.sp07149_UT_Surveyed_Pole_Work_Historical_List, (string.IsNullOrEmpty(strTreeIDs) ? "" : strTreeIDs), (string.IsNullOrEmpty(strSurveyedTreeIDs) ? "" : strSurveyedTreeIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState7.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl7.MainView.EndUpdate();
        }

        private void LoadLinkedTreeDefects()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SurveyedTreeID"])) + ',';
            }
            gridControl12.MainView.BeginUpdate();
            this.RefreshGridViewState12.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT.sp07150_UT_Surveyed_Tree_Defect_List.Clear();
            }
            else
            {
                try
                {
                    sp07150_UT_Surveyed_Tree_Defect_ListTableAdapter.Fill(dataSet_UT.sp07150_UT_Surveyed_Tree_Defect_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState12.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl12.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs12 != "")
            {
                strArray = i_str_AddedRecordIDs12.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl12.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["TreeDefectID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs12 = "";
            }
        }

        private void LoadLinkedTreeDefectPictures()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl12.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["TreeDefectID"])) + ',';
            }
            gridControl13.MainView.BeginUpdate();
            this.RefreshGridViewState13.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07154_UT_Surveyed_Tree_Defect_Pictures_List.Clear();
            }
            else
            {
                try
                {
                    sp07154_UT_Surveyed_Tree_Defect_Pictures_ListTableAdapter.Fill(dataSet_UT_Edit.sp07154_UT_Surveyed_Tree_Defect_Pictures_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), strDefaultPath);
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState13.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl13.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs8 != "")
            {
                strArray = i_str_AddedRecordIDs13.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl13.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SurveyPictureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs13 = "";
            }
        }

        private void LoadLinkedWorkPermissions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ActionID"])) + ',';
            }
            gridControl27.MainView.BeginUpdate();
            this.RefreshGridViewState27.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_WorkOrder.sp07401_UT_PD_Linked_To_Actions.Clear();
            }
            else
            {
                try
                {
                    sp07401_UT_PD_Linked_To_ActionsTableAdapter.Fill(dataSet_UT_WorkOrder.sp07401_UT_PD_Linked_To_Actions, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception ex)
                {
                }
                this.RefreshGridViewState27.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl27.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs27 != "")
            {
                strArray = i_str_AddedRecordIDs27.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl27.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PermissionDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs27 = "";
            }
        }


        #region GridView3

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView3_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();

            LoadLinkedTreeSpecies();
            GridView view = (GridView)gridControl4.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreePictures();
            view = (GridView)gridControl5.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreeWork();
            view = (GridView)gridControl6.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreeWorkHistorical();
            view = (GridView)gridControl7.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreeDefects();
            view = (GridView)gridControl12.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl3_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 3;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadTrees();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView4

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 4;
            SetMenuStatus();
        }

        private void gridView4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView4_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl4_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 4;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeSpecies();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView5

        private void gridView5_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 5;
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView5_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 5;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreePictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit3_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch(Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
        
        #endregion


        #region GridView6

        private void gridView6_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 6;
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView6_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            LoadLinkedTreeWorkPictures();
            GridView view = (GridView)gridControl8.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreeWorkEquiment();
            view = (GridView)gridControl9.MainView;
            view.ExpandAllGroups();

            LoadLinkedTreeWorkMaterials();
            view = (GridView)gridControl10.MainView;
            view.ExpandAllGroups();

            LoadLinkedWorkPermissions();
            view = (GridView)gridControl27.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl6_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 6;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    if ("blockadd".Equals(e.Button.Tag))
                    {
                        Block_Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeWork();
                    }
                    else if ("copy".Equals(e.Button.Tag))
                    {
                        frm_UT_Jobs_In_Memory_Copy frm_child = new frm_UT_Jobs_In_Memory_Copy();
                        frm_child.GlobalSettings = this.GlobalSettings;
                        frm_child._PassedInSurveyedPoleIDs = intPassedInSurveyPoleID.ToString() + ",";
                        if (frm_child.ShowDialog() == DialogResult.OK) SetMenuStatus();
                    }
                    else if ("paste".Equals(e.Button.Tag))
                    {
                        frm_UT_Jobs_In_Memory_Paste frm_child = new frm_UT_Jobs_In_Memory_Paste();
                        frm_child.GlobalSettings = this.GlobalSettings;
                        frm_child._PassedInSurveyedPoleIDs = intPassedInSurveyPoleID.ToString() + ",";
                        if (frm_child.ShowDialog() == DialogResult.OK)
                        {
                            LoadLinkedTreeWork();
                            GridView view = (GridView)gridControl6.MainView;
                            view.ExpandAllGroups();
                            SetMenuStatus();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView12

        private void gridView12_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView12_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 12;
            SetMenuStatus();
        }

        private void gridView12_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView12_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            LoadLinkedTreeDefectPictures();
            GridView view = (GridView)gridControl13.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl12_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 12;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeDefects();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView13

        private void gridView13_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 13;
            SetMenuStatus();
        }

        private void gridView13_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView13_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl13_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 13;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeDefectPictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit5_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView8

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 8;
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView8_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 8;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeWorkPictures();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit4_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe picture may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #region GridView9

        private void gridView9_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 9;
            SetMenuStatus();
        }

        private void gridView9_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView9_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl9_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 9;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeWorkEquiment();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView10

        private void gridView10_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 10;
            SetMenuStatus();
        }

        private void gridView10_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView10_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl10_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 10;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeWorkMaterials();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView7

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 7;
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView7_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 7;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedTreeWorkHistorical();
                    }
                    break;
                 default:
                    break;
            }
        }

        #endregion


        #region GridView27

        private void gridView27_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView27_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 27;
            SetMenuStatus();
        }

        private void gridView27_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView27_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridView27_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SignatureFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PDFFile").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView27_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "SignatureFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SignatureFile").ToString())) e.Cancel = true;
                    break;
                case "PDFFile":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("PDFFile").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl27_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 27;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedWorkPermissions();
                    }
                    else if ("add to existing".Equals(e.Button.Tag))
                    {
                        Add_Work_To_Existing_Permission_Document();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit7_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            if (view.FocusedColumn.Name == "colSignatureFile2")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "SignatureFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Signature Linked - unable to proceed.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strSignaturePath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    System.Diagnostics.Process.Start(strFilePath);
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Signature: " + strFile + ".\n\nThe Signature may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Signature", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (view.FocusedColumn.Name == "colPDFFile")
            {
                string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PDFFile").ToString();
                if (string.IsNullOrEmpty(strFile))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("No Permission Document Linked - unable to proceed.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                try
                {
                    string strFilePath = strPermissionDocumentPath;
                    if (!strFilePath.EndsWith("\\")) strFilePath += "\\";
                    strFilePath += strFile;
                    //System.Diagnostics.Process.Start(strFilePath);

                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                    fChildForm.strPDFFile = strFilePath;
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.Show();
                }
                catch
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view Permission Document: " + strFile + ".\n\nThe Permission Document may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        #endregion


        private void Add_Work_To_Existing_Permission_Document()
        {
            GridView view = (GridView)gridControl6.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;

            string strLayout = "";
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one Work record from the Tree Work List to link to an Existing Permission Document before proceeding.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int LastClientID = 0;
            int CurrentClientID = 0;
            string ActionIDs = "";
            string strSelectedMapIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                CurrentClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                if (LastClientID == 0)
                {
                    LastClientID = CurrentClientID;
                    strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                }
                else if (CurrentClientID != LastClientID)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You have selected Work Records from more than one client!\n\nSelect jobs from just one client before proceeding.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                ActionIDs += view.GetRowCellValue(intRowHandle, "ActionID").ToString() + ",";
                strSelectedMapIDs += view.GetRowCellValue(intRowHandle, "MapID").ToString() + ",";
            }
            frm_UT_Select_Permission_Document fChildForm = new frm_UT_Select_Permission_Document();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._PassedInClientID = LastClientID;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;

            int PermissionDocumentID = fChildForm.intSelectedID;
            DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter AddWorkToPermissionDocument = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
            AddWorkToPermissionDocument.ChangeConnectionString(strConnectionString);
            try
            {
                Convert.ToInt32(AddWorkToPermissionDocument.sp07243_UT_Add_Work_To_Permission_Document(PermissionDocumentID, ActionIDs));
            }
            catch (Exception Ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to add work to existing permission document - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Add Work to Existing Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (PermissionDocumentID <= 0) return;
            
            // Check if mapping open - if yes, update trees so thematic styling is adjusted if necessary //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_UT_Mapping")  // Mapping Form //
                    {
                        var fParentForm = (frm_UT_Mapping)frmChild;
                        fParentForm.RefreshMapObjects(strSelectedMapIDs, 1);
                    }
                }
            }

            // Permission Document now created or existing one added to, so open it //
            UpdateRefreshStatus = 27;
            i_str_AddedRecordIDs27 = PermissionDocumentID.ToString() + ";";

            this.RefreshGridViewState27.SaveViewInfo();  // Store Grid View State //
            if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
            {
                frm_UT_Permission_Doc_Edit_WPD fChildForm2 = new frm_UT_Permission_Doc_Edit_WPD();
                fChildForm2.MdiParent = this.MdiParent;
                fChildForm2.GlobalSettings = this.GlobalSettings;
                fChildForm2.strRecordIDs = PermissionDocumentID + ",";
                fChildForm2.strFormMode = "edit";
                fChildForm2.strCaller = this.Name;
                fChildForm2.intRecordCount = 1;
                fChildForm2.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm2.splashScreenManager = splashScreenManager1;
                fChildForm2.splashScreenManager.ShowWaitForm();
                fChildForm2.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm2, new object[] { null });
            }
            else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
            {
                frm_UT_Permission_Doc_Edit_UKPN fChildForm2 = new frm_UT_Permission_Doc_Edit_UKPN();
                fChildForm2.MdiParent = this.MdiParent;
                fChildForm2.GlobalSettings = this.GlobalSettings;
                fChildForm2.strRecordIDs = PermissionDocumentID + ",";
                fChildForm2.strFormMode = "edit";
                fChildForm2.strCaller = this.Name;
                fChildForm2.intRecordCount = 1;
                fChildForm2.FormPermissions = this.FormPermissions;
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                fChildForm2.splashScreenManager = splashScreenManager1;
                fChildForm2.splashScreenManager.ShowWaitForm();
                fChildForm2.Show();

                System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                if (method != null) method.Invoke(fChildForm2, new object[] { null });
            }           
        }

        #endregion


        #region Risk Assessment Page

        private void LoadLinkedRiskAssessments()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl22.MainView.BeginUpdate();
            this.RefreshGridViewState22.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07254_UT_Surveyed_Pole_Risk_Assessment_ListTableAdapter.Fill(dataSet_UT_Edit.sp07254_UT_Surveyed_Pole_Risk_Assessment_List, intPassedInSurveyPoleID);
                this.RefreshGridViewState22.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl22.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs22 != "")
            {
                strArray = i_str_AddedRecordIDs22.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl22.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RiskAssessmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs22 = "";
            }
        }

        private void LoadLinkedRiskAssessmentQuestions()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControl22.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RiskAssessmentID"])) + ',';
            }
            gridControl23.MainView.BeginUpdate();
            this.RefreshGridViewState23.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_UT_Edit.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_List.Clear();
            }
            else
            {
                try
                {
                    sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_ListTableAdapter.Fill(dataSet_UT_Edit.sp07255_UT_Surveyed_Pole_Risk_Assessment_Questions_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception)
                {
                }
                this.RefreshGridViewState23.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControl23.MainView.EndUpdate();
        }


        #region GridView22

        private void gridView22_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView22_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 22;
            SetMenuStatus();
        }

        private void gridView22_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView22_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            LoadLinkedRiskAssessmentQuestions();
            GridView view = (GridView)gridControl23.MainView;
            view.ExpandAllGroups();
        }

        private void gridControl22_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 22;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    {
                        if ("add".Equals(e.Button.Tag))
                        {
                            Add_Record();
                        }
                        else if ("edit".Equals(e.Button.Tag))
                        {
                            Edit_Record();
                        }
                        else if ("delete".Equals(e.Button.Tag))
                        {
                            Delete_Record();
                        }
                        else if ("view".Equals(e.Button.Tag))
                        {
                            View_Record();
                        }
                        else if ("refresh".Equals(e.Button.Tag))
                        {
                            LoadLinkedRiskAssessments();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView23

        private void gridView23_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 23;
            SetMenuStatus();
        }

        private void gridView23_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                // Set any of the Dataset Menu items as appropriate if required //
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView23_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl23_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 22;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadLinkedRiskAssessmentQuestions();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView23_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "Answer":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText1":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText2":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                case "AnswerText3":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RiskAssessmentTypeID")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView23_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "Answer":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) == 0) e.Cancel = true;
                    break;
                case "AnswerText1":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                case "AnswerText2":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                case "AnswerText3":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RiskAssessmentTypeID")) != 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }


        #endregion

        #endregion


        #region Finish Page

        private void btnFinish_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                currentRow["SurveyStatus"] = "Survey Completed";
                currentRow["SurveyStatusID"] = 3;
                currentRow["SurveyDate"] = DateTime.Now;
            }
            SaveChanges(false);
            if (string.IsNullOrEmpty(SaveChanges(false))) this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (strFormMode != "view")
            {
                if (string.IsNullOrEmpty(SaveChanges(false))) this.Close();
            }
            else
            {
                this.Close();
            }
        }

        private void btnOnHold_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                currentRow["SurveyStatus"] = "Survey On-Hold";
                currentRow["SurveyStatusID"] = 2;
            }
            if (string.IsNullOrEmpty(SaveChanges(false))) this.Close();
        }

        private void Set_Finish_Survey_Warning()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow != null)
            {
                string strErrorMessage = "";
                int intSpanClear = Convert.ToInt32(currentRow["IsSpanClear"]);
                int intInfestationRate = Convert.ToInt32(currentRow["InfestationRate"]);
                int intClearanceDistance = Convert.ToInt32(currentRow["ClearanceDistance"]);
                int intClearanceDistance2 = Convert.ToInt32(currentRow["ClearanceDistanceUnderID"]);
                int G55CategoryID = Convert.ToInt32(currentRow["G55CategoryID"]);

                int intIsShutdownRequired = Convert.ToInt32(currentRow["IsShutdownRequired"]);
                int intDeferred = Convert.ToInt32(currentRow["Deferred"]);

                int intActionCount = 0;
                int intSurveyedPoleID = (string.IsNullOrEmpty(currentRow["SurveyedPoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyedPoleID"]));

                int intClientID = Convert.ToInt32(currentRow["ClientID"]);
                int intClearanceDistanceAboveID = Convert.ToInt32(currentRow["ClearanceDistanceAboveID"]);

                DataSet_UTTableAdapters.QueriesTableAdapter GetCount = new DataSet_UTTableAdapters.QueriesTableAdapter();
                GetCount.ChangeConnectionString(strConnectionString);
                try
                {
                    intActionCount = Convert.ToInt32(GetCount.sp07479_UT_Get_Action_Count_For_Surveyed_Pole(intSurveyedPoleID.ToString() + ",")); 
                }
                catch (Exception)
                {
                }


                if (intSpanClear <= 0 && intInfestationRate <= 0)
                {
                    strErrorMessage = "The <b>Is Span Clear</b> or <b>Infestation Rate</b> must have a value set.";
                }
                if (intClearanceDistance == 0 || intClearanceDistance2 == 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "Both <b>Clearance Distances</b> must have a value set.";
                }
                if (G55CategoryID == 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "The <b>G55/2 Category</b> must have a value set.";
                }
                if (intIsShutdownRequired != 0 && intSpanClear != 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "The <b>Shutdown Required</b> and <b>Span Clear</b> cannot both be ticked.";
                }
                if (intIsShutdownRequired != 0 && intDeferred != 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "The <b>Shutdown Required</b> and <b>Deferred</b> cannot both be ticked.";
                }
                if (intInfestationRate != 0 && intDeferred == 0 && intActionCount <= 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "If <b>Infestation Rate</b> has a value but <b>Deferred</b> is unticked, at least one <b>Action</b> must be created.";
                }
                if (intClientID == ClientUKPN && intClearanceDistanceAboveID <= 0)
                {
                    if (!string.IsNullOrEmpty(strErrorMessage)) strErrorMessage += Environment.NewLine;
                    strErrorMessage += "The <b>UKPN</b> client requires a <b>Clearance Distance Above</b> to be recorded.";
                }

                if (string.IsNullOrEmpty(strErrorMessage))
                {
                    groupControlMissingInfo.Visible = false;
                    labelFinishSurveyWarningInfo.Visible = false;
                    labelFinishSurveyWarning.Visible = false;
                    btnFinish.Enabled = true;
                    btnFixFinishSurveyIssue.Enabled = false;
                    btnFixFinishSurveyIssue.Visible = false;
                }
                else
                {
                    labelFinishSurveyWarning.Text = strErrorMessage;
                    groupControlMissingInfo.Visible = true;
                    labelFinishSurveyWarningInfo.Visible = true;
                    labelFinishSurveyWarning.Visible = true;
                    btnFinish.Enabled = false;
                    btnFixFinishSurveyIssue.Enabled = true;
                    btnFixFinishSurveyIssue.Visible = true;
                }
            }

        }

        private void btnFixFinishSurveyIssue_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = xtraTabPageGeneralInfo;
            if (Convert.ToInt32(InfestationRateGridLookupEdit.EditValue) == 0) InfestationRateGridLookupEdit.Focus();
            else if (Convert.ToInt32(ClearanceDistanceGridLookUpEdit.EditValue) == 0) ClearanceDistanceGridLookUpEdit.Focus();
            else if (Convert.ToInt32(G55CategoryIDGridLookUpEdit.EditValue) == 0) G55CategoryIDGridLookUpEdit.Focus();
            else if (Convert.ToInt32(IsShutdownRequiredCheckEdit.EditValue) != 0 && Convert.ToInt32(IsSpanClearCheckEdit.EditValue) != 0) IsShutdownRequiredCheckEdit.Focus();
            else if (Convert.ToInt32(IsShutdownRequiredCheckEdit.EditValue) != 0 && Convert.ToInt32(DeferredCheckEdit.EditValue) != 0) DeferredCheckEdit.Focus();
            else
            {
                DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                if (currentRow != null)
                {
                    int intClientID = Convert.ToInt32(currentRow["ClientID"]);
                    int intClearanceDistanceAboveID = Convert.ToInt32(currentRow["ClearanceDistanceAboveID"]);
                    if (intClientID == ClientUKPN && intClearanceDistanceAboveID <= 0)
                    {
                        ClearanceDistanceAboveIDFGridLookUpEdit.Focus();
                    }
                    else InfestationRateGridLookupEdit.Focus();  // Last error check must be triggered... if (intInfestationRate != 0 && intDeferred == 0 && intActionCount <= 0) //
                }
            }
        }


        #endregion


        #region Linked Records Panel

        private void LoadAccessIssues()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["PoleID"].ToString() +",";

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl16.MainView.BeginUpdate();
            this.RefreshGridViewState16.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07070_UT_Pole_Manager_Linked_Access_IssuesTableAdapter.Fill(dataSet_UT.sp07070_UT_Pole_Manager_Linked_Access_Issues, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState16.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl16.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs16 != "")
            {
                strArray = i_str_AddedRecordIDs16.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl16.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleAccessIssueID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs16 = "";
            }
        }

        private void LoadEnviromentalIssues()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["PoleID"].ToString() + ",";

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl17.MainView.BeginUpdate();
            this.RefreshGridViewState17.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07075_UT_Pole_Manager_Linked_Environmental_IssuesTableAdapter.Fill(dataSet_UT.sp07075_UT_Pole_Manager_Linked_Environmental_Issues, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState17.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl17.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs17 != "")
            {
                strArray = i_str_AddedRecordIDs17.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl17.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleEnvironmentalIssueID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs17 = "";
            }
        }

        private void LoadSiteHazards()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["PoleID"].ToString() + ",";

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl18.MainView.BeginUpdate();
            this.RefreshGridViewState18.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07080_UT_Pole_Manager_Linked_Site_HazardsTableAdapter.Fill(dataSet_UT.sp07080_UT_Pole_Manager_Linked_Site_Hazards, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState18.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl18.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs18 != "")
            {
                strArray = i_str_AddedRecordIDs18.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl18.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleSiteHazardID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs18 = "";
            }
        }

        private void LoadElectricalHazards()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["PoleID"].ToString() + ",";

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = null;
            gridControl19.MainView.BeginUpdate();
            this.RefreshGridViewState19.SaveViewInfo();  // Store expanded groups and selected rows //
            try
            {
                sp07085_UT_Pole_Manager_Linked_Electrical_HazardsTableAdapter.Fill(dataSet_UT.sp07085_UT_Pole_Manager_Linked_Electrical_Hazards, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewState19.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception)
            {
            }
            gridControl19.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs19 != "")
            {
                strArray = i_str_AddedRecordIDs19.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl19.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PoleElectricalHazardID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs19 = "";
            }
        }

        private void LoadLinkedDocuments()
        {
            DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
            if (currentRow == null) return;
            string strSelectedIDs = currentRow["PoleID"].ToString() + ",";

            gridControl20.MainView.BeginUpdate();
            GridView view = null;
            this.RefreshGridViewState20.SaveViewInfo();  // Store expanded groups and selected rows //
            sp00220_Linked_Documents_ListTableAdapter.Fill(dataSet_AT.sp00220_Linked_Documents_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 51, strLinkedDocumentDefaultPath);
            this.RefreshGridViewState20.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl20.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs20 != "")
            {
                strArray = i_str_AddedRecordIDs20.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControl20.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LinkedDocumentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs20 = "";
            }
        }


        #region GridView16 Linked Access Issues

        private void gridView16_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView16_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 16;
            SetMenuStatus();
        }

        private void gridView16_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView16_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;

            SetMenuStatus();
        }

        private void gridControl16_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 16; 
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadAccessIssues();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView17 Linked Environmental Issues

        private void gridView17_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView17_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 17;
            SetMenuStatus();
        }

        private void gridView17_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView17_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;

            SetMenuStatus();
        }

        private void gridControl17_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 17;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadEnviromentalIssues();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView18 Linked Site Hazards

        private void gridView18_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView18_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 18;
            SetMenuStatus();
        }

        private void gridView18_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView18_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;

            SetMenuStatus();
        }

        private void gridControl18_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 18;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadSiteHazards();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView19 Linked Electrical Hazards

        private void gridView19_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView19_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 19;
            SetMenuStatus();
        }

        private void gridView19_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView19_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;

            SetMenuStatus();
        }

        private void gridControl19_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 19;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadElectricalHazards();
                    }
                   break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView20 Linked Documents

        private void gridView20_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridView20_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 20;
            SetMenuStatus();
        }

        private void gridView20_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView20_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
        }

        private void gridControl20_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            i_int_FocusedGrid = 20;
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("refresh".Equals(e.Button.Tag))
                    {
                        LoadEnviromentalIssues();
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit6_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "DocumentPath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Document Linked - unable to proceed.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        #endregion


        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Trees - activate mapping if already open or start mapping if not already open //
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        int intSurveyID = (string.IsNullOrEmpty(currentRow["SurveyID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyID"]));
                        int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
                        int intPoleID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        string strPoleNumber = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());

                        try
                        {
                            frmMain2 frmMDI = (frmMain2)this.MdiParent;
                            foreach (frmBase frmChild in frmMDI.MdiChildren)
                            {
                                if (frmChild.FormID == 10003)
                                {
                                    frmChild.Activate();
                                    frm_UT_Mapping frmMapping = (frm_UT_Mapping)frmChild;
                                    frmMapping._CurrentPoleID = intPoleID;
                                    frmMapping.SetModeToTreePlotting(strPoleNumber);
                                    return;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        // Get the Workspace ID from the Region on the Survey //
                        int intWorkSpaceID = 0;
                        int intRegionID = 0;
                        string strWorkspaceName = "";
                        string strReturn = "";
                        DataSet_UT_EditTableAdapters.QueriesTableAdapter getWorkspace = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                        getWorkspace.ChangeConnectionString(strConnectionString);
                        try
                        {
                            strReturn = getWorkspace.sp07124_UT_Get_Workspace_From_Region(intSurveyID).ToString();
                            char[] delimiters = new char[] { '|' };
                            string[] strArray = null;
                            strArray = strReturn.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strArray.Length == 3)
                            {
                                intWorkSpaceID = Convert.ToInt32(strArray[0]);
                                strWorkspaceName = strArray[1].ToString();
                                intRegionID = Convert.ToInt32(strArray[2]);
                            }
                        }
                        catch (Exception)
                        {
                            return;
                        }
                        if (intWorkSpaceID == 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Unable to plot tree via Mapping - the current Survey is linked to a region which has no Mapping Workspace ID.\n\nUpdate the Linked Region with a Mapping Workspace [via the Region Manager] then try again.", "Open Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        string strSurveyDescription = "";

                        // Get Surveyor Name from grid lookup Edit //
                        string strSurveyor = (string.IsNullOrEmpty(currentRow["Surveyor"].ToString()) ? "Unknown Surveyor" : currentRow["Surveyor"].ToString());

                        strSurveyDescription = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientName"])) ? "Unknown Client" : Convert.ToString(currentRow["ClientName"])) + " \\ " +
                            (String.IsNullOrEmpty(Convert.ToString(currentRow["SurveySurveyDate"])) ? "Unknown Date" : Convert.ToDateTime(currentRow["SurveySurveyDate"]).ToString("dd/MM/yyyy")) + " \\ " +
                                strSurveyor;

                        // Open Map and show Survey Panel //
                        int intWorkspaceOwner = 0;
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        intWorkspaceOwner = MapFunctions.Get_Mapping_Workspace_Owner(this.GlobalSettings, intWorkSpaceID);

                        frm_UT_Mapping frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();

                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance._SelectedSurveyID = intSurveyID;
                        frmInstance._SelectedSurveyClientID = intClientID;
                        frmInstance._SelectedSurveyDescription = strSurveyDescription;
                        frmInstance._PassedInClientIDs = (String.IsNullOrEmpty(Convert.ToString(currentRow["ClientID"])) ? "" : Convert.ToString(currentRow["ClientID"]) + ",");
                        frmInstance._PassedInRegionIDs = (String.IsNullOrEmpty(intRegionID.ToString()) ? "" : intRegionID.ToString() + ",");
                        frmInstance._SurveyMode = "PlotTrees";
                        frmInstance._CurrentPoleID = intPoleID;

                        frmInstance.Show();
                        // Invoke Post Open event on Base form //
                        method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null)
                        {
                            method.Invoke(frmInstance, new object[] { null });
                        }
                        frmInstance.SetPlottedTreeDescription(strPoleNumber);
                    }
                    break;
                case 5:  // Tree Picture //
                    {
                        view = (GridView)gridControl3.MainView;
                        int[] intSelectedTreeHandles = view.GetSelectedRows();
                        int intSelectedTrees = intSelectedTreeHandles.Length;
                        if (intSelectedTrees != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one Tree to add pictures to before proceeding.", "Add Tree Picture Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intTreeID = Convert.ToInt32(view.GetRowCellValue(intSelectedTreeHandles[0], "TreeID"));
                        frm_UT_Picture_Capture frmInstance = new frm_UT_Picture_Capture();
                        frmInstance.strCaller = this.Name;
                        frmInstance.strConnectionString = strConnectionString;
                        frmInstance.strFormMode = "add";
                        frmInstance.intAddToRecordID = intTreeID;
                        frmInstance.intAddToRecordTypeID = 1;  // 1 = Tree //
                        frmInstance.intCreatedByStaffID = GlobalSettings.UserID;
                        frmInstance.ShowDialog();
                        if (frmInstance._AddedPolePictureID != 0)  // At least 1 picture added so refresh picture grid //
                        {
                            int intReturnedValue = frmInstance._AddedPolePictureID;
                            i_str_AddedRecordIDs5 = intReturnedValue.ToString() + ";";
                            UpdateRefreshStatus = 5;
                            this.RefreshGridViewState5.SaveViewInfo();
                            LoadLinkedTreePictures();
                        }
                    }
                    break;
                case 6:     // Tree Work //
                    {
                        GridView ParentView = (GridView)gridControl3.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one tree to add work to before proceeding.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        string strPoleNumber = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        DateTime dtSurveyDate = DateTime.MinValue;
                        try
                        {
                            dtSurveyDate = (string.IsNullOrEmpty(currentRow["SurveyDate"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(currentRow["SurveyDate"]));
                        }
                        catch (Exception) { }

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = "";
                        frmInstance.strFormMode = "add";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        frmInstance.intLinkedToSurveyedTreeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SurveyedTreeID"));
                        frmInstance.strLinkedToSurveyedTree = strPoleNumber + " \\ " + Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ReferenceNumber"));
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 12:     // Tree Defect //
                    {
                        GridView ParentView = (GridView)gridControl3.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one tree to add defect to before proceeding.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        string strPoleNumber = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());

                        view = (GridView)gridControl2.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Defect_Edit frmInstance = new frm_UT_Surveyed_Tree_Defect_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = "";
                        frmInstance.strFormMode = "add";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        frmInstance.intLinkedToSurveyedTreeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "SurveyedTreeID"));
                        frmInstance.strLinkedToSurveyedTree = strPoleNumber + " \\ " + Convert.ToString(view.GetRowCellValue(intRowHandles[0], "ReferenceNumber"));
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;

                case 20:     // Linked Documents //
                    {
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        view = (GridView)gridControl20.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState20.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = "";
                        fChildForm2.strFormMode = "add";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = 0;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //

                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        fChildForm2.intLinkedToRecordID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        fChildForm2.strLinkedToRecordDesc = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;

                case 16:     // Linked Access Issues //
                    {
                        view = (GridView)gridControl16.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState16.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        fChildForm.intLinkedToRecordID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        fChildForm.strLinkedToRecordDesc = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));        

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 17:     // Linked Environmental Issues //
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();
                         this.RefreshGridViewState17.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        fChildForm.intLinkedToRecordID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        fChildForm.strLinkedToRecordDesc = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));        

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 18:     // Linked Site Hazards //
                    {
                        view = (GridView)gridControl18.MainView;
                        view.PostEditor();
                         this.RefreshGridViewState18.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        fChildForm.intLinkedToRecordID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        fChildForm.strLinkedToRecordDesc = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));        

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 19:     // Linked Electrical Hazards //
                    {
                        view = (GridView)gridControl19.MainView;
                        view.PostEditor();
                        this.RefreshGridViewState19.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        fChildForm.intLinkedToRecordID = (string.IsNullOrEmpty(currentRow["PoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PoleID"]));
                        fChildForm.strLinkedToRecordDesc = (string.IsNullOrEmpty(currentRow["PoleNumber"].ToString()) ? "" : currentRow["PoleNumber"].ToString());
                        fChildForm.intLinkedToRecordID2 = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));        

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 22:     // Risk Assessment //
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        int intSurveyedPoleID = (string.IsNullOrEmpty(currentRow["SurveyedPoleID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SurveyedPoleID"]));
                        int intNewRiskAssessmentID = 0;

                        // Get the Risk Assessment Type //
                        int intType = 0;
                        frm_UT_Risk_Assessment_Add_Select_Type fChildForm1 = new frm_UT_Risk_Assessment_Add_Select_Type();
                        switch (fChildForm1.ShowDialog())
                        {
                             	case DialogResult.OK:
                            		intType = 0;
                            		break;
                            	case DialogResult.Yes:
                            		intType = 1;
                            		break;
                           	    case DialogResult.Cancel:
                            		return;
                        }
          
                        // Add New Risk Assessment (Site Specific) //
                        try
                        {
                            DataSet_UT_EditTableAdapters.QueriesTableAdapter AddRecord = new DataSet_UT_EditTableAdapters.QueriesTableAdapter();
                            AddRecord.ChangeConnectionString(strConnectionString);
                            intNewRiskAssessmentID = Convert.ToInt32(AddRecord.sp07260_UT_Risk_Assessment_Create(intSurveyedPoleID, 0, GlobalSettings.UserID, intType));
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to add the Risk Assessment [" + ex.Message + "]. Try again. If the problem persists contact technical support.", "Add Risk Assessment", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Inform this screen to reload the list once the screen re-activates after editing the record //
                        UpdateRefreshStatus = 22;
                        i_str_AddedRecordIDs22 = intNewRiskAssessmentID.ToString() + ";";

                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit fChildForm = new frm_UT_Risk_Assessment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = intNewRiskAssessmentID.ToString() + ",";
                        fChildForm.strFormMode = "edit";  // This should be Edit since we already added the record, so now we edit it //
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 26:  // Traffic Management Requirements //
                    {
                        DataRowView currentRow = (DataRowView)sp07114UTSurveyedPoleItemBindingSource.Current;
                        if (currentRow == null) return;
                        try
                        {
                            int intSurveyedPoleID = 0;
                            if (!string.IsNullOrEmpty(currentRow["SurveyedPoleID"].ToString())) intSurveyedPoleID = Convert.ToInt32(currentRow["SurveyedPoleID"]);
                            DataRow drNewRow;
                            drNewRow = this.dataSet_UT_Edit.sp07367_UT_Traffic_Management_For_Surveyed_Pole.NewRow();
                            drNewRow["SurveyedPoleID"] = intSurveyedPoleID;
                            drNewRow["TrafficManagementTypeID"] = 0;
                            drNewRow["AmountRequired"] = (decimal)1.00;

                            this.dataSet_UT_Edit.sp07367_UT_Traffic_Management_For_Surveyed_Pole.Rows.Add(drNewRow);
                            SetChangesPendingLabel();
                            gridView26.FocusedRowHandle = gridView26.DataRowCount - 1;
                            Check_Traffic_Management();
                        }
                        catch (Exception Ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to add a new Traffic Management Requirement - Error:" + Ex.Message + ".\n\nPlease try again. If the problem persists, contact Technical Support.", "Add Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
                case 27:  // Permission Document //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        string strLayout = "";
                        int ClientID = 0;
                        if (intRowHandles.Length <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record from the Tree Work list to add to a new Permission Document before proceeding.", "Permission Work", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += view.GetRowCellValue(intRowHandle, "ActionID").ToString() + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                            ClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                        }

                        // Create New Permission Document //
                        DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter CreatePD = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                        CreatePD.ChangeConnectionString(strConnectionString);
                        int PermissionDocumentID = 0;
                        string strReferenceNumber = GlobalSettings.UserForename.Substring(0, 1).ToUpper() + GlobalSettings.UserSurname.Substring(0, 1).ToUpper() + DateTime.Now.ToString("yyyyMMddHHmmss");
                        try
                        {
                            PermissionDocumentID = Convert.ToInt32(CreatePD.sp07391_UT_PD_Insert(DateTime.Now, GlobalSettings.UserID, strReferenceNumber, strRecordIDs, ClientID));
                            if (PermissionDocumentID <= 0) return;
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Permission Document - [" + ex.Message + "].", "Create Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (PermissionDocumentID <= 0) return;
                        UpdateRefreshStatus = 1;
                        i_str_AddedRecordIDs1 = PermissionDocumentID.ToString() + ";";

                        this.RefreshGridViewState27.SaveViewInfo();  // Store Grid View State //

                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = PermissionDocumentID + ",";
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = PermissionDocumentID + ",";
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = 1;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                    }
                    break;
            }
        }

        private void Block_Add_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 6:     // Tree Work //
                    {
                        view = (GridView)gridControl3.MainView;  // Parent Tree Grid //
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at more than one Tree record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SurveyedTreeID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit fChildForm = new frm_UT_Surveyed_Tree_Work_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Trees //
                    {
                        //if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 6:     // Tree Work //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "blockedit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 12:     // Tree Defect //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Tree Defect(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeDefectID")) + ',';
                        }
                        this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Defect_Edit frmInstance = new frm_UT_Surveyed_Tree_Defect_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "blockedit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;

                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (strFormMode == "view") return;
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Survey Pictures //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Survey Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.ShowDialog();

                        UpdateRefreshStatus = 1;
                        this.RefreshGridViewState1.SaveViewInfo();
                        LoadPolePictures();
                    }
                    break;
                case 3:     // Trees //
                    {
                        //if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Tree Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 5:  // Tree Picture //
                    {
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Tree Picture Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intPictureID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "SurveyPictureID"));
                        int intAddToRecordID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordID"));
                        int intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "LinkedToRecordTypeID"));
                        frm_UT_Picture_Capture fChildForm = new frm_UT_Picture_Capture();
                        fChildForm.strCaller = this.Name;
                        fChildForm.strConnectionString = strConnectionString;
                        fChildForm.strFormMode = "edit";
                        fChildForm._PassedInEditImageID = intPictureID;
                        fChildForm.intAddToRecordID = intAddToRecordID;
                        fChildForm.intAddToRecordTypeID = intLinkedToRecordTypeID;
                        fChildForm.intCreatedByStaffID = GlobalSettings.UserID;
                        fChildForm.ShowDialog();
 
                        UpdateRefreshStatus = 5;
                        this.RefreshGridViewState5.SaveViewInfo();
                        LoadLinkedTreePictures();
                    }
                    break;
                 case 6:     // Tree Work //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 12:     // Tree Defect //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Tree Defect Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeDefectID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Defect_Edit frmInstance = new frm_UT_Surveyed_Tree_Defect_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 20:     // Linked Documents //
                    {
                        view = (GridView)gridControl20.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LinkedDocumentID")) + ',';
                        }
                        this.RefreshGridViewState20.SaveViewInfo();  // Store Grid View State //
                        frm_AT_Linked_Document_Edit fChildForm2 = new frm_AT_Linked_Document_Edit();
                        fChildForm2.MdiParent = this.MdiParent;
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.strRecordIDs = strRecordIDs;
                        fChildForm2.strFormMode = "edit";
                        fChildForm2.strCaller = this.Name;
                        fChildForm2.intRecordCount = intCount;
                        fChildForm2.FormPermissions = this.FormPermissions;
                        fChildForm2.fProgress = fProgress;
                        fChildForm2.intRecordTypeID = 51;  // Pole //
                        fChildForm2.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm2, new object[] { null });
                    }
                    break;
                 case 16:     // Linked Access Issues //
                    {
                        view = (GridView)gridControl16.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleAccessIssueID")) + ',';
                        }
                        this.RefreshGridViewState16.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Access_Issue_Edit fChildForm = new frm_UT_Pole_Access_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 17:     // Linked Environmental Issues //
                    {
                        view = (GridView)gridControl17.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleEnvironmentalIssueID")) + ',';
                        }
                        this.RefreshGridViewState17.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Environmental_Issue_Edit fChildForm = new frm_UT_Pole_Environmental_Issue_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 18:     // Linked Site Hazards //
                    {
                        view = (GridView)gridControl18.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleSiteHazardID")) + ',';
                        }
                         this.RefreshGridViewState18.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Site_Hazard_Edit fChildForm = new frm_UT_Pole_Site_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 19:     // Linked Electrical Hazards //
                    {
                        view = (GridView)gridControl19.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PoleElectricalHazardID")) + ',';
                        }
                        this.RefreshGridViewState19.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Pole_Electrical_Hazard_Edit fChildForm = new frm_UT_Pole_Electrical_Hazard_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case 22:     // Risk Assessment //
                    {
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Risk Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "edit";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                case 27:     // Work Permission //
                    {
                        view = (GridView)gridControl27.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to edit before proceeding.", "Edit Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strLayout = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        this.RefreshGridViewState27.SaveViewInfo();  // Store Grid View State //
                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "edit";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }                      
                    }
                    break;


                 default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Pole Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Survey Picture" : Convert.ToString(intRowHandles.Length) + " Survey Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Survey Picture" : "these Survey Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Survey Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("survey_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadPolePictures();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 3:  // Trees //
                    {
                        //if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl3.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Trees to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree" : Convert.ToString(intRowHandles.Length) + " Trees") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree" : "these Trees") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "TreeID")) + ",";
                            }

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState3.SaveViewInfo();  // Store Grid View State //
                            this.RefreshGridViewState4.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("tree", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadTrees();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 5:  // Tree Pictures //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl5.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Picture" : Convert.ToString(intRowHandles.Length) + " Tree Pictures") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Picture" : "these Tree Pictures") + " will no longer be available for selection and any associated images will be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            string strImageName = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strImageName = view.GetRowCellValue(intRowHandle, "PicturePath").ToString();
                                try
                                {
                                    base.ImagePreviewClear();  // Make sure no image is previewed just in case the image to be deleted is being previewed as this would cause a delete violation //
                                    GC.GetTotalMemory(true);
                                    System.IO.File.Delete(strImageName);
                                }
                                catch (Exception ex)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to delete a linked picture [" + ex.Message + "]. The record has not been deleted.\n\nTry deleting again. If the problem persists contact technical support.", "Delete Survey Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    continue;
                                }
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SurveyPictureID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState5.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("survey_picture", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedTreePictures();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 6:  // Tree Work //
                    {
                        //if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();

                        // Check user created record or is a super user - if not de-select row //
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (!(Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CreatedByStaffID")) == GlobalSettings.UserID || GlobalSettings.UserType1.ToLower() == "super" || GlobalSettings.UserType2.ToLower() == "super" || GlobalSettings.UserType3.ToLower() == "super")) view.UnselectRow(intRowHandle);
                        }
                        intRowHandles = view.GetSelectedRows();

                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Tree Work Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Work Record" : Convert.ToString(intRowHandles.Length) + " Tree Work Records") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Work Record" : "these Tree Work Records") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ActionID")) + ",";
                            }
 
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("tree_work", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedTreeWork();
                            LoadLinkedWorkPermissions();  // Permissions may have been deleted if a job has been deleted //

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 12:  // Tree Defects //
                    {
                        //if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Tree Defects to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Tree Defect" : Convert.ToString(intRowHandles.Length) + " Tree Defects") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Tree Defect" : "these Tree Defects") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "TreeDefectID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState12.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("tree_defect", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedTreeDefects();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case 20:  // Linked Documents //
                    {
                        view = (GridView)gridControl20.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Linked Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Document" : Convert.ToString(intRowHandles.Length) + " Linked Documents") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Document" : "these Linked Documents") + " will no longer be available for selection but the files(s) will still exist on the computer!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LinkedDocumentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_ATTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_ATTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                             this.RefreshGridViewState20.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp00223_Linked_Document_Delete(strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedDocuments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 16:  // Linked Access Issues //
                    {
                        view = (GridView)gridControl16.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Access Issues to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Access Issue" : Convert.ToString(intRowHandles.Length) + " Pole Access Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Access Issue" : "these Pole Access Issues") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleAccessIssueID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState16.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_access_issue", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadAccessIssues();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 17:  // Linked Environmental Issues //
                    {
                        view = (GridView)gridControl17.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Environmental Issues to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Environmental Issue" : Convert.ToString(intRowHandles.Length) + " Pole Environmental Issues") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Environmental Issue" : "these Pole Environmental Issues") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleEnvironmentalIssueID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState17.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_environmental_issue", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadEnviromentalIssues();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 18:  // Linked Site Hazards //
                    {
                        view = (GridView)gridControl18.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Site Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Site Hazard" : Convert.ToString(intRowHandles.Length) + " Pole Site Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Site Hazard" : "these Pole Site Hazards") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleSiteHazardID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState18.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_site_hazard", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadSiteHazards();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 19:  // Linked Electrical Hazards //
                    {
                        view = (GridView)gridControl19.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Pole Electrical Hazards to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pole Electrical Hazard" : Convert.ToString(intRowHandles.Length) + " Pole Electrical Hazards") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pole Electrical Hazard" : "these Pole Electrical Hazards") + " will no longer be available for selection and any related records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PoleElectricalHazardID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState19.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("pole_electrical_hazard", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadElectricalHazards();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 22:  // Risk Assessment //
                    {
                        if (strFormMode == "view") return;
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Risk Assessments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Risk Assessment" : Convert.ToString(intRowHandles.Length) + " Risk Assessments") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Risk Assessment" : "these Risk Assessments") + " will no longer be available for selection and any associated records will also be deleted!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "RiskAssessmentID")) + ",";
                            }
                            DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                            try
                            {
                                RemoveRecords.sp07000_UT_Delete("risk_assessment", strRecordIDs);  // Remove the records from the DB in one go //
                            }
                            catch (Exception)
                            {
                            }
                            LoadLinkedRiskAssessments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 26:  // Traffic Management Requirement //
                    {
                        //if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl26.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Traffic Management Requirements to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Traffic Management Requirement" : Convert.ToString(intRowHandles.Length) + " Traffic Management Requirements") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Traffic Management Requirement" : "these Traffic Management Requirements") + " will no longer be available for selection!\n\nIMPORTANT NOTE: On Closing the screen you MUST save changes to remove the deleted records!";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            view.BeginUpdate();
                            for (int i = intRowHandles.Length - 1; i >= 0; i--)
                            {
                                view.DeleteRow(intRowHandles[i]);
                            }
                            view.EndUpdate();
                            Check_Traffic_Management();
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.\n\nOn closing the screen, save changes to remove the deleted records.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case 27:  // Permission Documents //
                    view = (GridView)gridControl27.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Permission Documents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Permission Document" : Convert.ToString(intRowHandles.Length) + " Permission Documents") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Permission Document" : "these Permission Documents") + " will no longer be available for selection and any linked work will be unpermissioned!";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PermissionDocumentID")) + ",";
                        }

                        // Get unique MapIds from PermissionDocumentIDs so the map can be updated after the records are deleted. //
                        string strMapIDs = "";
                        DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter GetMapIDs = new DataSet_UT_WorkOrderTableAdapters.QueriesTableAdapter();
                        GetMapIDs.ChangeConnectionString(strConnectionString);
                        try
                        {
                            strMapIDs = GetMapIDs.sp07403_UT_Get_MapIDs_From_PermissionDocumentID(strRecordIDs).ToString();
                        }
                        catch (Exception)
                        {
                            return;
                        }

                        DataSet_UTTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_UTTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState27.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp07000_UT_Delete("permission_document", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        LoadLinkedWorkPermissions();

                        // Update Map //
                        if (this.ParentForm != null)
                        {
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Mapping")  // Mapping Form //
                                {
                                    var fParentForm = (frm_UT_Mapping)frmChild;
                                    fParentForm.RefreshMapObjects(strMapIDs, 1);
                                }
                            }
                        }

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;

            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 3:     // Trees //
                    {
                        view = (GridView)gridControl3.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeID")) + ',';
                        }
                        frm_UT_Tree_Edit fChildForm = new frm_UT_Tree_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                 case 6:     // Tree Work //
                    {
                        view = (GridView)gridControl6.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 7:     // Tree Work Historical //
                    {
                        view = (GridView)gridControl7.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Historical Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ActionID")) + ',';
                        }
                        this.RefreshGridViewState7.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Work_Edit frmInstance = new frm_UT_Surveyed_Tree_Work_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 12:     // Tree Defect //
                    {
                        view = (GridView)gridControl12.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Tree Defect Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "TreeDefectID")) + ',';
                        }
                        this.RefreshGridViewState6.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Surveyed_Tree_Defect_Edit frmInstance = new frm_UT_Surveyed_Tree_Defect_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;
                        frmInstance.strDefaultPath = strDefaultPath;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 22:     // Risk Assessment //
                    {
                        view = (GridView)gridControl22.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Work Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RiskAssessmentID")) + ',';
                        }
                        this.RefreshGridViewState22.SaveViewInfo();  // Store Grid View State //
                        frm_UT_Risk_Assessment_Edit frmInstance = new frm_UT_Risk_Assessment_Edit();
                        frmInstance.MdiParent = this.MdiParent;
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.strRecordIDs = strRecordIDs;
                        frmInstance.strFormMode = "view";
                        frmInstance.strCaller = this.Name;
                        frmInstance.intRecordCount = 0;
                        frmInstance.FormPermissions = this.FormPermissions;

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        frmInstance.splashScreenManager = splashScreenManager1;
                        frmInstance.splashScreenManager.ShowWaitForm();
                        frmInstance.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(frmInstance, new object[] { null });
                    }
                    break;
                 case 27:     // Work Permission //
                    {
                        view = (GridView)gridControl27.MainView;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select just one record to view before proceeding.", "View Permission Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strLayout = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PermissionDocumentID")) + ',';
                            strLayout = view.GetRowCellValue(intRowHandle, "DataEntryScreenName").ToString();
                        }
                        this.RefreshGridViewState27.SaveViewInfo();  // Store Grid View State //
                        if (strLayout == "frm_UT_Permission_Doc_Edit_WPD")
                        {
                            frm_UT_Permission_Doc_Edit_WPD fChildForm = new frm_UT_Permission_Doc_Edit_WPD();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }
                        else if (strLayout == "frm_UT_Permission_Doc_Edit_UKPN")
                        {
                            frm_UT_Permission_Doc_Edit_UKPN fChildForm = new frm_UT_Permission_Doc_Edit_UKPN();
                            fChildForm.MdiParent = this.MdiParent;
                            fChildForm.GlobalSettings = this.GlobalSettings;
                            fChildForm.strRecordIDs = strRecordIDs;
                            fChildForm.strFormMode = "view";
                            fChildForm.strCaller = this.Name;
                            fChildForm.intRecordCount = intCount;
                            fChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            fChildForm.splashScreenManager = splashScreenManager1;
                            fChildForm.splashScreenManager.ShowWaitForm();
                            fChildForm.Show();

                            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                            if (method != null) method.Invoke(fChildForm, new object[] { null });
                        }                        
                    }
                    break;

                default:
                    break;
            }
        }


        private void bciShowLinkedRecords_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciShowLinkedRecords.Checked)
            {
                dockPanelLinkedRecords.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelLinkedRecords.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }






 




    }
}


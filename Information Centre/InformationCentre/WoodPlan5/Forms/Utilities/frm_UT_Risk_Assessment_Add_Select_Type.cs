﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_UT_Risk_Assessment_Add_Select_Type : DevExpress.XtraEditors.XtraForm
    {
        public frm_UT_Risk_Assessment_Add_Select_Type()
        {
            InitializeComponent();
        }

        private void btnSiteSpecific_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnG552_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
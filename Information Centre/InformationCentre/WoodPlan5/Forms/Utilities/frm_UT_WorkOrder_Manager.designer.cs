namespace WoodPlan5
{
    partial class frm_UT_WorkOrder_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_WorkOrder_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07244UTWorkOrderManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateSubContractorSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colWorkOrderPDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHoldCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLinkedActions = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTeamRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentEcoPlugs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentPaint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStumpTreatmentSpraying = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementNone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementStandards = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReplacementWhips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstimatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPossibleLiveWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedSubContractors = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedMaps = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditMap = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colMapDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMapDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTakenByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPaddedWorkOrderID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedQuotes = new DevExpress.XtraTab.XtraTabPage();
            this.quotesGridControl = new DevExpress.XtraGrid.GridControl();
            this.sp07434UTQuoteManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Quote = new WoodPlan5.DataSet_UT_Quote();
            this.quotesGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQuoteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnalysisCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnalysisCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBillingCentreCodeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsMainQuote = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredExchequer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRatingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteItemID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuoteCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRateVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.moneyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSellRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellRateVAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSellRateTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBuyRateTotal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFreeTextRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMarkUp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuantity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemUnitsID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRating = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemCreationDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdatedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.bbiPrintWorkOrder = new DevExpress.XtraBars.BarButtonItem();
            this.sp07244_UT_Work_Order_Manager_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07244_UT_Work_Order_Manager_ListTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupContainerDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter();
            this.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter();
            this.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter();
            this.sp07434_UT_Quote_ManagerTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07434_UT_Quote_ManagerTableAdapter();
            this.dataSetUTQuoteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07418UTQuoteListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp07418_UT_Quote_ListTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07418_UT_Quote_ListTableAdapter();
            this.sp07448_UT_POTransactionTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07448_UT_POTransactionTableAdapter();
            this.sp07454_UT_POLineTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07454_UT_POLineTableAdapter();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp07448_UT_POHeaderTransactionTableAdapter = new WoodPlan5.DataSet_UT_QuoteTableAdapters.sp07448_UT_POHeaderTransactionTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07244UTWorkOrderManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageLinkedActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07245UTWorkOrderManagerLinkedActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            this.xtraTabPageLinkedSubContractors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.xtraTabPageLinkedMaps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07270UTWorkOrderManagerLinkedMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.xtraTabPageLinkedQuotes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quotesGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07434UTQuoteManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetUTQuoteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07418UTQuoteListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrintWorkOrder, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1193, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 683);
            this.barDockControlBottom.Size = new System.Drawing.Size(1193, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 649);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1193, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 649);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiPrintWorkOrder,
            this.popupContainerDateRange,
            this.bbiRefresh});
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 34);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Work Orders";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1193, 649);
            this.splitContainerControl1.SplitterPosition = 220;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1189, 399);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07244UTWorkOrderManagerListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemHyperLinkEdit3});
            this.gridControl1.Size = new System.Drawing.Size(1189, 399);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07244UTWorkOrderManagerListBindingSource
            // 
            this.sp07244UTWorkOrderManagerListBindingSource.DataMember = "sp07244_UT_Work_Order_Manager_List";
            this.sp07244UTWorkOrderManagerListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("export_16x16.png", "images/export/export_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/export/export_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "export_16x16.png");
            this.imageCollection1.InsertGalleryImage("deletelist_16x16.png", "images/actions/deletelist_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/deletelist_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "deletelist_16x16.png");
            this.imageCollection1.InsertGalleryImage("exporttopdf_16x16.png", "images/export/exporttopdf_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/export/exporttopdf_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "exporttopdf_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderID,
            this.colReferenceNumber,
            this.colDateCreated,
            this.colDateSubContractorSent,
            this.colDateCompleted,
            this.colTotalCost,
            this.colRemarks,
            this.colGUID,
            this.colCreatedByStaffID,
            this.colPaddedWorkOrderID1,
            this.colCreatedBy,
            this.colLinkedActionCount,
            this.colWorkOrderPDF,
            this.colStatusID,
            this.colStatusDescription,
            this.colPermissionDocumentCount,
            this.colTeamOnHoldCount});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 91;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 1;
            this.colReferenceNumber.Width = 125;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 3;
            this.colDateCreated.Width = 98;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDateSubContractorSent
            // 
            this.colDateSubContractorSent.Caption = "Contractor Sent Date";
            this.colDateSubContractorSent.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateSubContractorSent.FieldName = "DateSubContractorSent";
            this.colDateSubContractorSent.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateSubContractorSent.Name = "colDateSubContractorSent";
            this.colDateSubContractorSent.OptionsColumn.AllowEdit = false;
            this.colDateSubContractorSent.OptionsColumn.AllowFocus = false;
            this.colDateSubContractorSent.OptionsColumn.ReadOnly = true;
            this.colDateSubContractorSent.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateSubContractorSent.Visible = true;
            this.colDateSubContractorSent.VisibleIndex = 4;
            this.colDateSubContractorSent.Width = 124;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 5;
            this.colDateCompleted.Width = 98;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 6;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 12;
            this.colRemarks.Width = 142;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colPaddedWorkOrderID1
            // 
            this.colPaddedWorkOrderID1.Caption = "Work Order #";
            this.colPaddedWorkOrderID1.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID1.Name = "colPaddedWorkOrderID1";
            this.colPaddedWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID1.Visible = true;
            this.colPaddedWorkOrderID1.VisibleIndex = 0;
            this.colPaddedWorkOrderID1.Width = 101;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 7;
            this.colCreatedBy.Width = 116;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Actions";
            this.colLinkedActionCount.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 9;
            this.colLinkedActionCount.Width = 89;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // colWorkOrderPDF
            // 
            this.colWorkOrderPDF.Caption = "Saved Work Order PDF File";
            this.colWorkOrderPDF.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colWorkOrderPDF.FieldName = "WorkOrderPDF";
            this.colWorkOrderPDF.Name = "colWorkOrderPDF";
            this.colWorkOrderPDF.OptionsColumn.ReadOnly = true;
            this.colWorkOrderPDF.Visible = true;
            this.colWorkOrderPDF.VisibleIndex = 8;
            this.colWorkOrderPDF.Width = 289;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 2;
            this.colStatusDescription.Width = 142;
            // 
            // colPermissionDocumentCount
            // 
            this.colPermissionDocumentCount.Caption = "Linked Permission Docs";
            this.colPermissionDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colPermissionDocumentCount.FieldName = "PermissionDocumentCount";
            this.colPermissionDocumentCount.Name = "colPermissionDocumentCount";
            this.colPermissionDocumentCount.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentCount.Visible = true;
            this.colPermissionDocumentCount.VisibleIndex = 11;
            this.colPermissionDocumentCount.Width = 130;
            // 
            // colTeamOnHoldCount
            // 
            this.colTeamOnHoldCount.Caption = "Team On-Hold Count";
            this.colTeamOnHoldCount.FieldName = "TeamOnHoldCount";
            this.colTeamOnHoldCount.Name = "colTeamOnHoldCount";
            this.colTeamOnHoldCount.OptionsColumn.AllowEdit = false;
            this.colTeamOnHoldCount.OptionsColumn.AllowFocus = false;
            this.colTeamOnHoldCount.OptionsColumn.ReadOnly = true;
            this.colTeamOnHoldCount.Visible = true;
            this.colTeamOnHoldCount.VisibleIndex = 10;
            this.colTeamOnHoldCount.Width = 119;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(60, 90);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 4;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageLinkedActions;
            this.xtraTabControl1.Size = new System.Drawing.Size(1193, 220);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLinkedActions,
            this.xtraTabPageLinkedSubContractors,
            this.xtraTabPageLinkedMaps,
            this.xtraTabPageLinkedDocuments,
            this.xtraTabPageLinkedQuotes});
            // 
            // xtraTabPageLinkedActions
            // 
            this.xtraTabPageLinkedActions.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPageLinkedActions.Name = "xtraTabPageLinkedActions";
            this.xtraTabPageLinkedActions.Size = new System.Drawing.Size(1188, 194);
            this.xtraTabPageLinkedActions.Text = "Linked Actions";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1188, 194);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07245UTWorkOrderManagerLinkedActionsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1188, 194);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07245UTWorkOrderManagerLinkedActionsBindingSource
            // 
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource.DataMember = "sp07245_UT_Work_Order_Manager_Linked_Actions";
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.colPaddedWorkOrderID,
            this.colWorkOrderSortOrder,
            this.colPoleID,
            this.colCircuitName,
            this.colPoleNumber,
            this.colTreeReferenceNumber,
            this.colLastStartTime,
            this.colTotalTimeTaken,
            this.colTeamRemarks,
            this.colActionStatusID,
            this.colActionStatus,
            this.colRegionName,
            this.colRegionNumber,
            this.colSubAreaName,
            this.colSubAreaNumber,
            this.colPrimaryName,
            this.colPrimaryNumber,
            this.colFeederName,
            this.colFeederNumber,
            this.colCircuitNumber,
            this.colStumpTreatmentNone,
            this.colStumpTreatmentEcoPlugs,
            this.colStumpTreatmentPaint,
            this.colStumpTreatmentSpraying,
            this.colTreeReplacementNone,
            this.colTreeReplacementStandards,
            this.colTreeReplacementWhips,
            this.colActualHours,
            this.colEstimatedHours,
            this.colPossibleLiveWork,
            this.colTeamOnHold,
            this.colHeldUpType,
            this.colIsShutdownRequired});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Surveyed Tree ID";
            this.gridColumn2.FieldName = "SurveyedTreeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 106;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Job Type ID";
            this.gridColumn3.FieldName = "JobTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 79;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Reference Number";
            this.gridColumn4.FieldName = "ReferenceNumber";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 112;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Date Raised";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn5.FieldName = "DateRaised";
            this.gridColumn5.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            this.gridColumn5.Width = 99;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Date Scheduled";
            this.gridColumn6.FieldName = "DateScheduled";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            this.gridColumn6.Width = 96;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Date Completed";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn7.FieldName = "DateCompleted";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            this.gridColumn7.Width = 98;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Approved";
            this.gridColumn8.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn8.FieldName = "Approved";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 10;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Apporved By Staff ID";
            this.gridColumn9.FieldName = "ApprovedByStaffID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 124;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Est. Labour Cost";
            this.gridColumn16.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn16.FieldName = "EstimatedLabourCost";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 19;
            this.gridColumn16.Width = 101;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Act. Labour Cost";
            this.gridColumn17.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn17.FieldName = "ActualLabourCost";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 20;
            this.gridColumn17.Width = 102;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Est. Material Cost";
            this.gridColumn18.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn18.FieldName = "EstimatedMaterialsCost";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 27;
            this.gridColumn18.Width = 106;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Act. Material Cost";
            this.gridColumn19.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn19.FieldName = "ActualMaterialsCost";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Visible = true;
            this.gridColumn19.VisibleIndex = 28;
            this.gridColumn19.Width = 107;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Est. Equip. Cost";
            this.gridColumn20.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn20.FieldName = "EstimatedEquipmentCost";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 23;
            this.gridColumn20.Width = 98;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Act. Equip. Cost";
            this.gridColumn21.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn21.FieldName = "ActualEquipmentCost";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 24;
            this.gridColumn21.Width = 99;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Est. Total Cost";
            this.gridColumn22.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn22.FieldName = "EstimatedTotalCost";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 31;
            this.gridColumn22.Width = 92;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Act. Total Cost";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn23.FieldName = "ActualTotalCost";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 33;
            this.gridColumn23.Width = 93;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Work Order";
            this.gridColumn24.FieldName = "WorkOrderID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 77;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Self Billing ID";
            this.gridColumn25.FieldName = "SelfBillingInvoiceID";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Width = 82;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Finance Billing ID";
            this.gridColumn26.FieldName = "FinanceSystemBillingID";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Width = 101;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Remarks";
            this.gridColumn27.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn27.FieldName = "Remarks";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 14;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "GUID";
            this.gridColumn28.FieldName = "GUID";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Linked To Tree";
            this.gridColumn29.FieldName = "LinkedRecordDescription";
            this.gridColumn29.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 1;
            this.gridColumn29.Width = 235;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Job Description";
            this.gridColumn31.FieldName = "JobDescription";
            this.gridColumn31.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 2;
            this.gridColumn31.Width = 162;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Est. Total Sell";
            this.gridColumn32.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn32.FieldName = "EstimatedTotalSell";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 32;
            this.gridColumn32.Width = 86;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Act. Total Sell";
            this.gridColumn33.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn33.FieldName = "ActualTotalSell";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 34;
            this.gridColumn33.Width = 87;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Est. Labour Sell";
            this.gridColumn34.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn34.FieldName = "EstimatedLabourSell";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 21;
            this.gridColumn34.Width = 95;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Act. Labour Sell";
            this.gridColumn35.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn35.FieldName = "ActualLabourSell";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 22;
            this.gridColumn35.Width = 96;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Est. Equip. Sell";
            this.gridColumn36.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn36.FieldName = "EstimatedEquipmentSell";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.AllowEdit = false;
            this.gridColumn36.OptionsColumn.AllowFocus = false;
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 25;
            this.gridColumn36.Width = 92;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Act. Equip. Sell";
            this.gridColumn37.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn37.FieldName = "ActualEquipmentSell";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 26;
            this.gridColumn37.Width = 93;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Est. Material Sell";
            this.gridColumn38.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn38.FieldName = "EstimatedMaterialsSell";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.AllowEdit = false;
            this.gridColumn38.OptionsColumn.AllowFocus = false;
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 29;
            this.gridColumn38.Width = 100;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Act. Material Sell";
            this.gridColumn39.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.gridColumn39.FieldName = "ActualMaterialsSell";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 30;
            this.gridColumn39.Width = 101;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Linked To Work Order";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Visible = true;
            this.colPaddedWorkOrderID.VisibleIndex = 34;
            this.colPaddedWorkOrderID.Width = 138;
            // 
            // colWorkOrderSortOrder
            // 
            this.colWorkOrderSortOrder.Caption = "Order";
            this.colWorkOrderSortOrder.FieldName = "WorkOrderSortOrder";
            this.colWorkOrderSortOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colWorkOrderSortOrder.Name = "colWorkOrderSortOrder";
            this.colWorkOrderSortOrder.OptionsColumn.AllowEdit = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowFocus = false;
            this.colWorkOrderSortOrder.OptionsColumn.ReadOnly = true;
            this.colWorkOrderSortOrder.Visible = true;
            this.colWorkOrderSortOrder.VisibleIndex = 0;
            this.colWorkOrderSortOrder.Width = 62;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            this.colPoleID.Width = 59;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 43;
            this.colCircuitName.Width = 125;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 45;
            this.colPoleNumber.Width = 81;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Width = 96;
            // 
            // colLastStartTime
            // 
            this.colLastStartTime.Caption = "Last Start Time";
            this.colLastStartTime.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colLastStartTime.FieldName = "LastStartTime";
            this.colLastStartTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastStartTime.Name = "colLastStartTime";
            this.colLastStartTime.OptionsColumn.AllowEdit = false;
            this.colLastStartTime.OptionsColumn.AllowFocus = false;
            this.colLastStartTime.OptionsColumn.ReadOnly = true;
            this.colLastStartTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastStartTime.Visible = true;
            this.colLastStartTime.VisibleIndex = 7;
            this.colLastStartTime.Width = 93;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.Visible = true;
            this.colTotalTimeTaken.VisibleIndex = 9;
            this.colTotalTimeTaken.Width = 102;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colTeamRemarks
            // 
            this.colTeamRemarks.Caption = "Team Remarks";
            this.colTeamRemarks.FieldName = "TeamRemarks";
            this.colTeamRemarks.Name = "colTeamRemarks";
            this.colTeamRemarks.OptionsColumn.ReadOnly = true;
            this.colTeamRemarks.Visible = true;
            this.colTeamRemarks.VisibleIndex = 15;
            this.colTeamRemarks.Width = 91;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.Caption = "Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colActionStatus
            // 
            this.colActionStatus.Caption = "Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 6;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 35;
            this.colRegionName.Width = 125;
            // 
            // colRegionNumber
            // 
            this.colRegionNumber.Caption = "Region #";
            this.colRegionNumber.FieldName = "RegionNumber";
            this.colRegionNumber.Name = "colRegionNumber";
            this.colRegionNumber.OptionsColumn.AllowEdit = false;
            this.colRegionNumber.OptionsColumn.AllowFocus = false;
            this.colRegionNumber.OptionsColumn.ReadOnly = true;
            this.colRegionNumber.Visible = true;
            this.colRegionNumber.VisibleIndex = 36;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub-Area Name";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 37;
            this.colSubAreaName.Width = 125;
            // 
            // colSubAreaNumber
            // 
            this.colSubAreaNumber.Caption = "Sub-Area #";
            this.colSubAreaNumber.FieldName = "SubAreaNumber";
            this.colSubAreaNumber.Name = "colSubAreaNumber";
            this.colSubAreaNumber.OptionsColumn.AllowEdit = false;
            this.colSubAreaNumber.OptionsColumn.AllowFocus = false;
            this.colSubAreaNumber.OptionsColumn.ReadOnly = true;
            this.colSubAreaNumber.Visible = true;
            this.colSubAreaNumber.VisibleIndex = 38;
            this.colSubAreaNumber.Width = 79;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary Name";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 39;
            this.colPrimaryName.Width = 125;
            // 
            // colPrimaryNumber
            // 
            this.colPrimaryNumber.Caption = "Primary #";
            this.colPrimaryNumber.FieldName = "PrimaryNumber";
            this.colPrimaryNumber.Name = "colPrimaryNumber";
            this.colPrimaryNumber.OptionsColumn.AllowEdit = false;
            this.colPrimaryNumber.OptionsColumn.AllowFocus = false;
            this.colPrimaryNumber.OptionsColumn.ReadOnly = true;
            this.colPrimaryNumber.Visible = true;
            this.colPrimaryNumber.VisibleIndex = 40;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 41;
            this.colFeederName.Width = 125;
            // 
            // colFeederNumber
            // 
            this.colFeederNumber.Caption = "Feeder #";
            this.colFeederNumber.FieldName = "FeederNumber";
            this.colFeederNumber.Name = "colFeederNumber";
            this.colFeederNumber.OptionsColumn.AllowEdit = false;
            this.colFeederNumber.OptionsColumn.AllowFocus = false;
            this.colFeederNumber.OptionsColumn.ReadOnly = true;
            this.colFeederNumber.Visible = true;
            this.colFeederNumber.VisibleIndex = 42;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit #";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 44;
            // 
            // colStumpTreatmentNone
            // 
            this.colStumpTreatmentNone.Caption = "Stump Treatment - None";
            this.colStumpTreatmentNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentNone.FieldName = "StumpTreatmentNone";
            this.colStumpTreatmentNone.Name = "colStumpTreatmentNone";
            this.colStumpTreatmentNone.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentNone.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentNone.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentNone.Visible = true;
            this.colStumpTreatmentNone.VisibleIndex = 46;
            this.colStumpTreatmentNone.Width = 139;
            // 
            // colStumpTreatmentEcoPlugs
            // 
            this.colStumpTreatmentEcoPlugs.Caption = "Stump Treatment - Eco Plugs";
            this.colStumpTreatmentEcoPlugs.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentEcoPlugs.FieldName = "StumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.Name = "colStumpTreatmentEcoPlugs";
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentEcoPlugs.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentEcoPlugs.Visible = true;
            this.colStumpTreatmentEcoPlugs.VisibleIndex = 47;
            this.colStumpTreatmentEcoPlugs.Width = 159;
            // 
            // colStumpTreatmentPaint
            // 
            this.colStumpTreatmentPaint.Caption = "Stump Treatment - Paint";
            this.colStumpTreatmentPaint.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentPaint.FieldName = "StumpTreatmentPaint";
            this.colStumpTreatmentPaint.Name = "colStumpTreatmentPaint";
            this.colStumpTreatmentPaint.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentPaint.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentPaint.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentPaint.Visible = true;
            this.colStumpTreatmentPaint.VisibleIndex = 48;
            this.colStumpTreatmentPaint.Width = 138;
            // 
            // colStumpTreatmentSpraying
            // 
            this.colStumpTreatmentSpraying.Caption = "Stump Treatment - Spraying";
            this.colStumpTreatmentSpraying.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colStumpTreatmentSpraying.FieldName = "StumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.Name = "colStumpTreatmentSpraying";
            this.colStumpTreatmentSpraying.OptionsColumn.AllowEdit = false;
            this.colStumpTreatmentSpraying.OptionsColumn.AllowFocus = false;
            this.colStumpTreatmentSpraying.OptionsColumn.ReadOnly = true;
            this.colStumpTreatmentSpraying.Visible = true;
            this.colStumpTreatmentSpraying.VisibleIndex = 49;
            this.colStumpTreatmentSpraying.Width = 156;
            // 
            // colTreeReplacementNone
            // 
            this.colTreeReplacementNone.Caption = "Tree Replacement - None";
            this.colTreeReplacementNone.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTreeReplacementNone.FieldName = "TreeReplacementNone";
            this.colTreeReplacementNone.Name = "colTreeReplacementNone";
            this.colTreeReplacementNone.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementNone.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementNone.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementNone.Visible = true;
            this.colTreeReplacementNone.VisibleIndex = 50;
            this.colTreeReplacementNone.Width = 143;
            // 
            // colTreeReplacementStandards
            // 
            this.colTreeReplacementStandards.Caption = "Tree Replacement - Standards";
            this.colTreeReplacementStandards.FieldName = "TreeReplacementStandards";
            this.colTreeReplacementStandards.Name = "colTreeReplacementStandards";
            this.colTreeReplacementStandards.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementStandards.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementStandards.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementStandards.Visible = true;
            this.colTreeReplacementStandards.VisibleIndex = 51;
            this.colTreeReplacementStandards.Width = 167;
            // 
            // colTreeReplacementWhips
            // 
            this.colTreeReplacementWhips.Caption = "Tree Replacement - Whips";
            this.colTreeReplacementWhips.FieldName = "TreeReplacementWhips";
            this.colTreeReplacementWhips.Name = "colTreeReplacementWhips";
            this.colTreeReplacementWhips.OptionsColumn.AllowEdit = false;
            this.colTreeReplacementWhips.OptionsColumn.AllowFocus = false;
            this.colTreeReplacementWhips.OptionsColumn.ReadOnly = true;
            this.colTreeReplacementWhips.Visible = true;
            this.colTreeReplacementWhips.VisibleIndex = 52;
            this.colTreeReplacementWhips.Width = 147;
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 18;
            this.colActualHours.Width = 82;
            // 
            // colEstimatedHours
            // 
            this.colEstimatedHours.Caption = "Estimated Hours";
            this.colEstimatedHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colEstimatedHours.FieldName = "EstimatedHours";
            this.colEstimatedHours.Name = "colEstimatedHours";
            this.colEstimatedHours.OptionsColumn.AllowEdit = false;
            this.colEstimatedHours.OptionsColumn.AllowFocus = false;
            this.colEstimatedHours.OptionsColumn.ReadOnly = true;
            this.colEstimatedHours.Visible = true;
            this.colEstimatedHours.VisibleIndex = 17;
            this.colEstimatedHours.Width = 99;
            // 
            // colPossibleLiveWork
            // 
            this.colPossibleLiveWork.Caption = "Possible Live Work";
            this.colPossibleLiveWork.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colPossibleLiveWork.FieldName = "PossibleLiveWork";
            this.colPossibleLiveWork.Name = "colPossibleLiveWork";
            this.colPossibleLiveWork.OptionsColumn.AllowEdit = false;
            this.colPossibleLiveWork.OptionsColumn.AllowFocus = false;
            this.colPossibleLiveWork.OptionsColumn.ReadOnly = true;
            this.colPossibleLiveWork.Visible = true;
            this.colPossibleLiveWork.VisibleIndex = 16;
            this.colPossibleLiveWork.Width = 109;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.OptionsColumn.AllowEdit = false;
            this.colTeamOnHold.OptionsColumn.AllowFocus = false;
            this.colTeamOnHold.OptionsColumn.ReadOnly = true;
            this.colTeamOnHold.Visible = true;
            this.colTeamOnHold.VisibleIndex = 13;
            this.colTeamOnHold.Width = 87;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type ";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 11;
            this.colHeldUpType.Width = 101;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 12;
            this.colIsShutdownRequired.Width = 113;
            // 
            // xtraTabPageLinkedSubContractors
            // 
            this.xtraTabPageLinkedSubContractors.Controls.Add(this.gridControl4);
            this.xtraTabPageLinkedSubContractors.Name = "xtraTabPageLinkedSubContractors";
            this.xtraTabPageLinkedSubContractors.Size = new System.Drawing.Size(1188, 194);
            this.xtraTabPageLinkedSubContractors.Text = "Linked Sub-Contractors";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit4});
            this.gridControl4.Size = new System.Drawing.Size(1188, 194);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource
            // 
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource.DataMember = "sp07269_UT_Work_Order_Manager_Linked_SubContractors";
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderTeamID,
            this.colWorkOrderID1,
            this.colSubContractorID,
            this.colRemarks1,
            this.colGUID1,
            this.colSubContractorName,
            this.colInternalContractor,
            this.colMobileTel,
            this.colTelephone1,
            this.colTelephone2,
            this.colPostcode,
            this.colPaddedWorkOrderID2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colWorkOrderTeamID
            // 
            this.colWorkOrderTeamID.Caption = "Work Order Team ID";
            this.colWorkOrderTeamID.FieldName = "WorkOrderTeamID";
            this.colWorkOrderTeamID.Name = "colWorkOrderTeamID";
            this.colWorkOrderTeamID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderTeamID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderTeamID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderTeamID.Width = 120;
            // 
            // colWorkOrderID1
            // 
            this.colWorkOrderID1.Caption = "Work Order ID";
            this.colWorkOrderID1.FieldName = "WorkOrderID";
            this.colWorkOrderID1.Name = "colWorkOrderID1";
            this.colWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID1.Width = 91;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            this.colRemarks1.Width = 210;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Sub-Contractor Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 0;
            this.colSubContractorName.Width = 288;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 1;
            this.colInternalContractor.Width = 61;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Telephone";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 2;
            this.colMobileTel.Width = 117;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 3;
            this.colTelephone1.Width = 97;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 4;
            this.colTelephone2.Width = 98;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 5;
            // 
            // colPaddedWorkOrderID2
            // 
            this.colPaddedWorkOrderID2.Caption = "Work Order #";
            this.colPaddedWorkOrderID2.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID2.Name = "colPaddedWorkOrderID2";
            this.colPaddedWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID2.Visible = true;
            this.colPaddedWorkOrderID2.VisibleIndex = 7;
            this.colPaddedWorkOrderID2.Width = 88;
            // 
            // xtraTabPageLinkedMaps
            // 
            this.xtraTabPageLinkedMaps.Controls.Add(this.gridControl5);
            this.xtraTabPageLinkedMaps.Name = "xtraTabPageLinkedMaps";
            this.xtraTabPageLinkedMaps.Size = new System.Drawing.Size(1188, 194);
            this.xtraTabPageLinkedMaps.Text = "Linked Maps";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07270UTWorkOrderManagerLinkedMapsBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemHyperLinkEditMap,
            this.repositoryItemMemoExEdit4});
            this.gridControl5.Size = new System.Drawing.Size(1188, 194);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07270UTWorkOrderManagerLinkedMapsBindingSource
            // 
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource.DataMember = "sp07270_UT_Work_Order_Manager_Linked_Maps";
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMapID,
            this.colWorkOrderID2,
            this.colMap,
            this.colMapDateTimeCreated,
            this.colMapDescription,
            this.colMapOrder,
            this.colPictureTakenByName,
            this.colPictureRemarks,
            this.colPaddedWorkOrderID3});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colWorkOrderID2
            // 
            this.colWorkOrderID2.Caption = "Work Order ID";
            this.colWorkOrderID2.FieldName = "WorkOrderID";
            this.colWorkOrderID2.Name = "colWorkOrderID2";
            this.colWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID2.Width = 91;
            // 
            // colMap
            // 
            this.colMap.Caption = "Map";
            this.colMap.ColumnEdit = this.repositoryItemHyperLinkEditMap;
            this.colMap.FieldName = "Map";
            this.colMap.Name = "colMap";
            this.colMap.OptionsColumn.ReadOnly = true;
            this.colMap.Visible = true;
            this.colMap.VisibleIndex = 4;
            this.colMap.Width = 386;
            // 
            // repositoryItemHyperLinkEditMap
            // 
            this.repositoryItemHyperLinkEditMap.AutoHeight = false;
            this.repositoryItemHyperLinkEditMap.Name = "repositoryItemHyperLinkEditMap";
            this.repositoryItemHyperLinkEditMap.SingleClick = true;
            this.repositoryItemHyperLinkEditMap.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditMap_OpenLink);
            // 
            // colMapDateTimeCreated
            // 
            this.colMapDateTimeCreated.Caption = "Created On";
            this.colMapDateTimeCreated.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colMapDateTimeCreated.FieldName = "MapDateTimeCreated";
            this.colMapDateTimeCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colMapDateTimeCreated.Name = "colMapDateTimeCreated";
            this.colMapDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colMapDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colMapDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colMapDateTimeCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colMapDateTimeCreated.Visible = true;
            this.colMapDateTimeCreated.VisibleIndex = 3;
            this.colMapDateTimeCreated.Width = 86;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colMapDescription
            // 
            this.colMapDescription.Caption = "Description";
            this.colMapDescription.FieldName = "MapDescription";
            this.colMapDescription.Name = "colMapDescription";
            this.colMapDescription.OptionsColumn.AllowEdit = false;
            this.colMapDescription.OptionsColumn.AllowFocus = false;
            this.colMapDescription.OptionsColumn.ReadOnly = true;
            this.colMapDescription.Visible = true;
            this.colMapDescription.VisibleIndex = 1;
            this.colMapDescription.Width = 323;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 62;
            // 
            // colPictureTakenByName
            // 
            this.colPictureTakenByName.Caption = "Created By";
            this.colPictureTakenByName.FieldName = "PictureTakenByName";
            this.colPictureTakenByName.Name = "colPictureTakenByName";
            this.colPictureTakenByName.OptionsColumn.AllowEdit = false;
            this.colPictureTakenByName.OptionsColumn.AllowFocus = false;
            this.colPictureTakenByName.OptionsColumn.ReadOnly = true;
            this.colPictureTakenByName.Visible = true;
            this.colPictureTakenByName.VisibleIndex = 2;
            this.colPictureTakenByName.Width = 91;
            // 
            // colPictureRemarks
            // 
            this.colPictureRemarks.Caption = "Remarks";
            this.colPictureRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colPictureRemarks.FieldName = "PictureRemarks";
            this.colPictureRemarks.Name = "colPictureRemarks";
            this.colPictureRemarks.OptionsColumn.ReadOnly = true;
            this.colPictureRemarks.Visible = true;
            this.colPictureRemarks.VisibleIndex = 5;
            this.colPictureRemarks.Width = 136;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colPaddedWorkOrderID3
            // 
            this.colPaddedWorkOrderID3.Caption = "Work Order #";
            this.colPaddedWorkOrderID3.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID3.Name = "colPaddedWorkOrderID3";
            this.colPaddedWorkOrderID3.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID3.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID3.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID3.Visible = true;
            this.colPaddedWorkOrderID3.VisibleIndex = 7;
            this.colPaddedWorkOrderID3.Width = 88;
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(1188, 194);
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1188, 194);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl3.Size = new System.Drawing.Size(1188, 194);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // xtraTabPageLinkedQuotes
            // 
            this.xtraTabPageLinkedQuotes.Controls.Add(this.quotesGridControl);
            this.xtraTabPageLinkedQuotes.Name = "xtraTabPageLinkedQuotes";
            this.xtraTabPageLinkedQuotes.Size = new System.Drawing.Size(1188, 194);
            this.xtraTabPageLinkedQuotes.Text = "Linked Quotes";
            // 
            // quotesGridControl
            // 
            this.quotesGridControl.DataSource = this.sp07434UTQuoteManagerBindingSource;
            this.quotesGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.quotesGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.quotesGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Quote", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Amend Quote", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Quote", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Add completed main quote on Exchequer", "store"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Check if quote exists on Exchequer", "get"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete quote on Exchequer", "remove"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Export Quote To PDF", "report")});
            this.quotesGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.quotesGridControl_EmbeddedNavigator_ButtonClick);
            this.quotesGridControl.Location = new System.Drawing.Point(0, 0);
            this.quotesGridControl.MainView = this.quotesGridView;
            this.quotesGridControl.MenuManager = this.barManager1;
            this.quotesGridControl.Name = "quotesGridControl";
            this.quotesGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit2,
            this.moneyTextEdit});
            this.quotesGridControl.Size = new System.Drawing.Size(1188, 194);
            this.quotesGridControl.TabIndex = 2;
            this.quotesGridControl.UseEmbeddedNavigator = true;
            this.quotesGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.quotesGridView});
            // 
            // sp07434UTQuoteManagerBindingSource
            // 
            this.sp07434UTQuoteManagerBindingSource.DataMember = "sp07434_UT_Quote_Manager";
            this.sp07434UTQuoteManagerBindingSource.DataSource = this.dataSet_UT_Quote;
            // 
            // dataSet_UT_Quote
            // 
            this.dataSet_UT_Quote.DataSetName = "DataSet_UT_Quote";
            this.dataSet_UT_Quote.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // quotesGridView
            // 
            this.quotesGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQuoteID,
            this.colAnalysisCodeID,
            this.colAnalysisCode,
            this.colCircuitName1,
            this.colCircuitID,
            this.colFeederContractID,
            this.colContractName,
            this.colSubContractorID1,
            this.colSubContractorName1,
            this.colReference,
            this.colBillingCentreCode,
            this.colBillingCentreCodeID,
            this.colPaddedWorkOrderID4,
            this.colWorkOrderID3,
            this.colIsMainQuote,
            this.colTransferredExchequer,
            this.colPONumber,
            this.colCreationDate,
            this.colCreatedBy1,
            this.colRatingID,
            this.colQuoteItemID,
            this.colQuoteCategoryID,
            this.colQuoteCategory,
            this.colClientContractTypeID,
            this.colContractType,
            this.colClientName,
            this.colDescription1,
            this.colBuyRateVAT,
            this.colSellRate,
            this.colSellRateVAT,
            this.colSellRateTotal,
            this.colBuyRate,
            this.colBuyRateTotal,
            this.colFreeTextRate,
            this.colNetPrice,
            this.colNetCost,
            this.colMarkUp,
            this.colQuantity,
            this.colItemUnitsID,
            this.colUnits,
            this.colRating,
            this.colItemCreationDate,
            this.colLastUpdatedDate,
            this.colLastUpdatedBy,
            this.colNotes,
            this.colMode,
            this.colRecordID});
            this.quotesGridView.GridControl = this.quotesGridControl;
            this.quotesGridView.GroupCount = 1;
            this.quotesGridView.Name = "quotesGridView";
            this.quotesGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.quotesGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.quotesGridView.OptionsLayout.StoreAppearance = true;
            this.quotesGridView.OptionsMenu.ShowConditionalFormattingItem = true;
            this.quotesGridView.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.quotesGridView.OptionsSelection.MultiSelect = true;
            this.quotesGridView.OptionsView.AllowHtmlDrawHeaders = true;
            this.quotesGridView.OptionsView.ColumnAutoWidth = false;
            this.quotesGridView.OptionsView.EnableAppearanceEvenRow = true;
            this.quotesGridView.OptionsView.ShowGroupPanel = false;
            this.quotesGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReference, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.quotesGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.quotesGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.quotesGridView_SelectionChanged);
            this.quotesGridView.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.quotesGridView.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.quotesGridView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.quotesGridView_MouseUp);
            this.quotesGridView.DoubleClick += new System.EventHandler(this.quotesGridView_DoubleClick);
            this.quotesGridView.GotFocus += new System.EventHandler(this.quotesGridView_GotFocus);
            // 
            // colQuoteID
            // 
            this.colQuoteID.FieldName = "QuoteID";
            this.colQuoteID.Name = "colQuoteID";
            this.colQuoteID.OptionsColumn.AllowEdit = false;
            this.colQuoteID.OptionsColumn.AllowFocus = false;
            this.colQuoteID.OptionsColumn.ReadOnly = true;
            // 
            // colAnalysisCodeID
            // 
            this.colAnalysisCodeID.FieldName = "AnalysisCodeID";
            this.colAnalysisCodeID.Name = "colAnalysisCodeID";
            this.colAnalysisCodeID.OptionsColumn.AllowEdit = false;
            this.colAnalysisCodeID.OptionsColumn.AllowFocus = false;
            this.colAnalysisCodeID.OptionsColumn.ReadOnly = true;
            this.colAnalysisCodeID.OptionsColumn.ShowInCustomizationForm = false;
            this.colAnalysisCodeID.OptionsColumn.ShowInExpressionEditor = false;
            this.colAnalysisCodeID.Width = 102;
            // 
            // colAnalysisCode
            // 
            this.colAnalysisCode.FieldName = "AnalysisCode";
            this.colAnalysisCode.Name = "colAnalysisCode";
            this.colAnalysisCode.OptionsColumn.AllowEdit = false;
            this.colAnalysisCode.OptionsColumn.AllowFocus = false;
            this.colAnalysisCode.OptionsColumn.ReadOnly = true;
            this.colAnalysisCode.Visible = true;
            this.colAnalysisCode.VisibleIndex = 25;
            this.colAnalysisCode.Width = 88;
            // 
            // colCircuitName1
            // 
            this.colCircuitName1.FieldName = "CircuitName";
            this.colCircuitName1.Name = "colCircuitName1";
            this.colCircuitName1.OptionsColumn.AllowEdit = false;
            this.colCircuitName1.OptionsColumn.AllowFocus = false;
            this.colCircuitName1.OptionsColumn.ReadOnly = true;
            this.colCircuitName1.Visible = true;
            this.colCircuitName1.VisibleIndex = 24;
            this.colCircuitName1.Width = 81;
            // 
            // colCircuitID
            // 
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            this.colCircuitID.OptionsColumn.ShowInCustomizationForm = false;
            this.colCircuitID.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colFeederContractID
            // 
            this.colFeederContractID.FieldName = "FeederContractID";
            this.colFeederContractID.Name = "colFeederContractID";
            this.colFeederContractID.OptionsColumn.AllowEdit = false;
            this.colFeederContractID.OptionsColumn.AllowFocus = false;
            this.colFeederContractID.OptionsColumn.ReadOnly = true;
            this.colFeederContractID.OptionsColumn.ShowInCustomizationForm = false;
            this.colFeederContractID.OptionsColumn.ShowInExpressionEditor = false;
            this.colFeederContractID.Width = 114;
            // 
            // colContractName
            // 
            this.colContractName.FieldName = "ContractName";
            this.colContractName.Name = "colContractName";
            this.colContractName.OptionsColumn.AllowEdit = false;
            this.colContractName.OptionsColumn.AllowFocus = false;
            this.colContractName.OptionsColumn.ReadOnly = true;
            this.colContractName.Visible = true;
            this.colContractName.VisibleIndex = 23;
            this.colContractName.Width = 93;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            this.colSubContractorID1.Width = 108;
            // 
            // colSubContractorName1
            // 
            this.colSubContractorName1.FieldName = "SubContractorName";
            this.colSubContractorName1.Name = "colSubContractorName1";
            this.colSubContractorName1.OptionsColumn.AllowEdit = false;
            this.colSubContractorName1.OptionsColumn.AllowFocus = false;
            this.colSubContractorName1.OptionsColumn.ReadOnly = true;
            this.colSubContractorName1.Visible = true;
            this.colSubContractorName1.VisibleIndex = 22;
            this.colSubContractorName1.Width = 124;
            // 
            // colReference
            // 
            this.colReference.FieldName = "Reference";
            this.colReference.Name = "colReference";
            this.colReference.OptionsColumn.AllowEdit = false;
            this.colReference.OptionsColumn.AllowFocus = false;
            this.colReference.OptionsColumn.ReadOnly = true;
            this.colReference.Visible = true;
            this.colReference.VisibleIndex = 2;
            this.colReference.Width = 84;
            // 
            // colBillingCentreCode
            // 
            this.colBillingCentreCode.FieldName = "BillingCentreCode";
            this.colBillingCentreCode.Name = "colBillingCentreCode";
            this.colBillingCentreCode.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCode.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCode.OptionsColumn.ReadOnly = true;
            this.colBillingCentreCode.Visible = true;
            this.colBillingCentreCode.VisibleIndex = 1;
            this.colBillingCentreCode.Width = 111;
            // 
            // colBillingCentreCodeID
            // 
            this.colBillingCentreCodeID.FieldName = "BillingCentreCodeID";
            this.colBillingCentreCodeID.Name = "colBillingCentreCodeID";
            this.colBillingCentreCodeID.OptionsColumn.AllowEdit = false;
            this.colBillingCentreCodeID.OptionsColumn.AllowFocus = false;
            this.colBillingCentreCodeID.OptionsColumn.ReadOnly = true;
            // 
            // colPaddedWorkOrderID4
            // 
            this.colPaddedWorkOrderID4.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID4.Name = "colPaddedWorkOrderID4";
            this.colPaddedWorkOrderID4.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID4.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID4.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID4.Width = 130;
            // 
            // colWorkOrderID3
            // 
            this.colWorkOrderID3.FieldName = "WorkOrderID";
            this.colWorkOrderID3.Name = "colWorkOrderID3";
            this.colWorkOrderID3.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID3.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID3.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID3.Width = 91;
            // 
            // colIsMainQuote
            // 
            this.colIsMainQuote.FieldName = "IsMainQuote";
            this.colIsMainQuote.Name = "colIsMainQuote";
            this.colIsMainQuote.OptionsColumn.AllowEdit = false;
            this.colIsMainQuote.OptionsColumn.AllowFocus = false;
            this.colIsMainQuote.OptionsColumn.ReadOnly = true;
            this.colIsMainQuote.Visible = true;
            this.colIsMainQuote.VisibleIndex = 8;
            this.colIsMainQuote.Width = 101;
            // 
            // colTransferredExchequer
            // 
            this.colTransferredExchequer.FieldName = "TransferredExchequer";
            this.colTransferredExchequer.Name = "colTransferredExchequer";
            this.colTransferredExchequer.OptionsColumn.AllowEdit = false;
            this.colTransferredExchequer.OptionsColumn.AllowFocus = false;
            this.colTransferredExchequer.OptionsColumn.ReadOnly = true;
            this.colTransferredExchequer.Visible = true;
            this.colTransferredExchequer.VisibleIndex = 0;
            this.colTransferredExchequer.Width = 132;
            // 
            // colPONumber
            // 
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.OptionsColumn.AllowEdit = false;
            this.colPONumber.OptionsColumn.AllowFocus = false;
            this.colPONumber.OptionsColumn.ReadOnly = true;
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 2;
            // 
            // colCreationDate
            // 
            this.colCreationDate.FieldName = "CreationDate";
            this.colCreationDate.Name = "colCreationDate";
            this.colCreationDate.OptionsColumn.AllowEdit = false;
            this.colCreationDate.OptionsColumn.AllowFocus = false;
            this.colCreationDate.OptionsColumn.ReadOnly = true;
            this.colCreationDate.Visible = true;
            this.colCreationDate.VisibleIndex = 3;
            this.colCreationDate.Width = 88;
            // 
            // colCreatedBy1
            // 
            this.colCreatedBy1.FieldName = "CreatedBy";
            this.colCreatedBy1.Name = "colCreatedBy1";
            this.colCreatedBy1.OptionsColumn.AllowEdit = false;
            this.colCreatedBy1.OptionsColumn.AllowFocus = false;
            this.colCreatedBy1.OptionsColumn.ReadOnly = true;
            this.colCreatedBy1.Visible = true;
            this.colCreatedBy1.VisibleIndex = 4;
            // 
            // colRatingID
            // 
            this.colRatingID.FieldName = "RatingID";
            this.colRatingID.Name = "colRatingID";
            this.colRatingID.OptionsColumn.AllowEdit = false;
            this.colRatingID.OptionsColumn.AllowFocus = false;
            this.colRatingID.OptionsColumn.ReadOnly = true;
            this.colRatingID.OptionsColumn.ShowInCustomizationForm = false;
            this.colRatingID.OptionsColumn.ShowInExpressionEditor = false;
            // 
            // colQuoteItemID
            // 
            this.colQuoteItemID.FieldName = "QuoteItemID";
            this.colQuoteItemID.Name = "colQuoteItemID";
            this.colQuoteItemID.OptionsColumn.AllowEdit = false;
            this.colQuoteItemID.OptionsColumn.AllowFocus = false;
            this.colQuoteItemID.OptionsColumn.ReadOnly = true;
            this.colQuoteItemID.Width = 90;
            // 
            // colQuoteCategoryID
            // 
            this.colQuoteCategoryID.FieldName = "QuoteCategoryID";
            this.colQuoteCategoryID.Name = "colQuoteCategoryID";
            this.colQuoteCategoryID.OptionsColumn.AllowEdit = false;
            this.colQuoteCategoryID.OptionsColumn.AllowFocus = false;
            this.colQuoteCategoryID.OptionsColumn.ReadOnly = true;
            this.colQuoteCategoryID.Width = 113;
            // 
            // colQuoteCategory
            // 
            this.colQuoteCategory.FieldName = "QuoteCategory";
            this.colQuoteCategory.Name = "colQuoteCategory";
            this.colQuoteCategory.OptionsColumn.AllowEdit = false;
            this.colQuoteCategory.OptionsColumn.AllowFocus = false;
            this.colQuoteCategory.OptionsColumn.ReadOnly = true;
            this.colQuoteCategory.Visible = true;
            this.colQuoteCategory.VisibleIndex = 5;
            this.colQuoteCategory.Width = 99;
            // 
            // colClientContractTypeID
            // 
            this.colClientContractTypeID.FieldName = "ClientContractTypeID";
            this.colClientContractTypeID.Name = "colClientContractTypeID";
            this.colClientContractTypeID.OptionsColumn.AllowEdit = false;
            this.colClientContractTypeID.OptionsColumn.AllowFocus = false;
            this.colClientContractTypeID.OptionsColumn.ReadOnly = true;
            this.colClientContractTypeID.Width = 134;
            // 
            // colContractType
            // 
            this.colContractType.FieldName = "ContractType";
            this.colContractType.Name = "colContractType";
            this.colContractType.OptionsColumn.AllowEdit = false;
            this.colContractType.OptionsColumn.AllowFocus = false;
            this.colContractType.OptionsColumn.ReadOnly = true;
            this.colContractType.Visible = true;
            this.colContractType.VisibleIndex = 6;
            this.colContractType.Width = 90;
            // 
            // colClientName
            // 
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 7;
            this.colClientName.Width = 78;
            // 
            // colDescription1
            // 
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 9;
            // 
            // colBuyRateVAT
            // 
            this.colBuyRateVAT.ColumnEdit = this.moneyTextEdit;
            this.colBuyRateVAT.FieldName = "BuyRateVAT";
            this.colBuyRateVAT.Name = "colBuyRateVAT";
            this.colBuyRateVAT.OptionsColumn.AllowEdit = false;
            this.colBuyRateVAT.OptionsColumn.AllowFocus = false;
            this.colBuyRateVAT.OptionsColumn.ReadOnly = true;
            this.colBuyRateVAT.Visible = true;
            this.colBuyRateVAT.VisibleIndex = 10;
            this.colBuyRateVAT.Width = 87;
            // 
            // moneyTextEdit
            // 
            this.moneyTextEdit.AutoHeight = false;
            this.moneyTextEdit.DisplayFormat.FormatString = "c2";
            this.moneyTextEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.moneyTextEdit.Name = "moneyTextEdit";
            // 
            // colSellRate
            // 
            this.colSellRate.ColumnEdit = this.moneyTextEdit;
            this.colSellRate.FieldName = "SellRate";
            this.colSellRate.Name = "colSellRate";
            this.colSellRate.OptionsColumn.AllowEdit = false;
            this.colSellRate.OptionsColumn.AllowFocus = false;
            this.colSellRate.OptionsColumn.ReadOnly = true;
            this.colSellRate.Visible = true;
            this.colSellRate.VisibleIndex = 11;
            // 
            // colSellRateVAT
            // 
            this.colSellRateVAT.ColumnEdit = this.moneyTextEdit;
            this.colSellRateVAT.FieldName = "SellRateVAT";
            this.colSellRateVAT.Name = "colSellRateVAT";
            this.colSellRateVAT.OptionsColumn.AllowEdit = false;
            this.colSellRateVAT.OptionsColumn.AllowFocus = false;
            this.colSellRateVAT.OptionsColumn.ReadOnly = true;
            this.colSellRateVAT.Visible = true;
            this.colSellRateVAT.VisibleIndex = 12;
            this.colSellRateVAT.Width = 85;
            // 
            // colSellRateTotal
            // 
            this.colSellRateTotal.ColumnEdit = this.moneyTextEdit;
            this.colSellRateTotal.FieldName = "SellRateTotal";
            this.colSellRateTotal.Name = "colSellRateTotal";
            this.colSellRateTotal.OptionsColumn.AllowEdit = false;
            this.colSellRateTotal.OptionsColumn.AllowFocus = false;
            this.colSellRateTotal.OptionsColumn.ReadOnly = true;
            this.colSellRateTotal.Visible = true;
            this.colSellRateTotal.VisibleIndex = 13;
            this.colSellRateTotal.Width = 90;
            // 
            // colBuyRate
            // 
            this.colBuyRate.ColumnEdit = this.moneyTextEdit;
            this.colBuyRate.FieldName = "BuyRate";
            this.colBuyRate.Name = "colBuyRate";
            this.colBuyRate.OptionsColumn.AllowEdit = false;
            this.colBuyRate.OptionsColumn.AllowFocus = false;
            this.colBuyRate.OptionsColumn.ReadOnly = true;
            this.colBuyRate.Visible = true;
            this.colBuyRate.VisibleIndex = 14;
            // 
            // colBuyRateTotal
            // 
            this.colBuyRateTotal.ColumnEdit = this.moneyTextEdit;
            this.colBuyRateTotal.FieldName = "BuyRateTotal";
            this.colBuyRateTotal.Name = "colBuyRateTotal";
            this.colBuyRateTotal.OptionsColumn.AllowEdit = false;
            this.colBuyRateTotal.OptionsColumn.AllowFocus = false;
            this.colBuyRateTotal.OptionsColumn.ReadOnly = true;
            this.colBuyRateTotal.Visible = true;
            this.colBuyRateTotal.VisibleIndex = 15;
            this.colBuyRateTotal.Width = 92;
            // 
            // colFreeTextRate
            // 
            this.colFreeTextRate.ColumnEdit = this.moneyTextEdit;
            this.colFreeTextRate.FieldName = "FreeTextRate";
            this.colFreeTextRate.Name = "colFreeTextRate";
            this.colFreeTextRate.OptionsColumn.AllowEdit = false;
            this.colFreeTextRate.OptionsColumn.AllowFocus = false;
            this.colFreeTextRate.OptionsColumn.ReadOnly = true;
            this.colFreeTextRate.Visible = true;
            this.colFreeTextRate.VisibleIndex = 16;
            this.colFreeTextRate.Width = 94;
            // 
            // colNetPrice
            // 
            this.colNetPrice.ColumnEdit = this.moneyTextEdit;
            this.colNetPrice.FieldName = "NetPrice";
            this.colNetPrice.Name = "colNetPrice";
            this.colNetPrice.OptionsColumn.AllowEdit = false;
            this.colNetPrice.OptionsColumn.AllowFocus = false;
            this.colNetPrice.OptionsColumn.ReadOnly = true;
            this.colNetPrice.Visible = true;
            this.colNetPrice.VisibleIndex = 17;
            // 
            // colNetCost
            // 
            this.colNetCost.ColumnEdit = this.moneyTextEdit;
            this.colNetCost.FieldName = "NetCost";
            this.colNetCost.Name = "colNetCost";
            this.colNetCost.OptionsColumn.AllowEdit = false;
            this.colNetCost.OptionsColumn.AllowFocus = false;
            this.colNetCost.OptionsColumn.ReadOnly = true;
            this.colNetCost.Visible = true;
            this.colNetCost.VisibleIndex = 18;
            // 
            // colMarkUp
            // 
            this.colMarkUp.DisplayFormat.FormatString = "p";
            this.colMarkUp.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colMarkUp.FieldName = "MarkUp";
            this.colMarkUp.Name = "colMarkUp";
            this.colMarkUp.OptionsColumn.AllowEdit = false;
            this.colMarkUp.OptionsColumn.AllowFocus = false;
            this.colMarkUp.OptionsColumn.ReadOnly = true;
            this.colMarkUp.Visible = true;
            this.colMarkUp.VisibleIndex = 19;
            // 
            // colQuantity
            // 
            this.colQuantity.FieldName = "Quantity";
            this.colQuantity.Name = "colQuantity";
            this.colQuantity.OptionsColumn.AllowEdit = false;
            this.colQuantity.OptionsColumn.AllowFocus = false;
            this.colQuantity.OptionsColumn.ReadOnly = true;
            this.colQuantity.Visible = true;
            this.colQuantity.VisibleIndex = 20;
            // 
            // colItemUnitsID
            // 
            this.colItemUnitsID.FieldName = "ItemUnitsID";
            this.colItemUnitsID.Name = "colItemUnitsID";
            this.colItemUnitsID.OptionsColumn.AllowEdit = false;
            this.colItemUnitsID.OptionsColumn.AllowFocus = false;
            this.colItemUnitsID.OptionsColumn.ReadOnly = true;
            this.colItemUnitsID.Width = 84;
            // 
            // colUnits
            // 
            this.colUnits.FieldName = "Units";
            this.colUnits.Name = "colUnits";
            this.colUnits.OptionsColumn.AllowEdit = false;
            this.colUnits.OptionsColumn.AllowFocus = false;
            this.colUnits.OptionsColumn.ReadOnly = true;
            this.colUnits.Visible = true;
            this.colUnits.VisibleIndex = 21;
            // 
            // colRating
            // 
            this.colRating.FieldName = "Rating";
            this.colRating.Name = "colRating";
            this.colRating.OptionsColumn.AllowEdit = false;
            this.colRating.OptionsColumn.AllowFocus = false;
            this.colRating.OptionsColumn.ReadOnly = true;
            this.colRating.Visible = true;
            this.colRating.VisibleIndex = 26;
            // 
            // colItemCreationDate
            // 
            this.colItemCreationDate.FieldName = "ItemCreationDate";
            this.colItemCreationDate.Name = "colItemCreationDate";
            this.colItemCreationDate.OptionsColumn.AllowEdit = false;
            this.colItemCreationDate.OptionsColumn.AllowFocus = false;
            this.colItemCreationDate.OptionsColumn.ReadOnly = true;
            this.colItemCreationDate.Visible = true;
            this.colItemCreationDate.VisibleIndex = 27;
            this.colItemCreationDate.Width = 113;
            // 
            // colLastUpdatedDate
            // 
            this.colLastUpdatedDate.FieldName = "LastUpdatedDate";
            this.colLastUpdatedDate.Name = "colLastUpdatedDate";
            this.colLastUpdatedDate.OptionsColumn.AllowEdit = false;
            this.colLastUpdatedDate.OptionsColumn.AllowFocus = false;
            this.colLastUpdatedDate.OptionsColumn.ReadOnly = true;
            this.colLastUpdatedDate.Visible = true;
            this.colLastUpdatedDate.VisibleIndex = 28;
            this.colLastUpdatedDate.Width = 111;
            // 
            // colLastUpdatedBy
            // 
            this.colLastUpdatedBy.FieldName = "LastUpdatedBy";
            this.colLastUpdatedBy.Name = "colLastUpdatedBy";
            this.colLastUpdatedBy.OptionsColumn.AllowEdit = false;
            this.colLastUpdatedBy.OptionsColumn.AllowFocus = false;
            this.colLastUpdatedBy.OptionsColumn.ReadOnly = true;
            this.colLastUpdatedBy.Visible = true;
            this.colLastUpdatedBy.VisibleIndex = 29;
            this.colLastUpdatedBy.Width = 100;
            // 
            // colNotes
            // 
            this.colNotes.FieldName = "Notes";
            this.colNotes.Name = "colNotes";
            this.colNotes.OptionsColumn.AllowEdit = false;
            this.colNotes.OptionsColumn.AllowFocus = false;
            this.colNotes.OptionsColumn.ReadOnly = true;
            this.colNotes.Visible = true;
            this.colNotes.VisibleIndex = 30;
            this.colNotes.Width = 264;
            // 
            // colMode
            // 
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.OptionsColumn.AllowEdit = false;
            this.colMode.OptionsColumn.AllowFocus = false;
            this.colMode.OptionsColumn.ReadOnly = true;
            // 
            // colRecordID
            // 
            this.colRecordID.FieldName = "RecordID";
            this.colRecordID.Name = "colRecordID";
            this.colRecordID.OptionsColumn.AllowEdit = false;
            this.colRecordID.OptionsColumn.AllowFocus = false;
            this.colRecordID.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bbiPrintWorkOrder
            // 
            this.bbiPrintWorkOrder.Caption = "Print Work Order";
            this.bbiPrintWorkOrder.Id = 26;
            this.bbiPrintWorkOrder.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.bbiPrintWorkOrder.Name = "bbiPrintWorkOrder";
            this.bbiPrintWorkOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintWorkOrder_ItemClick);
            // 
            // sp07244_UT_Work_Order_Manager_ListTableAdapter
            // 
            this.sp07244_UT_Work_Order_Manager_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupContainerDateRange
            // 
            this.popupContainerDateRange.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.popupContainerDateRange.Caption = "Work Order Date Filter";
            this.popupContainerDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.popupContainerDateRange.EditValue = "No Date Filter";
            this.popupContainerDateRange.EditWidth = 172;
            this.popupContainerDateRange.Id = 28;
            this.popupContainerDateRange.Name = "popupContainerDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Load Work Orders";
            this.bbiRefresh.Id = 29;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter
            // 
            this.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter
            // 
            this.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter
            // 
            this.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07434_UT_Quote_ManagerTableAdapter
            // 
            this.sp07434_UT_Quote_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // dataSetUTQuoteBindingSource
            // 
            this.dataSetUTQuoteBindingSource.DataSource = this.dataSet_UT_Quote;
            this.dataSetUTQuoteBindingSource.Position = 0;
            // 
            // sp07418UTQuoteListBindingSource
            // 
            this.sp07418UTQuoteListBindingSource.DataMember = "sp07418_UT_Quote_List";
            this.sp07418UTQuoteListBindingSource.DataSource = this.dataSetUTQuoteBindingSource;
            // 
            // sp07418_UT_Quote_ListTableAdapter
            // 
            this.sp07418_UT_Quote_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07448_UT_POTransactionTableAdapter
            // 
            this.sp07448_UT_POTransactionTableAdapter.ClearBeforeFill = true;
            // 
            // sp07454_UT_POLineTableAdapter
            // 
            this.sp07454_UT_POLineTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.quotesGridControl;
            // 
            // sp07448_UT_POHeaderTransactionTableAdapter
            // 
            this.sp07448_UT_POHeaderTransactionTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_WorkOrder_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1193, 683);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_WorkOrder_Manager";
            this.Text = "Utilities - Work Order Manager";
            this.Activated += new System.EventHandler(this.frm_UT_WorkOrder_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_WorkOrder_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_WorkOrder_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07244UTWorkOrderManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageLinkedActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07245UTWorkOrderManagerLinkedActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            this.xtraTabPageLinkedSubContractors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.xtraTabPageLinkedMaps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07270UTWorkOrderManagerLinkedMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.xtraTabPageLinkedQuotes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quotesGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07434UTQuoteManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Quote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneyTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetUTQuoteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07418UTQuoteListBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedActions;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_AT dataSet_AT;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraBars.BarButtonItem bbiPrintWorkOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp07244UTWorkOrderManagerListBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07244_UT_Work_Order_Manager_ListTableAdapter sp07244_UT_Work_Order_Manager_ListTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem popupContainerDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private System.Windows.Forms.BindingSource sp07245UTWorkOrderManagerLinkedActionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DataSet_UT_WorkOrderTableAdapters.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDateSubContractorSent;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedMaps;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedSubContractors;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private System.Windows.Forms.BindingSource sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID2;
        private DataSet_UT_WorkOrderTableAdapters.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter;
        private System.Windows.Forms.BindingSource sp07270UTWorkOrderManagerLinkedMapsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMap;
        private DevExpress.XtraGrid.Columns.GridColumn colMapDateTimeCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colMapDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTakenByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID3;
        private DataSet_UT_WorkOrderTableAdapters.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditMap;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderPDF;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLastStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentNone;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentEcoPlugs;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentPaint;
        private DevExpress.XtraGrid.Columns.GridColumn colStumpTreatmentSpraying;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementNone;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementStandards;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReplacementWhips;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEstimatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colPossibleLiveWork;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedQuotes;
        private DevExpress.XtraGrid.GridControl quotesGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView quotesGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private System.Windows.Forms.BindingSource sp07434UTQuoteManagerBindingSource;
        private DataSet_UT_Quote dataSet_UT_Quote;
        private DataSet_UT_QuoteTableAdapters.sp07434_UT_Quote_ManagerTableAdapter sp07434_UT_Quote_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteID;
        private DevExpress.XtraGrid.Columns.GridColumn colReference;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID4;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID3;
        private DevExpress.XtraGrid.Columns.GridColumn colIsMainQuote;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredExchequer;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteItemID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colQuoteCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRateVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRate;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRateVAT;
        private DevExpress.XtraGrid.Columns.GridColumn colSellRateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRate;
        private DevExpress.XtraGrid.Columns.GridColumn colBuyRateTotal;
        private DevExpress.XtraGrid.Columns.GridColumn colFreeTextRate;
        private DevExpress.XtraGrid.Columns.GridColumn colNetPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colNetCost;
        private DevExpress.XtraGrid.Columns.GridColumn colMarkUp;
        private DevExpress.XtraGrid.Columns.GridColumn colQuantity;
        private DevExpress.XtraGrid.Columns.GridColumn colItemUnitsID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colRating;
        private DevExpress.XtraGrid.Columns.GridColumn colItemCreationDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdatedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBillingCentreCodeID;
        private System.Windows.Forms.BindingSource dataSetUTQuoteBindingSource;
        private System.Windows.Forms.BindingSource sp07418UTQuoteListBindingSource;
        private DataSet_UT_QuoteTableAdapters.sp07418_UT_Quote_ListTableAdapter sp07418_UT_Quote_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAnalysisCodeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAnalysisCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit moneyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colRatingID;
        private DataSet_UT_QuoteTableAdapters.sp07448_UT_POTransactionTableAdapter sp07448_UT_POTransactionTableAdapter;
        private DataSet_UT_QuoteTableAdapters.sp07454_UT_POLineTableAdapter sp07454_UT_POLineTableAdapter;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHoldCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
        private DataSet_UT_QuoteTableAdapters.sp07448_UT_POHeaderTransactionTableAdapter sp07448_UT_POHeaderTransactionTableAdapter;
    }
}

namespace WoodPlan5
{
    partial class frm_UT_Circuit_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Circuit_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.CreatedByStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp07004UTCircuitItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_Edit = new WoodPlan5.DataSet_UT_Edit();
            this.PrimaryNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PrimaryIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FeederIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubAreaNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.SubAreaIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VoltageIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07007UTVoltagesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp07006UTCircuitStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ClientIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.CircuitIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CircuitNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ClientNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CoordinatePairsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.LatLongPairsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.FeederNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.RegionNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForCircuitID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrimaryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreatedByStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClientName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForVoltageID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCoordinatePairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLatLongPairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPrimaryName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCircuitName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFeederName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp07004_UT_Circuit_ItemTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07004_UT_Circuit_ItemTableAdapter();
            this.sp07006_UT_Circuit_StatusTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter();
            this.sp07007_UT_Voltages_With_BlankTableAdapter = new WoodPlan5.DataSet_UT_EditTableAdapters.sp07007_UT_Voltages_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07004UTCircuitItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoltageIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07007UTVoltagesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoordinatePairsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongPairsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVoltageID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoordinatePairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongPairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID2
            // 
            this.colID2.Caption = "Voltage ID";
            this.colID2.FieldName = "ID";
            this.colID2.Name = "colID2";
            this.colID2.OptionsColumn.AllowEdit = false;
            this.colID2.OptionsColumn.AllowFocus = false;
            this.colID2.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.Glyph")));
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CreatedByStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimaryNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PrimaryIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubAreaNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.SubAreaIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VoltageIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.CircuitIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CircuitNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CoordinatePairsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.LatLongPairsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.FeederNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp07004UTCircuitItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCircuitID,
            this.ItemForClientID,
            this.ItemForRegionID,
            this.ItemForSubAreaID,
            this.ItemForFeederID,
            this.ItemForPrimaryID,
            this.ItemForCreatedByStaffID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1247, 209, 250, 350);
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CreatedByStaffIDTextEdit
            // 
            this.CreatedByStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "CreatedByStaffID", true));
            this.CreatedByStaffIDTextEdit.Location = new System.Drawing.Point(114, 117);
            this.CreatedByStaffIDTextEdit.MenuManager = this.barManager1;
            this.CreatedByStaffIDTextEdit.Name = "CreatedByStaffIDTextEdit";
            this.CreatedByStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CreatedByStaffIDTextEdit, true);
            this.CreatedByStaffIDTextEdit.Size = new System.Drawing.Size(485, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CreatedByStaffIDTextEdit, optionsSpelling1);
            this.CreatedByStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CreatedByStaffIDTextEdit.TabIndex = 36;
            // 
            // sp07004UTCircuitItemBindingSource
            // 
            this.sp07004UTCircuitItemBindingSource.DataMember = "sp07004_UT_Circuit_Item";
            this.sp07004UTCircuitItemBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // dataSet_UT_Edit
            // 
            this.dataSet_UT_Edit.DataSetName = "DataSet_UT_Edit";
            this.dataSet_UT_Edit.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PrimaryNameButtonEdit
            // 
            this.PrimaryNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "PrimaryName", true));
            this.PrimaryNameButtonEdit.Location = new System.Drawing.Point(98, 107);
            this.PrimaryNameButtonEdit.MenuManager = this.barManager1;
            this.PrimaryNameButtonEdit.Name = "PrimaryNameButtonEdit";
            this.PrimaryNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click to open Select Primary screen", "choose", null, true)});
            this.PrimaryNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.PrimaryNameButtonEdit.Size = new System.Drawing.Size(501, 20);
            this.PrimaryNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.PrimaryNameButtonEdit.TabIndex = 35;
            this.PrimaryNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.PrimaryNameButtonEdit_ButtonClick);
            // 
            // PrimaryIDTextEdit
            // 
            this.PrimaryIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "PrimaryID", true));
            this.PrimaryIDTextEdit.Location = new System.Drawing.Point(98, 131);
            this.PrimaryIDTextEdit.MenuManager = this.barManager1;
            this.PrimaryIDTextEdit.Name = "PrimaryIDTextEdit";
            this.PrimaryIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PrimaryIDTextEdit, true);
            this.PrimaryIDTextEdit.Size = new System.Drawing.Size(518, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PrimaryIDTextEdit, optionsSpelling2);
            this.PrimaryIDTextEdit.StyleController = this.dataLayoutControl1;
            this.PrimaryIDTextEdit.TabIndex = 34;
            // 
            // FeederIDTextEdit
            // 
            this.FeederIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "FeederID", true));
            this.FeederIDTextEdit.Location = new System.Drawing.Point(122, 210);
            this.FeederIDTextEdit.MenuManager = this.barManager1;
            this.FeederIDTextEdit.Name = "FeederIDTextEdit";
            this.FeederIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FeederIDTextEdit, true);
            this.FeederIDTextEdit.Size = new System.Drawing.Size(470, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FeederIDTextEdit, optionsSpelling3);
            this.FeederIDTextEdit.StyleController = this.dataLayoutControl1;
            this.FeederIDTextEdit.TabIndex = 33;
            // 
            // SubAreaNameButtonEdit
            // 
            this.SubAreaNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "SubAreaName", true));
            this.SubAreaNameButtonEdit.Location = new System.Drawing.Point(98, 83);
            this.SubAreaNameButtonEdit.MenuManager = this.barManager1;
            this.SubAreaNameButtonEdit.Name = "SubAreaNameButtonEdit";
            this.SubAreaNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click to open Select Sub-Area screen", "choose", null, true)});
            this.SubAreaNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SubAreaNameButtonEdit.Size = new System.Drawing.Size(501, 20);
            this.SubAreaNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaNameButtonEdit.TabIndex = 32;
            this.SubAreaNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SubAreaNameButtonEdit_ButtonClick);
            // 
            // SubAreaIDTextEdit
            // 
            this.SubAreaIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "SubAreaID", true));
            this.SubAreaIDTextEdit.Location = new System.Drawing.Point(122, 234);
            this.SubAreaIDTextEdit.MenuManager = this.barManager1;
            this.SubAreaIDTextEdit.Name = "SubAreaIDTextEdit";
            this.SubAreaIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubAreaIDTextEdit, true);
            this.SubAreaIDTextEdit.Size = new System.Drawing.Size(470, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubAreaIDTextEdit, optionsSpelling4);
            this.SubAreaIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubAreaIDTextEdit.TabIndex = 31;
            // 
            // RegionIDTextEdit
            // 
            this.RegionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "RegionID", true));
            this.RegionIDTextEdit.Location = new System.Drawing.Point(122, 284);
            this.RegionIDTextEdit.MenuManager = this.barManager1;
            this.RegionIDTextEdit.Name = "RegionIDTextEdit";
            this.RegionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionIDTextEdit, true);
            this.RegionIDTextEdit.Size = new System.Drawing.Size(470, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionIDTextEdit, optionsSpelling5);
            this.RegionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionIDTextEdit.TabIndex = 30;
            // 
            // VoltageIDGridLookUpEdit
            // 
            this.VoltageIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "VoltageID", true));
            this.VoltageIDGridLookUpEdit.Location = new System.Drawing.Point(122, 316);
            this.VoltageIDGridLookUpEdit.MenuManager = this.barManager1;
            this.VoltageIDGridLookUpEdit.Name = "VoltageIDGridLookUpEdit";
            this.VoltageIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("VoltageIDGridLookUpEdit.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Edit Underlying Data", "edit", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("VoltageIDGridLookUpEdit.Properties.Buttons1"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Reload Underlying Data", "reload", null, true)});
            this.VoltageIDGridLookUpEdit.Properties.DataSource = this.sp07007UTVoltagesWithBlankBindingSource;
            this.VoltageIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.VoltageIDGridLookUpEdit.Properties.NullText = "";
            this.VoltageIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.VoltageIDGridLookUpEdit.Properties.View = this.gridView4;
            this.VoltageIDGridLookUpEdit.Size = new System.Drawing.Size(453, 22);
            this.VoltageIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.VoltageIDGridLookUpEdit.TabIndex = 27;
            this.VoltageIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.VoltageIDGridLookUpEdit_ButtonClick);
            // 
            // sp07007UTVoltagesWithBlankBindingSource
            // 
            this.sp07007UTVoltagesWithBlankBindingSource.DataMember = "sp07007_UT_Voltages_With_Blank";
            this.sp07007UTVoltagesWithBlankBindingSource.DataSource = this.dataSet_UT_Edit;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID2,
            this.colDescription2,
            this.colRecordOrder});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID2;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Voltage";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 201;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.EditValue = "";
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(122, 292);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.sp07006UTCircuitStatusBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Properties.View = this.gridView1;
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(453, 20);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 26;
            // 
            // sp07006UTCircuitStatusBindingSource
            // 
            this.sp07006UTCircuitStatusBindingSource.DataMember = "sp07006_UT_Circuit_Status";
            this.sp07006UTCircuitStatusBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription1,
            this.colItemOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colID1
            // 
            this.colID1.Caption = "Status ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Status";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 174;
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // ClientIDTextEdit
            // 
            this.ClientIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "ClientID", true));
            this.ClientIDTextEdit.Location = new System.Drawing.Point(127, 93);
            this.ClientIDTextEdit.MenuManager = this.barManager1;
            this.ClientIDTextEdit.Name = "ClientIDTextEdit";
            this.ClientIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientIDTextEdit, true);
            this.ClientIDTextEdit.Size = new System.Drawing.Size(472, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientIDTextEdit, optionsSpelling6);
            this.ClientIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientIDTextEdit.TabIndex = 25;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp07004UTCircuitItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(98, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // CircuitIDTextEdit
            // 
            this.CircuitIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "CircuitID", true));
            this.CircuitIDTextEdit.Location = new System.Drawing.Point(127, 107);
            this.CircuitIDTextEdit.MenuManager = this.barManager1;
            this.CircuitIDTextEdit.Name = "CircuitIDTextEdit";
            this.CircuitIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitIDTextEdit, true);
            this.CircuitIDTextEdit.Size = new System.Drawing.Size(472, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitIDTextEdit, optionsSpelling7);
            this.CircuitIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitIDTextEdit.TabIndex = 4;
            // 
            // CircuitNumberTextEdit
            // 
            this.CircuitNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "CircuitNumber", true));
            this.CircuitNumberTextEdit.Location = new System.Drawing.Point(98, 189);
            this.CircuitNumberTextEdit.MenuManager = this.barManager1;
            this.CircuitNumberTextEdit.Name = "CircuitNumberTextEdit";
            this.CircuitNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNumberTextEdit, true);
            this.CircuitNumberTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNumberTextEdit, optionsSpelling8);
            this.CircuitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNumberTextEdit.TabIndex = 6;
            // 
            // CircuitNameTextEdit
            // 
            this.CircuitNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "CircuitName", true));
            this.CircuitNameTextEdit.Location = new System.Drawing.Point(98, 165);
            this.CircuitNameTextEdit.MenuManager = this.barManager1;
            this.CircuitNameTextEdit.Name = "CircuitNameTextEdit";
            this.CircuitNameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CircuitNameTextEdit, true);
            this.CircuitNameTextEdit.Size = new System.Drawing.Size(501, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CircuitNameTextEdit, optionsSpelling9);
            this.CircuitNameTextEdit.StyleController = this.dataLayoutControl1;
            this.CircuitNameTextEdit.TabIndex = 7;
            this.CircuitNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.CircuitNameTextEdit_Validating);
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 292);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(539, 216);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling10);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 18;
            this.strRemarksMemoEdit.TabStop = false;
            // 
            // ClientNameButtonEdit
            // 
            this.ClientNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "ClientName", true));
            this.ClientNameButtonEdit.EditValue = "";
            this.ClientNameButtonEdit.Location = new System.Drawing.Point(98, 35);
            this.ClientNameButtonEdit.MenuManager = this.barManager1;
            this.ClientNameButtonEdit.Name = "ClientNameButtonEdit";
            this.ClientNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "Click to open Select Client screen", "choose", null, true)});
            this.ClientNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ClientNameButtonEdit.Size = new System.Drawing.Size(501, 20);
            this.ClientNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameButtonEdit.TabIndex = 5;
            this.ClientNameButtonEdit.TabStop = false;
            this.ClientNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ClientNameButtonEdit_ButtonClick);
            this.ClientNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ClientNameButtonEdit_Validating);
            // 
            // CoordinatePairsMemoEdit
            // 
            this.CoordinatePairsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "CoordinatePairs", true));
            this.CoordinatePairsMemoEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.CoordinatePairsMemoEdit.Location = new System.Drawing.Point(122, 342);
            this.CoordinatePairsMemoEdit.MenuManager = this.barManager1;
            this.CoordinatePairsMemoEdit.Name = "CoordinatePairsMemoEdit";
            this.CoordinatePairsMemoEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CoordinatePairsMemoEdit, true);
            this.CoordinatePairsMemoEdit.Size = new System.Drawing.Size(453, 81);
            this.scSpellChecker.SetSpellCheckerOptions(this.CoordinatePairsMemoEdit, optionsSpelling11);
            this.CoordinatePairsMemoEdit.StyleController = this.dataLayoutControl1;
            this.CoordinatePairsMemoEdit.TabIndex = 19;
            // 
            // LatLongPairsMemoEdit
            // 
            this.LatLongPairsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "LatLongPairs", true));
            this.LatLongPairsMemoEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.LatLongPairsMemoEdit.Location = new System.Drawing.Point(122, 427);
            this.LatLongPairsMemoEdit.MenuManager = this.barManager1;
            this.LatLongPairsMemoEdit.Name = "LatLongPairsMemoEdit";
            this.LatLongPairsMemoEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LatLongPairsMemoEdit, true);
            this.LatLongPairsMemoEdit.Size = new System.Drawing.Size(453, 81);
            this.scSpellChecker.SetSpellCheckerOptions(this.LatLongPairsMemoEdit, optionsSpelling12);
            this.LatLongPairsMemoEdit.StyleController = this.dataLayoutControl1;
            this.LatLongPairsMemoEdit.TabIndex = 20;
            // 
            // FeederNameButtonEdit
            // 
            this.FeederNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "FeederName", true));
            this.FeederNameButtonEdit.Location = new System.Drawing.Point(98, 131);
            this.FeederNameButtonEdit.MenuManager = this.barManager1;
            this.FeederNameButtonEdit.Name = "FeederNameButtonEdit";
            this.FeederNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "Click to open Select Feeder screen", "choose", null, true)});
            this.FeederNameButtonEdit.Properties.MaxLength = 50;
            this.FeederNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.FeederNameButtonEdit.Size = new System.Drawing.Size(501, 20);
            this.FeederNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.FeederNameButtonEdit.TabIndex = 28;
            this.FeederNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.FeederNameButtonEdit_ButtonClick);
            // 
            // RegionNameButtonEdit
            // 
            this.RegionNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp07004UTCircuitItemBindingSource, "RegionName", true));
            this.RegionNameButtonEdit.Location = new System.Drawing.Point(98, 59);
            this.RegionNameButtonEdit.MenuManager = this.barManager1;
            this.RegionNameButtonEdit.Name = "RegionNameButtonEdit";
            this.RegionNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "Click to open Select Region screen", "choose", null, true)});
            this.RegionNameButtonEdit.Properties.MaxLength = 50;
            this.RegionNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.RegionNameButtonEdit.Size = new System.Drawing.Size(501, 20);
            this.RegionNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameButtonEdit.TabIndex = 29;
            this.RegionNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.RegionNameButtonEdit_ButtonClick);
            // 
            // ItemForCircuitID
            // 
            this.ItemForCircuitID.Control = this.CircuitIDTextEdit;
            this.ItemForCircuitID.CustomizationFormText = "Circuit ID:";
            this.ItemForCircuitID.Location = new System.Drawing.Point(0, 95);
            this.ItemForCircuitID.Name = "ItemForCircuitID";
            this.ItemForCircuitID.Size = new System.Drawing.Size(591, 24);
            this.ItemForCircuitID.Text = "Circuit ID:";
            this.ItemForCircuitID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForClientID
            // 
            this.ItemForClientID.Control = this.ClientIDTextEdit;
            this.ItemForClientID.CustomizationFormText = "Client ID:";
            this.ItemForClientID.Location = new System.Drawing.Point(0, 95);
            this.ItemForClientID.Name = "ItemForClientID";
            this.ItemForClientID.Size = new System.Drawing.Size(591, 24);
            this.ItemForClientID.Text = "Client ID:";
            this.ItemForClientID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRegionID
            // 
            this.ItemForRegionID.Control = this.RegionIDTextEdit;
            this.ItemForRegionID.CustomizationFormText = "Region ID:";
            this.ItemForRegionID.Location = new System.Drawing.Point(0, 74);
            this.ItemForRegionID.Name = "ItemForRegionID";
            this.ItemForRegionID.Size = new System.Drawing.Size(560, 24);
            this.ItemForRegionID.Text = "Region ID:";
            this.ItemForRegionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSubAreaID
            // 
            this.ItemForSubAreaID.Control = this.SubAreaIDTextEdit;
            this.ItemForSubAreaID.CustomizationFormText = "Sub-Area ID:";
            this.ItemForSubAreaID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSubAreaID.Name = "ItemForSubAreaID";
            this.ItemForSubAreaID.Size = new System.Drawing.Size(560, 24);
            this.ItemForSubAreaID.Text = "Sub-Area ID:";
            this.ItemForSubAreaID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForFeederID
            // 
            this.ItemForFeederID.Control = this.FeederIDTextEdit;
            this.ItemForFeederID.CustomizationFormText = "Feeder ID:";
            this.ItemForFeederID.Location = new System.Drawing.Point(0, 0);
            this.ItemForFeederID.Name = "ItemForFeederID";
            this.ItemForFeederID.Size = new System.Drawing.Size(560, 24);
            this.ItemForFeederID.Text = "Feeder ID:";
            this.ItemForFeederID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForPrimaryID
            // 
            this.ItemForPrimaryID.Control = this.PrimaryIDTextEdit;
            this.ItemForPrimaryID.CustomizationFormText = "Primary ID:";
            this.ItemForPrimaryID.Location = new System.Drawing.Point(0, 119);
            this.ItemForPrimaryID.Name = "ItemForPrimaryID";
            this.ItemForPrimaryID.Size = new System.Drawing.Size(608, 24);
            this.ItemForPrimaryID.Text = "Primary ID:";
            this.ItemForPrimaryID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCreatedByStaffID
            // 
            this.ItemForCreatedByStaffID.Control = this.CreatedByStaffIDTextEdit;
            this.ItemForCreatedByStaffID.Location = new System.Drawing.Point(0, 201);
            this.ItemForCreatedByStaffID.Name = "ItemForCreatedByStaffID";
            this.ItemForCreatedByStaffID.Size = new System.Drawing.Size(591, 24);
            this.ItemForCreatedByStaffID.Text = "Created By Staff ID:";
            this.ItemForCreatedByStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(611, 564);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClientName,
            this.ItemForCircuitNumber,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.emptySpaceItem5,
            this.ItemForRegionName,
            this.ItemForSubAreaName,
            this.emptySpaceItem6,
            this.ItemForPrimaryName,
            this.ItemForCircuitName,
            this.ItemForFeederName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(591, 534);
            // 
            // ItemForClientName
            // 
            this.ItemForClientName.AllowHide = false;
            this.ItemForClientName.Control = this.ClientNameButtonEdit;
            this.ItemForClientName.CustomizationFormText = "Linked to Client:";
            this.ItemForClientName.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientName.Name = "ItemForClientName";
            this.ItemForClientName.Size = new System.Drawing.Size(591, 24);
            this.ItemForClientName.Text = "Linked to Client:";
            this.ItemForClientName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForCircuitNumber
            // 
            this.ItemForCircuitNumber.AllowHide = false;
            this.ItemForCircuitNumber.Control = this.CircuitNumberTextEdit;
            this.ItemForCircuitNumber.CustomizationFormText = "Circuit Number:";
            this.ItemForCircuitNumber.Location = new System.Drawing.Point(0, 177);
            this.ItemForCircuitNumber.Name = "ItemForCircuitNumber";
            this.ItemForCircuitNumber.Size = new System.Drawing.Size(591, 24);
            this.ItemForCircuitNumber.Text = "Circuit Number:";
            this.ItemForCircuitNumber.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(86, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(86, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(86, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(263, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(328, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(86, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 201);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 211);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(591, 313);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup5;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(567, 268);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVoltageID,
            this.ItemForCoordinatePairs,
            this.ItemForLatLongPairs,
            this.ItemForStatusID});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(543, 220);
            this.layoutControlGroup5.Text = "Details";
            // 
            // ItemForVoltageID
            // 
            this.ItemForVoltageID.Control = this.VoltageIDGridLookUpEdit;
            this.ItemForVoltageID.CustomizationFormText = "Voltage:";
            this.ItemForVoltageID.Location = new System.Drawing.Point(0, 24);
            this.ItemForVoltageID.Name = "ItemForVoltageID";
            this.ItemForVoltageID.Size = new System.Drawing.Size(543, 26);
            this.ItemForVoltageID.Text = "Voltage:";
            this.ItemForVoltageID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForCoordinatePairs
            // 
            this.ItemForCoordinatePairs.Control = this.CoordinatePairsMemoEdit;
            this.ItemForCoordinatePairs.CustomizationFormText = "X Coordinate:";
            this.ItemForCoordinatePairs.Location = new System.Drawing.Point(0, 50);
            this.ItemForCoordinatePairs.MaxSize = new System.Drawing.Size(0, 85);
            this.ItemForCoordinatePairs.MinSize = new System.Drawing.Size(100, 85);
            this.ItemForCoordinatePairs.Name = "ItemForCoordinatePairs";
            this.ItemForCoordinatePairs.Size = new System.Drawing.Size(543, 85);
            this.ItemForCoordinatePairs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCoordinatePairs.Text = "Coordinate Pairs:";
            this.ItemForCoordinatePairs.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForLatLongPairs
            // 
            this.ItemForLatLongPairs.Control = this.LatLongPairsMemoEdit;
            this.ItemForLatLongPairs.CustomizationFormText = "Y Coordinate:";
            this.ItemForLatLongPairs.Location = new System.Drawing.Point(0, 135);
            this.ItemForLatLongPairs.MaxSize = new System.Drawing.Size(0, 85);
            this.ItemForLatLongPairs.MinSize = new System.Drawing.Size(100, 85);
            this.ItemForLatLongPairs.Name = "ItemForLatLongPairs";
            this.ItemForLatLongPairs.Size = new System.Drawing.Size(543, 85);
            this.ItemForLatLongPairs.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForLatLongPairs.Text = "Lat \\ Long Pairs:";
            this.ItemForLatLongPairs.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 0);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(543, 24);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(543, 220);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(543, 220);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 524);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameButtonEdit;
            this.ItemForRegionName.CustomizationFormText = "Region:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 47);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(591, 24);
            this.ItemForRegionName.Text = "Region:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForSubAreaName
            // 
            this.ItemForSubAreaName.Control = this.SubAreaNameButtonEdit;
            this.ItemForSubAreaName.CustomizationFormText = "Sub-Area:";
            this.ItemForSubAreaName.Location = new System.Drawing.Point(0, 71);
            this.ItemForSubAreaName.Name = "ItemForSubAreaName";
            this.ItemForSubAreaName.Size = new System.Drawing.Size(591, 24);
            this.ItemForSubAreaName.Text = "Sub-Area:";
            this.ItemForSubAreaName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPrimaryName
            // 
            this.ItemForPrimaryName.Control = this.PrimaryNameButtonEdit;
            this.ItemForPrimaryName.CustomizationFormText = "Primary Name:";
            this.ItemForPrimaryName.Location = new System.Drawing.Point(0, 95);
            this.ItemForPrimaryName.Name = "ItemForPrimaryName";
            this.ItemForPrimaryName.Size = new System.Drawing.Size(591, 24);
            this.ItemForPrimaryName.Text = "Primary Name:";
            this.ItemForPrimaryName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForCircuitName
            // 
            this.ItemForCircuitName.AllowHide = false;
            this.ItemForCircuitName.Control = this.CircuitNameTextEdit;
            this.ItemForCircuitName.CustomizationFormText = "Circuit Name:";
            this.ItemForCircuitName.Location = new System.Drawing.Point(0, 153);
            this.ItemForCircuitName.Name = "ItemForCircuitName";
            this.ItemForCircuitName.Size = new System.Drawing.Size(591, 24);
            this.ItemForCircuitName.Text = "Circuit Name:";
            this.ItemForCircuitName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForFeederName
            // 
            this.ItemForFeederName.Control = this.FeederNameButtonEdit;
            this.ItemForFeederName.CustomizationFormText = "Feeder:";
            this.ItemForFeederName.Location = new System.Drawing.Point(0, 119);
            this.ItemForFeederName.Name = "ItemForFeederName";
            this.ItemForFeederName.Size = new System.Drawing.Size(591, 24);
            this.ItemForFeederName.Text = "Feeder:";
            this.ItemForFeederName.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 534);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(591, 10);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(591, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07004_UT_Circuit_ItemTableAdapter
            // 
            this.sp07004_UT_Circuit_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp07006_UT_Circuit_StatusTableAdapter
            // 
            this.sp07006_UT_Circuit_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // sp07007_UT_Voltages_With_BlankTableAdapter
            // 
            this.sp07007_UT_Voltages_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Circuit_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Circuit_Edit";
            this.Text = "Edit Circuit";
            this.Activated += new System.EventHandler(this.frm_UT_Circuit_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Circuit_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Circuit_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CreatedByStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07004UTCircuitItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrimaryIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubAreaIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VoltageIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07007UTVoltagesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07006UTCircuitStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CircuitNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoordinatePairsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LatLongPairsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeederNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreatedByStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVoltageID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCoordinatePairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLatLongPairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrimaryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCircuitName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFeederName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit CircuitIDTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit CircuitNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit CircuitNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCircuitName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCoordinatePairs;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLatLongPairs;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp07004UTCircuitItemBindingSource;
        private DataSet_UT_Edit dataSet_UT_Edit;
        private DataSet_UT_EditTableAdapters.sp07004_UT_Circuit_ItemTableAdapter sp07004_UT_Circuit_ItemTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit ClientNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit ClientIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientID;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private DataSet_UT dataSet_UT;
        private System.Windows.Forms.BindingSource sp07006UTCircuitStatusBindingSource;
        private DataSet_UTTableAdapters.sp07006_UT_Circuit_StatusTableAdapter sp07006_UT_Circuit_StatusTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraEditors.GridLookUpEdit VoltageIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVoltageID;
        private System.Windows.Forms.BindingSource sp07007UTVoltagesWithBlankBindingSource;
        private DataSet_UT_EditTableAdapters.sp07007_UT_Voltages_With_BlankTableAdapter sp07007_UT_Voltages_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.MemoEdit CoordinatePairsMemoEdit;
        private DevExpress.XtraEditors.MemoEdit LatLongPairsMemoEdit;
        private DevExpress.XtraEditors.TextEdit RegionIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionID;
        private DevExpress.XtraEditors.ButtonEdit FeederNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit SubAreaIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaID;
        private DevExpress.XtraEditors.ButtonEdit SubAreaNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubAreaName;
        private DevExpress.XtraEditors.ButtonEdit RegionNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit FeederIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFeederID;
        private DevExpress.XtraEditors.TextEdit PrimaryIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimaryID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.ButtonEdit PrimaryNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrimaryName;
        private DevExpress.XtraEditors.TextEdit CreatedByStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreatedByStaffID;
    }
}

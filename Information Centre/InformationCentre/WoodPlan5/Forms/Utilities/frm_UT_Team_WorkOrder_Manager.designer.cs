namespace WoodPlan5
{
    partial class frm_UT_Team_WorkOrder_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Team_WorkOrder_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07290UTTeamWorkOrderManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateSubContractorSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCompleted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalCost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedActionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderPDF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHoldCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlTeams = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp07289UTTeamWorkOrderTeamFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalTeam = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobileTel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnTeamFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLinkedActions = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colActionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderSortOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTreeReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeldUpType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsShutdownRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageLinkedSubContractors = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkOrderTeamID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMobileTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaddedWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedMaps = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMapID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkOrderID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMap = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditMap = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colMapDateTimeCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMapDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMapOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureTakenByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPictureRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPaddedWorkOrderID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkRiskAssessments = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl22 = new DevExpress.XtraGrid.GridControl();
            this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskAssessmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurveyedPoleID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colPersonCompletingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermissionChecked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colElectricalAssessmentVerified = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorkingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPermitHolderName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNRSWAOpeningNoticeNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit21 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastUpdated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElectricalRiskCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOfWorking = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonCompletingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMEWPArialRescuers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyKitLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAirAmbulanceLong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBanksMan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmergencyServicesMeetPoint = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobilePhoneSignalBars = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestAandE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNearestLandLine = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl23 = new DevExpress.XtraGrid.GridControl();
            this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRiskQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit22 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colQuestionNotes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQuestionOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMasterQuestionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentDateTimeRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colRiskAssessmentType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnswerText3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRiskAssessmentTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp07244UTWorkOrderManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT_WorkOrders = new WoodPlan5.DataSet_AT_WorkOrders();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.bbiPrintWorkOrder = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.popupTeamFilter = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditTeam = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter();
            this.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter();
            this.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter();
            this.sp07290_UT_Team_Work_Order_Manager_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07290_UT_Team_Work_Order_Manager_ListTableAdapter();
            this.sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter();
            this.sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter();
            this.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending22 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending23 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07290UTTeamWorkOrderManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTeams)).BeginInit();
            this.popupContainerControlTeams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07289UTTeamWorkOrderTeamFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageLinkedActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07245UTWorkOrderManagerLinkedActionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            this.xtraTabPageLinkedSubContractors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.xtraTabPageLinkedMaps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07270UTWorkOrderManagerLinkedMapsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.xtraTabPageLinkRiskAssessments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07244UTWorkOrderManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditTeam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrintWorkOrder, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1094, 34);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 682);
            this.barDockControlBottom.Size = new System.Drawing.Size(1094, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 34);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 648);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1094, 34);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 648);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiPrintWorkOrder,
            this.popupContainerDateRange,
            this.bbiRefresh,
            this.popupTeamFilter});
            this.barManager1.MaxItemId = 32;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEditTeam});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 2;
            this.colDisabled.Width = 61;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 34);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Work Orders";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1094, 648);
            this.splitContainerControl1.SplitterPosition = 237;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlTeams);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1090, 381);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07290UTTeamWorkOrderManagerListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl1.Size = new System.Drawing.Size(1090, 381);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07290UTTeamWorkOrderManagerListBindingSource
            // 
            this.sp07290UTTeamWorkOrderManagerListBindingSource.DataMember = "sp07290_UT_Team_Work_Order_Manager_List";
            this.sp07290UTTeamWorkOrderManagerListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "Refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderID,
            this.colReferenceNumber,
            this.colDateCreated,
            this.colDateSubContractorSent,
            this.colDateCompleted,
            this.colTotalCost,
            this.colRemarks,
            this.colGUID,
            this.colCreatedByStaffID,
            this.colPaddedWorkOrderID1,
            this.colCreatedBy,
            this.colLinkedActionCount,
            this.colWorkOrderPDF,
            this.colStatusID,
            this.colStatusDescription,
            this.colTeamOnHoldCount});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colWorkOrderID
            // 
            this.colWorkOrderID.Caption = "Work Order ID";
            this.colWorkOrderID.FieldName = "WorkOrderID";
            this.colWorkOrderID.Name = "colWorkOrderID";
            this.colWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID.Width = 91;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 1;
            this.colReferenceNumber.Width = 125;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 3;
            this.colDateCreated.Width = 98;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDateSubContractorSent
            // 
            this.colDateSubContractorSent.Caption = "Contractor Sent Date";
            this.colDateSubContractorSent.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateSubContractorSent.FieldName = "DateSubContractorSent";
            this.colDateSubContractorSent.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateSubContractorSent.Name = "colDateSubContractorSent";
            this.colDateSubContractorSent.OptionsColumn.AllowEdit = false;
            this.colDateSubContractorSent.OptionsColumn.AllowFocus = false;
            this.colDateSubContractorSent.OptionsColumn.ReadOnly = true;
            this.colDateSubContractorSent.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateSubContractorSent.Visible = true;
            this.colDateSubContractorSent.VisibleIndex = 4;
            this.colDateSubContractorSent.Width = 124;
            // 
            // colDateCompleted
            // 
            this.colDateCompleted.Caption = "Date Completed";
            this.colDateCompleted.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateCompleted.FieldName = "DateCompleted";
            this.colDateCompleted.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateCompleted.Name = "colDateCompleted";
            this.colDateCompleted.OptionsColumn.AllowEdit = false;
            this.colDateCompleted.OptionsColumn.AllowFocus = false;
            this.colDateCompleted.OptionsColumn.ReadOnly = true;
            this.colDateCompleted.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateCompleted.Visible = true;
            this.colDateCompleted.VisibleIndex = 5;
            this.colDateCompleted.Width = 98;
            // 
            // colTotalCost
            // 
            this.colTotalCost.Caption = "Total Cost";
            this.colTotalCost.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalCost.FieldName = "TotalCost";
            this.colTotalCost.Name = "colTotalCost";
            this.colTotalCost.OptionsColumn.AllowEdit = false;
            this.colTotalCost.OptionsColumn.AllowFocus = false;
            this.colTotalCost.OptionsColumn.ReadOnly = true;
            this.colTotalCost.Visible = true;
            this.colTotalCost.VisibleIndex = 6;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 11;
            this.colRemarks.Width = 142;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 116;
            // 
            // colPaddedWorkOrderID1
            // 
            this.colPaddedWorkOrderID1.Caption = "Work Order #";
            this.colPaddedWorkOrderID1.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID1.Name = "colPaddedWorkOrderID1";
            this.colPaddedWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID1.Visible = true;
            this.colPaddedWorkOrderID1.VisibleIndex = 0;
            this.colPaddedWorkOrderID1.Width = 101;
            // 
            // colCreatedBy
            // 
            this.colCreatedBy.Caption = "Created By";
            this.colCreatedBy.FieldName = "CreatedBy";
            this.colCreatedBy.Name = "colCreatedBy";
            this.colCreatedBy.OptionsColumn.AllowEdit = false;
            this.colCreatedBy.OptionsColumn.AllowFocus = false;
            this.colCreatedBy.OptionsColumn.ReadOnly = true;
            this.colCreatedBy.Visible = true;
            this.colCreatedBy.VisibleIndex = 7;
            this.colCreatedBy.Width = 116;
            // 
            // colLinkedActionCount
            // 
            this.colLinkedActionCount.Caption = "Linked Actions";
            this.colLinkedActionCount.FieldName = "LinkedActionCount";
            this.colLinkedActionCount.Name = "colLinkedActionCount";
            this.colLinkedActionCount.OptionsColumn.AllowEdit = false;
            this.colLinkedActionCount.OptionsColumn.AllowFocus = false;
            this.colLinkedActionCount.OptionsColumn.ReadOnly = true;
            this.colLinkedActionCount.Visible = true;
            this.colLinkedActionCount.VisibleIndex = 9;
            this.colLinkedActionCount.Width = 89;
            // 
            // colWorkOrderPDF
            // 
            this.colWorkOrderPDF.Caption = "Saved Work Order PDF File";
            this.colWorkOrderPDF.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colWorkOrderPDF.FieldName = "WorkOrderPDF";
            this.colWorkOrderPDF.Name = "colWorkOrderPDF";
            this.colWorkOrderPDF.OptionsColumn.ReadOnly = true;
            this.colWorkOrderPDF.Visible = true;
            this.colWorkOrderPDF.VisibleIndex = 8;
            this.colWorkOrderPDF.Width = 289;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status Description";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 2;
            this.colStatusDescription.Width = 147;
            // 
            // colTeamOnHoldCount
            // 
            this.colTeamOnHoldCount.Caption = "Team On-hold Count";
            this.colTeamOnHoldCount.FieldName = "TeamOnHoldCount";
            this.colTeamOnHoldCount.Name = "colTeamOnHoldCount";
            this.colTeamOnHoldCount.OptionsColumn.AllowEdit = false;
            this.colTeamOnHoldCount.OptionsColumn.AllowFocus = false;
            this.colTeamOnHoldCount.OptionsColumn.ReadOnly = true;
            this.colTeamOnHoldCount.Visible = true;
            this.colTeamOnHoldCount.VisibleIndex = 10;
            this.colTeamOnHoldCount.Width = 118;
            // 
            // popupContainerControlTeams
            // 
            this.popupContainerControlTeams.Controls.Add(this.gridControl6);
            this.popupContainerControlTeams.Controls.Add(this.btnTeamFilterOK);
            this.popupContainerControlTeams.Location = new System.Drawing.Point(196, 27);
            this.popupContainerControlTeams.Name = "popupContainerControlTeams";
            this.popupContainerControlTeams.Size = new System.Drawing.Size(580, 332);
            this.popupContainerControlTeams.TabIndex = 5;
            // 
            // gridControl6
            // 
            this.gridControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl6.DataSource = this.sp07289UTTeamWorkOrderTeamFilterListBindingSource;
            this.gridControl6.Location = new System.Drawing.Point(3, 3);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit2});
            this.gridControl6.Size = new System.Drawing.Size(574, 301);
            this.gridControl6.TabIndex = 3;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp07289UTTeamWorkOrderTeamFilterListBindingSource
            // 
            this.sp07289UTTeamWorkOrderTeamFilterListBindingSource.DataMember = "sp07289_UT_Team_Work_Order_Team_Filter_List";
            this.sp07289UTTeamWorkOrderTeamFilterListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTeamID,
            this.colTeamName,
            this.colInternalTeam,
            this.colMobileTel1,
            this.colTelephone11,
            this.colTelephone21,
            this.colPostcode1,
            this.colDisabled,
            this.colRemarks2});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colDisabled;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 1;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTeamName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTeamID
            // 
            this.colTeamID.Caption = "Team ID";
            this.colTeamID.FieldName = "TeamID";
            this.colTeamID.Name = "colTeamID";
            this.colTeamID.OptionsColumn.AllowEdit = false;
            this.colTeamID.OptionsColumn.AllowFocus = false;
            this.colTeamID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamName
            // 
            this.colTeamName.Caption = "Team Name";
            this.colTeamName.FieldName = "TeamName";
            this.colTeamName.Name = "colTeamName";
            this.colTeamName.OptionsColumn.AllowEdit = false;
            this.colTeamName.OptionsColumn.AllowFocus = false;
            this.colTeamName.OptionsColumn.ReadOnly = true;
            this.colTeamName.Visible = true;
            this.colTeamName.VisibleIndex = 0;
            this.colTeamName.Width = 265;
            // 
            // colInternalTeam
            // 
            this.colInternalTeam.Caption = "Internal";
            this.colInternalTeam.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colInternalTeam.FieldName = "InternalTeam";
            this.colInternalTeam.Name = "colInternalTeam";
            this.colInternalTeam.OptionsColumn.AllowEdit = false;
            this.colInternalTeam.OptionsColumn.AllowFocus = false;
            this.colInternalTeam.OptionsColumn.ReadOnly = true;
            this.colInternalTeam.Visible = true;
            this.colInternalTeam.VisibleIndex = 1;
            this.colInternalTeam.Width = 59;
            // 
            // colMobileTel1
            // 
            this.colMobileTel1.Caption = "Mobile Tel";
            this.colMobileTel1.FieldName = "MobileTel";
            this.colMobileTel1.Name = "colMobileTel1";
            this.colMobileTel1.OptionsColumn.AllowEdit = false;
            this.colMobileTel1.OptionsColumn.AllowFocus = false;
            this.colMobileTel1.OptionsColumn.ReadOnly = true;
            this.colMobileTel1.Visible = true;
            this.colMobileTel1.VisibleIndex = 3;
            this.colMobileTel1.Width = 86;
            // 
            // colTelephone11
            // 
            this.colTelephone11.Caption = "Telephone 1";
            this.colTelephone11.FieldName = "Telephone1";
            this.colTelephone11.Name = "colTelephone11";
            this.colTelephone11.OptionsColumn.AllowEdit = false;
            this.colTelephone11.OptionsColumn.AllowFocus = false;
            this.colTelephone11.OptionsColumn.ReadOnly = true;
            this.colTelephone11.Visible = true;
            this.colTelephone11.VisibleIndex = 4;
            this.colTelephone11.Width = 84;
            // 
            // colTelephone21
            // 
            this.colTelephone21.Caption = "Telephone 2";
            this.colTelephone21.FieldName = "Telephone2";
            this.colTelephone21.Name = "colTelephone21";
            this.colTelephone21.OptionsColumn.AllowEdit = false;
            this.colTelephone21.OptionsColumn.AllowFocus = false;
            this.colTelephone21.OptionsColumn.ReadOnly = true;
            this.colTelephone21.Visible = true;
            this.colTelephone21.VisibleIndex = 5;
            this.colTelephone21.Width = 90;
            // 
            // colPostcode1
            // 
            this.colPostcode1.Caption = "Postcode";
            this.colPostcode1.FieldName = "Postcode";
            this.colPostcode1.Name = "colPostcode1";
            this.colPostcode1.OptionsColumn.AllowEdit = false;
            this.colPostcode1.OptionsColumn.AllowFocus = false;
            this.colPostcode1.OptionsColumn.ReadOnly = true;
            this.colPostcode1.Visible = true;
            this.colPostcode1.VisibleIndex = 6;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 7;
            this.colRemarks2.Width = 118;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // btnTeamFilterOK
            // 
            this.btnTeamFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTeamFilterOK.Location = new System.Drawing.Point(3, 306);
            this.btnTeamFilterOK.Name = "btnTeamFilterOK";
            this.btnTeamFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnTeamFilterOK.TabIndex = 2;
            this.btnTeamFilterOK.Text = "OK";
            this.btnTeamFilterOK.Click += new System.EventHandler(this.btnTeamFilterOK_Click);
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(10, 40);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(180, 109);
            this.popupContainerControlDateRange.TabIndex = 4;
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(75, 23);
            this.btnDateRangeFilterOK.TabIndex = 3;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(174, 76);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Date Range";
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditToDate.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Size = new System.Drawing.Size(129, 20);
            this.dateEditFromDate.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageLinkedActions;
            this.xtraTabControl1.Size = new System.Drawing.Size(1094, 237);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLinkedActions,
            this.xtraTabPageLinkedSubContractors,
            this.xtraTabPageLinkedMaps,
            this.xtraTabPageLinkRiskAssessments,
            this.xtraTabPageLinkedDocuments});
            // 
            // xtraTabPageLinkedActions
            // 
            this.xtraTabPageLinkedActions.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPageLinkedActions.Name = "xtraTabPageLinkedActions";
            this.xtraTabPageLinkedActions.Size = new System.Drawing.Size(1089, 211);
            this.xtraTabPageLinkedActions.Text = "Linked Actions";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1089, 211);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07245UTWorkOrderManagerLinkedActionsBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemCheckEdit1});
            this.gridControl2.Size = new System.Drawing.Size(1089, 211);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07245UTWorkOrderManagerLinkedActionsBindingSource
            // 
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource.DataMember = "sp07245_UT_Work_Order_Manager_Linked_Actions";
            this.sp07245UTWorkOrderManagerLinkedActionsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colActionID,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn24,
            this.gridColumn27,
            this.gridColumn29,
            this.gridColumn31,
            this.colPaddedWorkOrderID,
            this.colWorkOrderSortOrder,
            this.colPoleID,
            this.colCircuitName,
            this.colPoleNumber,
            this.colTreeReferenceNumber,
            this.colLastStartTime,
            this.colTotalTimeTaken,
            this.colTeamRemarks,
            this.colActionStatus,
            this.colActionStatusID,
            this.colTeamOnHold,
            this.colHeldUpType,
            this.colIsShutdownRequired});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWorkOrderSortOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colActionID
            // 
            this.colActionID.Caption = "Action ID";
            this.colActionID.FieldName = "ActionID";
            this.colActionID.Name = "colActionID";
            this.colActionID.OptionsColumn.AllowEdit = false;
            this.colActionID.OptionsColumn.AllowFocus = false;
            this.colActionID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Surveyed Tree ID";
            this.gridColumn2.FieldName = "SurveyedTreeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 106;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Job Type ID";
            this.gridColumn3.FieldName = "JobTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 79;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Reference Number";
            this.gridColumn4.FieldName = "ReferenceNumber";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 5;
            this.gridColumn4.Width = 112;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Date Raised";
            this.gridColumn5.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn5.FieldName = "DateRaised";
            this.gridColumn5.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.gridColumn5.Width = 99;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Date Scheduled";
            this.gridColumn6.FieldName = "DateScheduled";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 6;
            this.gridColumn6.Width = 96;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Date Completed";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.gridColumn7.FieldName = "DateCompleted";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 8;
            this.gridColumn7.Width = 98;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Approved";
            this.gridColumn8.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gridColumn8.FieldName = "Approved";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 9;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Apporved By Staff ID";
            this.gridColumn9.FieldName = "ApprovedByStaffID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 124;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Estimated PZ";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn10.FieldName = "EstimatedPZHours";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 15;
            this.gridColumn10.Width = 83;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Actual PZ";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn11.FieldName = "ActualPZHours";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 16;
            this.gridColumn11.Width = 82;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Estimated VZ";
            this.gridColumn12.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn12.FieldName = "EstimatedVZHours";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 17;
            this.gridColumn12.Width = 83;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Actual VZ";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn13.FieldName = "ActualVZHours";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 18;
            this.gridColumn13.Width = 81;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Estimated SD";
            this.gridColumn14.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn14.FieldName = "EstimatedSDHours";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 19;
            this.gridColumn14.Width = 84;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Actual SD";
            this.gridColumn15.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.gridColumn15.FieldName = "ActualSDHours";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 20;
            this.gridColumn15.Width = 80;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Work Order";
            this.gridColumn24.FieldName = "WorkOrderID";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Width = 77;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Remarks";
            this.gridColumn27.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn27.FieldName = "Remarks";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Linked To Tree";
            this.gridColumn29.FieldName = "LinkedRecordDescription";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Width = 235;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Job Description";
            this.gridColumn31.FieldName = "JobDescription";
            this.gridColumn31.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 4;
            this.gridColumn31.Width = 162;
            // 
            // colPaddedWorkOrderID
            // 
            this.colPaddedWorkOrderID.Caption = "Linked To Work Order";
            this.colPaddedWorkOrderID.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID.Name = "colPaddedWorkOrderID";
            this.colPaddedWorkOrderID.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID.Visible = true;
            this.colPaddedWorkOrderID.VisibleIndex = 20;
            this.colPaddedWorkOrderID.Width = 125;
            // 
            // colWorkOrderSortOrder
            // 
            this.colWorkOrderSortOrder.Caption = "Order";
            this.colWorkOrderSortOrder.FieldName = "WorkOrderSortOrder";
            this.colWorkOrderSortOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colWorkOrderSortOrder.Name = "colWorkOrderSortOrder";
            this.colWorkOrderSortOrder.OptionsColumn.AllowEdit = false;
            this.colWorkOrderSortOrder.OptionsColumn.AllowFocus = false;
            this.colWorkOrderSortOrder.OptionsColumn.ReadOnly = true;
            this.colWorkOrderSortOrder.Visible = true;
            this.colWorkOrderSortOrder.VisibleIndex = 0;
            this.colWorkOrderSortOrder.Width = 50;
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            this.colPoleID.Width = 55;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 1;
            this.colCircuitName.Width = 126;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 2;
            this.colPoleNumber.Width = 81;
            // 
            // colTreeReferenceNumber
            // 
            this.colTreeReferenceNumber.Caption = "Tree Reference";
            this.colTreeReferenceNumber.FieldName = "TreeReferenceNumber";
            this.colTreeReferenceNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colTreeReferenceNumber.Name = "colTreeReferenceNumber";
            this.colTreeReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colTreeReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colTreeReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colTreeReferenceNumber.Visible = true;
            this.colTreeReferenceNumber.VisibleIndex = 3;
            this.colTreeReferenceNumber.Width = 96;
            // 
            // colLastStartTime
            // 
            this.colLastStartTime.Caption = "Last Start Time";
            this.colLastStartTime.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colLastStartTime.FieldName = "LastStartTime";
            this.colLastStartTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastStartTime.Name = "colLastStartTime";
            this.colLastStartTime.OptionsColumn.AllowEdit = false;
            this.colLastStartTime.OptionsColumn.AllowFocus = false;
            this.colLastStartTime.OptionsColumn.ReadOnly = true;
            this.colLastStartTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastStartTime.Width = 93;
            // 
            // colTotalTimeTaken
            // 
            this.colTotalTimeTaken.Caption = "Total Time Taken";
            this.colTotalTimeTaken.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colTotalTimeTaken.FieldName = "TotalTimeTaken";
            this.colTotalTimeTaken.Name = "colTotalTimeTaken";
            this.colTotalTimeTaken.OptionsColumn.AllowEdit = false;
            this.colTotalTimeTaken.OptionsColumn.AllowFocus = false;
            this.colTotalTimeTaken.OptionsColumn.ReadOnly = true;
            this.colTotalTimeTaken.Width = 102;
            // 
            // colTeamRemarks
            // 
            this.colTeamRemarks.Caption = "Team Remarks";
            this.colTeamRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colTeamRemarks.FieldName = "TeamRemarks";
            this.colTeamRemarks.Name = "colTeamRemarks";
            this.colTeamRemarks.OptionsColumn.ReadOnly = true;
            this.colTeamRemarks.Visible = true;
            this.colTeamRemarks.VisibleIndex = 14;
            this.colTeamRemarks.Width = 91;
            // 
            // colActionStatus
            // 
            this.colActionStatus.Caption = "Status";
            this.colActionStatus.FieldName = "ActionStatus";
            this.colActionStatus.Name = "colActionStatus";
            this.colActionStatus.OptionsColumn.AllowEdit = false;
            this.colActionStatus.OptionsColumn.AllowFocus = false;
            this.colActionStatus.OptionsColumn.ReadOnly = true;
            this.colActionStatus.Visible = true;
            this.colActionStatus.VisibleIndex = 7;
            // 
            // colActionStatusID
            // 
            this.colActionStatusID.Caption = "Status ID";
            this.colActionStatusID.FieldName = "ActionStatusID";
            this.colActionStatusID.Name = "colActionStatusID";
            this.colActionStatusID.OptionsColumn.AllowEdit = false;
            this.colActionStatusID.OptionsColumn.AllowFocus = false;
            this.colActionStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colTeamOnHold
            // 
            this.colTeamOnHold.Caption = "Team On-Hold";
            this.colTeamOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTeamOnHold.FieldName = "TeamOnHold";
            this.colTeamOnHold.Name = "colTeamOnHold";
            this.colTeamOnHold.OptionsColumn.AllowEdit = false;
            this.colTeamOnHold.OptionsColumn.AllowFocus = false;
            this.colTeamOnHold.OptionsColumn.ReadOnly = true;
            this.colTeamOnHold.Visible = true;
            this.colTeamOnHold.VisibleIndex = 13;
            this.colTeamOnHold.Width = 87;
            // 
            // colHeldUpType
            // 
            this.colHeldUpType.Caption = "Held-Up Type";
            this.colHeldUpType.FieldName = "HeldUpType";
            this.colHeldUpType.Name = "colHeldUpType";
            this.colHeldUpType.OptionsColumn.AllowEdit = false;
            this.colHeldUpType.OptionsColumn.AllowFocus = false;
            this.colHeldUpType.OptionsColumn.ReadOnly = true;
            this.colHeldUpType.Visible = true;
            this.colHeldUpType.VisibleIndex = 11;
            this.colHeldUpType.Width = 110;
            // 
            // colIsShutdownRequired
            // 
            this.colIsShutdownRequired.Caption = "Shutdown Required";
            this.colIsShutdownRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsShutdownRequired.FieldName = "IsShutdownRequired";
            this.colIsShutdownRequired.Name = "colIsShutdownRequired";
            this.colIsShutdownRequired.OptionsColumn.AllowEdit = false;
            this.colIsShutdownRequired.OptionsColumn.AllowFocus = false;
            this.colIsShutdownRequired.OptionsColumn.ReadOnly = true;
            this.colIsShutdownRequired.Visible = true;
            this.colIsShutdownRequired.VisibleIndex = 12;
            this.colIsShutdownRequired.Width = 113;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // xtraTabPageLinkedSubContractors
            // 
            this.xtraTabPageLinkedSubContractors.Controls.Add(this.gridControl4);
            this.xtraTabPageLinkedSubContractors.Name = "xtraTabPageLinkedSubContractors";
            this.xtraTabPageLinkedSubContractors.Size = new System.Drawing.Size(1089, 211);
            this.xtraTabPageLinkedSubContractors.Text = "Linked Sub-Contractors";
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit4});
            this.gridControl4.Size = new System.Drawing.Size(1089, 211);
            this.gridControl4.TabIndex = 1;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource
            // 
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource.DataMember = "sp07269_UT_Work_Order_Manager_Linked_SubContractors";
            this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkOrderTeamID,
            this.colWorkOrderID1,
            this.colSubContractorID,
            this.colRemarks1,
            this.colGUID1,
            this.colSubContractorName,
            this.colInternalContractor,
            this.colMobileTel,
            this.colTelephone1,
            this.colTelephone2,
            this.colPostcode,
            this.colPaddedWorkOrderID2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colWorkOrderTeamID
            // 
            this.colWorkOrderTeamID.Caption = "Work Order Team ID";
            this.colWorkOrderTeamID.FieldName = "WorkOrderTeamID";
            this.colWorkOrderTeamID.Name = "colWorkOrderTeamID";
            this.colWorkOrderTeamID.OptionsColumn.AllowEdit = false;
            this.colWorkOrderTeamID.OptionsColumn.AllowFocus = false;
            this.colWorkOrderTeamID.OptionsColumn.ReadOnly = true;
            this.colWorkOrderTeamID.Width = 120;
            // 
            // colWorkOrderID1
            // 
            this.colWorkOrderID1.Caption = "Work Order ID";
            this.colWorkOrderID1.FieldName = "WorkOrderID";
            this.colWorkOrderID1.Name = "colWorkOrderID1";
            this.colWorkOrderID1.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID1.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID1.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID1.Width = 91;
            // 
            // colSubContractorID
            // 
            this.colSubContractorID.Caption = "Sub-Contractor ID";
            this.colSubContractorID.FieldName = "SubContractorID";
            this.colSubContractorID.Name = "colSubContractorID";
            this.colSubContractorID.OptionsColumn.AllowEdit = false;
            this.colSubContractorID.OptionsColumn.AllowFocus = false;
            this.colSubContractorID.OptionsColumn.ReadOnly = true;
            this.colSubContractorID.Width = 109;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 6;
            this.colRemarks1.Width = 210;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID1
            // 
            this.colGUID1.Caption = "GUID";
            this.colGUID1.FieldName = "GUID";
            this.colGUID1.Name = "colGUID1";
            this.colGUID1.OptionsColumn.AllowEdit = false;
            this.colGUID1.OptionsColumn.AllowFocus = false;
            this.colGUID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName
            // 
            this.colSubContractorName.Caption = "Sub-Contractor Name";
            this.colSubContractorName.FieldName = "SubContractorName";
            this.colSubContractorName.Name = "colSubContractorName";
            this.colSubContractorName.OptionsColumn.AllowEdit = false;
            this.colSubContractorName.OptionsColumn.AllowFocus = false;
            this.colSubContractorName.OptionsColumn.ReadOnly = true;
            this.colSubContractorName.Visible = true;
            this.colSubContractorName.VisibleIndex = 0;
            this.colSubContractorName.Width = 288;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal";
            this.colInternalContractor.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Visible = true;
            this.colInternalContractor.VisibleIndex = 1;
            this.colInternalContractor.Width = 61;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colMobileTel
            // 
            this.colMobileTel.Caption = "Mobile Telephone";
            this.colMobileTel.FieldName = "MobileTel";
            this.colMobileTel.Name = "colMobileTel";
            this.colMobileTel.OptionsColumn.AllowEdit = false;
            this.colMobileTel.OptionsColumn.AllowFocus = false;
            this.colMobileTel.OptionsColumn.ReadOnly = true;
            this.colMobileTel.Visible = true;
            this.colMobileTel.VisibleIndex = 2;
            this.colMobileTel.Width = 117;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Visible = true;
            this.colTelephone1.VisibleIndex = 3;
            this.colTelephone1.Width = 97;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Visible = true;
            this.colTelephone2.VisibleIndex = 4;
            this.colTelephone2.Width = 98;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 5;
            // 
            // colPaddedWorkOrderID2
            // 
            this.colPaddedWorkOrderID2.Caption = "Work Order #";
            this.colPaddedWorkOrderID2.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID2.Name = "colPaddedWorkOrderID2";
            this.colPaddedWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID2.Visible = true;
            this.colPaddedWorkOrderID2.VisibleIndex = 11;
            this.colPaddedWorkOrderID2.Width = 88;
            // 
            // xtraTabPageLinkedMaps
            // 
            this.xtraTabPageLinkedMaps.Controls.Add(this.gridControl5);
            this.xtraTabPageLinkedMaps.Name = "xtraTabPageLinkedMaps";
            this.xtraTabPageLinkedMaps.Size = new System.Drawing.Size(1089, 211);
            this.xtraTabPageLinkedMaps.Text = "Linked Maps";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp07270UTWorkOrderManagerLinkedMapsBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime3,
            this.repositoryItemHyperLinkEditMap,
            this.repositoryItemMemoExEdit4});
            this.gridControl5.Size = new System.Drawing.Size(1089, 211);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp07270UTWorkOrderManagerLinkedMapsBindingSource
            // 
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource.DataMember = "sp07270_UT_Work_Order_Manager_Linked_Maps";
            this.sp07270UTWorkOrderManagerLinkedMapsBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMapID,
            this.colWorkOrderID2,
            this.colMap,
            this.colMapDateTimeCreated,
            this.colMapDescription,
            this.colMapOrder,
            this.colPictureTakenByName,
            this.colPictureRemarks,
            this.colPaddedWorkOrderID3});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 1;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPaddedWorkOrderID3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colMapOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView5_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colMapID
            // 
            this.colMapID.Caption = "Map ID";
            this.colMapID.FieldName = "MapID";
            this.colMapID.Name = "colMapID";
            this.colMapID.OptionsColumn.AllowEdit = false;
            this.colMapID.OptionsColumn.AllowFocus = false;
            this.colMapID.OptionsColumn.ReadOnly = true;
            // 
            // colWorkOrderID2
            // 
            this.colWorkOrderID2.Caption = "Work Order ID";
            this.colWorkOrderID2.FieldName = "WorkOrderID";
            this.colWorkOrderID2.Name = "colWorkOrderID2";
            this.colWorkOrderID2.OptionsColumn.AllowEdit = false;
            this.colWorkOrderID2.OptionsColumn.AllowFocus = false;
            this.colWorkOrderID2.OptionsColumn.ReadOnly = true;
            this.colWorkOrderID2.Width = 91;
            // 
            // colMap
            // 
            this.colMap.Caption = "Map";
            this.colMap.ColumnEdit = this.repositoryItemHyperLinkEditMap;
            this.colMap.FieldName = "Map";
            this.colMap.Name = "colMap";
            this.colMap.OptionsColumn.ReadOnly = true;
            this.colMap.Visible = true;
            this.colMap.VisibleIndex = 4;
            this.colMap.Width = 386;
            // 
            // repositoryItemHyperLinkEditMap
            // 
            this.repositoryItemHyperLinkEditMap.AutoHeight = false;
            this.repositoryItemHyperLinkEditMap.Name = "repositoryItemHyperLinkEditMap";
            this.repositoryItemHyperLinkEditMap.SingleClick = true;
            this.repositoryItemHyperLinkEditMap.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditMap_OpenLink);
            // 
            // colMapDateTimeCreated
            // 
            this.colMapDateTimeCreated.Caption = "Created On";
            this.colMapDateTimeCreated.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colMapDateTimeCreated.FieldName = "MapDateTimeCreated";
            this.colMapDateTimeCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colMapDateTimeCreated.Name = "colMapDateTimeCreated";
            this.colMapDateTimeCreated.OptionsColumn.AllowEdit = false;
            this.colMapDateTimeCreated.OptionsColumn.AllowFocus = false;
            this.colMapDateTimeCreated.OptionsColumn.ReadOnly = true;
            this.colMapDateTimeCreated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colMapDateTimeCreated.Visible = true;
            this.colMapDateTimeCreated.VisibleIndex = 3;
            this.colMapDateTimeCreated.Width = 86;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colMapDescription
            // 
            this.colMapDescription.Caption = "Description";
            this.colMapDescription.FieldName = "MapDescription";
            this.colMapDescription.Name = "colMapDescription";
            this.colMapDescription.OptionsColumn.AllowEdit = false;
            this.colMapDescription.OptionsColumn.AllowFocus = false;
            this.colMapDescription.OptionsColumn.ReadOnly = true;
            this.colMapDescription.Visible = true;
            this.colMapDescription.VisibleIndex = 1;
            this.colMapDescription.Width = 323;
            // 
            // colMapOrder
            // 
            this.colMapOrder.Caption = "Order";
            this.colMapOrder.FieldName = "MapOrder";
            this.colMapOrder.Name = "colMapOrder";
            this.colMapOrder.OptionsColumn.AllowEdit = false;
            this.colMapOrder.OptionsColumn.AllowFocus = false;
            this.colMapOrder.OptionsColumn.ReadOnly = true;
            this.colMapOrder.Visible = true;
            this.colMapOrder.VisibleIndex = 0;
            this.colMapOrder.Width = 62;
            // 
            // colPictureTakenByName
            // 
            this.colPictureTakenByName.Caption = "Created By";
            this.colPictureTakenByName.FieldName = "PictureTakenByName";
            this.colPictureTakenByName.Name = "colPictureTakenByName";
            this.colPictureTakenByName.OptionsColumn.AllowEdit = false;
            this.colPictureTakenByName.OptionsColumn.AllowFocus = false;
            this.colPictureTakenByName.OptionsColumn.ReadOnly = true;
            this.colPictureTakenByName.Visible = true;
            this.colPictureTakenByName.VisibleIndex = 2;
            this.colPictureTakenByName.Width = 91;
            // 
            // colPictureRemarks
            // 
            this.colPictureRemarks.Caption = "Remarks";
            this.colPictureRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colPictureRemarks.FieldName = "PictureRemarks";
            this.colPictureRemarks.Name = "colPictureRemarks";
            this.colPictureRemarks.OptionsColumn.ReadOnly = true;
            this.colPictureRemarks.Visible = true;
            this.colPictureRemarks.VisibleIndex = 5;
            this.colPictureRemarks.Width = 136;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colPaddedWorkOrderID3
            // 
            this.colPaddedWorkOrderID3.Caption = "Work Order #";
            this.colPaddedWorkOrderID3.FieldName = "PaddedWorkOrderID";
            this.colPaddedWorkOrderID3.Name = "colPaddedWorkOrderID3";
            this.colPaddedWorkOrderID3.OptionsColumn.AllowEdit = false;
            this.colPaddedWorkOrderID3.OptionsColumn.AllowFocus = false;
            this.colPaddedWorkOrderID3.OptionsColumn.ReadOnly = true;
            this.colPaddedWorkOrderID3.Visible = true;
            this.colPaddedWorkOrderID3.VisibleIndex = 7;
            this.colPaddedWorkOrderID3.Width = 88;
            // 
            // xtraTabPageLinkRiskAssessments
            // 
            this.xtraTabPageLinkRiskAssessments.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageLinkRiskAssessments.Name = "xtraTabPageLinkRiskAssessments";
            this.xtraTabPageLinkRiskAssessments.Size = new System.Drawing.Size(1089, 211);
            this.xtraTabPageLinkRiskAssessments.Text = "Linked Risk Assessments";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl22);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.gridControl23);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1089, 211);
            this.splitContainerControl2.SplitterPosition = 517;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl22
            // 
            this.gridControl22.DataSource = this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource;
            this.gridControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl22.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl22.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl22.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl22.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl22.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Site Specific Risk Assessment", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "refresh")});
            this.gridControl22.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl22_EmbeddedNavigator_ButtonClick);
            this.gridControl22.Location = new System.Drawing.Point(0, 0);
            this.gridControl22.MainView = this.gridView22;
            this.gridControl22.MenuManager = this.barManager1;
            this.gridControl22.Name = "gridControl22";
            this.gridControl22.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2,
            this.repositoryItemMemoExEdit21,
            this.repositoryItemCheckEdit3});
            this.gridControl22.Size = new System.Drawing.Size(517, 211);
            this.gridControl22.TabIndex = 25;
            this.gridControl22.UseEmbeddedNavigator = true;
            this.gridControl22.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView22});
            // 
            // sp07291UTTeamWorkOrderRiskAssessmentListBindingSource
            // 
            this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource.DataMember = "sp07291_UT_Team_Work_Order_Risk_Assessment_List";
            this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView22
            // 
            this.gridView22.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskAssessmentID,
            this.colSurveyedPoleID1,
            this.colDateTimeRecorded,
            this.colPersonCompletingTypeID,
            this.colPersonCompletingID,
            this.colPermissionChecked,
            this.colElectricalAssessmentVerified,
            this.colElectricalRiskCategoryID,
            this.colTypeOfWorkingID,
            this.colPermitHolderName,
            this.colNRSWAOpeningNoticeNo,
            this.colRemarks6,
            this.colGUID6,
            this.colRiskAssessmentTypeID,
            this.colLastUpdated,
            this.colElectricalRiskCategory,
            this.colTypeOfWorking,
            this.colPersonCompletingType,
            this.colPersonCompletingName,
            this.colRiskAssessmentType,
            this.colSiteAddress,
            this.colMEWPArialRescuers,
            this.colEmergencyKitLocation,
            this.colAirAmbulanceLat,
            this.colAirAmbulanceLong,
            this.colBanksMan,
            this.colEmergencyServicesMeetPoint,
            this.colMobilePhoneSignalBars,
            this.colNearestAandE,
            this.colNearestLandLine});
            this.gridView22.GridControl = this.gridControl22;
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView22.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView22.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView22.OptionsLayout.StoreAppearance = true;
            this.gridView22.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView22.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView22.OptionsSelection.MultiSelect = true;
            this.gridView22.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView22.OptionsView.ColumnAutoWidth = false;
            this.gridView22.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            this.gridView22.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeRecorded, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView22.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView22.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView22_SelectionChanged);
            this.gridView22.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView22.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView22.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView22.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView22_MouseUp);
            this.gridView22.DoubleClick += new System.EventHandler(this.gridView22_DoubleClick);
            this.gridView22.GotFocus += new System.EventHandler(this.gridView22_GotFocus);
            // 
            // colRiskAssessmentID
            // 
            this.colRiskAssessmentID.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID.Name = "colRiskAssessmentID";
            this.colRiskAssessmentID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID.Width = 114;
            // 
            // colSurveyedPoleID1
            // 
            this.colSurveyedPoleID1.Caption = "Surveyed Pole ID";
            this.colSurveyedPoleID1.FieldName = "SurveyedPoleID";
            this.colSurveyedPoleID1.Name = "colSurveyedPoleID1";
            this.colSurveyedPoleID1.OptionsColumn.AllowEdit = false;
            this.colSurveyedPoleID1.OptionsColumn.AllowFocus = false;
            this.colSurveyedPoleID1.OptionsColumn.ReadOnly = true;
            this.colSurveyedPoleID1.Width = 104;
            // 
            // colDateTimeRecorded
            // 
            this.colDateTimeRecorded.Caption = "Created On";
            this.colDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit2;
            this.colDateTimeRecorded.FieldName = "DateTimeRecorded";
            this.colDateTimeRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateTimeRecorded.Name = "colDateTimeRecorded";
            this.colDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateTimeRecorded.Visible = true;
            this.colDateTimeRecorded.VisibleIndex = 0;
            this.colDateTimeRecorded.Width = 102;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit2.Mask.EditMask = "g";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // colPersonCompletingTypeID
            // 
            this.colPersonCompletingTypeID.Caption = "Person Completing Type ID";
            this.colPersonCompletingTypeID.FieldName = "PersonCompletingTypeID";
            this.colPersonCompletingTypeID.Name = "colPersonCompletingTypeID";
            this.colPersonCompletingTypeID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingTypeID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingTypeID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingTypeID.Width = 151;
            // 
            // colPersonCompletingID
            // 
            this.colPersonCompletingID.Caption = "Person Completing ID";
            this.colPersonCompletingID.FieldName = "PersonCompletingID";
            this.colPersonCompletingID.Name = "colPersonCompletingID";
            this.colPersonCompletingID.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingID.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingID.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingID.Width = 124;
            // 
            // colPermissionChecked
            // 
            this.colPermissionChecked.Caption = "Permission Checked";
            this.colPermissionChecked.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colPermissionChecked.FieldName = "PermissionChecked";
            this.colPermissionChecked.Name = "colPermissionChecked";
            this.colPermissionChecked.OptionsColumn.AllowEdit = false;
            this.colPermissionChecked.OptionsColumn.AllowFocus = false;
            this.colPermissionChecked.OptionsColumn.ReadOnly = true;
            this.colPermissionChecked.Visible = true;
            this.colPermissionChecked.VisibleIndex = 4;
            this.colPermissionChecked.Width = 115;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colElectricalAssessmentVerified
            // 
            this.colElectricalAssessmentVerified.Caption = "Electrical Assessment Verified";
            this.colElectricalAssessmentVerified.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colElectricalAssessmentVerified.FieldName = "ElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.Name = "colElectricalAssessmentVerified";
            this.colElectricalAssessmentVerified.OptionsColumn.AllowEdit = false;
            this.colElectricalAssessmentVerified.OptionsColumn.AllowFocus = false;
            this.colElectricalAssessmentVerified.OptionsColumn.ReadOnly = true;
            this.colElectricalAssessmentVerified.Visible = true;
            this.colElectricalAssessmentVerified.VisibleIndex = 5;
            this.colElectricalAssessmentVerified.Width = 162;
            // 
            // colElectricalRiskCategoryID
            // 
            this.colElectricalRiskCategoryID.Caption = "Electrical Risk Category ID";
            this.colElectricalRiskCategoryID.FieldName = "ElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.Name = "colElectricalRiskCategoryID";
            this.colElectricalRiskCategoryID.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategoryID.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategoryID.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategoryID.Width = 147;
            // 
            // colTypeOfWorkingID
            // 
            this.colTypeOfWorkingID.Caption = "Type Of Working ID";
            this.colTypeOfWorkingID.FieldName = "TypeOfWorkingID";
            this.colTypeOfWorkingID.Name = "colTypeOfWorkingID";
            this.colTypeOfWorkingID.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorkingID.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorkingID.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorkingID.Width = 116;
            // 
            // colPermitHolderName
            // 
            this.colPermitHolderName.Caption = "Permit Holder Name";
            this.colPermitHolderName.FieldName = "PermitHolderName";
            this.colPermitHolderName.Name = "colPermitHolderName";
            this.colPermitHolderName.OptionsColumn.AllowEdit = false;
            this.colPermitHolderName.OptionsColumn.AllowFocus = false;
            this.colPermitHolderName.OptionsColumn.ReadOnly = true;
            this.colPermitHolderName.Visible = true;
            this.colPermitHolderName.VisibleIndex = 8;
            this.colPermitHolderName.Width = 115;
            // 
            // colNRSWAOpeningNoticeNo
            // 
            this.colNRSWAOpeningNoticeNo.Caption = "NRSWA Opening Notice";
            this.colNRSWAOpeningNoticeNo.FieldName = "NRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.Name = "colNRSWAOpeningNoticeNo";
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowEdit = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.AllowFocus = false;
            this.colNRSWAOpeningNoticeNo.OptionsColumn.ReadOnly = true;
            this.colNRSWAOpeningNoticeNo.Visible = true;
            this.colNRSWAOpeningNoticeNo.VisibleIndex = 9;
            this.colNRSWAOpeningNoticeNo.Width = 134;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit21
            // 
            this.repositoryItemMemoExEdit21.AutoHeight = false;
            this.repositoryItemMemoExEdit21.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit21.Name = "repositoryItemMemoExEdit21";
            this.repositoryItemMemoExEdit21.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit21.ShowIcon = false;
            // 
            // colGUID6
            // 
            this.colGUID6.Caption = "GUID";
            this.colGUID6.FieldName = "GUID";
            this.colGUID6.Name = "colGUID6";
            this.colGUID6.OptionsColumn.AllowEdit = false;
            this.colGUID6.OptionsColumn.AllowFocus = false;
            this.colGUID6.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentTypeID
            // 
            this.colRiskAssessmentTypeID.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.Name = "colRiskAssessmentTypeID";
            this.colRiskAssessmentTypeID.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID.Width = 141;
            // 
            // colLastUpdated
            // 
            this.colLastUpdated.Caption = "Last Updated";
            this.colLastUpdated.ColumnEdit = this.repositoryItemDateEdit2;
            this.colLastUpdated.FieldName = "LastUpdated";
            this.colLastUpdated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastUpdated.Name = "colLastUpdated";
            this.colLastUpdated.OptionsColumn.AllowEdit = false;
            this.colLastUpdated.OptionsColumn.AllowFocus = false;
            this.colLastUpdated.OptionsColumn.ReadOnly = true;
            this.colLastUpdated.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastUpdated.Visible = true;
            this.colLastUpdated.VisibleIndex = 10;
            this.colLastUpdated.Width = 85;
            // 
            // colElectricalRiskCategory
            // 
            this.colElectricalRiskCategory.Caption = "Electrical Risk Category";
            this.colElectricalRiskCategory.FieldName = "ElectricalRiskCategory";
            this.colElectricalRiskCategory.Name = "colElectricalRiskCategory";
            this.colElectricalRiskCategory.OptionsColumn.AllowEdit = false;
            this.colElectricalRiskCategory.OptionsColumn.AllowFocus = false;
            this.colElectricalRiskCategory.OptionsColumn.ReadOnly = true;
            this.colElectricalRiskCategory.Visible = true;
            this.colElectricalRiskCategory.VisibleIndex = 6;
            this.colElectricalRiskCategory.Width = 133;
            // 
            // colTypeOfWorking
            // 
            this.colTypeOfWorking.Caption = "Type of Working";
            this.colTypeOfWorking.FieldName = "TypeOfWorking";
            this.colTypeOfWorking.Name = "colTypeOfWorking";
            this.colTypeOfWorking.OptionsColumn.AllowEdit = false;
            this.colTypeOfWorking.OptionsColumn.AllowFocus = false;
            this.colTypeOfWorking.OptionsColumn.ReadOnly = true;
            this.colTypeOfWorking.Visible = true;
            this.colTypeOfWorking.VisibleIndex = 7;
            this.colTypeOfWorking.Width = 100;
            // 
            // colPersonCompletingType
            // 
            this.colPersonCompletingType.Caption = "Person Type";
            this.colPersonCompletingType.FieldName = "PersonCompletingType";
            this.colPersonCompletingType.Name = "colPersonCompletingType";
            this.colPersonCompletingType.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingType.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingType.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingType.Visible = true;
            this.colPersonCompletingType.VisibleIndex = 3;
            this.colPersonCompletingType.Width = 81;
            // 
            // colPersonCompletingName
            // 
            this.colPersonCompletingName.Caption = "Person Completing Name";
            this.colPersonCompletingName.FieldName = "PersonCompletingName";
            this.colPersonCompletingName.Name = "colPersonCompletingName";
            this.colPersonCompletingName.OptionsColumn.AllowEdit = false;
            this.colPersonCompletingName.OptionsColumn.AllowFocus = false;
            this.colPersonCompletingName.OptionsColumn.ReadOnly = true;
            this.colPersonCompletingName.Visible = true;
            this.colPersonCompletingName.VisibleIndex = 2;
            this.colPersonCompletingName.Width = 140;
            // 
            // colRiskAssessmentType
            // 
            this.colRiskAssessmentType.Caption = "Risk Assessment Type";
            this.colRiskAssessmentType.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType.Name = "colRiskAssessmentType";
            this.colRiskAssessmentType.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType.Visible = true;
            this.colRiskAssessmentType.VisibleIndex = 1;
            this.colRiskAssessmentType.Width = 127;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Site Address and Access";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 12;
            this.colSiteAddress.Width = 138;
            // 
            // colMEWPArialRescuers
            // 
            this.colMEWPArialRescuers.Caption = "MEWP \\ Aerial Rescuers";
            this.colMEWPArialRescuers.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colMEWPArialRescuers.FieldName = "MEWPArialRescuers";
            this.colMEWPArialRescuers.Name = "colMEWPArialRescuers";
            this.colMEWPArialRescuers.OptionsColumn.ReadOnly = true;
            this.colMEWPArialRescuers.Visible = true;
            this.colMEWPArialRescuers.VisibleIndex = 13;
            this.colMEWPArialRescuers.Width = 135;
            // 
            // colEmergencyKitLocation
            // 
            this.colEmergencyKitLocation.Caption = "Emergency Kit Location";
            this.colEmergencyKitLocation.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyKitLocation.FieldName = "EmergencyKitLocation";
            this.colEmergencyKitLocation.Name = "colEmergencyKitLocation";
            this.colEmergencyKitLocation.OptionsColumn.ReadOnly = true;
            this.colEmergencyKitLocation.Visible = true;
            this.colEmergencyKitLocation.VisibleIndex = 14;
            this.colEmergencyKitLocation.Width = 132;
            // 
            // colAirAmbulanceLat
            // 
            this.colAirAmbulanceLat.Caption = "Air Ambulance Lat";
            this.colAirAmbulanceLat.FieldName = "AirAmbulanceLat";
            this.colAirAmbulanceLat.Name = "colAirAmbulanceLat";
            this.colAirAmbulanceLat.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLat.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLat.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLat.Visible = true;
            this.colAirAmbulanceLat.VisibleIndex = 15;
            this.colAirAmbulanceLat.Width = 107;
            // 
            // colAirAmbulanceLong
            // 
            this.colAirAmbulanceLong.Caption = "Air Ambulance Long";
            this.colAirAmbulanceLong.FieldName = "AirAmbulanceLong";
            this.colAirAmbulanceLong.Name = "colAirAmbulanceLong";
            this.colAirAmbulanceLong.OptionsColumn.AllowEdit = false;
            this.colAirAmbulanceLong.OptionsColumn.AllowFocus = false;
            this.colAirAmbulanceLong.OptionsColumn.ReadOnly = true;
            this.colAirAmbulanceLong.Visible = true;
            this.colAirAmbulanceLong.VisibleIndex = 16;
            this.colAirAmbulanceLong.Width = 115;
            // 
            // colBanksMan
            // 
            this.colBanksMan.Caption = "Banksman";
            this.colBanksMan.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colBanksMan.FieldName = "BanksMan";
            this.colBanksMan.Name = "colBanksMan";
            this.colBanksMan.OptionsColumn.ReadOnly = true;
            this.colBanksMan.Visible = true;
            this.colBanksMan.VisibleIndex = 17;
            // 
            // colEmergencyServicesMeetPoint
            // 
            this.colEmergencyServicesMeetPoint.Caption = "Emergency Service Meet Point";
            this.colEmergencyServicesMeetPoint.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colEmergencyServicesMeetPoint.FieldName = "EmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.Name = "colEmergencyServicesMeetPoint";
            this.colEmergencyServicesMeetPoint.OptionsColumn.ReadOnly = true;
            this.colEmergencyServicesMeetPoint.Visible = true;
            this.colEmergencyServicesMeetPoint.VisibleIndex = 18;
            this.colEmergencyServicesMeetPoint.Width = 166;
            // 
            // colMobilePhoneSignalBars
            // 
            this.colMobilePhoneSignalBars.Caption = "Phone Signal Bars";
            this.colMobilePhoneSignalBars.FieldName = "MobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.Name = "colMobilePhoneSignalBars";
            this.colMobilePhoneSignalBars.OptionsColumn.AllowEdit = false;
            this.colMobilePhoneSignalBars.OptionsColumn.AllowFocus = false;
            this.colMobilePhoneSignalBars.OptionsColumn.ReadOnly = true;
            this.colMobilePhoneSignalBars.Visible = true;
            this.colMobilePhoneSignalBars.VisibleIndex = 19;
            this.colMobilePhoneSignalBars.Width = 106;
            // 
            // colNearestAandE
            // 
            this.colNearestAandE.Caption = "Nearest A & E";
            this.colNearestAandE.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestAandE.FieldName = "NearestAandE";
            this.colNearestAandE.Name = "colNearestAandE";
            this.colNearestAandE.OptionsColumn.ReadOnly = true;
            this.colNearestAandE.Visible = true;
            this.colNearestAandE.VisibleIndex = 20;
            this.colNearestAandE.Width = 88;
            // 
            // colNearestLandLine
            // 
            this.colNearestLandLine.Caption = "Nearest Landline";
            this.colNearestLandLine.ColumnEdit = this.repositoryItemMemoExEdit21;
            this.colNearestLandLine.FieldName = "NearestLandLine";
            this.colNearestLandLine.Name = "colNearestLandLine";
            this.colNearestLandLine.OptionsColumn.ReadOnly = true;
            this.colNearestLandLine.Visible = true;
            this.colNearestLandLine.VisibleIndex = 21;
            this.colNearestLandLine.Width = 101;
            // 
            // gridControl23
            // 
            this.gridControl23.DataSource = this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource;
            this.gridControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl23.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl23.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl23.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl23.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl23.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "refresh")});
            this.gridControl23.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl23_EmbeddedNavigator_ButtonClick);
            this.gridControl23.Location = new System.Drawing.Point(0, 0);
            this.gridControl23.MainView = this.gridView23;
            this.gridControl23.MenuManager = this.barManager1;
            this.gridControl23.Name = "gridControl23";
            this.gridControl23.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit3,
            this.repositoryItemMemoExEdit22});
            this.gridControl23.Size = new System.Drawing.Size(566, 211);
            this.gridControl23.TabIndex = 26;
            this.gridControl23.UseEmbeddedNavigator = true;
            this.gridControl23.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView23});
            this.gridControl23.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView23_MouseUp);
            // 
            // sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource
            // 
            this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource.DataMember = "sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_List";
            this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // gridView23
            // 
            this.gridView23.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRiskQuestionID,
            this.colRiskAssessmentID1,
            this.colQuestionDescription,
            this.colQuestionNotes,
            this.colQuestionOrder,
            this.colAnswer,
            this.colRemarks7,
            this.colMasterQuestionID,
            this.colGUID7,
            this.colRiskAssessmentDateTimeRecorded,
            this.colRiskAssessmentType1,
            this.colAnswerDescription,
            this.colAnswerText1,
            this.colAnswerText2,
            this.colAnswerText3,
            this.colRiskAssessmentTypeID1});
            this.gridView23.GridControl = this.gridControl23;
            this.gridView23.GroupCount = 1;
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView23.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView23.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView23.OptionsLayout.StoreAppearance = true;
            this.gridView23.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView23.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView23.OptionsSelection.MultiSelect = true;
            this.gridView23.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView23.OptionsView.ColumnAutoWidth = false;
            this.gridView23.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView23.OptionsView.ShowGroupPanel = false;
            this.gridView23.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRiskAssessmentDateTimeRecorded, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colQuestionOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView23.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView23_CustomRowCellEdit);
            this.gridView23.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView23.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView23_SelectionChanged);
            this.gridView23.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView23.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView23_ShowingEditor);
            this.gridView23.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView23.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView23.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView23_MouseUp);
            this.gridView23.GotFocus += new System.EventHandler(this.gridView23_GotFocus);
            // 
            // colRiskQuestionID
            // 
            this.colRiskQuestionID.Caption = "Risk Question ID";
            this.colRiskQuestionID.FieldName = "RiskQuestionID";
            this.colRiskQuestionID.Name = "colRiskQuestionID";
            this.colRiskQuestionID.OptionsColumn.AllowEdit = false;
            this.colRiskQuestionID.OptionsColumn.AllowFocus = false;
            this.colRiskQuestionID.OptionsColumn.ReadOnly = true;
            this.colRiskQuestionID.Width = 100;
            // 
            // colRiskAssessmentID1
            // 
            this.colRiskAssessmentID1.Caption = "Risk Assessment ID";
            this.colRiskAssessmentID1.FieldName = "RiskAssessmentID";
            this.colRiskAssessmentID1.Name = "colRiskAssessmentID1";
            this.colRiskAssessmentID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentID1.Width = 114;
            // 
            // colQuestionDescription
            // 
            this.colQuestionDescription.Caption = "Question";
            this.colQuestionDescription.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colQuestionDescription.FieldName = "QuestionDescription";
            this.colQuestionDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionDescription.Name = "colQuestionDescription";
            this.colQuestionDescription.OptionsColumn.ReadOnly = true;
            this.colQuestionDescription.Visible = true;
            this.colQuestionDescription.VisibleIndex = 1;
            this.colQuestionDescription.Width = 328;
            // 
            // repositoryItemMemoExEdit22
            // 
            this.repositoryItemMemoExEdit22.AutoHeight = false;
            this.repositoryItemMemoExEdit22.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit22.Name = "repositoryItemMemoExEdit22";
            this.repositoryItemMemoExEdit22.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit22.ShowIcon = false;
            // 
            // colQuestionNotes
            // 
            this.colQuestionNotes.Caption = "Question Notes";
            this.colQuestionNotes.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colQuestionNotes.FieldName = "QuestionNotes";
            this.colQuestionNotes.Name = "colQuestionNotes";
            this.colQuestionNotes.OptionsColumn.ReadOnly = true;
            this.colQuestionNotes.Visible = true;
            this.colQuestionNotes.VisibleIndex = 3;
            this.colQuestionNotes.Width = 166;
            // 
            // colQuestionOrder
            // 
            this.colQuestionOrder.Caption = "Order";
            this.colQuestionOrder.FieldName = "QuestionOrder";
            this.colQuestionOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colQuestionOrder.Name = "colQuestionOrder";
            this.colQuestionOrder.OptionsColumn.AllowEdit = false;
            this.colQuestionOrder.OptionsColumn.AllowFocus = false;
            this.colQuestionOrder.OptionsColumn.ReadOnly = true;
            this.colQuestionOrder.Visible = true;
            this.colQuestionOrder.VisibleIndex = 0;
            this.colQuestionOrder.Width = 62;
            // 
            // colAnswer
            // 
            this.colAnswer.Caption = "Answer ID";
            this.colAnswer.FieldName = "Answer";
            this.colAnswer.Name = "colAnswer";
            this.colAnswer.OptionsColumn.AllowEdit = false;
            this.colAnswer.OptionsColumn.AllowFocus = false;
            this.colAnswer.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 8;
            this.colRemarks7.Width = 175;
            // 
            // colMasterQuestionID
            // 
            this.colMasterQuestionID.Caption = "Master Question ID";
            this.colMasterQuestionID.FieldName = "MasterQuestionID";
            this.colMasterQuestionID.Name = "colMasterQuestionID";
            this.colMasterQuestionID.OptionsColumn.AllowEdit = false;
            this.colMasterQuestionID.OptionsColumn.AllowFocus = false;
            this.colMasterQuestionID.OptionsColumn.ReadOnly = true;
            this.colMasterQuestionID.Width = 114;
            // 
            // colGUID7
            // 
            this.colGUID7.Caption = "GUID";
            this.colGUID7.FieldName = "GUID";
            this.colGUID7.Name = "colGUID7";
            this.colGUID7.OptionsColumn.AllowEdit = false;
            this.colGUID7.OptionsColumn.AllowFocus = false;
            this.colGUID7.OptionsColumn.ReadOnly = true;
            // 
            // colRiskAssessmentDateTimeRecorded
            // 
            this.colRiskAssessmentDateTimeRecorded.Caption = "Risk Assessment Date";
            this.colRiskAssessmentDateTimeRecorded.ColumnEdit = this.repositoryItemDateEdit3;
            this.colRiskAssessmentDateTimeRecorded.FieldName = "RiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.Name = "colRiskAssessmentDateTimeRecorded";
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentDateTimeRecorded.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentDateTimeRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRiskAssessmentDateTimeRecorded.Visible = true;
            this.colRiskAssessmentDateTimeRecorded.VisibleIndex = 7;
            this.colRiskAssessmentDateTimeRecorded.Width = 126;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemDateEdit3.Mask.EditMask = "g";
            this.repositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // colRiskAssessmentType1
            // 
            this.colRiskAssessmentType1.Caption = "Assessment Type";
            this.colRiskAssessmentType1.FieldName = "RiskAssessmentType";
            this.colRiskAssessmentType1.Name = "colRiskAssessmentType1";
            this.colRiskAssessmentType1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentType1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentType1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentType1.Visible = true;
            this.colRiskAssessmentType1.VisibleIndex = 2;
            this.colRiskAssessmentType1.Width = 105;
            // 
            // colAnswerDescription
            // 
            this.colAnswerDescription.Caption = "Answer";
            this.colAnswerDescription.FieldName = "AnswerDescription";
            this.colAnswerDescription.Name = "colAnswerDescription";
            this.colAnswerDescription.OptionsColumn.AllowEdit = false;
            this.colAnswerDescription.OptionsColumn.AllowFocus = false;
            this.colAnswerDescription.OptionsColumn.ReadOnly = true;
            this.colAnswerDescription.Visible = true;
            this.colAnswerDescription.VisibleIndex = 4;
            this.colAnswerDescription.Width = 57;
            // 
            // colAnswerText1
            // 
            this.colAnswerText1.Caption = "Significant Hazards";
            this.colAnswerText1.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText1.FieldName = "AnswerText1";
            this.colAnswerText1.Name = "colAnswerText1";
            this.colAnswerText1.OptionsColumn.ReadOnly = true;
            this.colAnswerText1.Visible = true;
            this.colAnswerText1.VisibleIndex = 5;
            this.colAnswerText1.Width = 175;
            // 
            // colAnswerText2
            // 
            this.colAnswerText2.Caption = "People Affected";
            this.colAnswerText2.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText2.FieldName = "AnswerText2";
            this.colAnswerText2.Name = "colAnswerText2";
            this.colAnswerText2.OptionsColumn.ReadOnly = true;
            this.colAnswerText2.Visible = true;
            this.colAnswerText2.VisibleIndex = 6;
            this.colAnswerText2.Width = 175;
            // 
            // colAnswerText3
            // 
            this.colAnswerText3.Caption = "Controls";
            this.colAnswerText3.ColumnEdit = this.repositoryItemMemoExEdit22;
            this.colAnswerText3.FieldName = "AnswerText3";
            this.colAnswerText3.Name = "colAnswerText3";
            this.colAnswerText3.OptionsColumn.ReadOnly = true;
            this.colAnswerText3.Visible = true;
            this.colAnswerText3.VisibleIndex = 7;
            this.colAnswerText3.Width = 175;
            // 
            // colRiskAssessmentTypeID1
            // 
            this.colRiskAssessmentTypeID1.Caption = "Risk Assessment Type ID";
            this.colRiskAssessmentTypeID1.FieldName = "RiskAssessmentTypeID";
            this.colRiskAssessmentTypeID1.Name = "colRiskAssessmentTypeID1";
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowEdit = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.AllowFocus = false;
            this.colRiskAssessmentTypeID1.OptionsColumn.ReadOnly = true;
            this.colRiskAssessmentTypeID1.Width = 141;
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(1089, 211);
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(1089, 211);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gridControl3.Size = new System.Drawing.Size(1089, 211);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView3_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView3_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // sp07244UTWorkOrderManagerListBindingSource
            // 
            this.sp07244UTWorkOrderManagerListBindingSource.DataMember = "sp07244_UT_Work_Order_Manager_List";
            this.sp07244UTWorkOrderManagerListBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_AT_WorkOrders
            // 
            this.dataSet_AT_WorkOrders.DataSetName = "DataSet_AT_WorkOrders";
            this.dataSet_AT_WorkOrders.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // bbiPrintWorkOrder
            // 
            this.bbiPrintWorkOrder.Caption = "Print Work Order";
            this.bbiPrintWorkOrder.Id = 26;
            this.bbiPrintWorkOrder.ImageOptions.Image = global::WoodPlan5.PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            this.bbiPrintWorkOrder.Name = "bbiPrintWorkOrder";
            this.bbiPrintWorkOrder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrintWorkOrder_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.popupTeamFilter),
            new DevExpress.XtraBars.LinkPersistInfo(this.popupContainerDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // popupTeamFilter
            // 
            this.popupTeamFilter.Caption = "Team Filter";
            this.popupTeamFilter.Description = "Team Filter";
            this.popupTeamFilter.Edit = this.repositoryItemPopupContainerEditTeam;
            this.popupTeamFilter.EditValue = "No Team Filter";
            this.popupTeamFilter.EditWidth = 472;
            this.popupTeamFilter.Id = 30;
            this.popupTeamFilter.Name = "popupTeamFilter";
            // 
            // repositoryItemPopupContainerEditTeam
            // 
            this.repositoryItemPopupContainerEditTeam.AutoHeight = false;
            this.repositoryItemPopupContainerEditTeam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditTeam.Name = "repositoryItemPopupContainerEditTeam";
            this.repositoryItemPopupContainerEditTeam.PopupControl = this.popupContainerControlTeams;
            this.repositoryItemPopupContainerEditTeam.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditTeam_QueryResultValue);
            // 
            // popupContainerDateRange
            // 
            this.popupContainerDateRange.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.popupContainerDateRange.Caption = "Date Filter";
            this.popupContainerDateRange.Description = "Date Issued Date Filter";
            this.popupContainerDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.popupContainerDateRange.EditValue = "No Date Filter";
            this.popupContainerDateRange.EditWidth = 172;
            this.popupContainerDateRange.Id = 28;
            this.popupContainerDateRange.Name = "popupContainerDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Load Work Orders";
            this.bbiRefresh.Id = 29;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter
            // 
            this.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter
            // 
            this.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter
            // 
            this.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter.ClearBeforeFill = true;
            // 
            // sp07290_UT_Team_Work_Order_Manager_ListTableAdapter
            // 
            this.sp07290_UT_Team_Work_Order_Manager_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter
            // 
            this.sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter
            // 
            this.sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter
            // 
            this.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.gridControl4;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // xtraGridBlending22
            // 
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending22.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending22.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending22.GridControl = this.gridControl22;
            // 
            // xtraGridBlending23
            // 
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending23.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending23.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending23.GridControl = this.gridControl23;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControl6;
            // 
            // frm_UT_Team_WorkOrder_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1094, 682);
            this.Controls.Add(this.splitContainerControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_UT_Team_WorkOrder_Manager";
            this.ShowIcon = true;
            this.Text = "Utilities - Team Work Order Manager";
            this.Activated += new System.EventHandler(this.frm_UT_Team_WorkOrder_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_UT_Team_WorkOrder_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_UT_Team_WorkOrder_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07290UTTeamWorkOrderManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTeams)).EndInit();
            this.popupContainerControlTeams.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07289UTTeamWorkOrderTeamFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageLinkedActions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07245UTWorkOrderManagerLinkedActionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            this.xtraTabPageLinkedSubContractors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.xtraTabPageLinkedMaps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07270UTWorkOrderManagerLinkedMapsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.xtraTabPageLinkRiskAssessments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07291UTTeamWorkOrderRiskAssessmentListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07244UTWorkOrderManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_WorkOrders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditTeam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedActions;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_AT dataSet_AT;
        private DataSet_AT_WorkOrders dataSet_AT_WorkOrders;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colActionID;
        private DevExpress.XtraBars.BarButtonItem bbiPrintWorkOrder;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp07244UTWorkOrderManagerListBindingSource;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DataSet_UT_WorkOrderTableAdapters.sp07244_UT_Work_Order_Manager_ListTableAdapter sp07244_UT_Work_Order_Manager_ListTableAdapter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem popupContainerDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private System.Windows.Forms.BindingSource sp07245UTWorkOrderManagerLinkedActionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID;
        private DataSet_UT_WorkOrderTableAdapters.sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter sp07245_UT_Work_Order_Manager_Linked_ActionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colDateSubContractorSent;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCompleted;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalCost;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedActionCount;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedMaps;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedSubContractors;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private System.Windows.Forms.BindingSource sp07269UTWorkOrderManagerLinkedSubContractorsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID2;
        private DataSet_UT_WorkOrderTableAdapters.sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter sp07269_UT_Work_Order_Manager_Linked_SubContractorsTableAdapter;
        private System.Windows.Forms.BindingSource sp07270UTWorkOrderManagerLinkedMapsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colMapID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderID2;
        private DevExpress.XtraGrid.Columns.GridColumn colMap;
        private DevExpress.XtraGrid.Columns.GridColumn colMapDateTimeCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colMapDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMapOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureTakenByName;
        private DevExpress.XtraGrid.Columns.GridColumn colPictureRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPaddedWorkOrderID3;
        private DataSet_UT_WorkOrderTableAdapters.sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter sp07270_UT_Work_Order_Manager_Linked_MapsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditMap;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderSortOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkOrderPDF;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraBars.BarEditItem popupTeamFilter;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditTeam;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTeams;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.SimpleButton btnTeamFilterOK;
        private System.Windows.Forms.BindingSource sp07290UTTeamWorkOrderManagerListBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07290_UT_Team_Work_Order_Manager_ListTableAdapter sp07290_UT_Team_Work_Order_Manager_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp07289UTTeamWorkOrderTeamFilterListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamID;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamName;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalTeam;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colMobileTel1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone11;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone21;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode1;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DataSet_UT_WorkOrderTableAdapters.sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter sp07289_UT_Team_Work_Order_Team_Filter_ListTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkRiskAssessments;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl22;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colSurveyedPoleID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeRecorded;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionChecked;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalAssessmentVerified;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorkingID;
        private DevExpress.XtraGrid.Columns.GridColumn colPermitHolderName;
        private DevExpress.XtraGrid.Columns.GridColumn colNRSWAOpeningNoticeNo;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit21;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID6;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLastUpdated;
        private DevExpress.XtraGrid.Columns.GridColumn colElectricalRiskCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOfWorking;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingType;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonCompletingName;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colMEWPArialRescuers;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyKitLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLat;
        private DevExpress.XtraGrid.Columns.GridColumn colAirAmbulanceLong;
        private DevExpress.XtraGrid.Columns.GridColumn colBanksMan;
        private DevExpress.XtraGrid.Columns.GridColumn colEmergencyServicesMeetPoint;
        private DevExpress.XtraGrid.Columns.GridColumn colMobilePhoneSignalBars;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestAandE;
        private DevExpress.XtraGrid.Columns.GridColumn colNearestLandLine;
        private DevExpress.XtraGrid.GridControl gridControl23;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionDescription;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit22;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionNotes;
        private DevExpress.XtraGrid.Columns.GridColumn colQuestionOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswer;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraGrid.Columns.GridColumn colMasterQuestionID;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID7;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentDateTimeRecorded;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentType1;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText1;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText2;
        private DevExpress.XtraGrid.Columns.GridColumn colAnswerText3;
        private DevExpress.XtraGrid.Columns.GridColumn colRiskAssessmentTypeID1;
        private System.Windows.Forms.BindingSource sp07291UTTeamWorkOrderRiskAssessmentListBindingSource;
        private System.Windows.Forms.BindingSource sp07292UTTeamWorkOrderRiskAssessmentQuestionsListBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter sp07291_UT_Team_Work_Order_Risk_Assessment_ListTableAdapter;
        private DataSet_UT_WorkOrderTableAdapters.sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter sp07292_UT_Team_Work_Order_Risk_Assessment_Questions_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTreeReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colLastStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colActionStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending22;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending23;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHoldCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colHeldUpType;
        private DevExpress.XtraGrid.Columns.GridColumn colIsShutdownRequired;
    }
}

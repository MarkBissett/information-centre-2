namespace WoodPlan5
{
    partial class frm_UT_Surveyed_Tree_Permission_Documents_View
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT_WorkOrder = new WoodPlan5.DataSet_UT_WorkOrder();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPermissionDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSignatureFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDFFile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToClientDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailedToCustomerDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLandownerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDataEntryScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportLayoutName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter = new WoodPlan5.DataSet_UT_WorkOrderTableAdapters.sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(649, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 443);
            this.barDockControlBottom.Size = new System.Drawing.Size(649, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 417);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(649, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 417);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 32;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(648, 382);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource
            // 
            this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource.DataMember = "sp07166_UT_Permission_Documents_From_Surveyed_Trees";
            this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource.DataSource = this.dataSet_UT_WorkOrder;
            // 
            // dataSet_UT_WorkOrder
            // 
            this.dataSet_UT_WorkOrder.DataSetName = "DataSet_UT_WorkOrder";
            this.dataSet_UT_WorkOrder.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPermissionDocumentID,
            this.colDateRaised,
            this.colRaisedByID,
            this.colSignatureFile,
            this.colPDFFile,
            this.colEmailedToClientDate,
            this.colEmailedToCustomerDate,
            this.colRemarks,
            this.colLandownerName1,
            this.colRaisedByName,
            this.colReferenceNumber,
            this.colDataEntryScreenName,
            this.colReportLayoutName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLandownerName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colPermissionDocumentID
            // 
            this.colPermissionDocumentID.Caption = "Permission Document ID";
            this.colPermissionDocumentID.FieldName = "PermissionDocumentID";
            this.colPermissionDocumentID.Name = "colPermissionDocumentID";
            this.colPermissionDocumentID.OptionsColumn.AllowEdit = false;
            this.colPermissionDocumentID.OptionsColumn.AllowFocus = false;
            this.colPermissionDocumentID.OptionsColumn.ReadOnly = true;
            this.colPermissionDocumentID.Width = 136;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 97;
            // 
            // colRaisedByID
            // 
            this.colRaisedByID.Caption = "Raised By ID";
            this.colRaisedByID.FieldName = "RaisedByID";
            this.colRaisedByID.Name = "colRaisedByID";
            this.colRaisedByID.OptionsColumn.AllowEdit = false;
            this.colRaisedByID.OptionsColumn.AllowFocus = false;
            this.colRaisedByID.OptionsColumn.ReadOnly = true;
            this.colRaisedByID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByID.Width = 82;
            // 
            // colSignatureFile
            // 
            this.colSignatureFile.Caption = "Customer Signature File";
            this.colSignatureFile.FieldName = "SignatureFile";
            this.colSignatureFile.Name = "colSignatureFile";
            this.colSignatureFile.OptionsColumn.ReadOnly = true;
            this.colSignatureFile.Visible = true;
            this.colSignatureFile.VisibleIndex = 6;
            this.colSignatureFile.Width = 200;
            // 
            // colPDFFile
            // 
            this.colPDFFile.Caption = "Saved Permission Document";
            this.colPDFFile.FieldName = "PDFFile";
            this.colPDFFile.Name = "colPDFFile";
            this.colPDFFile.OptionsColumn.ReadOnly = true;
            this.colPDFFile.Visible = true;
            this.colPDFFile.VisibleIndex = 7;
            this.colPDFFile.Width = 200;
            // 
            // colEmailedToClientDate
            // 
            this.colEmailedToClientDate.Caption = "Emailed To Client";
            this.colEmailedToClientDate.FieldName = "EmailedToClientDate";
            this.colEmailedToClientDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToClientDate.Name = "colEmailedToClientDate";
            this.colEmailedToClientDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToClientDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToClientDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToClientDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToClientDate.Visible = true;
            this.colEmailedToClientDate.VisibleIndex = 4;
            this.colEmailedToClientDate.Width = 102;
            // 
            // colEmailedToCustomerDate
            // 
            this.colEmailedToCustomerDate.Caption = "Emailed To Customer";
            this.colEmailedToCustomerDate.FieldName = "EmailedToCustomerDate";
            this.colEmailedToCustomerDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEmailedToCustomerDate.Name = "colEmailedToCustomerDate";
            this.colEmailedToCustomerDate.OptionsColumn.AllowEdit = false;
            this.colEmailedToCustomerDate.OptionsColumn.AllowFocus = false;
            this.colEmailedToCustomerDate.OptionsColumn.ReadOnly = true;
            this.colEmailedToCustomerDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEmailedToCustomerDate.Visible = true;
            this.colEmailedToCustomerDate.VisibleIndex = 5;
            this.colEmailedToCustomerDate.Width = 121;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 8;
            this.colRemarks.Width = 95;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLandownerName1
            // 
            this.colLandownerName1.Caption = "Land Owner Name";
            this.colLandownerName1.FieldName = "OwnerName";
            this.colLandownerName1.Name = "colLandownerName1";
            this.colLandownerName1.OptionsColumn.AllowEdit = false;
            this.colLandownerName1.OptionsColumn.AllowFocus = false;
            this.colLandownerName1.OptionsColumn.ReadOnly = true;
            this.colLandownerName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLandownerName1.Visible = true;
            this.colLandownerName1.VisibleIndex = 1;
            this.colLandownerName1.Width = 198;
            // 
            // colRaisedByName
            // 
            this.colRaisedByName.Caption = "Raised By";
            this.colRaisedByName.FieldName = "RaisedByName";
            this.colRaisedByName.Name = "colRaisedByName";
            this.colRaisedByName.OptionsColumn.AllowEdit = false;
            this.colRaisedByName.OptionsColumn.AllowFocus = false;
            this.colRaisedByName.OptionsColumn.ReadOnly = true;
            this.colRaisedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByName.Visible = true;
            this.colRaisedByName.VisibleIndex = 3;
            this.colRaisedByName.Width = 97;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference Number";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 2;
            this.colReferenceNumber.Width = 111;
            // 
            // colDataEntryScreenName
            // 
            this.colDataEntryScreenName.Caption = "Data Entry Screen";
            this.colDataEntryScreenName.FieldName = "DataEntryScreenName";
            this.colDataEntryScreenName.Name = "colDataEntryScreenName";
            this.colDataEntryScreenName.OptionsColumn.AllowEdit = false;
            this.colDataEntryScreenName.OptionsColumn.AllowFocus = false;
            this.colDataEntryScreenName.OptionsColumn.ReadOnly = true;
            this.colDataEntryScreenName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDataEntryScreenName.Width = 109;
            // 
            // colReportLayoutName
            // 
            this.colReportLayoutName.Caption = "Report Layout";
            this.colReportLayoutName.FieldName = "ReportLayoutName";
            this.colReportLayoutName.Name = "colReportLayoutName";
            this.colReportLayoutName.OptionsColumn.AllowEdit = false;
            this.colReportLayoutName.OptionsColumn.AllowFocus = false;
            this.colReportLayoutName.OptionsColumn.ReadOnly = true;
            this.colReportLayoutName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportLayoutName.Width = 90;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(481, 415);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(562, 415);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(648, 382);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter
            // 
            this.sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_UT_Surveyed_Tree_Permission_Documents_View
            // 
            this.ClientSize = new System.Drawing.Size(649, 443);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Surveyed_Tree_Permission_Documents_View";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Surveyed Tree - View Permissions";
            this.Load += new System.EventHandler(this.frm_UT_Surveyed_Tree_Permission_Documents_View_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT_WorkOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPermissionDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colSignatureFile;
        private DevExpress.XtraGrid.Columns.GridColumn colPDFFile;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToClientDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailedToCustomerDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLandownerName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByName;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_UT_WorkOrder dataSet_UT_WorkOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private System.Windows.Forms.BindingSource sp07166UTPermissionDocumentsFromSurveyedTreesBindingSource;
        private DataSet_UT_WorkOrderTableAdapters.sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter sp07166_UT_Permission_Documents_From_Surveyed_TreesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDataEntryScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colReportLayoutName;
    }
}

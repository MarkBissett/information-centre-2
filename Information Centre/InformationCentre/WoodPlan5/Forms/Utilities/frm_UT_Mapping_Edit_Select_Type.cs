﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace WoodPlan5
{
    public partial class frm_UT_Mapping_Edit_Select_Type : DevExpress.XtraEditors.XtraForm
    {
        #region Instance Variables

        public string strReturnedValue = "Cancel";

        #endregion

        public frm_UT_Mapping_Edit_Select_Type()
        {
            InitializeComponent();
        }

        private void btnAmenityTrees_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Poles";
            this.Close();
        }

        private void btnAssets_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Trees";
            this.Close();
        }

        private void btnBoth_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Both";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            strReturnedValue = "Cancel";
            this.Close();
        }




    }
}
namespace WoodPlan5
{
    partial class frm_UT_Select_Pole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_UT_Select_Pole));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03054EPSelectClientListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp07003UTSelectCircuitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_UT = new WoodPlan5.DataSet_UT();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCircuitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltageType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCoordinatePairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLatLongPairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp07042UTPoleSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPoleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colInspectionCycle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInspectionUnitDesc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextInspectionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLatitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTransformer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTransformerNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCircuitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrimaryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFeederID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleSubAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPoleSubAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.sp07003_UT_Select_CircuitTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07003_UT_Select_CircuitTableAdapter();
            this.sp03054_EP_Select_Client_ListTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03054_EP_Select_Client_ListTableAdapter();
            this.sp07042_UT_Pole_SelectTableAdapter = new WoodPlan5.DataSet_UTTableAdapters.sp07042_UT_Pole_SelectTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03054EPSelectClientListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07003UTSelectCircuitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07042UTPoleSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 641);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 615);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 615);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Clients";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Related Circuits";
            this.splitContainerControl1.Size = new System.Drawing.Size(628, 578);
            this.splitContainerControl1.SplitterPosition = 152;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(603, 149);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.sp03054EPSelectClientListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(603, 149);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp03054EPSelectClientListBindingSource
            // 
            this.sp03054EPSelectClientListBindingSource.DataMember = "sp03054_EP_Select_Client_List";
            this.sp03054EPSelectClientListBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 240;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Width = 101;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 108;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 1;
            this.colClientTypeDescription.Width = 153;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 170;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Related Circuits";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Related Poles";
            this.splitContainerControl2.Size = new System.Drawing.Size(628, 420);
            this.splitContainerControl2.SplitterPosition = 192;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(603, 189);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp07003UTSelectCircuitBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl2.Size = new System.Drawing.Size(603, 189);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp07003UTSelectCircuitBindingSource
            // 
            this.sp07003UTSelectCircuitBindingSource.DataMember = "sp07003_UT_Select_Circuit";
            this.sp07003UTSelectCircuitBindingSource.DataSource = this.dataSet_UT;
            // 
            // dataSet_UT
            // 
            this.dataSet_UT.DataSetName = "DataSet_UT";
            this.dataSet_UT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCircuitID,
            this.colClientID1,
            this.colClientName1,
            this.colClientCode1,
            this.colStatusID,
            this.colCircuitName,
            this.colCircuitNumber,
            this.colVoltageID,
            this.colVoltageType,
            this.colFeederName,
            this.colCoordinatePairs,
            this.colLatLongPairs,
            this.colRemarks1,
            this.colStatus,
            this.colPrimaryName,
            this.colRegionName,
            this.colSubAreaName});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCircuitNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colCircuitID
            // 
            this.colCircuitID.Caption = "Circuit ID";
            this.colCircuitID.FieldName = "CircuitID";
            this.colCircuitID.Name = "colCircuitID";
            this.colCircuitID.OptionsColumn.AllowEdit = false;
            this.colCircuitID.OptionsColumn.AllowFocus = false;
            this.colCircuitID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 2;
            this.colClientName1.Width = 152;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.Width = 76;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCircuitName
            // 
            this.colCircuitName.Caption = "Circuit Name";
            this.colCircuitName.FieldName = "CircuitName";
            this.colCircuitName.Name = "colCircuitName";
            this.colCircuitName.OptionsColumn.AllowEdit = false;
            this.colCircuitName.OptionsColumn.AllowFocus = false;
            this.colCircuitName.OptionsColumn.ReadOnly = true;
            this.colCircuitName.Visible = true;
            this.colCircuitName.VisibleIndex = 0;
            this.colCircuitName.Width = 212;
            // 
            // colCircuitNumber
            // 
            this.colCircuitNumber.Caption = "Circuit Number";
            this.colCircuitNumber.FieldName = "CircuitNumber";
            this.colCircuitNumber.Name = "colCircuitNumber";
            this.colCircuitNumber.OptionsColumn.AllowEdit = false;
            this.colCircuitNumber.OptionsColumn.AllowFocus = false;
            this.colCircuitNumber.OptionsColumn.ReadOnly = true;
            this.colCircuitNumber.Visible = true;
            this.colCircuitNumber.VisibleIndex = 1;
            this.colCircuitNumber.Width = 144;
            // 
            // colVoltageID
            // 
            this.colVoltageID.Caption = "Voltage ID";
            this.colVoltageID.FieldName = "VoltageID";
            this.colVoltageID.Name = "colVoltageID";
            this.colVoltageID.OptionsColumn.AllowEdit = false;
            this.colVoltageID.OptionsColumn.AllowFocus = false;
            this.colVoltageID.OptionsColumn.ReadOnly = true;
            // 
            // colVoltageType
            // 
            this.colVoltageType.Caption = "Voltage Type";
            this.colVoltageType.FieldName = "VoltageType";
            this.colVoltageType.Name = "colVoltageType";
            this.colVoltageType.OptionsColumn.AllowEdit = false;
            this.colVoltageType.OptionsColumn.AllowFocus = false;
            this.colVoltageType.OptionsColumn.ReadOnly = true;
            this.colVoltageType.Visible = true;
            this.colVoltageType.VisibleIndex = 3;
            this.colVoltageType.Width = 127;
            // 
            // colFeederName
            // 
            this.colFeederName.Caption = "Feeder Name";
            this.colFeederName.FieldName = "FeederName";
            this.colFeederName.Name = "colFeederName";
            this.colFeederName.OptionsColumn.AllowEdit = false;
            this.colFeederName.OptionsColumn.AllowFocus = false;
            this.colFeederName.OptionsColumn.ReadOnly = true;
            this.colFeederName.Visible = true;
            this.colFeederName.VisibleIndex = 7;
            this.colFeederName.Width = 136;
            // 
            // colCoordinatePairs
            // 
            this.colCoordinatePairs.Caption = "Coordinate Pairs";
            this.colCoordinatePairs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colCoordinatePairs.FieldName = "CoordinatePairs";
            this.colCoordinatePairs.Name = "colCoordinatePairs";
            this.colCoordinatePairs.OptionsColumn.ReadOnly = true;
            this.colCoordinatePairs.Width = 203;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLatLongPairs
            // 
            this.colLatLongPairs.Caption = "Lat\\Long Pairs";
            this.colLatLongPairs.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colLatLongPairs.FieldName = "LatLongPairs";
            this.colLatLongPairs.Name = "colLatLongPairs";
            this.colLatLongPairs.OptionsColumn.ReadOnly = true;
            this.colLatLongPairs.Width = 200;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 9;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 8;
            this.colStatus.Width = 62;
            // 
            // colPrimaryName
            // 
            this.colPrimaryName.Caption = "Primary";
            this.colPrimaryName.FieldName = "PrimaryName";
            this.colPrimaryName.Name = "colPrimaryName";
            this.colPrimaryName.OptionsColumn.AllowEdit = false;
            this.colPrimaryName.OptionsColumn.AllowFocus = false;
            this.colPrimaryName.OptionsColumn.ReadOnly = true;
            this.colPrimaryName.Visible = true;
            this.colPrimaryName.VisibleIndex = 6;
            this.colPrimaryName.Width = 127;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 4;
            this.colRegionName.Width = 123;
            // 
            // colSubAreaName
            // 
            this.colSubAreaName.Caption = "Sub Area";
            this.colSubAreaName.FieldName = "SubAreaName";
            this.colSubAreaName.Name = "colSubAreaName";
            this.colSubAreaName.OptionsColumn.AllowEdit = false;
            this.colSubAreaName.OptionsColumn.AllowFocus = false;
            this.colSubAreaName.OptionsColumn.ReadOnly = true;
            this.colSubAreaName.Visible = true;
            this.colSubAreaName.VisibleIndex = 5;
            this.colSubAreaName.Width = 118;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(603, 219);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp07042UTPoleSelectBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit1});
            this.gridControl3.Size = new System.Drawing.Size(603, 219);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp07042UTPoleSelectBindingSource
            // 
            this.sp07042UTPoleSelectBindingSource.DataMember = "sp07042_UT_Pole_Select";
            this.sp07042UTPoleSelectBindingSource.DataSource = this.dataSet_UT;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPoleID,
            this.gridColumn1,
            this.colPoleNumber,
            this.colPoleTypeID,
            this.gridColumn2,
            this.colLastInspectionDate,
            this.colInspectionCycle,
            this.colInspectionUnit,
            this.colInspectionUnitDesc,
            this.colNextInspectionDate,
            this.colX,
            this.colY,
            this.colLatitude,
            this.colLongitude,
            this.colIsTransformer,
            this.colTransformerNumber,
            this.gridColumn3,
            this.colGUID,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colCircuitStatus,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colRegionID,
            this.colSubAreaID,
            this.colPrimaryID,
            this.colFeederID,
            this.colPoleType,
            this.colPoleSubAreaID,
            this.colPoleSubAreaName});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPoleNumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colPoleID
            // 
            this.colPoleID.Caption = "Pole ID";
            this.colPoleID.FieldName = "PoleID";
            this.colPoleID.Name = "colPoleID";
            this.colPoleID.OptionsColumn.AllowEdit = false;
            this.colPoleID.OptionsColumn.AllowFocus = false;
            this.colPoleID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Circuit ID";
            this.gridColumn1.FieldName = "CircuitID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // colPoleNumber
            // 
            this.colPoleNumber.Caption = "Pole Number";
            this.colPoleNumber.FieldName = "PoleNumber";
            this.colPoleNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPoleNumber.Name = "colPoleNumber";
            this.colPoleNumber.OptionsColumn.AllowEdit = false;
            this.colPoleNumber.OptionsColumn.AllowFocus = false;
            this.colPoleNumber.OptionsColumn.ReadOnly = true;
            this.colPoleNumber.Visible = true;
            this.colPoleNumber.VisibleIndex = 0;
            this.colPoleNumber.Width = 94;
            // 
            // colPoleTypeID
            // 
            this.colPoleTypeID.Caption = "Pole Type ID";
            this.colPoleTypeID.FieldName = "PoleTypeID";
            this.colPoleTypeID.Name = "colPoleTypeID";
            this.colPoleTypeID.OptionsColumn.AllowEdit = false;
            this.colPoleTypeID.OptionsColumn.AllowFocus = false;
            this.colPoleTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Status ID";
            this.gridColumn2.FieldName = "StatusID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // colLastInspectionDate
            // 
            this.colLastInspectionDate.Caption = "Last Inspection";
            this.colLastInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colLastInspectionDate.FieldName = "LastInspectionDate";
            this.colLastInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastInspectionDate.Name = "colLastInspectionDate";
            this.colLastInspectionDate.OptionsColumn.AllowEdit = false;
            this.colLastInspectionDate.OptionsColumn.AllowFocus = false;
            this.colLastInspectionDate.OptionsColumn.ReadOnly = true;
            this.colLastInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastInspectionDate.Visible = true;
            this.colLastInspectionDate.VisibleIndex = 8;
            this.colLastInspectionDate.Width = 94;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colInspectionCycle
            // 
            this.colInspectionCycle.Caption = "Cycle";
            this.colInspectionCycle.FieldName = "InspectionCycle";
            this.colInspectionCycle.Name = "colInspectionCycle";
            this.colInspectionCycle.OptionsColumn.AllowEdit = false;
            this.colInspectionCycle.OptionsColumn.AllowFocus = false;
            this.colInspectionCycle.OptionsColumn.ReadOnly = true;
            this.colInspectionCycle.Visible = true;
            this.colInspectionCycle.VisibleIndex = 9;
            this.colInspectionCycle.Width = 55;
            // 
            // colInspectionUnit
            // 
            this.colInspectionUnit.Caption = "Inspection Cycle Unit ID";
            this.colInspectionUnit.FieldName = "InspectionUnit";
            this.colInspectionUnit.Name = "colInspectionUnit";
            this.colInspectionUnit.OptionsColumn.AllowEdit = false;
            this.colInspectionUnit.OptionsColumn.AllowFocus = false;
            this.colInspectionUnit.OptionsColumn.ReadOnly = true;
            this.colInspectionUnit.Width = 136;
            // 
            // colInspectionUnitDesc
            // 
            this.colInspectionUnitDesc.Caption = "Cycle Unit";
            this.colInspectionUnitDesc.FieldName = "InspectionUnitDesc";
            this.colInspectionUnitDesc.Name = "colInspectionUnitDesc";
            this.colInspectionUnitDesc.OptionsColumn.AllowEdit = false;
            this.colInspectionUnitDesc.OptionsColumn.AllowFocus = false;
            this.colInspectionUnitDesc.OptionsColumn.ReadOnly = true;
            this.colInspectionUnitDesc.Visible = true;
            this.colInspectionUnitDesc.VisibleIndex = 10;
            // 
            // colNextInspectionDate
            // 
            this.colNextInspectionDate.Caption = "Next Inspection";
            this.colNextInspectionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colNextInspectionDate.FieldName = "NextInspectionDate";
            this.colNextInspectionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextInspectionDate.Name = "colNextInspectionDate";
            this.colNextInspectionDate.OptionsColumn.AllowEdit = false;
            this.colNextInspectionDate.OptionsColumn.AllowFocus = false;
            this.colNextInspectionDate.OptionsColumn.ReadOnly = true;
            this.colNextInspectionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextInspectionDate.Visible = true;
            this.colNextInspectionDate.VisibleIndex = 11;
            this.colNextInspectionDate.Width = 97;
            // 
            // colX
            // 
            this.colX.Caption = "X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.Visible = true;
            this.colX.VisibleIndex = 16;
            this.colX.Width = 59;
            // 
            // colY
            // 
            this.colY.Caption = "Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.Visible = true;
            this.colY.VisibleIndex = 17;
            this.colY.Width = 59;
            // 
            // colLatitude
            // 
            this.colLatitude.Caption = "Latitude";
            this.colLatitude.FieldName = "Latitude";
            this.colLatitude.Name = "colLatitude";
            this.colLatitude.OptionsColumn.AllowEdit = false;
            this.colLatitude.OptionsColumn.AllowFocus = false;
            this.colLatitude.OptionsColumn.ReadOnly = true;
            this.colLatitude.Visible = true;
            this.colLatitude.VisibleIndex = 18;
            this.colLatitude.Width = 66;
            // 
            // colLongitude
            // 
            this.colLongitude.Caption = "Longitude";
            this.colLongitude.FieldName = "Longitude";
            this.colLongitude.Name = "colLongitude";
            this.colLongitude.OptionsColumn.AllowEdit = false;
            this.colLongitude.OptionsColumn.AllowFocus = false;
            this.colLongitude.OptionsColumn.ReadOnly = true;
            this.colLongitude.Visible = true;
            this.colLongitude.VisibleIndex = 19;
            this.colLongitude.Width = 68;
            // 
            // colIsTransformer
            // 
            this.colIsTransformer.Caption = "Is Transformer";
            this.colIsTransformer.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTransformer.FieldName = "IsTransformer";
            this.colIsTransformer.Name = "colIsTransformer";
            this.colIsTransformer.OptionsColumn.AllowEdit = false;
            this.colIsTransformer.OptionsColumn.AllowFocus = false;
            this.colIsTransformer.OptionsColumn.ReadOnly = true;
            this.colIsTransformer.Visible = true;
            this.colIsTransformer.VisibleIndex = 20;
            this.colIsTransformer.Width = 92;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colTransformerNumber
            // 
            this.colTransformerNumber.Caption = "Transformer Number";
            this.colTransformerNumber.FieldName = "TransformerNumber";
            this.colTransformerNumber.Name = "colTransformerNumber";
            this.colTransformerNumber.OptionsColumn.AllowEdit = false;
            this.colTransformerNumber.OptionsColumn.AllowFocus = false;
            this.colTransformerNumber.OptionsColumn.ReadOnly = true;
            this.colTransformerNumber.Visible = true;
            this.colTransformerNumber.VisibleIndex = 21;
            this.colTransformerNumber.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Remarks";
            this.gridColumn3.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.gridColumn3.FieldName = "Remarks";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Client ID";
            this.gridColumn4.FieldName = "ClientID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Client Name";
            this.gridColumn5.FieldName = "ClientName";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 109;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Client Code";
            this.gridColumn6.FieldName = "ClientCode";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 23;
            this.gridColumn6.Width = 76;
            // 
            // colCircuitStatus
            // 
            this.colCircuitStatus.Caption = "Client Status";
            this.colCircuitStatus.FieldName = "CircuitStatus";
            this.colCircuitStatus.Name = "colCircuitStatus";
            this.colCircuitStatus.OptionsColumn.AllowEdit = false;
            this.colCircuitStatus.OptionsColumn.AllowFocus = false;
            this.colCircuitStatus.OptionsColumn.ReadOnly = true;
            this.colCircuitStatus.Visible = true;
            this.colCircuitStatus.VisibleIndex = 22;
            this.colCircuitStatus.Width = 82;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Circuit Name";
            this.gridColumn7.FieldName = "CircuitName";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 94;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Circuit Number";
            this.gridColumn8.FieldName = "CircuitNumber";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 24;
            this.gridColumn8.Width = 91;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Voltage ID";
            this.gridColumn9.FieldName = "VoltageID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Voltage Type";
            this.gridColumn10.FieldName = "VoltageType";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 13;
            this.gridColumn10.Width = 84;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Feeder Name";
            this.gridColumn11.FieldName = "FeederName";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 6;
            this.gridColumn11.Width = 98;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Region Name";
            this.gridColumn12.FieldName = "RegionName";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 2;
            this.gridColumn12.Width = 97;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Status";
            this.gridColumn13.FieldName = "Status";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 14;
            this.gridColumn13.Width = 63;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Primary Name";
            this.gridColumn14.FieldName = "PrimaryName";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 5;
            this.gridColumn14.Width = 100;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Sub-Area Name";
            this.gridColumn15.FieldName = "SubAreaName";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 3;
            this.gridColumn15.Width = 109;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            // 
            // colSubAreaID
            // 
            this.colSubAreaID.Caption = "Sub-Area ID";
            this.colSubAreaID.FieldName = "SubAreaID";
            this.colSubAreaID.Name = "colSubAreaID";
            this.colSubAreaID.OptionsColumn.AllowEdit = false;
            this.colSubAreaID.OptionsColumn.AllowFocus = false;
            this.colSubAreaID.OptionsColumn.ReadOnly = true;
            this.colSubAreaID.Width = 80;
            // 
            // colPrimaryID
            // 
            this.colPrimaryID.Caption = "Primary ID";
            this.colPrimaryID.FieldName = "PrimaryID";
            this.colPrimaryID.Name = "colPrimaryID";
            this.colPrimaryID.OptionsColumn.AllowEdit = false;
            this.colPrimaryID.OptionsColumn.AllowFocus = false;
            this.colPrimaryID.OptionsColumn.ReadOnly = true;
            // 
            // colFeederID
            // 
            this.colFeederID.Caption = "Feeder ID";
            this.colFeederID.FieldName = "FeederID";
            this.colFeederID.Name = "colFeederID";
            this.colFeederID.OptionsColumn.AllowEdit = false;
            this.colFeederID.OptionsColumn.AllowFocus = false;
            this.colFeederID.OptionsColumn.ReadOnly = true;
            // 
            // colPoleType
            // 
            this.colPoleType.Caption = "Pole Type";
            this.colPoleType.FieldName = "PoleType";
            this.colPoleType.Name = "colPoleType";
            this.colPoleType.OptionsColumn.AllowEdit = false;
            this.colPoleType.OptionsColumn.AllowFocus = false;
            this.colPoleType.OptionsColumn.ReadOnly = true;
            this.colPoleType.Visible = true;
            this.colPoleType.VisibleIndex = 12;
            // 
            // colPoleSubAreaID
            // 
            this.colPoleSubAreaID.Caption = "Pole Sub-Area ID";
            this.colPoleSubAreaID.FieldName = "PoleSubAreaID";
            this.colPoleSubAreaID.Name = "colPoleSubAreaID";
            this.colPoleSubAreaID.OptionsColumn.AllowEdit = false;
            this.colPoleSubAreaID.OptionsColumn.AllowFocus = false;
            this.colPoleSubAreaID.OptionsColumn.ReadOnly = true;
            this.colPoleSubAreaID.Width = 101;
            // 
            // colPoleSubAreaName
            // 
            this.colPoleSubAreaName.Caption = "Pole Sub-Area Name";
            this.colPoleSubAreaName.FieldName = "PoleSubAreaName";
            this.colPoleSubAreaName.Name = "colPoleSubAreaName";
            this.colPoleSubAreaName.OptionsColumn.AllowEdit = false;
            this.colPoleSubAreaName.OptionsColumn.AllowFocus = false;
            this.colPoleSubAreaName.OptionsColumn.ReadOnly = true;
            this.colPoleSubAreaName.Visible = true;
            this.colPoleSubAreaName.VisibleIndex = 7;
            this.colPoleSubAreaName.Width = 124;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(327, 612);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(227, 612);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // sp07003_UT_Select_CircuitTableAdapter
            // 
            this.sp07003_UT_Select_CircuitTableAdapter.ClearBeforeFill = true;
            // 
            // sp03054_EP_Select_Client_ListTableAdapter
            // 
            this.sp03054_EP_Select_Client_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp07042_UT_Pole_SelectTableAdapter
            // 
            this.sp07042_UT_Pole_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // frm_UT_Select_Pole
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(628, 641);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_UT_Select_Pole";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Pole";
            this.Load += new System.EventHandler(this.frm_UT_Select_Pole_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03054EPSelectClientListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07003UTSelectCircuitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_UT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp07042UTPoleSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource sp07003UTSelectCircuitBindingSource;
        private DataSet_UT dataSet_UT;
        private DataSet_UTTableAdapters.sp07003_UT_Select_CircuitTableAdapter sp07003_UT_Select_CircuitTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitName;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageID;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltageType;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederName;
        private DevExpress.XtraGrid.Columns.GridColumn colCoordinatePairs;
        private DevExpress.XtraGrid.Columns.GridColumn colLatLongPairs;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DataSet_EP dataSet_EP;
        private System.Windows.Forms.BindingSource sp03054EPSelectClientListBindingSource;
        private DataSet_EPTableAdapters.sp03054_EP_Select_Client_ListTableAdapter sp03054_EP_Select_Client_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaName;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp07042UTPoleSelectBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colLastInspectionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionCycle;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnit;
        private DevExpress.XtraGrid.Columns.GridColumn colInspectionUnitDesc;
        private DevExpress.XtraGrid.Columns.GridColumn colNextInspectionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colLatitude;
        private DevExpress.XtraGrid.Columns.GridColumn colLongitude;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTransformer;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransformerNumber;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colCircuitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPrimaryID;
        private DevExpress.XtraGrid.Columns.GridColumn colFeederID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleType;
        private DataSet_UTTableAdapters.sp07042_UT_Pole_SelectTableAdapter sp07042_UT_Pole_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleSubAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPoleSubAreaName;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_UT_Select_SubArea : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strSelectedValue = "";
        public string strSelectedClientName = "";
        public string strSelectedRegionName = "";
        public string strSelectedSubAreaName = "";
        public int intSelectedClientID = 0;
        public int intSelectedRegionID = 0;
        public int intSelectedSubAreaID = 0;
        public int intPassedInClientID = 0;
        public int intPassedInRegionID = 0;
        public int intPassedInSubAreaID = 0;
        GridHitInfo downHitInfo = null;

        #endregion

        public frm_UT_Select_SubArea()
        {
            InitializeComponent();
        }

        private void frm_UT_Select_SubArea_Load(object sender, EventArgs e)
        {
            this.FormID = 500006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp07016_UT_Region_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp07021_UT_SubArea_SelectTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                this.sp07015_UT_Client_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp07015_UT_Client_SelectTableAdapter.Fill(this.dataSet_UT.sp07015_UT_Client_Select);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the clients list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInClientID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], intPassedInClientID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInRegionID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["RegionID"], intPassedInRegionID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl3.ForceInitialize();
            view = (GridView)gridControl3.MainView;
            if (intPassedInSubAreaID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["SubAreaID"], intPassedInSubAreaID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

        }

        bool internalRowFocusing;
        private int GetTopDataRowHandle(int ARowHandle, GridView view)
        {
            if (view.GetVisibleIndex(ARowHandle) > 0)
            {
                int rowHandle = view.GetVisibleRowHandle(view.GetVisibleIndex(ARowHandle) - 1);
                if (view.GetParentRowHandle(ARowHandle) == rowHandle)
                {
                    return GetTopDataRowHandle(rowHandle, view);
                }
                else
                {
                    return GetBottomDataRowHandle(rowHandle, view);
                }
            }
            else return -1;
        }

        private int GetBottomDataRowHandle(int ARowHandle, GridView view)
        {
            int rowHandle = ARowHandle;
            if (view.IsGroupRow(rowHandle))
            {
                do
                {
                    rowHandle = view.GetChildRowHandle(rowHandle, view.GetChildRowCount(rowHandle) - 1);
                } while (view.IsGroupRow(rowHandle));
            }
            return rowHandle;
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Regions Available For Selection - Select a Client to see Related Regions";
                    break;
                case "gridView3":
                    message = "No Sub-Areas Available For Selection - Select a Region to see Related Sub-Areas";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT.sp07016_UT_Region_Select.Clear();
            }
            else
            {
                try
                {
                    sp07016_UT_Region_SelectTableAdapter.Fill(dataSet_UT.sp07016_UT_Region_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related regions.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["RegionID"])) + ',';
            }

            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_UT.sp07021_UT_SubArea_Select.Clear();
            }
            else
            {
                try
                {
                    sp07021_UT_SubArea_SelectTableAdapter.Fill(dataSet_UT.sp07021_UT_SubArea_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related sub-areas.\n\nTry selecting a region again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedValue))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) + " \\ " +
                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName"))) ? "Unknown Region" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName"))) + " \\ " +
                                (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SubAreaName"))) ? "Unknown Sub-Area" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SubAreaName")));
                strSelectedClientName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")));
                strSelectedRegionName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName"))) ? "Unknown Region" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "RegionName")));
                strSelectedSubAreaName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SubAreaName"))) ? "Unknown Sub-Area" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SubAreaName")));
                intSelectedClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                intSelectedRegionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RegionID"));
                intSelectedSubAreaID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SubAreaID"));
            }
        }



    }
}

